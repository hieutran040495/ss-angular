# Web Cashless For Client

This project was generated with [Angular CLI](https://cli.angular.io/) version 7.1.4.

## Prerequisites

- Node: v10.13.0
- Npm: v6.4.1
- Yarn: 1.12.3
- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Libraries

- [Angular](https://angular.io/): v7.1.4
- [Angular CLI](https://cli.angular.io/): v7.1.4
- [Bootstrap](https://getbootstrap.com)
- [Luxon](https://moment.github.io/luxon/index.html)
- [Prettier](https://prettier.io/)

## How to setup

#### Install Node, NPM

- [How to install nvm](https://github.com/creationix/nvm)

### Install Angular-cli

```
npm install -g @angular/cli
```

### Run project

```
cd <Project Name>

# clone evironment with mode: dev, prod, stag
cp -f src/environments/environment.ts src/environments/environment.<mode>.ts

# Install libraries
yarn install

# Run project with server localhost
yarn serve:<mode>

# Build project
yarn build
```

- Open browser with link [localhost:4200](http://localhost:4200/). Enjoy !
