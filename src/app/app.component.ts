import { Component, OnInit, OnDestroy, AfterViewInit, HostListener } from '@angular/core';

import 'shared/extensions/number';
import 'shared/extensions/string';
import 'shared/extensions/duration';

import { filter } from 'rxjs/operators';
import 'rxjs-compat/add/operator/mergeMap';
import 'rxjs-compat/add/operator/map';
import * as findIndex from 'lodash/findIndex';

import { PusherEvent } from 'shared/models/event';
import { NOTIFY_MAINTENANCE } from 'shared/enums/code-notify';

import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { MaintenanceService } from 'shared/http-services/maintenance.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { Maintenance } from 'shared/models/maintenance';
import { AppScreen } from 'shared/models/app-screen';
import { LOCALSTORAGE_KEY } from 'shared/enums/local-storage';
import { Version } from 'shared/models/version';

import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';

import { DialogMaintenanceComponent } from './modules/dialog-maintenance/dialog-maintenance.component';
import { DialogUpdateVersionComponent } from './modules/dialog-update-version/dialog-update-version.component';
import { Subscription } from 'rxjs';
import { KEY_CODE } from 'shared/enums/key-code';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {
  private maintenance: Maintenance = new Maintenance();
  private version: Version = new Version();

  private screenName: string;
  private isLoading: boolean;
  private isFirstLoading: boolean = false;
  private bsModalMaintenance: BsModalRef;
  private bsModalVersion: BsModalRef;
  private subModal: Subscription;

  @HostListener('document:keydown', ['$event'])
  onKeyDown(event: any) {
    if (event.ctrlKey && (event.which === KEY_CODE.MINUS || event.which === KEY_CODE.PLUS)) {
      event.preventDefault();
    }
  }

  @HostListener('mousewheel', ['$event'])
  onMouseScroll(event: any) {
    if (event.ctrlKey) {
      event.preventDefault();
    }
  }

  @HostListener('touchstart', ['$event'])
  onTouchStart(event: any) {
    const t2 = event.timeStamp;
    const t1 = event.currentTarget.dataset.lastTouch || t2;
    const dt = t2 - t1;
    const fingers = event.touches.length;
    event.currentTarget.dataset.lastTouch = t2;

    if (!dt || dt > 500 || fingers > 1) return;
    event.preventDefault();
    event.target.click();
  }

  get isUseInMaintenance(): boolean {
    const is_use_in_maintenance = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY.USE_IN_MAINTENANCE));
    return !!is_use_in_maintenance;
  }

  constructor(
    private laravelEcho: LaravelEchoService,
    private router: Router,
    private activeRouter: ActivatedRoute,
    private modalSv: BsModalService,
    private maintenanceSv: MaintenanceService,
    private toastSv: ToastService,
  ) {}

  async ngOnInit() {
    await this._initPusherGlobal();
    await this._getAppScreen();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.isFirstLoading = true;

      this._onNotifiGlobal();
      this._getMaintenanceStatus();
    }, 0);
  }

  ngOnDestroy() {
    this.laravelEcho.leaveChannelGlobal();
    if (this.subModal) {
      this.subModal.unsubscribe();
    }
  }

  private async _initPusherGlobal() {
    try {
      await this.laravelEcho.initEcho();
      await this.laravelEcho.initChannelGlobal();
      return;
    } catch (error) {
      console.error(error);
    }
  }

  private _getAppScreen() {
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .map(() => this.activeRouter)
      .map(() => {
        let route = this.activeRouter.firstChild;
        let child = route;
        while (child) {
          if (child.firstChild) {
            child = child.firstChild;
            route = child;
          } else {
            child = null;
          }
        }
        return route;
      })
      .mergeMap((route) => route.data)
      .subscribe((res) => {
        if (res.appScreen === this.screenName) {
          return;
        }
        this.screenName = res.appScreen;
        this._openDialogMaintenance();
      });
  }

  private _getMaintenanceScreen() {
    if (!this.screenName) {
      return false;
    }

    const index = findIndex(this.maintenance.app_screens, (res: AppScreen) => {
      return res.name === this.screenName;
    });
    return index !== -1 && !this.maintenance.app_screens[index].isNotMaintenance
      ? this.maintenance.app_screens[index]
      : false;
  }

  private _onNotifiGlobal() {
    this.laravelEcho.onNotificationGlobal((event: any) => {
      const eventMaintenance = new PusherEvent().deserialize(event);
      if (eventMaintenance.code && eventMaintenance.code !== 'ai-demo') {
        this._getMaintenanceStatus(eventMaintenance.code);
      }
    });
  }

  private _openDialogMaintenance() {
    if (this.isUseInMaintenance || !this.maintenance.in_maintenance_time) {
      return;
    }

    const screen = this._getMaintenanceScreen();
    if (!screen) {
      return;
    }
    const initialState = {
      maintenance: this.maintenance,
      appScreen: screen,
    };

    this._openModalWithComponent(DialogMaintenanceComponent, initialState);
  }

  private _openDialogUpdateVersion(initialState: any) {
    if (this.maintenance.in_maintenance_time) {
      return;
    }
    this._openModalWithComponent(DialogUpdateVersionComponent, initialState, true);
  }

  private _openModalWithComponent(comp, initialState?: any, isModalVersion?: boolean) {
    const config: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      initialState: initialState,
      keyboard: false,
      ignoreBackdropClick: true,
    };

    this.subModal = this.modalSv.onHidden.subscribe((reason: string) => {
      if (isModalVersion) {
        this.bsModalVersion = null;
      } else {
        this.bsModalMaintenance = null;
      }
    });

    if (isModalVersion && !this.bsModalVersion) {
      this.bsModalVersion = this.modalSv.show(comp, config);
      return;
    }

    if (!isModalVersion && !this.bsModalMaintenance) {
      this.bsModalMaintenance = this.modalSv.show(comp, config);
    }
  }

  private _getMaintenanceStatus(code?: string) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.maintenanceSv.getMaintenanceStatus().subscribe(
      (res) => {
        this.isLoading = false;
        this.maintenance.deserialize(res.maintenance);

        if (res.version) {
          this.version.deserialize(res.version);
        }

        if (this.isFirstLoading) {
          this._openDialogMaintenance();
          this.isFirstLoading = false;
          return;
        }

        if (code) {
          this._callOpenModal(code);
        }
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this.isFirstLoading = false;
      },
    );
  }

  private _callOpenModal(code: string) {
    switch (code) {
      case NOTIFY_MAINTENANCE.MAINTENANCE_START:
        return this._openDialogMaintenance();

      case NOTIFY_MAINTENANCE.MAINTENANCE_END:
        // const currentVerion = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY.LASTEST_VERSION));
        //
        // if (!currentVerion || currentVerion.id === this.version.id) {
        //   return;
        // }
        if (this.version.app_versions.web && this.version.app_versions.web.message) {
          localStorage.setItem(LOCALSTORAGE_KEY.LASTEST_VERSION, this.version.toJsonString());
          const data = {
            message: this.version.app_versions.web.message,
          };
          this._openDialogUpdateVersion(data);
        }
        return;
      default:
        break;
    }
  }
}
