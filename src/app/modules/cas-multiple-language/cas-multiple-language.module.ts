import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';

import { CasMultipleLanguageComponent } from './cas-multiple-language.component';

@NgModule({
  declarations: [CasMultipleLanguageComponent],
  imports: [CommonModule, FormsModule, SpaceValidationModule],
  exports: [CasMultipleLanguageComponent],
})
export class CasMultipleLanguageModule {}
