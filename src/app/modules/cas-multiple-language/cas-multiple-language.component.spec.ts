import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasMultipleLanguageComponent } from './cas-multiple-language.component';

describe('CasMultipleLanguageComponent', () => {
  let component: CasMultipleLanguageComponent;
  let fixture: ComponentFixture<CasMultipleLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasMultipleLanguageComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasMultipleLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
