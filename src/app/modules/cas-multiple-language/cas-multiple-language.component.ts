import {
  Component,
  Input,
  OnInit,
  ViewEncapsulation,
  HostListener,
  ViewChild,
  ElementRef,
  forwardRef,
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, NG_VALIDATORS, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';

const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = [
  {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CasMultipleLanguageComponent),
    multi: true,
  },
  {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => CasMultipleLanguageComponent),
    multi: true,
  },
];

@Component({
  selector: 'app-cas-multiple-language',
  templateUrl: './cas-multiple-language.component.html',
  styleUrls: ['./cas-multiple-language.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
  encapsulation: ViewEncapsulation.None,
})
export class CasMultipleLanguageComponent implements OnInit, ControlValueAccessor {
  private _lang: any;
  get lang(): any {
    return this._lang;
  }
  set lang(v: any) {
    if (v !== this.lang) {
      this._lang = v;
    }
  }

  constructor() {}
  @ViewChild('selectBoxRef') selectBoxRef: ElementRef;

  @Input('maxlength') maxlength: number = 255;
  @Input('required') required: boolean = false;

  @Input('validatorError') validatorError: string;
  @Input('nameRequiredErrorMsg') nameRequiredErrorMsg: string = '';
  @Input('class') class: string = '';
  @Input('languages') languages: Observable<any[]>;

  langName: string;
  langType: any;
  placeholder: string = '';

  isOpenSelect: boolean = false;

  /**
   * Overriden for interface ControlValueAccessor
   */
  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;

  @HostListener('document:click', ['$event'])
  clickOutSide(event) {
    this.languages.subscribe((res: any) => {
      const langTmp = res.filter((item: any) => !item.isSelected);
      if (langTmp.length) {
        this.isOpenSelect = this.selectBoxRef.nativeElement.contains(event.target);
      }
    });
  }

  ngOnInit() {}

  onTouched() {
    this._onTouchedCallback();
  }

  writeValue(value: any) {
    if (!value) {
      return;
    }

    this.languages.subscribe((res: any) => {
      this.langType = res.filter((lang: any) => lang.type === value.type)[0];
      this.placeholder = this.langType.placeholder;
      if (this.langType) {
        this.langType.isSelected = true;
        this.langName = value.value;
        this.lang = value;
      }
    });
  }

  registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  onChange() {
    this.lang.value = this.langName;

    if (!this.langName) {
      this._onTouchedCallback();
    }

    this._onChangeCallback(this.lang);
  }

  selectLanguage(lang: any) {
    this.langType.isSelected = false;
    lang.isSelected = true;
    this.langType = lang;
    this.lang.type = this.langType.type;
    this._onChangeCallback(this.lang);
  }

  validate(c: FormControl) {
    // if (this.langName) {
    //   this.langName = this.langName.trim();
    // }

    if (!this.langName && this.required) {
      return {
        required: {
          valid: false,
          actual: c.value,
        },
      };
    }
  }
}
