import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogUpdateVersionComponent } from './dialog-update-version.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CasDialogModule } from '../cas-dialog/cas-dialog.module';

@NgModule({
  declarations: [DialogUpdateVersionComponent],
  imports: [CommonModule, ModalModule.forRoot(), CasDialogModule],
  exports: [DialogUpdateVersionComponent],
  entryComponents: [DialogUpdateVersionComponent],
})
export class DialogUpdateVersionModule {}
