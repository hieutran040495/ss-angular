import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogUpdateVersionComponent } from './dialog-update-version.component';

describe('DialogUpdateVersionComponent', () => {
  let component: DialogUpdateVersionComponent;
  let fixture: ComponentFixture<DialogUpdateVersionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogUpdateVersionComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogUpdateVersionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
