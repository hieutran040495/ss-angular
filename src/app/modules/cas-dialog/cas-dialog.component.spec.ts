import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasDialogComponent } from './cas-dialog.component';

describe('CasDialogComponent', () => {
  let component: CasDialogComponent;
  let fixture: ComponentFixture<CasDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
