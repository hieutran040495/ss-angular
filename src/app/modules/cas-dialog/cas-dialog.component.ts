import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { CasDialogService } from './cas-dialog.service';

@Component({
  selector: 'app-cas-dialog',
  templateUrl: './cas-dialog.component.html',
  styleUrls: ['./cas-dialog.component.scss'],
})
export class CasDialogComponent implements OnInit, OnDestroy {
  @Output() close: EventEmitter<any> = new EventEmitter<any>();

  private _title: string;

  @Input('title')
  get title(): string {
    return this._title || 'Title';
  }
  set title(v: string) {
    this._title = v;
  }

  private _hiddenHeader: boolean;
  @Input('isHiddenHeader')
  get isHiddenHeader(): boolean {
    return this._hiddenHeader || false;
  }
  set isHiddenHeader(v: boolean) {
    this._hiddenHeader = v;
  }

  private _hiddenfooter: boolean;
  @Input('isHiddenFooter')
  get isHiddenFooter(): boolean {
    return this._hiddenfooter || false;
  }
  set isHiddenFooter(v: boolean) {
    this._hiddenfooter = v;
  }

  @Input() isCloseBtn: boolean = true;

  private _closeBtnImg: string;
  @Input('closeBtnImg')
  get closeBtnImg(): string {
    return this._closeBtnImg ? this._closeBtnImg : 'assets/icon/close-dialog.svg';
  }
  set closeBtnImg(v: string) {
    this._closeBtnImg = v;
  }

  private _dialogClass: string;
  @Input('dialogClass')
  get dialogClass(): string {
    return this._dialogClass || '';
  }
  set dialogClass(v: string) {
    this._dialogClass = v;
  }

  private _headerClass: string;
  @Input('headerClass')
  get headerClass(): string {
    return this._headerClass || '';
  }
  set headerClass(v: string) {
    this._headerClass = v;
  }

  private _isBtnSquareUp: boolean;
  @Input('isBtnSquareUp')
  get isBtnSquareUp(): boolean {
    return this._isBtnSquareUp;
  }
  set isBtnSquareUp(v: boolean) {
    this._isBtnSquareUp = v;
  }

  private _isSquareConnect: boolean;
  @Input('isSquareConnect')
  get isSquareConnect(): boolean {
    return this._isSquareConnect;
  }
  get squareDisplay(): string {
    return this._isSquareConnect ? 'Square POSレジ連携中' : 'Square POS未接続';
  }
  set isSquareConnect(v: boolean) {
    this._isSquareConnect = v;
  }

  private _bodyClass: string;
  @Input('bodyClass')
  get bodyClass(): string {
    return this._bodyClass || '';
  }
  set bodyClass(v: string) {
    this._bodyClass = v;
  }

  private _footerClass: string;
  @Input('footerClass')
  get footerClass(): string {
    return this._footerClass || '';
  }
  set footerClass(v: string) {
    this._footerClass = v;
  }

  constructor(private _casDialogSv: CasDialogService) {}

  ngOnInit() {}

  ngOnDestroy() {
    this._casDialogSv.casSelectComponent = [];
  }

  closeDialog() {
    this.close.emit(true);
  }
}
