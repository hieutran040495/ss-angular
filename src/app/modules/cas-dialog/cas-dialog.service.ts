import { Injectable } from '@angular/core';
import { NgSelectComponent } from '@ng-select/ng-select';

@Injectable({
  providedIn: 'root',
})
export class CasDialogService {
  private _casSelectComponent: NgSelectComponent[];
  public get casSelectComponent(): NgSelectComponent[] {
    return this._casSelectComponent;
  }
  public set casSelectComponent(v: NgSelectComponent[]) {
    this._casSelectComponent = v;
  }

  constructor() {
    this.casSelectComponent = [];
  }
}
