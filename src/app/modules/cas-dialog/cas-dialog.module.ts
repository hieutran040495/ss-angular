import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasDialogComponent } from './cas-dialog.component';
import { ScrollerModule } from 'shared/directive/scroller/scroller.module';

@NgModule({
  imports: [CommonModule, ScrollerModule],
  declarations: [CasDialogComponent],
  exports: [CasDialogComponent],
})
export class CasDialogModule {}
