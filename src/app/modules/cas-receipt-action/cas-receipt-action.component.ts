import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-cas-receipt-action',
  templateUrl: './cas-receipt-action.component.html',
  styleUrls: ['./cas-receipt-action.component.scss'],
})
export class CasReceiptActionComponent implements OnInit {
  @Output() close: EventEmitter<any> = new EventEmitter<any>();
  @Output() minus: EventEmitter<any> = new EventEmitter<any>();
  @Output() plus: EventEmitter<any> = new EventEmitter<any>();
  @Output() deleteOption: EventEmitter<any> = new EventEmitter<any>();
  @Output() deleteItem: EventEmitter<any> = new EventEmitter<any>();

  private _quantity: number;
  @Input('quantity')
  get quantity(): number {
    return this._quantity;
  }
  set quantity(v: number) {
    this._quantity = v;
  }

  constructor() {}

  ngOnInit() {}
}
