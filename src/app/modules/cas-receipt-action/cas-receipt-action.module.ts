import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CasReceiptActionComponent } from './cas-receipt-action.component';

@NgModule({
  declarations: [CasReceiptActionComponent],
  imports: [CommonModule, FormsModule],
  exports: [CasReceiptActionComponent],
})
export class CasReceiptActionModule {}
