import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasReceiptActionComponent } from './cas-receipt-action.component';

describe('CasReceiptActionComponent', () => {
  let component: CasReceiptActionComponent;
  let fixture: ComponentFixture<CasReceiptActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasReceiptActionComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasReceiptActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
