import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfirmLogoutComponent } from './modal-confirm-logout.component';

describe('ModalConfirmLogoutComponent', () => {
  let component: ModalConfirmLogoutComponent;
  let fixture: ComponentFixture<ModalConfirmLogoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalConfirmLogoutComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfirmLogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
