import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AuthService } from 'shared/http-services/auth.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { Router } from '@angular/router';

import { SessionService } from 'shared/states/session';
import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';
import { ClientWorkingsService } from 'shared/states/client_workings';

@Component({
  selector: 'app-modal-confirm-logout',
  templateUrl: './modal-confirm-logout.component.html',
  styleUrls: ['./modal-confirm-logout.component.scss'],
})
export class ModalConfirmLogoutComponent implements OnInit {
  isLoading: boolean = false;
  oneSignalSv: any;

  constructor(
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private router: Router,
    private authService: AuthService,
    private toastSv: ToastService,
    private sessionService: SessionService,
    private clientWorkingsService: ClientWorkingsService,
    private echoSv: LaravelEchoService,
  ) {}

  ngOnInit() {}

  logout() {
    if (this.isLoading) return;

    this.isLoading = true;

    this.authService.logout().subscribe(
      (res) => {
        this.echoSv.leaveChannel();
        this.oneSignalSv.destroyOneSignal();
        sessionStorage.clear();
        localStorage.clear();
        this.sessionService.resetStore();
        this.clientWorkingsService.resetStore();
        this.toastSv.success('ログアウトしました');
        this.router.navigate(['/auth']);
        this.isLoading = false;
        this.closeModal();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
