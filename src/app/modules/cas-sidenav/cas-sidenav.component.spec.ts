import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasSidenavComponent } from './cas-sidenav.component';

describe('CasSidenavComponent', () => {
  let component: CasSidenavComponent;
  let fixture: ComponentFixture<CasSidenavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasSidenavComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasSidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
