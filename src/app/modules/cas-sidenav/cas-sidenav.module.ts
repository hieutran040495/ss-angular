import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CasSidenavComponent } from './cas-sidenav.component';
import { ModalConfirmLogoutComponent } from './modal-confirm-logout/modal-confirm-logout.component';

@NgModule({
  declarations: [CasSidenavComponent, ModalConfirmLogoutComponent],
  imports: [CommonModule, RouterModule],
  exports: [CasSidenavComponent],
  entryComponents: [ModalConfirmLogoutComponent],
})
export class CasSidenavModule {}
