import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { OneSignalService } from 'shared/logical-services/one-signal/one-signal.service';

import { ModalConfirmLogoutComponent } from './modal-confirm-logout/modal-confirm-logout.component';

@Component({
  selector: 'app-cas-sidenav',
  templateUrl: './cas-sidenav.component.html',
  styleUrls: ['./cas-sidenav.component.scss'],
})
export class CasSidenavComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  modalRef: BsModalRef;

  navigation = [
    {
      text: '予約',
      link: ['/reservations'],
      icon: 'far fa-calendar-alt',
    },
    // {
    //   text: '座席管理',
    //   link: ['/tables/view'],
    //   icon: 'fas fa-th-list',
    // },
    {
      text: '顧客情報',
      link: ['/users'],
      icon: 'fas fa-user',
    },
    {
      text: '予約履歴',
      link: ['/history'],
      icon: 'far fa-file-alt',
    },
    {
      text: '変更履歴',
      link: ['/resv-update-history'],
      icon: 'fas fa-history',
    },
    {
      text: '店舗設定',
      link: ['/setting/information'],
      icon: 'fas fa-cog',
    },
    {
      text: '着信履歴',
      link: ['/history-call'],
      icon: 'nav-item--icon__phone-reserve fas fa-phone',
    },
    {
      text: '売上分析',
      link: undefined,
      icon: 'fas fa-calculator',
      class: 'nav-item__sub',
      sub_icon: 'fas fa-chevron-down',
      sub: [
        {
          text: '会計一覧',
          link: ['/receipts'],
        },
        {
          text: '売上を分析',
          link: ['/profit-analysis/day'],
        },
      ],
    },
    {
      text: '来店分析',
      link: undefined,
      icon: 'fas fa-video',
      class: 'nav-item__sub',
      sub_icon: 'fas fa-chevron-down',
      sub: [
        {
          text: '来店分析',
          link: ['/ai-analysis'],
        },
        {
          text: '出入分析',
          link: ['/ai-analysis-access'],
        },
      ],
    },
  ];

  constructor(private oneSignalSv: OneSignalService, private modalSv: BsModalService, private router: Router) {}

  ngOnInit() {
    this.oneSignalSv.init();
  }

  ngOnDestroy() {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  openModalConfirmLogout() {
    const opts: ModalOptions = {
      class: 'modal-sm modal-dialog-centered',
      initialState: {
        oneSignalSv: this.oneSignalSv,
      },
      keyboard: false,
      ignoreBackdropClick: true,
    };

    this.openModalWithComponent(ModalConfirmLogoutComponent, opts);
  }

  private openModalWithComponent(comp, opts: ModalOptions = {}) {
    this.modalRef = this.modalSv.show(comp, opts);
  }

  goToPages(nav: any) {
    if (!nav.link) {
      const classDefault = 'nav-item__sub';
      const classOpen = 'nav-item__sub--open';

      if (nav.class.search(classOpen) > -1) {
        nav.class = classDefault;
      } else {
        nav.class = `${classDefault} ${classOpen}`;
      }
    } else {
      this.navigation.filter((item: any) => {
        if (!item.link) {
          item.class = 'nav-item__sub';
        }
      });
      this.router.navigate(nav.link);
    }
  }

  checkActiveMenu(nav) {
    if (nav.link) {
      return nav.link.indexOf(this.router.url) !== -1;
    } else {
      return nav.sub.map((item: any) => item.link[0]).indexOf(this.router.url) !== -1;
    }
  }
}
