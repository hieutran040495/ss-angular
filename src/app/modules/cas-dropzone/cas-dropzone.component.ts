import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FilePreview } from 'shared/models/file-preview';

import { ToastService } from 'shared/logical-services/toast.service';
import { DROPZONE_TYPE } from 'shared/enums/drop-zone';

@Component({
  selector: 'app-cas-dropzone',
  templateUrl: './cas-dropzone.component.html',
  styleUrls: ['./cas-dropzone.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CasDropzoneComponent implements OnInit, AfterViewInit {
  @ViewChild('formFiles') formFiles: ElementRef;
  @Input() eventDropzone: EventEmitter<any>;

  private _oldImage: string;
  @Input('oldImage')
  get oldImage(): string {
    return this._oldImage;
  }
  set oldImage(v: string) {
    this._oldImage = v;
    if (!v) {
      this.resetInput();
    }
  }

  private isMultiple: boolean;
  @Input('multiple')
  get multiple(): boolean {
    return this.isMultiple;
  }
  set multiple(v: boolean) {
    this.isMultiple = v;
  }

  private isLarge: boolean;
  @Input('large')
  get large(): boolean {
    return this.isLarge;
  }
  set large(v: boolean) {
    this.isLarge = v;
  }

  private isRect: boolean;
  @Input('rectangle')
  get rectangle(): boolean {
    return this.isRect;
  }
  set rectangle(v: boolean) {
    this.isRect = v;
  }

  private isContain: boolean;
  @Input('contain')
  get contain(): boolean {
    return this.isContain;
  }
  set contain(v: boolean) {
    this.isContain = v;
  }

  private isRemoveReview: boolean;
  @Input('removeReview')
  get removeReview(): boolean {
    return this.isRemoveReview;
  }
  set removeReview(v: boolean) {
    this.isRemoveReview = v;
    if (v) {
      this.resetInput();
    }
  }

  private isdisabled: boolean;
  @Input('disabled')
  get disabled(): boolean {
    return this.isdisabled;
  }
  set disabled(v: boolean) {
    this.isdisabled = v;
  }

  private isDelete: boolean;
  @Input('delete')
  get delete(): boolean {
    return this.isDelete;
  }
  set delete(v: boolean) {
    this.isDelete = v;
  }

  private isBtnDelete: boolean;
  @Input('btnDelete')
  get btnDelete(): boolean {
    return this.isBtnDelete;
  }
  set btnDelete(v: boolean) {
    this.isBtnDelete = v;
  }

  private fileAccept: string;
  @Input('accept')
  get accept(): string {
    return this.fileAccept || 'image/jpeg,image/png,image/jpg,image/gif';
  }
  set accept(v: string) {
    this.fileAccept = v;
  }

  private msgFileAccept: string;
  @Input('msgAccept')
  get msgAccept(): string {
    return this.msgFileAccept || `jpg, pngとgifのファイルでアップロードしてください`;
  }
  set msgAccept(v: string) {
    this.msgFileAccept = v;
  }

  private maxLength: number;
  @Input('maxFile')
  get maxFile(): number {
    return this.maxLength || 4;
  }
  set maxFile(v: number) {
    this.maxLength = v;
  }

  private maxSize: number;
  @Input('maxFileSize')
  get maxFileSize(): number {
    return this.maxSize || 3145728;
  }
  set maxFileSize(v: number) {
    this.maxSize = v * (1024 * 1024);
  }

  private msgError: string;
  @Input('error')
  get error(): string {
    return this.msgError;
  }
  set error(v: string) {
    this.msgError = v;
  }

  private rectHeight: boolean;
  @Input('rectangleHeight')
  get rectangleHeight(): boolean {
    return this.rectHeight;
  }
  set rectangleHeight(v: boolean) {
    this.rectHeight = v;
  }

  // State for drop zone CSS toggling
  isHovering: boolean;
  previewImage: any;

  private inputFiles: FilePreview[] = [];

  get isShowBtnDelete(): boolean {
    return !!this.previewImage && this.isDelete;
  }

  constructor(private toastSv: ToastService, private cdr: ChangeDetectorRef) {}

  ngOnInit() {}

  ngAfterViewInit(): void {
    this.eventDropzone.subscribe((res: any) => {
      if (!res.type) {
        return;
      }

      if (res.type === DROPZONE_TYPE.RESET_INPUT) {
        this.resetInput();
      }
    });

    if (this.oldImage) {
      this.previewImage = {
        'background-image': `url(${this.oldImage})`,
        'background-size': this.contain ? 'auto 100px' : 'cover',
      };
      this.cdr.markForCheck();
    }
  }

  toggleHover(event: boolean) {
    this.isHovering = event;
    this.cdr.markForCheck();
  }

  startUpload(files: FileList) {
    this.msgError = '';

    if (files.length === 0) {
      return;
    }

    if (this.inputFiles.length >= this.maxFile) {
      return;
    }

    for (const i of Object.keys(files)) {
      const file: any = files.item(+i);

      if (this.accept.indexOf(files.item(+i).type) === -1) {
        this.toastSv.info(this.msgAccept);
        return false;
      }

      if (+i >= this.maxFile) {
        file.isError = true;
        file.errorMsg = `Error max file: ${this.maxFile}`;
      } else if (file.size > this.maxFileSize) {
        file.isError = true;
        file.errorMsg = `Error max Size: ${this.maxFileSize}`;
        this.toastSv.info(`サイズが3MBを超える画像はアップロードできません`);
      }

      if (file.isError) {
        this.formFiles.nativeElement.value = '';
        this.cdr.markForCheck();
        return;
      }

      this.handleFile(files[i]);

      if (+i === files.length - 1) {
        this.eventDropzone.next({
          type: DROPZONE_TYPE.UPLOAD,
          data: this.inputFiles,
        });
        this.formFiles.nativeElement.value = '';
        this.inputFiles = [];
        this.cdr.markForCheck();
      }
    }
  }

  deleteImage() {
    this.eventDropzone.next({
      type: DROPZONE_TYPE.REMOVE,
    });

    if (!this.oldImage) {
      this.resetInput();
    }
  }

  private resetInput() {
    this.previewImage = undefined;
    this.formFiles.nativeElement.value = '';
    this.inputFiles = [];
    this.error = '';
    this.cdr.detectChanges();
  }

  private handleFile(inputFile: File) {
    const file: FilePreview = new FilePreview().deserialize(inputFile);
    file.origin = inputFile;

    // Render file preview
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(inputFile);

    reader.onloadend = (e: any) => {
      file.base64 = e.target.result;

      if (!this.multiple) {
        this.previewImage = {
          'background-image': `url(${e.target.result})`,
          'background-size': this.contain ? 'auto 100px' : 'cover',
        };
        this.cdr.markForCheck();
      }
    };

    this.inputFiles.push(file);
  }
}
