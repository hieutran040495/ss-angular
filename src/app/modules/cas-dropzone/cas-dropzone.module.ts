import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CasDropzoneComponent } from './cas-dropzone.component';
import { DropZoneDirective } from './drop-zone.directive';

@NgModule({
  declarations: [CasDropzoneComponent, DropZoneDirective],
  imports: [CommonModule, FormsModule],
  exports: [CasDropzoneComponent],
})
export class CasDropzoneModule {}
