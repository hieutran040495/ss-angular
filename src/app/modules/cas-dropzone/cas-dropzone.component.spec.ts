import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasDropzoneComponent } from './cas-dropzone.component';

describe('CasDropzoneComponent', () => {
  let component: CasDropzoneComponent;
  let fixture: ComponentFixture<CasDropzoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasDropzoneComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasDropzoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
