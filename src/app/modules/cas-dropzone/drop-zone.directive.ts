import { Directive, HostListener, Output, EventEmitter, ElementRef } from '@angular/core';

@Directive({
  selector: '[appDropZone]',
})
export class DropZoneDirective {
  @Output() dropped = new EventEmitter<FileList>();
  @Output() hovered = new EventEmitter<boolean>();

  constructor(private el: ElementRef) {}

  @HostListener('drop', ['$event'])
  onDrop($event) {
    $event.preventDefault();
    this.dropped.emit($event.dataTransfer.files);
    this.hovered.emit(false);
  }

  @HostListener('dragover', ['$event'])
  onDragOver($event: MouseEvent) {
    $event.preventDefault();
    this.hovered.emit(true);
  }

  @HostListener('dragleave', ['$event'])
  onDragLeave($event: MouseEvent) {
    $event.preventDefault();
    this.hovered.emit(false);
  }

  @HostListener('click', ['$event'])
  onClick($event: MouseEvent) {
    if (
      ($event.target as HTMLElement).classList.contains('action-delete') ||
      ($event.target as HTMLElement).classList.contains('cas-dz__btn-delete')
    ) {
      $event.preventDefault();
    } else {
      const el: any = this.el.nativeElement.getElementsByClassName('cas-dz__input');
      el[0].click();
    }
  }
}
