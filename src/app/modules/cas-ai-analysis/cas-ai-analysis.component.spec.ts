import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasAiAnalysisComponent } from './cas-ai-analysis.component';

describe('CasAiAnalysisComponent', () => {
  let component: CasAiAnalysisComponent;
  let fixture: ComponentFixture<CasAiAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasAiAnalysisComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasAiAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
