import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasAiAnalysisComponent } from './cas-ai-analysis.component';

@NgModule({
  declarations: [CasAiAnalysisComponent],
  imports: [CommonModule],
  exports: [CasAiAnalysisComponent],
})
export class CasAiAnalysisModule {}
