import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-cas-ai-analysis',
  templateUrl: './cas-ai-analysis.component.html',
  styleUrls: ['./cas-ai-analysis.component.scss'],
})
export class CasAiAnalysisComponent implements OnInit {
  private title: string;
  @Input('analysisTitle')
  get analysisTitle(): string {
    return this.title;
  }
  set analysisTitle(v: string) {
    this.title = v;
  }

  private styles: any;
  @Input('analysisStyles')
  get analysisStyles(): any {
    return this.styles;
  }
  set analysisStyles(v: any) {
    this.styles = v;
  }

  constructor() {}

  ngOnInit() {}
}
