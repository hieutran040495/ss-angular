import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasTableComponent } from './cas-table.component';

describe('CasTableComponent', () => {
  let component: CasTableComponent;
  let fixture: ComponentFixture<CasTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasTableComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
