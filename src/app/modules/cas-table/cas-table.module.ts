import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasTableComponent } from './cas-table.component';

@NgModule({
  declarations: [CasTableComponent],
  imports: [CommonModule],
  exports: [CasTableComponent],
})
export class CasTableModule {}
