import { Component, Input, OnInit, TemplateRef } from '@angular/core';

export interface TableHeader {
  name: string;
  width: number | string;
  class: string;
}

@Component({
  selector: 'app-cas-table',
  templateUrl: './cas-table.component.html',
  styleUrls: ['./cas-table.component.scss'],
})
export class CasTableComponent implements OnInit {
  @Input() theadTemplate: TemplateRef<any>;

  private _headers: Partial<TableHeader>[];
  @Input('headers')
  get headers(): Partial<TableHeader>[] {
    return this._headers;
  }
  set headers(v: Partial<TableHeader>[]) {
    this._headers = v;
  }

  constructor() {}

  ngOnInit() {}
}
