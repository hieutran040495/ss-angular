import { Component, OnInit, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { PhoneLookupService } from 'shared/http-services/phone-lookup.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ClientUser } from 'shared/models/client-user';
import { Regexs } from 'shared/constants/regex';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  /* tslint:disable-next-line: no-use-before-declare */
  useExisting: forwardRef(() => ResvPhoneLookupComponent),
  provide: NG_VALUE_ACCESSOR,
  multi: true,
};

@Component({
  selector: 'app-resv-phone-lookup',
  templateUrl: './resv-phone-lookup.component.html',
  styleUrls: ['./resv-phone-lookup.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
})
export class ResvPhoneLookupComponent implements OnInit, ControlValueAccessor {
  @Input() required: boolean;
  @Input() minlength: number;
  @Input() maxlength: number;
  @Input() clientUser: ClientUser;
  @Input() class: string;
  @Input() disabled: boolean;
  @Input() placeholder: string = '';
  @Input() isFormUpdate: boolean;

  rules = Regexs;
  inValidErr: boolean;
  isShowStatus: boolean = false;
  isLoading: boolean = false;
  currentUser = new ClientUser();
  hasOldInfo: boolean = false;
  errorMessage: string = '電話番号は必須です';

  isOpenUserList: boolean = false;
  userList: any[] = [];

  private msgMinLength: string = '電話番号は10文字以上で入力してください';

  @Output() foundClientUser = new EventEmitter<ClientUser>();

  private _phone: string;
  get phone(): string {
    return this._phone;
  }
  set phone(v: string) {
    this._phone = v;
    this._onChangeCallback(v);
    if (v) {
      if (v.length < 10) {
        this.errorMessage = this.msgMinLength;
      } else {
        this.onLookupPhone(v);
      }
    }
  }

  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;

  constructor(private lookupSv: PhoneLookupService, private toast: ToastService) {}

  ngOnInit() {}

  writeValue(value: any) {
    this.phone = value;
  }

  onTouched() {
    this._onTouchedCallback();
  }

  registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  onChangePhone($event, inValidErr: any) {
    this.isShowStatus = false;
    this.inValidErr = inValidErr;
    this.userList = [];

    if (inValidErr && inValidErr.required) {
      this.errorMessage = '電話番号は必須です';
    }

    if (inValidErr && inValidErr.minlength) {
      this.errorMessage = this.msgMinLength;
    }

    if (!inValidErr && $event) {
      this.onLookupPhone($event.trim());
    }
  }

  onLookupPhone(phone: string) {
    if (this.isLoading) {
      return;
    }

    this.currentUser = this.clientUser;

    this.isLoading = true;
    const opts: any = {
      phone: phone,
      with: 'finished_reservations_count',
    };
    this.lookupSv.getPhoneLookup(opts).subscribe(
      (res) => {
        if (!this.inValidErr) {
          this.isShowStatus = true;
        }

        this.isLoading = false;

        if (res.data.length) {
          if (!this.isFormUpdate) {
            this.isOpenUserList = true;
          }
          this.userList = res.data;

          if (this.isFormUpdate) {
            const userIdx = this.userList.findIndex((user: ClientUser) => user.id === this.currentUser.id);
            if (userIdx !== -1) {
              this.handleUser(this.userList[userIdx]);
            }
          }
        } else {
          if (this.hasOldInfo) {
            this.currentUser = new ClientUser().deserialize({
              name: null,
              phone: this.phone,
              memo: null,
              finished_reservations_count: 0,
              is_new: true,
            });
          }
          this.hasOldInfo = false;
          this.foundClientUser.emit(this.currentUser);
        }
      },
      (errors) => {
        this.toast.error(errors);
        this.isLoading = false;
      },
    );
  }

  chooseUser(idx: number) {
    this.isOpenUserList = false;
    this.handleUser(this.userList[idx]);
  }

  createNewUser() {
    this.isOpenUserList = false;
    this.currentUser = new ClientUser().deserialize({
      name: null,
      phone: this.phone,
      memo: null,
      finished_reservations_count: 0,
      is_new: true,
    });
    this.foundClientUser.emit(this.currentUser);
  }

  openListUser() {
    if (!this.disabled) {
      this.isOpenUserList = true;
    }
  }

  private handleUser(user: any) {
    this.isLoading = false;

    if (user) {
      this.hasOldInfo = true;
      this.currentUser.deserialize(user);
    }

    this.foundClientUser.emit(this.currentUser);
  }
}
