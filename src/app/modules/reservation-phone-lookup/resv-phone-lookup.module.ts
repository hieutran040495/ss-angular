import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';

import { ResvPhoneLookupComponent } from './resv-phone-lookup.component';

import { PhoneLookupService } from 'shared/http-services/phone-lookup.service';
import { InputTextModule } from 'shared/modules/input-text/input-text.module';

@NgModule({
  imports: [CommonModule, FormsModule, OnlyNumericModule, InputTextModule],
  declarations: [ResvPhoneLookupComponent],
  exports: [ResvPhoneLookupComponent],
  providers: [PhoneLookupService],
})
export class ResvPhoneLookupModule {}
