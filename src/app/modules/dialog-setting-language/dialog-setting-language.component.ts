import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ClientLanguage } from 'shared/models/client-language';
import { ClientService } from 'shared/http-services/client.service';
import { ToastService } from 'shared/logical-services/toast.service';

@Component({
  selector: 'app-dialog-setting-language',
  templateUrl: './dialog-setting-language.component.html',
  styleUrls: ['./dialog-setting-language.component.scss'],
})
export class DialogSettingLanguageComponent implements OnInit {
  isLoading: boolean = false;

  clientLanguage = new ClientLanguage();

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private _clientService: ClientService,
    private _toastrSv: ToastService,
  ) {}

  ngOnInit() {
    this.isLoading = true;

    this._clientService.fetchClient({ with: 'client_language' }).subscribe(
      (res) => {
        this.isLoading = false;
        this.clientLanguage = res.client_language;
      },
      (errors) => {
        this.closeModal();
        this.isLoading = false;
        this._toastrSv.error(errors);
      },
    );
  }

  closeModal(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  public saveSettingClientLanguage() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;

    this._clientService.settingClientLanguage(this.clientLanguage.toJSON()).subscribe(
      (res) => {
        this.isLoading = false;
        this._toastrSv.success('言語設定が成功しました。');
        this.closeModal();
      },
      (errors) => {
        this.isLoading = false;
        this._toastrSv.error(errors);
      },
    );
  }
}
