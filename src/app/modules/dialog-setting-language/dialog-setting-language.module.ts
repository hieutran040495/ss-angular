import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogSettingLanguageComponent } from './dialog-setting-language.component';
import { CasDialogModule } from '../cas-dialog/cas-dialog.module';
import { UiSwitchModule } from 'shared/modules/ui-switch';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, CasDialogModule, UiSwitchModule, FormsModule],
  declarations: [DialogSettingLanguageComponent],
  entryComponents: [DialogSettingLanguageComponent],
  exports: [DialogSettingLanguageComponent],
})
export class DialogSettingLanguageModule {}
