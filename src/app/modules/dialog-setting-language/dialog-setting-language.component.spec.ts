import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSettingLanguageComponent } from './dialog-setting-language.component';

describe('DialogSettingLanguageComponent', () => {
  let component: DialogSettingLanguageComponent;
  let fixture: ComponentFixture<DialogSettingLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogSettingLanguageComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSettingLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
