import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

import { DialogRichMenuComponent } from './dialog-rich-menu.component';

import { ListCategoryMenuModule } from 'app/pages/list-category-menu/list-category-menu.module';
import { KonvaCategoryLayoutModule } from 'shared/modules/konva-category-layout/konva-category-layout.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, CasDialogModule, KonvaCategoryLayoutModule, FormsModule, ListCategoryMenuModule],
  declarations: [DialogRichMenuComponent],
  entryComponents: [DialogRichMenuComponent],
})
export class DialogRichMenuModule {}
