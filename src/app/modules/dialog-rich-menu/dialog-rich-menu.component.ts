import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { SCREENS_SIZE } from 'shared/constants/screen-size';
import { SETTING_RICH_MENU } from 'shared/enums/event-emitter';

import { EventEmitterService } from 'shared/logical-services/event-emitter.service';

import { ItemCategory } from 'shared/models/item-category';

import { Subject, Subscription } from 'rxjs';

import { RichMenu } from 'shared/models/rich-menu';

@Component({
  selector: 'app-dialog-rich-menu',
  templateUrl: './dialog-rich-menu.component.html',
  styleUrls: ['./dialog-rich-menu.component.scss'],
})
export class DialogRichMenuComponent implements OnInit, OnDestroy {
  selectedScreen = SCREENS_SIZE[1].value;
  // SCREENS_SIZE = SCREENS_SIZE;
  itemCategory: ItemCategory = new ItemCategory();
  onEventEmitter: Subject<any> = new Subject();
  private _subEvent: Subscription;

  isLoading: boolean = false;

  page: number = 0;

  imageHelper = [
    'assets/img/rich-menu-helper-001.svg',
    'assets/img/rich-menu-helper-002.svg',
    'assets/img/rich-menu-helper-003.svg',
  ];

  get dialogTitle(): string {
    const str = `“${this.itemCategory.name}”のリッチメニューを`;
    return !!this.itemCategory.order_rich_menu_settings.length ? `${str}編集` : `${str}作成`;
  }

  get buttonTitle(): string {
    return !!this.itemCategory.order_rich_menu_settings.length ? '編集する' : '作成する';
  }

  get disabledBtnSave(): boolean {
    return this.isLoading || !this.itemCategory.order_rich_menu_settings[this.page].image_url;
  }

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private eventEmitterSv: EventEmitterService,
  ) {
    this._subEvent = this.onEventEmitter.subscribe((res: any) => {
      if (!res.type) return;

      if (res.type === SETTING_RICH_MENU.CLOSE_MODAL_SETTING_RICH_MENU) {
        this.closeModal(SETTING_RICH_MENU.RELOAD_RICH_MENU);
      }

      if (res.type === SETTING_RICH_MENU.ERROR_RICH_MENU) {
        this.isLoading = false;
      }
    });
  }

  ngOnInit() {
    if (!this.itemCategory.order_rich_menu_settings.length) {
      this.itemCategory.order_rich_menu_settings.push(new RichMenu());
    }
  }

  ngOnDestroy() {
    if (this._subEvent) {
      this._subEvent.unsubscribe();
    }
  }

  closeModal(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
    this.isLoading = false;
    this.eventEmitterSv.publishData({
      type: SETTING_RICH_MENU.RELOAD_RICH_MENU,
    });
  }

  public saveRichMenu() {
    if (this.isLoading) return;

    this.isLoading = true;
    this.onEventEmitter.next({
      type: SETTING_RICH_MENU.SAVE_RICH_MENU,
    });
  }
}
