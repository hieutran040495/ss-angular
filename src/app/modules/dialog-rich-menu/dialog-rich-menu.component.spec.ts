import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogRichMenuComponent } from './dialog-rich-menu.component';

describe('DialogRichMenuComponent', () => {
  let component: DialogRichMenuComponent;
  let fixture: ComponentFixture<DialogRichMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogRichMenuComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogRichMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
