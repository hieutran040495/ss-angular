import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MissedCallComponent } from './missed-call.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalMissedCallModule } from 'app/pages/modal-missed-call/modal-missed-call.module';

@NgModule({
  imports: [CommonModule, ModalMissedCallModule, ModalModule.forRoot()],
  declarations: [MissedCallComponent],
  exports: [MissedCallComponent],
})
export class MissedCallModule {}
