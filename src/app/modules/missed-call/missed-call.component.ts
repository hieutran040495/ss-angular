import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalMissedCallComponent } from 'app/pages/modal-missed-call/modal-missed-call.component';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { MissedCallService } from 'shared/http-services/missed-call.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { Subscription } from 'rxjs';
import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';
import { PusherEvent } from 'shared/models/event';
import { NOTIFY_CODE } from 'shared/enums/code-notify';

@Component({
  selector: 'app-missed-call',
  templateUrl: './missed-call.component.html',
  styleUrls: ['./missed-call.component.scss'],
})
export class MissedCallComponent implements OnInit, OnDestroy {
  public unsolved = 0;
  private subscriber: Subscription;

  constructor(
    private modalSv: BsModalService,
    private _missedCallService: MissedCallService,
    private eventEmitter: EventEmitterService,
    private echoSv: LaravelEchoService,
  ) {}

  ngOnInit() {
    this._getUnSolved();
    this._onEventEmitter();
    this._onNotification();
  }

  ngOnDestroy() {
    if (this.subscriber) {
      this.subscriber.unsubscribe();
    }
  }

  private _onNotification() {
    this.echoSv.onNotification((e: any) => {
      const event = new PusherEvent().deserialize(e);
      switch (event.code) {
        case NOTIFY_CODE.CALL_SOLVE_MISSED:
          this.unsolved = e.unsolved_calls_count || 0;
          break;
      }
    });
  }

  private _onEventEmitter() {
    this.subscriber = this.eventEmitter.caseNumber$.subscribe((res) => {});
  }

  private _getUnSolved() {
    const opts = {
      with: ['unsolved_calls'],
    };

    this._missedCallService.getUnSolvedMissedCall(opts).subscribe((res) => {
      this.unsolved = res.data.unsolved_calls.unsolved_calls_count;
    });
  }

  historyCallClick() {
    const opts: ModalOptions = {
      class: 'modal-lg',
    };

    this.openModalWithComponent(ModalMissedCallComponent, opts);
  }

  private openModalWithComponent(comp, opts?: ModalOptions) {
    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();
    });

    this.modalSv.show(comp, opts);
  }
}
