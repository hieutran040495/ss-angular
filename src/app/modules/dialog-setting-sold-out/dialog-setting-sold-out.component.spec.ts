import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSettingSoldOutComponent } from './dialog-setting-sold-out.component';

describe('DialogSettingSoldOutComponent', () => {
  let component: DialogSettingSoldOutComponent;
  let fixture: ComponentFixture<DialogSettingSoldOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogSettingSoldOutComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSettingSoldOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
