import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ToastService } from 'shared/logical-services/toast.service';
import { DROPZONE_TYPE } from 'shared/enums/drop-zone';
import { FileUpload } from 'shared/interfaces/file-upload';

import { ClientService } from 'shared/http-services/client.service';
import { ValidatorService } from 'shared/utils/validator.service';

import { Subscription } from 'rxjs';

interface ValidationInput {
  image: string;
}

@Component({
  selector: 'app-dialog-setting-sold-out',
  templateUrl: './dialog-setting-sold-out.component.html',
  styleUrls: ['./dialog-setting-sold-out.component.scss'],
})
export class DialogSettingSoldOutComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  soldOutImg: string;
  soldOut: FileUpload;
  eventDropzone: EventEmitter<any> = new EventEmitter<any>();

  validation: ValidationInput = {
    image: undefined,
  };

  pageSub: Subscription;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private toastSv: ToastService,
    private clientSv: ClientService,
    private validatorSv: ValidatorService,
  ) {
    this.pageSub = this.eventDropzone.subscribe((res: any) => {
      if (!res.type) {
        return;
      }
      this.validation.image = undefined;
      switch (res.type) {
        case DROPZONE_TYPE.UPLOAD:
          this.soldOut = {
            key: 'image',
            value: res.data[0].origin,
          };
          this.soldOutImg = res.data[0].origin;
          break;
        case DROPZONE_TYPE.REMOVE:
          this.soldOutImg = '';
          break;
      }
    });
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    if (this.pageSub) {
      this.pageSub.unsubscribe();
    }
  }

  closeModal(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  saveImageSoldOut() {
    if (!this.soldOutImg) {
      this.toastSv.error('画像を設定してください');
      return;
    }

    if (this.isLoading) return;

    this.isLoading = true;

    this.clientSv.settingSoldOut([this.soldOut]).subscribe(
      (res: any) => {
        this.isLoading = false;
        this.toastSv.success('売切画像を設定しました');
        this.closeModal();
      },
      (errors: any) => {
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validatorSv.setErrors(errors.errors);
        }
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }
}
