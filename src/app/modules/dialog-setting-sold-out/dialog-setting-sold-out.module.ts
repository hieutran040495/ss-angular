import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CasDropzoneModule } from 'app/modules/cas-dropzone/cas-dropzone.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

import { ValidatorService } from 'shared/utils/validator.service';

import { DialogSettingSoldOutComponent } from './dialog-setting-sold-out.component';

@NgModule({
  declarations: [DialogSettingSoldOutComponent],
  imports: [CommonModule, FormsModule, CasDialogModule, CasDropzoneModule],
  providers: [ValidatorService],
  entryComponents: [DialogSettingSoldOutComponent],
  exports: [DialogSettingSoldOutComponent],
})
export class DialogSettingSoldOutModule {}
