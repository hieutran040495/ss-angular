import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogMenuSoldOutComponent } from './dialog-menu-sold-out.component';

describe('DialogMenuSoldOutComponent', () => {
  let component: DialogMenuSoldOutComponent;
  let fixture: ComponentFixture<DialogMenuSoldOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogMenuSoldOutComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogMenuSoldOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
