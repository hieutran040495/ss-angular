import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DialogMenuSoldOutComponent } from './dialog-menu-sold-out.component';

@NgModule({
  declarations: [DialogMenuSoldOutComponent],
  imports: [CommonModule],
  entryComponents: [DialogMenuSoldOutComponent],
  exports: [DialogMenuSoldOutComponent],
})
export class DialogMenuSoldOutModule {}
