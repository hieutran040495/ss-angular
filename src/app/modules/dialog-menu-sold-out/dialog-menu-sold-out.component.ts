import { Component, Input, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { Menu } from 'shared/models/menu';

import { DIALOG_EVENT } from 'shared/enums/modes';

@Component({
  selector: 'app-dialog-menu-sold-out',
  templateUrl: './dialog-menu-sold-out.component.html',
  styleUrls: ['./dialog-menu-sold-out.component.scss'],
})
export class DialogMenuSoldOutComponent implements OnInit {
  private _menu: Menu;
  @Input('menu')
  get menu(): Menu {
    return this._menu;
  }
  set menu(v: Menu) {
    this._menu = v;
  }

  constructor(private bsModalRef: BsModalRef, private bsModalSv: BsModalService) {}

  ngOnInit() {}

  applyMenuSoldOut() {
    this.closeModal(DIALOG_EVENT.APPLY_MENU_SOLD_OUT);
  }

  closeModal(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
