import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasSwitchTimebarComponent } from './cas-switch-timebar.component';

describe('CasSwitchTimebarComponent', () => {
  let component: CasSwitchTimebarComponent;
  let fixture: ComponentFixture<CasSwitchTimebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasSwitchTimebarComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasSwitchTimebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
