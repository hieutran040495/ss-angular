import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CasSwitchTimebarComponent } from './cas-switch-timebar.component';

@NgModule({
  declarations: [CasSwitchTimebarComponent],
  imports: [CommonModule, FormsModule],
  exports: [CasSwitchTimebarComponent],
})
export class CasSwitchTimebarModule {}
