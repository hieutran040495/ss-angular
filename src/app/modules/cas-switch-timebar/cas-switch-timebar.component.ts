import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { SwitchTimebar } from 'shared/models/switch-timebar';
import { SESSION_STORAGE_KEY } from 'shared/enums/session-storage';

@Component({
  selector: 'app-cas-switch-timebar',
  templateUrl: './cas-switch-timebar.component.html',
  styleUrls: ['./cas-switch-timebar.component.scss'],
})
export class CasSwitchTimebarComponent implements OnInit {
  @Output() eventClose: EventEmitter<any> = new EventEmitter<any>();
  @Output() eventChange: EventEmitter<any> = new EventEmitter<any>();

  public selectedSerial: number;
  public scales = [2, 4, 6, 8, 10, 12, 14];
  public slotDurations = {
    2: 5,
    4: 10,
    6: 15,
    8: 20,
    10: 20,
    12: 30,
    14: 30,
  };
  public isDisabled: boolean = false;

  private _switchTimebar: SwitchTimebar = new SwitchTimebar();

  constructor() {
    if (JSON.parse(sessionStorage.getItem(SESSION_STORAGE_KEY.SWITCH_TIMEBAR))) {
      this._switchTimebar.deserialize(JSON.parse(sessionStorage.getItem(SESSION_STORAGE_KEY.SWITCH_TIMEBAR)));
    }
  }

  ngOnInit() {
    this.selectedSerial = this._switchTimebar.serial;
    this.changeScale();
  }

  public closeSwitchTimebar() {
    sessionStorage.setItem(SESSION_STORAGE_KEY.SWITCH_TIMEBAR, JSON.stringify(this._cal_values()));

    this.eventClose.emit();
  }

  public changeScale() {
    const el = $('.slider-range__input');
    const min = +el.attr('min');
    const max = +el.attr('max');
    this._switchTimebar.val_color = (this.selectedSerial - min) / (max - min);

    el.css(
      'background-image',
      '-webkit-gradient(linear, left top, right top, ' +
        'color-stop(' +
        this._switchTimebar.val_color +
        ', #f4ad49), ' +
        'color-stop(' +
        this._switchTimebar.val_color +
        ', #ccc)' +
        ')',
    );

    this.isDisabled = true;
    const timeout = setTimeout(() => {
      this.eventChange.emit(this._cal_values());
      this.isDisabled = false;
      clearTimeout(timeout);
    }, 400);
  }

  private _cal_values() {
    const scale = this.selectedSerial * 2;
    const timebar_width = window.innerWidth - 210;
    let total_slot_in_col: number;
    let total_column: number;

    if (scale === 2) {
      total_slot_in_col = 30 / this.slotDurations[scale];
      total_column = scale * 2;
    } else {
      total_slot_in_col = 60 / this.slotDurations[scale];
      total_column = scale;
    }

    const width_column = Math.round(timebar_width / total_column);
    const width_slot = Math.round(width_column / total_slot_in_col);

    return {
      slot_duration: this.slotDurations[scale],
      slot_width: width_slot,
      serial: this.selectedSerial,
      scale: scale,
      width_column: width_column,
    };
  }
}
