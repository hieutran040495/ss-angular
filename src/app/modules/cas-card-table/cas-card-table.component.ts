import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Table } from 'shared/models/table';

import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-cas-card-table',
  templateUrl: './cas-card-table.component.html',
  styleUrls: ['./cas-card-table.component.scss'],
})
export class CasCardTableComponent implements OnInit {
  @Output() changedTable: EventEmitter<Table> = new EventEmitter<Table>();

  @Input('table') table: Table = new Table();
  get img_smoking(): string {
    return this.table.id && this.table.can_smoke ? 'assets/icons/icon_smoke.png' : 'assets/icons/icon_nosmoke.png';
  }

  constructor() {}

  ngOnInit() {}

  public changeTable() {
    this.changedTable.emit(cloneDeep(this.table));
  }
}
