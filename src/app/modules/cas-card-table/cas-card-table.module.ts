import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasCardTableComponent } from './cas-card-table.component';

@NgModule({
  declarations: [CasCardTableComponent],
  imports: [CommonModule],
  exports: [CasCardTableComponent],
})
export class CasCardTableModule {}
