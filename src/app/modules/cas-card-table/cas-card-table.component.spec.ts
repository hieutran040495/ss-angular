import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasCardTableComponent } from './cas-card-table.component';

describe('CasCardTableComponent', () => {
  let component: CasCardTableComponent;
  let fixture: ComponentFixture<CasCardTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasCardTableComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasCardTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
