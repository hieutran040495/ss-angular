import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasReceiptItemComponent } from './cas-receipt-item.component';

describe('CasReceiptItemComponent', () => {
  let component: CasReceiptItemComponent;
  let fixture: ComponentFixture<CasReceiptItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasReceiptItemComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasReceiptItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
