import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PopoverModule } from 'ngx-bootstrap/popover';
import { CasReceiptActionModule } from 'app/modules/cas-receipt-action/cas-receipt-action.module';

import { CasReceiptItemComponent } from './cas-receipt-item.component';

@NgModule({
  declarations: [CasReceiptItemComponent],
  imports: [CommonModule, CasReceiptActionModule, PopoverModule.forRoot()],
  exports: [CasReceiptItemComponent],
})
export class CasReceiptItemModule {}
