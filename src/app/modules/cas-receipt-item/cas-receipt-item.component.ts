import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-cas-receipt-item',
  templateUrl: './cas-receipt-item.component.html',
  styleUrls: ['./cas-receipt-item.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CasReceiptItemComponent implements OnInit {
  quantity: number = 10;

  constructor() {}

  ngOnInit() {}

  changeQuantity(type: boolean = false) {
    if (type) {
      if (this.quantity === 99) return;
      return this.quantity++;
    }

    if (this.quantity === 0) return;
    return this.quantity--;
  }
}
