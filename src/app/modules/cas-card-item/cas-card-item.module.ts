import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasCardItemComponent } from './cas-card-item.component';

@NgModule({
  declarations: [CasCardItemComponent],
  imports: [CommonModule],
  exports: [CasCardItemComponent],
})
export class CasCardItemModule {}
