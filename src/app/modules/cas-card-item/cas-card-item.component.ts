import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-cas-card-item',
  templateUrl: './cas-card-item.component.html',
  styleUrls: ['./cas-card-item.component.scss'],
})
export class CasCardItemComponent implements OnInit {
  @Output() eventClick: EventEmitter<any> = new EventEmitter();

  private cardImage: string;
  @Input('image')
  get image(): string {
    return this.cardImage || 'assets/img/no-image.png';
  }
  get style(): any {
    return {
      'background-image': `url(${this.image})`,
      'background-size': 'cover',
    };
  }
  set image(v: string) {
    this.cardImage = v;
  }

  private cardCaption: string;
  @Input('caption')
  get caption(): string {
    return this.cardCaption || '';
  }
  set caption(v: string) {
    this.cardCaption = v;
  }

  private cardContent: string;
  @Input('content')
  get content(): string {
    return this.cardContent || '';
  }
  set content(v: string) {
    this.cardContent = v;
  }

  private cardPrice: number;
  @Input('price')
  get price(): number {
    return this.cardPrice || 0;
  }
  get displayPrice(): string {
    return new Intl.NumberFormat('ja-JP', {
      style: 'currency',
      currency: 'JPY',
    }).format(this.cardPrice);
  }
  set price(v: number) {
    this.cardPrice = v;
  }

  @Input('is_show') isShow: boolean;
  @Input('recommend') recommend: boolean = false;
  @Input('isSoldOut') isSoldOut: boolean;
  @Input('class') class: string;

  constructor() {}

  ngOnInit() {}

  cardClick() {
    this.eventClick.emit(true);
  }
}
