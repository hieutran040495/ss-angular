import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasCardItemComponent } from './cas-card-item.component';

describe('CasCardItemComponent', () => {
  let component: CasCardItemComponent;
  let fixture: ComponentFixture<CasCardItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasCardItemComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasCardItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
