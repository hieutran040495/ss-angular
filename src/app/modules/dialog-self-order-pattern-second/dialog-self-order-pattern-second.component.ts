import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SETTING_RICH_MENU, SELF_ORDER } from 'shared/enums/event-emitter';

import { StyleOpts } from 'shared/interfaces/style-setting';
import { TopImage } from 'shared/models/top-image';
import { Subject, Subscription } from 'rxjs';
import Sortable from 'sortablejs';

import * as cloneDeep from 'lodash/cloneDeep';
import { ItemCategory } from 'shared/models/item-category';
import { STYLE_SETTING_DEFAULT } from 'shared/constants/style-config-default';
import { ToastService } from 'shared/logical-services/toast.service';
import { RichMenuItem } from 'shared/models/rich-menu-item';
import { ItemCategoryService } from 'shared/http-services/item-category.service';

@Component({
  selector: 'app-dialog-self-order-pattern-second',
  templateUrl: './dialog-self-order-pattern-second.component.html',
  styleUrls: ['./dialog-self-order-pattern-second.component.scss'],
})
export class DialogSelfOrderPatternSecondComponent implements OnInit, OnDestroy {
  @ViewChild('CategoryLayout') selfOrderLayoutList: ElementRef;
  @ViewChild('SelfOrderBodyContent') selfOrderBodyContent: ElementRef;

  @Input('itemCategory') itemCategory: ItemCategory;
  @Input('maxItems') maxItems: number = 100;
  @Input('onEventEmitter') onEventEmitter: Subject<any>;
  @Input('styleSetting') styleSetting: StyleOpts;

  @Output() eventChange: EventEmitter<any> = new EventEmitter<any>();

  // Config CasCarousel
  slideItems: TopImage[];

  isLoading: boolean;

  private eventSubscribe: Subscription;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private itemCategorySv: ItemCategoryService,
    private toastSv: ToastService,
  ) {}

  ngOnInit() {
    this.initStyle();
    this.renderLayoutItems();
    this.bindingEventEmitter();
  }

  ngOnDestroy(): void {
    this.eventSubscribe.unsubscribe();
  }

  private bindingEventEmitter() {
    this.eventSubscribe = this.onEventEmitter.subscribe((res) => {
      if (!res || !res.type) {
        return;
      }

      switch (res.type) {
        case SETTING_RICH_MENU.CHANGE_RICH_MENU:
          this.addLayoutItem(res.data);
          break;
        case SELF_ORDER.SAVE_SETING_RICH_MENU:
          if (res.data.pattern === 1) {
            return;
          }
          this.saveSelfOrderSecondPattern();
          break;
        case SELF_ORDER.REMOVE_ITEM_SUCCESS:
          this.removeLayoutItem(res.data);
          break;
      }
    });
  }

  initStyle() {
    if (this.itemCategory) {
      this.styleSetting = this.itemCategory.self_order_rich_menu_settings[0].getStyleSetting();
      return;
    }

    this.styleSetting = STYLE_SETTING_DEFAULT;
  }

  saveSelfOrderSecondPattern() {
    if (this.isLoading) {
      return;
    }

    const data: any = {
      rich_menu_settings: [
        {
          pattern: 2,
          page: 1,
          image_path: null,
          pos_x: 0,
          pos_y: 0,
          ratio: 1,
          text_color: this.styleSetting.text_color,
          bg_color: this.styleSetting.bg_color,
          font_family: this.styleSetting.font_family,
          rich_menu_items: this.itemCategory.self_order_rich_menu_settings[0].items.map((item) => {
            return {
              id: item.id,
              pos_x: item.pos_x,
              pos_y: item.pos_y,
            };
          }),
        },
      ],
    };
    this.isLoading = true;
    this.itemCategorySv.settingSelfOrder(this.itemCategory.id, data).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastSv.success('リッチメニューを設定しました');
        this.onEventEmitter.next({
          type: SETTING_RICH_MENU.CLOSE_MODAL_SETTING_RICH_MENU,
        });
      },
      (errors) => {
        this.isLoading = false;
        this.toastSv.error(errors);
      },
    );
  }

  renderLayoutItems() {
    Sortable.create(this.selfOrderLayoutList.nativeElement, {
      delay: 100,
      onSort: (e: any) => {
        const originItems = cloneDeep(this.itemCategory.self_order_rich_menu_settings[0].items[e.oldIndex]);

        this.itemCategory.self_order_rich_menu_settings[0].items.splice(e.oldIndex, 1);
        this.itemCategory.self_order_rich_menu_settings[0].items.splice(e.newIndex, 0, originItems);
      },
      onChange: (e: any) => {
        this.selfOrderBodyContent.nativeElement.scrollTo({
          top:
            ((Math.floor(e.newIndex / 10) * 10) / this.itemCategory.self_order_rich_menu_settings[0].items.length) *
              this.selfOrderBodyContent.nativeElement.scrollHeight -
            this.selfOrderBodyContent.nativeElement.clientHeight / 2,
          behavior: 'smooth',
        });
      },
    });
  }

  addLayoutItem(menu: RichMenuItem) {
    if (this.itemCategory.self_order_rich_menu_settings[0].items.length < this.maxItems) {
      this.itemCategory.self_order_rich_menu_settings[0].items.push(menu);
    }
  }

  private removeLayoutItem(menu: RichMenuItem) {
    this.itemCategory.self_order_rich_menu_settings[0].items = this.itemCategory.self_order_rich_menu_settings[0].items.filter(
      (item) => item.id !== menu.id,
    );
    this.toastSv.info('メニューが一旦リッチメニューから削除されます');
  }

  drag(e: any, menu: RichMenuItem) {
    e.dataTransfer.setData('type', SELF_ORDER.REMOVE_ITEM);
    this.onEventEmitter.next({
      type: SELF_ORDER.REMOVE_ITEM,
      data: menu,
    });
  }

  touchend(event, item: any, menu: RichMenuItem) {
    const parentHeight = this.selfOrderBodyContent.nativeElement.clientHeight;
    const parentWidth = this.selfOrderBodyContent.nativeElement.clientWidth;

    if (
      event.changedTouches[0].clientX - item.clientWidth > parentWidth ||
      event.changedTouches[0].clientY - item.clientHeight > parentHeight
    ) {
      this.removeLayoutItem(menu);
      this.onEventEmitter.next({
        type: SELF_ORDER.REMOVE_ITEM_DEVICE,
        data: menu,
      });
    }
  }

  closeModal(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
