import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalSettingDisplayModule } from 'app/pages/modal-setting-display/modal-setting-display.module';
import { CasPopupGuideDragModule } from 'app/modules/cas-popup-guide-drag/cas-popup-guide-drag.module';
import { CasOrderTopImageModule } from 'shared/modules/cas-order-top-image/cas-order-top-image.module';
import { ListCategoryMenuModule } from 'app/pages/list-category-menu/list-category-menu.module';
import { DialogSelfOrderPatternSecondComponent } from './dialog-self-order-pattern-second.component';
import { CasCarouselModule } from 'app/modules/cas-carousel/cas-carousel.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CasCarouselModule,
    CasPopupGuideDragModule,
    CasOrderTopImageModule,
    ModalSettingDisplayModule,
    ModalModule.forRoot(),
    ListCategoryMenuModule,
  ],
  declarations: [DialogSelfOrderPatternSecondComponent],
  entryComponents: [DialogSelfOrderPatternSecondComponent],
  exports: [DialogSelfOrderPatternSecondComponent],
})
export class DialogSelfOrderPatternSecondModule {}
