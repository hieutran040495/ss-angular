import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSelfOrderPatternSecondComponent } from './dialog-self-order-pattern-second.component';

describe('DialogSelfOrderPatternSecondComponent', () => {
  let component: DialogSelfOrderPatternSecondComponent;
  let fixture: ComponentFixture<DialogSelfOrderPatternSecondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogSelfOrderPatternSecondComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSelfOrderPatternSecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
