import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSelfOrderComponent } from './dialog-self-order.component';

describe('DialogSelfOrderComponent', () => {
  let component: DialogSelfOrderComponent;
  let fixture: ComponentFixture<DialogSelfOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogSelfOrderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSelfOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
