import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogSelfOrderComponent } from './dialog-self-order.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CasDialogModule } from '../cas-dialog/cas-dialog.module';
import { ScrollerModule } from 'shared/directive/scroller/scroller.module';
import { CasPopupGuideDragModule } from '../cas-popup-guide-drag/cas-popup-guide-drag.module';
import { CasTopImageModule } from 'shared/modules/cas-top-image/cas-top-image.module';
import { FormsModule } from '@angular/forms';
import { ModalSettingDisplayModule } from 'app/pages/modal-setting-display/modal-setting-display.module';
import { ListCategoryMenuModule } from 'app/pages/list-category-menu/list-category-menu.module';
import { KonvaSelfOrderModule } from 'shared/modules/konva-self-order/konva-self-order.module';
import { CasCarouselModule } from '../cas-carousel/cas-carousel.module';
import { DialogSelfOrderPatternSecondModule } from '../dialog-self-order-pattern-second/dialog-self-order-pattern-second.module';
import { ModalConfirmChangePatternModule } from 'app/pages/modal-confirm-change-pattern/modal-confirm-change-pattern.module';

@NgModule({
  declarations: [DialogSelfOrderComponent],
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    CasDialogModule,
    ScrollerModule,
    CasPopupGuideDragModule,
    CasTopImageModule,
    FormsModule,
    ModalSettingDisplayModule,
    ListCategoryMenuModule,
    KonvaSelfOrderModule,
    CasCarouselModule,
    DialogSelfOrderPatternSecondModule,
    ModalModule,
    ModalConfirmChangePatternModule,
  ],
  exports: [DialogSelfOrderComponent],
  entryComponents: [DialogSelfOrderComponent],
})
export class DialogSelfOrderModule {}
