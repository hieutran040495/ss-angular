import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { SETTING_DISPLAY, SELF_ORDER, SETTING_RICH_MENU } from 'shared/enums/event-emitter';
import { Subscription, Subject } from 'rxjs';
import { ModalSettingDisplayComponent } from 'app/pages/modal-setting-display/modal-setting-display.component';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { RichMenu } from 'shared/models/rich-menu';
import { StyleOpts } from 'shared/interfaces/style-setting';
import { ItemCategory } from 'shared/models/item-category';
import { ModalConfirmChangePatternComponent } from 'app/pages/modal-confirm-change-pattern/modal-confirm-change-pattern.component';
import { STYLE_SETTING_DEFAULT } from 'shared/constants/style-config-default';
import { User } from 'shared/models/user';
import { SELF_ORDER_SCREEN_DIRECTION } from 'shared/enums/konva-size-config';

@Component({
  selector: 'app-dialog-self-order',
  templateUrl: './dialog-self-order.component.html',
  styleUrls: ['./dialog-self-order.component.scss'],
})
export class DialogSelfOrderComponent implements OnInit, OnDestroy {
  currentUser: User = new User();
  richMenu: RichMenu = new RichMenu();
  itemCategory: ItemCategory = new ItemCategory();
  hasSetting: boolean = false;
  slideItems: RichMenu[] = [];
  page: number = 1;
  pattern: number = 1;

  private _subEventDisplay: Subscription;
  private _subEventSlide: Subscription;
  onEventEmitter: Subject<any> = new Subject();

  public styleOpts: StyleOpts;

  get isFirstPattern(): boolean {
    return this.pattern === 1;
  }

  get dialogBtnSave() {
    return this.hasSetting ? '編集する' : '作成する';
  }

  get isDirectionVert(): boolean {
    return this.currentUser.client.self_order_direction === SELF_ORDER_SCREEN_DIRECTION.VERT;
  }

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private emitSv: EventEmitterService,
    private modalSv: BsModalService,
  ) {}

  ngOnInit() {
    this._renderData();
    this._onEventSub();
  }

  ngOnDestroy() {
    if (this._subEventSlide) {
      this._subEventSlide.unsubscribe();
    }
    if (this._subEventDisplay) {
      this._subEventDisplay.unsubscribe();
    }
  }

  private _onEventSub() {
    this._subEventDisplay = this.emitSv.caseNumber$.subscribe((res) => {
      if (!res && !res.type) {
        return;
      }

      switch (res.type) {
        case SETTING_DISPLAY.CHANGE_SETTING_DISPLAY:
          return this._emitSettingDislay(res.data);
      }
    });

    this._subEventSlide = this.onEventEmitter.subscribe((res) => {
      if (!res && !res.type) {
        return;
      }

      switch (res.type) {
        case SELF_ORDER.CHANGE_SLIDE:
          this.page = res.data.page;
          break;
        case SETTING_RICH_MENU.CLOSE_MODAL_SETTING_RICH_MENU:
          this.closeModal();
          this.emitSv.publishData({
            type: SETTING_RICH_MENU.RELOAD_RICH_MENU,
          });
          break;
      }
    });
  }

  private _renderData() {
    if (!this.itemCategory.self_order_rich_menu_settings || !this.itemCategory.self_order_rich_menu_settings.length) {
      return;
    }
    this.slideItems = this.itemCategory.self_order_rich_menu_settings;

    this.pattern = this.itemCategory.self_order_rich_menu_settings[0].pattern;
  }

  private _emitSettingDislay(data: StyleOpts) {
    this.styleOpts = data;
    this.onEventEmitter.next({
      type: SELF_ORDER.CHANGE_SETTING_DISPLAY,
      data: data,
    });
  }

  private _initStypeOpt(): StyleOpts {
    if (this.itemCategory.self_order_rich_menu_settings.length < this.page) {
      return;
    }
    const currentPage = this.itemCategory.self_order_rich_menu_settings[this.page - 1];
    return {
      bg_color: currentPage.bg_color,
      text_color: currentPage.text_color,
      font_family: currentPage.font_family,
    };
  }

  settingDisplay() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-md',
      ignoreBackdropClick: true,
      initialState: {
        settingOpts: !this.isFirstPattern ? this.styleOpts : this._initStypeOpt(),
      },
      keyboard: false,
    };

    this.bsModalSv.show(ModalSettingDisplayComponent, opts);
  }

  saveSettingSelfOrder() {
    return this.onEventEmitter.next({
      type: SELF_ORDER.SAVE_SETING_RICH_MENU,
      data: {
        pattern: this.pattern,
      },
    });
  }

  closeModal(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  onChangePattern(pattern: number) {
    this.pattern = pattern;
    this.page = 1;

    if (
      this.itemCategory.self_order_rich_menu_settings &&
      this.itemCategory.self_order_rich_menu_settings.length !== 0
    ) {
      this.itemCategory.self_order_rich_menu_settings = [];
    }

    this.onEventEmitter.next({
      type: SELF_ORDER.CHANGE_PATTERN,
      data: {
        itemCategory: this.itemCategory,
        pattern: pattern,
      },
    });

    const newItemCategory = new ItemCategory().deserialize(this.itemCategory);
    const newRichMenu = new RichMenu().deserialize({
      ...STYLE_SETTING_DEFAULT,
      page: this.page,
      pattern: pattern,
      pos_x: 0,
      pos_y: 0,
      items: [],
    });

    newItemCategory.self_order_rich_menu_settings[0] = newRichMenu;
    this.itemCategory = newItemCategory;

    this.slideItems = [newRichMenu];
  }

  openModalConfirmChangePattern(pattern: number) {
    if (this.pattern === pattern) {
      return;
    }

    const opts: ModalOptions = {
      class: 'modal-sm modal-dialog-centered',
      keyboard: false,
      ignoreBackdropClick: true,
    };

    this.openModalWithComponent(ModalConfirmChangePatternComponent, pattern, opts);
  }

  private openModalWithComponent(comp, pattern: number, opts?: ModalOptions) {
    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();

      if (reason === SELF_ORDER.CHANGE_PATTERN) {
        this.onChangePattern(pattern);
      }
    });

    this.modalSv.show(comp, opts);
  }
}
