import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-cas-card-options',
  templateUrl: './cas-card-options.component.html',
  styleUrls: ['./cas-card-options.component.scss'],
})
export class CasCardOptionsComponent implements OnInit {
  @Output() eventDelete: EventEmitter<any> = new EventEmitter<any>();
  @Output() eventEdit: EventEmitter<any> = new EventEmitter<any>();

  private _title: string;
  @Input('title')
  get title(): string {
    return this._title;
  }
  set title(v: string) {
    this._title = v;
  }

  private _price: string;
  @Input('price')
  get price(): string {
    return this._price;
  }
  set price(v: string) {
    this._price = v;
  }

  constructor() {}

  ngOnInit() {}

  editEvent() {
    this.eventEdit.emit(null);
  }

  trashEvent() {
    this.eventDelete.emit(null);
  }
}
