import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasCardOptionsComponent } from './cas-card-options.component';

@NgModule({
  declarations: [CasCardOptionsComponent],
  imports: [CommonModule],
  exports: [CasCardOptionsComponent],
})
export class CasCardOptionsModule {}
