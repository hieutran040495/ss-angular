import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasCardOptionsComponent } from './cas-card-options.component';

describe('CasCardOptionsComponent', () => {
  let component: CasCardOptionsComponent;
  let fixture: ComponentFixture<CasCardOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasCardOptionsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasCardOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
