import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopoverModule, PopoverDirective } from 'ngx-bootstrap/popover';

import { CasPopupGuideDragComponent } from './cas-popup-guide-drag.component';

@NgModule({
  declarations: [CasPopupGuideDragComponent],
  imports: [CommonModule, PopoverModule.forRoot()],
  exports: [CasPopupGuideDragComponent],
  providers: [PopoverDirective],
})
export class CasPopupGuideDragModule {}
