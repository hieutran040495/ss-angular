import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasPopupGuideDragComponent } from './cas-popup-guide-drag.component';

describe('CasPopupGuideDragComponent', () => {
  let component: CasPopupGuideDragComponent;
  let fixture: ComponentFixture<CasPopupGuideDragComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasPopupGuideDragComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasPopupGuideDragComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
