import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cas-popup-guide-drag',
  templateUrl: './cas-popup-guide-drag.component.html',
  styleUrls: ['./cas-popup-guide-drag.component.scss'],
})
export class CasPopupGuideDragComponent implements OnInit {
  imageHelper = [
    'assets/img/rich-menu-helper-001.svg',
    'assets/img/rich-menu-helper-002.svg',
    'assets/img/rich-menu-helper-003.svg',
  ];

  imageHelper2 = [
    {
      img: 'assets/img/rich-menu-helper-004.svg',
      name: `左でメニューの順番を\n確認`,
    },
    {
      img: 'assets/img/rich-menu-helper-005.svg',
      name: `右でメニューを長押し\nドラッグで位置を変更`,
    },
    {
      img: 'assets/img/rich-menu-helper-006.svg',
      name: `オリジナルの\nオーダー画面が完成`,
    },
  ];
  constructor() {}

  ngOnInit() {}
}
