import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogMaintenanceComponent } from './dialog-maintenance.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CasDialogModule } from '../cas-dialog/cas-dialog.module';

@NgModule({
  declarations: [DialogMaintenanceComponent],
  imports: [CommonModule, ModalModule.forRoot(), CasDialogModule],
  exports: [DialogMaintenanceComponent],
  entryComponents: [DialogMaintenanceComponent],
})
export class DialogMaintenanceModule {}
