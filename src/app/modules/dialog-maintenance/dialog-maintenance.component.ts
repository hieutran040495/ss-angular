import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Maintenance } from 'shared/models/maintenance';
import { Router } from '@angular/router';
import { AuthService } from 'shared/http-services/auth.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { SessionService } from 'shared/states/session';
import { ClientWorkingsService } from 'shared/states/client_workings';
import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';
import { AppScreen } from 'shared/models/app-screen';
import { APP_PATTERN } from 'shared/enums/maintenance';
import { LOCALSTORAGE_KEY } from 'shared/enums/local-storage';

@Component({
  selector: 'app-dialog-maintenance',
  templateUrl: './dialog-maintenance.component.html',
  styleUrls: ['./dialog-maintenance.component.scss'],
})
export class DialogMaintenanceComponent implements OnInit {
  maintenance: Maintenance = new Maintenance();
  appScreen: AppScreen;
  oneSignalSv: any;

  isLoading: boolean = false;

  get isAuth(): boolean {
    if (localStorage.getItem(LOCALSTORAGE_KEY.APP_NAME)) {
      return true;
    }
    return false;
  }
  constructor(
    private bsModalRef: BsModalRef,
    private router: Router,
    private authService: AuthService,
    private toastSv: ToastService,
    private sessionService: SessionService,
    private clientWorkingsService: ClientWorkingsService,
    private echoSv: LaravelEchoService,
  ) {}

  ngOnInit() {}

  private _closeDialog() {
    this.bsModalRef.hide();
  }

  closeDialogMaintenance() {
    if (this.appScreen && this.appScreen.pattern === APP_PATTERN.PATTERN_2) {
      return this._logout();
    }

    this._closeDialog();
  }

  private _logout() {
    if (!this.isAuth) {
      this._closeDialog();
      return this.router.navigate(['/auth/login']);
    }

    if (this.isLoading) return;

    this.isLoading = true;

    this.authService.logout().subscribe(
      (res) => {
        this.echoSv.leaveChannel();

        if (this.oneSignalSv) {
          this.oneSignalSv.destroyOneSignal();
        }

        sessionStorage.clear();
        localStorage.clear();
        this.sessionService.resetStore();
        this.clientWorkingsService.resetStore();

        this.router.navigate(['/auth/login']);
        this._closeDialog();

        this.isLoading = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }
}
