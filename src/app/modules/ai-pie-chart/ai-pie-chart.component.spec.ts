import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AiPieChartComponent } from './ai-pie-chart.component';

describe('AiPieChartComponent', () => {
  let component: AiPieChartComponent;
  let fixture: ComponentFixture<AiPieChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AiPieChartComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AiPieChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
