import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ai-pie-chart',
  templateUrl: './ai-pie-chart.component.html',
  styleUrls: ['./ai-pie-chart.component.scss'],
})
export class AiPieChartComponent implements OnInit {
  private data: number[];
  @Input('chartData')
  get chartData(): number[] {
    return this.data;
  }
  set chartData(v: number[]) {
    this.data = v;
  }

  @Input('percentLeft') percentLeft: string;
  @Input('percentRight') percentRight: string;
  @Input('isGender') isGender: boolean = false;
  @Input('labels') labels: string[];
  @Input('tooltip') tooltip: string[];
  @Input('title') title: string;
  @Input('isLoadingChart') isLoadingChart: string;

  private _isDataNotFound: boolean;
  get isDataNotFound(): boolean {
    return this._isDataNotFound || false;
  }
  @Input('isDataNotFound') set isDataNotFound(v: boolean) {
    this._isDataNotFound = v;
  }

  colors = [
    {
      backgroundColor: ['#54C2CB', '#8D8D8D'],
    },
  ];

  pieChartOptions = {
    title: {
      display: false,
    },
    legend: {
      display: false,
    },
    cutoutPercentage: 70,
    showTooltips: true,
    responsive: true,
  };

  get cardLeftTitle(): string {
    return this.isGender ? '男性' : '新規顧客';
  }
  get cardRightTitle(): string {
    return this.isGender ? '女性' : 'リピーター';
  }

  private isLeftTitle: boolean = true;

  get renderLegend(): string {
    const title: string = this.isLeftTitle ? this.cardLeftTitle : this.cardRightTitle;
    const per: string = this.isLeftTitle ? this.percentLeft : this.percentRight;

    return `<p class="mb-0 font-size-15">${title}</p><p class="mb-0 font-size-42">${per}%</p>`;
  }

  constructor() {}

  ngOnInit() {}

  changeLegend() {
    this.isLeftTitle = !this.isLeftTitle;
  }
}
