import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ChartsModule } from 'ng2-charts';
import { CasAiAnalysisModule } from 'app/modules/cas-ai-analysis/cas-ai-analysis.module';

import { AiPieChartComponent } from './ai-pie-chart.component';

@NgModule({
  declarations: [AiPieChartComponent],
  imports: [CommonModule, ChartsModule, FormsModule, CasAiAnalysisModule],
  exports: [AiPieChartComponent],
})
export class AiPieChartModule {}
