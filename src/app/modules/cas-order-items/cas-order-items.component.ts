import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Course } from 'shared/models/course';
import { Menu } from 'shared/models/menu';
import { MenuBuffet } from 'shared/models/menu-buffet';

@Component({
  selector: 'app-cas-order-items',
  templateUrl: './cas-order-items.component.html',
  styleUrls: ['./cas-order-items.component.scss'],
})
export class CasOrderItemsComponent implements OnInit {
  @Output() eventItemsChange: EventEmitter<any> = new EventEmitter();

  @Input('items') items: Array<Course | Menu | MenuBuffet>;
  @Input('disabledQuantity') disabledQuantity: boolean;

  constructor() {}

  ngOnInit() {}

  showPriceSelection(quantity: number, price: number): string {
    if (!quantity) {
      return '0';
    }
    return (quantity * price).format();
  }

  emitItemsChange() {
    this.eventItemsChange.emit({
      items: this.items,
    });
  }

  changeQuantity() {
    this.emitItemsChange();
  }

  removeSelection(item: Menu | Course | MenuBuffet) {
    this.items = this.items.filter((el: Menu | Course | MenuBuffet) => {
      return el.id !== item.id;
    });

    this.emitItemsChange();
  }
}
