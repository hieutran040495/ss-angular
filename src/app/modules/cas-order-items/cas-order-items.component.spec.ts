import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasOrderItemsComponent } from './cas-order-items.component';

describe('CasOrderItemsComponent', () => {
  let component: CasOrderItemsComponent;
  let fixture: ComponentFixture<CasOrderItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasOrderItemsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasOrderItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
