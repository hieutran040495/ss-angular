import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasOrderItemsComponent } from './cas-order-items.component';
import { InputQuantityModule } from 'shared/modules/input-quantity/input-quantity.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CasOrderItemsComponent],
  imports: [CommonModule, InputQuantityModule, FormsModule],
  exports: [CasOrderItemsComponent],
})
export class CasOrderItemsModule {}
