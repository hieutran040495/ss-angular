import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RECEIPT_SORT } from 'shared/enums/receipt-sort';

interface SearchConfig {
  placeholder: string;
  button: string;
}

@Component({
  selector: 'app-cas-filter-self-order',
  templateUrl: './cas-filter-self-order.component.html',
  styleUrls: ['./cas-filter-self-order.component.scss'],
})
export class CasFilterSelfOrderComponent implements OnInit {
  @Output() eventSort: EventEmitter<any> = new EventEmitter<any>();
  @Output() eventSearch: EventEmitter<any> = new EventEmitter<any>();

  @Input('sortSearch') sortSearch: any;
  @Input('isSelfOrder') isSelfOrder: boolean;

  private sortData: any[];
  @Input('sortItems')
  get sortItems(): any[] {
    return this.sortData || [];
  }
  get isSort(): boolean {
    return this.sortItems.length !== 0;
  }
  set sortItems(v: any[]) {
    this.sortData = v;
  }

  private _searchConf: SearchConfig;
  @Input('searchConf')
  get searchConf(): SearchConfig {
    return this._searchConf;
  }
  set searchConf(v: SearchConfig) {
    this._searchConf = v;
  }
  get isSearch(): boolean {
    return !!this.searchConf;
  }

  isAscending: boolean = false;
  disabledDirection: boolean = false;
  activedItem: any;

  constructor() {}

  ngOnInit() {
    this.sortItems.filter((item: any) => {
      if (item.active) {
        this.activedItem = item;
      }
    });
  }

  searchAction() {
    this.eventSearch.emit();
  }

  /**
   * Event sort
   * @param sort: any
   */
  sortAction(sortItem: any) {
    this.sortItems.filter((item: any) => (item.active = false));

    sortItem.active = !sortItem.active;
    this.activedItem = sortItem;

    this.emitSortData();
  }

  changeSortDirection() {
    this.isAscending = !this.isAscending;
    this.emitSortData();
  }

  emitSortData() {
    if (!this.activedItem || !this.activedItem.active) {
      this.activedItem = null;
      this.disabledDirection = true;

      this.eventSort.emit({
        order: '-updated_at',
      });

      return;
    }
    this.disabledDirection = false;

    this.eventSort.emit({
      order:
        (!this.isAscending && this.activedItem.value !== RECEIPT_SORT.IS_PAID) ||
        (this.isAscending && this.activedItem.value === RECEIPT_SORT.IS_PAID)
          ? '-' + this.activedItem.value
          : this.activedItem.value,
    });
  }
}
