import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { CasFilterSelfOrderComponent } from './cas-filter-self-order.component';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';

@NgModule({
  declarations: [CasFilterSelfOrderComponent],
  imports: [CommonModule, FormsModule, PopoverModule.forRoot(), OnlyNumericModule],
  exports: [CasFilterSelfOrderComponent],
})
export class CasFilterSelfOrderModule {}
