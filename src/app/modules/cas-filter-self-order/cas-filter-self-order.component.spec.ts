import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasFilterSelfOrderComponent } from './cas-filter-self-order.component';

describe('CasFilterSelfOrderComponent', () => {
  let component: CasFilterSelfOrderComponent;
  let fixture: ComponentFixture<CasFilterSelfOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasFilterSelfOrderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasFilterSelfOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
