import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject, Subscription } from 'rxjs';

import { Reservation } from 'shared/models/reservation';
import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { CALENDAR_EVENT_EMITTER, RESV_MODE_EMITTER, EventEmitterType } from 'shared/enums/event-emitter';

import { Walkin } from 'shared/models/walkin';
import { Mode } from 'shared/utils/mode';

import { ClientWorkingsQuery } from 'shared/states/client_workings';
import { NotifyService } from 'shared/http-services/notify.service';
import * as moment from 'moment';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { BS_DATEPICKER_CONFIG } from 'shared/constants/datepicker-config';
import { Client } from 'shared/models/client';
import { HEADER_MODE } from 'shared/enums/header';
import { ClientWorkingTimeWeek } from 'shared/models/client-working-week';
import { SessionQuery } from 'shared/states/session';
import { User } from 'shared/models/user';
import { ClientUser } from 'shared/models/client-user';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import * as $ from 'jquery';
import { DatePicker } from 'shared/mixins/date-picker';
import { applyMixins } from 'shared/utils/apply-mixins';
import { ClientProfileQuery } from 'shared/states/client-profile';
import { PusherEvent } from 'shared/models/event';
import { RESERVATION_RECEIPT_TYPE } from 'shared/enums/reservation';

@Component({
  selector: 'app-cas-header',
  templateUrl: './cas-header.component.html',
  styleUrls: ['./cas-header.component.scss'],
})
export class CasHeaderComponent implements OnInit, DatePicker {
  onShownDatepicker: (event) => void;
  _onClickNextPrev: () => void;
  _checkAndAddClassVisible: () => Promise<void>;
  _handleDayOff: (date: moment.Moment, el: JQuery, workingTime: ClientWorkingTimeWeek) => void;

  @Output() toggleEvent: EventEmitter<any> = new EventEmitter<any>();

  bsConfig = {
    ...BS_DATEPICKER_CONFIG,
    containerClass: 'bs-datepicker cashless-theme calendar-date-picker',
  };

  daysDisabled = [];

  totalUnseen: number;
  totalUnseenCsv: number;

  openRes: boolean = false;
  isWalkIn: boolean = false;
  resData = new BehaviorSubject<Reservation>(new Reservation());
  workingTimeDisplay: string;
  walkinData = new BehaviorSubject<Walkin>(new Walkin());
  selectedDate: Date = moment().toDate();

  get date_display(): string {
    return moment(this.selectedDate, 'YYYY-MM-DD').format('M/D (ddd)');
  }
  get isToday(): boolean {
    let currentDate = moment();

    if (moment().hour() < 6) {
      currentDate = moment().subtract(1, 'd');
    }

    return moment(this.selectedDate, 'YYYY-MM-DD').isSame(currentDate, 'day');
  }
  pageSub: Subscription;
  formMode: Mode = new Mode();
  numSelectedItems: number = 0;

  timeOff: { start: string; end: string } | null;

  clientProfile: Client;
  clientCurrent: User = new User();

  isShowReservation: boolean;

  dateTitle: string = undefined;

  private activeRouter: string[] = ['/', '/reservations', '/reservation'];

  constructor(
    private router: Router,
    private eventEmitter: EventEmitterService,
    private clientWorkingsQuery: ClientWorkingsQuery,
    private resvSv: ReservationService,
    private toastSv: ToastService,
    private sessionQuery: SessionQuery,
    private bsLocaleSv: BsLocaleService,
    private storeQuery: ClientProfileQuery,
    private notifySv: NotifyService,
    private laravelEchoSv: LaravelEchoService,
  ) {
    this.bsLocaleSv.use('ja');
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        const urlPath: string[] = decodeURIComponent(event.url).split('?');
        this.isShowReservation = this.activeRouter.indexOf(urlPath[0]) >= 0;
        this.resetSelectedDate();
      }
    });

    this.laravelEchoSv.onNotification((event: any) => {
      const pusherEvent: PusherEvent = new PusherEvent().deserialize(event);
      switch (pusherEvent.code) {
        case RESERVATION_RECEIPT_TYPE.KITCHEN_ITEM_UPDATE_QUANTITY:
        case RESERVATION_RECEIPT_TYPE.KITCHEN_ITEM_UPDATE_OPTIONS:
        case RESERVATION_RECEIPT_TYPE.KITCHEN_ITEM_UPDATE_STATUS:
        case RESERVATION_RECEIPT_TYPE.RESERVATION_ADD_ITEM:
        case RESERVATION_RECEIPT_TYPE.RESERVATION_ITEM_CANCEL:
        case RESERVATION_RECEIPT_TYPE.RESERVATION_UPDATE:
        case RESERVATION_RECEIPT_TYPE.RESERVATION_PAYMENT:
        case RESERVATION_RECEIPT_TYPE.RESERVATION_UNDO_PAYMENT:
          this.eventEmitter.publishData({
            type: RESERVATION_RECEIPT_TYPE.RELOAD_DETAILS,
          });
          break;
      }
    });
  }

  async ngOnInit() {
    this.getCountNotify();
    if (moment(this.selectedDate).hours() < 6) {
      this.selectedDate = moment(this.selectedDate)
        .subtract(1, 'd')
        .toDate();
    }

    this.checkTitleDate();

    this.pageSub = this.eventEmitter.caseNumber$.subscribe((res) => {
      switch (res.type) {
        case RESV_MODE_EMITTER.CREATE:
          this.isUrl();
          if (res.data.client_user) {
            this.reservationClick(null, res.data.client_user);
          }
          break;
        case RESV_MODE_EMITTER.EDIT:
          this.isUrl(res.data.start_at);
          if (res.data.isWalkIn) {
            this.walkInClick(res.data);
            return;
          }
          this.reservationClick(res.data);
          break;
        case CALENDAR_EVENT_EMITTER.EDIT_WALKIN:
          this.isUrl(res.data.start_at);
          this.walkInClick(res.data);
          break;
        case CALENDAR_EVENT_EMITTER.CREATE_WALKIN:
          this.isUrl();
          this.walkInClick(res.data);
          break;
        case EventEmitterType.UPDATE_CLIENT_PROFILE:
          this._checkDayOff();
          this.initClientProfile();
          break;
        case CALENDAR_EVENT_EMITTER.CREATE_RES_CHANGE_DATE:
          this.selectedDate = res.data;
          this.changeDate();
          break;
        case RESV_MODE_EMITTER.CLOSE:
          this.closeResStore(false);
          break;
      }
    });

    this.clientCurrent = await this.sessionQuery.getUserLoggined();

    if (this.clientCurrent && this.clientCurrent.id) {
      await this._checkDayOff();
      await this.initClientProfile();
    }
    this.checkTitleDate();
  }

  private getCountNotify() {
    this.notifySv.getNotifyStatistics().subscribe(
      (res) => {
        this.totalUnseen = res.total_unseen;
        this.totalUnseenCsv = res.total_unseen_crawler;
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  private resetSelectedDate() {
    if (!this.isToday && !this.isShowReservation) {
      this.selectedDate = moment().toDate();
      this._checkDayOff();
      this.checkTitleDate();
    }
  }

  private isUrl(start_at?: string) {
    const urlPath: string[] = decodeURIComponent(this.router.url).split('?');
    if (start_at) {
      this.selectedDate = moment(start_at).toDate();
    }
    if (this.activeRouter.indexOf(urlPath[0]) === -1) {
      this.router.navigate(['/reservations']);
      const changeDateTimeout = setTimeout(() => {
        this.changeDate();
        clearTimeout(changeDateTimeout);
      }, 200);
    }
  }

  private async initClientProfile() {
    this.clientProfile = await this.storeQuery.getClientProfile();
  }

  toggleClick() {
    this.openRes = false;
    $('.calendar-wrap').removeClass('calendar-reservation');
    this.toggleEvent.emit(true);
  }

  async walkInClick(walkin?: Walkin) {
    this.setActive(HEADER_MODE.WALKIN);
    const cMode: Mode = new Mode();
    const workingTime = await this._checkDayOff();

    if (!workingTime) {
      return;
    }
    $('.calendar-wrap').addClass('calendar-reservation');

    this.eventEmitter.publishData({
      type: CALENDAR_EVENT_EMITTER.DISABLE_CHOOSE_TABLE,
    });

    this.openRes = true;

    let data: any = {
      tables: [],
      start_time: moment().format('HH:mm:ss'),
      start_date: moment().format('YYYY/MM/DD'),
      duration: this.clientProfile ? this.clientProfile.min_reservation_duration : 1,
      quantity: 0,
    };
    cMode.setNew();
    if (walkin) {
      data = Object.assign(data, walkin);

      if (walkin.id) {
        cMode.setEdit();
      }
    }

    this.formMode = cMode;
    this.walkinData.next(new Walkin().deserialize(data));
    this.isWalkIn = true;
  }

  async reservationClick(resv?: Reservation, client_user?: ClientUser) {
    $('.calendar-wrap').addClass('calendar-reservation');

    this.setActive(HEADER_MODE.RESV);
    const cMode: Mode = new Mode();
    this.isWalkIn = false;
    this.numSelectedItems = 0;
    this.eventEmitter.publishData({
      type: CALENDAR_EVENT_EMITTER.DISABLE_CHOOSE_TABLE,
    });

    if (resv && resv.id) {
      cMode.setEdit();
      this.formMode = cMode;
      const duration = moment.duration(resv.start_time);
      if (duration.hours() < 6) {
        resv.start_date = moment(resv.start_date, 'YYYY/MM/DD')
          .subtract(1, 'days')
          .format('YYYY-MM-DD');
      }
      this.getResvItems(resv);
      return;
    }

    cMode.setNew();
    this.formMode = cMode;
    await this.initClientProfile();
    this.resData.next(this.initDataCreateResv(resv, client_user));
    this.openRes = true;
  }

  private initDataCreateResv(resv?: Reservation, client_user?: ClientUser) {
    if (resv) {
      this.numSelectedItems = resv.all_selection.length;
      return resv;
    }

    const now = moment();
    if (now.hour() <= 6) {
      now.subtract(1, 'day');
    }
    if (now.isAfter(moment(this.selectedDate, 'YYYY-MM-DD'), 'days')) {
      this.selectedDate = moment().toDate();
      this.changeDate();
    }

    const res = new Reservation().deserialize({
      tables: [],
      start_at: moment(this.selectedDate).toISOString(),
      quantity: 0,
      client_user: client_user ? client_user : new ClientUser(),
      duration: this.clientProfile ? this.clientProfile.min_reservation_duration : 1,
    });

    res.start_time = moment()
      .local()
      .format('HH:mm');

    return res;
  }

  private getResvItems(resv: Reservation) {
    const opts = {
      with: 'courses,items,buffets',
      reservation_id: resv.id,
      is_free: 0,
    };

    this.resvSv.getResvItems(opts).subscribe(
      (res) => {
        resv.deserialize(res.data);
        this.resData.next(resv);
        this.numSelectedItems = resv.all_selection.length;
        this.openRes = true;
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  private async _checkDayOff() {
    const workingTime: ClientWorkingTimeWeek = await this.clientWorkingsQuery.getWorkingTime();
    this.daysDisabled = Object.keys(workingTime)
      .filter((key) => !workingTime[key].durations.length)
      .map((dayweek) => Number(dayweek));

    this.timeOff = workingTime.getWorkingTime(moment(this.selectedDate, 'YYYY-MM-DD'));

    if (!this.timeOff) {
      // handle day off
      this.workingTimeDisplay = `休業日`;
      return;
    }

    const startDuration = moment.duration(this.timeOff.start);
    if (startDuration.asHours() >= 24) {
      startDuration.subtract(24, 'hour');
    }
    const endDuration = moment.duration(this.timeOff.end);
    if (endDuration.asHours() >= 24) {
      endDuration.subtract(24, 'hour');
    }

    this.workingTimeDisplay = `${startDuration.format('HH:mm')} ~ ${endDuration.format('HH:mm')}`;

    return this.timeOff;
  }

  closeResStore(event) {
    $('.calendar-wrap').removeClass('calendar-reservation');
    this.openRes = event;
    this.eventEmitter.publishData({
      type: CALENDAR_EVENT_EMITTER.DISABLE_CHOOSE_TABLE,
      close: true,
    });
  }

  prevDate() {
    this.selectedDate = moment(this.selectedDate)
      .subtract(1, 'days')
      .toDate();

    this.changeDate();
  }

  nextDate() {
    this.selectedDate = moment(this.selectedDate)
      .add(1, 'days')
      .toDate();

    this.changeDate();
  }

  changeDate() {
    this.eventEmitter.publishData({
      type: CALENDAR_EVENT_EMITTER.CHANGE_DATE,
      data: moment(this.selectedDate, 'YYYY-MM-DD'),
    });

    this._checkDayOff();
    this.checkTitleDate();
  }

  private setActive(headerMode: string) {
    $('.cas-header__btn').removeClass('active');

    switch (headerMode) {
      case HEADER_MODE.WALKIN:
        $('.cas-header__btn--walk-in').addClass('active');
        break;
      case HEADER_MODE.RESV:
        $('.cas-header__btn--reservation').addClass('active');
        break;
      default:
        break;
    }
  }

  async changeToCurrent() {
    this.selectedDate = moment().toDate();

    if (moment().hour() < 6) {
      this.selectedDate = moment()
        .subtract(1, 'd')
        .toDate();
    }

    this.changeDate();
  }

  checkTitleDate() {
    let currentDate = moment();

    if (moment().hour() < 6) {
      currentDate = moment().subtract(1, 'd');
    }

    const numberDate = moment(this.selectedDate, 'YYYY-MM-DD')
      .startOf('d')
      .diff(currentDate.startOf('d'), 'days');

    switch (numberDate) {
      case -2:
        this.dateTitle = '一昨日';
        break;
      case -1:
        this.dateTitle = '昨日';
        break;
      case 0:
        this.dateTitle = '今日';
        break;
      case 1:
        this.dateTitle = '明日';
        break;
      case 2:
        this.dateTitle = '明後日';
        break;
      default:
        this.dateTitle = '';
        break;
    }
  }

  _getWorkingTime(): Promise<ClientWorkingTimeWeek> {
    return this.clientWorkingsQuery.getWorkingTime();
  }
}

applyMixins(CasHeaderComponent, [DatePicker]);
