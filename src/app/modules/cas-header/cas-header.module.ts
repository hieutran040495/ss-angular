import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationModule } from 'app/pages/notification/notification.module';
import { NotificationCsvModule } from 'app/pages/notification-csv/notification-csv.module';
import { ReservationStoreModule } from 'app/modules/reservation-store/reservation-store.module';

import { CasHeaderComponent } from './cas-header.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
import { MissedCallModule } from '../missed-call/missed-call.module';

@NgModule({
  declarations: [CasHeaderComponent],
  imports: [
    CommonModule,
    ReservationStoreModule,
    NotificationModule,
    NotificationCsvModule,
    BsDatepickerModule.forRoot(),
    FormsModule,
    MissedCallModule,
  ],
  exports: [CasHeaderComponent],
})
export class CasHeaderModule {}
