import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasHeaderComponent } from './cas-header.component';

describe('CasHeaderComponent', () => {
  let component: CasHeaderComponent;
  let fixture: ComponentFixture<CasHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasHeaderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
