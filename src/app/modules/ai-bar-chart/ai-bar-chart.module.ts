import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ChartsModule } from 'ng2-charts';
import { CasAiAnalysisModule } from 'app/modules/cas-ai-analysis/cas-ai-analysis.module';

import { AiBarChartComponent } from './ai-bar-chart.component';

@NgModule({
  declarations: [AiBarChartComponent],
  imports: [CommonModule, FormsModule, ChartsModule, CasAiAnalysisModule],
  exports: [AiBarChartComponent],
})
export class AiBarChartModule {}
