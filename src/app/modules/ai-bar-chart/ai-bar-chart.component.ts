import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ai-bar-chart',
  templateUrl: './ai-bar-chart.component.html',
  styleUrls: ['./ai-bar-chart.component.scss'],
})
export class AiBarChartComponent implements OnInit {
  private data: number[];
  @Input('chartData')
  get chartData(): number[] {
    return this.data;
  }
  set chartData(v: number[]) {
    this.data = v;
  }

  @Input('labels') labels: string[];

  private _isDataNotFound: boolean;
  get isDataNotFound(): boolean {
    return this._isDataNotFound || false;
  }
  @Input('isDataNotFound') set isDataNotFound(v: boolean) {
    this._isDataNotFound = v;
  }

  @Input('colors') colors: any = [
    {
      backgroundColor: ['#54C2CB', '#54C2CB', '#54C2CB', '#8D8D8D', '#8D8D8D', '#8D8D8D', '#8D8D8D'],
    },
  ];

  // bar chart
  isLoadingBarChart: boolean = false;
  barChartOptions: any = {
    scaleShowVerticalLines: false,
    title: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false,
    },
    scales: {
      yAxes: [
        {
          gridLines: {
            display: false,
          },
          ticks: {
            fontColor: '#707070',
            fontSize: 10,
          },
        },
      ],
      xAxes: [
        {
          ticks: {
            beginAtZero: true,
            fontColor: '#BCBCBC',
            fontSize: 10,
          },
        },
      ],
    },
    tooltips: {
      callbacks: {
        label: function(tooltipItem) {
          let label = tooltipItem.yLabel || '';

          if (label) {
            label = `${label}: ${tooltipItem.xLabel}`;
          }

          return label;
        },
        title: function() {
          return '';
        },
      },
    },
  };

  constructor() {}

  ngOnInit() {}

  chartHovered(event: any): void {
    // TODO handle event when user hover
  }

  chartClicked(event: any): void {
    // TODO handle event when user click
  }
}
