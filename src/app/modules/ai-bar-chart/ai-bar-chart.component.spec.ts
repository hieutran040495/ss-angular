import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AiBarChartComponent } from './ai-bar-chart.component';

describe('AiBarChartComponent', () => {
  let component: AiBarChartComponent;
  let fixture: ComponentFixture<AiBarChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AiBarChartComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AiBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
