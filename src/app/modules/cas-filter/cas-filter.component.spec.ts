import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasFilterComponent } from './cas-filter.component';

describe('CasFilterComponent', () => {
  let component: CasFilterComponent;
  let fixture: ComponentFixture<CasFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasFilterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
