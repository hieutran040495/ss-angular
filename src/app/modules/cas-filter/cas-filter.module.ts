import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { CasFilterComponent } from './cas-filter.component';

@NgModule({
  declarations: [CasFilterComponent],
  imports: [CommonModule, FormsModule],
  exports: [CasFilterComponent],
})
export class CasFilterModule {}
