import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

interface SearchConfig {
  placeholder: string;
  button: string;
}

@Component({
  selector: 'app-cas-filter',
  templateUrl: './cas-filter.component.html',
  styleUrls: ['./cas-filter.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CasFilterComponent implements OnInit {
  @Output() eventSort: EventEmitter<any> = new EventEmitter<any>();
  @Output() eventSearch: EventEmitter<any> = new EventEmitter<any>();

  @Input('sortSearch') sortSearch: any;

  private openText: string;
  @Input('filterBtnOpenText')
  get filterOpenText(): string {
    return this.openText || '条件を絞込む';
  }
  set filterOpenText(v: string) {
    this.openText = v;
  }

  private sortData: any[];
  @Input('sortItems')
  get sortItems(): any[] {
    return this.sortData || [];
  }
  get isSort(): boolean {
    return this.sortItems.length !== 0;
  }
  set sortItems(v: any[]) {
    this.sortData = v;
  }

  private isLoading: boolean;
  @Input('filterLoading')
  get filterLoading(): boolean {
    return this.isLoading;
  }
  set filterLoading(v: boolean) {
    this.isLoading = v;
  }

  private isDisabled: boolean;
  @Input('disabled')
  get disabled(): boolean {
    return this.isDisabled;
  }
  set disabled(v: boolean) {
    this.isDisabled = v;
  }

  private _searchConf: SearchConfig;
  @Input('searchConf')
  get searchConf(): SearchConfig {
    return this._searchConf;
  }
  set searchConf(v: SearchConfig) {
    this._searchConf = v;
  }
  get isSearch(): boolean {
    return !!this.searchConf;
  }

  isShowForm: boolean = false;
  currentSort: any;

  constructor() {}

  ngOnInit() {
    this._initCurrentSort();
  }

  showFormFilter() {
    this.isShowForm = !this.isShowForm;
  }

  searchAction() {
    this.eventSearch.emit();
  }

  private _initCurrentSort() {
    const currentId: number = this.sortItems.findIndex((item: any) => item.active);
    if (currentId !== -1) {
      this.currentSort = this.sortItems[currentId];
    }
  }

  /**
   * Event sort
   * @param sort: any
   */
  sortAction(nextId: number) {
    if (this.disabled) return;

    const currentId: number = this.sortItems.findIndex((item: any) => item.active);

    if (currentId !== -1 && nextId !== currentId) {
      this.sortItems[currentId].active = false;
    }

    this.sortItems[nextId].active = !this.sortItems[nextId].active;
    this.currentSort = this.sortItems[nextId];

    this.eventSort.emit({
      order:
        this.sortItems[nextId].value.indexOf('-') === 0
          ? // support order by desc as default, eg: -created_at
            this.sortItems[nextId].active
            ? this.sortItems[nextId].value
            : this.sortItems[nextId].value.slice(1)
          : // support order by asc as default, eg: created_at
          this.sortItems[nextId].active
          ? this.sortItems[nextId].value
          : '-' + this.sortItems[nextId].value,
    });
  }

  isActiveSort(sort: any) {
    if (!this.currentSort) {
      return false;
    }
    return this.currentSort.value === sort.value;
  }
}
