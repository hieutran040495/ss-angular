import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasCardTypeComponent } from './cas-card-type.component';

@NgModule({
  declarations: [CasCardTypeComponent],
  imports: [CommonModule],
  exports: [CasCardTypeComponent],
})
export class CasCardTypeModule {}
