import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasCardTypeComponent } from './cas-card-type.component';

describe('CasCardTypeComponent', () => {
  let component: CasCardTypeComponent;
  let fixture: ComponentFixture<CasCardTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasCardTypeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasCardTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
