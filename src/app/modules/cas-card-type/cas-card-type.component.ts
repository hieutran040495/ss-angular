import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-cas-card-type',
  templateUrl: './cas-card-type.component.html',
  styleUrls: ['./cas-card-type.component.scss'],
})
export class CasCardTypeComponent implements OnInit {
  @Output() eventDelete: EventEmitter<any> = new EventEmitter<any>();
  @Output() eventEdit: EventEmitter<any> = new EventEmitter<any>();

  private textTitle: string;
  @Input('title')
  get title(): string {
    return this.textTitle;
  }
  set title(v: string) {
    this.textTitle = v;
  }

  private textCaption: string;
  @Input('caption')
  get caption(): string {
    return this.textCaption;
  }
  set caption(v: string) {
    this.textCaption = v;
  }

  private _class: string;
  @Input('class')
  get class(): string {
    return this._class;
  }
  set class(v: string) {
    this._class = v;
  }

  private toggleAction: boolean;
  @Input('isAction')
  get isAction(): boolean {
    return this.toggleAction;
  }
  set isAction(v: boolean) {
    this.toggleAction = v;
  }

  constructor() {}

  ngOnInit() {}

  editEvent() {
    this.eventEdit.emit(null);
  }

  trashEvent() {
    this.eventDelete.emit(null);
  }
}
