import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap/modal';

import { FormTrackingModule } from 'shared/directive/form-tracking/form-tracking.module';
import { ValidatorService } from 'shared/utils/validator.service';

import { ReservationOrderModule } from 'app/forms/reservation-order/reservation-order.module';
import { ReservationInfoModule } from 'app/forms/reservation-info/reservation-info.module';
import { ReservationMemoModule } from 'app/forms/reservation-memo/reservation-memo.module';
import { ReservationWalkinModule } from 'app/forms/reservation-walkin/reservation-walkin.module';
import { ModalResvCancelModule } from 'app/pages/modal-resv-cancel/modal-resv-cancel.module';
import { ModalResvConfirmCreateModule } from 'app/pages/modal-resv-confirm-create/modal-resv-confirm-create.module';
import { ModalReceiptDetailModule } from 'app/pages/modal-receipt-detail/modal-receipt-detail.module';
import { DialogConfirmFinishComponent } from './dialog-confirm-finish/dialog-confirm-finish.component';

import { ReservationStoreComponent } from './reservation-store.component';

@NgModule({
  declarations: [ReservationStoreComponent, DialogConfirmFinishComponent],
  imports: [
    CommonModule,
    FormsModule,
    FormTrackingModule,
    ReservationInfoModule,
    ReservationOrderModule,
    ReservationMemoModule,
    ReservationWalkinModule,
    ModalResvCancelModule,
    ModalResvConfirmCreateModule,
    ModalModule.forRoot(),
    ModalReceiptDetailModule,
  ],
  exports: [ReservationStoreComponent],
  providers: [ValidatorService],
  entryComponents: [DialogConfirmFinishComponent],
})
export class ReservationStoreModule {}
