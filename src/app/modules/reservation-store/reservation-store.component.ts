import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { ModalResvCancelComponent } from 'app/pages/modal-resv-cancel/modal-resv-cancel.component';
import { ModalResvConfirmCreateComponent } from 'app/pages/modal-resv-confirm-create/modal-resv-confirm-create.component';
import { ModalReceiptDetailComponent } from 'app/pages/modal-receipt-detail/modal-receipt-detail.component';

import { ReservationService } from 'shared/http-services/reservation.service';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ToastService } from 'shared/logical-services/toast.service';
import { DataEmitter, EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { ValidatorService } from 'shared/utils/validator.service';

import { CALENDAR_EVENT_EMITTER } from 'shared/enums/event-emitter';
import { RES_FORM_TYPE } from 'shared/enums/reservation';
import { Walkin } from 'shared/models/walkin';
import { Reservation } from 'shared/models/reservation';
import { Mode } from 'shared/utils/mode';
import { Receipt } from 'shared/models/receipt';
import { ClientWorkingTimeWeek } from 'shared/models/client-working-week';

import { RESV_MODE } from 'shared/enums/resv-mode';
import { ClientWorkingsQuery } from 'shared/states/client_workings';
import { Subscription, forkJoin } from 'rxjs';

import * as moment from 'moment';
import * as cloneDeep from 'lodash/cloneDeep';
import * as isEqual from 'lodash/isEqual';

import { DeviceDetectorService } from 'shared/utils/device-detector';
import { DialogConfirmFinishComponent } from './dialog-confirm-finish/dialog-confirm-finish.component';
import { DIALOG_EVENT } from '../../../shared/enums/modes';

@Component({
  selector: 'app-res-store',
  templateUrl: './reservation-store.component.html',
  styleUrls: ['./reservation-store.component.scss'],
})
export class ReservationStoreComponent implements OnDestroy {
  @Output() close: EventEmitter<any> = new EventEmitter<any>();

  private _isWalkIn: boolean;
  @Input('isWalkIn')
  get isWalkIn(): boolean {
    return this._isWalkIn;
  }
  set isWalkIn(v: boolean) {
    this._isWalkIn = v;
    if (v) {
      this.formSelected = RES_FORM_TYPE.INFO;
    }
  }

  private isOpen: boolean;
  @Input('open')
  get open(): boolean {
    return this.isOpen;
  }
  set open(v: boolean) {
    this.isOpen = v;
    if (v) {
      this.formSelected = RES_FORM_TYPE.INFO;
    }
  }

  private mode: Mode;
  @Input('formMode')
  get formMode(): Mode {
    return this.mode;
  }
  set formMode(v: Mode) {
    this.mode = v;
    if (v) {
      this.isFormFocus = false;
    }
  }

  private formData: Reservation;
  @Input('resData')
  get resData(): Reservation {
    return this.formData;
  }
  set resData(v: Reservation) {
    this.formData = v;
    if (v && v.id) {
      this._orginData = cloneDeep(v);
      this.walkinData = new Walkin();
    }
  }

  private fWalkinData: Walkin;
  @Input('walkinData')
  get walkinData(): Walkin {
    return this.fWalkinData;
  }
  set walkinData(v: Walkin) {
    this.fWalkinData = v;
    if (v && v.id) {
      this._orginData = cloneDeep(v);
      this.resData = new Reservation();
    }
  }

  private orderNumber: number;
  @Input('numSelectedItems')
  get numSelectedItems(): number {
    return this.orderNumber;
  }
  set numSelectedItems(v: number) {
    this.orderNumber = v;
  }

  formSelected: string = RES_FORM_TYPE.INFO;

  tabs: any = [
    {
      type: RES_FORM_TYPE.INFO,
      text: ['予約', '情報'],
    },
    {
      type: RES_FORM_TYPE.ORDER,
      text: ['メニュー', 'コース', 'クーポン'],
      count: true,
    },
    {
      type: RES_FORM_TYPE.MEMO,
      text: ['予約', 'メモ'],
    },
  ];
  FORM_TYPE = RES_FORM_TYPE;
  validator: any = {};
  triggerValidForm: any;
  resetForm: any;
  walkinFormStatus: string;

  modalRef: BsModalRef;
  minDate: Date = new Date();

  isFormFocus: boolean = false;
  isLoading: boolean = false;
  isSubmitingWalkin: boolean = false;
  isCleaning: boolean = false;

  get isValidFromWalkin(): boolean {
    if (!this.isWalkIn) {
      return true;
    }
    return this.walkinFormStatus === 'VALID';
  }

  private _orginData: Reservation | Walkin;
  public isChangeFormValues = false;

  private routerSub: Subscription;
  receiptData: any;

  private eventSub: Subscription;

  constructor(
    private resvSv: ReservationService,
    private toastSv: ToastService,
    private eventEmitter: EventEmitterService,
    private validatorSv: ValidatorService,
    private modalSv: BsModalService,
    public activatedRoute: ActivatedRoute,
    private clientWorkingsQuery: ClientWorkingsQuery,
    public deviceDetectorSv: DeviceDetectorService,
    private router: Router,
  ) {
    this.routerSub = this.activatedRoute.queryParams.subscribe((params: Params) => {
      if (Object.keys(params).length > 0) {
        // Android
        if (
          this.deviceDetectorSv.getMobileOperatingSystem() === 'Android' &&
          params['com.squareup.pos.REQUEST_METADATA']
        ) {
          this.receiptData = JSON.parse(params['com.squareup.pos.REQUEST_METADATA']);
          this.receiptData.errorAnd = params['com.squareup.pos.ERROR_CODE'];

          if (params['com.squareup.pos.SERVER_TRANSACTION_ID']) {
            this.receiptData.transaction_id = params['com.squareup.pos.SERVER_TRANSACTION_ID'];
          }

          if (params['com.squareup.pos.CLIENT_TRANSACTION_ID']) {
            this.receiptData.client_transaction_id = params['com.squareup.pos.CLIENT_TRANSACTION_ID'];
          }
        }

        // IOS
        if (this.deviceDetectorSv.getMobileOperatingSystem() === 'iOS' && params.data) {
          const results = JSON.parse(params.data);
          this.receiptData = JSON.parse(results.state);
          this.receiptData.status = results.status;

          if (results.transaction_id) {
            this.receiptData.transaction_id = results.transaction_id;
          }

          if (results.client_transaction_id) {
            this.receiptData.client_transaction_id = results.client_transaction_id;
          }
        }

        if (params.receiptId) {
          this.receiptData = { receiptId: params.receiptId };
        }

        this.openModalReceiptDetail();
      }
    });

    this.eventSub = this.eventEmitter.caseNumber$.subscribe((res: DataEmitter) => {
      if (!res) {
        return;
      }

      if (res.type === DIALOG_EVENT.CLOSE_RESERVATION_STORE) {
        if (this.isWalkIn) {
          this.walkinData.status = res.data.status;
          return;
        }
        this.resData.status = res.data.status;
      }
    });
  }

  ngOnDestroy(): void {
    if (this.routerSub) {
      this.routerSub.unsubscribe();
    }
    if (this.eventSub) {
      this.eventSub.unsubscribe();
    }
  }

  selectForm(form: string) {
    if (!this.resData.isEnoughInfo) {
      this.formSelected = RES_FORM_TYPE.INFO;
      setTimeout(() => {
        this.triggerValidForm = { isValid: true };
      });
      return;
    }

    this.triggerValidForm = null;
    this.formSelected = form;
  }

  activeForm(form: string): boolean {
    return this.formSelected === form;
  }

  closeSlide() {
    $('.cas-header__btn').removeClass('active');
    this.resetForm = { reset: true };
    this.formSelected = null;
    this._orginData = undefined;
    this.isChangeFormValues = false;
    this.close.emit(false);
  }

  async saveRes() {
    if (this.isWalkIn) {
      this.saveWalkin(this.walkinData);
      return;
    }

    if (!this.resData.isEnoughInfo) {
      this.formSelected = RES_FORM_TYPE.INFO;
      setTimeout(() => {
        this.triggerValidForm = { isValid: true };
      });
      return;
    }

    const isValidBusinessTime = await this._checkTimeBusiness();

    if (!isValidBusinessTime) {
      return this.toastSv.error('開始時間は営業時間以内で設定してください');
    }

    if (this.resData.id) {
      this.updateReservation(this.resData.id, this.resData.formData());
      return;
    }

    if (this._isInValidEndTime()) {
      return this.toastSv.error('終了時間は現時点より未来の日時で設定してください');
    }

    this.createResv(this.resData);
  }

  private createResv(resv: Reservation) {
    if (this.formMode.isEdit || this.isWalkIn) {
      return;
    }
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-lg',
      initialState: {
        resv: cloneDeep(resv),
      },
      ignoreBackdropClick: true,
      keyboard: false,
    };
    this.closeSlide();
    this.openModalWithComponent(ModalResvConfirmCreateComponent, opts);
  }

  private updateReservation(resID: number, data: any) {
    if (this.isLoading) return;

    this.isLoading = true;

    this.resvSv.updateResv(resID, data).subscribe(
      (res: any) => {
        this.toastSv.success('予約を更新しました');
        this.closeSlide();
        this.isLoading = false;
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  eventChangeOrder(event) {
    this.numSelectedItems = event.orders.length;
  }

  startWarkin() {
    if (!this.formMode.isEdit) {
      return;
    }
    this.startResv(this.walkinData.id);
  }

  private saveWalkin(walkin: Walkin) {
    if (this.isSubmitingWalkin) {
      return;
    }
    if (this.formMode.isEdit) {
      this.updateWalkinResv(walkin.id, walkin.formData());
      return;
    }

    this.createWalkinResv(walkin.formData());
  }

  private createWalkinResv(data: any) {
    this.isSubmitingWalkin = true;
    this.validator = {};
    this.resvSv.createResvWalkIn(data).subscribe(
      (res) => {
        this.toastSv.success('ウォークイン予約を作成しました');
        this.isSubmitingWalkin = false;
        this.closeSlide();
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validator = this.validatorSv.setErrors(errors.errors);
        }
        this.toastSv.error(errors);
        this.isSubmitingWalkin = false;
      },
    );
  }

  private updateWalkinResv(id: number, data: any) {
    this.isSubmitingWalkin = true;
    this.validator = {};
    this.resvSv.updateResvWalkIn(id, data).subscribe(
      (res) => {
        this.toastSv.success('ウォークイン予約を更新しました');
        this.isSubmitingWalkin = false;
        this.closeSlide();
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validator = this.validatorSv.setErrors(errors.errors);
        }
        this.toastSv.error(errors);
        this.isSubmitingWalkin = false;
      },
    );
  }

  startResv(id: number) {
    if (this.isSubmitingWalkin) {
      return;
    }

    this.isSubmitingWalkin = true;

    this.resvSv.startReservation(id).subscribe(
      (res) => {
        this.toastSv.success('予約が開始しました');
        this.isSubmitingWalkin = false;
        this.closeSlide();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isSubmitingWalkin = false;
      },
    );
  }

  finishResv(id: number) {
    const reservation: Walkin | Reservation = this.isWalkIn ? this.walkinData : this.resData;

    if (!reservation.is_paid) {
      const opts: ModalOptions = {
        class: 'modal-dialog-centered modal-sm',
        initialState: {
          resvId: reservation.id,
        },
        ignoreBackdropClick: true,
        keyboard: false,
      };

      this.openModalWithComponent(DialogConfirmFinishComponent, opts);
    } else {
      if (this.isCleaning) {
        return;
      }

      this.isCleaning = true;
      this.resvSv.cleanUpResv(id).subscribe(
        (res) => {
          this.isCleaning = false;
          this.toastSv.success('予約を終了しました');

          this.closeSlide();

          if (this.isWalkIn) {
            this.walkinData.status = res.status;
            return;
          }
          this.resData.status = res.status;
        },
        (errors) => {
          this.toastSv.error(errors);
          this.isCleaning = false;
        },
      );
    }
  }

  formChange(event) {
    this.walkinFormStatus = event;
    this.isChangeFormValues = event;
  }

  openModalResvCancel() {
    const resvId = this.isWalkIn ? this.walkinData.id : this.resData.id;
    if (!this.formMode.isEdit || !resvId) {
      return;
    }

    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      initialState: {
        resvId: resvId,
        isWalkIn: this.isWalkIn,
      },
      ignoreBackdropClick: true,
      keyboard: false,
    };
    this.openModalWithComponent(ModalResvCancelComponent, opts);
  }

  openModalReceiptDetail() {
    let id_rsv;
    if (this.resData || this.walkinData) {
      if (!this.formMode.isEdit) {
        return;
      }

      let receipt;

      if (this.resData.id) {
        receipt = this.resData;
      }
      if (this.walkinData.id) {
        receipt = this.walkinData;
      }
      id_rsv = receipt.id;
    } else {
      id_rsv = this.receiptData.receiptId;
    }

    this.closeSlide();
    const opts = {
      with: 'client_user,tables,coupon,reservation_passcode,latest_payment',
    };

    const itemOpts = {
      with: 'courses,items,buffets',
      reservation_id: id_rsv,
      is_free: 0,
    };

    const reservationDetails = this.resvSv.getResvById(id_rsv, opts);
    const reservationItems = this.resvSv.getResvItems(itemOpts);

    forkJoin([reservationDetails, reservationItems]).subscribe(
      (results: any) => {
        const modalOpts: ModalOptions = {
          class: 'modal-dialog-centered modal-lg',
          initialState: {
            receipt: new Receipt().deserialize({ ...results[0], ...results[1].data }),
            isShowPaidResv: true,
            receiptData: this.receiptData,
          },
          ignoreBackdropClick: true,
          keyboard: false,
        };
        this.openModalWithComponent(ModalReceiptDetailComponent, modalOpts);
        this.router.navigate([`${window.location.pathname}`], { replaceUrl: true });
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  private openModalWithComponent(comp, opts: ModalOptions = {}) {
    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      switch (reason) {
        case RESV_MODE.CANCEL:
          this.closeSlide();
          break;
        case RESV_MODE.EDIT:
          this.formSelected = 'info';
          break;
      }
      this.receiptData = undefined;
      subscribe.unsubscribe();
    });

    this.modalRef = this.modalSv.show(comp, opts);
  }

  trackFormFocus(event: boolean) {
    if (event) {
      // return if refocus form
      if (this.isFormFocus) {
        return;
      }

      // handle focus form
      // this._orginData = this.walkIn ? cloneDeep(this.walkinData) : cloneDeep(this.resData);
      this.isFormFocus = event;
      return;
    }

    // handle unforcus form
    this.isFormFocus = event;

    if (this._orginData) {
      this.eventEmitter.publishData({
        type: CALENDAR_EVENT_EMITTER.DISABLE_CHOOSE_TABLE,
      });

      if (this.isWalkIn) {
        this.walkinData = cloneDeep(this._orginData);
      } else {
        this.resData = cloneDeep(this._orginData);
      }
      this.isChangeFormValues = false;
      // this._orginData = undefined;
    }
  }

  private _isInValidEndTime() {
    const dateTimeFormData = this.resData.renderDateTimeFormData();

    // dont need check time if date start in the feature
    if (moment(dateTimeFormData.start_at).isAfter(moment())) {
      return false;
    }

    const currentDuration = moment.duration(moment().format('HH:mm:ss')).asMinutes();
    return currentDuration > moment.duration(this.resData.end_time).asMinutes();
  }

  private async _checkTimeBusiness() {
    const workingTime: ClientWorkingTimeWeek = await this.clientWorkingsQuery.getWorkingTime();

    const isHoliday = workingTime.getWorkingTime(moment(this.resData.start_date, 'YYYY/MM/DD'));

    // If is holiday can not create reservation
    return !!isHoliday;
  }

  clearCoupon() {
    this.isFormFocus = true;
    this.changeFormValue();
  }

  changeFormValue(isSkipFocus?: any) {
    const data = this.isWalkIn ? this.walkinData : this.resData;
    if (isSkipFocus) {
      this.isFormFocus = true;
    }

    if (this._orginData && !isEqual(data, this._orginData) && this.isFormFocus) {
      this.isChangeFormValues = true;
    }
  }
}
