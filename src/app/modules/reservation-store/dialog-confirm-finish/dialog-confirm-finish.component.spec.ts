import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogConfirmFinishComponent } from './dialog-confirm-finish.component';

describe('DialogConfirmFinishComponent', () => {
  let component: DialogConfirmFinishComponent;
  let fixture: ComponentFixture<DialogConfirmFinishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogConfirmFinishComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogConfirmFinishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
