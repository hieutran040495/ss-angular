import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { DIALOG_EVENT } from 'shared/enums/modes';
import { ToastService } from 'shared/logical-services/toast.service';
import { ReservationService } from 'shared/http-services/reservation.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';

@Component({
  selector: 'app-dialog-confirm-finish',
  templateUrl: './dialog-confirm-finish.component.html',
  styleUrls: ['./dialog-confirm-finish.component.scss'],
})
export class DialogConfirmFinishComponent implements OnInit {
  isLoading: boolean = false;
  resvId: number;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private toastSv: ToastService,
    private resvSv: ReservationService,
    private eventEmitterSv: EventEmitterService,
  ) {}

  ngOnInit() {}

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  finished() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    this.resvSv.cleanUpResv(this.resvId).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastSv.success('予約を終了しました');
        this.eventEmitterSv.publishData({
          type: DIALOG_EVENT.CLOSE_RESERVATION_STORE,
          data: res,
        });
        this.closeDialog(DIALOG_EVENT.CLOSE_RESERVATION_STORE);
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }
}
