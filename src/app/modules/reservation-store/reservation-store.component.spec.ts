import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationStoreComponent } from './reservation-store.component';

describe('ReservationStoreComponent', () => {
  let component: ReservationStoreComponent;
  let fixture: ComponentFixture<ReservationStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReservationStoreComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
