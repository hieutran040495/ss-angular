import { Component, OnInit, forwardRef, Input, AfterViewInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, FormControl, Validator, NG_VALIDATORS } from '@angular/forms';

const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  useExisting: forwardRef(() => InputContenteditableComponent),
  provide: NG_VALUE_ACCESSOR,
  multi: true,
};

const CUSTOM_NG_VALIDATORS = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => InputContenteditableComponent),
  multi: true,
};

@Component({
  selector: 'app-input-contenteditable',
  templateUrl: './input-contenteditable.component.html',
  styleUrls: ['./input-contenteditable.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR, CUSTOM_NG_VALIDATORS],
})
export class InputContenteditableComponent implements OnInit, AfterViewInit, ControlValueAccessor, Validator {
  @Input('required') public readonly required: boolean;
  @Input('ngClass') public readonly ngClass: string;
  @Input('placeholder') public readonly placeholder: string;
  @Input('name') public readonly name: string;
  @Input('maxlength') public readonly maxlength: number = 255;

  private _value: string;
  public get value(): string {
    return this._value;
  }
  public set value(v: string) {
    if (v !== this._value) {
      this._value = v;
      this._onChangeCallback(v);
    }
  }

  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;

  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {}

  public onInput(value: string) {
    this.value = value;
  }

  onTouched() {
    this._onTouchedCallback();
  }

  writeValue(value: string) {
    this.value = value;
  }

  registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  validate(c: FormControl) {
    if (this.required !== undefined && !this.value) {
      return {
        required: true,
      };
    }

    if (this.maxlength && this.value.length > this.maxlength) {
      return {
        maxlength: true,
      };
    }

    return null;
  }
}
