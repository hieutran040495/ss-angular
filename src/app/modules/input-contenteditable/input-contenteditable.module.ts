import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputContenteditableComponent } from './input-contenteditable.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [InputContenteditableComponent],
  exports: [InputContenteditableComponent],
})
export class InputContenteditableModule {}
