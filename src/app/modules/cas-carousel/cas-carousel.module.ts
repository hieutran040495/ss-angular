import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasCarouselComponent } from './cas-carousel.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalConfirmRemovePageModule } from 'app/pages/modal-confirm-remove-page/modal-confirm-remove-page.module';

@NgModule({
  declarations: [CasCarouselComponent],
  imports: [CommonModule, ModalModule.forRoot(), ModalConfirmRemovePageModule],
  exports: [CasCarouselComponent],
})
export class CasCarouselModule {}
