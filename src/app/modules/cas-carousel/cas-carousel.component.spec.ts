import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasCarouselComponent } from './cas-carousel.component';

describe('CasCarouselComponent', () => {
  let component: CasCarouselComponent;
  let fixture: ComponentFixture<CasCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasCarouselComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
