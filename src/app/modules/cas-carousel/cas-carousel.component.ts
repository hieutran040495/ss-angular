import { Component, Input, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { TopImage } from 'shared/models/top-image';
import { Subject, Subscription } from 'rxjs';
import { KONVAJS_TYPE, ORDER, SELF_ORDER } from 'shared/enums/event-emitter';
import { RichMenu } from 'shared/models/rich-menu';
import { STYLE_SETTING_DEFAULT } from 'shared/constants/style-config-default';
import { StyleOpts } from 'shared/interfaces/style-setting';
import { Slide } from 'shared/models/slide';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalConfirmRemovePageComponent } from 'app/pages/modal-confirm-remove-page/modal-confirm-remove-page.component';

@Component({
  selector: 'app-cas-carousel',
  templateUrl: './cas-carousel.component.html',
  styleUrls: ['./cas-carousel.component.scss'],
})
export class CasCarouselComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('CasCarouselScroll') CasCarouselScroll: ElementRef;
  private _items: any[];
  @Input('items')
  get items(): any[] {
    return this._items || [];
  }
  set items(v: any[]) {
    this._items = v;
  }

  private _defaultWidth: number;
  @Input('defaultWidth')
  get defaultWidth(): number {
    return this._defaultWidth || 100;
  }
  set defaultWidth(v: number) {
    this._defaultWidth = v;
    if (v) {
      this._initSlides();
      this.defaultStyle.width = `${this.defaultWidth}px`;
    }
  }

  private _isTopScreen: boolean;
  @Input('isTopScreen')
  get isTopScreen(): boolean {
    return this._isTopScreen;
  }
  set isTopScreen(v: boolean) {
    this._isTopScreen = v;
    if (v) {
      this._initSlides();
    }
  }

  @Input('pattern') pattern: number;
  @Input('onEventEmitter') onEventEmitter: Subject<any>;
  @Input('maxItems') maxItems: number = 10;
  @Input('isDefaultSlide') isDefaultSlide: boolean = false;
  @Input('defaultImage') defaultImage: string;
  @Input('isRemove') isRemove: boolean = true;

  slides: Slide[];
  defaultActive: boolean = true;
  defaultStyle: any = {
    'background-image': `url(${this.defaultImage})`,
    width: `${this.defaultWidth}px`,
  };
  private _subEvent: Subscription;
  private _modalRef: BsModalRef;

  get disabled(): boolean {
    return !this.CasCarouselScroll.nativeElement.scrollLeft;
  }
  constructor(private modalSv: BsModalService) {}

  ngOnInit() {
    this._initSlides();
  }

  ngAfterViewInit() {
    this.casCarouselEventEmitter();
  }

  ngOnDestroy() {
    if (this._subEvent) {
      this._subEvent.unsubscribe();
    }
  }

  casCarouselEventEmitter() {
    this._subEvent = this.onEventEmitter.subscribe((res) => {
      if (!res || !res.type) {
        return;
      }

      switch (res.type) {
        case ORDER.CHANGE_TOP_IMAGE:
          this.uploadImage(res.data);
          break;
        case ORDER.SAVE_TOP_SCREEN:
          this.onEventEmitter.next({
            type: ORDER.TRANSFER_TOP_SCREEN_DATA,
            data: this.items,
          });
          break;
        case ORDER.CHANGE_SETTING_DISPLAY:
          this._updateBgColorItems(res.data);
          break;
        case SELF_ORDER.CHANGE_SETTING_DISPLAY:
          this._updateBgColorSlide(res.data);
          break;
        case SELF_ORDER.CHANGE_IMAGE_RICH_MENU:
          this._updateBgImageSlide(res.data);
          break;
        case ORDER.REMOVE_TOP_IMAGE:
          this._updateBgImageSlide('');
          this.uploadImage(res.data);
          break;
        case KONVAJS_TYPE.REMOVE_MAIN_IMAGE:
          this._updateBgImageSlide('');
          break;
        default:
          break;
      }
    });
  }

  private _initSlides() {
    this.slides = [];

    if (!this.isTopScreen) {
      return this._initSelfOrder();
    }

    this.items.forEach((topImage, index) => {
      const slide = new Slide().deserialize({
        image: topImage,
        ...this._renderStyleSlide(topImage, index),
      });

      this.slides.push(slide);
    });
  }

  private _initSelfOrder() {
    if (!this.items.length) {
      return this._initNewPageRichMenu(this._renderStyleSlide(), true);
    }

    this.items.forEach((item, index) => {
      const slide: Slide = new Slide().deserialize({
        richMenu: item,
        ...this._renderStyleSlide(item, index),
      });
      this.slides.push(slide);
    });
  }

  onSelectDefault() {
    this.removeActiveSlide();
    this.defaultActive = true;
  }

  removeActiveSlide() {
    this.slides.forEach((item) => (item.active = false));
  }

  onSelectSlide(slide: Slide) {
    this.removeActiveSlide();
    this.defaultActive = false;
    slide.active = true;

    if (!this.isTopScreen) {
      return this.onEventEmitter.next({
        type: SELF_ORDER.CHANGE_SLIDE,
        data: {
          page: slide.richMenu.page,
          isNew: false,
        },
      });
    }

    this.onEventEmitter.next({
      type: ORDER.CHANGE_TOP_SCREEN,
      data: slide,
    });
  }

  getActiveIndex(): number {
    return this.slides.findIndex((item) => item.active);
  }

  createSlide() {
    if (this.items.length === this.maxItems) {
      return;
    }

    this.removeActiveSlide();

    this._initNewPageRichMenu(this._renderStyleSlide());
    setTimeout(() => {
      this.scrollNext();
    }, 0);
  }

  private _renderStyleSlide(item?: any, index?: number) {
    if (!item) {
      return {
        style: {
          width: `${this.defaultWidth}px`,
          backgroundColor: `${STYLE_SETTING_DEFAULT.bg_color}`,
        },
        caption: `${this.items.length + 1}ページ`,
        active: true,
        isNew: true,
      };
    }

    let caption = `${index + 1}ページ`;
    if (this.isTopScreen) {
      caption = index === 1 ? `事前予約あり` : `事前予約なし`;
    }

    return {
      style: {
        backgroundImage: `url(${item.image_url})`,
        backgroundColor: `${item.bg_color}`,
        width: `${this.defaultWidth}px`,
      },
      caption: caption,
      active: index === 0 ? true : false,
    };
  }

  private _initNewPageRichMenu(data: any, isNotEmit?: boolean) {
    const richMenu = new RichMenu().deserialize({
      page: this.slides.length + 1,
      pattern: this.pattern,
      ...STYLE_SETTING_DEFAULT,
    });

    const slide = new Slide().deserialize({
      richMenu: richMenu,
      ...data,
    });
    this.items.push(richMenu);

    this.slides.push(slide);

    if (!isNotEmit) {
      this.onEventEmitter.next({
        type: SELF_ORDER.CHANGE_SLIDE,
        data: {
          page: richMenu.page,
          isNew: true,
        },
      });
    }
  }

  private _openModalWithComponent(comp, opts: ModalOptions, data?: any) {
    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();
      if (reason === SELF_ORDER.ACCEPT_REMOVE_SLIDE) {
        this._removeSlide(data.slide, data.index);
      }
    });

    this._modalRef = this.modalSv.show(comp, opts);
  }

  openModalConfirmRemove(slide: Slide, index: number) {
    const data = {
      slide: slide,
      index: index,
    };
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
    };
    this._openModalWithComponent(ModalConfirmRemovePageComponent, opts, data);
  }

  private _removeSlide(slide: Slide, index: number) {
    this.onEventEmitter.next({
      type: SELF_ORDER.ACCEPT_REMOVE_SLIDE,
      data: slide.richMenu.page,
    });

    const newItems = this.items;
    newItems.splice(index, 1);
    this.items = newItems.map((res, idx) => {
      res.page = idx + 1;
      return res;
    });

    const newSlide = this.slides;
    newSlide.splice(index, 1);
    this.slides = newSlide.map((res: Slide, idx) => {
      res.richMenu.page = idx + 1;
      return res;
    });

    this.removeActiveSlide();
    this.slides[0].active = true;
  }

  scrollNext() {
    this.CasCarouselScroll.nativeElement.scrollTo({
      left: this.CasCarouselScroll.nativeElement.scrollLeft + this.defaultWidth,
    });
  }

  scrollPrev() {
    this.CasCarouselScroll.nativeElement.scrollTo({
      left: this.CasCarouselScroll.nativeElement.scrollLeft - this.defaultWidth,
    });
  }

  uploadImage(image: TopImage) {
    const activeIndex = this.getActiveIndex();
    this.slides[activeIndex].image = new TopImage().deserialize(image);
    this.items[activeIndex].image = image;
    this.slides[activeIndex].style.backgroundImage = `url(${image.image_url})`;
  }

  private _updateBgColorItems(data: StyleOpts) {
    const activeIndex = this.getActiveIndex();
    this.items[activeIndex].deserialize(data);
    this.slides[activeIndex].image.bg_color = data.bg_color;
    this.slides[activeIndex].image.text_color = data.text_color;
    this.slides[activeIndex].style.backgroundColor = data.bg_color;
  }

  private _updateBgColorSlide(data: StyleOpts) {
    const activeIndex = this.getActiveIndex();
    this.slides[activeIndex].richMenu.bg_color = data.bg_color;
    this.slides[activeIndex].style.backgroundColor = data.bg_color;
  }

  private _updateBgImageSlide(image_url: string) {
    const activeIndex = this.getActiveIndex();
    if (!this.isTopScreen) {
      this.slides[activeIndex].richMenu.image_url = image_url;
    }
    this.slides[activeIndex].style.backgroundImage = `url(${image_url})`;
  }

  isShowRemove(index: number): boolean {
    return this.isRemove && index > 0;
  }
}
