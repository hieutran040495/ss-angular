import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { Image } from 'shared/models/image';
import { DIALOG_EVENT } from 'shared/enums/modes';

import { ToastService } from 'shared/logical-services/toast.service';
import { ClientService } from 'shared/http-services/client.service';

@Component({
  selector: 'app-dialog-delete',
  templateUrl: './dialog-delete.component.html',
  styleUrls: ['./dialog-delete.component.scss'],
})
export class DialogDeleteComponent implements OnInit {
  image: Image = new Image();

  isLoading: boolean = false;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private toastSv: ToastService,
    private clientSv: ClientService,
  ) {}

  ngOnInit() {}

  removeImage() {
    this.isLoading = true;

    this.clientSv.removeImage(this.image.id).subscribe(
      (res) => {
        this.toastSv.success('店舗画像を削除しました');
        this.closeDialog(DIALOG_EVENT.IMAGE_DELETE);
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
