import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

import { DialogDeleteComponent } from './dialog-delete.component';

@NgModule({
  declarations: [DialogDeleteComponent],
  imports: [CommonModule, CasDialogModule],
  entryComponents: [DialogDeleteComponent],
  exports: [DialogDeleteComponent],
})
export class DialogDeleteModule {}
