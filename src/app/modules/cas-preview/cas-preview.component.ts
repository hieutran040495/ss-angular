import { Component, Input, Output, OnInit, EventEmitter, OnDestroy } from '@angular/core';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

import { ClientService } from 'shared/http-services/client.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { FilePreview } from 'shared/models/file-preview';
import { Image } from 'shared/models/image';
import { DIALOG_EVENT } from 'shared/enums/modes';

import { DialogDeleteComponent } from 'app/modules/cas-preview/dialog-delete/dialog-delete.component';
import { DROPZONE_TYPE } from 'shared/enums/drop-zone';

@Component({
  selector: 'app-cas-preview',
  templateUrl: './cas-preview.component.html',
  styleUrls: ['./cas-preview.component.scss'],
})
export class CasPreviewComponent implements OnInit, OnDestroy {
  @Output() eventImage: EventEmitter<Image[]> = new EventEmitter<Image[]>();

  eventDropzone: EventEmitter<any> = new EventEmitter<any>();

  private _oldImages: any;
  @Input('oldImages')
  get oldImages(): any {
    return this._oldImages;
  }
  set oldImages(v: any) {
    this._oldImages = v;
    if (v) {
      this.fileList = v;
      this.fileMain = v[0];
    }
  }

  private _disabled: boolean;
  @Input('disabled')
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(v: boolean) {
    this._disabled = v;
  }

  fileList: Image[] | FilePreview[] | any = [];
  fileMain: FilePreview | Image;
  imgIdxSelected: number = undefined;

  isLoadImage: boolean = false;

  get uploadDisabled(): boolean {
    return this.fileList.length >= 5;
  }

  private pageSub;

  constructor(private toastSv: ToastService, private clientSv: ClientService, private bsModalSv: BsModalService) {
    this.pageSub = this.eventDropzone.subscribe((res: any) => {
      if (!res.type) {
        return;
      }

      if (res.type === DROPZONE_TYPE.UPLOAD) {
        this.uploadFile(res.data);
      }
    });
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    this.pageSub.unsubscribe();
    this.eventImage.next();
    this.eventImage.complete();
  }

  uploadFile(files: FilePreview[]) {
    if (this.fileList.length) {
      files.map((item: FilePreview) => this.fileList.push(item));
    } else {
      this.fileList = files;
    }

    if (!this.fileMain) {
      this.fileMain = files[0];
    }

    this.fileList.map((item: any, idx: number) => {
      if (!item.id) {
        item.isUpload = true;
        this.uploadImage(item.origin, idx);
      }
    });
  }

  deleteImage(idx: number) {
    if (this.fileList[idx].id) {
      const opts: ModalOptions = {
        class: 'modal-dialog-centered modal-sm',
        ignoreBackdropClick: true,
        keyboard: false,
        initialState: {
          image: this.fileList[idx],
        },
      };
      this.imgIdxSelected = idx;
      this.openModalWithComponent(DialogDeleteComponent, opts);
    } else {
      this.fileList.splice(idx, 1);
      if (idx === 0) {
        this.fileMain = this.fileList[0];
      }
    }
  }

  renderStyles(file: any) {
    const urlPath: string = file && file.url ? file.url : file && file.base64 ? file.base64 : undefined;
    if (urlPath) {
      return {
        'background-image': `url(${urlPath})`,
        'background-size': 'cover',
      };
    }
  }

  private uploadImage(file: File, idx: number) {
    const files = [];

    files.push({
      key: 'image',
      value: file,
      name: 'menuImg',
    });

    this.isLoadImage = true;

    this.clientSv.updateImage(files).subscribe(
      (res) => {
        this.toastSv.success('店舗画像を更新しました');
        this.isLoadImage = false;
        this.fileList[idx] = new Image().deserialize(res);
        this.eventImage.emit(this.fileList);
      },
      (errors) => {
        this.isLoadImage = false;
        this.fileList.splice(idx, 1);
        this.toastSv.error(errors);
      },
    );
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    const subscribe = this.bsModalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();
      if (reason === DIALOG_EVENT.IMAGE_DELETE) {
        this.fileList.splice(this.imgIdxSelected, 1);
        if (this.imgIdxSelected === 0) {
          this.fileMain = this.fileList[0];
        }
        this.eventImage.emit(this.fileList);
        this.imgIdxSelected = undefined;
      }
    });

    this.bsModalSv.show(comp, opts);
  }
}
