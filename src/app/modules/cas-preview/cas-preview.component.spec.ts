import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasPreviewComponent } from './cas-preview.component';

describe('CasPreviewComponent', () => {
  let component: CasPreviewComponent;
  let fixture: ComponentFixture<CasPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasPreviewComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
