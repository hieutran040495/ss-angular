import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';

import { CasDropzoneModule } from 'app/modules/cas-dropzone/cas-dropzone.module';
import { DialogDeleteModule } from 'app/modules/cas-preview/dialog-delete/dialog-delete.module';

import { CasPreviewComponent } from './cas-preview.component';

@NgModule({
  declarations: [CasPreviewComponent],
  imports: [CommonModule, CasDropzoneModule, ModalModule.forRoot(), DialogDeleteModule],
  exports: [CasPreviewComponent],
})
export class CasPreviewModule {}
