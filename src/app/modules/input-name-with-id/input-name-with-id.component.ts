import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, NG_VALIDATORS, FormControl } from '@angular/forms';
import { Regexs } from 'shared/constants/regex';
import { OrdinalInput } from 'shared/interfaces/ordinal-input';
import { ORDINAL_INPUT_TYPE } from 'shared/enums/ordinal-input-type';

const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = [
  {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputNameWithIdComponent),
    multi: true,
  },
  {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => InputNameWithIdComponent),
    multi: true,
  },
];

@Component({
  selector: 'app-input-name-with-id',
  templateUrl: './input-name-with-id.component.html',
  styleUrls: ['./input-name-with-id.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
})
export class InputNameWithIdComponent implements OnInit, ControlValueAccessor {
  @Input('nameMaxlength') nameMaxlength: number;
  @Input('namePlaceholder') namePlaceholder: string = '';
  @Input('nameRequiredErrorMsg') nameRequiredErrorMsg: string = '';
  @Input('validatorError') validatorError: string;
  @Input('isMenuName') isMenuName: boolean = false;

  @Input('numberMaxlength') numberMaxlength: number = 4;
  @Input('numberMinlength') numberMinlength: number = 1;
  @Input('numberPlaceholder') numberPlaceholder: string = '0000';
  @Input('numberRequiredErrorMsg') numberRequiredErrorMsg: string = '桁の数字で入力してください';
  @Input('numberUniqueErrorMsg') numberUniqueErrorMsg: string;

  private _ordinalInput: OrdinalInput;
  get ordinalInput(): OrdinalInput {
    return this._ordinalInput;
  }
  set ordinalInput(v: OrdinalInput) {
    if (v !== this.ordinalInput) {
      this._ordinalInput = v;
    }
  }

  input_name: string;
  input_number: number;
  ORDINAL_INPUT_TYPE = ORDINAL_INPUT_TYPE;
  rules = Regexs;

  /**
   * Overriden for interface ControlValueAccessor
   */
  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;

  constructor() {}

  ngOnInit() {}

  onTouched() {
    this._onTouchedCallback();
  }

  writeValue(value: any) {
    if (!value) {
      return;
    }
    this.ordinalInput = value;

    if (this.ordinalInput.name) {
      this.input_name = this.ordinalInput.name;
    }
    if (this.ordinalInput.ordinal_number) {
      this.input_number = this.ordinalInput.ordinal_number;
    }
  }

  registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  onChange(type: string) {
    this._initValue(type);
    if (!this.input_name && !this.input_number) {
      this._onTouchedCallback();
    }

    this._onChangeCallback(this.ordinalInput);
  }

  private _initValue(type: string) {
    switch (type) {
      case ORDINAL_INPUT_TYPE.ORDINAL_NUMBER:
        this.numberUniqueErrorMsg = undefined;
        this.ordinalInput.ordinal_number = this.input_number;
        break;
      case ORDINAL_INPUT_TYPE.NAME:
        this.validatorError = undefined;
        this.ordinalInput.name = this.input_name;
        break;
    }
  }

  validate(c: FormControl) {
    if (this.input_name && !this.isMenuName) {
      this.input_name = this.input_name.trim();
    }

    if (!this.input_name || !this.input_number) {
      return {
        required: {
          valid: false,
          actual: c.value,
        },
      };
    }
  }
}
