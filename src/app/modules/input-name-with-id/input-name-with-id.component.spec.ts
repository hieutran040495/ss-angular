import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputNameWithIdComponent } from './input-name-with-id.component';

describe('InputNameWithIdComponent', () => {
  let component: InputNameWithIdComponent;
  let fixture: ComponentFixture<InputNameWithIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputNameWithIdComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputNameWithIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
