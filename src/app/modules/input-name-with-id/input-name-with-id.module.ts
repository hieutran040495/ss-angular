import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { InputNameWithIdComponent } from './input-name-with-id.component';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';
import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';

@NgModule({
  declarations: [InputNameWithIdComponent],
  imports: [CommonModule, FormsModule, OnlyNumericModule, SpaceValidationModule],
  exports: [InputNameWithIdComponent],
})
export class InputNameWithIdModule {}
