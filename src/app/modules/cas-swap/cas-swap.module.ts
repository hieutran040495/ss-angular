import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasSwapComponent } from './cas-swap.component';

@NgModule({
  imports: [CommonModule],
  declarations: [CasSwapComponent],
  exports: [CasSwapComponent],
})
export class CasSwapModule {}
