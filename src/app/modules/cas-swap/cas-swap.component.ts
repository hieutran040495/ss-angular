import { Component, OnInit, Input, ViewEncapsulation, Output, EventEmitter } from '@angular/core';

import { Menu } from 'shared/models/menu';
import { Category } from 'shared/models/category';
import { Table } from 'shared/models/table';
import { SWAP_TYPE } from 'shared/enums/swap';

import { MenuService } from 'shared/http-services/menu.service';
import { CategoryService } from 'shared/http-services/category.service';
import { TableService } from 'shared/http-services/table.service';
import { ToastService } from 'shared/logical-services/toast.service';

import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-cas-swap',
  templateUrl: './cas-swap.component.html',
  styleUrls: ['./cas-swap.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CasSwapComponent implements OnInit {
  @Input() items: any;
  @Input() swapType: string;

  @Output() itemsSwapped: EventEmitter<Menu[]> = new EventEmitter<Menu[]>();

  private _isSwap: boolean;
  @Input('isSwap')
  get isSwap(): boolean {
    return this._isSwap;
  }
  set isSwap(v: boolean) {
    this._isSwap = v;
    if (!v) {
      this.swapArray = [];
    }
  }

  private _swapId: number;
  @Input('swapId')
  get swapId(): number {
    return this._swapId;
  }
  set swapId(v: number) {
    if (v) {
      this.selectItemSwap(v);
    }
    this._swapId = v;
  }

  private _isResetSwap: boolean;
  @Input('isResetSwap')
  get isResetSwap(): boolean {
    return this._isResetSwap;
  }
  set isResetSwap(v: boolean) {
    this._isResetSwap = v;
    if (v) {
      this.swapArray = [];
    }
  }

  swapArray: number[] = [];
  isSubmitting: boolean = false;

  constructor(
    private menuService: MenuService,
    private categorySv: CategoryService,
    private tableSv: TableService,
    private toastrSv: ToastService,
  ) {}

  ngOnInit() {}

  handleSwitch() {
    this.isSubmitting = true;

    const opts = {
      from: this.swapArray[0],
      to: this.swapArray[1],
    };

    switch (this.swapType) {
      case SWAP_TYPE.MENU:
        this.menusSwap(opts);
        break;
      case SWAP_TYPE.CATEGORY:
        this.categoriesSwap(opts);
        break;
      case SWAP_TYPE.TABLE:
        this.tablesSwap(opts);
        break;
      default:
        break;
    }
  }

  private menusSwap(opts: any) {
    this.menuService.swapMenu(opts).subscribe(
      (res) => {
        this.changePosition();
        this.toastrSv.success('メニューの位置を調整しました');
        this.isSubmitting = false;
      },
      (errors) => {
        this.toastrSv.error(errors);
        this.isSubmitting = false;
      },
    );
  }

  private categoriesSwap(opts: any) {
    this.categorySv.swapCategory(opts).subscribe(
      (res) => {
        this.changePosition();
        this.toastrSv.success('カテゴリーの位置を調整しました');
        this.isSubmitting = false;
      },
      (errors) => {
        this.toastrSv.error(errors);
        this.isSubmitting = false;
      },
    );
  }

  private tablesSwap(opts: any) {
    this.tableSv.swapTable(opts).subscribe(
      (res) => {
        this.changePosition();
        this.toastrSv.success('テーブルの位置を調整しました');
        this.isSubmitting = false;
      },
      (errors) => {
        this.toastrSv.error(errors);
        this.isSubmitting = false;
      },
    );
  }

  private changePosition() {
    const idxOne = this.items.findIndex((item: Menu | Category | Table) => item.id === this.swapArray[0]);
    const idxTwo = this.items.findIndex((item: Menu | Category | Table) => item.id === this.swapArray[1]);

    const swapTmp = cloneDeep(this.items[idxOne]);
    this.items[idxOne] = this.items[idxTwo];
    this.items[idxOne].swapClassActive = '';
    this.items[idxTwo] = swapTmp;
    this.items[idxTwo].swapClassActive = '';

    this.swapArray = [];
    this.itemsSwapped.emit(this.items);
  }

  selectItemSwap(id: number) {
    if (this.swapArray.length === 0) {
      this.swapArray[0] = id;
    } else {
      const idx = this.swapArray.findIndex((item: number) => item === id);
      if (idx !== -1) {
        this.swapArray.splice(idx, 1);
      } else {
        this.swapArray[1] = id;
      }
    }

    this.items.filter((item: Menu | Category | Table) => {
      item.swapClassActive = '';
    });

    this.swapArray.map((item: number) => {
      const idx = this.items.findIndex((sItem: Menu | Category | Table) => sItem.id === item);
      if (idx !== -1) {
        this.items[idx].swapClassActive = 'cas-swap__item--active';
      }
    });
  }
}
