import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasSwapComponent } from './cas-swap.component';

describe('CasSwapComponent', () => {
  let component: CasSwapComponent;
  let fixture: ComponentFixture<CasSwapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasSwapComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasSwapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
