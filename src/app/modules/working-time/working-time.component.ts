import { Component, Input, OnInit } from '@angular/core';
import { WEEK_DAY, WORKING_TYPES, WORKING_TYPE } from 'shared/constants/client-working';
import { ClientWorkingTimeWeek } from 'shared/models/client-working-week';
import * as moment from 'moment';
import { ClientWorkingTime } from 'shared/models/client-working-time';

@Component({
  selector: 'app-working-time',
  templateUrl: './working-time.component.html',
  styleUrls: ['./working-time.component.scss'],
})
export class WorkingTimeComponent implements OnInit {
  @Input() workingData: ClientWorkingTimeWeek;

  private _disabled: boolean;
  @Input('disabled')
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(v: boolean) {
    this._disabled = v;
  }

  public WEEK_DAY = WEEK_DAY;
  WORKING_TYPES = WORKING_TYPES;

  constructor() {}

  ngOnInit() {}

  updateTime(event: { start_at: string; end_at: string }, key: number, index: number) {
    this.workingData[key].durations[index].start_at = event.start_at;
    this.workingData[key].durations[index].end_at = event.end_at;

    // rehandle invalid
    if (this.workingData[key].durations.length > 0) {
      // handle invalid full day
      const start1 = moment.duration(this.workingData[key].durations[0].start_at);
      const end1 = moment.duration(this.workingData[key].durations[0].end_at);
      if (start1.asMinutes() <= 359) {
        start1.add(1, 'day');
      }
      if (end1.asMinutes() <= 359) {
        end1.add(1, 'day');
      }
      if (end1.asMinutes() <= start1.asMinutes()) {
        this.workingData[key].durations[0].error = '終了時間は開始時間よりも未来の時間で設定してください';
        return;
      }

      this.workingData[key].durations[0].error = '';

      // handle invalid break day
      if (this.workingData[key].durations.length > 1) {
        const start2 = moment.duration(this.workingData[key].durations[1].start_at);
        const end2 = moment.duration(this.workingData[key].durations[1].end_at);
        if (start2.asMinutes() <= 359) {
          start2.add(1, 'day');
        }
        if (end2.asMinutes() <= 359) {
          end2.add(1, 'day');
        }
        if (end2.asMinutes() <= start2.asMinutes()) {
          this.workingData[key].durations[1].error = '終了時間は開始時間よりも未来の時間で設定してください';
          return;
        }

        if (start2.asMinutes() <= end1.asMinutes()) {
          this.workingData[key].durations[0].error = '設定した終了時間と開始時間とが重複しています';
          this.workingData[key].durations[1].error = '設定した終了時間と開始時間とが重複しています';

          return;
        }
        this.workingData[key].durations[0].error = '';
        this.workingData[key].durations[1].error = '';
      }
    }
  }

  setWorkingTime(key: number) {
    switch (this.workingData[key].type) {
      case WORKING_TYPE.FULL_DAY:
        this.workingData[key].durations = [
          new ClientWorkingTime().deserialize({
            start_at: '06:00:00',
            end_at: '05:59:00',
          }),
        ];
        break;
      case WORKING_TYPE.BREAKING:
        this.workingData[key].durations = [
          new ClientWorkingTime().deserialize({
            start_at: '06:00:00',
            end_at: '12:00:00',
          }),
          new ClientWorkingTime().deserialize({
            start_at: '13:00:00',
            end_at: '05:59:00',
          }),
        ];
        break;
      case WORKING_TYPE.DAY_OFF:
        this.workingData[key].durations = [];
        break;
    }
  }
}
