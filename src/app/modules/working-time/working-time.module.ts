import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { WorkingTimeComponent } from './working-time.component';
import { SelectTimeModule } from 'app/forms/select-time/select-time.module';
import { OrderByModule } from 'shared/pipes/order-by/order-by.module';

@NgModule({
  declarations: [WorkingTimeComponent],
  imports: [CommonModule, FormsModule, SelectTimeModule, OrderByModule],
  exports: [WorkingTimeComponent],
})
export class WorkingTimeModule {}
