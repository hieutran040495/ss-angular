import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoOmisenoComponent } from './demo-omiseno.component';

describe('DemoOmisenoComponent', () => {
  let component: DemoOmisenoComponent;
  let fixture: ComponentFixture<DemoOmisenoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DemoOmisenoComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoOmisenoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
