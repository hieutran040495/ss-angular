import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ModalModule } from 'ngx-bootstrap/modal';

import { DemoOmisenoComponent } from './demo-omiseno.component';

const routes: Routes = [
  {
    path: '',
    component: DemoOmisenoComponent,
  },
];

@NgModule({
  declarations: [DemoOmisenoComponent],
  imports: [CommonModule, RouterModule.forChild(routes), ModalModule.forRoot()],
})
export class DemoOmisenoModule {}
