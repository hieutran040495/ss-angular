import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';

import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import _map from 'lodash/map';

@Component({
  selector: 'app-demo-omiseno',
  templateUrl: './demo-omiseno.component.html',
  styleUrls: ['./demo-omiseno.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DemoOmisenoComponent implements OnInit, OnDestroy {
  @ViewChild('OmisenoModal') OmisenoModal: TemplateRef<any>;

  modalRef: BsModalRef;
  timeCloseModal: number = 5000;
  faceIdIntervalTime: number = 7000;

  oldUserData: any[] = [
    {
      face_id: '5d79c43738386662fc164773',
      image: 'demo-2',
      name: '藤田 靖征',
    },
    {
      face_id: '5d79c5063838666305221e4d',
      image: 'demo-1',
      name: '田島 未晴',
    },
    {
      face_id: '5d79c5413838666313f94e35',
      image: 'demo-3',
      name: '熊谷 一樹',
    },
  ];
  faceIds: string[] = [];
  classDemo: string = 'demo-1';

  faceIdInterval: any;

  constructor(private echoSv: LaravelEchoService, private modalService: BsModalService) {}

  ngOnInit() {
    const validFaceIds = _map(this.oldUserData, 'face_id');

    this.echoSv.onNotificationGlobal((event: any) => {
      if (event.code === 'ai-demo') {
        this.faceIds.push(event.face_id);
      }
    });

    this.faceIdInterval = setInterval(() => {
      if (this.faceIds.length) {
        let index = -1;
        for (const v of this.faceIds) {
          const found = validFaceIds.indexOf(v);

          if (found > -1) {
            index = found;
            break;
          }
        }

        this.faceIds = []; // reset the array after then
        this.checkOpenModal(index);
      }
    }, this.faceIdIntervalTime);
  }

  ngOnDestroy(): void {
    clearInterval(this.faceIdInterval);
  }

  checkOpenModal(userIdx: number) {
    if (userIdx !== -1 && !this.modalRef) {
      this.classDemo = this.oldUserData[userIdx].image;
      this.openModal('modal-lg');
    }

    if (userIdx === -1 && !this.modalRef) {
      this.classDemo = 'demo-4';
      this.openModal('modal-md');
    }
  }

  openModal(modalClass?: string) {
    this.modalRef = this.modalService.show(this.OmisenoModal, {
      ignoreBackdropClick: true,
      keyboard: false,
      class: `modal-dialog-centered ${modalClass} omiseno-modal`,
      backdrop: false,
    });

    const closeTimeOut = setTimeout(() => {
      this.modalRef.hide();
      this.modalRef = undefined;
      clearTimeout(closeTimeOut);
    }, this.timeCloseModal);
  }
}
