import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SessionQuery, SessionService } from 'shared/states/session';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private _router: Router, private _sessionQuery: SessionQuery, private _sessionService: SessionService) {
    this._sessionService.fetchLocalStorageSession();
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    this._sessionQuery.isLoggedIn$.subscribe((isLoggedIn) => {
      if (!isLoggedIn) {
        this._router.navigate(['/auth'], { queryParams: { redirect: state.url } });
      }
    });
    return this._sessionQuery.isLoggedIn$;
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(next, state);
  }
}
