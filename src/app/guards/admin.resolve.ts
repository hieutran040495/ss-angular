import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { SessionQuery } from 'shared/states/session';
import { User } from 'shared/models/user';

@Injectable()
export class AdminResolve implements Resolve<any> {
  constructor(private sessionQuery: SessionQuery, private _router: Router) {}

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
    const curUser: User = await this.sessionQuery.getUserLoggined();

    if (!curUser.is_admin) {
      this._router.navigate(['/']);
    }
    return curUser.is_admin;
  }
}
