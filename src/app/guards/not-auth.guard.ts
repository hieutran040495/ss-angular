import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SessionQuery, SessionService } from 'shared/states/session';

@Injectable({
  providedIn: 'root',
})
export class NotAuthGuard implements CanActivate {
  constructor(private _router: Router, private _sessionQuery: SessionQuery, private _sessionService: SessionService) {
    this._sessionService.fetchLocalStorageSession();
  }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    this._sessionQuery.isLoggedIn$.subscribe((isLoggedIn) => {
      if (isLoggedIn) {
        return this._router.navigate(['/']);
      }
    });
    return true;
  }

  canActivateChild(): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate();
  }
}
