import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';

import { AppRoutingModule } from './app-routing.module';
import { JwtInterceporService } from 'shared/http-services/jwt-intercepor.service';

import { AppComponent } from './app.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { DialogMaintenanceModule } from './modules/dialog-maintenance/dialog-maintenance.module';
import { DialogUpdateVersionModule } from './modules/dialog-update-version/dialog-update-version.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    LoadingBarRouterModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
      enableHtml: true,
    }),
    ModalModule.forRoot(),
    DialogMaintenanceModule,
    DialogUpdateVersionModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceporService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
