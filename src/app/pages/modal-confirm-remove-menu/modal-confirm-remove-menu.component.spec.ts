import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfirmRemoveMenuComponent } from './modal-confirm-remove-menu.component';

describe('ModalConfirmRemoveMenuComponent', () => {
  let component: ModalConfirmRemoveMenuComponent;
  let fixture: ComponentFixture<ModalConfirmRemoveMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalConfirmRemoveMenuComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfirmRemoveMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
