import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { Menu } from 'shared/models/menu';

@Component({
  selector: 'app-modal-confirm-remove-menu',
  templateUrl: './modal-confirm-remove-menu.component.html',
  styleUrls: ['./modal-confirm-remove-menu.component.scss'],
})
export class ModalConfirmRemoveMenuComponent implements OnInit {
  menu: Menu[] = [];

  constructor(private bsModalSv: BsModalService, private bsModalRef: BsModalRef) {}

  ngOnInit() {}

  removeMenu() {
    // Do something
    this.closeModal();
  }

  closeModal(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
