import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalConfirmRemoveMenuComponent } from './modal-confirm-remove-menu.component';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

@NgModule({
  declarations: [ModalConfirmRemoveMenuComponent],
  imports: [CommonModule, FormsModule, CasDialogModule],
  exports: [ModalConfirmRemoveMenuComponent],
  entryComponents: [ModalConfirmRemoveMenuComponent],
})
export class ModalConfirmRemoveMenuModule {}
