import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  AfterViewInit,
  ViewChild,
} from '@angular/core';
import { Pagination } from 'shared/models/pagination';

import { NotifyService } from 'shared/http-services/notify.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';

import { NOTIFY_CODE } from 'shared/enums/code-notify';
import { Notify } from 'shared/models/notify';
import { PusherEvent } from 'shared/models/event';
import { Reservation } from 'shared/models/reservation';

import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { RESV_MODE_EMITTER } from 'shared/enums/event-emitter';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ClientProfileQuery } from 'shared/states/client-profile';

@Component({
  selector: 'app-notify-list-csv',
  templateUrl: './notify-list-csv.component.html',
  styleUrls: ['./notify-list-csv.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotifyListCsvComponent implements OnInit, AfterViewInit {
  @ViewChild(PerfectScrollbarComponent)
  componentScroll;

  isLoading: boolean = false;
  pagination: Pagination = new Pagination();

  notifies: Notify[] = [];
  resv: Reservation;

  private isReadNotify: boolean = false;
  isUseSmpOrder: boolean;

  @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private notifySv: NotifyService,
    private toastSv: ToastService,
    private eventEmitter: EventEmitterService,
    private echoSv: LaravelEchoService,
    private resvSv: ReservationService,
    private cd: ChangeDetectorRef,
    private clientProfileQuery: ClientProfileQuery,
  ) {}

  ngOnInit() {
    this.getListNotify();
    this.onNotification();
    this.getClientInfo();
  }

  ngAfterViewInit() {
    this.eventScroll();
  }

  eventScroll() {
    this.componentScroll.directiveRef.elementRef.nativeElement.addEventListener('ps-y-reach-end', (e) => {
      if (!this.isLoading && this.pagination.hasNextPage()) {
        this.pagination.nextPage();
        this.getListNotify();
      }
    });
  }

  private onNotification() {
    this.echoSv.onNotification((e: any) => {
      const event = new PusherEvent().deserialize(e);
      if (event.isImportResComplete && event.isReloadCalendar) {
        this.pagination.reset();
        this.getListNotify();
      }
    });
  }

  private getListNotify() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    const opts = {
      ...this.pagination.hasJSON(),
      with: 'reservation',
      order: '-created_at',
    };

    this.notifySv.getNotifyCsv(opts).subscribe(
      (res) => {
        this.notifies = this.notifies.concat(res.data);
        this.pagination.deserialize(res);
        this.isLoading = false;
        this.cd.markForCheck();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  readNotify(id: string) {
    this.isReadNotify = true;
    this.cd.markForCheck();

    this.notifySv.readNotifyCsv(id).subscribe(
      (res) => {
        const itemRead = this.notifies.findIndex((item) => item.id === id);
        this.eventEmitter.publishData({
          type: NOTIFY_CODE.READ_NOTIFY_CSV,
        });
        this.notifies[itemRead] = new Notify().deserialize(res);
        this.isReadNotify = false;
        this.cd.markForCheck();
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  getResById(resID: number) {
    if (this.isLoading) {
      return;
    }

    this.resv = new Reservation();
    this.isLoading = true;
    const opts: any = {
      with: `coupon,clientUser,tables,user,client_member,keywords,${this.isUseSmpOrder ? 'reservation_passcode' : ''}`,
    };

    this.resvSv.getResvById(resID, opts).subscribe(
      (res) => {
        this.isLoading = false;
        this.resv.deserialize(res);
        this.getResItems(resID);
      },
      (errors) => {
        this.isLoading = false;
        this.toastSv.error(errors);
      },
    );
  }

  getResItems(resId: number) {
    const opts = {
      with: 'courses,items,buffets',
      reservation_id: resId,
    };

    this.resvSv.getResvItems(opts).subscribe(
      (res) => {
        this.resv.deserialize(res.data);
        this.eventEmitter.publishData({
          type: RESV_MODE_EMITTER.EDIT,
          data: this.resv,
        });
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  private async getClientInfo() {
    const data = await this.clientProfileQuery.getClientProfile();
    this.isUseSmpOrder = data.allow_smp_order;
  }

  closeNotify(notify: Notify) {
    if (!this.isReadNotify) {
      this.closeEvent.emit(null);
      if (notify.is_import_resv_fail && notify.link) {
        window.open(notify.link, '_blank');
      } else {
        this.getResById(notify.data.reservation.id);
      }
    }
  }

  closeAll() {
    this.closeEvent.emit(null);
  }
}
