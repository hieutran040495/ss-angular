import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotifyListCsvComponent } from './notify-list-csv.component';
import { RouterModule } from '@angular/router';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { FormsModule } from '@angular/forms';
import { ModalSettingNotifyModule } from '../modal-setting-notify/modal-setting-notify.module';
import { DEFAULT_PERFECT_SCROLLBAR_CONFIG } from 'shared/constants/scroll-config';
import { PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';

@NgModule({
  declarations: [NotifyListCsvComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    CasPaginationModule.forRoot(),
    ModalSettingNotifyModule,
    PerfectScrollbarModule,
  ],
  exports: [NotifyListCsvComponent],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
  ],
})
export class NotifyListCsvModule {}
