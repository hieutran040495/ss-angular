import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifyListCsvComponent } from './notify-list-csv.component';

describe('NotifyListCsvComponent', () => {
  let component: NotifyListCsvComponent;
  let fixture: ComponentFixture<NotifyListCsvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NotifyListCsvComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotifyListCsvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
