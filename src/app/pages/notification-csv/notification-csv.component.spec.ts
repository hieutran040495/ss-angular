import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationCsvComponent } from './notification-csv.component';

describe('NotificationCsvComponent', () => {
  let component: NotificationCsvComponent;
  let fixture: ComponentFixture<NotificationCsvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NotificationCsvComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationCsvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
