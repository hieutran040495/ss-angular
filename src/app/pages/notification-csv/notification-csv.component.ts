import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Input } from '@angular/core';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { Subscription } from 'rxjs/Subscription';
import { NOTIFY_CODE } from 'shared/enums/code-notify';
import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';
import { PusherEvent } from 'shared/models/event';
import { NotifyService } from 'shared/http-services/notify.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { RESV_MODE_EMITTER } from 'shared/enums/event-emitter';

@Component({
  selector: 'app-notification-csv',
  templateUrl: './notification-csv.component.html',
  styleUrls: ['./notification-csv.component.scss'],
})
export class NotificationCsvComponent implements OnInit, OnDestroy {
  @ViewChild('NotifyRef') private notifyRefCsv: ElementRef;
  @Input() totalUnseen: number;

  private pageSub: Subscription;
  isShowNotiList: boolean = false;

  constructor(
    private eventEmitter: EventEmitterService,
    private echoSv: LaravelEchoService,
    private notifySv: NotifyService,
    private toastSv: ToastService,
  ) {}

  ngOnInit() {
    this.onNotification();
    this.pageSub = this.eventEmitter.caseNumber$.subscribe((res) => {
      if (res.type === NOTIFY_CODE.READ_NOTIFY_CSV) {
        this.getCountNotify();
        this.clearUnseen(true);
      }
    });
  }

  ngOnDestroy() {
    if (this.pageSub) {
      this.pageSub.unsubscribe();
    }
  }

  private onNotification() {
    this.echoSv.onNotification((e: any) => {
      const event = new PusherEvent().deserialize(e);
      if (event.isImportResComplete && event.isReloadCalendar) {
        this.totalUnseen = event.notifications_count + this.totalUnseen;
      }
    });
  }

  private getCountNotify() {
    this.notifySv.getNotifyStatistics().subscribe(
      (res) => {
        this.totalUnseen = res.total_unseen_crawler;
        this.toggleShowNotify();
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  private toggleShowNotify() {
    if (this.totalUnseen > 0) {
      return this.notifyRefCsv.nativeElement.classList.add('show');
    }

    if (this.totalUnseen === 0) {
      return this.notifyRefCsv.nativeElement.classList.remove('show');
    }
  }

  clearUnseen(eventClick: boolean = false) {
    this.isShowNotiList = true;

    if (!eventClick) {
      this.eventEmitter.publishData({
        type: RESV_MODE_EMITTER.CLOSE,
      });
    }

    if (!this.totalUnseen) {
      return;
    }

    this.notifySv.unSeenNotifyCsv().subscribe(
      (res) => {
        this.totalUnseen = 0;
      },
      (error) => {
        this.toastSv.error(error);
      },
    );
  }

  clickOutside($event) {
    this.isShowNotiList = false;
  }
}
