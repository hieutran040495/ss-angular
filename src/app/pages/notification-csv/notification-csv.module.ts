import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationCsvComponent } from './notification-csv.component';
import { ClickOutsideModule } from 'shared/directive/click-outside/click-outside.module';
import { NotifyListCsvModule } from '../notify-list-csv/notify-list-csv.module';

@NgModule({
  declarations: [NotificationCsvComponent],
  imports: [CommonModule, NotifyListCsvModule, ClickOutsideModule],
  exports: [NotificationCsvComponent],
})
export class NotificationCsvModule {}
