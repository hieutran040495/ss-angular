import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

import { CasTableModule } from 'app/modules/cas-table/cas-table.module';
import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { FilterUserModule } from 'app/forms/filter-user/filter-user.module';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { ModalUserDetailModule } from 'app/pages/modal-user-detail/modal-user-detail.module';
import { ModalDownloadCsvClientModule } from 'app/pages/modal-download-csv-client/modal-download-csv-client.module';

import { UserListComponent } from './user-list.component';

const routes: Routes = [
  {
    path: '',
    component: UserListComponent,
  },
];

@NgModule({
  declarations: [UserListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    CasTableModule,
    CasFilterModule,
    FilterUserModule,
    CasPaginationModule.forRoot(),
    ModalModule.forRoot(),
    ModalUserDetailModule,
    ModalDownloadCsvClientModule,
  ],
})
export class UserListModule {}
