import { Component, OnDestroy, OnInit } from '@angular/core';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

import { User } from 'shared/models/user';
import { ClientUser } from 'shared/models/client-user';
import { Pagination, PageChangedInput } from 'shared/models/pagination';

import { UserService } from 'shared/http-services/user.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { EventEmitterType, RESV_MODE_EMITTER } from 'shared/enums/event-emitter';
import { DIALOG_EVENT } from 'shared/enums/modes';

import { TableHeader } from 'app/modules/cas-table/cas-table.component';
import { ModalUserDetailComponent } from 'app/pages/modal-user-detail/modal-user-detail.component';

import * as cloneDeep from 'lodash/cloneDeep';

import { ModalDownloadCsvClientComponent } from 'app/pages/modal-download-csv-client/modal-download-csv-client.component';
import { BehaviorSubject } from 'rxjs';
import { FilterUserModel } from 'app/forms/filter-user/filter-user.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  users: User[] = [];
  selectedUser: User;
  isPagination: boolean;
  pagination: Pagination = new Pagination();
  orderParams: any = {};

  filterData = new BehaviorSubject<FilterUserModel>(new FilterUserModel());
  private _filterData: any;

  searchConf: any = {
    placeholder: '名前もしくは電話番号を入力してください',
    button: '検索',
  };

  sortItems = [
    {
      name: '顧客名',
      value: 'name',
      active: false,
    },
    {
      name: '顧客ステータス',
      value: 'has_finished_resv',
      active: false,
    },
    {
      name: '来店回数',
      value: 'finished_reservations_count',
      active: false,
    },
  ];

  tableHeader: Partial<TableHeader>[] = [
    {
      name: '顧客名',
      width: 150,
      class: 'text-center',
    },
    {
      name: `電話番号`,
      width: 150,
      class: 'text-center',
    },
    {
      name: '顧客ステータス',
      width: 150,
      class: 'text-center',
    },
    {
      name: '来店回数',
      width: 100,
      class: 'text-center',
    },
    {
      name: '詳細',
      width: 150,
      class: 'text-center',
    },
  ];
  private pageSub;

  constructor(
    private userSv: UserService,
    private toastSv: ToastService,
    private bsModalSv: BsModalService,
    private eventEmitterSv: EventEmitterService,
  ) {}

  ngOnInit() {
    this.pageSub = this.eventEmitterSv.caseNumber$.subscribe((res) => {
      if (res.type === EventEmitterType.OPEN_MODAL_USER_DETAIL) {
        this.openUserDetails(this.selectedUser);
        return;
      }
    });
    this.getListUser();
  }

  ngOnDestroy(): void {
    this.filterData.next(null);
    this.filterData.complete();
    this._filterData = undefined;
    this.pageSub.unsubscribe();
  }

  getListUser() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;

    this.users = [];
    const opts = {
      ...this.pagination.hasJSON(),
      ...this.orderParams,
      ...this._filterData,
      with: 'finished_reservations_count',
    };

    this.eventEmitterSv.publishData({
      type: EventEmitterType.SEARCH_CLIENT_USER,
      data: opts.phone_or_name,
    });

    this.userSv.getUsers(opts).subscribe(
      (res) => {
        this.pagination.deserialize(res);
        this.users = res.data;
        this.isLoading = false;
        this.isPagination = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this.isPagination = false;
      },
    );
  }

  filterUser() {
    this.pagination.reset();
    const filterSubscribe = this.filterData.subscribe((res: FilterUserModel) => {
      this._filterData = res.toJSON();
      this.getListUser();
    });
    filterSubscribe.unsubscribe();
  }

  orderEvent(event) {
    this.orderParams = event;
    this.filterUser();
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPagination) {
      return;
    }
    this.isPagination = true;

    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.getListUser();
  }

  openUserDetails(user: User) {
    this.selectedUser = user;
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-lg',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        user: cloneDeep(user),
      },
    };

    this.openModalWithComponent(ModalUserDetailComponent, opts);
  }

  openModalDownloadCSV() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered',
      ignoreBackdropClick: true,
      keyboard: false,
    };

    this.openModalWithComponent(ModalDownloadCsvClientComponent, opts);
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    const subscribe = this.bsModalSv.onHidden.subscribe((reason: string) => {
      if (reason === DIALOG_EVENT.CLOSE) {
        subscribe.unsubscribe();
      }

      switch (reason) {
        case DIALOG_EVENT.OPEN_USER_DETAIL:
          this.openUserDetails(this.selectedUser);
          break;
        case DIALOG_EVENT.CREATE_RESERVATION:
          this.eventEmitterSv.publishData({
            type: RESV_MODE_EMITTER.CREATE,
            data: {
              client_user: new ClientUser().deserialize(this.selectedUser),
            },
          });
          break;
        case DIALOG_EVENT.USER_LIST_RELOAD:
          this.pagination.reset();
          this.getListUser();
          break;
      }
    });

    this.bsModalSv.show(comp, opts);
  }
}
