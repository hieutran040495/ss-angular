import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  AfterViewInit,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { Pagination } from 'shared/models/pagination';

import { NotifyService } from 'shared/http-services/notify.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

import { NOTIFY_CODE } from 'shared/enums/code-notify';
import { Notify } from 'shared/models/notify';
import { PusherEvent } from 'shared/models/event';
import { RESV_MODE_EMITTER } from 'shared/enums/event-emitter';
import { ReservationService } from 'shared/http-services/reservation.service';
import { Reservation } from 'shared/models/reservation';

import { ModalSettingNotifyComponent } from '../modal-setting-notify/modal-setting-notify.component';

import * as moment from 'moment';
import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { ClientProfileQuery } from 'shared/states/client-profile';

@Component({
  selector: 'app-notify-list',
  templateUrl: './notify-list.component.html',
  styleUrls: ['./notify-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotifyListComponent implements OnInit, AfterViewInit {
  @ViewChild(PerfectScrollbarComponent)
  componentScroll;

  isLoading: boolean = false;
  pagination: Pagination = new Pagination();

  notifies: Notify[] = [];
  resv: Reservation;
  isUseSmpOrder: boolean;

  @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private notifySv: NotifyService,
    private toastSv: ToastService,
    private eventEmitter: EventEmitterService,
    private echoSv: LaravelEchoService,
    private cd: ChangeDetectorRef,
    private resvSv: ReservationService,
    private router: Router,
    private modalSv: BsModalService,
    private clientProfileQuery: ClientProfileQuery,
  ) {}

  ngOnInit() {
    this.getListNotify();
    this.onNotification();
    this.getClientInfo();
  }

  ngAfterViewInit() {
    this.eventScroll();
  }

  eventScroll() {
    this.componentScroll.directiveRef.elementRef.nativeElement.addEventListener('ps-y-reach-end', (e) => {
      if (!this.isLoading && this.pagination.hasNextPage()) {
        this.pagination.nextPage();
        this.getListNotify();
      }
    });
  }

  private onNotification() {
    this.echoSv.onNotification((e: any) => {
      const event = new PusherEvent().deserialize(e);
      if (event.hasNotification && event.isReloadNotify && !event.isImportResComplete) {
        this._findNotify(event.id);
      }
    });
  }

  private _findNotify(notifyId: number) {
    const opts = {
      with: 'reservation',
    };

    this.notifySv.fetchNotifyById(notifyId, opts).subscribe(
      (res) => {
        this.notifies.unshift(res);
        this.cd.markForCheck();
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  private getListNotify() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    const opts = {
      ...this.pagination.hasJSON(),
      with: 'reservation',
    };

    this.notifySv.getNotify(opts).subscribe(
      (res) => {
        this.notifies = this.notifies.concat(res.data);
        this.pagination.deserialize(res);
        this.isLoading = false;
        this.cd.markForCheck();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  readDetail(notify: Notify) {
    if (!notify.id) {
      return;
    }

    if (!notify.read_at) {
      notify.read_at = moment()
        .local()
        .toISOString();
      this.readNotify(notify);
    } else {
      this.eventEmitter.publishData({
        type: NOTIFY_CODE.READ_NOTIFY,
      });
    }

    if (notify.reservation && notify.reservation.id) {
      this.getResById(notify.reservation.id);
    } else {
      if (notify.is_import_resv_fail) {
        window.open(notify.link, '_bank');
      } else {
        this.router.navigate([notify.link]);
      }
    }
  }

  getResById(resID: number) {
    if (this.isLoading) {
      return;
    }

    this.resv = new Reservation();
    this.isLoading = true;
    const opts: any = {
      with: `coupon,clientUser,tables,user,client_member,keywords,${this.isUseSmpOrder ? 'reservation_passcode' : ''}`,
    };

    this.resvSv.getResvById(resID, opts).subscribe(
      (res) => {
        this.isLoading = false;
        this.resv.deserialize(res);
        this.getResItems(resID);
      },
      (errors) => {
        this.isLoading = false;
        this.toastSv.error(errors);
      },
    );
  }

  getResItems(resId: number) {
    const opts = {
      with: 'courses,items,buffets',
      reservation_id: resId,
    };

    this.resvSv.getResvItems(opts).subscribe(
      (res) => {
        this.resv.deserialize(res.data);
        this.eventEmitter.publishData({
          type: RESV_MODE_EMITTER.EDIT,
          data: this.resv,
        });
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  private async getClientInfo() {
    const data = await this.clientProfileQuery.getClientProfile();
    this.isUseSmpOrder = data.allow_smp_order;
  }

  private readNotify(notify: Notify) {
    this.notifySv.readNotify(notify.id).subscribe(
      (res) => {
        this.eventEmitter.publishData({
          type: NOTIFY_CODE.READ_NOTIFY,
        });
        this.cd.markForCheck();
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  openModalSettingNotify() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      keyboard: false,
      ignoreBackdropClick: true,
    };

    this.openModalWithComponent(ModalSettingNotifyComponent, opts);
    this.closeNotify();
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();
    });

    this.modalSv.show(comp, opts);
  }

  closeNotify() {
    this.closeEvent.emit(null);
  }

  preventDefaultEvent($event) {
    $event.stopPropagation();
  }
}
