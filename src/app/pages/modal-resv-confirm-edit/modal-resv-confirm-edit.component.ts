import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal-resv-confirm-edit',
  templateUrl: './modal-resv-confirm-edit.component.html',
  styleUrls: ['./modal-resv-confirm-edit.component.scss'],
})
export class ModalResvConfirmEditComponent implements OnInit {
  isLoading: boolean = false;
  resvId: number;
  tableIds: number[];

  constructor(
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private resvSv: ReservationService,
    private toastSv: ToastService,
    private router: Router,
  ) {}

  ngOnInit() {}

  onConfirmEdit() {
    if (this.tableIds && this.tableIds.length !== 0) {
      this.updateTable();
      return;
    }

    this.updatedResv();
  }

  updatedResv() {
    this.closeModal('updateResv');
  }

  updateTable() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    const data: any = {
      table_ids: this.tableIds,
    };

    this.resvSv.updateTable(this.resvId, data).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastSv.success('予約を更新しました');
        this.closeModal();
        this.router.navigate(['/reservations']);
      },
      (errors) => {
        this.isLoading = false;
        this.toastSv.error(errors);
      },
    );
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
