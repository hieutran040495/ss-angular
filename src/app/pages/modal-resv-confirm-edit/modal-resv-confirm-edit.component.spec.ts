import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalResvConfirmEditComponent } from './modal-resv-confirm-edit.component';

describe('ModalResvConfirmEditComponent', () => {
  let component: ModalResvConfirmEditComponent;
  let fixture: ComponentFixture<ModalResvConfirmEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalResvConfirmEditComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalResvConfirmEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
