import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalResvConfirmEditComponent } from './modal-resv-confirm-edit.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  imports: [CommonModule, FormsModule, ModalModule.forRoot()],
  entryComponents: [ModalResvConfirmEditComponent],
  declarations: [ModalResvConfirmEditComponent],
  exports: [ModalResvConfirmEditComponent],
})
export class ModalResvConfirmEditModule {}
