import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalUpdateTableComponent } from './modal-update-table.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ModalUpdateTableComponent],
  entryComponents: [ModalUpdateTableComponent],
  exports: [ModalUpdateTableComponent],
})
export class ModalUpdateTableModule {}
