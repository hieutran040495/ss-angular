import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';

import * as uniq from 'lodash/uniq';

@Component({
  selector: 'app-modal-update-table',
  templateUrl: './modal-update-table.component.html',
  styleUrls: ['./modal-update-table.component.scss'],
})
export class ModalUpdateTableComponent implements OnInit {
  public isLoading: boolean = false;
  public resvId: number;
  public tableIds: number[];
  public revertFunc: Function;

  constructor(
    private bsModalRef: BsModalRef,
    private _reservationService: ReservationService,
    private toastSv: ToastService,
  ) {}

  ngOnInit() {}

  updateTablesForResv() {
    this.isLoading = true;
    const data = {
      table_ids: uniq(this.tableIds),
    };
    this._reservationService.updateTable(this.resvId, data).subscribe(
      () => {
        this.isLoading = false;
        this.toastSv.success('予約を更新しました');
        this.closeModal(false);
      },
      (errors) => {
        this.toastSv.error(errors);
        this.closeModal();
      },
    );
  }

  closeModal(isRevert: boolean = true) {
    if (isRevert) {
      this.revertFunc();
    }
    this.bsModalRef.hide();
  }
}
