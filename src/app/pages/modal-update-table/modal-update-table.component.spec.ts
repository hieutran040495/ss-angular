import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalUpdateTableComponent } from './modal-update-table.component';

describe('ModalUpdateTableComponent', () => {
  let component: ModalUpdateTableComponent;
  let fixture: ComponentFixture<ModalUpdateTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalUpdateTableComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalUpdateTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
