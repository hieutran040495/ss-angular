import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ValidatorService } from 'shared/utils/validator.service';

import { ManageAccountCeComponent } from './manage-account-ce.component';

import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { EmailValidatorModule } from 'shared/directive/email-validator/email-validator.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

@NgModule({
  declarations: [ManageAccountCeComponent],
  imports: [CommonModule, FormsModule, SpaceValidationModule, EmailValidatorModule, CasDialogModule],
  providers: [ValidatorService],
  entryComponents: [ManageAccountCeComponent],
  exports: [ManageAccountCeComponent],
})
export class ManageAccountCeModule {}
