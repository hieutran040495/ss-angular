import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';

import { MemberServices } from 'shared/http-services/member.services';

@Injectable({
  providedIn: 'root',
})
export class ManageAccountCeResolve implements Resolve<any> {
  constructor(private _router: Router, private memberSv: MemberServices) {}

  resolve(activeRoute: ActivatedRouteSnapshot) {
    if (activeRoute.params.accountId && isNaN(Number(activeRoute.params.accountId))) {
      return this._router.navigate(['/users']);
    }
    return this.memberSv.getMemberById(activeRoute.params.accountId);
  }
}
