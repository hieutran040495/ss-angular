import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ToastService } from 'shared/logical-services/toast.service';
import { MemberServices } from 'shared/http-services/member.services';
import { ValidatorService } from 'shared/utils/validator.service';

import { User } from 'shared/models/user';
import { Mode } from 'shared/utils/mode';
import { DIALOG_EVENT } from 'shared/enums/modes';

interface MemberValidation {
  name: string | undefined;
  email: string | undefined;
  role_name: string | undefined;
}

@Component({
  selector: 'app-manage-account-ce',
  templateUrl: './manage-account-ce.component.html',
  styleUrls: ['./manage-account-ce.component.scss'],
})
export class ManageAccountCeComponent implements OnInit {
  user: User = new User();
  cMode: Mode = new Mode();

  isLoading = false;

  validation: MemberValidation = {
    name: undefined,
    email: undefined,
    role_name: undefined,
  };

  get title(): string {
    return this.user.id ? 'アカウント編集' : 'アカウント新規作成';
  }

  get submitText(): string {
    return this.user.id ? '編集する' : '作成する';
  }

  constructor(
    private toastrSv: ToastService,
    private memberSv: MemberServices,
    private validationSv: ValidatorService,
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.user.id) {
      this.cMode.setEdit();
    }
  }

  submitAccount(): void {
    if (this.user.id) {
      this.updateAccount();
    } else {
      this.createAccount();
    }
  }

  private createAccount() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    this._cd.detectChanges();

    this.memberSv.createMember(this.user.memberFormData()).subscribe(
      (res) => {
        this.toastrSv.success('アカウントを作成しました');
        this.closeModal(DIALOG_EVENT.ACCOUNT_RELOAD);
        this._cd.detectChanges();
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validationSv.setErrors(errors.errors);
        }
        this.toastrSv.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  private updateAccount() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    this._cd.detectChanges();

    this.memberSv.editMember(this.user.memberFormData()).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastrSv.success('アカウントを編集しました');
        this.closeModal(DIALOG_EVENT.ACCOUNT_RELOAD);
        this._cd.detectChanges();
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validationSv.setErrors(errors.errors);
        }
        this.toastrSv.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  resetValidation(name: string): void {
    this.validation[name] = undefined;
    this._cd.detectChanges();
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
