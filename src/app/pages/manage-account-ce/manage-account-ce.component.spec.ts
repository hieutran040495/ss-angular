import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageAccountCeComponent } from './manage-account-ce.component';

describe('ManageAccountCeComponent', () => {
  let component: ManageAccountCeComponent;
  let fixture: ComponentFixture<ManageAccountCeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManageAccountCeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageAccountCeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
