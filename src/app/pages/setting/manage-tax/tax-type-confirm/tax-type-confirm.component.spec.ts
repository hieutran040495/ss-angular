import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxTypeConfirmComponent } from './tax-type-confirm.component';

describe('TaxTypeConfirmComponent', () => {
  let component: TaxTypeConfirmComponent;
  let fixture: ComponentFixture<TaxTypeConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxTypeConfirmComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxTypeConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
