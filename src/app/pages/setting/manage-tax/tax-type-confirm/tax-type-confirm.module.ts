import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

import { TaxService } from 'shared/http-services/tax.service';

import { TaxTypeConfirmComponent } from './tax-type-confirm.component';

@NgModule({
  declarations: [TaxTypeConfirmComponent],
  imports: [CommonModule, CasDialogModule],
  providers: [TaxService],
  entryComponents: [TaxTypeConfirmComponent],
  exports: [TaxTypeConfirmComponent],
})
export class TaxTypeConfirmModule {}
