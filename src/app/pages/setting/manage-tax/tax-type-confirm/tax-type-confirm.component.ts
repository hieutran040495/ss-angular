import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

import { ToastService } from 'shared/logical-services/toast.service';
import { TaxService } from 'shared/http-services/tax.service';

@Component({
  selector: 'app-tax-type-confirm',
  templateUrl: './tax-type-confirm.component.html',
  styleUrls: ['./tax-type-confirm.component.scss'],
})
export class TaxTypeConfirmComponent implements OnInit {
  taxType: string;
  isLoading: boolean;

  constructor(private bsModalRef: BsModalRef, private toastSv: ToastService, private taxSv: TaxService) {}

  ngOnInit() {}

  changeTaxType() {
    if (this.isLoading) return;

    this.isLoading = true;

    this.taxSv
      .updateTaxType({
        tax_type: this.taxType,
      })
      .subscribe(
        (res: any) => {
          this.toastSv.success('課税方法を更新しました');
          this.isLoading = false;
          this.closeModal();
        },
        (errors: any) => {
          this.isLoading = false;
          this.toastSv.error(errors);
          this.closeModal();
        },
      );
  }

  closeModal() {
    this.bsModalRef.hide();
  }
}
