import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TAX_ROUND } from 'shared/enums/tax';

@Component({
  selector: 'app-tax-round',
  templateUrl: './tax-round.component.html',
  styleUrls: ['./tax-round.component.scss'],
})
export class TaxRoundComponent implements OnInit {
  roundType: string;

  TAX_ROUND = TAX_ROUND;
  isSubmiting: boolean = false;

  constructor(private modalSv: BsModalService, private bsModalRef: BsModalRef) {}

  ngOnInit() {}

  confirmSettingRound() {
    this.closeModal(this.roundType);
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
