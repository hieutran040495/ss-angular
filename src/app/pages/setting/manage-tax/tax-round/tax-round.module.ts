import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TaxRoundComponent } from './tax-round.component';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

@NgModule({
  declarations: [TaxRoundComponent],
  imports: [CommonModule, FormsModule, CasDialogModule],
  entryComponents: [TaxRoundComponent],
  exports: [TaxRoundComponent],
})
export class TaxRoundModule {}
