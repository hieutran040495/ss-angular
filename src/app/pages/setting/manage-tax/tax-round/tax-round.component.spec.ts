import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxRoundComponent } from './tax-round.component';

describe('TaxRoundComponent', () => {
  let component: TaxRoundComponent;
  let fixture: ComponentFixture<TaxRoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxRoundComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxRoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
