import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

import { TAX_APPLY_TYPE } from 'shared/enums/tax';

import { TaxTypeConfirmComponent } from '../tax-type-confirm/tax-type-confirm.component';

@Component({
  selector: 'app-tax-type',
  templateUrl: './tax-type.component.html',
  styleUrls: ['./tax-type.component.scss'],
})
export class TaxTypeComponent implements OnInit {
  isLoading: boolean = false;
  taxType: string;

  TAX_APPLY_TYPE = TAX_APPLY_TYPE;

  constructor(private bsModalRef: BsModalRef, private bsModalSv: BsModalService) {}

  ngOnInit() {}

  confirmTaxType() {
    const config: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        taxType: this.taxType,
      },
    };

    this.bsModalRef.hide();
    this.bsModalSv.show(TaxTypeConfirmComponent, config);
  }

  closeModal() {
    this.bsModalRef.hide();
  }
}
