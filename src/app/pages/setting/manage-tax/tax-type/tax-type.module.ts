import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { TaxTypeConfirmModule } from '../tax-type-confirm/tax-type-confirm.module';

import { TaxTypeComponent } from './tax-type.component';

@NgModule({
  declarations: [TaxTypeComponent],
  imports: [CommonModule, FormsModule, CasDialogModule, TaxTypeConfirmModule],
  entryComponents: [TaxTypeComponent],
  exports: [TaxTypeComponent],
})
export class TaxTypeModule {}
