import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { ModalModule } from 'ngx-bootstrap/modal';

import { CasTableModule } from 'app/modules/cas-table/cas-table.module';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { TaxCeModule } from './tax-ce/tax-ce.module';
import { TaxTypeModule } from './tax-type/tax-type.module';

import { TaxService } from 'shared/http-services/tax.service';

import { ManageTaxComponent } from './manage-tax.component';

const routes: Routes = [
  {
    path: '',
    component: ManageTaxComponent,
  },
];

@NgModule({
  declarations: [ManageTaxComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot(),
    CasTableModule,
    CasPaginationModule,
    TaxCeModule,
    TaxTypeModule,
  ],
  providers: [TaxService],
})
export class ManageTaxModule {}
