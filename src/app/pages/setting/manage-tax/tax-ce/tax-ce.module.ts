import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap/modal';

import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { TaxRoundModule } from '../tax-round/tax-round.module';
import { ModalApplyTaxMenusModule } from 'app/pages/modal-apply-tax-menus/modal-apply-tax-menus.module';

import { TaxCeComponent } from './tax-ce.component';

@NgModule({
  declarations: [TaxCeComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule.forRoot(),
    CasDialogModule,
    TaxRoundModule,
    ModalApplyTaxMenusModule,
  ],
  entryComponents: [TaxCeComponent],
  exports: [TaxCeComponent],
})
export class TaxCeModule {}
