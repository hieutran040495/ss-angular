import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

import { Tax } from 'shared/models/tax';
import { Menu } from 'shared/models/menu';

import { TaxService } from 'shared/http-services/tax.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { ModalApplyTaxMenusComponent } from 'app/pages/modal-apply-tax-menus/modal-apply-tax-menus.component';
import { TaxRoundComponent } from '../tax-round/tax-round.component';

import { Subscription } from 'rxjs';

import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-tax-ce',
  templateUrl: './tax-ce.component.html',
  styleUrls: ['./tax-ce.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaxCeComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  tax: Tax = new Tax();
  private bsModalSubscribe: Subscription;
  private isModalRound: boolean = false;

  get itemsString(): string {
    return this.tax.allItems.length !== 0
      ? this.tax.allItems.map((menu: Menu) => menu.name.toEllipsisWithCountLetters(30).string).join(',')
      : 'メニューを選択してください';
  }

  constructor(
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private bsModalSv: BsModalService,
    private cdRef: ChangeDetectorRef,
    private taxSv: TaxService,
    private toastSv: ToastService,
  ) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    if (this.bsModalSubscribe) {
      this.bsModalSubscribe.unsubscribe();
    }
  }

  openModalSettingTax() {
    this.isModalRound = false;

    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered',
      ignoreBackdropClick: true,
      initialState: {
        items: cloneDeep(this.tax.items),
        is_apply_all: cloneDeep(this.tax.is_apply_all),
      },
    };
    this.cdRef.markForCheck();

    this.openModalWithComponent(ModalApplyTaxMenusComponent, opts);
  }

  openModalSettingRound() {
    this.isModalRound = true;

    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      ignoreBackdropClick: true,
      initialState: {
        roundType: cloneDeep(this.tax.round),
      },
    };
    this.cdRef.markForCheck();

    this.openModalWithComponent(TaxRoundComponent, opts);
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    this.bsModalSubscribe = this.bsModalSv.onHidden.subscribe((reason: string) => {
      if (reason) {
        if (this.isModalRound) {
          this.tax.round = reason;
        } else {
          const data = JSON.parse(reason);
          this.tax.is_apply_all = data.is_apply_all;
          this.tax.items = data.items;
        }
      }
      this.bsModalSubscribe.unsubscribe();
      this.cdRef.markForCheck();
    });

    this.bsModalSv.show(comp, opts);
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  saveTax() {
    this.taxSv.updateTax(this.tax.id, this.tax.formData()).subscribe(
      (res: any) => {
        this.closeModal('reload');
      },
      (errors: any) => {
        this.toastSv.error(errors);
      },
    );
  }
}
