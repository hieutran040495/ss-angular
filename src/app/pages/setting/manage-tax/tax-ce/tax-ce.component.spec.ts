import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxCeComponent } from './tax-ce.component';

describe('TaxCeComponent', () => {
  let component: TaxCeComponent;
  let fixture: ComponentFixture<TaxCeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxCeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxCeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
