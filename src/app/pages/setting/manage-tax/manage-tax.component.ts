import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

import { TableHeader } from 'app/modules/cas-table/cas-table.component';
import { PageChangedInput, Pagination } from 'shared/models/pagination';
import { Tax } from 'shared/models/tax';
import { PusherEvent } from 'shared/models/event';

import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { TaxService } from 'shared/http-services/tax.service';

import { TaxCeComponent } from './tax-ce/tax-ce.component';
import { TaxTypeComponent } from './tax-type/tax-type.component';

import * as cloneDeep from 'lodash/cloneDeep';
import * as orderBy from 'lodash/orderBy';

@Component({
  selector: 'app-manage-tax',
  templateUrl: './manage-tax.component.html',
  styleUrls: ['./manage-tax.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ManageTaxComponent implements OnInit {
  isLoading: boolean = false;
  isPaginate: boolean = false;
  isUpdate: boolean = true;
  pagination: Pagination = new Pagination();
  bsModalRef: BsModalRef;

  taxes: Tax[] = [];
  taxSelected: Tax = new Tax();
  tableHeader: Partial<TableHeader>[] = [
    {
      name: '設定ステータス',
      class: 'text-left',
      width: 120,
    },
    {
      name: '税金名',
      class: 'text-left',
    },
    {
      name: '',
      width: 130,
    },
  ];

  constructor(
    private router: Router,
    private bsModalSv: BsModalService,
    private toastSv: ToastService,
    private taxSv: TaxService,
    private echoSv: LaravelEchoService,
    private cdRef: ChangeDetectorRef,
  ) {
    this.echoSv.onNotification((e: any) => {
      const event = new PusherEvent().deserialize(e);
      if (event.isTaxUpdate) {
        if (this.pagination.isPagination) {
          this.pageChanged({
            page: 1,
            itemsPerPage: this.pagination.per_page,
          });
        } else {
          this.getTaxes();
        }
      }
    });
  }

  ngOnInit(): void {
    this.getTaxes();
  }

  pageChanged(paginate: PageChangedInput): void {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;
    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.getTaxes();
    this.appendQueryParams(paginate.page);
  }

  private appendQueryParams(page): void {
    this.router.navigate([], {
      queryParams: {
        page: page,
      },
      queryParamsHandling: 'merge',
    });
  }

  private getTaxes(): void {
    if (this.isLoading) return;
    this.isLoading = true;
    this.cdRef.markForCheck();

    this.taxSv.getTaxes().subscribe(
      (res: any) => {
        this.taxes = orderBy(res.data, ['isOrder']);
        this.taxSelected = this.taxes[0];
        this.pagination.deserialize(res);
        this.isLoading = false;
        this.isUpdate = false;
        this.isPaginate = false;
        this.cdRef.markForCheck();
      },
      (error) => {
        this.toastSv.error(error);
        this.isLoading = false;
        this.isUpdate = false;
        this.isPaginate = false;
        this.cdRef.markForCheck();
      },
    );
  }

  updateTax(taxId: number): void {
    if (!taxId) return;
    this.isLoading = true;

    this.taxSv.getTaxById(taxId, { with: 'items,buffets,courses' }).subscribe(
      (res: Tax) => {
        const opts: ModalOptions = {
          class: 'modal-dialog-centered modal-lg',
          ignoreBackdropClick: true,
          keyboard: false,
          initialState: {
            tax: res,
          },
        };

        this.openModalWithComponent(TaxCeComponent, opts);
      },
      (error: any) => {
        this.toastSv.error(error);
        this.isLoading = false;
        this.cdRef.markForCheck();
      },
    );
  }

  private openModalWithComponent(comp, config: ModalOptions) {
    this.bsModalSv.show(comp, config);
    this.isLoading = false;
    this.cdRef.markForCheck();
  }

  changeTaxType(): void {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        taxType: cloneDeep(this.taxes[0].tax_type),
      },
    };

    this.openModalWithComponent(TaxTypeComponent, opts);
  }
}
