import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

import { TableService } from 'shared/http-services/table.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { Table } from 'shared/models/table';
import { Pagination, PageChangedInput } from 'shared/models/pagination';

import { ModalTableCeComponent } from 'app/pages/modal-table-ce/modal-table-ce.component';

import { DIALOG_EVENT } from 'shared/enums/modes';
import { SWAP_TYPE } from 'shared/enums/swap';

import { Subscription } from 'rxjs';

import * as cloneDeep from 'lodash/cloneDeep';
import { DownloadHelpers } from 'shared/utils/download';
import { ClientProfileQuery } from 'shared/states/client-profile';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit, OnDestroy {
  tables: Table[] = [];
  isLoading: boolean = false;
  isPaginate: boolean = false;
  modalRef: BsModalRef;
  private _subModal: Subscription;

  get summaryTable(): string {
    return `( 合計 ${this.pagination.total}件 )`;
  }

  pagination: Pagination = new Pagination();

  sortTables = [
    {
      name: 'テーブル名',
      value: 'ordinal_number,name',
      active: false,
    },
    {
      name: '人数',
      value: 'quantity',
      active: false,
    },
    {
      name: 'タイプ',
      value: 'type.name',
      active: false,
    },
    {
      name: '喫煙',
      value: 'can_smoke',
      active: false,
    },
  ];
  orderTable = {
    order: 'ordinal_number',
  };
  filterTables;

  isSwap: boolean = false;
  swapId: number;
  SWAP_TYPE = SWAP_TYPE;
  isResetSwap: boolean = false;
  isLoadingQRCode: boolean = false;
  downQRCodeInterval;
  isShowQRCode: boolean = false;

  constructor(
    private _tableService: TableService,
    private modalSv: BsModalService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private toastrSv: ToastService,
    private _cd: ChangeDetectorRef,
    private clientProfileQuery: ClientProfileQuery,
  ) {}

  ngOnInit() {
    if (this.activeRoute.snapshot.queryParams.hasOwnProperty('page')) {
      this.pagination.current_page = this.activeRoute.snapshot.queryParams.page;
    }
    this.fetchTables();
  }

  ngOnDestroy() {
    if (this._subModal) {
      this._subModal.unsubscribe();
    }
  }

  private async getClientInfo() {
    const data = await this.clientProfileQuery.getClientProfile();
    this.isShowQRCode = data.allow_smp_order;
  }

  handleSwap() {
    this.isSwap = !this.isSwap;
    if (this.isSwap) {
      this.fetchTables(true);
    } else {
      this.fetchTables();
    }
  }

  selectItemSwap(id: number) {
    if (!this.isSwap) return;
    this.swapId = id;

    setTimeout(() => {
      this.swapId = undefined;
    }, 0);
  }

  tableSwapped(e) {
    this.tables = e;
    this.swapId = undefined;
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;
    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.fetchTables();
    this.appendQueryParams(paginate.page);
  }

  private appendQueryParams(page) {
    this.router.navigate([], {
      queryParams: {
        page: page,
      },
      queryParamsHandling: 'merge',
    });
  }

  private fetchTables(isSwap: boolean = false) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.tables = [];

    let opts = {
      with: 'type',
      ...this.orderTable,
      ...this.filterTables,
      ...this.pagination.hasJSON(),
    };

    if (isSwap) {
      opts = {
        ...this.filterTables,
        order: 'ordinal_number',
        with: 'type',
      };
    }

    this._tableService.getTables(opts).subscribe(
      (res) => {
        this.tables = res.data;
        this.pagination = new Pagination().deserialize(res);
        this.isLoading = false;
        this.isPaginate = false;
        this.isResetSwap = false;
        const timeout = setTimeout(() => {
          this.getClientInfo();
          this._cd.detectChanges();
          clearTimeout(timeout);
        }, 100);
      },
      (errors) => {
        this.toastrSv.error(errors);
        this.isLoading = false;
        this.isPaginate = false;
        this.isResetSwap = false;
        this._cd.detectChanges();
      },
    );
  }

  createTable() {
    if (this.isSwap) return;

    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-lg',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        table: new Table(),
      },
    };

    this.openModalWithComponent(ModalTableCeComponent, opts);
  }

  editTable(table: Table) {
    if (this.isSwap) return;

    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-lg',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        table,
      },
    };

    this.openModalWithComponent(ModalTableCeComponent, opts);
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    this._subModal = this.modalSv.onHidden.subscribe((reason: string) => {
      let currentPage: number = cloneDeep(this.pagination.current_page);

      if (reason === DIALOG_EVENT.TABLE_REMOVE) {
        if (this.pagination.current_page === 1) {
          currentPage = 1;
        } else if (this.tables.length > 1) {
          currentPage = cloneDeep(this.pagination.current_page);
        } else {
          currentPage = cloneDeep(this.pagination.current_page) - 1;
        }
      }

      if (reason === DIALOG_EVENT.TABLE_RELOAD || reason === DIALOG_EVENT.TABLE_REMOVE) {
        this.pagination.reset();
        this._cd.detectChanges();
        this.pageChanged({
          page: currentPage,
          itemsPerPage: this.pagination.per_page,
        });
      }
    });

    this.modalRef = this.modalSv.show(comp, opts);
  }

  tableSort(data) {
    this.orderTable = data;
    this.pagination.reset();
    this.fetchTables();
  }

  tableFilter(data: any) {
    this.filterTables = data;
    if (this.isSwap) {
      this.isResetSwap = true;
      this.fetchTables(true);
    } else {
      this.pagination.reset();
      this.appendQueryParams(this.pagination.current_page);
      this.fetchTables();
    }
  }

  exportPathQRCode() {
    if (this.isLoadingQRCode) return;
    this.isLoadingQRCode = true;

    if (!this.downQRCodeInterval) {
      this.downQRCode();
    }

    this.downQRCodeInterval = setInterval(() => {
      this.downQRCode();
    }, 10000);
  }

  downQRCode() {
    this._tableService.downloadQRCode().subscribe(
      (res) => {
        if (res.url) {
          clearInterval(this.downQRCodeInterval);
          DownloadHelpers.downloadFromUri(res.url);
          this.isLoadingQRCode = false;
          this._cd.detectChanges();
        }
      },
      (error) => {
        this.toastrSv.error(error);
        this.isLoadingQRCode = false;
        this._cd.detectChanges();
      },
    );
  }
}
