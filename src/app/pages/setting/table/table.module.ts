import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { ModalModule } from 'ngx-bootstrap/modal';

import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { CasCardTableModule } from 'app/modules/cas-card-table/cas-card-table.module';
import { FilterTableModule } from 'app/forms/filter-table/filter-table.module';
import { ModalTableCeModule } from 'app/pages/modal-table-ce/modal-table-ce.module';

import { TableComponent } from './table.component';

import { CasSwapModule } from 'app/modules/cas-swap/cas-swap.module';

const routes: Routes = [
  {
    path: '',
    component: TableComponent,
  },
];

@NgModule({
  declarations: [TableComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    CasPaginationModule.forRoot(),
    ModalModule.forRoot(),
    CasFilterModule,
    CasCardTableModule,
    FilterTableModule,
    ModalTableCeModule,
    CasSwapModule,
  ],
})
export class TableModule {}
