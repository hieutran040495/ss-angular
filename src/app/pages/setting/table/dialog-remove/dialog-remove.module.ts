import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { DialogRemoveComponent } from './dialog-remove.component';

@NgModule({
  declarations: [DialogRemoveComponent],
  imports: [CommonModule, CasDialogModule],
  entryComponents: [DialogRemoveComponent],
  exports: [DialogRemoveComponent],
})
export class DialogRemoveModule {}
