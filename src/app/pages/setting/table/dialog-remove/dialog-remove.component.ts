import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ToastService } from 'shared/logical-services/toast.service';
import { TableService } from 'shared/http-services/table.service';
import { DIALOG_EVENT } from 'shared/enums/modes';

@Component({
  selector: 'app-dialog-remove',
  templateUrl: './dialog-remove.component.html',
  styleUrls: ['./dialog-remove.component.scss'],
})
export class DialogRemoveComponent implements OnInit {
  isLoading: boolean = false;

  tableID: number;

  constructor(
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private tableSv: TableService,
    private toastSv: ToastService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {}

  removeTable() {
    if (this.isLoading) return;

    this.isLoading = true;
    this._cd.detectChanges();

    this.tableSv.removeTable(this.tableID).subscribe(
      () => {
        this.toastSv.success('テーブルを削除しました');
        this.closeModal(DIALOG_EVENT.TABLE_REMOVE);
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
