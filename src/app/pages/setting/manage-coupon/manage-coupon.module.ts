import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { EllipsisModule } from 'shared/pipes/ellipsis/ellipsis.module';

import { ManageCouponComponent } from './manage-coupon.component';
import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { CouponFilterModule } from './coupon-filter/coupon-filter.module';
import { CouponItemModule } from './coupon-item/coupon-item.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalManageCouponCeModule } from 'app/pages/modal-manage-coupon-ce/modal-manage-coupon-ce.module';

const routes: Routes = [
  {
    path: '',
    component: ManageCouponComponent,
  },
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    EllipsisModule,
    CasPaginationModule.forRoot(),
    RouterModule.forChild(routes),
    CasFilterModule,
    CouponFilterModule,
    CouponItemModule,
    ModalModule.forRoot(),
    ModalManageCouponCeModule,
  ],
  declarations: [ManageCouponComponent],
})
export class ManageCouponModule {}
