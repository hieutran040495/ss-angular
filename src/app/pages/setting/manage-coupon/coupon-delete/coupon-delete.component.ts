import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { CouponService } from 'shared/http-services/coupon.service';
import { ToastService } from 'shared/logical-services/toast.service';

@Component({
  selector: 'app-coupon-delete',
  templateUrl: './coupon-delete.component.html',
  styleUrls: ['./coupon-delete.component.scss'],
})
export class CouponDeleteComponent implements OnInit {
  couponId: number;

  isLoading: boolean = false;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private toastSv: ToastService,
    private couponSv: CouponService,
  ) {}

  ngOnInit() {}

  couponDelete() {
    if (this.isLoading) return;

    this.isLoading = true;

    this.couponSv.deleteCoupon(this.couponId).subscribe(
      (res: any) => {
        this.closeDialog('delete');
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
    this.isLoading = false;
  }
}
