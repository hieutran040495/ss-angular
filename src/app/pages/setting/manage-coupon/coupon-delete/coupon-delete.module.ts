import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

import { CouponDeleteComponent } from './coupon-delete.component';

@NgModule({
  declarations: [CouponDeleteComponent],
  imports: [CommonModule, CasDialogModule],
  entryComponents: [CouponDeleteComponent],
  exports: [CouponDeleteComponent],
})
export class CouponDeleteModule {}
