import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { Pagination, PageChangedInput } from 'shared/models/pagination';

import { COUPON_STATUS } from 'shared/enums/coupon';
import { Coupon, CouponStatistics } from 'shared/models/coupon';

import { ToastService } from 'shared/logical-services/toast.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { CouponService } from 'shared/http-services/coupon.service';

import { ModalManageCouponCeComponent } from 'app/pages/modal-manage-coupon-ce/modal-manage-coupon-ce.component';
import { Subscription } from 'rxjs';

import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-manage-coupon',
  templateUrl: './manage-coupon.component.html',
  styleUrls: ['./manage-coupon.component.scss'],
})
export class ManageCouponComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  isLoadingStatistics: boolean = false;
  isPaginate: boolean = false;

  coupons: Coupon[] = [];
  couponStatistics: CouponStatistics = {
    total_drafts: 0,
    total_publishes: 0,
    total_unpublishes: 0,
    total_expiries: 0,
    total: 0,
  };
  pagination: Pagination = new Pagination();
  COUPON_STATUS = COUPON_STATUS;
  statusFilter: string = COUPON_STATUS.PUBLISH;

  sortItems = [
    {
      name: 'クーポン名',
      value: 'name',
      active: false,
    },
    {
      name: '期間開始日',
      value: 'start_date',
      active: false,
    },
  ];

  private _filterTerm: any = {};
  private _orderTerm: any = {};

  private _modalRef: BsModalRef;
  private modalSubscribe: Subscription;
  private eventSubscribe: Subscription;

  is_active_filter(status?: string): boolean {
    return this.statusFilter === status;
  }

  constructor(
    private eventEmitterSv: EventEmitterService,
    private couponSv: CouponService,
    private toastSv: ToastService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private modalSv: BsModalService,
    private _cd: ChangeDetectorRef,
  ) {
    this.activeRoute.queryParams.subscribe((res) => {
      if (res.page) {
        this.pagination.current_page = +res.page;
      }
      if (res.status) {
        this.statusFilter = res.status;
      }
    });

    this.eventSubscribe = this.eventEmitterSv.caseNumber$.subscribe((res: any) => {
      if (res.type && res.type === 'publish-coupon') {
        const currentPage = this.settingCurrentPage();
        this.pagination.reset();
        this._cd.detectChanges();
        this.pageChanged({
          page: currentPage,
          itemsPerPage: this.pagination.per_page,
        });
      }
    });
  }

  ngOnInit() {
    this.getCoupons();
  }

  ngOnDestroy() {
    if (this._modalRef) {
      this._modalRef.hide();
    }

    if (this.modalSubscribe) {
      this.modalSubscribe.unsubscribe();
    }

    if (this.eventSubscribe) {
      this.eventSubscribe.unsubscribe();
    }
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }

    this.isPaginate = true;
    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.getCoupons();
    this.appendQueryParams(paginate.page);
  }

  private getCountStatistics() {
    this.isLoadingStatistics = true;
    this._cd.detectChanges();

    this.couponSv.couponStatistics().subscribe(
      (res) => {
        this.couponStatistics = res;
        this.isLoadingStatistics = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoadingStatistics = false;
        this._cd.detectChanges();
      },
    );
  }

  private getCoupons() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;

    this.coupons = [];
    const opts = {
      ...this.pagination.hasJSON(),
      ...this._filterTerm,
      ...this._orderTerm,
      status: this.statusFilter === COUPON_STATUS.TOTAL ? undefined : this.statusFilter,
    };

    this.couponSv.getCoupons(opts).subscribe(
      (res) => {
        this.coupons = res.data;
        this.pagination = new Pagination().deserialize(res);
        this.isLoading = false;
        this.isPaginate = false;
        this.getCountStatistics();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this.isPaginate = false;
        this._cd.detectChanges();
      },
    );
  }

  filterCoupons(status?: string) {
    this.statusFilter = status;
    this.resetCoupons();
  }

  private appendQueryParams(page: number) {
    this.router.navigate([], {
      queryParams: {
        page: page,
        status: this.statusFilter,
      },
      queryParamsHandling: 'merge',
    });
  }

  updateSortTerm(newTerm: any) {
    this._orderTerm = newTerm;
    this.resetCoupons();
  }

  changeFilter(event: any) {
    this._filterTerm = event;
    this.resetCoupons();
  }

  private resetCoupons() {
    this.pagination = new Pagination();
    this._cd.detectChanges();
    this.getCoupons();
    this.appendQueryParams(this.pagination.current_page);
  }

  createCoupon() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-lg',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        coupon: new Coupon(),
      },
    };

    this._openModalWithComponent(ModalManageCouponCeComponent, opts);
  }

  editCoupon(coupon: Coupon) {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-lg',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        coupon,
      },
    };

    this._openModalWithComponent(ModalManageCouponCeComponent, opts);
  }

  private _openModalWithComponent(comp, opts: ModalOptions = {}) {
    this.modalSubscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      let currentPage: number = cloneDeep(this.pagination.current_page);

      if (reason === 'delete') {
        currentPage = this.settingCurrentPage();
      }

      if (reason === 'reload' || reason === 'delete') {
        this.pagination.reset();
        this._cd.detectChanges();
        this.pageChanged({
          page: currentPage,
          itemsPerPage: this.pagination.per_page,
        });
      }
    });

    this.modalSv.show(comp, opts);
  }

  private settingCurrentPage(): number {
    let currentPage: number = cloneDeep(this.pagination.current_page);

    if (this.pagination.current_page === 1) {
      currentPage = 1;
    } else if (this.coupons.length > 1) {
      currentPage = cloneDeep(this.pagination.current_page);
    } else {
      currentPage = cloneDeep(this.pagination.current_page) - 1;
    }

    return currentPage;
  }
}
