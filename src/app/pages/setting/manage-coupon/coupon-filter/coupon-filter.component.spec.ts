import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CouponFilterComponent } from './coupon-filter.component';

describe('CouponFilterComponent', () => {
  let component: CouponFilterComponent;
  let fixture: ComponentFixture<CouponFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CouponFilterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CouponFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
