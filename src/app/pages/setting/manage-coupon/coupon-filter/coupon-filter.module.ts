import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CouponFilterComponent } from './coupon-filter.component';
import { FormsModule } from '@angular/forms';
import { NgxDatepickerModule } from 'shared/modules/ngx-datepicker/ngx-datepicker.module';
import { NumbericValidatorModule } from 'shared/directive/numberic-validator/numberic-validator.module';
import { DigitFormaterModule } from 'shared/modules/digit-formater/digit-formater.module';

@NgModule({
  imports: [CommonModule, FormsModule, NgxDatepickerModule, NumbericValidatorModule, DigitFormaterModule],
  declarations: [CouponFilterComponent],
  exports: [CouponFilterComponent],
})
export class CouponFilterModule {}
