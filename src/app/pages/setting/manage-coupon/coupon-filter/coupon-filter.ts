import * as pickBy from 'lodash/pickBy';
import * as moment from 'moment';

export class CouponFilter {
  name: string;
  price_lte: number;
  price_gte: number;
  end_date_lte: Date;
  start_date_gte: Date;

  constructor() {}

  public toJSON() {
    const output = pickBy(this, (value, key) => {
      return ['name', 'price_lte', 'price_gte'].indexOf(key) >= 0 && value;
    });
    if (this.name) {
      output.name = this.name.trim();
    }

    if (this.end_date_lte) {
      output.end_date_lte = moment(this.end_date_lte).format('YYYY-MM-DD');
    }

    if (this.start_date_gte) {
      output.start_date_gte = moment(this.start_date_gte).format('YYYY-MM-DD');
    }

    return output;
  }
}
