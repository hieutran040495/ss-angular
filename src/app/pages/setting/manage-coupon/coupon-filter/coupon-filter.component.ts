import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CouponFilter } from './coupon-filter';
import * as moment from 'moment';
import { ToastService } from 'shared/logical-services/toast.service';

@Component({
  selector: 'app-coupon-filter',
  templateUrl: './coupon-filter.component.html',
  styleUrls: ['./coupon-filter.component.scss'],
})
export class CouponFilterComponent implements OnInit {
  @Output() changeFilter: EventEmitter<any> = new EventEmitter<any>();
  @Input('disabled') disabled: boolean;

  filterTerm: CouponFilter = new CouponFilter();

  constructor(private _toastSv: ToastService) {}

  ngOnInit() {}

  public filterCoupon() {
    if (
      this.filterTerm.start_date_gte &&
      this.filterTerm.end_date_lte &&
      moment(this.filterTerm.start_date_gte).isAfter(moment(this.filterTerm.end_date_lte))
    ) {
      this._toastSv.warning(`終了日付は開始日付以降の日付で設定してください`);
      return;
    }
    this.changeFilter.emit(this.filterTerm.toJSON());
  }
}
