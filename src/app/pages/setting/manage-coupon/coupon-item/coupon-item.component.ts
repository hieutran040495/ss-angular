import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Coupon } from 'shared/models/coupon';

@Component({
  selector: 'app-coupon-item',
  templateUrl: './coupon-item.component.html',
  styleUrls: ['./coupon-item.component.scss'],
})
export class CouponItemComponent implements OnInit {
  @Input('coupon') coupon: Coupon;
  @Output() changeCoupon: EventEmitter<Coupon> = new EventEmitter<Coupon>();

  constructor() {}

  ngOnInit() {}

  public changedCoupon() {
    this.changeCoupon.emit(this.coupon);
  }
}
