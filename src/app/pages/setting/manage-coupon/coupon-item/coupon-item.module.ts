import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CouponItemComponent } from './coupon-item.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [CouponItemComponent],
  exports: [CouponItemComponent],
})
export class CouponItemModule {}
