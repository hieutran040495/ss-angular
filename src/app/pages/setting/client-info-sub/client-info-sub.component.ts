import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Regexs } from 'shared/constants/regex';
import { DURATIONS } from 'shared/constants/time-setting';
import { Client } from 'shared/models/client';
import { User } from 'shared/models/user';

import { ValidatorService } from 'shared/utils/validator.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ClientService } from 'shared/http-services/client.service';
import { ClientProfileService } from 'shared/states/client-profile';
import { SessionService } from 'shared/states/session';

import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-client-info-sub',
  templateUrl: './client-info-sub.component.html',
  styleUrls: ['./client-info-sub.component.scss'],
})
export class ClientInfoSubComponent implements OnInit {
  clientInfo: Client = new Client();
  currentUser: User = new User();

  rules = Regexs;
  DURATIONS = DURATIONS;
  validator: any = {};
  isLoading: boolean = false;
  isUpdatingPolicy: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastSv: ToastService,
    private clientSv: ClientService,
    private clientProfileSv: ClientProfileService,
    private sessionService: SessionService,
    private validationSv: ValidatorService,
    private _cd: ChangeDetectorRef,
  ) {
    this.activatedRoute.data.subscribe(
      (res) => {
        if (res.clientInfo) {
          this.clientInfo = res.clientInfo;
        }
      },
      (errors) => {
        this.router.navigate(['/']);
      },
    );
  }

  async ngOnInit() {
    this.currentUser = await this.sessionService.getUserLoggined();
    this._cd.detectChanges();
  }

  resetValidator(key: string) {
    if (this.validator[key]) {
      this.validator[key] = undefined;
    }
    this._cd.detectChanges();
  }

  updateClient() {
    this._updateClient(this.clientInfo.formDataSubClient(), '店舗情報を更新しました');
  }

  private _updateClient(params: any, msg: string, loading: string = 'isLoading') {
    if (this[loading]) {
      return;
    }

    this[loading] = true;
    this._cd.detectChanges();

    this.clientSv.updateClient(params).subscribe(
      (res) => {
        this.toastSv.success(msg);
        this[loading] = false;
        this.clientInfo = new Client().deserialize({ ...res, cancel_policy: this.clientInfo.cancel_policy });
        this.clientProfileSv.updateClientProfile(cloneDeep(this.clientInfo));
        this._cd.detectChanges();
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validator = this.validationSv.setErrors(errors.errors);
        }
        this.toastSv.error(errors);
        this[loading] = false;
        this._cd.detectChanges();
      },
    );
  }
}
