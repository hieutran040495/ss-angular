import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { ClientInfoResolve } from 'app/pages/setting/client-info/client-info.resolve';
import { ValidatorService } from 'shared/utils/validator.service';

import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';

import { ClientInfoSubComponent } from './client-info-sub.component';

const routes: Routes = [
  {
    path: '',
    component: ClientInfoSubComponent,
    resolve: {
      clientInfo: ClientInfoResolve,
    },
  },
];

@NgModule({
  declarations: [ClientInfoSubComponent],
  imports: [CommonModule, FormsModule, RouterModule.forChild(routes), OnlyNumericModule, CasNgSelectModule],
  providers: [ClientInfoResolve, ValidatorService],
})
export class ClientInfoSubModule {}
