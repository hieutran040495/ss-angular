import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientInfoSubComponent } from './client-info-sub.component';

describe('ClientInfoSubComponent', () => {
  let component: ClientInfoSubComponent;
  let fixture: ComponentFixture<ClientInfoSubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ClientInfoSubComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientInfoSubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
