import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DropDownModule } from 'shared/directive/dropdown/dropdown.module';

import { SettingComponent } from './setting.component';
import { AdminResolve } from 'app/guards/admin.resolve';

const routes: Routes = [
  {
    path: '',
    component: SettingComponent,
    children: [
      {
        path: 'information',
        loadChildren: './client-info/client-info.module#ClientInfoModule',
        data: {
          isSetting: true,
          appScreen: 'C-5',
        },
      },
      {
        path: 'sub-information',
        loadChildren: './client-info-sub/client-info-sub.module#ClientInfoSubModule',
        data: {
          isSetting: true,
          appScreen: 'C-5-8-1',
        },
      },
      {
        path: 'menus',
        loadChildren: './menus/menus.module#MenusModule',
        data: {
          isSetting: true,
          appScreen: 'C-5-1',
        },
      },
      {
        path: 'menu-types',
        loadChildren: './menus-type/menus-type.module#MenuTypeModule',
        data: {
          isSetting: true,
          appScreen: 'C-5-1-6',
        },
      },
      {
        path: 'menu-buffet',
        loadChildren: './menu-buffet/menu-buffet.module#MenuBuffetModule',
        data: {
          isSetting: true,
          appScreen: 'C-5-2-3',
        },
      },
      {
        path: 'category',
        loadChildren: './category/category.module#CategoryModule',
        data: {
          isSetting: true,
          appScreen: 'C-5-1-5',
        },
      },
      {
        path: 'rich-menu',
        loadChildren: './rich-menu/rich-menu.module#RichMenuModule',
        data: {
          isSetting: true,
          appScreen: 'C-5-1-7',
        },
      },
      {
        path: 'courses',
        loadChildren: './courses/courses.module#CoursesModule',
        data: {
          isSetting: true,
          appScreen: 'C-5-2',
        },
      },
      {
        path: 'coupons',
        loadChildren: './manage-coupon/manage-coupon.module#ManageCouponModule',
        data: {
          isSetting: true,
          appScreen: 'C-5-3',
        },
      },
      //
      {
        path: 'manage-gourmet',
        loadChildren: './manage-gourmet/manage-gourmet.module#ManageGourmetModule',
        data: {
          isSetting: true,
          appScreen: 'C-5-8-2',
        },
      },
      //

      {
        path: 'account',
        loadChildren: './manage-account/manage-account.module#ManageAccountModule',
        resolve: {
          role: AdminResolve,
        },
        data: {
          isSetting: true,
          appScreen: 'C-5-5',
        },
      },
      {
        path: 'table',
        data: {
          isSetting: true,
        },
        children: [
          {
            path: '',
            loadChildren: './table/table.module#TableModule',
            data: {
              appScreen: 'C-5-4',
            },
          },
          {
            path: 'type',
            loadChildren: './table-type/table-type.module#TableTypeModule',
            data: {
              appScreen: 'C-5-4-3',
            },
          },
        ],
      },

      {
        path: 'menu-options',
        loadChildren: './menu-options/menu-options.module#MenuOptionsModule',
        data: {
          isSetting: true,
          appScreen: 'C-5-1-1-1',
        },
      },
      {
        path: 'cooking-masters',
        loadChildren: './cooking-master/cooking-master.module#CookingMasterModule',
        data: {
          isSetting: true,
          appScreen: 'C-11',
        },
      },
      {
        path: 'manager-tax',
        loadChildren: 'app/pages/setting/manage-tax/manage-tax.module#ManageTaxModule',
        data: {
          isSetting: true,
        },
      },
      {
        path: '**',
        redirectTo: 'information',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  declarations: [SettingComponent],
  imports: [CommonModule, DropDownModule, RouterModule.forChild(routes)],
  providers: [AdminResolve],
})
export class SettingModule {}
