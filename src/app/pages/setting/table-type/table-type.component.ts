import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { TableTypeService } from 'shared/http-services/table-type.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { PageChangedInput, Pagination } from 'shared/models/pagination';
import { TABLETYPE, TableType } from 'shared/models/table-type';

// Import component
import { DialogCeComponent } from 'app/pages/setting/table-type/dialog-ce/dialog-ce.component';
import { DialogDestroyComponent } from 'app/pages/setting/table-type/dialog-destroy/dialog-destroy.component';

import { ActivatedRoute, Router } from '@angular/router';

import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-table-type',
  templateUrl: './table-type.component.html',
  styleUrls: ['./table-type.component.scss'],
})
export class TableTypeComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  isPaginate: boolean = false;

  tableTypes: TableType[] = [];
  pagination: Pagination = new Pagination();

  private bsModalRef: BsModalRef;

  private orderBuffet;
  sortTableType = [
    {
      name: 'テーブルタイプ名',
      value: 'name',
      active: false,
    },
  ];
  filterName = '';

  constructor(
    private bsModalSv: BsModalService,
    private tableTypeSv: TableTypeService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private toastr: ToastService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.activeRoute.snapshot.queryParams.hasOwnProperty('page')) {
      this.pagination.current_page = this.activeRoute.snapshot.queryParams.page;
    }
    this.fetchTableTypes();
  }

  ngOnDestroy(): void {
    if (this.bsModalRef) {
      this.bsModalRef.hide();
    }
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;

    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.fetchTableTypes();
    this.appendQueryParams(paginate.page);
  }

  private appendQueryParams(page: number) {
    this.router.navigate([], {
      queryParams: {
        page: page,
      },
      queryParamsHandling: 'merge',
    });
  }

  private fetchTableTypes() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    const opts: any = {
      ...this.pagination.hasJSON(),
      ...this.orderBuffet,
      name: this.filterName.trim(),
    };

    this.tableTypes = [];
    this._cd.detectChanges();

    this.tableTypeSv.fetchTableTypes(opts).subscribe(
      (res) => {
        this.tableTypes = res.data;
        this.pagination.deserialize(res);
        this.isLoading = false;
        this.isPaginate = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastr.error(errors);
        this.isLoading = false;
        this.isPaginate = false;
        this._cd.detectChanges();
      },
    );
  }

  createTableType() {
    const modalOpts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      keyboard: false,
      ignoreBackdropClick: true,
    };
    this.openModalWithComponent(DialogCeComponent, modalOpts);
  }

  editTableType(table: TableType) {
    const modalOpts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      keyboard: false,
      ignoreBackdropClick: true,
      initialState: {
        tableType: cloneDeep(table),
      },
    };

    this.openModalWithComponent(DialogCeComponent, modalOpts);
  }

  destroyTableType(table: TableType) {
    const modalOpts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      keyboard: false,
      ignoreBackdropClick: true,
      initialState: {
        tableType: cloneDeep(table),
      },
    };

    this.openModalWithComponent(DialogDestroyComponent, modalOpts);
  }

  private openModalWithComponent(comp, config: ModalOptions) {
    const subscribe = this.bsModalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();

      let currentPage: number = cloneDeep(this.pagination.current_page);

      if (reason === TABLETYPE.RELOAD) {
        if (this.pagination.current_page === 1) {
          currentPage = 1;
        } else if (this.tableTypes.length > 1) {
          currentPage = cloneDeep(this.pagination.current_page);
        } else {
          currentPage = cloneDeep(this.pagination.current_page) - 1;
        }
      }

      if (reason === TABLETYPE.RELOAD || reason === TABLETYPE.RELOAD) {
        this.pagination.reset();
        this._cd.detectChanges();
        this.pageChanged({
          page: currentPage,
          itemsPerPage: this.pagination.per_page,
        });
      }
    });

    this.bsModalRef = this.bsModalSv.show(comp, config);
  }

  tableTypeSort(data: any) {
    this.orderBuffet = data;
    this.filterTableType();
  }

  filterTableType() {
    this.pagination.reset();
    this.appendQueryParams(this.pagination.current_page);
    this.fetchTableTypes();
  }
}
