import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { ModalModule } from 'ngx-bootstrap/modal';

import { DialogCeModule } from 'app/pages/setting/table-type/dialog-ce/dialog-ce.module';
import { DialogDestroyModule } from 'app/pages/setting/table-type/dialog-destroy/dialog-destroy.module';
import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { CasCardTypeModule } from 'app/modules/cas-card-type/cas-card-type.module';

import { TableTypeComponent } from './table-type.component';

const routes: Routes = [
  {
    path: '',
    component: TableTypeComponent,
  },
];

@NgModule({
  declarations: [TableTypeComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    CasPaginationModule.forRoot(),
    ModalModule.forRoot(),
    DialogCeModule,
    DialogDestroyModule,
    CasFilterModule,
    CasCardTypeModule,
  ],
})
export class TableTypeModule {}
