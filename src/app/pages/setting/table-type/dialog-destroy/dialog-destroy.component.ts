import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { TableTypeService } from 'shared/http-services/table-type.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { TableType, TABLETYPE } from 'shared/models/table-type';

@Component({
  selector: 'app-dialog-destroy',
  templateUrl: './dialog-destroy.component.html',
  styleUrls: ['./dialog-destroy.component.scss'],
})
export class DialogDestroyComponent implements OnInit {
  isLoading: boolean = false;
  tableType: TableType = new TableType();

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private tableTypeSv: TableTypeService,
    private toastrSv: ToastService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {}

  destroyTableType() {
    if (this.isLoading) return;

    this.isLoading = true;
    this._cd.detectChanges();

    this.tableTypeSv.destroyTableType(this.tableType.formData()).subscribe(
      () => {
        this.toastrSv.success('テーブルタイプを削除しました');
        this.closeDialog(TABLETYPE.RELOAD);
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
