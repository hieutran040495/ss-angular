import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DialogDestroyComponent } from './dialog-destroy.component';

import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

@NgModule({
  declarations: [DialogDestroyComponent],
  imports: [CommonModule, CasDialogModule],
  entryComponents: [DialogDestroyComponent],
  exports: [DialogDestroyComponent],
})
export class DialogDestroyModule {}
