import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogDestroyComponent } from './dialog-destroy.component';

describe('DialogDestroyComponent', () => {
  let component: DialogDestroyComponent;
  let fixture: ComponentFixture<DialogDestroyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogDestroyComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogDestroyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
