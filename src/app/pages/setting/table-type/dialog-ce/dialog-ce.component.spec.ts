import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCeComponent } from './dialog-ce.component';

describe('DialogCeComponent', () => {
  let component: DialogCeComponent;
  let fixture: ComponentFixture<DialogCeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogCeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
