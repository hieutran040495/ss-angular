import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ValidatorService } from 'shared/utils/validator.service';

import { DialogCeComponent } from './dialog-ce.component';

import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

@NgModule({
  declarations: [DialogCeComponent],
  imports: [CommonModule, FormsModule, SpaceValidationModule, CasDialogModule],
  providers: [ValidatorService],
  entryComponents: [DialogCeComponent],
  exports: [DialogCeComponent],
})
export class DialogCeModule {}
