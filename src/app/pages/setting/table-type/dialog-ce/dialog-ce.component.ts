import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { TableTypeService } from 'shared/http-services/table-type.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ValidatorService } from 'shared/utils/validator.service';

import { TableType, TABLETYPE } from 'shared/models/table-type';
import { Mode } from 'shared/utils/mode';

interface Validation {
  name: string | undefined;
}

@Component({
  selector: 'app-dialog-ce',
  templateUrl: './dialog-ce.component.html',
  styleUrls: ['./dialog-ce.component.scss'],
})
export class DialogCeComponent implements OnInit {
  isLoading: boolean = false;
  mode: Mode = new Mode();

  tableType: TableType = new TableType();
  validation: Validation = {
    name: undefined,
  };

  get dialogTitle(): string {
    return this.mode.isNew ? 'テーブルタイプ新規追加' : 'テーブルタイプ編集';
  }

  get submitText(): string {
    return this.mode.isNew ? '作成する' : '編集する';
  }

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private tableTypeSv: TableTypeService,
    private toastrSv: ToastService,
    private validationSv: ValidatorService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.tableType && this.tableType.id) {
      this.mode.setEdit();
    } else {
      this.mode.setNew();
    }
  }

  submitTableType(): void {
    if (this.mode.isNew) {
      this.createTableType();
    } else {
      this.updateTableType();
    }
  }

  private createTableType() {
    if (this.isLoading) return;

    this.isLoading = true;
    this._cd.detectChanges();

    this.tableTypeSv.createTableType(this.tableType.formData()).subscribe(
      () => {
        this.toastrSv.success('テーブルタイプを追加しました');
        this.closeDialog(TABLETYPE.RELOAD);
        this._cd.detectChanges();
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validationSv.setErrors(errors.errors);
        }
        this.toastrSv.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  private updateTableType() {
    if (this.isLoading) return;

    this.isLoading = true;
    this._cd.detectChanges();

    this.tableTypeSv.updateTableType(this.tableType.formData()).subscribe(
      () => {
        this.toastrSv.success('テーブルタイプを編集しました');
        this.closeDialog(TABLETYPE.RELOAD);
        this._cd.detectChanges();
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validationSv.setErrors(errors.errors);
        }
        this.toastrSv.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  validatorReset() {
    this.validation.name = undefined;
    this._cd.detectChanges();
  }
}
