import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { MemberServices } from 'shared/http-services/member.services';
import { ToastService } from 'shared/logical-services/toast.service';

import { User } from 'shared/models/user';
import { PageChangedInput, Pagination } from 'shared/models/pagination';

import { ManageAccountCeComponent } from 'app/pages/manage-account-ce/manage-account-ce.component';
import { DIALOG_EVENT } from 'shared/enums/modes';
import { Router, ActivatedRoute } from '@angular/router';

import * as cloneDeep from 'lodash/cloneDeep';

export interface Role {
  role_name: string;
}

@Component({
  selector: 'app-manage-account',
  templateUrl: './manage-account.component.html',
  styleUrls: ['./manage-account.component.scss'],
})
export class ManageAccountComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  isPagination: boolean = false;

  users: User[] = [];
  pagination: Pagination = new Pagination();

  tableHeaders: any = [
    {
      name: 'アカウント名',
      class: 'text-center',
      width: 200,
    },
    {
      name: 'メールアドレス',
      class: 'text-center',
    },
    {
      name: '役職',
      class: 'text-center',
      width: 200,
    },
    {
      name: '',
      width: 100,
    },
  ];

  // Filter
  private orderAccount;
  sortAccount = [
    {
      name: 'アカウント名',
      value: 'name',
      active: false,
    },
    {
      name: '役職',
      value: 'role_name',
      active: false,
    },
  ];
  filterData: any = {
    name: '',
    role_name: undefined,
  };

  roles: Role[] = [];

  private modalRef: BsModalRef;

  constructor(
    private memberSv: MemberServices,
    private toastrSv: ToastService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private modalSv: BsModalService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.activeRoute.snapshot.queryParams.hasOwnProperty('page')) {
      this.pagination.current_page = this.activeRoute.snapshot.queryParams.page;
    }
    this.getUsers();
    this.getRoles();
  }

  ngOnDestroy() {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPagination) {
      return;
    }
    this.isPagination = true;

    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.getUsers();
    this.appendQueryParams(paginate.page);
  }

  private appendQueryParams(page: number) {
    this.router.navigate([], {
      queryParams: {
        page: page,
      },
      queryParamsHandling: 'merge',
    });
  }

  private getUsers() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.filterData.name = this.filterData.name.trim();
    const opts: any = {
      ...this.pagination.hasJSON(),
      ...this.orderAccount,
      ...this.filterData,
    };
    this.users = [];
    this._cd.detectChanges();

    this.memberSv.getMembers(opts).subscribe(
      (res) => {
        this.users = res.data;
        this.pagination.deserialize(res);

        this.isLoading = false;
        this.isPagination = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
        this.isLoading = false;
        this.isPagination = false;
        this._cd.detectChanges();
      },
    );
  }

  private getRoles() {
    this._cd.detectChanges();

    this.memberSv.getRoles().subscribe(
      (res) => {
        this.roles = res.data;
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
        this._cd.detectChanges();
      },
    );
  }

  accountSort(data: any) {
    this.orderAccount = data;
    this.filterAccount();
  }

  filterAccount() {
    this.pagination.reset();
    this.appendQueryParams(this.pagination.current_page);
    this.getUsers();
  }

  openDialogUser(user?: User) {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-lg',
      ignoreBackdropClick: true,
      keyboard: false,
    };

    if (user && user.id) {
      opts.initialState = {
        user: cloneDeep(user),
      };
    }

    this.openModalWithComponent(ManageAccountCeComponent, opts);
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();
      if (reason === DIALOG_EVENT.ACCOUNT_RELOAD) {
        const currentPage = cloneDeep(this.pagination.current_page);
        this.pagination.reset();
        this._cd.detectChanges();
        this.pageChanged({
          page: currentPage,
          itemsPerPage: this.pagination.per_page,
        });
        this.getRoles();
      }
    });

    this.modalSv.show(comp, opts);
  }
}
