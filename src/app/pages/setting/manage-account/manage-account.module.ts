import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { ModalModule } from 'ngx-bootstrap/modal';

import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { CasTableModule } from 'app/modules/cas-table/cas-table.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';
import { ManageAccountCeModule } from 'app/pages/manage-account-ce/manage-account-ce.module';

import { ManageAccountComponent } from './manage-account.component';

const routes: Routes = [
  {
    path: '',
    component: ManageAccountComponent,
  },
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    CasPaginationModule.forRoot(),
    ModalModule.forRoot(),
    CasFilterModule,
    CasTableModule,
    CasNgSelectModule,
    ManageAccountCeModule,
  ],
  declarations: [ManageAccountComponent],
})
export class ManageAccountModule {}
