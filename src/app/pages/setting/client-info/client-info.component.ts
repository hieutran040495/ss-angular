import { Component, OnInit, ChangeDetectorRef, EventEmitter, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Regexs } from 'shared/constants/regex';
import { Prefecture } from 'shared/models/prefecture';
import { Client } from 'shared/models/client';
import { Genre } from 'shared/models/genre';
import { City } from 'shared/models/city';
import { Tag } from 'shared/models/tag';
import { User } from 'shared/models/user';

import { PrefecturesService } from 'shared/http-services/prefecture.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ClientService } from 'shared/http-services/client.service';
import { GenreService } from 'shared/http-services/genre.service';
import { ValidatorService } from 'shared/utils/validator.service';
import { CityService } from 'shared/http-services/city.service';
import { TagService } from 'shared/http-services/tag.service';

import { FileUpload } from 'shared/interfaces/file-upload';
import { Image } from 'shared/models/image';

import { ClientWorkingsService } from 'shared/states/client_workings';
import { ClientProfileService } from 'shared/states/client-profile';
import { SessionService } from 'shared/states/session';

import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { EventEmitterType } from 'shared/enums/event-emitter';

import { ClientWorkingTimeWeekChanging } from 'shared/models/client-working-week';
import { ClientWorkingTime } from 'shared/models/client-working-time';
import { Pagination } from 'shared/models/pagination';

import * as cloneDeep from 'lodash/cloneDeep';
import { Subject, of, Subscription } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, tap, switchMap } from 'rxjs/operators';
import { DROPZONE_TYPE } from 'shared/enums/drop-zone';
import { ClientLogo } from 'shared/models/client-logo';
import { ModalOptions, BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalConfirmTwilioComponent } from 'app/pages/modal-confirm-twilio/modal-confirm-twilio.component';

@Component({
  selector: 'app-client-info',
  templateUrl: './client-info.component.html',
  styleUrls: ['./client-info.component.scss'],
})
export class ClientInfoComponent implements OnInit, OnDestroy {
  clientInfo: Client = new Client();
  currentUser: User = new User();

  isLoading: boolean = false;
  isSearching: boolean = false;
  isLoadGenres: boolean = false;
  isLoadTags: boolean = false;
  isLoadPrefs: boolean = false;
  isLoadCities: boolean = false;
  isShowSecretCode: boolean = false;
  isLoadStoreLogo: boolean = false;
  isRemoveReview: boolean = false;

  genres: Genre[] = [];
  tags: Tag[] = [];
  prefs: Prefecture[] = [];
  cities: City[] = [];
  validator: any = {};

  files: FileUpload[] = [];

  rules = Regexs;
  pagination: Pagination = new Pagination();
  pageSub: Subscription;

  genresInput$: Subject<string> = new Subject<string>();
  eventDropzone: EventEmitter<any> = new EventEmitter<any>();

  get type_secret_code(): string {
    return this.isShowSecretCode ? 'text' : 'password';
  }
  get class_secret_code(): string {
    return this.isShowSecretCode ? 'fa fa-eye-slash' : 'fa fa-eye';
  }

  public isShowAuthToken: boolean = false;

  private bsModalRef: BsModalRef;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _genreSv: GenreService,
    private _tagSv: TagService,
    private _toast: ToastService,
    private _clientSv: ClientService,
    private _prefService: PrefecturesService,
    private _cityService: CityService,
    private _validationSv: ValidatorService,
    private clientWorkingSv: ClientWorkingsService,
    private clientProfileSv: ClientProfileService,
    private eventEmmiterSv: EventEmitterService,
    private sessionService: SessionService,
    private _cd: ChangeDetectorRef,
    private modalSv: BsModalService,
  ) {
    this.pageSub = this.eventDropzone.subscribe((res: any) => {
      if (!res.type) {
        return;
      }

      if (res.type === DROPZONE_TYPE.UPLOAD) {
        this.uploadLogoPrint(res.data[0].origin);
      }

      if (res.type === DROPZONE_TYPE.REMOVE) {
        this.removeLogoPrint();
      }
    });

    this._activatedRoute.data.subscribe(
      (res) => {
        if (res.clientInfo) {
          this.clientInfo = res.clientInfo;
        }
      },
      (errors) => {
        this._router.navigate(['/']);
      },
    );
    this.searchGenre();
  }

  async ngOnInit() {
    this.fetchPrefs();
    this.fetchCities();
    this.fetchGenres();
    this.fetchTags();

    this.currentUser = await this.sessionService.getUserLoggined();
    this._cd.detectChanges();
  }

  ngOnDestroy(): void {
    if (this.pageSub) {
      this.pageSub.unsubscribe();
    }

    if (this.bsModalRef) {
      this.bsModalRef.hide();
    }
  }

  fetchPrefs() {
    this.isLoadPrefs = true;
    this._cd.detectChanges();

    this._prefService.fetchPrefectures().subscribe(
      (res) => {
        this.prefs = res.data;
        this.isLoadPrefs = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.isLoadPrefs = false;
        this._toast.error(errors);
        this._cd.detectChanges();
      },
    );
  }

  changePref() {
    this.resetValidator('pref_code');
    this.clientInfo.city = undefined;
    this.fetchCities();
    this._cd.detectChanges();
  }

  fetchCities() {
    if (!this.clientInfo.pref.prefCode) {
      return;
    }
    this.isLoadCities = true;

    const opts = {
      pref: this.clientInfo.pref.prefCode,
    };

    this._cd.detectChanges();

    this._cityService.fetchCities(opts).subscribe(
      (res) => {
        this.cities = res.data;
        this.isLoadCities = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.isLoadCities = false;
        this._toast.error(errors);
        this._cd.detectChanges();
      },
    );
  }

  fetchGenres() {
    this.isLoadGenres = true;
    const opts: any = {
      ...this.pagination.hasJSON(),
      order: '-updated',
    };
    this._cd.detectChanges();

    this._genreSv.fetchGenresSelect(opts).subscribe(
      (res) => {
        this.genres = this.genres.concat(res);
        this.isLoadGenres = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this._toast.error(errors);
        this.isLoadGenres = false;
        this._cd.detectChanges();
      },
    );
  }

  private searchGenre() {
    this.genresInput$
      .pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => {
          this.isLoadGenres = true;
          this._cd.detectChanges();
        }),
        switchMap((name) => {
          this.pagination.reset();
          const opts: any = {
            ...this.pagination.hasJSON(),
            order: '-updated',
          };
          if (name && name.trim().length > 0) {
            opts.name = name.trim();
            this.isSearching = true;
          } else {
            delete opts.name;
            this.isSearching = false;
          }

          return this._genreSv.fetchGenresSelect(opts).pipe(
            catchError(() => of([])),
            tap(() => {
              this.isLoadGenres = false;
              this._cd.detectChanges();
            }),
          );
        }),
      )
      .subscribe((result) => {
        this.genres = result;
      });
  }

  fetchMore() {
    if (!this.isSearching) {
      this.pagination.nextPage();
      this.fetchGenres();
    }
  }

  fetchTags() {
    this.isLoadTags = true;
    this._cd.detectChanges();

    this._tagSv.fetchTags().subscribe(
      (res) => {
        this.tags = res.data;
        this.isLoadTags = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.isLoadTags = false;
        this._toast.error(errors);
        this._cd.detectChanges();
      },
    );
  }

  private _getClient() {
    const opts: any = {
      with: 'images,tags,genre,client_workings,clientAdmin,logo,cancel_policy,twilio',
    };
    this._cd.detectChanges();

    this._clientSv.fetchClient(opts).subscribe(
      (res) => {
        this.clientInfo = res;
        this.clientWorkingSv.updateWorkingTime(cloneDeep(this.clientInfo.client_workings));
        this.clientProfileSv.updateClientProfile(cloneDeep(this.clientInfo));
        this.eventEmmiterSv.publishData({
          type: EventEmitterType.UPDATE_CLIENT_PROFILE,
        });
        this._cd.detectChanges();
      },
      (errors) => {
        this._toast.error(errors);
        this._cd.detectChanges();
      },
    );
  }

  private _updateClient(params: any, msg: string, isReceiverClient: boolean = false) {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    this._cd.detectChanges();

    this._clientSv.updateClient(params).subscribe(
      (res) => {
        this._toast.success(msg);
        this.isLoading = false;
        this.clientProfileSv.updateClientProfile(cloneDeep(this.clientInfo));
        this._cd.detectChanges();
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validator = this._validationSv.setErrors(errors.errors);
        }
        this._toast.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
      () => {
        if (isReceiverClient) {
          this._getClient();
        }
      },
    );
  }

  updateClient() {
    /**
     * check invalid client working time
     * return true => invalid
     */
    const isInvalid = Object.values(this.clientInfo.client_workings).some(
      (ClientWorking: ClientWorkingTimeWeekChanging) => {
        return ClientWorking.durations.some((duration: ClientWorkingTime) => {
          if (duration.error) {
            return true;
          }

          return false;
        });
      },
    );

    if (isInvalid) {
      this._toast.error('入力項目に間違いがあります');
      this._cd.detectChanges();
      return;
    }

    this._updateClient(this.clientInfo.formDataClient(), '店舗情報を更新しました', true);
  }

  resetValidator(key: string) {
    if (this.validator[key]) {
      this.validator[key] = undefined;
    }
    this._cd.detectChanges();
  }

  updateImages(images: Image[]) {
    this.clientInfo.images = images;
    this._cd.detectChanges();
  }

  updatePrivateSetting() {
    this._updateClient(this.clientInfo.formDataPrivateSetting(), '個別設定を行いました');
  }

  toogleShowCode() {
    return (this.isShowSecretCode = !this.isShowSecretCode);
  }

  private uploadLogoPrint(file: File) {
    const files = [];

    files.push({
      key: 'image',
      value: file,
      name: 'storeLogo',
    });

    this.isLoadStoreLogo = true;
    this.isRemoveReview = false;

    this._clientSv.updateImage(files, { type: 'logo' }).subscribe(
      (res) => {
        this._toast.success('店舗ロゴを設定しました');
        this.clientInfo.logo = new ClientLogo().deserialize(res);
        this.isLoadStoreLogo = false;
        this._cd.markForCheck();
      },
      (errors) => {
        this.isLoadStoreLogo = false;
        this.isRemoveReview = true;
        this._toast.error(errors);
        this._cd.markForCheck();
      },
    );
  }

  private removeLogoPrint() {
    this._clientSv.removeImage(this.clientInfo.logo.id).subscribe(
      (res: any) => {
        this.clientInfo.logo.url = undefined;
        this._toast.success('店舗ロゴを削除しました');
        this._cd.markForCheck();
      },
      (error) => {
        this._toast.error(error);
      },
    );
  }

  private _openModalWithComponent(comp, initialState?: any) {
    const config: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      initialState: initialState,
      keyboard: false,
      ignoreBackdropClick: true,
    };

    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();
      if (reason === 'reload') {
        this._getClient();
      }
    });

    this.bsModalRef = this.modalSv.show(comp, config);
  }

  /**
   * settingTwilio
   */
  public settingTwilio() {
    this._openModalWithComponent(ModalConfirmTwilioComponent);
  }
}
