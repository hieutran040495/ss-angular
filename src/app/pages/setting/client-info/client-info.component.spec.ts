import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA, ChangeDetectorRef } from '@angular/core';
import { ClientInfoComponent } from './client-info.component';
import { ToastService } from 'shared/logical-services/toast.service';
import { SessionService } from 'shared/states/session';
import { ToastServiceMock } from 'testing/services/toast.service.mock';
import { MockRouter } from 'testing/router.mock';
import { Router, ActivatedRoute } from '@angular/router';
import { of, throwError } from 'rxjs';
import { ClientService } from 'shared/http-services/client.service';
import { CityService } from 'shared/http-services/city.service';
import { PrefecturesService } from 'shared/http-services/prefecture.service';
import { TagService } from 'shared/http-services/tag.service';
import { GenreService } from 'shared/http-services/genre.service';
import { ClientWorkingsService } from 'shared/states/client_workings';
import { ClientProfileService } from 'shared/states/client-profile';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { ValidatorService } from 'shared/utils/validator.service';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonModule } from '@angular/common';
import { UploadFileModule } from 'app/pages/upload-file/upload-file.module';
import { FormsModule } from '@angular/forms';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';
import { TimePickerModule } from 'shared/modules/time-picker/time-picker.module';
import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { KatakanaValidationModule } from 'shared/directive/katakana-validation/katakana-validation.module';
import { UrlValidationModule } from 'shared/directive/url-validation/url-validation.module';
import { PhoneValidationModule } from 'shared/directive/phone-validation/phone-validation.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';
import { CasPreviewModule } from 'app/modules/cas-preview/cas-preview.module';
import { clientData, prefs, cities, genres, tags, currentUser } from 'testing/data/client.mock';
import { By } from '@angular/platform-browser';

fdescribe('ClientInfoComponent', () => {
  let component: ClientInfoComponent;
  let fixture: ComponentFixture<ClientInfoComponent>;
  let toastSv: ToastService;

  const sessionSv = jasmine.createSpyObj('SessionService', ['getUserLoggined']);
  const clientSv = jasmine.createSpyObj('ClientService', [
    'fetchClient',
    'updateClient',
    'updateImage',
    'updatePrivateSetting',
    'settingCancelPolicy',
    'settingChangeFee',
  ]);
  const citySv = jasmine.createSpyObj('CityService', ['fetchCities']);
  const prefecturesSv = jasmine.createSpyObj('PrefecturesService', ['fetchPrefectures']);
  const tagSv = jasmine.createSpyObj('TagService', ['fetchTags']);
  const genreSv = jasmine.createSpyObj('GenreService', ['fetchGenresSelect']);
  const clientWorkingSv = jasmine.createSpyObj('ClientWorkingsService', ['updateWorkingTime']);
  const clientProfileSv = jasmine.createSpyObj('ClientProfileService', ['updateClientProfile']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ClientInfoComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: ClientService, useValue: clientSv },
        { provide: CityService, useValue: citySv },
        { provide: PrefecturesService, useValue: prefecturesSv },
        { provide: TagService, useValue: tagSv },
        { provide: GenreService, useValue: genreSv },
        { provide: ToastService, useClass: ToastServiceMock },
        { provide: SessionService, useValue: sessionSv },
        { provide: ClientWorkingsService, useValue: clientWorkingSv },
        { provide: ClientProfileService, useValue: clientProfileSv },
        { provide: Router, useClass: MockRouter },
        { provide: ActivatedRoute, useValue: { data: of({ clientInfo: clientData }) } },
        ValidatorService,
        EventEmitterService,
        ChangeDetectorRef,
      ],
      imports: [
        RouterTestingModule,
        CommonModule,
        UploadFileModule,
        FormsModule,
        OnlyNumericModule,
        TimePickerModule,
        SpaceValidationModule,
        KatakanaValidationModule,
        UrlValidationModule,
        PhoneValidationModule,
        CasNgSelectModule,
        OnlyNumericModule,
        PhoneValidationModule,
        CasPreviewModule,
      ],
    }).compileComponents();
  }));

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(ClientInfoComponent);
    component = fixture.componentInstance;

    toastSv = TestBed.get(ToastService);

    prefecturesSv.fetchPrefectures.and.returnValue(of({ data: prefs }));
    citySv.fetchCities.and.returnValue(of({ data: cities }));
    genreSv.fetchGenresSelect.and.returnValue(of({ data: genres }));
    tagSv.fetchTags.and.returnValue(of({ data: tags }));
    sessionSv.getUserLoggined.and.returnValue(currentUser);
    fixture.detectChanges();
    tick();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('When component OnInit', () => {
    it('should call prefecturesSv', () => {
      expect(prefecturesSv.fetchPrefectures).toHaveBeenCalled();
    });

    it('should call fetchCities', () => {
      expect(citySv.fetchCities).toHaveBeenCalled();
    });

    it('should call fetchGenresSelect', () => {
      expect(genreSv.fetchGenresSelect).toHaveBeenCalled();
    });

    it('should call fetchTags', () => {
      expect(tagSv.fetchTags).toHaveBeenCalled();
    });

    it('should call getUserLoggined', () => {
      expect(sessionSv.getUserLoggined).toHaveBeenCalled();
    });
  });

  describe('Describe update client', () => {
    let btnUpdatePrivateSetting;
    beforeEach(() => {
      const btn = fixture.debugElement.queryAll(By.css('button.btn-warning'));
      btn.forEach((element) => {
        if (element.nativeElement.textContent.trim() === '個別設定を保存') {
          btnUpdatePrivateSetting = element.nativeElement;
        }
      });
    });

    describe('updatePrivateSetting success', () => {
      beforeEach(fakeAsync(() => {
        clientSv.updatePrivateSetting.and.returnValue(of(true));
        clientProfileSv.updateClientProfile.and.returnValue(of(true));
        btnUpdatePrivateSetting.click();
        tick();
      }));

      it('should call updatePrivateSetting in client service', () => {
        expect(clientSv.updatePrivateSetting).toHaveBeenCalled();
      });

      it('should call notify success', () => {
        expect(toastSv.success).toHaveBeenCalled();
      });

      it('should call updateClientProfile in client profile service', () => {
        expect(clientProfileSv.updateClientProfile).toHaveBeenCalled();
      });
    });

    describe('updatePrivateSetting error', () => {
      beforeEach(fakeAsync(() => {
        clientSv.updatePrivateSetting.and.returnValue(throwError('update fail'));
        btnUpdatePrivateSetting.click();
        tick();
      }));

      it('should call notify error', () => {
        expect(toastSv.error).toHaveBeenCalled();
      });
    });

    describe('settingCancelPolicy sucssess', () => {
      beforeEach(fakeAsync(() => {
        clientSv.settingCancelPolicy.and.returnValue(of(true));
        clientProfileSv.updateClientProfile.and.returnValue(of(true));
        component.settingCancelPolicy();
        tick();
      }));

      it('should call settingCancelPolicy in client service', () => {
        expect(clientSv.settingCancelPolicy).toHaveBeenCalled();
      });

      it('should call notify success', () => {
        expect(toastSv.success).toHaveBeenCalled();
      });

      it('should call updateClientProfile in client profile service', () => {
        expect(clientProfileSv.updateClientProfile.calls.any()).toBe(true, ' Call many time before');
      });
    });

    describe('settingCancelPolicy error', () => {
      beforeEach(fakeAsync(() => {
        clientSv.settingCancelPolicy.and.returnValue(throwError('settingCancelPolicy fail'));
        component.settingCancelPolicy();
        tick();
      }));

      it('should call notify error', () => {
        expect(toastSv.error).toHaveBeenCalled();
      });
    });

    describe('updateClient sucssess', () => {
      beforeEach(fakeAsync(() => {
        clientSv.fetchClient.and.returnValue(of(clientData));
        clientSv.updateClient.and.returnValue(of(true));
        clientProfileSv.updateClientProfile.and.returnValue(of(true));
        component.updateClient();
        tick();
      }));

      it('should call updateClient in client service', () => {
        expect(clientSv.updateClient).toHaveBeenCalled();
      });

      it('should call notify success', () => {
        expect(toastSv.success).toHaveBeenCalled();
      });

      it('should call updateClientProfile in client profile service', () => {
        expect(clientProfileSv.updateClientProfile.calls.any()).toBe(true, ' Call many time before');
      });

      it('should call fetchClient in client profile service', () => {
        expect(clientSv.fetchClient).toHaveBeenCalled();
      });
    });
  });
});
