import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ClientInfoComponent } from './client-info.component';
import { ValidatorService } from 'shared/utils/validator.service';
import { ClientInfoResolve } from './client-info.resolve';
import { TimePickerModule } from 'shared/modules/time-picker/time-picker.module';

import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';
import { KatakanaValidationModule } from 'shared/directive/katakana-validation/katakana-validation.module';
import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { UrlValidationModule } from 'shared/directive/url-validation/url-validation.module';
import { PhoneValidationModule } from 'shared/directive/phone-validation/phone-validation.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';
import { WorkingTimeModule } from 'app/modules/working-time/working-time.module';

import { CasPreviewModule } from 'app/modules/cas-preview/cas-preview.module';
import { CasDropzoneModule } from 'app/modules/cas-dropzone/cas-dropzone.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalConfirmTwilioModule } from 'app/pages/modal-confirm-twilio/modal-confirm-twilio.module';

const routes: Routes = [
  {
    path: '',
    component: ClientInfoComponent,
    resolve: {
      clientInfo: ClientInfoResolve,
    },
  },
];

@NgModule({
  declarations: [ClientInfoComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    OnlyNumericModule,
    TimePickerModule,
    SpaceValidationModule,
    KatakanaValidationModule,
    UrlValidationModule,
    PhoneValidationModule,
    CasNgSelectModule,
    OnlyNumericModule,
    PhoneValidationModule,
    WorkingTimeModule,
    CasPreviewModule,
    CasDropzoneModule,
    ModalModule.forRoot(),
    ModalConfirmTwilioModule,
  ],
  providers: [ClientInfoResolve, ValidatorService],
})
export class ClientInfoModule {}
