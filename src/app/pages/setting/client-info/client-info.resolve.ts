import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { ClientService } from 'shared/http-services/client.service';

@Injectable()
export class ClientInfoResolve implements Resolve<any> {
  constructor(private clientSv: ClientService) {}

  resolve() {
    const opts: any = {
      with: 'tags,genre,images,client_workings,logo,cancel_policy,twilio',
    };

    return this.clientSv.fetchClient(opts);
  }
}
