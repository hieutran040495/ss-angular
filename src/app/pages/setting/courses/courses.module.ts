import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { CourseService } from 'shared/http-services/course.service';
import { CoursesComponent } from './courses.component';
import { CasCardItemModule } from 'app/modules/cas-card-item/cas-card-item.module';
import { FilterMenuModule } from 'app/forms/filter-menu/filter-menu.module';
import { FilterCourseModule } from 'app/forms/filter-course/filter-course.module';
import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';

import { CourseCeModule } from 'app/pages/course-ce/course-ce.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalSettingChargeFeeModule } from 'app/pages/modal-setting-charge-fee/modal-setting-charge-fee.module';

const routes: Routes = [
  {
    path: '',
    component: CoursesComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CasPaginationModule.forRoot(),
    RouterModule.forChild(routes),
    CasCardItemModule,
    FilterMenuModule,
    FilterCourseModule,
    CasFilterModule,
    ModalModule.forRoot(),
    CourseCeModule,
    ModalSettingChargeFeeModule,
  ],
  declarations: [CoursesComponent],
  providers: [CourseService],
})
export class CoursesModule {}
