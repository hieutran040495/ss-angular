import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';

import { CourseService } from 'shared/http-services/course.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { Course } from 'shared/models/course';
import { PageChangedInput, Pagination } from 'shared/models/pagination';
import { ActivatedRoute, Router } from '@angular/router';

import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { DIALOG_EVENT } from 'shared/enums/modes';

import { CourseCeComponent } from 'app/pages/course-ce/course-ce.component';

import { ModalSettingChargeFeeComponent } from 'app/pages/modal-setting-charge-fee/modal-setting-charge-fee.component';
import { Subscription } from 'rxjs';

import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-manage-course',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss'],
})
export class CoursesComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  isPaginate: boolean = false;

  courses: Course[] = [];
  pagination: Pagination = new Pagination().deserialize({ current_page: null, per_page: 20 });

  courseOrder = [
    {
      name: 'コース名',
      value: 'name',
      active: false,
    },
    {
      name: '価格',
      value: 'price',
      active: false,
    },
    {
      name: '表示',
      value: '-is_show',
      active: false,
    },
  ];

  orderCourse: any = {
    order: '-created_at',
  };
  filterCourse: any;
  private subModal: Subscription;

  constructor(
    private courseSv: CourseService,
    private toastrSv: ToastService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private modalSv: BsModalService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.activeRoute.snapshot.queryParams.hasOwnProperty('page')) {
      this.pagination.current_page = this.activeRoute.snapshot.queryParams.page;
    }
    this.fetchCourses();
  }

  ngOnDestroy() {
    if (this.subModal) {
      this.subModal.unsubscribe();
    }
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;

    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.fetchCourses();
    this.appendQueryParams(paginate.page);
  }

  private appendQueryParams(page: number) {
    this.router.navigate([], {
      queryParams: {
        page: page,
      },
      queryParamsHandling: 'merge',
    });
  }

  private fetchCourses() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;

    const opts = {
      ...this.pagination.hasJSON(),
      ...this.orderCourse,
      ...this.filterCourse,
      with: 'items,buffets',
    };
    this.courses = [];
    this._cd.detectChanges();

    this.courseSv.fetchCourses(opts).subscribe(
      (res) => {
        this.courses = res.data;
        this.pagination.deserialize(res);
        this.isLoading = false;
        this.isPaginate = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
        this.isLoading = false;
        this.isPaginate = false;
        this._cd.detectChanges();
      },
    );
  }

  sortCourse(data: any) {
    this.orderCourse = data;
    this.pagination.reset();
    this.fetchCourses();
  }

  courseFilter(data: any) {
    this.filterCourse = data;
    this.pagination.reset();
    this.appendQueryParams(this.pagination.current_page);
    this.fetchCourses();
  }

  openDialogCoures(course?: Course) {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-lg',
      ignoreBackdropClick: true,
      keyboard: false,
    };

    if (course && course.id) {
      this.courseSv.fetchCourseById(course.id, { with: 'items,buffets' }).subscribe(
        (res: Course) => {
          opts.initialState = {
            course: res,
          };
          this.openModalWithComponent(CourseCeComponent, opts);
        },
        (error: any) => {
          this.toastrSv.error(error);
        },
      );
    } else {
      this.openModalWithComponent(CourseCeComponent, opts);
    }
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    this.subModal = this.modalSv.onHidden.subscribe((reason: string) => {
      let currentPage: number = cloneDeep(this.pagination.current_page);

      if (reason === DIALOG_EVENT.COURSE_REMOVE) {
        if (this.pagination.current_page === 1) {
          currentPage = 1;
        } else if (this.courses.length > 1) {
          currentPage = cloneDeep(this.pagination.current_page);
        } else {
          currentPage = cloneDeep(this.pagination.current_page) - 1;
        }
      }

      if (reason === DIALOG_EVENT.COURSE_RELOAD || reason === DIALOG_EVENT.COURSE_REMOVE) {
        this.pagination.reset();
        this._cd.detectChanges();
        this.pageChanged({
          page: currentPage,
          itemsPerPage: this.pagination.per_page,
        });
      }
    });

    this.modalSv.show(comp, opts);
  }

  openModalSettingChargeFee() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-md',
      ignoreBackdropClick: true,
      keyboard: false,
    };

    this.openModalWithComponent(ModalSettingChargeFeeComponent, opts);
  }
}
