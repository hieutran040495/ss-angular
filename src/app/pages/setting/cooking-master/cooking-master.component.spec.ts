import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CookingMasterComponent } from './cooking-master.component';

describe('CookingMasterComponent', () => {
  let component: CookingMasterComponent;
  let fixture: ComponentFixture<CookingMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CookingMasterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CookingMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
