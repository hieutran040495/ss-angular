import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CookingMasterComponent } from './cooking-master.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CasTableModule } from 'app/modules/cas-table/cas-table.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalPreviewMasterCsvModule } from '../../modal-preview-master-csv/modal-preview-master-csv.module';
import { ModalNotifyUploadMasterModule } from '../../modal-notify-upload-master/modal-notify-upload-master.module';

const routes: Routes = [
  {
    path: '',
    component: CookingMasterComponent,
  },
];

@NgModule({
  declarations: [CookingMasterComponent],
  imports: [
    CommonModule,
    FormsModule,
    CasTableModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot(),
    ModalPreviewMasterCsvModule,
    ModalNotifyUploadMasterModule,
  ],
})
export class CookingMasterModule {}
