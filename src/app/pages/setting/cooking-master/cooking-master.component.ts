import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { TableHeader } from 'app/modules/cas-table/cas-table.component';
import { COOKING_MASTER, MASTER_STATUS } from 'shared/enums/cooking-master';
import { ToastService } from 'shared/logical-services/toast.service';
import { CookingMasterService } from 'shared/http-services/cooking-master';
import { ModalOptions, BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalPreviewMasterCsvComponent } from '../../modal-preview-master-csv/modal-preview-master-csv.component';
import { ModalNotifyUploadMasterComponent } from '../../modal-notify-upload-master/modal-notify-upload-master.component';
import { MenuCookingDurationInput, MenuCookingDuration } from 'shared/models/menu-cooking-duration';
import { MenuCookingTypeInput, MenuCookingType } from 'shared/models/menu-cooking-type';
import { MenuSubStatusInput, MenuSubStatus } from 'shared/models/menu-substatus';
import { ApproveMasterData } from 'shared/interfaces/approve-master-data';

@Component({
  selector: 'app-cooking-master',
  templateUrl: './cooking-master.component.html',
  styleUrls: ['./cooking-master.component.scss'],
})
export class CookingMasterComponent implements OnInit {
  @ViewChild('subStatus') uploadSubStatus: ElementRef;
  @ViewChild('cookingType') uploadCookingType: ElementRef;
  @ViewChild('timeLimit') uploadTimeLimit: ElementRef;

  MIME_TYPE = [
    'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'text/csv',
    '.xls',
    '.xlsx',
    '.csv',
  ].join(',');

  tableHeader: Partial<TableHeader>[] = [
    {
      name: '顧客名',
      class: 'text-left',
    },
    {
      name: '',
      width: 200,
      class: 'text-center',
    },
  ];
  COOKING_MASTER = COOKING_MASTER;
  modalRef: BsModalRef;
  isUploading: boolean = false;

  constructor(
    private toastSv: ToastService,
    private cookingMasterSv: CookingMasterService,
    private modalSv: BsModalService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {}

  setting(files: FileList, master_type: string) {
    if (files.length === 0 || this.isUploading) {
      return;
    }

    const file = files[0];
    this._resetFile(master_type);

    if (!file.type || this.MIME_TYPE.indexOf(file.type) < 0) {
      return this.toastSv.error('ファイル形式は.xls,.xlsx,.csvにて\nお願いします');
    }

    const fileData = [
      {
        key: 'file',
        value: file,
      },
    ];

    if (this.isUploading) {
      return;
    }

    this.isUploading = true;
    this._cd.detectChanges();

    switch (master_type) {
      case COOKING_MASTER.SUB_STATUS:
        return this._subStatus(fileData);
      case COOKING_MASTER.COOKING_TYPE:
        return this._cookingType(fileData);
      case COOKING_MASTER.TIME_LIMIT:
        return this._timeLimit(fileData);
    }
  }

  private _resetFile(master_type: string) {
    switch (master_type) {
      case COOKING_MASTER.SUB_STATUS:
        this.uploadSubStatus.nativeElement.value = null;
        break;
      case COOKING_MASTER.COOKING_TYPE:
        this.uploadCookingType.nativeElement.value = null;
        break;
      case COOKING_MASTER.TIME_LIMIT:
        this.uploadTimeLimit.nativeElement.value = null;
        break;
    }
    this._cd.detectChanges();
  }

  private _subStatus(fileData: any) {
    this.cookingMasterSv.importSubStatus(fileData).subscribe(
      (res) => {
        res.data = res.data.map((master: MenuSubStatusInput) => new MenuSubStatus().deserialize(master));
        this._openModalPreviewMaster(res, COOKING_MASTER.SUB_STATUS);
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isUploading = false;
        this._cd.detectChanges();
      },
    );
  }

  private _cookingType(fileData: any) {
    this.cookingMasterSv.importCookingType(fileData).subscribe(
      (res) => {
        res.data = res.data.map((master: MenuCookingTypeInput) => new MenuCookingType().deserialize(master));
        this._openModalPreviewMaster(res, COOKING_MASTER.COOKING_TYPE);
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isUploading = false;
        this._cd.detectChanges();
      },
    );
  }

  private _timeLimit(fileData: any) {
    this.cookingMasterSv.importTimeLimit(fileData).subscribe(
      (res) => {
        res.data = res.data.map((master: MenuCookingDurationInput) => new MenuCookingDuration().deserialize(master));
        this._openModalPreviewMaster(res, COOKING_MASTER.TIME_LIMIT);
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isUploading = false;
        this._cd.detectChanges();
      },
    );
  }

  private _openModalPreviewMaster(masters: any, master_type: string) {
    const approveData: ApproveMasterData = {
      path: masters.path,
      import_at: masters.import_at,
    };

    const data: ModalOptions = {
      class: 'modal-dialog-centered modal-lg',
      initialState: {
        masters: masters.data,
        approveData: approveData,
        master_type: master_type,
      },
      ignoreBackdropClick: true,
      keyboard: false,
    };

    this._openModalWithComponent(ModalPreviewMasterCsvComponent, data);
  }

  private _openModalUploadFail() {
    const data: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
    };
    this._openModalWithComponent(ModalNotifyUploadMasterComponent, data);
  }

  private _openModalWithComponent(comp, opts: ModalOptions) {
    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();
      if (reason === MASTER_STATUS.ERROR) {
        this._openModalUploadFail();
      }
      this.isUploading = false;
      this._cd.detectChanges();
    });
    this.modalRef = this.modalSv.show(comp, opts);
  }
}
