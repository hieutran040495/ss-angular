import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { GourmetSite, GourmetSiteInput } from 'shared/models/gourmet-site';
import { GourmetSiteService } from 'shared/http-services/gourmet-site.service';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { SettingGourmetComponent } from './setting-gourmet/setting-gourmet.component';
import { ToastService } from 'shared/logical-services/toast.service';
import { User } from 'shared/models/user';
import { SessionService } from 'shared/states/session';

@Component({
  selector: 'app-manage-gourmet',
  templateUrl: './manage-gourmet.component.html',
  styleUrls: ['./manage-gourmet.component.scss'],
})
export class ManageGourmetComponent implements OnInit {
  gourmetSite: GourmetSite[] = [];

  tableHeaders: any = [
    {
      name: '設定ステータス',
      class: 'text-center',
      width: 200,
    },
    {
      name: 'グルメサイト名',
      class: 'text-left',
    },
    {
      name: '',
      width: 300,
    },
  ];

  listGourmet = [
    {
      service_type: 't5g',
    },
    {
      service_type: 'r3y',
    },
    {
      service_type: 'h7r',
    },
    {
      service_type: 'g3i',
    },
  ];

  constructor(
    private goutmertSv: GourmetSiteService,
    private bsModalSv: BsModalService,
    private sessionService: SessionService,
    private toastrSv: ToastService,
    private _cd: ChangeDetectorRef,
  ) {}

  isLoading: boolean = false;

  currentUser: User = new User();

  async ngOnInit() {
    this.getGourmet();
    this.currentUser = await this.sessionService.getUserLoggined();
  }

  private getGourmet() {
    if (this.isLoading) return;
    this.isLoading = true;
    this._cd.detectChanges();
    this.goutmertSv.fetchGourmet().subscribe(
      (res: any) => {
        this.gourmetSite = this.listGourmet.map((item: GourmetSiteInput) => {
          const gourmet_Link = res.data.find((gourmet: GourmetSiteInput) => gourmet.service_type === item.service_type);
          if (gourmet_Link) {
            item = gourmet_Link;
          }
          return new GourmetSite().deserialize(item);
        });
        this.isLoading = false;
        this._cd.detectChanges();
      },
      (error) => {
        this.toastrSv.error(error);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  openSetting(gourmet) {
    const modalOpts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      initialState: {
        gourmet: gourmet,
        currentUser: this.currentUser,
      },
      keyboard: false,
      ignoreBackdropClick: true,
    };
    this.openModalWithComponent(SettingGourmetComponent, modalOpts);
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    this.bsModalSv.onHidden.subscribe((reason: string) => {
      if (reason === 'reload') {
        this.getGourmet();
      }
    });
    this.bsModalSv.show(comp, opts);
  }
}
