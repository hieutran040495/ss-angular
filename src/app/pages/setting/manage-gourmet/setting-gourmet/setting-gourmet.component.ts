import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { GourmetSiteService } from 'shared/http-services/gourmet-site.service';
import { ValidatorService } from 'shared/utils/validator.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { GourmetSite } from 'shared/models/gourmet-site';
import { User } from 'shared/models/user';
import { DestroyGourmetComponent } from '../destroy-gourmet/destroy-gourmet.component';

@Component({
  selector: 'app-setting-gourmet',
  templateUrl: './setting-gourmet.component.html',
  styleUrls: ['./setting-gourmet.component.scss'],
})
export class SettingGourmetComponent implements OnInit {
  isSetting: boolean = false;
  isLoading: boolean = false;

  gourmet: GourmetSite = new GourmetSite();

  currentUser: User = new User();

  password_user: string = '';

  validation: any = {};

  get title_modal(): string {
    return `${this.gourmet.service_name}設定`;
  }

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private goutmertSv: GourmetSiteService,
    private validatorSv: ValidatorService,
    private toastrSv: ToastService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {}

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  checkPassword() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this._cd.detectChanges();
    this.goutmertSv.authenticateUser({ password: this.password_user }).subscribe(
      (res) => {
        this.isLoading = false;
        this.isSetting = true;
        this.gourmet.code = res.code;
        this._cd.detectChanges();
      },
      (error) => {
        this.isLoading = false;
        if (error.hasOwnProperty('errors')) {
          this.validation = this.validatorSv.setErrors(error.errors);
        }
        this.toastrSv.error(error);
        this._cd.detectChanges();
      },
    );
  }

  submitGourmet() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this._cd.detectChanges();

    this.goutmertSv.linkGourmet(this.gourmet).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastrSv.success('連携に成功しました');
        this.closeDialog('reload');
        this._cd.detectChanges();
      },
      (error) => {
        this.isLoading = false;
        if (error.hasOwnProperty('errors')) {
          this.validation = this.validatorSv.setErrors(error.errors);
        }
        this.toastrSv.error(error);
        this._cd.detectChanges();
      },
    );
  }

  destroyGourmet() {
    const modalOpts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      initialState: {
        gourmet: this.gourmet,
      },
      keyboard: false,
      ignoreBackdropClick: true,
    };
    this.openModalWithComponent(DestroyGourmetComponent, modalOpts);
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    this.bsModalSv.onHidden.subscribe((reason: string) => {
      if (reason === 'reload') {
        this.closeDialog('reload');
      }
    });

    this.bsModalSv.show(comp, opts);
  }
}
