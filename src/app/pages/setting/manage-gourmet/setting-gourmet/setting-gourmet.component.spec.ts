import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingGourmetComponent } from './setting-gourmet.component';

describe('SettingGourmetComponent', () => {
  let component: SettingGourmetComponent;
  let fixture: ComponentFixture<SettingGourmetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SettingGourmetComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingGourmetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
