import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingGourmetComponent } from './setting-gourmet.component';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { FormsModule } from '@angular/forms';
import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { ValidatorService } from 'shared/utils/validator.service';
import { DestroyGourmetModule } from '../destroy-gourmet/destroy-gourmet.module';

@NgModule({
  declarations: [SettingGourmetComponent],
  imports: [CommonModule, FormsModule, SpaceValidationModule, CasDialogModule, DestroyGourmetModule],
  providers: [ValidatorService],
  exports: [SettingGourmetComponent],
  entryComponents: [SettingGourmetComponent],
})
export class SettingGourmetModule {}
