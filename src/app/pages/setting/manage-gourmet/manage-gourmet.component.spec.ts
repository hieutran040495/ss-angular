import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageGourmetComponent } from './manage-gourmet.component';

describe('ManageGourmetComponent', () => {
  let component: ManageGourmetComponent;
  let fixture: ComponentFixture<ManageGourmetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManageGourmetComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageGourmetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
