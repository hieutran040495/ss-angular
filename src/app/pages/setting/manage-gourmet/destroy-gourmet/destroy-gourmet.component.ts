import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { GourmetSite } from 'shared/models/gourmet-site';
import { GourmetSiteService } from 'shared/http-services/gourmet-site.service';
import { ToastService } from 'shared/logical-services/toast.service';

@Component({
  selector: 'app-destroy-gourmet',
  templateUrl: './destroy-gourmet.component.html',
  styleUrls: ['./destroy-gourmet.component.scss'],
})
export class DestroyGourmetComponent implements OnInit {
  gourmet: GourmetSite = new GourmetSite();
  isLoading: boolean = false;

  get title_modal(): string {
    return `${this.gourmet.service_name}連携解除`;
  }

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private goutmertSv: GourmetSiteService,
    private toastrSv: ToastService,
  ) {}

  ngOnInit() {}

  deleteLink() {
    if (this.isLoading) return;

    this.isLoading = true;

    this.goutmertSv.deleteLink(this.gourmet.id).subscribe(
      (res: any) => {
        this.toastrSv.success('連携解除に成功しました');
        this.isLoading = false;
        this.closeDialog('reload');
      },
      (errors: any) => {
        this.isLoading = false;
        this.toastrSv.error(errors);
        this.closeDialog();
      },
    );
  }
  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
