import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestroyGourmetComponent } from './destroy-gourmet.component';

describe('DestroyGourmetComponent', () => {
  let component: DestroyGourmetComponent;
  let fixture: ComponentFixture<DestroyGourmetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DestroyGourmetComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestroyGourmetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
