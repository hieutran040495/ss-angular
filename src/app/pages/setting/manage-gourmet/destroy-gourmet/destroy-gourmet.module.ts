import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DestroyGourmetComponent } from './destroy-gourmet.component';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

@NgModule({
  declarations: [DestroyGourmetComponent],
  imports: [CommonModule, CasDialogModule],
  exports: [DestroyGourmetComponent],
  entryComponents: [DestroyGourmetComponent],
})
export class DestroyGourmetModule {}
