import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageGourmetComponent } from './manage-gourmet.component';
import { Routes, RouterModule } from '@angular/router';
import { CasTableModule } from 'app/modules/cas-table/cas-table.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SettingGourmetModule } from './setting-gourmet/setting-gourmet.module';

const routes: Routes = [
  {
    path: '',
    component: ManageGourmetComponent,
  },
];

@NgModule({
  declarations: [ManageGourmetComponent],
  imports: [CommonModule, RouterModule.forChild(routes), ModalModule.forRoot(), CasTableModule, SettingGourmetModule],
})
export class ManageGourmetModule {}
