import { Component, OnInit, OnDestroy, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { ModalCeMenuOptionComponent } from './modal-ce/modal-ce.component';
import { ModalDeleteMenuOptionComponent } from './modal-delete/modal-delete.component';
import { MenuOption } from 'shared/models/menu-option';

import { Pagination, PageChangedInput } from 'shared/models/pagination';
import { ToastService } from 'shared/logical-services/toast.service';
import { MenuOptionService } from 'shared/http-services/menu-option.service';
import { DIALOG_EVENT } from 'shared/enums/modes';

import * as cloneDeep from 'lodash/clone';

@Component({
  selector: 'app-menu-options',
  templateUrl: './menu-options.component.html',
  styleUrls: ['./menu-options.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuOptionsComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  isPaginate: boolean = false;

  menuOptions: MenuOption[] = [];
  pagination: Pagination = new Pagination();

  optionSort = [
    {
      name: 'オプション名',
      value: 'name',
      active: false,
    },
    {
      name: '価格',
      value: 'price',
      active: false,
    },
  ];

  private orderOptions;
  private filterOptions;

  private modalRef: BsModalRef;

  constructor(
    private modalSv: BsModalService,
    private menuOptionSv: MenuOptionService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private toastSv: ToastService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.activeRoute.snapshot.queryParams.hasOwnProperty('page')) {
      this.pagination.current_page = +this.activeRoute.snapshot.queryParams.page;
    }
    this.getMenuOptions();
  }

  ngOnDestroy() {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;
    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.getMenuOptions();
    this.appendQueryParams(paginate.page);
  }

  private appendQueryParams(page) {
    this.router.navigate([], {
      queryParams: {
        page: page,
      },
      queryParamsHandling: 'merge',
    });
  }

  private getMenuOptions() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.menuOptions = [];
    this._cd.detectChanges();

    const opts = {
      ...this.pagination.hasJSON(),
      ...this.orderOptions,
      ...this.filterOptions,
    };

    this.menuOptionSv.getListMenuOptions(opts).subscribe(
      (res) => {
        this.menuOptions = res.data;
        this.pagination.deserialize(res);
        this.isLoading = false;
        this.isPaginate = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this.isPaginate = false;
        this._cd.detectChanges();
      },
    );
  }

  openModalCEMenuOption(menuOption?: MenuOption) {
    const data: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      initialState: {
        menuOption: menuOption ? cloneDeep(menuOption) : new MenuOption(),
      },
      ignoreBackdropClick: true,
      keyboard: false,
    };
    this.openModalWithComponent(ModalCeMenuOptionComponent, data);
  }

  openModalRemoveMenuOption(menuOption: MenuOption) {
    const data: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      initialState: {
        menuOption: cloneDeep(menuOption),
      },
      ignoreBackdropClick: true,
      keyboard: false,
    };
    this.openModalWithComponent(ModalDeleteMenuOptionComponent, data);
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();

      let currentPage: number = cloneDeep(this.pagination.current_page);

      if (reason === DIALOG_EVENT.MENU_OPTION_REMOVE) {
        if (this.pagination.current_page === 1) {
          currentPage = 1;
        } else if (this.menuOptions.length > 1) {
          currentPage = cloneDeep(this.pagination.current_page);
        } else {
          currentPage = cloneDeep(this.pagination.current_page) - 1;
        }
      }

      if (reason === DIALOG_EVENT.MENU_OPTION_RELOAD || reason === DIALOG_EVENT.MENU_OPTION_REMOVE) {
        this.pagination.reset();
        this._cd.detectChanges();
        this.pageChanged({
          page: currentPage,
          itemsPerPage: this.pagination.per_page,
        });
      }
    });

    this.modalRef = this.modalSv.show(comp, opts);
  }

  sortMenuOption(data: any) {
    this.orderOptions = data;
    this.pagination.reset();
    this.getMenuOptions();
  }

  filterMenuOption(data: any) {
    this.filterOptions = data;
    this.pagination.reset();
    this.appendQueryParams(this.pagination.current_page);
    this.getMenuOptions();
  }
}
