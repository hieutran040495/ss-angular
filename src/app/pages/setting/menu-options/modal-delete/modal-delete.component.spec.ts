import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDeleteMenuOptionComponent } from './modal-delete.component';

describe('ModalDeleteMenuOptionComponent', () => {
  let component: ModalDeleteMenuOptionComponent;
  let fixture: ComponentFixture<ModalDeleteMenuOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalDeleteMenuOptionComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDeleteMenuOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
