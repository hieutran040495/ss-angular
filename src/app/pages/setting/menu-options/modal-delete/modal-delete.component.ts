import { Component, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToastService } from 'shared/logical-services/toast.service';
import { MenuOption } from 'shared/models/menu-option';
import { MenuOptionService } from 'shared/http-services/menu-option.service';
import { DIALOG_EVENT } from 'shared/enums/modes';

@Component({
  selector: 'app-menu-option-modal-delete',
  templateUrl: './modal-delete.component.html',
  styleUrls: ['./modal-delete.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModalDeleteMenuOptionComponent {
  menuOption: MenuOption;
  isSubmiting: boolean = false;

  constructor(
    private bsModalRef: BsModalRef,
    private modalSv: BsModalService,
    private menuOptionSv: MenuOptionService,
    private toastSv: ToastService,
    private _cd: ChangeDetectorRef,
  ) {}

  removeCategory() {
    if (this.isSubmiting) {
      return;
    }

    this.isSubmiting = true;

    this.menuOptionSv.deleteMenuOption(this.menuOption.id).subscribe(
      (res) => {
        this.toastSv.success('オプションを削除しました');
        this.closeModal(DIALOG_EVENT.MENU_OPTION_REMOVE);
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
      (error) => {
        this.toastSv.error(error);
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
    );
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
