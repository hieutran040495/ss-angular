import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

import { ValidatorService } from 'shared/utils/validator.service';

import { ModalDeleteMenuOptionComponent } from './modal-delete.component';

@NgModule({
  declarations: [ModalDeleteMenuOptionComponent],
  imports: [CommonModule, FormsModule, SpaceValidationModule, CasDialogModule],
  providers: [ValidatorService],
  entryComponents: [ModalDeleteMenuOptionComponent],
  exports: [ModalDeleteMenuOptionComponent],
})
export class ModalDeleteMenuOptionModule {}
