import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ValidatorService } from 'shared/utils/validator.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { Mode } from 'shared/utils/mode';
import { MenuOption } from 'shared/models/menu-option';
import { MenuOptionService } from 'shared/http-services/menu-option.service';
import { DIALOG_EVENT } from 'shared/enums/modes';

@Component({
  selector: 'app-menu-option-modal-ce',
  templateUrl: './modal-ce.component.html',
  styleUrls: ['./modal-ce.component.scss'],
})
export class ModalCeMenuOptionComponent implements OnInit {
  mode: Mode = new Mode();
  menuOption: MenuOption;

  isSubmiting: boolean = false;
  validator: any = {};

  get title_modal(): string {
    if (this.mode.isEdit) {
      return 'オプション編集';
    }
    return 'オプション新規作成';
  }

  constructor(
    private menuOptionSv: MenuOptionService,
    private modalSv: BsModalService,
    private validatorSv: ValidatorService,
    private bsModalRef: BsModalRef,
    private toastSv: ToastService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.menuOption.id) {
      this.mode.setEdit();
    } else {
      this.mode.setNew();
    }
  }

  createMenuOption() {
    if (this.isSubmiting) {
      return;
    }

    this.isSubmiting = true;

    this.menuOptionSv.createMenuOption(this.menuOption.formDataString()).subscribe(
      (res) => {
        this.toastSv.success('新規オプションを作成しました');
        this.closeModal(DIALOG_EVENT.MENU_OPTION_RELOAD);
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
      (error) => {
        if (error.hasOwnProperty('errors')) {
          this.validator = this.validatorSv.setErrors(error.errors);
        }
        this.toastSv.error(error);
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
    );
  }

  editMenuOption() {
    if (this.isSubmiting) {
      return;
    }

    this.isSubmiting = true;

    this.menuOptionSv.editMenuOption(this.menuOption.formDataString()).subscribe(
      (res) => {
        this.toastSv.success('オプションを更新しました');
        this.closeModal(DIALOG_EVENT.MENU_OPTION_RELOAD);
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
      (error) => {
        if (error.hasOwnProperty('errors')) {
          this.validator = this.validatorSv.setErrors(error.errors);
        }
        this.toastSv.error(error);
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
    );
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  validatorReset() {
    this.validator = this.validatorSv.resetErrors();
    this._cd.detectChanges();
  }
}
