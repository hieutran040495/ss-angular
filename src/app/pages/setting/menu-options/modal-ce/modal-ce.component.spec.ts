import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCeMenuOptionComponent } from './modal-ce.component';

describe('ModalCeMenuOptionComponent', () => {
  let component: ModalCeMenuOptionComponent;
  let fixture: ComponentFixture<ModalCeMenuOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalCeMenuOptionComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCeMenuOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
