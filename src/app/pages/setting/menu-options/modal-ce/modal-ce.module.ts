import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalCeMenuOptionComponent } from './modal-ce.component';
import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { DigitFormaterModule } from 'shared/modules/digit-formater/digit-formater.module';
import { UiSwitchModule } from 'shared/modules/ui-switch/index';

@NgModule({
  declarations: [ModalCeMenuOptionComponent],
  imports: [
    CommonModule,
    FormsModule,
    SpaceValidationModule,
    CasDialogModule,
    DigitFormaterModule,
    FormsModule,
    UiSwitchModule.forRoot({
      size: 'small',
      bgColor: '#c3ed12',
    }),
  ],
  entryComponents: [ModalCeMenuOptionComponent],
  exports: [ModalCeMenuOptionComponent],
})
export class ModalCeMenuOptionModule {}
