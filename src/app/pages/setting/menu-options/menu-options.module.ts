import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';

import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

import { ValidatorService } from 'shared/utils/validator.service';

import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { CasMaxlengthModule } from 'shared/directive/cas-maxlength/cas-maxlength.module';
import { MenuOptionsComponent } from './menu-options.component';

import { ModalCeMenuOptionModule } from './modal-ce/modal-ce.module';
import { ModalDeleteMenuOptionModule } from './modal-delete/modal-delete.module';
import { CasCardOptionsModule } from 'app/modules/cas-card-options/cas-card-options.module';
import { FilterOptionsModule } from 'app/forms/filter-options/filter-options.module';

const routes: Routes = [
  {
    path: '',
    component: MenuOptionsComponent,
  },
];

@NgModule({
  declarations: [MenuOptionsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot(),
    FormsModule,
    CasPaginationModule.forRoot(),
    SpaceValidationModule,
    CasMaxlengthModule,
    CasFilterModule,
    CasCardOptionsModule,
    CasNgSelectModule,
    CasDialogModule,
    FilterOptionsModule,
    ModalCeMenuOptionModule,
    ModalDeleteMenuOptionModule,
  ],
  providers: [ValidatorService],
})
export class MenuOptionsModule {}
