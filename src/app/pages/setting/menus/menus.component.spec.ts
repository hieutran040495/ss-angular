import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

import { MenusComponent } from './menus.component';
import { ToastService } from 'shared/logical-services/toast.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { FilterMenuModule } from 'app/forms/filter-menu/filter-menu.module';
import { CasCardItemModule } from 'app/modules/cas-card-item/cas-card-item.module';
import { MenuCeModule } from 'app/pages/menu-ce/menu-ce.module';
import { ModalSettingChargeFeeModule } from 'app/pages/modal-setting-charge-fee/modal-setting-charge-fee.module';

import { ToastServiceMock } from 'testing/services/toast.service.mock';
import { MenuService } from 'shared/http-services/menu.service';
import { ClientService } from 'shared/http-services/client.service';
import { menus } from 'testing/data/menu.mock';
import { asyncData } from 'testing/utils/async-data.mock';
import { of, throwError } from 'rxjs';

fdescribe('MenusComponent', () => {
  let component: MenusComponent;
  let fixture: ComponentFixture<MenusComponent>;
  let toastSv: ToastService;

  const menuSv = jasmine.createSpyObj('MenuService', ['fetchMenus']);
  const clientSv = jasmine.createSpyObj('ClientService', ['fetchClient']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MenusComponent],
      imports: [
        CommonModule,
        RouterTestingModule,
        FormsModule,
        CasPaginationModule.forRoot(),
        ModalModule.forRoot(),
        CasFilterModule,
        FilterMenuModule,
        CasCardItemModule,
        MenuCeModule,
        ModalSettingChargeFeeModule,
        HttpClientModule,
      ],
      providers: [
        { provide: ToastService, useClass: ToastServiceMock },
        { provide: MenuService, useValue: menuSv },
        { provide: ClientService, useValue: clientSv },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenusComponent);
    component = fixture.componentInstance;
    toastSv = TestBed.get(ToastService);
    menuSv.fetchMenus.and.returnValue(asyncData({ data: menus }));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call fetchMenus from menuSv', () => {
    expect(menuSv.fetchMenus.calls.any()).toBe(true, 'fetch menu list');
  });

  describe('Fetch List of Menu Success', () => {
    const fetchMenuSpy = menuSv.fetchMenus.and.returnValue(of({ sussess: true }));

    beforeEach(() => {
      component.menus = [];
    });

    it('isLoading should be true', () => {
      expect(component.isLoading).toBeTruthy();
    });

    it('should call function fetch menu', () => {
      expect(fetchMenuSpy.calls.any()).toBe(true, 'call fetch menu function of menu service');
    });
  });

  describe('Fetch List of Menu Error', () => {
    beforeEach(() => {
      menuSv.fetchMenus.and.returnValue(throwError('fetch menus failled'));
      component.menus = [];
      component.fetchMenu();
    });

    it('isLoading should be false', () => {
      expect(component.isLoading).toBeFalsy();
    });

    it('should call notify error', () => {
      expect(toastSv.error).toHaveBeenCalled();
    });
  });

  describe('Sort Menus Function', () => {
    beforeEach(() => {
      component.sortMenu({});
    });

    it('should call function fetch menu', () => {
      expect(menuSv.fetchMenus).toHaveBeenCalled();
    });
  });

  describe('Filter Menus Function', () => {
    beforeEach(() => {
      component.menuFilter({});
    });

    it('should call function fetch menu', () => {
      expect(menuSv.fetchMenus).toHaveBeenCalled();
    });
  });
});
