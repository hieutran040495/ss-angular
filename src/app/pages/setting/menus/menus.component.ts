import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { DIALOG_EVENT } from 'shared/enums/modes';
import { SWAP_TYPE } from 'shared/enums/swap';

import { Menu } from 'shared/models/menu';
import { Image } from 'shared/models/image';
import { Pagination, PageChangedInput } from 'shared/models/pagination';

import { MenuService } from 'shared/http-services/menu.service';
import { ClientService } from 'shared/http-services/client.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { MenuCeComponent } from 'app/pages/menu-ce/menu-ce.component';
import { ModalSettingChargeFeeComponent } from 'app/pages/modal-setting-charge-fee/modal-setting-charge-fee.component';
import { DialogSettingSoldOutComponent } from 'app/modules/dialog-setting-sold-out/dialog-setting-sold-out.component';

import { Subscription } from 'rxjs';
import * as cloneDeep from 'lodash/cloneDeep';
import { DialogSettingLanguageComponent } from 'app/modules/dialog-setting-language/dialog-setting-language.component';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenusComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  isPaginate: boolean = false;

  menus: Menu[] = [];
  pagination: Pagination = new Pagination();

  menuOrder = [
    {
      name: 'メニュー名',
      value: 'ordinal_number,name',
      active: false,
    },
    {
      name: 'カテゴリー名',
      value: 'item_category.name',
      active: false,
    },
    {
      name: '価格',
      value: 'price',
      active: false,
    },
    {
      name: '表示',
      value: '-is_show',
      active: false,
    },
  ];

  orderMenu = {
    order: 'ordinal_number',
  };
  filterMenu;

  isSwap: boolean = false;
  id: number;
  SWAP_TYPE = SWAP_TYPE;
  isResetSwap: boolean = false;
  private subModal: Subscription;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private modalSv: BsModalService,
    private menuSv: MenuService,
    private toastrSv: ToastService,
    private _cd: ChangeDetectorRef,
    private clientSv: ClientService,
  ) {
    if (this.activeRoute.snapshot.queryParams.hasOwnProperty('page')) {
      this.pagination.current_page = +this.activeRoute.snapshot.queryParams.page;
    }
  }

  ngOnInit() {
    this.fetchMenu();
  }

  ngOnDestroy(): void {
    if (this.subModal) {
      this.subModal.unsubscribe();
    }
  }

  handleSwap() {
    this.isSwap = !this.isSwap;
    if (this.isSwap) {
      this.fetchMenu(true);
    } else {
      this.fetchMenu();
    }
  }

  selectItemSwap(id: number) {
    if (!this.isSwap) return;
    this.id = id;

    setTimeout(() => {
      this.id = undefined;
    }, 0);
  }

  menuSwapped(e) {
    this.menus = e;
    this.id = undefined;
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;

    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.fetchMenu();
    this.appendQueryParams(paginate.page);
  }

  private appendQueryParams(page) {
    this.router.navigate([], {
      queryParams: {
        page: page,
      },
      queryParamsHandling: 'merge',
    });
  }

  fetchMenu(isSwap: boolean = false) {
    this.isLoading = true;

    let opts: any = {
      ...this.pagination.hasJSON(),
      ...this.filterMenu,
      ...this.orderMenu,
      with: 'item_category',
    };

    if (isSwap) {
      opts = {
        ...this.filterMenu,
        order: 'ordinal_number',
        with: 'item_category',
      };
    }

    this.menus = [];

    this.menuSv.fetchMenus(opts).subscribe(
      (res) => {
        this.pagination.deserialize(res);
        this.menus = res.data;
        this.isLoading = false;
        this.isPaginate = false;
        this.isResetSwap = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
        this.isLoading = false;
        this.isPaginate = false;
        this.isResetSwap = false;
        this._cd.detectChanges();
      },
    );
  }

  sortMenu(data: any) {
    this.orderMenu = data;
    this.pagination = new Pagination();
    this._cd.detectChanges();
    this.appendQueryParams(this.pagination.current_page);
    this.fetchMenu();
  }

  menuFilter(data: any) {
    this.filterMenu = data;
    if (this.isSwap) {
      this.isResetSwap = true;
      this.fetchMenu(true);
    } else {
      this.pagination = new Pagination();
      this._cd.detectChanges();
      this.appendQueryParams(this.pagination.current_page);
      this.fetchMenu();
    }
  }

  openModalSettingChargeFee() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-md',
      ignoreBackdropClick: true,
      keyboard: false,
    };

    this.clientSv.fetchClient().subscribe((res) => {
      opts.initialState = {
        clientChargeFee: res,
      };
      this.openModalWithComponent(ModalSettingChargeFeeComponent, opts);
    });
  }

  openDialogMenu(menu?: Menu) {
    if (this.isSwap) return;

    if (menu && menu.id) {
      const opts = {
        with: 'item_category.item_type,item_category,menu_options,cooking_type,cooking_duration,sub_statuses',
      };

      this.menuSv.fetchMenuById(menu.id, opts).subscribe(
        (res: any) => {
          this.dialogMenu(res);
        },
        (errors: any) => {
          this.toastrSv.error(errors);
        },
      );
    } else {
      this.dialogMenu();
    }
  }

  private dialogMenu(menuData?: Menu) {
    const modalOpts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-lg',
      ignoreBackdropClick: true,
      keyboard: false,
    };

    if (menuData) {
      modalOpts.initialState = {
        menu: menuData,
      };
    }

    this.openModalWithComponent(MenuCeComponent, modalOpts);
  }

  dialogSettingSoulOut() {
    this.clientSv.getSoldOut().subscribe(
      (res: Image) => {
        const opts: ModalOptions = {
          class: 'modal-dialog-centered modal-dialog-absolute-centered',
          ignoreBackdropClick: true,
          keyboard: false,
          initialState: {
            soldOutImg: res.url,
          },
        };

        this.openModalWithComponent(DialogSettingSoldOutComponent, opts);
      },
      (errors: any) => {
        this.toastrSv.error(errors);
      },
    );
  }

  public dialogSettingClientLanguage() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered',
      ignoreBackdropClick: true,
      keyboard: false,
    };

    this.openModalWithComponent(DialogSettingLanguageComponent, opts);
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    this.subModal = this.modalSv.onHidden.subscribe((reason: string) => {
      let currentPage: number = cloneDeep(this.pagination.current_page);

      if (reason === DIALOG_EVENT.MENU_REMOVE) {
        if (this.pagination.current_page === 1) {
          currentPage = 1;
        } else if (this.menus.length > 1) {
          currentPage = cloneDeep(this.pagination.current_page);
        } else {
          currentPage = cloneDeep(this.pagination.current_page) - 1;
        }
      }

      if (reason === DIALOG_EVENT.MENU_RELOAD || reason === DIALOG_EVENT.MENU_REMOVE) {
        this.pagination.reset();
        this._cd.detectChanges();
        this.pageChanged({
          page: currentPage,
          itemsPerPage: this.pagination.per_page,
        });
      }
    });

    this.modalSv.show(comp, opts);
  }
}
