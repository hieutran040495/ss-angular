import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ModalModule } from 'ngx-bootstrap/modal';

import { FilterMenuModule } from 'app/forms/filter-menu/filter-menu.module';
import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { CasCardItemModule } from 'app/modules/cas-card-item/cas-card-item.module';

import { MenuCeModule } from 'app/pages/menu-ce/menu-ce.module';

import { MenusComponent } from './menus.component';
import { ModalSettingChargeFeeModule } from 'app/pages/modal-setting-charge-fee/modal-setting-charge-fee.module';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { CasSwapModule } from 'app/modules/cas-swap/cas-swap.module';
import { DialogSettingSoldOutModule } from 'app/modules/dialog-setting-sold-out/dialog-setting-sold-out.module';
import { DialogSettingLanguageModule } from 'app/modules/dialog-setting-language/dialog-setting-language.module';

const routes: Routes = [
  {
    path: '',
    component: MenusComponent,
  },
];

@NgModule({
  declarations: [MenusComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    CasPaginationModule.forRoot(),
    ModalModule.forRoot(),
    CasFilterModule,
    FilterMenuModule,
    CasCardItemModule,
    MenuCeModule,
    ModalSettingChargeFeeModule,
    CasSwapModule,
    DialogSettingSoldOutModule,
    DialogSettingLanguageModule,
  ],
})
export class MenusModule {}
