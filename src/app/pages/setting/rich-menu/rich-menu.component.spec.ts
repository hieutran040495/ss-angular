import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RichMenuComponent } from './rich-menu.component';

describe('RichMenuComponent', () => {
  let component: RichMenuComponent;
  let fixture: ComponentFixture<RichMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RichMenuComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RichMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
