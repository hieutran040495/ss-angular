import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { Pagination, PageChangedInput } from 'shared/models/pagination';

import { SETTING_RICH_MENU } from 'shared/enums/event-emitter';
import { APP_SETTING } from 'shared/enums/rich-menu';

import { User } from 'shared/models/user';
import { ItemCategory } from 'shared/models/item-category';
import { RichMenu } from 'shared/models/rich-menu';

import { RichMenuService } from 'shared/http-services/rich-menu.service';
import { ItemCategoryService } from 'shared/http-services/item-category.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { TopScreenService } from 'shared/http-services/top-screen.service';
import { SessionService } from 'shared/states/session';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';

import { ModalSettingOrderComponent } from 'app/pages/modal-setting-order/modal-setting-order.component';
import { ModalSettingOrderTopScreenComponent } from 'app/pages/modal-setting-order-top-screen/modal-setting-order-top-screen.component';
import { DialogRichMenuComponent } from 'app/modules/dialog-rich-menu/dialog-rich-menu.component';

import { Subscription } from 'rxjs';

import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-rich-menu',
  templateUrl: './rich-menu.component.html',
  styleUrls: ['./rich-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RichMenuComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  isLoadingSettingOrder: boolean = false;
  isPaginate: boolean = false;

  richMenus: RichMenu[] = [];
  pagination: Pagination = new Pagination();

  richMenuSort = [
    {
      name: '画面名',
      value: 'name',
      active: false,
    },
    {
      name: 'ステータス',
      value: '-has_setting',
      active: false,
    },
  ];

  orderRichMenu = {
    order: '-top_screen,-recommend,-is_take_out,orinal_number',
  };
  filterRichMenu;

  private eventSubscribe: Subscription;
  isSelfOrder: boolean = true;

  currentUser: User = new User();

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private bsModalSv: BsModalService,
    private richMenuSv: RichMenuService,
    private eventEmitterSv: EventEmitterService,
    private itemCategorySv: ItemCategoryService,
    private sessionService: SessionService,
    private topImageSv: TopScreenService,
    private toastSv: ToastService,
    private cdr: ChangeDetectorRef,
  ) {}

  async ngOnInit() {
    this.currentUser = await this.sessionService.getUserLoggined();
    if (this.currentUser.client) {
      this.isSelfOrder = this.currentUser.client.use_self_order;
    }

    if (this.activeRoute.snapshot.queryParams.hasOwnProperty('page')) {
      this.pagination.current_page = this.activeRoute.snapshot.queryParams.page;
    }

    this.getRichMenus();

    this.eventSubscribe = this.eventEmitterSv.caseNumber$.subscribe((res: any) => {
      if (!res.type) return;

      switch (res.type) {
        case SETTING_RICH_MENU.RELOAD_RICH_MENU:
          this.pagination.reset();
          this.getRichMenus();
      }
    });
  }

  ngOnDestroy(): void {
    this.eventSubscribe.unsubscribe();
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;
    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.getRichMenus();
    this.appendQueryParams(paginate.page);
  }

  private appendQueryParams(page) {
    this.router.navigate([], {
      queryParams: {
        page: page,
      },
      queryParamsHandling: 'merge',
    });
  }

  private getRichMenus() {
    const opts: any = {
      ...this.pagination.hasJSON(),
      ...this.orderRichMenu,
      ...this.filterRichMenu,
    };

    this.isLoading = true;

    this.richMenuSv.getRichMenus(opts).subscribe(
      (res: any) => {
        this.richMenus = res.data;
        this.pagination.deserialize(res);
        this.isLoading = false;
        this.isPaginate = false;
        this.cdr.markForCheck();
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this.isPaginate = false;
        this.cdr.markForCheck();
      },
    );
  }

  eventSortRichMenu(data: any) {
    this.orderRichMenu = data;
    this.pagination.reset();
    this.getRichMenus();
  }

  eventFilterRichMenu(data: any) {
    this.filterRichMenu = data;
    this.pagination.reset();
    this.getRichMenus();
  }

  openSettingModal(richMenu: RichMenu) {
    if (!this.isSelfOrder) {
      if (richMenu.top_screen) {
        // this._getTopScreenById(richMenu);
      } else {
        this._getSettingRecommendScreen(richMenu);
      }
      return;
    }

    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-dialog-large-border modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        richMenu: richMenu,
        currentUser: this.currentUser,
      },
    };
    this.openModalWithComponent(ModalSettingOrderComponent, opts);
  }

  private _getTopScreenById(richMenu: RichMenu) {
    if (this.isLoadingSettingOrder) {
      return;
    }
    this.isLoadingSettingOrder = true;

    const topScreenOpts: any = {
      with: APP_SETTING.ORDER_SETTING,
    };

    this.topImageSv.getDetailWithId(richMenu.id, topScreenOpts).subscribe(
      (res: any) => {
        const opts: ModalOptions = {
          class: 'modal-dialog-centered modal-dialog-absolute-centered modal-lg',
          ignoreBackdropClick: true,
          keyboard: false,
          initialState: {
            topScreen: res,
          },
        };

        this.openModalWithComponent(ModalSettingOrderTopScreenComponent, opts);
        this.isLoadingSettingOrder = false;
        this.cdr.markForCheck();
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoadingSettingOrder = false;
        this.cdr.markForCheck();
      },
    );
  }

  private _getSettingRecommendScreen(richMenu: RichMenu) {
    if (this.isLoadingSettingOrder) {
      return;
    }
    this.isLoadingSettingOrder = true;

    const opts: any = {
      with: 'order_rich_menu_settings,order_rich_menu_settings.items',
    };

    this.itemCategorySv.getSettingRecommendScreen(richMenu.id, 'order', opts).subscribe(
      (res: any) => {
        this.isLoadingSettingOrder = false;
        this._openModalRecommendScreen(res, richMenu);
        this.cdr.markForCheck();
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoadingSettingOrder = false;
        this.cdr.markForCheck();
      },
    );
  }

  private _openModalRecommendScreen(itemCategory: ItemCategory, richMenu: RichMenu) {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-lg',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        itemCategory: itemCategory,
        hasSetting: richMenu.has_setting_order,
      },
    };

    this.openModalWithComponent(DialogRichMenuComponent, opts);
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    const subscribe = this.bsModalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();
      if (reason === SETTING_RICH_MENU.RELOAD_RICH_MENU) {
        const currentPage = cloneDeep(this.pagination.current_page);
        this.pagination.reset();
        this.cdr.detectChanges();
        this.pageChanged({
          page: currentPage,
          itemsPerPage: this.pagination.per_page,
        });
      }
    });

    this.bsModalSv.show(comp, opts);
  }
}
