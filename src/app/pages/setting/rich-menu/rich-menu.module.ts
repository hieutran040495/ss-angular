import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap/modal';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';

import { RichMenuComponent } from './rich-menu.component';

import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';
import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { CasTableModule } from 'app/modules/cas-table/cas-table.module';
import { FilterRichMenuModule } from 'app/forms/filter-rich-menu/filter-rich-menu.module';

import { ValidatorService } from 'shared/utils/validator.service';
import { ModalSettingOrderModule } from 'app/pages/modal-setting-order/modal-setting-order.module';
import { ModalSettingOrderTopScreenModule } from 'app/pages/modal-setting-order-top-screen/modal-setting-order-top-screen.module';
import { DialogRichMenuModule } from 'app/modules/dialog-rich-menu/dialog-rich-menu.module';

const routes: Routes = [
  {
    path: '',
    component: RichMenuComponent,
  },
];

@NgModule({
  declarations: [RichMenuComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    CasPaginationModule.forRoot(),
    ModalModule.forRoot(),
    CasFilterModule,
    CasNgSelectModule,
    CasDialogModule,
    CasTableModule,
    FilterRichMenuModule,
    ModalSettingOrderTopScreenModule,
    ModalSettingOrderModule,
    DialogRichMenuModule,
  ],
  providers: [ValidatorService],
})
export class RichMenuModule {}
