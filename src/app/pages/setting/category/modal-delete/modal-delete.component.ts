import { Component, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToastService } from 'shared/logical-services/toast.service';
import { Category } from 'shared/models/category';
import { CategoryService } from 'shared/http-services/category.service';

@Component({
  selector: 'app-category-modal-delete',
  templateUrl: './modal-delete.component.html',
  styleUrls: ['./modal-delete.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CategoryModalDeleteComponent {
  category: Category;
  isSubmiting: boolean = false;

  constructor(
    private bsModalRef: BsModalRef,
    private modalSv: BsModalService,
    private categorySv: CategoryService,
    private toastSv: ToastService,
    private _cd: ChangeDetectorRef,
  ) {}

  removeCategory() {
    if (this.isSubmiting) {
      return;
    }

    this.isSubmiting = true;
    this._cd.detectChanges();

    this.categorySv.deleteCategory(this.category.id).subscribe(
      (res) => {
        this.toastSv.success('カテゴリーを削除しました');
        this.closeModal('remove');
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
      (error) => {
        this.toastSv.error(error);
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
    );
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
