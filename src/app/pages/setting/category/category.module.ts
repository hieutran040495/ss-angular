import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';

import { CategoryComponent } from './category.component';
import { CategoryModalCeComponent } from './modal-ce/modal-ce.component';
import { CategoryModalDeleteComponent } from './modal-delete/modal-delete.component';

import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { CasCardTypeModule } from 'app/modules/cas-card-type/cas-card-type.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { FilterCategoryModule } from 'app/forms/filter-category/filter-category.module';
import { InputNameWithIdModule } from 'app/modules/input-name-with-id/input-name-with-id.module';

import { CategoryService } from 'shared/http-services/category.service';
import { ValidatorService } from 'shared/utils/validator.service';

import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { CasMaxlengthModule } from 'shared/directive/cas-maxlength/cas-maxlength.module';
import { CasSwapModule } from 'app/modules/cas-swap/cas-swap.module';

const routes: Routes = [
  {
    path: '',
    component: CategoryComponent,
  },
];

@NgModule({
  declarations: [CategoryComponent, CategoryModalCeComponent, CategoryModalDeleteComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot(),
    FormsModule,
    CasPaginationModule.forRoot(),
    SpaceValidationModule,
    CasMaxlengthModule,
    CasFilterModule,
    CasCardTypeModule,
    CasNgSelectModule,
    CasDialogModule,
    FilterCategoryModule,
    InputNameWithIdModule,
    CasSwapModule,
  ],
  entryComponents: [CategoryModalCeComponent, CategoryModalDeleteComponent],
  providers: [CategoryService, ValidatorService],
})
export class CategoryModule {}
