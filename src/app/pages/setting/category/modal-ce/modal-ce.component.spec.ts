import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryModalCeComponent } from './modal-ce.component';

describe('CategoryModalCeComponent', () => {
  let component: CategoryModalCeComponent;
  let fixture: ComponentFixture<CategoryModalCeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CategoryModalCeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryModalCeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
