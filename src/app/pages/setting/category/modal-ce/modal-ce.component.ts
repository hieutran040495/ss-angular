import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { CategoryService } from 'shared/http-services/category.service';
import { ValidatorService } from 'shared/utils/validator.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { Category } from 'shared/models/category';
import { Mode } from 'shared/utils/mode';
import { MenuType } from 'shared/models/menu-type';
import { MenuTypeService } from 'shared/http-services/menu-type.service';
import { Pagination } from 'shared/models/pagination';
import { OrdinalInput } from 'shared/interfaces/ordinal-input';

@Component({
  selector: 'app-category-modal-ce',
  templateUrl: './modal-ce.component.html',
  styleUrls: ['./modal-ce.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryModalCeComponent implements OnInit {
  mode: Mode = new Mode();
  category: Category;

  isSubmiting: boolean = false;
  validator: any = {};

  isLoadMenuType: boolean = false;
  menuTypes: MenuType[];
  pagination: Pagination = new Pagination();

  get title_modal(): string {
    if (this.mode.isEdit) {
      return 'カテゴリー編集';
    }
    return 'カテゴリー新規作成';
  }

  ordinalInput: OrdinalInput;

  constructor(
    private categorySv: CategoryService,
    private modalSv: BsModalService,
    private validatorSv: ValidatorService,
    private bsModalRef: BsModalRef,
    private toastSv: ToastService,
    private menuTypeSv: MenuTypeService,
    private _cd: ChangeDetectorRef,
  ) {
    this.pagination.per_page = 50;
  }

  ngOnInit() {
    if (this.category.id) {
      this.mode.setEdit();
    } else {
      this.mode.setNew();
    }

    this.ordinalInput = {
      name: this.category.name,
      ordinal_number: this.category.ordinal_number,
    };

    this.fetchMenuTypes();
  }

  private fetchMenuTypes(isScroll?: boolean) {
    this.isLoadMenuType = true;
    const opts = {
      ...this.pagination.hasJSON(),
    };

    this._cd.detectChanges();
    return this.menuTypeSv.getListMenutypes(opts).subscribe(
      (res) => {
        if (isScroll) {
          this.menuTypes = this.menuTypes.concat(res.data);
        } else {
          this.menuTypes = res.data;
        }
        this.pagination.deserialize(res);
        this.isLoadMenuType = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoadMenuType = false;
        this._cd.detectChanges();
      },
    );
  }

  createCategory() {
    if (this.isSubmiting) {
      return;
    }

    this.isSubmiting = true;
    this._cd.detectChanges();
    this.categorySv.createCategory(this.category.formDataString()).subscribe(
      (res) => {
        this.toastSv.success('新規カテゴリーを作成しました');
        this.closeModal('reload');
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
      (error) => {
        if (error.hasOwnProperty('errors')) {
          this.validator = this.validatorSv.setErrors(error.errors);
        }
        this.toastSv.error(error);
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
    );
  }

  editCategory() {
    if (this.isSubmiting) {
      return;
    }

    this.isSubmiting = true;
    this._cd.detectChanges();

    this.categorySv.editCategory(this.category.formDataString()).subscribe(
      (res) => {
        this.toastSv.success('編集内容を保存しました');
        this.closeModal('reload');
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
      (error) => {
        if (error.hasOwnProperty('errors')) {
          this.validator = this.validatorSv.setErrors(error.errors);
        }
        this.toastSv.error(error);
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
    );
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  validatorReset() {
    this.validator = this.validatorSv.resetErrors();
    this._cd.detectChanges();
  }

  onScrollToEnd() {
    if (this.isLoadMenuType || !this.pagination.hasNextPage()) {
      return;
    }
    this.pagination.nextPage();
    this.fetchMenuTypes(true);
  }

  onChangeOrdinal() {
    if (!this.ordinalInput) {
      return;
    }
    this.category.name = this.ordinalInput.name;
    this.category.ordinal_number = this.ordinalInput.ordinal_number;
  }
}
