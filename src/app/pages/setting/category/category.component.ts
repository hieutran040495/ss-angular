import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

import { PageChangedInput, Pagination } from 'shared/models/pagination';
import { CategoryModalCeComponent } from './modal-ce/modal-ce.component';
import { CategoryModalDeleteComponent } from './modal-delete/modal-delete.component';

import { CategoryService } from 'shared/http-services/category.service';
import { Category } from 'shared/models/category';
import { ToastService } from 'shared/logical-services/toast.service';

import { SWAP_TYPE } from 'shared/enums/swap';

import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  isPaginate: boolean = false;

  categories: Category[] = [];
  pagination: Pagination = new Pagination();

  categorySort = [
    {
      name: 'カテゴリー名',
      value: 'ordinal_number,name',
      active: false,
    },
    {
      name: '種別名',
      value: 'itemType.name',
      active: false,
    },
  ];

  isSwap: boolean = false;
  swapId: number;
  SWAP_TYPE = SWAP_TYPE;
  isResetSwap: boolean = false;

  private orderCategory = {
    order: 'ordinal_number',
  };
  private filterCategories;

  private modalRef: BsModalRef;

  constructor(
    private modalSv: BsModalService,
    private categorySv: CategoryService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private toastSv: ToastService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.activeRoute.snapshot.queryParams.hasOwnProperty('page')) {
      this.pagination.current_page = this.activeRoute.snapshot.queryParams.page;
    }
    this.getCategories();
  }

  ngOnDestroy() {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  handleSwap() {
    this.isSwap = !this.isSwap;
    if (this.isSwap) {
      this.getCategories(true);
    } else {
      this.getCategories();
    }
  }

  selectItemSwap(id: number) {
    if (!this.isSwap) return;
    this.swapId = id;
    setTimeout(() => {
      this.swapId = undefined;
    }, 0);
  }

  categorySwapped(e) {
    this.categories = e;
    this.swapId = undefined;
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;
    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.getCategories();
    this.appendQueryParams(paginate.page);
  }

  private appendQueryParams(page) {
    this.router.navigate([], {
      queryParams: {
        page: page,
      },
      queryParamsHandling: 'merge',
    });
  }

  private getCategories(isSwap: boolean = false) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;

    let opts = {
      ...this.pagination.hasJSON(),
      ...this.orderCategory,
      ...this.filterCategories,
      with: 'item_type',
    };

    if (isSwap) {
      opts = {
        ...this.filterCategories,
        order: 'ordinal_number',
        with: 'item_type',
      };
    }

    this.categories = [];

    this.categorySv.getListCategory(opts).subscribe(
      (res) => {
        this.categories = res.data;
        this.pagination = new Pagination().deserialize(res);
        this.isLoading = false;
        this.isPaginate = false;
        this.isResetSwap = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this.isPaginate = false;
        this.isResetSwap = false;
        this._cd.detectChanges();
      },
    );
  }

  openModalCECategory(cat?: Category) {
    if (this.isSwap) return;

    const data: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      initialState: {
        category: cat ? cloneDeep(cat) : new Category(),
      },
      ignoreBackdropClick: true,
      keyboard: false,
    };
    this.openModalWithComponent(CategoryModalCeComponent, data);
  }

  openModalRemoveCategory(cat: Category) {
    if (this.isSwap) return;

    const data: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      initialState: {
        category: cloneDeep(cat),
      },
      ignoreBackdropClick: true,
      keyboard: false,
    };
    this.openModalWithComponent(CategoryModalDeleteComponent, data);
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();

      let currentPage: number = cloneDeep(this.pagination.current_page);

      if (reason === 'remove') {
        if (this.pagination.current_page === 1) {
          currentPage = 1;
        } else if (this.categories.length > 1) {
          currentPage = cloneDeep(this.pagination.current_page);
        } else {
          currentPage = cloneDeep(this.pagination.current_page) - 1;
        }
      }

      if (reason === 'reload' || reason === 'remove') {
        this.pagination.reset();
        this._cd.detectChanges();
        this.pageChanged({
          page: currentPage,
          itemsPerPage: this.pagination.per_page,
        });
      }
    });

    this.modalRef = this.modalSv.show(comp, opts);
  }

  sortCategories(data: any) {
    this.orderCategory = data;
    this.pagination.reset();
    this.getCategories();
  }

  filterCategory(data: any) {
    this.filterCategories = data;
    if (this.isSwap) {
      this.isResetSwap = true;
      this.getCategories(true);
    } else {
      this.pagination.reset();
      this.appendQueryParams(this.pagination.current_page);
      this.getCategories();
    }
  }
}
