import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { User } from 'shared/models/user';
import { SessionQuery } from 'shared/states/session';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingComponent implements OnInit {
  curUser: User = new User();

  constructor(private sessionQuery: SessionQuery, private _cd: ChangeDetectorRef) {}

  ngOnInit() {
    this.getCurUser();
  }

  private async getCurUser() {
    this.curUser = await this.sessionQuery.getUserLoggined();

    this._cd.detectChanges();
  }
}
