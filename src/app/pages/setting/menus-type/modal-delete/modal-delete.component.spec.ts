import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuTypeModalDeleteComponent } from './modal-delete.component';

describe('MenuTypeModalDeleteComponent', () => {
  let component: MenuTypeModalDeleteComponent;
  let fixture: ComponentFixture<MenuTypeModalDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MenuTypeModalDeleteComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuTypeModalDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
