import { Component, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToastService } from 'shared/logical-services/toast.service';
import { MenuType } from 'shared/models/menu-type';
import { MenuTypeService } from 'shared/http-services/menu-type.service';

@Component({
  selector: 'app-menu-type-modal-delete',
  templateUrl: './modal-delete.component.html',
  styleUrls: ['./modal-delete.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MenuTypeModalDeleteComponent {
  menuType: MenuType;
  isSubmiting: boolean = false;

  constructor(
    private bsModalRef: BsModalRef,
    private modalSv: BsModalService,
    private menuTypeSv: MenuTypeService,
    private toastSv: ToastService,
    private _cd: ChangeDetectorRef,
  ) {}

  removeMenuType() {
    if (this.isSubmiting) {
      return;
    }

    this.isSubmiting = true;
    this._cd.detectChanges();

    this.menuTypeSv.destroyMenutype(this.menuType.id).subscribe(
      (res) => {
        this.toastSv.success('メニュー種別を削除しました');
        this.closeModal('remove');
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
      (error) => {
        this.toastSv.error(error);
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
    );
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
