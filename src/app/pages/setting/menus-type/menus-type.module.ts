import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';

import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';

import { MenuTypeComponent } from './menus-type.component';
import { MenuTypeModalCeComponent } from './modal-ce/modal-ce.component';
import { MenuTypeModalDeleteComponent } from './modal-delete/modal-delete.component';

import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { CasCardTypeModule } from 'app/modules/cas-card-type/cas-card-type.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { InputNameWithIdModule } from 'app/modules/input-name-with-id/input-name-with-id.module';

import { ValidatorService } from 'shared/utils/validator.service';

import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';

const routes: Routes = [
  {
    path: '',
    component: MenuTypeComponent,
  },
];

@NgModule({
  declarations: [MenuTypeComponent, MenuTypeModalCeComponent, MenuTypeModalDeleteComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot(),
    FormsModule,
    CasPaginationModule.forRoot(),
    SpaceValidationModule,
    CasFilterModule,
    CasCardTypeModule,
    CasDialogModule,
    InputNameWithIdModule,
  ],
  entryComponents: [MenuTypeModalCeComponent, MenuTypeModalDeleteComponent],
  providers: [ValidatorService],
})
export class MenuTypeModule {}
