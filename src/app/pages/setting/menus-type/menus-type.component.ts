import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { Pagination, PageChangedInput } from 'shared/models/pagination';
import { MenuTypeModalCeComponent } from './modal-ce/modal-ce.component';
import { MenuTypeModalDeleteComponent } from './modal-delete/modal-delete.component';

import { MenuTypeService } from 'shared/http-services/menu-type.service';
import { MenuType } from 'shared/models/menu-type';
import { ToastService } from 'shared/logical-services/toast.service';
import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-menu-type',
  templateUrl: './menus-type.component.html',
  styleUrls: ['./menus-type.component.scss'],
})
export class MenuTypeComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  isPaginate: boolean = false;

  menuTypes: MenuType[] = [];
  pagination: Pagination = new Pagination();

  sortMenuType = [
    {
      name: '種別名',
      value: 'name',
      active: false,
    },
  ];

  private orderMenu = {
    order: 'ordinal_number',
  };
  public filterName = '';

  private modalRef: BsModalRef;

  constructor(
    private modalSv: BsModalService,
    private menuTypeSv: MenuTypeService,
    private toastSv: ToastService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.activeRoute.snapshot.queryParams.hasOwnProperty('page')) {
      this.pagination.current_page = +this.activeRoute.snapshot.queryParams.page;
    }
    this.getMenusType();
  }

  ngOnDestroy() {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;
    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.getMenusType();
    this.appendQueryParams(paginate.page);
  }

  private appendQueryParams(page) {
    this.router.navigate([], {
      queryParams: {
        page: page,
      },
      queryParamsHandling: 'merge',
    });
  }

  private getMenusType() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this._cd.detectChanges();

    const opts = {
      ...this.pagination.hasJSON(),
      ...this.orderMenu,
      name: this.filterName.trim(),
    };
    this.menuTypes = [];

    this.menuTypeSv.getListMenutypes(opts).subscribe(
      (res) => {
        this.menuTypes = res.data;
        this.pagination.deserialize(res);
        this.isLoading = false;
        this.isPaginate = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this.isPaginate = false;
        this._cd.detectChanges();
      },
    );
  }

  openModalCEMenuType(menuType?: MenuType) {
    const modalOpts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      initialState: {
        menuType: menuType ? cloneDeep(menuType) : new MenuType(),
      },
      ignoreBackdropClick: true,
      keyboard: false,
    };
    this.openModalWithComponent(MenuTypeModalCeComponent, modalOpts);
  }

  openModalRemoveMenuType(menuType: MenuType) {
    const modalOpts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      initialState: {
        menuType: cloneDeep(menuType),
      },
      ignoreBackdropClick: true,
      keyboard: false,
    };
    this.openModalWithComponent(MenuTypeModalDeleteComponent, modalOpts);
  }

  private openModalWithComponent(comp, config: ModalOptions) {
    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();

      let currentPage: number = cloneDeep(this.pagination.current_page);

      if (reason === 'remove') {
        if (this.pagination.current_page === 1) {
          currentPage = 1;
        } else if (this.menuTypes.length > 1) {
          currentPage = cloneDeep(this.pagination.current_page);
        } else {
          currentPage = cloneDeep(this.pagination.current_page) - 1;
        }
      }

      if (reason === 'reload' || reason === 'remove') {
        this.pagination.reset();
        this._cd.detectChanges();
        this.pageChanged({
          page: currentPage,
          itemsPerPage: this.pagination.per_page,
        });
      }
    });

    this.modalRef = this.modalSv.show(comp, config);
  }

  menuTypeSort(data: any) {
    this.orderMenu = data;
    this.filterMenuType();
  }

  filterMenuType() {
    this.pagination.reset();
    this.appendQueryParams(this.pagination.current_page);
    this.getMenusType();
  }
}
