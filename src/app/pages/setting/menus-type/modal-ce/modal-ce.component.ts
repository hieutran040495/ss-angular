import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { MenuTypeService } from 'shared/http-services/menu-type.service';
import { ValidatorService } from 'shared/utils/validator.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { MenuType } from 'shared/models/menu-type';
import { Mode } from 'shared/utils/mode';
import { Regexs } from 'shared/constants/regex';
import { OrdinalInput } from 'shared/interfaces/ordinal-input';

@Component({
  selector: 'app-menu-type-modal-ce',
  templateUrl: './modal-ce.component.html',
  styleUrls: ['./modal-ce.component.scss'],
})
export class MenuTypeModalCeComponent implements OnInit {
  mode: Mode = new Mode();
  menuType: MenuType;
  ordinalInput: OrdinalInput;

  isSubmiting: boolean = false;
  validator: any = {};
  Regexs = Regexs;

  get title_modal(): string {
    if (this.mode.isEdit) {
      return 'メニュー種別編集';
    }
    return 'メニュー種別作成';
  }

  constructor(
    private menuTypeSv: MenuTypeService,
    private modalSv: BsModalService,
    private validatorSv: ValidatorService,
    private bsModalRef: BsModalRef,
    private toastSv: ToastService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.menuType.id) {
      this.mode.setEdit();
    } else {
      this.mode.setNew();
    }

    this.ordinalInput = {
      name: this.menuType.name,
      ordinal_number: this.menuType.ordinal_number,
    };
  }

  createMenuType() {
    if (this.isSubmiting) {
      return;
    }

    this.isSubmiting = true;
    this._cd.detectChanges();

    this.menuTypeSv.createMenutype(this.menuType.formDataString()).subscribe(
      (response) => {
        this.toastSv.success('メニュー種別を作成しました');
        this.closeModal('reload');
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
      (error) => {
        if (error.hasOwnProperty('errors')) {
          this.validator = this.validatorSv.setErrors(error.errors);
        }
        this.toastSv.error(error);
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
    );
  }

  editMenuType() {
    if (this.isSubmiting) {
      return;
    }

    this.isSubmiting = true;
    this._cd.detectChanges();

    this.menuTypeSv.updateMenutype(this.menuType.id, this.menuType.formDataString()).subscribe(
      (res) => {
        this.toastSv.success('メニュー種別を編集しました');
        this.closeModal('reload');
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
      (error) => {
        if (error.hasOwnProperty('errors')) {
          this.validator = this.validatorSv.setErrors(error.errors);
        }
        this.toastSv.error(error);
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
    );
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  validatorReset() {
    this.validator = this.validatorSv.resetErrors();
    this._cd.detectChanges();
  }

  onChangeOrdinal() {
    if (!this.ordinalInput) {
      return;
    }
    this.menuType.name = this.ordinalInput.name;
    this.menuType.ordinal_number = this.ordinalInput.ordinal_number;
  }
}
