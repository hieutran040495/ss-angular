import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuTypeModalCeComponent } from './modal-ce.component';

describe('MenuTypeModalCeComponent', () => {
  let component: MenuTypeModalCeComponent;
  let fixture: ComponentFixture<MenuTypeModalCeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MenuTypeModalCeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuTypeModalCeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
