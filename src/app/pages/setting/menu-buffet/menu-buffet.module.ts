import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';

import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { CasCardTypeModule } from 'app/modules/cas-card-type/cas-card-type.module';
import { MenuBuffetCeModule } from 'app/pages/menu-buffet-ce/menu-buffet-ce.module';

import { MenuBuffetComponent } from './menu-buffet.component';

const routes: Routes = [
  {
    path: '',
    component: MenuBuffetComponent,
  },
];

@NgModule({
  declarations: [MenuBuffetComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule.forRoot(),
    CasPaginationModule.forRoot(),
    RouterModule.forChild(routes),
    CasFilterModule,
    CasCardTypeModule,
    MenuBuffetCeModule,
  ],
})
export class MenuBuffetModule {}
