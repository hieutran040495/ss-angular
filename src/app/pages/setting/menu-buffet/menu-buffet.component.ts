import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { MenuBuffetService } from 'shared/http-services/menu-buffet.service';
import { MenuBuffet } from 'shared/models/menu-buffet';
import { Pagination, PageChangedInput } from 'shared/models/pagination';
import { ToastService } from 'shared/logical-services/toast.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DIALOG_EVENT } from 'shared/enums/modes';
import { MenuBuffetCeComponent } from 'app/pages/menu-buffet-ce/menu-buffet-ce.component';

import { Subscription } from 'rxjs';

import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-menu-buffet',
  templateUrl: './menu-buffet.component.html',
  styleUrls: ['./menu-buffet.component.scss'],
})
export class MenuBuffetComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  isPaginate: boolean = false;
  menuBuffet: MenuBuffet[] = [];
  pagination: Pagination = new Pagination();

  private orderBuffet;
  sortMenuBuffet = [
    {
      name: '放題メニュー名',
      value: 'name',
      active: false,
    },
  ];
  filterName = '';
  private modalRef: BsModalRef;

  private subscribe: Subscription;

  constructor(
    private menuBuffetSv: MenuBuffetService,
    private toastrSv: ToastService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private modalSv: BsModalService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.activeRoute.snapshot.queryParams.hasOwnProperty('page')) {
      this.pagination.current_page = this.activeRoute.snapshot.queryParams.page;
    }
    this.getBuffets();
  }

  ngOnDestroy() {
    if (this.modalRef) {
      this.modalRef.hide();
    }

    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }

  getBuffets() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;

    const opts: any = {
      ...this.pagination.hasJSON(),
      ...this.orderBuffet,
      name: this.filterName.trim(),
      with: 'items',
    };
    this.menuBuffet = [];
    this._cd.detectChanges();

    this.menuBuffetSv.getBuffets(opts).subscribe(
      (res) => {
        this.pagination.deserialize(res);
        this.menuBuffet = res.data;
        this.isLoading = false;
        this.isPaginate = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;
    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.getBuffets();
    this.appendQueryParams(paginate.page);
  }

  private appendQueryParams(page: number) {
    this.router.navigate([], {
      queryParams: {
        page: page,
      },
      queryParamsHandling: 'merge',
    });
  }

  menuBuffetSort(data: any) {
    this.orderBuffet = data;
    this.filterMenuBuffet();
  }

  filterMenuBuffet() {
    this.pagination.reset();
    this.appendQueryParams(this.pagination.current_page);
    this.getBuffets();
  }

  openDialogBuffet(buffet?: MenuBuffet) {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-lg',
      ignoreBackdropClick: true,
      keyboard: false,
    };

    if (buffet && buffet.id) {
      this.menuBuffetSv.fetchMenuBuffetById(buffet.id, { with: 'items' }).subscribe(
        (res: MenuBuffet) => {
          opts.initialState = {
            menuBuffet: res,
          };
          this.openModalWithComponent(MenuBuffetCeComponent, opts);
        },
        (error: any) => {
          this.toastrSv.error(error);
        },
      );
    } else {
      this.openModalWithComponent(MenuBuffetCeComponent, opts);
    }
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    this.subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      let currentPage: number = cloneDeep(this.pagination.current_page);

      if (reason === DIALOG_EVENT.BUFFET_REMOVE) {
        if (this.pagination.current_page === 1) {
          currentPage = 1;
        } else if (this.menuBuffet.length > 1) {
          currentPage = cloneDeep(this.pagination.current_page);
        } else {
          currentPage = cloneDeep(this.pagination.current_page) - 1;
        }
      }

      if (reason === DIALOG_EVENT.BUFFET_RELOAD || reason === DIALOG_EVENT.BUFFET_REMOVE) {
        this.pagination.reset();
        this._cd.detectChanges();
        this.pageChanged({
          page: currentPage,
          itemsPerPage: this.pagination.per_page,
        });
      }
    });

    this.modalSv.show(comp, opts);
  }
}
