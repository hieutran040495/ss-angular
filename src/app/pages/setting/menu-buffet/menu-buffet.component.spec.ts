import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuBuffetComponent } from './menu-buffet.component';

describe('MenuBuffetComponent', () => {
  let component: MenuBuffetComponent;
  let fixture: ComponentFixture<MenuBuffetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MenuBuffetComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuBuffetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
