import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ProfitMonthComponent } from './profit-month.component';
import { ProfitChartsModule } from 'app/pages/profit-charts/profit-charts.module';

const routes: Routes = [
  {
    path: '',
    component: ProfitMonthComponent,
  },
];

@NgModule({
  declarations: [ProfitMonthComponent],
  imports: [CommonModule, FormsModule, RouterModule.forChild(routes), ProfitChartsModule],
})
export class ProfitMonthModule {}
