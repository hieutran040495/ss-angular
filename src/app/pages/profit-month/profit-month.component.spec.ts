import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitMonthComponent } from './profit-month.component';

describe('ProfitMonthComponent', () => {
  let component: ProfitMonthComponent;
  let fixture: ComponentFixture<ProfitMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfitMonthComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
