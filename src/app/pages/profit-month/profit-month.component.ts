import { Component, OnInit } from '@angular/core';
import { PROFIT_DATETIME } from 'shared/enums/profit';

@Component({
  selector: 'app-profit-month',
  templateUrl: './profit-month.component.html',
  styleUrls: ['./profit-month.component.scss'],
})
export class ProfitMonthComponent implements OnInit {
  profitType: string = PROFIT_DATETIME.MONTHS;

  constructor() {}

  ngOnInit() {}
}
