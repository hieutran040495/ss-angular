import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitSelectDateComponent } from './profit-select-date.component';

describe('ProfitSelectDateComponent', () => {
  let component: ProfitSelectDateComponent;
  let fixture: ComponentFixture<ProfitSelectDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfitSelectDateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitSelectDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
