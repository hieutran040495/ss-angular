import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { PROFIT_DATETIME, PROFIT_TYPE } from 'shared/enums/profit';
import * as moment from 'moment';

@Component({
  selector: 'app-profit-select-date',
  templateUrl: './profit-select-date.component.html',
  styleUrls: ['./profit-select-date.component.scss'],
})
export class ProfitSelectDateComponent implements OnInit {
  private dateType: string;
  @Input('type')
  get type(): string {
    return this.dateType || PROFIT_DATETIME.DAY;
  }
  set type(v: string) {
    this.dateType = v;
  }
  @Input() disabled: boolean = false;

  @Output() change = new EventEmitter<any>();

  private dateSelected: moment.Moment = moment();
  private weekSelected: moment.Moment;
  private monthSelected: moment.Moment;

  get dateFormat() {
    switch (this.type) {
      case PROFIT_DATETIME.DAY:
        return this.dateSelected.format('YYYY[年]MMMDo');
      case PROFIT_DATETIME.WEEKS:
        const weekStart = this.weekSelected.clone().format('MM/DD [(]ddd[)]');
        const weekEnd = this.weekSelected
          .clone()
          .endOf('week')
          .format('MM/DD [(]ddd[)]');
        return `${weekStart} ~ ${weekEnd}`;
      case PROFIT_DATETIME.MONTHS:
        return this.monthSelected.format('YYYY[年]MMM');
      default:
        return this.dateSelected.format('YYYY[年]MMMDo');
    }
  }

  constructor() {}

  ngOnInit() {
    if (this.dateSelected.hour() < 6) {
      this.dateSelected.subtract(1, 'day');
    }
    this.weekSelected = this.dateSelected.clone().startOf(PROFIT_TYPE.WEEKS);
    this.monthSelected = this.dateSelected.clone().startOf(PROFIT_TYPE.MONTHS);

    this.change.emit({
      date: this.dateSelected.clone(),
      start_at: this.dateSelected
        .clone()
        .set({
          hour: 6,
          minute: 0,
          second: 0,
          millisecond: 0,
        })
        .toISOString(),
      end_at: this.dateSelected
        .clone()
        .endOf('day')
        .set({
          hour: 29,
          minute: 59,
          second: 0,
          millisecond: 0,
        })
        .toISOString(),
      weekStart: this.weekSelected
        .clone()
        .set({
          hour: 6,
          minute: 0,
          second: 0,
          millisecond: 0,
        })
        .toISOString(),
      weekEnd: this.weekSelected
        .clone()
        .endOf('week')
        .set({
          hour: 29,
          minute: 59,
          second: 0,
          millisecond: 0,
        })
        .toISOString(),
      month_start: this.monthSelected
        .clone()
        .set({
          hour: 6,
          minute: 0,
          second: 0,
          millisecond: 0,
        })
        .toISOString(),
      month_end: this.monthSelected
        .clone()
        .endOf('month')
        .set({
          hour: 29,
          minute: 59,
          second: 0,
          millisecond: 0,
        })
        .toISOString(),
    });
  }

  private weekPrev() {
    this.weekSelected.subtract(1, 'week');
    this._emitChangeWeek();
  }

  private weekNext() {
    if (this.weekSelected.diff(moment().startOf('week')) < 0) {
      this.weekSelected.add(1, 'week');
      this._emitChangeWeek();
    }
  }

  private monthPrev() {
    this.monthSelected.subtract(1, 'month');
    this._emitChangeMonth();
  }

  private monthNext() {
    if (this.monthSelected.diff(moment().startOf('month')) < 0) {
      this.monthSelected.add(1, 'month');
      this._emitChangeMonth();
    }
  }

  private _emitChangeMonth() {
    this.change.emit({
      month_start: this.monthSelected
        .clone()
        .set({
          hour: 6,
          minute: 0,
          second: 0,
          millisecond: 0,
        })
        .toISOString(),
      month_end: this.monthSelected
        .clone()
        .endOf('month')
        .set({
          hour: 29,
          minute: 59,
          second: 0,
          millisecond: 0,
        })
        .toISOString(),
    });
  }

  private _emitChangeWeek() {
    this.change.emit({
      weekStart: this.weekSelected
        .clone()
        .set({
          hour: 6,
          minute: 0,
          second: 0,
          millisecond: 0,
        })
        .toISOString(),
      weekEnd: this.weekSelected
        .clone()
        .endOf('week')
        .set({
          hour: 29,
          minute: 59,
          second: 0,
          millisecond: 0,
        })
        .toISOString(),
    });
  }

  datePrev() {
    switch (this.type) {
      case PROFIT_DATETIME.DAY:
        this.dateSelected.subtract(1, 'day');
        this._emitChangeDate();
        return;
      case PROFIT_DATETIME.WEEKS:
        this.weekPrev();
        break;
      case PROFIT_DATETIME.MONTHS:
        this.monthPrev();
        break;
      default:
        return this.dateSelected.format('YYYY[年]MMMDo');
    }
  }

  dateNext() {
    switch (this.type) {
      case PROFIT_DATETIME.DAY:
        if (this.dateSelected.diff(moment().startOf('day')) < 0) {
          this.dateSelected.add(1, 'day');
          this._emitChangeDate();
        }
        return;
      case PROFIT_DATETIME.WEEKS:
        this.weekNext();
        break;
      case PROFIT_DATETIME.MONTHS:
        this.monthNext();
        break;
      default:
        return this.dateSelected.format('YYYY[年]MMMDo');
    }
  }

  private _emitChangeDate() {
    this.change.emit({
      days: this.dateSelected.clone(),
      start_at: this.dateSelected
        .clone()
        .set({
          hour: 6,
          minute: 0,
          second: 0,
          millisecond: 0,
        })
        .toISOString(),
      end_at: this.dateSelected
        .clone()
        .endOf('day')
        .set({
          hour: 29,
          minute: 59,
          second: 0,
          millisecond: 0,
        })
        .toISOString(),
    });
  }
}
