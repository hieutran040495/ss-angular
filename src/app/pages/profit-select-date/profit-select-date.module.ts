import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfitSelectDateComponent } from './profit-select-date.component';

@NgModule({
  declarations: [ProfitSelectDateComponent],
  imports: [CommonModule],
  exports: [ProfitSelectDateComponent],
})
export class ProfitSelectDateModule {}
