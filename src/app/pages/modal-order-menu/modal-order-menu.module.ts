import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';
import { ModalOrderMenuComponent } from './modal-order-menu.component';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { DialogMenuSoldOutModule } from 'app/modules/dialog-menu-sold-out/dialog-menu-sold-out.module';

@NgModule({
  declarations: [ModalOrderMenuComponent],
  imports: [
    CommonModule,
    OnlyNumericModule,
    FormsModule,
    ModalModule.forRoot(),
    CasPaginationModule.forRoot(),
    CasDialogModule,
    DialogMenuSoldOutModule,
  ],
  entryComponents: [ModalOrderMenuComponent],
  exports: [ModalOrderMenuComponent],
})
export class ModalOrderMenuModule {}
