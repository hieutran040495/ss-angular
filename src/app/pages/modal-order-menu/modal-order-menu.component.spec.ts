import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalOrderMenuComponent } from './modal-order-menu.component';

describe('ModalOrderMenuComponent', () => {
  let component: ModalOrderMenuComponent;
  let fixture: ComponentFixture<ModalOrderMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalOrderMenuComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalOrderMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
