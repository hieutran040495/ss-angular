import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

import { Pagination, PageChangedInput } from 'shared/models/pagination';
import { MenuService } from 'shared/http-services/menu.service';
import { CourseService } from 'shared/http-services/course.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { CategoryService } from 'shared/http-services/category.service';

import { Regexs } from 'shared/constants/regex';
import { Menu } from 'shared/models/menu';
import { Course } from 'shared/models/course';
import { Category } from 'shared/models/category';
import { SEARCH_WITH, ORDER_TYPE, OPTION_TYPE } from 'shared/enums/type-search-order';
import { RESV_ITEMS_TYPE } from 'shared/enums/reservation';
import { EventEmitterType } from 'shared/enums/event-emitter';
import { MenuBuffet } from 'shared/models/menu-buffet';
import { MenuBuffetService } from 'shared/http-services/menu-buffet.service';

import { DialogMenuSoldOutComponent } from 'app/modules/dialog-menu-sold-out/dialog-menu-sold-out.component';
import { DIALOG_EVENT } from 'shared/enums/modes';

@Component({
  selector: 'app-modal-order-menu',
  templateUrl: './modal-order-menu.component.html',
  styleUrls: ['./modal-order-menu.component.scss'],
})
export class ModalOrderMenuComponent implements OnInit {
  isLoading: boolean = false;
  isLoadingCat: boolean = false;
  isPaginate: boolean = false;

  categories: Category[] = [];
  listMenus: Menu[] = [];
  listCourses: Course[] = [];
  listBuffets: MenuBuffet[] = [];

  items: Menu[] = [];
  menuSoldOutSelected: Menu;
  courses: Course[] = [];
  buffets: MenuBuffet[] = [];

  rules = Regexs;
  SEARCH_WITH = SEARCH_WITH;

  seachType: string;
  orderType: string;
  optionType: string;

  categoryId: number;
  totalCourse: number;
  totalBuffet: number;
  pagination: Pagination = new Pagination().deserialize({ per_page: 10 });

  get listItems(): Menu[] | Course[] | MenuBuffet[] {
    switch (this.seachType) {
      case SEARCH_WITH.COURSE:
        return this.listCourses;
      case SEARCH_WITH.CATEGORY:
        return this.listMenus;
      case SEARCH_WITH.BUFFET:
        return this.listBuffets;
      default:
        break;
    }
  }

  get isResvOrder(): boolean {
    return this.orderType === ORDER_TYPE.RESV_ORDER;
  }
  get isSelectMenu(): boolean {
    return this.orderType === ORDER_TYPE.SELECT_MENU;
  }
  get isSelectBuffet(): boolean {
    return this.orderType === ORDER_TYPE.SELECT_BUFFET;
  }

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private menuSv: MenuService,
    private toastrSv: ToastService,
    private emitSv: EventEmitterService,
    private courseSv: CourseService,
    private categorySv: CategoryService,
    private buffetSv: MenuBuffetService,
  ) {}

  ngOnInit() {
    this._fetchCategories(true);
    if (!this.isSelectMenu) {
      this._fetchListOrder();
    }
  }

  closeModal() {
    this.bsModalRef.hide();
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;

    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this._fetchListOrder();
  }

  callPlus(item: any) {
    if (item.quantity === 100) {
      return;
    }
    switch (this.seachType) {
      case SEARCH_WITH.COURSE:
        this.plusCourse(item);
        break;
      case SEARCH_WITH.CATEGORY:
        this.plusMenu(item);
        break;
      case SEARCH_WITH.BUFFET:
        this.plusBuffet(item);
        break;
      default:
        break;
    }
  }

  callMinus(item: any) {
    if (item.quantity === 0) {
      return;
    }
    switch (this.seachType) {
      case SEARCH_WITH.COURSE:
        this.minusCourse(item);
        break;
      case SEARCH_WITH.CATEGORY:
        this.minusMenu(item);
        break;
      case SEARCH_WITH.BUFFET:
        this.minusBuffet(item);
        break;
      default:
        break;
    }
  }

  plusCourse(course: Course) {
    course.plusQuantity();
    this._pushSelectedCourse(course);
  }

  minusCourse(course: Course) {
    course.minusQuantity();
    this._pushSelectedCourse(course);
  }

  private _pushSelectedCourse(course: Course) {
    const index = this.courses.findIndex((item: Course) => item.id === course.id);
    if (index < 0) {
      if (course.quantity > 0) {
        course.type = RESV_ITEMS_TYPE.COURSE;
        this.courses.push(course);
      }
      return;
    }
    if (course.quantity > 0) {
      this.courses[index] = course;
      return;
    }
    if (course.quantity === 0) {
      this.courses.splice(index, 1);
    }
  }

  plusMenu(menu: Menu) {
    if (menu.is_sold_out && this.isResvOrder && menu.quantity === 0) {
      if (!this.menuSoldOutSelected || (this.menuSoldOutSelected && this.menuSoldOutSelected.id !== menu.id)) {
        this.menuSoldOutSelected = menu;
        this._dialogMenuSoldOut();
      }
    } else {
      menu.plusQuantity();
      this._pushSelectedMenu(menu);
    }
  }

  minusMenu(menu: Menu) {
    menu.minusQuantity();
    this._pushSelectedMenu(menu);
  }

  private _pushSelectedMenu(menu: Menu) {
    if (this.menuSoldOutSelected && this.menuSoldOutSelected.quantity === 0) {
      this.menuSoldOutSelected = undefined;
    }

    const index = this.items.findIndex((item: Menu) => item.id === menu.id);
    if (index < 0) {
      if (menu.quantity > 0) {
        menu.type = RESV_ITEMS_TYPE.ITEM;
        this.items.push(menu);
      }
      return;
    }
    if (menu.quantity > 0) {
      this.items[index] = menu;
      return;
    }
    if (menu.quantity === 0) {
      this.items.splice(index, 1);
    }
  }

  plusBuffet(buffet: MenuBuffet) {
    buffet.plusQuantity();
    this._pushSelectedBuffet(buffet);
  }

  minusBuffet(buffet: MenuBuffet) {
    buffet.minusQuantity();
    this._pushSelectedBuffet(buffet);
  }

  private _pushSelectedBuffet(buffet: MenuBuffet) {
    const index = this.buffets.findIndex((item: MenuBuffet) => item.id === buffet.id);
    if (index < 0) {
      if (buffet.quantity > 0) {
        buffet.type = RESV_ITEMS_TYPE.BUFFET;
        this.buffets.push(buffet);
      }
      return;
    }
    if (buffet.quantity > 0) {
      this.buffets[index] = buffet;
      return;
    }
    if (buffet.quantity === 0) {
      this.buffets.splice(index, 1);
    }
  }

  orderMenu() {
    this.closeModal();
    this._emitMenuItem();
  }

  searchListOrder(type: string, catogoryId?: number) {
    this.seachType = type;
    this.categoryId = catogoryId ? catogoryId : undefined;
    this.pagination.reset();
    this._fetchListOrder();
  }

  isActiveFilter(type: string, catId?: number): boolean {
    if (!catId) {
      return type === this.seachType;
    }
    return type === this.seachType && this.categoryId === catId;
  }

  private _emitMenuItem() {
    switch (this.orderType) {
      case ORDER_TYPE.RESV_ORDER:
        this.emitSv.publishData({
          type: EventEmitterType.ORDER_MENU,
          data: {
            items: this.items,
            courses: this.courses,
          },
        });
        break;
      case ORDER_TYPE.SELECT_BUFFET:
        this.emitSv.publishData({
          type: EventEmitterType.SELECT_MENU_BUFFET,
          data: {
            items: this.buffets,
          },
        });
        break;
      case ORDER_TYPE.SELECT_MENU:
        this.emitSv.publishData({
          type: EventEmitterType.SELECT_MENU_ITEM,
          data: {
            items: this.items,
          },
        });
        break;

      default:
        break;
    }
  }

  private _fetchCategories(isFirstLoading?: boolean) {
    if (this.isLoadingCat || this.isSelectBuffet) {
      return;
    }
    this.isLoadingCat = true;

    const opts: any = {};

    switch (this.optionType) {
      case OPTION_TYPE.COURSE:
        opts.with = 'items_count_for_course';
        break;
      case OPTION_TYPE.BUFFET:
        opts.with = 'items_count_for_buffet';
        break;
      default:
        opts.with = 'items_count';
        break;
    }

    this.categories = [];

    return this.categorySv.getCategories(opts).subscribe(
      (res) => {
        this.categories = res.filter((cat: Category) => {
          return cat.items_count > 0;
        });

        this.categoryId = this.categories.length ? this.categories[0].id : undefined;
        this.isLoadingCat = false;
      },
      (errors) => {
        this.toastrSv.error(errors);
        this.isLoadingCat = false;
      },
      () => {
        if (isFirstLoading) {
          this._fetchMenu();
        }
      },
    );
  }

  private _fetchListOrder() {
    switch (this.seachType) {
      case SEARCH_WITH.COURSE:
        this._fetchCourses();
        break;
      case SEARCH_WITH.CATEGORY:
        this._fetchMenu();
        break;
      case SEARCH_WITH.BUFFET:
        this._fetchBuffets();
        break;
      default:
        break;
    }
  }

  private _fetchMenu() {
    this.isLoading = true;

    const opts: any = {
      ...this.pagination.hasJSON(),
      category_id: this.categoryId,
    };

    switch (this.optionType) {
      case OPTION_TYPE.COURSE:
        opts.with = 'menu_for_course';
        break;
      case OPTION_TYPE.BUFFET:
        opts.with = 'menu_for_buffet';
        break;
      default:
        opts.is_show = 1;
        break;
    }

    this.listMenus = [];

    this.menuSv.fetchMenus(opts, true).subscribe(
      (res) => {
        this.pagination.deserialize(res);

        if (!!this.items.length) {
          this.items.filter((item: Menu) => {
            const index = res.data.findIndex((menu: Menu) => menu.id === item.id);
            if (index !== -1) {
              res.data[index] = item;
            }
          });
        }

        this.listMenus = res.data;
        this.isLoading = false;
        this.isPaginate = false;
      },
      (errors) => {
        this.toastrSv.error(errors);
        this.isLoading = false;
        this.isPaginate = false;
      },
    );
  }

  private _fetchCourses() {
    this.isLoading = true;

    const opts = {
      ...this.pagination.hasJSON(),
    };
    this.listCourses = [];

    this.courseSv.fetchCourses(opts).subscribe(
      (res) => {
        this.pagination.deserialize(res);

        if (!!this.courses.length) {
          this.courses.filter((selected: Course) => {
            const index = res.data.findIndex((course: Course) => course.id === selected.id);
            if (index !== -1) {
              res.data[index] = selected;
            }
          });
        }
        this.listCourses = res.data;
        this.totalCourse = this.pagination.total;

        this.isLoading = false;
        this.isPaginate = false;
      },
      (errors) => {
        this.toastrSv.error(errors);
        this.isLoading = false;
        this.isPaginate = false;
      },
    );
  }

  private _fetchBuffets() {
    if (this.isLoading) return;

    const opts: any = {
      ...this.pagination.hasJSON(),
      is_show: 1,
    };
    this.isLoading = true;

    return this.buffetSv.getBuffets(opts, true).subscribe(
      (res) => {
        this.pagination.deserialize(res);

        if (!!this.buffets.length) {
          this.buffets.filter((selected: MenuBuffet) => {
            const index = res.data.findIndex((buffet: MenuBuffet) => buffet.id === selected.id);
            if (index !== -1) {
              res.data[index] = selected;
            }
          });
        }
        this.listBuffets = res.data;
        this.totalBuffet = this.pagination.total;

        this.isLoading = false;
        this.isPaginate = false;
      },
      (errors) => {
        this.toastrSv.error(errors);

        this.isLoading = false;
        this.isPaginate = false;
      },
    );
  }

  private _dialogMenuSoldOut() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        menu: this.menuSoldOutSelected,
      },
    };

    this.openModalWithComponent(DialogMenuSoldOutComponent, opts);
  }

  private openModalWithComponent(comp, opts: ModalOptions) {
    const subscribe = this.bsModalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();
      if (reason === DIALOG_EVENT.APPLY_MENU_SOLD_OUT) {
        this.menuSoldOutSelected.plusQuantity();
        this._pushSelectedMenu(this.menuSoldOutSelected);
      } else {
        this.menuSoldOutSelected = undefined;
      }
    });

    this.bsModalSv.show(comp, opts);
  }
}
