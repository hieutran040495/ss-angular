import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-resv-filter',
  templateUrl: './resv-filter.component.html',
  styleUrls: ['./resv-filter.component.scss'],
})
export class ResvFilterComponent implements OnInit {
  minDate = new Date();
  @Input() disabledDateTime: boolean = false;

  private _start_at: string;
  get start_at(): string {
    return this._start_at || moment().format('YYYY/MM/DD');
  }
  @Input('startAt')
  set start_at(v: string) {
    this._start_at = v;
    this.start_date = moment(this.start_at).format('YYYY/MM/DD');
    this.start_time = moment(this.start_at).format('HH:mm');
  }

  private _end_at: string;
  get end_at(): string {
    return this._end_at;
  }
  @Input('endAt')
  set end_at(v: string) {
    this._end_at = v;
    this.end_date = moment(this.end_at).format('YYYY/MM/DD');
    this.end_time = moment(this.end_at).format('HH:mm');
  }

  start_date: string = moment().format('YYYY/MM/DD');
  start_time: string = '00:00';

  end_date: string = moment().format('YYYY/MM/DD');
  end_time: string = '23:59';

  @Input('quantity')
  quantity: number = 1;

  @Input('hasChildren')
  has_children: boolean = false;

  @Input('validator')
  validator: string;

  @Input('disabled')
  disabled: boolean;

  @Output()
  valueChange: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}

  onChangeValue() {
    const data: any = {
      start_at: this.getStartAt(),
      end_at: this.getEndAt(),
      quantity: this.quantity,
      has_children: this.has_children,
    };

    this.valueChange.emit(data);
  }

  getStartAt(): string {
    const timeDuration = moment.duration(this.start_time);

    return moment(this.start_date, 'YYYY/MM/DD')
      .set({
        hours: Math.floor(timeDuration.asHours()),
        minutes: timeDuration.minutes(),
      })
      .toISOString();
  }

  getEndAt(): string {
    const timeDuration = moment.duration(this.end_time);

    return moment(this.start_date, 'YYYY/MM/DD')
      .set({
        hours: Math.floor(timeDuration.asHours()),
        minutes: timeDuration.minutes(),
      })
      .toISOString();
  }

  resetValidation(): void {
    this.validator = undefined;
  }
}
