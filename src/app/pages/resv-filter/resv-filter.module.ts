import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TimePickerModule } from 'shared/modules/time-picker/time-picker.module';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';

import { ResvFilterComponent } from './resv-filter.component';
import { NumbericValidatorModule } from 'shared/directive/numberic-validator/numberic-validator.module';
import { NgxDatepickerModule } from 'shared/modules/ngx-datepicker/ngx-datepicker.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TimePickerModule,
    OnlyNumericModule,
    NumbericValidatorModule,
    NgxDatepickerModule,
  ],
  declarations: [ResvFilterComponent],
  exports: [ResvFilterComponent],
})
export class ResvFilterModule {}
