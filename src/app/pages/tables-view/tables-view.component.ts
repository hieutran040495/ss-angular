import { Component, OnInit, EventEmitter, OnDestroy, ViewEncapsulation } from '@angular/core';

import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ClientService } from 'shared/http-services/client.service';
import { TableService } from 'shared/http-services/table.service';

import { Table } from 'shared/models/table';
import { Walkin } from 'shared/models/walkin';
import { Client } from 'shared/models/client';

import { DURATIONS_VALUE } from 'shared/constants/time-setting';
import { KONVA_EMITTER_TYPE, KonvaDataEmitter } from 'shared/modules/konva-canvas/data-emitter';
import { MODES } from 'shared/enums/modes';
import { RESV_MODE } from 'shared/enums/resv-mode';
import { RESV_TYPE } from 'shared/enums/reservation';
import { CALENDAR_EVENT_EMITTER, RESV_MODE_EMITTER, RESV_TABLE_EVENT_EMITTER } from 'shared/enums/event-emitter';

import { EventEmitterService } from 'shared/logical-services/event-emitter.service';

import { Reservation } from 'shared/models/reservation';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ModalUpdateTableComponent } from 'app/pages/modal-update-table/modal-update-table.component';
import { Subject } from 'rxjs/Subject';

import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-tables-view',
  templateUrl: './tables-view.component.html',
  styleUrls: ['./tables-view.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TablesViewComponent implements OnInit, OnDestroy {
  tableSelected: Table = new Table();
  selectedQuantity: number;
  clientInfo: Client = new Client();
  isLoading: boolean = false;

  eventReloadTable: EventEmitter<any> = new EventEmitter<any>();
  canvasHandleEvent$: Subject<KonvaDataEmitter> = new Subject<KonvaDataEmitter>();

  tables: Table[] = [];
  MODES = MODES;

  public eventUpdateCalendar: Subject<any> = new Subject<any>();
  private _modalRef: BsModalRef;

  constructor(
    private resvSv: ReservationService,
    private toastSv: ToastService,
    private clientSv: ClientService,
    private eventEmitter: EventEmitterService,
    private modalSv: BsModalService,
    private _tableSv: TableService,
  ) {}

  ngOnInit() {
    this.eventReloadTable.subscribe((res) => {
      if (res && res.reason === RESV_MODE.REFRESH) {
        this._getTables(true);
      }
    });

    this.getClient();
    this._getTables();
  }

  ngOnDestroy(): void {
    if (this._modalRef) {
      this._modalRef.hide();
    }
  }

  private _getTables(isReloadKonvaLayout: boolean = false) {
    this.isLoading = true;
    this._tableSv.getTables().subscribe(
      (res) => {
        const configTable = [];

        this.canvasHandleEvent$.next({
          type: isReloadKonvaLayout ? KONVA_EMITTER_TYPE.RELOAD_KONVA_LAYOUT : KONVA_EMITTER_TYPE.RELOAD_KONVA,
          data: configTable,
        });

        this.isLoading = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  chooseTable($event) {
    setTimeout(() => {
      window.scrollTo(0, 1000);
    }, 0);

    this.tableSelected = $event;
    this.selectedQuantity = this.tableSelected.quantity;
  }

  private getClient() {
    this.clientSv.fetchClient().subscribe(
      (res) => {
        this.clientInfo = res;
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  eventClick($event) {
    if ($event.reservation.type === RESV_TYPE.WALK_IN) {
      this.eventEmitter.publishData({
        type: CALENDAR_EVENT_EMITTER.EDIT_WALKIN,
        data: cloneDeep($event.reservation),
      });
      return;
    }

    this.eventEmitter.publishData({
      type: RESV_MODE_EMITTER.EDIT,
      data: cloneDeep($event.reservation),
    });
  }

  resourcesClick($event) {
    if ($event.table) {
      this.eventEmitter.publishData({
        type: RESV_TABLE_EVENT_EMITTER.CHOOSE_TABLE,
        data: $event.table,
      });
    }
  }

  dayClicked(event) {
    this.eventEmitter.publishData({
      type: CALENDAR_EVENT_EMITTER.CREATE_WALKIN,
      data: new Walkin().deserialize({
        tables: [event.table],
        start_time: event.date.format('HH:mm:ss'),
        start_date: event.date.format('YYYY/MM/DD'),
        duration: DURATIONS_VALUE.VALUE_120,
      }),
    });
  }

  scrollEnd($event) {
    // TODO call api change data
  }

  eventDrop(event: { resv: Reservation | Walkin; oldTable: number; newTable: number; revertFunc: Function }) {
    const tables = event.resv.tables.map((t) => {
      return t.id === event.oldTable ? event.newTable : t.id;
    });

    const opts: ModalOptions = {
      class: 'modal-sm modal-dialog-centered',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        resvId: event.resv.id,
        tableIds: tables,
        revertFunc: event.revertFunc,
      },
    };

    this._openModalWithComponent(ModalUpdateTableComponent, opts);
  }

  private _openModalWithComponent(comp, opts: ModalOptions = {}) {
    this._modalRef = this.modalSv.show(comp, opts);
  }

  submitSelectedTable() {
    if (this.tableSelected.hasReservation) {
      this.startReservation();
    }
  }

  startReservation() {
    if (this.isLoading) return;

    this.isLoading = true;

    this.resvSv.startReservation(this.tableSelected.current_reservation.id).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastSv.success('予約が開始されました');
      },
      (errors) => {
        this.isLoading = false;
        this.toastSv.error(errors);
      },
    );
  }
}
