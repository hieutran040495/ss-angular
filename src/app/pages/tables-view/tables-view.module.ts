import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalUpdateTableModule } from 'app/pages/modal-update-table/modal-update-table.module';

import { CalendarModule } from 'shared/modules/calendar/calendar.module';
import { KonvaCanvasModule } from 'shared/modules/konva-canvas/konva-canvas.module';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';

import { TablesViewComponent } from './tables-view.component';

const routes: Routes = [
  {
    path: '',
    component: TablesViewComponent,
  },
];

@NgModule({
  declarations: [TablesViewComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    CalendarModule,
    KonvaCanvasModule,
    OnlyNumericModule,
    ModalModule.forRoot(),
    ModalUpdateTableModule,
  ],
})
export class TablesViewModule {}
