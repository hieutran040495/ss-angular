import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';

@Injectable()
export class PagesResolve implements Resolve<any> {
  constructor(private echoSv: LaravelEchoService) {}

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
    return;
  }
}
