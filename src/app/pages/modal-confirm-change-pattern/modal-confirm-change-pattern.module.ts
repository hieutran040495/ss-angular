import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalConfirmChangePatternComponent } from './modal-confirm-change-pattern.component';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

@NgModule({
  imports: [CommonModule, FormsModule, CasDialogModule],
  entryComponents: [ModalConfirmChangePatternComponent],
  declarations: [ModalConfirmChangePatternComponent],
  exports: [ModalConfirmChangePatternComponent],
})
export class ModalConfirmChangePatternModule {}
