import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SELF_ORDER } from 'shared/enums/event-emitter';

@Component({
  selector: 'app-modal-confirm-change-pattern',
  templateUrl: './modal-confirm-change-pattern.component.html',
  styleUrls: ['./modal-confirm-change-pattern.component.scss'],
})
export class ModalConfirmChangePatternComponent implements OnInit {
  isLoading: boolean = false;

  constructor(private modalSv: BsModalService, private bsModalRef: BsModalRef) {}

  ngOnInit() {}

  confirmChange() {
    this.closeModal(SELF_ORDER.CHANGE_PATTERN);
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
