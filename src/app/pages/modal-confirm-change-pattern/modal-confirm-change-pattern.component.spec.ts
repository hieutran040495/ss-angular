import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfirmChangePatternComponent } from './modal-confirm-change-pattern.component';

describe('ModalConfirmChangePatternComponent', () => {
  let component: ModalConfirmChangePatternComponent;
  let fixture: ComponentFixture<ModalConfirmChangePatternComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalConfirmChangePatternComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfirmChangePatternComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
