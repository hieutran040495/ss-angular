import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from './notification.component';
import { NotifyListModule } from '../notify-list/notify-list.module';
import { ClickOutsideModule } from 'shared/directive/click-outside/click-outside.module';

@NgModule({
  declarations: [NotificationComponent],
  imports: [CommonModule, NotifyListModule, ClickOutsideModule],
  exports: [NotificationComponent],
})
export class NotificationModule {}
