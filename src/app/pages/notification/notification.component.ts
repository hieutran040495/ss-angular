import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Input } from '@angular/core';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { Subscription } from 'rxjs/Subscription';
import { NOTIFY_CODE } from 'shared/enums/code-notify';
import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';
import { PusherEvent } from 'shared/models/event';
import { NotifyService } from 'shared/http-services/notify.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { RESV_MODE_EMITTER } from '../../../shared/enums/event-emitter';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent implements OnInit, OnDestroy {
  @ViewChild('NotifyRef') private notifyRef: ElementRef;
  @Input() totalUnseen: number;

  private pageSub: Subscription;
  isShowNotiList: boolean = false;

  constructor(
    private eventEmitter: EventEmitterService,
    private echoSv: LaravelEchoService,
    private notifySv: NotifyService,
    private toastSv: ToastService,
  ) {}

  ngOnInit() {
    this.onNotification();

    this.pageSub = this.eventEmitter.caseNumber$.subscribe((res) => {
      if (res.type === NOTIFY_CODE.READ_NOTIFY) {
        this.getCountNotify();
        this.clearUnseen(true);
      }
    });
  }

  ngOnDestroy() {
    if (this.pageSub) {
      this.pageSub.unsubscribe();
    }
  }

  private onNotification() {
    this.echoSv.onNotification((e: any) => {
      const event = new PusherEvent().deserialize(e);
      if (event.hasNotification && event.isReloadNotify) {
        this.getCountNotify();
      }
    });
  }

  private getCountNotify() {
    this.notifySv.getNotifyStatistics().subscribe(
      (res) => {
        this.totalUnseen = res.total_unseen;
        this.toggleShowNotify();
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  private toggleShowNotify() {
    if (this.totalUnseen > 0) {
      return this.notifyRef.nativeElement.classList.add('show');
    }

    if (this.totalUnseen === 0) {
      return this.notifyRef.nativeElement.classList.remove('show');
    }
  }

  clearUnseen(eventClick: boolean = false) {
    this.isShowNotiList = !this.isShowNotiList;

    if (!eventClick) {
      this.eventEmitter.publishData({
        type: RESV_MODE_EMITTER.CLOSE,
      });
    }

    if (!this.totalUnseen) {
      return;
    }

    this.notifySv.unSeenNotify().subscribe(
      (res) => {
        this.totalUnseen = 0;
      },
      (error) => {
        this.toastSv.error(error);
      },
    );
  }

  clickOutside($event) {
    this.isShowNotiList = false;
  }
}
