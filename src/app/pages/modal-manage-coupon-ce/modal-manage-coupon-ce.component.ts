import { Component, OnInit, ViewEncapsulation, AfterViewInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ValidatorService } from 'shared/utils/validator.service';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { Coupon } from 'shared/models/coupon';
import { COUPON_TARGET, USAGE_TYPE, COUPON_STATUS } from 'shared/enums/coupon';
import { Regexs } from 'shared/constants/regex';

import { CouponService } from 'shared/http-services/coupon.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { CouponDeleteComponent } from 'app/pages/setting/manage-coupon/coupon-delete/coupon-delete.component';

import { Subscription } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-modal-manage-coupon-ce',
  templateUrl: './modal-manage-coupon-ce.component.html',
  styleUrls: ['./modal-manage-coupon-ce.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ModalManageCouponCeComponent implements OnInit, AfterViewInit, OnDestroy {
  isSubmiting: boolean = false;

  coupon: Coupon = new Coupon();
  isPublish: boolean = false;
  isOriginPublish: boolean;

  minDateStart = moment().toDate();
  minDateEnd = moment()
    .add(1, 'day')
    .toDate();

  usage_count_greater: number;
  usage_count_equal: number;
  validation: any = {};

  rules = Regexs;

  COUPON_TARGET = COUPON_TARGET;
  USAGE_TYPE = USAGE_TYPE;
  COUPON_STATUS = COUPON_STATUS;

  get is_can_not_edit(): boolean {
    return this.coupon.id && !this.coupon.isDraft;
  }
  get is_edit(): boolean {
    return this.coupon && !!this.coupon.id;
  }
  get title_display(): string {
    return this.is_edit ? 'クーポン編集' : 'クーポン新規作成';
  }

  private modalSubscribe: Subscription;

  constructor(
    private couponSV: CouponService,
    private validatorSv: ValidatorService,
    private toastrSv: ToastService,
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private _cd: ChangeDetectorRef,
    private eventEmitterSv: EventEmitterService,
  ) {}

  ngOnInit() {}

  ngOnDestroy(): void {
    if (this.modalSubscribe) {
      this.modalSubscribe.unsubscribe();
    }
  }

  ngAfterViewInit() {
    if (this.coupon.id) {
      this._showCoupon();
    }
  }

  private _showCoupon() {
    const opts: any = {
      with: 'client',
    };
    this._cd.detectChanges();

    this.couponSV.showCoupon(this.coupon.id, opts).subscribe(
      (res) => {
        this.coupon = res;

        this.isPublish = this.coupon.isPublish;
        this.isOriginPublish = this.isPublish;

        if (this.coupon.isUsageEqual) {
          this.usage_count_equal = this.coupon.usage_count;
        } else {
          this.usage_count_greater = this.coupon.usage_count;
        }

        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
        this._cd.detectChanges();
      },
    );
  }

  submitCoupon(status: string) {
    if (this.isSubmiting) {
      return;
    }
    this.isSubmiting = true;

    this.coupon.status = status;
    this.coupon.usage_count = this.coupon.isUsageEqual ? this.usage_count_equal : this.usage_count_greater;

    this._cd.detectChanges();

    if (this.coupon.id) {
      this._updateCoupon();
    } else {
      this._createCoupon();
    }
  }

  deleteCoupon() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        couponId: this.coupon.id,
      },
    };

    this._openModalWithComponent(CouponDeleteComponent, opts);
  }

  private _createCoupon() {
    this.couponSV.createCoupon(this.coupon.formData()).subscribe(
      (res) => {
        this.toastrSv.success('クーポンを作成しました');

        this.isSubmiting = false;
        this.closeModal('reload');
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors.message);
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validatorSv.setErrors(errors.errors);
        }
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
    );
  }

  private _updateCoupon() {
    this.couponSV.updateCoupon(this.coupon.id, this.coupon.formData()).subscribe(
      (res) => {
        this.toastrSv.success('クーポンを更新しました');
        this.closeModal('reload');
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors.message);
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validatorSv.setErrors(errors.errors);
        }
        this.isSubmiting = false;
        this._cd.detectChanges();
      },
    );
  }

  changeStatus() {
    if (this.isPublish) {
      this.coupon.status = COUPON_STATUS.PUBLISH;
    } else {
      this.coupon.status = COUPON_STATUS.UNPUBLISH;
    }

    if (this.coupon.id) {
      this.publishCoupon();
    }
  }

  private publishCoupon() {
    this._cd.detectChanges();

    this.couponSV.publicCoupon(this.coupon.id, { status: this.coupon.status }).subscribe(
      (res) => {
        this.eventEmitterSv.publishData({
          type: 'publish-coupon',
          data: this.coupon,
        });
        if (this.coupon.isPublish) {
          this.toastrSv.success('クーポンを公開しました');
          this._cd.detectChanges();
          return;
        }
        this.toastrSv.success('クーポンを非公開にしました');
        this._cd.detectChanges();
      },
      (error) => {
        this.toastrSv.error(error);
        this._cd.detectChanges();
      },
    );
  }

  setUsageType() {
    if (this.coupon.isUsageEqual) {
      this.usage_count_greater = undefined;
    } else {
      this.usage_count_equal = undefined;
    }
  }

  resetValidation(key: string): void {
    this.validation[key] = undefined;
    this._cd.detectChanges();
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    if (this.isOriginPublish !== this.isPublish && this.is_edit) {
      this.modalSv.setDismissReason('reload');
    }
    this.bsModalRef.hide();
  }

  private _openModalWithComponent(comp, opts: ModalOptions = {}) {
    this.modalSubscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      if (reason === 'delete') {
        this.closeModal('delete');
      }
    });

    this.modalSv.show(comp, opts);
  }
}
