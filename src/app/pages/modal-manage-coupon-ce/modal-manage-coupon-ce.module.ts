import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalManageCouponCeComponent } from './modal-manage-coupon-ce.component';
import { FormsModule } from '@angular/forms';

import { UiSwitchModule } from 'shared/modules/ui-switch/index';
import { ValidatorService } from 'shared/utils/validator.service';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';
import { TimePickerModule } from 'shared/modules/time-picker/time-picker.module';

import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { CasMaxlengthModule } from 'shared/directive/cas-maxlength/cas-maxlength.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';

import { NgxDatepickerModule } from 'shared/modules/ngx-datepicker/ngx-datepicker.module';
import { DigitFormaterModule } from 'shared/modules/digit-formater/digit-formater.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { CouponDeleteModule } from 'app/pages/setting/manage-coupon/coupon-delete/coupon-delete.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    OnlyNumericModule,
    NgxDatepickerModule,
    TimePickerModule,
    UiSwitchModule.forRoot({
      size: 'small',
      bgColor: '#c3ed12',
    }),
    SpaceValidationModule,
    CasMaxlengthModule,
    CasNgSelectModule,
    DigitFormaterModule,
    CasDialogModule,
    CouponDeleteModule,
  ],
  declarations: [ModalManageCouponCeComponent],
  entryComponents: [ModalManageCouponCeComponent],
  exports: [ModalManageCouponCeComponent],
  providers: [ValidatorService],
})
export class ModalManageCouponCeModule {}
