import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalManageCouponCeComponent } from './modal-manage-coupon-ce.component';

describe('ModalManageCouponCeComponent', () => {
  let component: ModalManageCouponCeComponent;
  let fixture: ComponentFixture<ModalManageCouponCeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalManageCouponCeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalManageCouponCeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
