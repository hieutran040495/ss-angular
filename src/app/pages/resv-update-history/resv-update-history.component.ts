import { Component, OnDestroy, OnInit } from '@angular/core';

import { ReservationService } from 'shared/http-services/reservation.service';

import { ReservationHistory } from 'shared/models/reservation-history';
import { Pagination, PageChangedInput } from 'shared/models/pagination';
import { ToastService } from 'shared/logical-services/toast.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { Reservation } from 'shared/models/reservation';
import { RESV_MODE_EMITTER } from 'shared/enums/event-emitter';
import { TableHeader } from 'app/modules/cas-table/cas-table.component';
import { BehaviorSubject } from 'rxjs';

import { ResvUpdateHistoryFilter } from './resv-update-history-filter/resv-update-history-filter';
import { ClientProfileQuery } from 'shared/states/client-profile';

@Component({
  selector: 'app-resv-update-history',
  templateUrl: './resv-update-history.component.html',
  styleUrls: ['./resv-update-history.component.scss'],
})
export class ResvUpdateHistoryComponent implements OnInit, OnDestroy {
  resvHistorys: ReservationHistory[] = [];
  isLoading: boolean = false;
  isPaginate: boolean = false;
  pagination: Pagination = new Pagination();

  private _orderTerm: any = {};

  filterData = new BehaviorSubject<ResvUpdateHistoryFilter>(new ResvUpdateHistoryFilter());
  filterSearch: any;
  isUseSmpOrder: boolean;

  sortItems = [
    {
      name: '実行者',
      value: 'client_member.name',
      active: false,
    },
    {
      name: '予約者名',
      value: 'client_user_name',
      active: false,
    },
    {
      name: '予約日時',
      value: 'start_at',
      active: false,
    },
    {
      name: '予約更新日時',
      value: 'created_at',
      active: false,
    },
    {
      name: 'アクション',
      value: 'status',
      active: false,
    },
  ];

  tableHeader: Partial<TableHeader>[] = [
    {
      name: '実行者',
      class: 'text-center',
    },
    {
      name: `予約者名`,
      class: 'text-center',
    },
    {
      name: '予約日時',
      class: 'text-center',
    },
    {
      name: '予約更新日時',
      class: 'text-center',
    },
    {
      name: 'アクション',
      class: 'text-center',
    },
    {
      name: '',
    },
  ];

  searchConf: any = {
    placeholder: '名前を入力してください',
    button: '検索',
  };

  constructor(
    private resvSv: ReservationService,
    private toast: ToastService,
    private eventEmitter: EventEmitterService,
    public clientProfileQuery: ClientProfileQuery,
  ) {}

  ngOnInit() {
    this.getResvHistorys();
    this.getClientInfo();
  }

  ngOnDestroy(): void {
    this.filterData.next(null);
    this.filterData.complete();
  }

  private getResvHistorys() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.resvHistorys = [];

    const opts = {
      with: 'clientMember,reservation.clientUser',
      created_by_not: 'self-order',
      ...this.pagination.hasJSON(),
      ...this.filterSearch,
      ...this._orderTerm,
    };

    this.resvSv.getHistoryReservation(opts).subscribe(
      (res) => {
        this.resvHistorys = res.data;
        this.pagination.deserialize(res);
        this.isLoading = false;
        this.isPaginate = false;
      },
      (errors) => {
        this.toast.error(errors);
        this.isLoading = false;
        this.isPaginate = false;
      },
    );
  }

  navigate(resv: ReservationHistory) {
    this.goToResvDetails(resv);
  }

  private goToResvDetails(resv: ReservationHistory) {
    const opts = {
      with: `coupon,clientUser,tables,user,${this.isUseSmpOrder ? 'reservation_passcode' : ''}`,
    };
    this.resvSv.getResvById(resv.reservation_id, opts).subscribe(
      (res) => {
        this.eventEmitter.publishData({
          type: RESV_MODE_EMITTER.EDIT,
          data: new Reservation().deserialize(res),
        });
      },
      (errors) => {
        this.toast.error(errors);
      },
    );
  }

  private async getClientInfo() {
    const data = await this.clientProfileQuery.getClientProfile();
    this.isUseSmpOrder = data.allow_smp_order;
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;
    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.getResvHistorys();
  }

  updateSortTerm(sortTerm: any) {
    this._orderTerm = sortTerm;
    this.reservationFilter();
  }

  reservationFilter() {
    this.pagination.reset();
    const filterSubscribe = this.filterData.subscribe((res: ResvUpdateHistoryFilter) => {
      this.filterSearch = res.toJSON();
      this.getResvHistorys();
    });
    filterSubscribe.unsubscribe();
  }
}
