import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ResvUpdateHistoryFilter } from './resv-update-history-filter';
import { ToastService } from 'shared/logical-services/toast.service';
import * as moment from 'moment';

@Component({
  selector: 'app-resv-update-history-filter',
  templateUrl: './resv-update-history-filter.component.html',
  styleUrls: ['./resv-update-history-filter.component.scss'],
})
export class ResvUpdateHistoryFilterComponent implements OnInit {
  @Output() changeFilter: EventEmitter<any> = new EventEmitter<any>();
  @Input('disabled') disabled: boolean;

  @Input('filterTerm') filterTerm: ResvUpdateHistoryFilter;

  constructor(private _toastSv: ToastService) {}

  ngOnInit() {}

  filterResvUpdateHistory() {
    if (
      this.filterTerm.start_at_gte &&
      this.filterTerm.start_at_lte &&
      moment(this.filterTerm.start_at_gte).isAfter(moment(this.filterTerm.start_at_lte))
    ) {
      this._toastSv.warning(`終了日付は開始日付以降の日付で設定してください`);
      return;
    }

    if (
      this.filterTerm.created_at_gte &&
      this.filterTerm.created_at_lte &&
      moment(this.filterTerm.created_at_gte).isAfter(moment(this.filterTerm.created_at_lte))
    ) {
      this._toastSv.warning(`終了日付は開始日付以降の日付で設定してください`);
      return;
    }
    this.changeFilter.emit(this.filterTerm.toJSON());
  }
}
