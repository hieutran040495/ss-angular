import * as pickBy from 'lodash/pickBy';
import * as moment from 'moment';

export class ResvUpdateHistoryFilter {
  client_member_name: string;
  client_user_name: string;
  client_member_or_user_name: string;
  start_at_gte: Date;
  start_at_lte: Date;
  created_at_gte: Date;
  created_at_lte: Date;
  search_filter: string;

  constructor() {}

  public toJSON() {
    const output = pickBy(this, (value, key) => {
      return (
        ['client_member_name', 'client_user_name', 'client_member_or_user_name'].indexOf(key) >= 0 &&
        value &&
        value.trim()
      );
    });

    if (this.start_at_gte) {
      output.start_at_gte = moment(this.start_at_gte).format('YYYY-MM-DD');
    }

    if (this.start_at_lte) {
      output.start_at_lte = moment(this.start_at_lte).format('YYYY-MM-DD');
    }

    if (this.created_at_gte) {
      output.created_at_gte = moment(this.created_at_gte).format('YYYY-MM-DD');
    }

    if (this.created_at_lte) {
      output.created_at_lte = moment(this.created_at_lte).format('YYYY-MM-DD');
    }

    if (this.search_filter) {
      output.client_member_or_user_name = this.search_filter.trim();
    }

    return output;
  }
}
