import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResvUpdateHistoryFilterComponent } from './resv-update-history-filter.component';

describe('ResvUpdateHistoryFilterComponent', () => {
  let component: ResvUpdateHistoryFilterComponent;
  let fixture: ComponentFixture<ResvUpdateHistoryFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResvUpdateHistoryFilterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResvUpdateHistoryFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
