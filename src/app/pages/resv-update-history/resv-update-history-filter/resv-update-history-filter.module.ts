import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResvUpdateHistoryFilterComponent } from './resv-update-history-filter.component';
import { FormsModule } from '@angular/forms';
import { NgxDatepickerModule } from 'shared/modules/ngx-datepicker/ngx-datepicker.module';

@NgModule({
  imports: [CommonModule, FormsModule, NgxDatepickerModule],
  declarations: [ResvUpdateHistoryFilterComponent],
  exports: [ResvUpdateHistoryFilterComponent],
})
export class ResvUpdateHistoryFilterModule {}
