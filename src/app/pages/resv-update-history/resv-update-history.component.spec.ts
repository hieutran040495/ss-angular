import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResvUpdateHistoryComponent } from './resv-update-history.component';

describe('ResvUpdateHistoryComponent', () => {
  let component: ResvUpdateHistoryComponent;
  let fixture: ComponentFixture<ResvUpdateHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResvUpdateHistoryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResvUpdateHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
