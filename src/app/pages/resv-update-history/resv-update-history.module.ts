import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { ResvUpdateHistoryComponent } from './resv-update-history.component';
import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { ResvUpdateHistoryFilterModule } from './resv-update-history-filter/resv-update-history-filter.module';
import { CasTableModule } from 'app/modules/cas-table/cas-table.module';

const routes: Routes = [
  {
    path: '',
    component: ResvUpdateHistoryComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    CasPaginationModule.forRoot(),
    CasFilterModule,
    CasTableModule,
    ResvUpdateHistoryFilterModule,
  ],
  declarations: [ResvUpdateHistoryComponent],
})
export class ResvUpdateHistoryModule {}
