import { Component, OnInit, ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ToastService } from 'shared/logical-services/toast.service';
import { PaymentMethod } from 'shared/models/payment-method';
import { Subscription } from 'rxjs';
import { RemovePaymentMethodComponent } from '../remove-payment-method/remove-payment-method.component';
import { PaymentMethodService } from 'shared/http-services/payment-methods.service';
import { DIALOG_EVENT } from 'shared/enums/modes';

@Component({
  selector: 'app-ce-payment-method',
  templateUrl: './ce-payment-method.component.html',
  styleUrls: ['./ce-payment-method.component.scss'],
})
export class CePaymentMethodComponent implements OnInit {
  @Output() paymentTypeEvent = new EventEmitter<string>();
  public isLoading: boolean = false;
  public isShowPaymentInput: boolean = false;
  public isSelectPaymentMethod: boolean = false;

  public paymentMethodName: string = '';
  public selectedPaymentMethod: PaymentMethod = new PaymentMethod();

  public paymentMethods: PaymentMethod[] = [];

  private modalSub: Subscription;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private toastSv: ToastService,
    private paymentMethodSv: PaymentMethodService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.fetchPaymentMethods();
  }

  togglePaymentInput() {
    this.isShowPaymentInput = !this.isShowPaymentInput;
  }

  private fetchPaymentMethods() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;

    this.paymentMethodSv.fetchPaymentMethods({ payment_type: 'other' }).subscribe(
      (res) => {
        this.paymentMethods = res.data;

        if (this.selectedPaymentMethod) {
          const paymentIdx = this.paymentMethods.findIndex(
            (payment: PaymentMethod) => payment.id === this.selectedPaymentMethod.id,
          );
          if (paymentIdx !== -1) {
            this.paymentMethods[paymentIdx].active = true;
            this.selectedPaymentMethod.active = true;
          }
        }

        this.isLoading = false;
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  addPayment() {
    if (
      !this.paymentMethodName ||
      !(this.paymentMethodName && this.paymentMethodName.trim()) ||
      this.paymentMethods.length >= 10
    )
      return;

    const data = {
      name: this.paymentMethodName,
    };
    this.isLoading = true;
    this._cd.detectChanges();

    this.paymentMethodSv.createPaymentMethod(data).subscribe(
      (res) => {
        this.toastSv.success('その他会計を作成しました');
        this.paymentMethodName = '';
        this.isLoading = false;
        this.fetchPaymentMethods();
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  removePaymentMethod(item: PaymentMethod) {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        paymentMethod: item,
      },
    };

    this._openModalWithComponent(RemovePaymentMethodComponent, opts);
  }

  confirmPayment() {
    const paymentType: any = {
      type: DIALOG_EVENT.OTHER_PAYMENT_METHOD,
      data: this.selectedPaymentMethod.name,
    };
    this.closeDialog(JSON.stringify(paymentType));
  }

  choosePaymentMethod(item: PaymentMethod) {
    this.selectedPaymentMethod = item;
    this.isSelectPaymentMethod = true;
  }

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
    this.isLoading = false;
  }

  private _openModalWithComponent(comp, opts: ModalOptions) {
    this.modalSub = this.bsModalSv.onHidden.subscribe((reason: string) => {
      if (reason === DIALOG_EVENT.PAYMENT_METHOD_REMOVE) {
        this.fetchPaymentMethods();
      }

      this.modalSub.unsubscribe();
    });

    this.bsModalSv.show(comp, opts);
  }
}
