import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CePaymentMethodComponent } from './ce-payment-method.component';

describe('CePaymentMethodComponent', () => {
  let component: CePaymentMethodComponent;
  let fixture: ComponentFixture<CePaymentMethodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CePaymentMethodComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CePaymentMethodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
