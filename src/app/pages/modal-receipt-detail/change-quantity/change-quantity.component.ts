import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { Menu } from 'shared/models/menu';
import { MenuBuffet } from 'shared/models/menu-buffet';
import { Course } from 'shared/models/course';
import { DIALOG_EVENT } from 'shared/enums/modes';

import { ReservationItemService } from 'shared/http-services/reservation-item.service';
import { ToastService } from 'shared/logical-services/toast.service';

import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-change-quantity',
  templateUrl: './change-quantity.component.html',
  styleUrls: ['./change-quantity.component.scss'],
})
export class ChangeQuantityComponent implements OnInit {
  isLoading: boolean = false;

  receiptItem: Menu | MenuBuffet | Course;
  oldQuantity: number = 0;

  isConfirm: boolean = false;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private reservationItemSv: ReservationItemService,
    private toastSv: ToastService,
  ) {}

  ngOnInit() {
    this.oldQuantity = cloneDeep(this.receiptItem.quantity);
  }

  minusQuantity() {
    if (this.receiptItem.quantity <= 1) return;

    this.receiptItem.quantity -= 1;
  }

  plusQuantity() {
    if (this.receiptItem.quantity >= 99) return;

    this.receiptItem.quantity += 1;
  }

  confirmChangeQuantity() {
    this.isConfirm = true;
  }

  changeQuantity() {
    this.reservationItemSv
      .updateQuantity(this.receiptItem.reservation_item_id, { quantity: this.receiptItem.quantity })
      .subscribe(
        (res: any) => {
          this.toastSv.success('メニュー品数が変更されました。');
          this.closeDialog(DIALOG_EVENT.RECEIPT_UPDATE_QUANTITY_SUCCESS);
        },
        (error: any) => {
          this.toastSv.error(error);
        },
      );
  }

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
    this.isLoading = false;
  }
}
