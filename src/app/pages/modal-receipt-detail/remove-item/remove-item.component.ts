import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { DIALOG_EVENT } from 'shared/enums/modes';

import { Menu } from 'shared/models/menu';
import { MenuBuffet } from 'shared/models/menu-buffet';
import { Course } from 'shared/models/course';

import { ReservationItemService } from 'shared/http-services/reservation-item.service';
import { ToastService } from 'shared/logical-services/toast.service';

@Component({
  selector: 'app-remove-item',
  templateUrl: './remove-item.component.html',
  styleUrls: ['./remove-item.component.scss'],
})
export class RemoveItemComponent implements OnInit {
  isLoading: boolean = false;

  receiptItem: Menu | MenuBuffet | Course;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private toastSv: ToastService,
    private reservationItemSv: ReservationItemService,
  ) {}

  ngOnInit() {}

  removeItem() {
    if (this.isLoading) return;

    this.isLoading = true;

    this.reservationItemSv.removeById(this.receiptItem.reservation_item_id).subscribe(
      (res: any) => {
        this.toastSv.success('メニューが削除されました。');
        this.isLoading = false;
        this.closeDialog(DIALOG_EVENT.RECEIPT_REMOVE_MENU_ITEM_SUCCESS);
      },
      (error: any) => {
        this.toastSv.error(error);
        this.isLoading = false;
      },
    );
  }

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
    this.isLoading = false;
  }
}
