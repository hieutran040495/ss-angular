import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CasTableModule } from 'app/modules/cas-table/cas-table.module';
import { ModalUndoPaymentModule } from '../modal-undo-payment/modal-undo-payment.module';

import { ReservationItemService } from 'shared/http-services/reservation-item.service';
import { SquareService } from 'shared/logical-services/square.service';

import { ModalReceiptDetailComponent } from './modal-receipt-detail.component';
import { RemoveItemComponent } from './remove-item/remove-item.component';
import { ChangeQuantityComponent } from './change-quantity/change-quantity.component';
import { ChangeOptionsComponent } from './change-options/change-options.component';
import { LoadingReceiptComponent } from './loading-receipt/loading-receipt.component';
import { ErrorFieldComponent } from './error-field/error-field.component';
import { CePaymentMethodComponent } from './ce-payment-method/ce-payment-method.component';
import { RemovePaymentMethodComponent } from './remove-payment-method/remove-payment-method.component';

@NgModule({
  declarations: [
    ModalReceiptDetailComponent,
    RemoveItemComponent,
    ChangeQuantityComponent,
    ChangeOptionsComponent,
    LoadingReceiptComponent,
    ErrorFieldComponent,
    CePaymentMethodComponent,
    RemovePaymentMethodComponent,
  ],
  imports: [CommonModule, FormsModule, ModalModule.forRoot(), CasDialogModule, CasTableModule, ModalUndoPaymentModule],
  exports: [ModalReceiptDetailComponent],
  providers: [ReservationItemService, SquareService],
  entryComponents: [
    ModalReceiptDetailComponent,
    RemoveItemComponent,
    ChangeQuantityComponent,
    ChangeOptionsComponent,
    LoadingReceiptComponent,
    ErrorFieldComponent,
    CePaymentMethodComponent,
    RemovePaymentMethodComponent,
  ],
})
export class ModalReceiptDetailModule {}
