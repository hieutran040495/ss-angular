import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalReceiptDetailComponent } from './modal-receipt-detail.component';

describe('ModalRecieptDetailComponent', () => {
  let component: ModalReceiptDetailComponent;
  let fixture: ComponentFixture<ModalReceiptDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalReceiptDetailComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalReceiptDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
