import { Component, OnDestroy, OnInit } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ReservationItemService } from 'shared/http-services/reservation-item.service';
import { DeviceDetectorService } from 'shared/utils/device-detector';
import { SquareService } from 'shared/logical-services/square.service';

import { ClientProfileQuery } from 'shared/states/client-profile';
import { ClientProfileService } from 'shared/states/client-profile';

import { RESERVATION_RECEIPT_TYPE, RESV_ITEMS_TYPE, RESV_PAYMENT_BY } from 'shared/enums/reservation';
import { RESV_MODE_EMITTER } from 'shared/enums/event-emitter';
import { DIALOG_EVENT } from 'shared/enums/modes';

import { User } from 'shared/models/user';
import { Receipt } from 'shared/models/receipt';
import { Reservation } from 'shared/models/reservation';
import { Menu } from 'shared/models/menu';
import { MenuBuffet } from 'shared/models/menu-buffet';
import { Course } from 'shared/models/course';
import { Client } from 'shared/models/client';

import { ModalUndoPaymentComponent } from 'app/pages/modal-undo-payment/modal-undo-payment.component';
import { RemoveItemComponent } from './remove-item/remove-item.component';
import { ChangeQuantityComponent } from './change-quantity/change-quantity.component';
import { LoadingReceiptComponent } from './loading-receipt/loading-receipt.component';
import { ChangeOptionsComponent } from './change-options/change-options.component';
import { ErrorFieldComponent } from './error-field/error-field.component';
import { CePaymentMethodComponent } from './ce-payment-method/ce-payment-method.component';

import { Subscription, forkJoin } from 'rxjs';

import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-modal-receipt-detail',
  templateUrl: './modal-receipt-detail.component.html',
  styleUrls: ['./modal-receipt-detail.component.scss'],
})
export class ModalReceiptDetailComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  isSubmitting: boolean = false;
  isShowPaidResv: boolean = false;

  receipt: Receipt = new Receipt();
  user: User;

  isOutside: boolean = false;

  RESERVATION_PAYMENT_UNIT = RESV_PAYMENT_BY;
  private paymentUnit: string[] = ['square_cash', 'square_card'];
  get isPaymentSquare(): boolean {
    return this.paymentUnit.indexOf(this.receipt.payment_by) !== -1;
  }

  get receipt_title(): string {
    return this.receipt.isSelfOrder ? 'KIOSK会計詳細' : 'オーダー会計詳細';
  }

  private routerSub: Subscription;

  client: Client = new Client();
  page: number;
  receiptData: any = undefined;
  isUseSquare: boolean;
  isAllowPrinter: boolean;

  offNotify: Subscription;

  // isPaid: boolean = false;

  get isReceiptTotalLower(): boolean {
    return this.receipt.total_price < 100;
  }

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private resvSv: ReservationService,
    private toastSv: ToastService,
    private eventEmitterSv: EventEmitterService,
    private reservationItemSv: ReservationItemService,
    private deviceDetectorSv: DeviceDetectorService,
    private squareSv: SquareService,
    private router: Router,
    private clientProfileQuery: ClientProfileQuery,
    private clientProfileService: ClientProfileService,
  ) {
    this.getClientInfo();

    const handleTimeout = setTimeout(() => {
      if (this.receiptData) {
        if (this.receiptData.status === 'error' || this.receiptData.errorAnd) {
          this.errorPaymentCard();
        }
        if (this.receiptData.transaction_id || this.receiptData.client_transaction_id) {
          this.isUseSquare = true;
          this.paymentSuccessSquare(this.RESERVATION_PAYMENT_UNIT.SQUARE);
        }
      }
      clearTimeout(handleTimeout);
    }, 200);

    this.offNotify = this.eventEmitterSv.caseNumber$.subscribe((res: any) => {
      if (res && res.type === RESERVATION_RECEIPT_TYPE.RELOAD_DETAILS) {
        this.getReceiptDetail();
        if (this.isUseSquare) {
          this.getClientInfo();
        }
      }
    });
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    if (this.routerSub) {
      this.routerSub.unsubscribe();
    }
    if (this.offNotify) {
      this.offNotify.unsubscribe();
    }
  }

  closeDialog(reason?: string) {
    if (this.isShowPaidResv) {
      const reservation = new Reservation().deserialize(this.receipt);
      this.eventEmitterSv.publishData({
        type: RESV_MODE_EMITTER.EDIT,
        data: reservation,
      });
    }

    if (this.receiptData) {
      this.router.navigate([window.location.pathname]);
      this.receiptData = null;
    }

    if (!!this.receipt.paid_at) {
      reason = DIALOG_EVENT.RECEIPT_RELOAD;
    }

    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  // checkPaymentRsv() {
  //   this.isSubmitting = true;

  //   this.resvSv.checkPaymentRsv(this.receipt.id).subscribe(
  //     (res) => {
  //       this.isPaid = res.is_paid;
  //       this.isSubmitting = false;
  //     },
  //     (errors) => {
  //       this.toastSv.error(errors);
  //       this.isSubmitting = false;
  //     },
  //   );
  // }

  errorPaymentCard() {
    const opts: ModalOptions = {
      class: 'modal-md modal-dialog-centered',
      keyboard: false,
    };

    this._openModalWithComponent(ErrorFieldComponent, opts);
    this.receiptData = undefined;
  }

  paymentBySquare() {
    const opts: ModalOptions = {
      class: 'modal-loading',
      keyboard: false,
    };
    this._openModalWithComponent(LoadingReceiptComponent, opts);

    this.checkOpenSquare();
  }

  paymentInStore(paymentUnit: string) {
    const data: any = {
      payment_by: paymentUnit,
    };

    this.payment(data);
  }

  paymentSuccessSquare(paymentUnit: string) {
    const data: any = {
      payment_by: paymentUnit,
      transaction_id: this.receiptData.transaction_id,
      client_transaction_id: this.receiptData.client_transaction_id,
    };

    this.payment(data);
  }

  payment(data: any) {
    if (this.isSubmitting) {
      return;
    }

    this.isSubmitting = true;

    this.resvSv.paymentResv(this.receipt.id, data).subscribe(
      (res) => {
        this.receipt.paid_at = res.paid_at;
        this.receipt.payment_by = data.paymentUnit;
        this.toastSv.success('会計に成功しました');
        this.isSubmitting = false;
        this.receiptData = undefined;
        if (!this.receipt.isSelfOrder && this.isAllowPrinter) {
          let screen: string = 'receipts';
          if (this.router.url === '/reservations') {
            screen = 'reservations';
          }

          if (this.deviceDetectorSv.getMobileOperatingSystem() === 'iOS') {
            const urlPath = `${window.location.origin}/printer/${encodeURIComponent(
              `${this.receipt.receipt_code}_cs_${screen}_cs_${this.deviceDetectorSv.getBrowserSystem()}`,
            )}`;
            // @ts-ignore
            window.location = `webprnt://starmicronics.com/open?url=${urlPath}`;
          } else {
            this.toastSv.info('iPad上のみレシートが印刷できます');
          }
        }
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isSubmitting = false;
        this.receiptData = undefined;
      },
    );
  }

  cancelRequestPayment() {
    if (this.isSubmitting) {
      return;
    }

    this.isSubmitting = true;
    this.resvSv.cancelReqPayment(this.receipt.id).subscribe(
      (res) => {
        this.toastSv.success('会計リクエスト解除に成功しました');
        this.closeDialog(DIALOG_EVENT.RECEIPT_RELOAD);
        this.isSubmitting = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isSubmitting = false;
      },
    );
  }

  undoPaymentResv() {
    const data: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        receipt: this.receipt,
      },
    };
    this._openModalWithComponent(ModalUndoPaymentComponent, data);
  }

  private _openModalWithComponent(comp, opts: ModalOptions) {
    const modalSub = this.bsModalSv.onHidden.subscribe((reason: string) => {
      if (!reason) {
        return;
      }

      if (reason === DIALOG_EVENT.RECEIPT_UNDO_PAYMENT_SUCCESS) {
        this.receipt.paid_at = null;
        this.closeDialog(DIALOG_EVENT.RECEIPT_RELOAD);
      }

      if (reason.search(DIALOG_EVENT.OTHER_PAYMENT_METHOD) > 0) {
        const data: any = {
          payment_by: JSON.parse(reason).data,
        };

        this.payment(data);
      }

      modalSub.unsubscribe();
    });

    this.bsModalSv.show(comp, opts);
  }

  getPriceItem(item: Menu | MenuBuffet | Course): string {
    if (item.type === RESV_ITEMS_TYPE.ITEM) {
      if (item.total_price === 0) {
        return '0';
      }
      const price = item.total_price / item.quantity;
      return `${price.format()}`;
    }
    return item.price_format;
  }

  focusItem(receiptItem: Menu | MenuBuffet | Course) {
    if (this.receipt.is_paid || this.receipt.isSelfOrder) {
      return;
    }

    if (this.isOutside) {
      this.isOutside = false;
      return;
    }

    this.receipt.all_selection.filter((item: any) => {
      if (item.reservation_item_id === receiptItem.reservation_item_id) {
        item.isReceiptFocus = true;
      } else {
        item.isReceiptFocus = false;
      }
    });
  }

  closeItem(receiptItem: Menu | MenuBuffet | Course) {
    this.isOutside = true;
    receiptItem.isReceiptFocus = false;
  }

  removeItem(receiptItem: Menu | MenuBuffet | Course) {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        receiptItem: receiptItem,
      },
    };

    this._openModalWithComponent(RemoveItemComponent, opts);
  }

  changeQuantityItem(receiptItem: Menu | MenuBuffet | Course) {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        receiptItem: cloneDeep(receiptItem),
      },
    };

    this._openModalWithComponent(ChangeQuantityComponent, opts);
  }

  changeOptions(receiptItem: any) {
    this.reservationItemSv.getItemById({ item_id: receiptItem.id, order: 'item_menu_option_id' }).subscribe(
      (res: any) => {
        const opts: ModalOptions = {
          class: 'modal-dialog-centered modal-sm',
          ignoreBackdropClick: true,
          keyboard: false,
          initialState: {
            receiptId: receiptItem.reservation_item_id,
            receiptOpts: receiptItem.menu_options.map((opt: any) => opt.id),
            itemOptions: res.data,
          },
        };

        this._openModalWithComponent(ChangeOptionsComponent, opts);
      },
      (errors: any) => {
        this.toastSv.error(errors);
      },
    );
  }

  addPaymentMethod() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
    };

    this._openModalWithComponent(CePaymentMethodComponent, opts);
  }

  private getReceiptDetail() {
    if (this.isLoading) return;

    this.isLoading = true;

    const opts = {
      with: 'client_user,tables,coupon,reservation_passcode,latest_payment',
    };

    const itemOpts = {
      with: 'courses,items,buffets',
      reservation_id: this.receipt.id,
      is_free: 0,
    };

    const reservationDetails = this.resvSv.getResvById(this.receipt.id, opts);
    const reservationItems = this.resvSv.getResvItems(itemOpts);

    forkJoin([reservationDetails, reservationItems]).subscribe(
      (results: any) => {
        this.isLoading = false;
        this.receipt.deserialize({ ...results[0], ...results[1].data });
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  private checkOpenSquare() {
    const params = {
      receiptId: this.receipt.id,
      page: this.page,
    };

    if (this.deviceDetectorSv.getMobileOperatingSystem() === 'Android') {
      this.squareSv.openAndroidPOS(this.receipt.total_price, params, this.receipt.id);
    } else {
      this.squareSv.openIosPOS(this.receipt.total_price, params, this.receipt.id);
    }
  }

  private async getClientInfo() {
    await this.clientProfileService.getClientInfor();
    const data: Client = await this.clientProfileQuery.getClientProfile();
    this.isUseSquare = data.use_client_pos;
    this.isAllowPrinter = data.client_setting.allow_client_printer;
  }
}
