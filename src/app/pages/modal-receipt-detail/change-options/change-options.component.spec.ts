import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeOptionsComponent } from './change-options.component';

describe('ChangeOptionsComponent', () => {
  let component: ChangeOptionsComponent;
  let fixture: ComponentFixture<ChangeOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChangeOptionsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
