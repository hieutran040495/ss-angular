import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { ReservationItemService } from 'shared/http-services/reservation-item.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { DIALOG_EVENT } from '../../../../shared/enums/modes';

@Component({
  selector: 'app-change-options',
  templateUrl: './change-options.component.html',
  styleUrls: ['./change-options.component.scss'],
})
export class ChangeOptionsComponent implements OnInit {
  isLoading: boolean = false;

  receiptId: number;
  itemOptions: any[] = [];
  receiptOpts: number[] = [];

  isOptSingle: boolean = false;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private reservationItemSv: ReservationItemService,
    private toastSv: ToastService,
  ) {}

  ngOnInit() {
    this.receiptOpts.filter((itemId: number, idx: number) => {
      const optSingle = this.itemOptions.find((opt: any) => opt.id === itemId);

      if (optSingle.is_single) {
        this.isOptSingle = true;
      }
    });
  }

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
    this.isLoading = false;
  }

  clickOptions(optId: number) {
    const optIndex: number = this.receiptOpts.findIndex((opt: any) => opt === optId);
    const optSingle = this.itemOptions.find((opt: any) => opt.id === optId);

    if (optIndex !== -1) {
      this.receiptOpts.splice(optIndex, 1);
      if (optSingle.is_single) {
        this.isOptSingle = false;
      }
    } else {
      this.receiptOpts.push(optId);
      if (optSingle.is_single) {
        this.isOptSingle = true;
      }
    }
  }

  updateItemOpts() {
    if (this.isLoading) return;

    this.isLoading = true;

    this.reservationItemSv.updateOptions(this.receiptId, { options: this.receiptOpts }).subscribe(
      (res: any) => {
        this.isLoading = false;
        this.toastSv.success('オプションが変更されました。');
        this.closeDialog(DIALOG_EVENT.RECEIPT_UPDATE_OPTIONS_SUCCESS);
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  disableOptSingle(opt: any) {
    return this.isOptSingle && opt.is_single && !this.receiptOpts.find((optSub: number) => optSub === opt.id);
  }
}
