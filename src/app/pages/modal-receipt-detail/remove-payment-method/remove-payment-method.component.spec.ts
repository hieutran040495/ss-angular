import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemovePaymentMethodComponent } from './remove-payment-method.component';

describe('RemovePaymentMethodComponent', () => {
  let component: RemovePaymentMethodComponent;
  let fixture: ComponentFixture<RemovePaymentMethodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RemovePaymentMethodComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemovePaymentMethodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
