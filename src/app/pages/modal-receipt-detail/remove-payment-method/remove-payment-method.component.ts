import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastService } from 'shared/logical-services/toast.service';
import { PaymentMethod } from 'shared/models/payment-method';
import { PaymentMethodService } from 'shared/http-services/payment-methods.service';
import { DIALOG_EVENT } from 'shared/enums/modes';

@Component({
  selector: 'app-remove-payment-method',
  templateUrl: './remove-payment-method.component.html',
  styleUrls: ['./remove-payment-method.component.scss'],
})
export class RemovePaymentMethodComponent implements OnInit {
  public isLoading: boolean = false;
  public paymentMethod: PaymentMethod;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private toastSv: ToastService,
    private paymentMethodSv: PaymentMethodService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {}

  removePaymentMethod() {
    if (this.isLoading) return;
    this.isLoading = true;
    this._cd.detectChanges();

    this.paymentMethodSv.deletePaymentMethod(this.paymentMethod.id).subscribe(
      (res: any) => {
        this.toastSv.success('その他会計を削除しました');
        this.closeDialog(DIALOG_EVENT.PAYMENT_METHOD_REMOVE);
        this._cd.detectChanges();
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
    this.isLoading = false;
  }
}
