import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-loading-receipt',
  templateUrl: './loading-receipt.component.html',
  styleUrls: ['./loading-receipt.component.scss'],
})
export class LoadingReceiptComponent implements OnInit {
  constructor(private bsModalRef: BsModalRef) {}

  ngOnInit() {
    const closeModal = setTimeout(() => {
      this.bsModalRef.hide();
      clearTimeout(closeModal);
    }, 5000);
  }
}
