import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingReceiptComponent } from './loading-receipt.component';

describe('LoadingReceiptComponent', () => {
  let component: LoadingReceiptComponent;
  let fixture: ComponentFixture<LoadingReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoadingReceiptComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
