import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToastService } from 'shared/logical-services/toast.service';
import { Record } from 'shared/models/send-sales-record';
import { SendSalesRecordService } from 'shared/http-services/send-sales-record.service';

@Component({
  selector: 'app-send-sales-record',
  templateUrl: './send-sales-record.component.html',
  styleUrls: ['./send-sales-record.component.scss'],
})
export class SendSalesRecordComponent implements OnInit {
  recordSend: Record = new Record();
  settingDay: boolean = true;
  isLoading: boolean = false;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private sendMailSv: SendSalesRecordService,
    private toastrSv: ToastService,
  ) {}

  ngOnInit() {}

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  clearSendEmail() {
    if (this.recordSend.enable) {
      this.recordSend.email = null;
    }
  }
  submitSend() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.sendMailSv.sendEmail(this.recordSend).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastrSv.success('設定を保存しました。');
        this.closeDialog();
      },
      (error) => {
        this.isLoading = false;
        this.toastrSv.error(error);
      },
    );
  }
}
