import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SendSalesRecordComponent } from './send-sales-record.component';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { ValidatorService } from 'shared/utils/validator.service';
import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { UiSwitchModule } from 'shared/modules/ui-switch/index';
import { EmailValidatorModule } from 'shared/directive/email-validator/email-validator.module';

@NgModule({
  declarations: [SendSalesRecordComponent],
  imports: [
    CommonModule,
    CasDialogModule,
    SpaceValidationModule,
    FormsModule,
    EmailValidatorModule,
    UiSwitchModule.forRoot({
      size: 'small',
      bgColor: '#c3ed12',
    }),
  ],
  providers: [ValidatorService],
  exports: [SendSalesRecordComponent],
  entryComponents: [SendSalesRecordComponent],
})
export class SendSalesRecordModule {}
