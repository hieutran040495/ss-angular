import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendSalesRecordComponent } from './send-sales-record.component';

describe('SendSalesRecordComponent', () => {
  let component: SendSalesRecordComponent;
  let fixture: ComponentFixture<SendSalesRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SendSalesRecordComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendSalesRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
