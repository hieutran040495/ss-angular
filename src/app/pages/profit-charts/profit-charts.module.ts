import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap/modal';
import { BarChartModule } from 'shared/modules/bar-chart/bar-chart.module';
import { PieChartModule } from 'shared/modules/pie-chart/pie-chart.module';
import { CasMaxlengthModule } from 'shared/directive/cas-maxlength/cas-maxlength.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';
import { ProfitSelectDateModule } from 'app/pages/profit-select-date/profit-select-date.module';
import { SendSalesRecordModule } from './send-sales-record/send-sales-record.module';

import { ProfitChartsComponent } from './profit-charts.component';

@NgModule({
  declarations: [ProfitChartsComponent],
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    FormsModule,
    BarChartModule,
    PieChartModule,
    CasMaxlengthModule,
    CasNgSelectModule,
    ProfitSelectDateModule,
    SendSalesRecordModule,
  ],
  exports: [ProfitChartsComponent],
})
export class ProfitChartsModule {}
