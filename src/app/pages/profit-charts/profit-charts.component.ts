import { Component, OnInit, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { Subject, Observable, of, concat } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, tap, switchMap } from 'rxjs/operators';

import { ProfitService } from 'shared/http-services/profit.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { CategoryService } from 'shared/http-services/category.service';
import { ClientService } from 'shared/http-services/client.service';

import { PIE_CHART_PAYMENT, PIE_CHART_USER } from 'shared/enums/chart';
import { PROFIT_DATETIME } from 'shared/enums/profit';

import { Category } from 'shared/models/category';
import { ProfitSummary } from 'shared/models/profit-summary';
import { catSelectedDefault } from 'shared/constants/category-default';
import * as moment from 'moment';
import { SendSalesRecordComponent } from './send-sales-record/send-sales-record.component';

@Component({
  selector: 'app-profit-charts',
  templateUrl: './profit-charts.component.html',
  styleUrls: ['./profit-charts.component.scss'],
})
export class ProfitChartsComponent implements OnInit, AfterViewInit {
  @Input() profitType: string;

  private _dateOpts: any;
  get dateOpts(): any {
    return this._dateOpts;
  }
  set dateOpts(v: any) {
    this._dateOpts = v;
    if (v) {
      this.fetchChartsInfo();
    }
  }

  // sales summary
  profitSum: ProfitSummary;

  // bar chart
  dataBarChart: any[] = [];
  labelsBarChart: string[] = [];

  // data Analytics Customer
  dataAnalyticsCustomer: number[] = [];
  labelsAnalyticsCustomer: string[] = [];
  tooltipCustomer: string[] = [];
  customerDataNotFound: boolean;

  // data AnalyticsPayment
  dataAnalyticsPayment: number[] = [];
  labelsAnalyticsPayment: string[] = [];
  tooltipPayment: string[] = [];
  paymentDataNotFound: boolean;

  // data Analytics Category
  dataAnalyticsCategory: number[] = [];
  labelsAnalyticsCategory: string[] = [];
  tooltipCategory: string[] = [];
  categoryDataNotFound: boolean;

  // data Analytics Coupon
  dataAnalyticsCoupon: number[] = [];
  labelsAnalyticsCoupon: string[] = [];
  tooltipCoupon: string[] = [];
  couponDataNotFound: boolean;

  // Categories
  categories$: Observable<Category[]>;
  categoriesInput$: Subject<string> = new Subject<string>();
  selectedBarChartCategory: Category = catSelectedDefault;
  categoriesDefault: Category[] = [];

  isLoadCategory: boolean = false;
  isLoadingBarChart: boolean = false;
  customerLoadingChart: boolean = false;
  paymentLoadingChart: boolean = false;
  categoryLoadingChart: boolean = false;
  couponLoadingChart: boolean = false;

  get isLoadingAllChart(): boolean {
    return (
      this.isLoadingBarChart ||
      this.customerLoadingChart ||
      this.paymentLoadingChart ||
      this.categoryLoadingChart ||
      this.couponLoadingChart
    );
  }

  constructor(
    private bsModalSv: BsModalService,
    private profitSv: ProfitService,
    private toastSv: ToastService,
    private categorySv: CategoryService,
    private cd: ChangeDetectorRef,
    private clientSv: ClientService,
  ) {}

  ngOnInit() {
    this.fetchCategories();
  }

  ngAfterViewInit() {
    this.cd.detectChanges();
  }

  fetchChartsInfo() {
    this.getProfitDays();
    this.getProfitSummary();
    this.getAnalyticsCustomer();
    this.getAnalyticsPayment();
    this.getAnalyticsCategory();
    this.getAnalyticsCoupon();
  }

  private fetchCategories() {
    return this.categorySv.getCategories().subscribe(
      (res) => {
        this.categoriesDefault = [catSelectedDefault].concat(res);
      },
      (errors) => {
        this.toastSv.error(errors);
      },
      () => {
        this.searchCategories();
      },
    );
  }

  private searchCategories() {
    this.categories$ = concat(
      of(this.categoriesDefault),
      this.categoriesInput$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => (this.isLoadCategory = true)),
        switchMap((name) => {
          const opts: any = {
            order: '-updated',
          };

          if (name && name.trim().length > 0) {
            opts.name = name.trim();
          } else {
            delete opts.name;
          }

          return this.categorySv
            .getCategories(opts)
            .map((res) => {
              if (!name) {
                return [catSelectedDefault].concat(res);
              }
              return res;
            })
            .pipe(
              catchError(() => of([])),
              tap(() => (this.isLoadCategory = false)),
            );
        }),
      ),
    );
  }

  sendMail() {
    this.clientSv.fetchClient({ with: 'sale_email' }).subscribe(
      (res) => {
        const modalOpts: ModalOptions = {
          class: 'modal-dialog-centered modal-sm',
          keyboard: false,
          initialState: {
            recordSend: res.sale_email,
          },
          ignoreBackdropClick: true,
        };
        this.bsModalSv.show(SendSalesRecordComponent, modalOpts);
      },
      (error) => {
        this.toastSv.error(error);
      },
    );
  }

  getProfitSummary() {
    const opts = {
      end_at_gte: this.dateOpts.start_at,
      end_at_lte: this.dateOpts.end_at,
    };

    this.profitSv.profitSummary(opts).subscribe(
      (res) => {
        this.profitSum = new ProfitSummary().deserialize(res);
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  getProfitDays() {
    if (this.isLoadingBarChart) {
      return;
    }

    this.isLoadingBarChart = true;

    this.dataBarChart = [
      {
        data: null,
      },
    ];

    const opts = {
      end_at_gte: this.dateOpts.start_at,
      end_at_lte: this.dateOpts.end_at,
      category_id: this.selectedBarChartCategory ? this.selectedBarChartCategory.id : null,
    };

    this.profitSv.getAnalysisBarchartRevenue(opts).subscribe(
      (res) => {
        this.dataBarChart = [
          {
            data: res.data.map((item) => item.total_revenue),
          },
        ];
        this.labelsBarChart = res.data.map((item) => item.name);
        this.isLoadingBarChart = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoadingBarChart = false;
      },
    );
  }

  getAnalyticsCustomer() {
    this.dataAnalyticsCustomer = [];
    this.labelsAnalyticsCustomer = [];
    this.tooltipCustomer = [];
    this.customerLoadingChart = true;
    const opts = {
      end_at_gte: this.dateOpts.start_at,
      end_at_lte: this.dateOpts.end_at,
    };

    this.profitSv.getAnalyticsCustomer(opts).subscribe(
      (res) => {
        this.dataAnalyticsCustomer = Object.values(res);
        Object.keys(res).forEach((label) => {
          this.labelsAnalyticsCustomer.push(`${PIE_CHART_USER[label].truncateString(10)}： ${res[label]}`);
          this.tooltipCustomer.push(`${PIE_CHART_USER[label]}： ${res[label]}`);
        });
        if (!res) {
          this.customerDataNotFound = true;
        }
        this.customerLoadingChart = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.customerLoadingChart = false;
      },
    );
  }

  getAnalyticsPayment() {
    this.dataAnalyticsPayment = [];
    this.labelsAnalyticsPayment = [];
    this.tooltipPayment = [];
    this.paymentLoadingChart = true;
    const opts: any = {
      end_at_gte: this.dateOpts.start_at,
      end_at_lte: this.dateOpts.end_at,
    };

    this.profitSv.getAnalyticsPayment(opts).subscribe(
      (res) => {
        this.dataAnalyticsPayment = Object.values(res.data);
        Object.keys(res.data).forEach((label) => {
          this.labelsAnalyticsPayment.push(`${PIE_CHART_PAYMENT[label].truncateString(10)}： ${res.data[label]}`);
          this.tooltipPayment.push(`${PIE_CHART_PAYMENT[label]}： ${res.data[label]}`);
        });
        if (!res) {
          this.paymentDataNotFound = true;
        }
        this.paymentLoadingChart = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.paymentLoadingChart = false;
      },
    );
  }

  getAnalyticsCategory() {
    this.dataAnalyticsCategory = [];
    this.labelsAnalyticsCategory = [];
    this.tooltipCategory = [];
    this.categoryLoadingChart = true;
    const opts = {
      end_at_gte: this.dateOpts.start_at,
      end_at_lte: this.dateOpts.end_at,
    };

    this.profitSv.getAnalyticsCategory(opts).subscribe(
      (res) => {
        this.dataAnalyticsCategory = res.data.map((item) => item.used_count);
        res.data.forEach((item) => {
          this.labelsAnalyticsCategory.push(`${item.name.truncateString(10)}： ${item.used_count}`);
          this.tooltipCategory.push(`${item.name}： ${item.used_count}`);
        });
        if (res.data && !res.data.length) {
          // Still draw chart even dont have data
          this.dataAnalyticsCategory = [0];
          this.labelsAnalyticsCategory = [''];
          this.categoryDataNotFound = true;
        }
        this.categoryLoadingChart = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.categoryLoadingChart = false;
      },
    );
  }

  getAnalyticsCoupon() {
    this.dataAnalyticsCoupon = [];
    this.labelsAnalyticsCoupon = [];
    this.tooltipCoupon = [];
    this.couponLoadingChart = true;
    const opts = {
      end_at_gte: this.dateOpts.start_at,
      end_at_lte: this.dateOpts.end_at,
    };
    this.profitSv.getAnalyticsCoupon(opts).subscribe(
      (res) => {
        this.dataAnalyticsCoupon = res.data.map((item) => item.used_count);
        res.data.forEach((item) => {
          this.labelsAnalyticsCoupon.push(`${item.name.truncateString(10)}： ${item.used_count}`);
          this.tooltipCoupon.push(`${item.name}： ${item.used_count}`);
        });
        if (res.data && !res.data.length) {
          // Still draw chart even dont have data
          this.dataAnalyticsCoupon = [0];
          this.labelsAnalyticsCoupon = [''];
          this.couponDataNotFound = true;
        }
        this.couponLoadingChart = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.couponLoadingChart = false;
      },
    );
  }

  datetimeChanged($event: any) {
    switch (this.profitType) {
      case PROFIT_DATETIME.DAY:
        this.dateOpts = {
          start_at: $event.start_at,
          end_at: $event.end_at,
        };
        break;
      case PROFIT_DATETIME.WEEKS:
        this.dateOpts = {
          start_at: $event.weekStart,
          end_at: $event.weekEnd,
        };
        break;
      case PROFIT_DATETIME.MONTHS:
        this.dateOpts = {
          start_at: $event.month_start,
          end_at: $event.month_end,
        };
        break;
      default:
        this.dateOpts = {
          start_at: moment().toISOString(),
          end_at: moment().toISOString(),
        };
    }
  }
}
