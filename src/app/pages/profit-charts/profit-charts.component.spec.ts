import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitChartsComponent } from './profit-charts.component';

describe('ProfitChartsComponent', () => {
  let component: ProfitChartsComponent;
  let fixture: ComponentFixture<ProfitChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfitChartsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
