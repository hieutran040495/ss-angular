import { Component, OnInit, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { PhoneLookupService } from 'shared/http-services/phone-lookup.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ClientUser } from 'shared/models/client-user';
import { Regexs } from 'shared/constants/regex';

import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  /* tslint:disable-next-line: no-use-before-declare */
  useExisting: forwardRef(() => ResvPhoneLookupComponent),
  provide: NG_VALUE_ACCESSOR,
  multi: true,
};

@Component({
  selector: 'app-resv-phone-lookup',
  templateUrl: './resv-phone-lookup.component.html',
  styleUrls: ['./resv-phone-lookup.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
})
export class ResvPhoneLookupComponent implements OnInit, ControlValueAccessor {
  @Input() required: string;
  @Input() minlength: number;
  @Input() maxlength: number;
  @Input() isCashlessUser: boolean;
  @Input() clientUser: ClientUser;

  rules = Regexs;

  @Output() foundClientUser = new EventEmitter<ClientUser>();

  private _phone: string;
  get phone(): string {
    return this._phone;
  }
  set phone(v: string) {
    this._phone = v;
    this._onChangeCallback(v);
  }

  searchInput$ = new Subject<string>();
  inValidErr: boolean;
  isShowStatus: boolean = false;
  isLoading: boolean = false;
  currentUser = new ClientUser();
  hasOldInfo: boolean = false;

  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;

  constructor(private lookupSv: PhoneLookupService, private toast: ToastService) {}

  ngOnInit() {
    this.searchInput$
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
      )
      .subscribe((searchTerms) => {
        this.onLookupPhone(searchTerms);
      });
  }

  writeValue(value: any) {
    this.phone = value;
  }

  onTouched() {
    this._onTouchedCallback();
  }

  registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  onChangePhone($event, inValidErr: any) {
    let inputVal: string;
    this.isShowStatus = false;
    this.inValidErr = inValidErr;

    if (!inValidErr && $event) {
      inputVal = $event.trim();
      this.searchInput$.next(inputVal);
    }
  }

  onLookupPhone(phone: string) {
    if (this.isLoading) {
      return;
    }

    this.currentUser = this.clientUser;
    this.isLoading = true;
    const opts: any = {
      phone: phone,
    };
    this.lookupSv.getPhoneLookup(opts).subscribe(
      (res) => {
        if (!this.inValidErr) {
          this.isShowStatus = true;
        }
        this.isLoading = false;

        if (res.client_user) {
          this.hasOldInfo = true;
          this.currentUser.deserialize(res.client_user);
        }

        if (!res.client_user) {
          if (this.hasOldInfo) {
            this.currentUser.deserialize({ name: null, phone: this.phone, memo: null });
          }
          this.hasOldInfo = false;
        }

        this.foundClientUser.emit(this.currentUser);
      },
      (errors) => {
        this.toast.error(errors);
        this.isLoading = false;
      },
    );
  }
}
