import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';
import { PhoneValidationModule } from 'shared/directive/phone-validation/phone-validation.module';

import { ResvPhoneLookupComponent } from './resv-phone-lookup.component';

import { PhoneLookupService } from 'shared/http-services/phone-lookup.service';

@NgModule({
  imports: [CommonModule, FormsModule, OnlyNumericModule, PhoneValidationModule],
  declarations: [ResvPhoneLookupComponent],
  exports: [ResvPhoneLookupComponent],
  providers: [PhoneLookupService],
})
export class ResvPhoneLookupModule {}
