import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { TableHeader } from 'app/modules/cas-table/cas-table.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastService } from 'shared/logical-services/toast.service';
import { COOKING_MASTER, MASTER_TABLE_NAME, MASTER_STATUS } from 'shared/enums/cooking-master';
import { CookingMasterService } from 'shared/http-services/cooking-master';
import { MenuCookingDuration } from 'shared/models/menu-cooking-duration';
import { MenuCookingType } from 'shared/models/menu-cooking-type';
import { MenuSubStatus } from 'shared/models/menu-substatus';
import { ApproveMasterData } from 'shared/interfaces/approve-master-data';

@Component({
  selector: 'app-modal-preview-master-csv',
  templateUrl: './modal-preview-master-csv.component.html',
  styleUrls: ['./modal-preview-master-csv.component.scss'],
})
export class ModalPreviewMasterCsvComponent implements OnInit {
  tableHeader: Partial<TableHeader>[] = [];
  master_type: string;
  masters: MenuCookingDuration[] | MenuCookingType[] | MenuSubStatus[] = [];
  approveData: ApproveMasterData;
  isSubmiting: boolean = false;

  get master_table_before_update(): string {
    return `変更前${MASTER_TABLE_NAME[this.master_type]}`;
  }
  get master_table_after_update(): string {
    return `変更後${MASTER_TABLE_NAME[this.master_type]}`;
  }
  constructor(
    private bsModalRef: BsModalRef,
    private BsModalSv: BsModalService,
    private toastSv: ToastService,
    private cookingMasterSv: CookingMasterService,
    private _cd: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.tableHeader = [
      {
        name: 'マスターID',
        class: 'text-left',
        width: 80,
      },
      {
        name: this.master_table_before_update,
        width: 200,
        class: 'text-left',
      },
      {
        name: this.master_table_after_update,
        width: 200,
        class: 'text-left',
      },
      {
        name: '更新日時',
        width: 115,
        class: 'text-center',
      },
      {
        name: '作成日時',
        width: 115,
        class: 'text-center',
      },
    ];
  }

  updateMaster() {
    if (this.isSubmiting) {
      return;
    }
    this.isSubmiting = true;
    this._cd.detectChanges();

    switch (this.master_type) {
      case COOKING_MASTER.SUB_STATUS:
        this._subStatus();
        break;
      case COOKING_MASTER.COOKING_TYPE:
        this._cookingType();
        break;
      case COOKING_MASTER.TIME_LIMIT:
        this._timeLimit();
        break;
    }
  }

  closeDialog(reason?: string) {
    this.BsModalSv.setDismissReason(reason);
    this.isSubmiting = false;
    this._cd.detectChanges();
    this.bsModalRef.hide();
  }

  private _subStatus() {
    this.cookingMasterSv.approveSubStatus(this.approveData).subscribe(
      () => {
        this.toastSv.success('ファイルをアップロードしました');
        this.closeDialog();
      },
      () => {
        this.closeDialog(MASTER_STATUS.ERROR);
      },
    );
  }

  private _cookingType() {
    this.cookingMasterSv.approveCookingType(this.approveData).subscribe(
      () => {
        this.toastSv.success('ファイルをアップロードしました');
        this.closeDialog();
      },
      () => {
        this.closeDialog(MASTER_STATUS.ERROR);
      },
    );
  }

  private _timeLimit() {
    this.cookingMasterSv.approveTimeLimit(this.approveData).subscribe(
      () => {
        this.toastSv.success('ファイルをアップロードしました');
        this.closeDialog();
      },
      () => {
        this.closeDialog(MASTER_STATUS.ERROR);
      },
    );
  }

  nameDisplay(master: any): string {
    if (this.master_type === COOKING_MASTER.TIME_LIMIT) {
      return master.duration_display;
    }
    return master.name;
  }

  oldNameDisplay(master: any): string {
    if (this.master_type === COOKING_MASTER.TIME_LIMIT) {
      return master.old_duration_display;
    }
    return master.old_name;
  }
}
