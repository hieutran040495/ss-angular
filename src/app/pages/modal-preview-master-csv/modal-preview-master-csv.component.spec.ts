import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPreviewMasterCsvComponent } from './modal-preview-master-csv.component';

describe('ModalPreviewMasterCsvComponent', () => {
  let component: ModalPreviewMasterCsvComponent;
  let fixture: ComponentFixture<ModalPreviewMasterCsvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalPreviewMasterCsvComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPreviewMasterCsvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
