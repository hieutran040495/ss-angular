import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CasTableModule } from 'app/modules/cas-table/cas-table.module';

import { ModalPreviewMasterCsvComponent } from './modal-preview-master-csv.component';

@NgModule({
  declarations: [ModalPreviewMasterCsvComponent],
  imports: [CommonModule, CasDialogModule, ModalModule.forRoot(), CasTableModule],
  exports: [ModalPreviewMasterCsvComponent],
  entryComponents: [ModalPreviewMasterCsvComponent],
})
export class ModalPreviewMasterCsvModule {}
