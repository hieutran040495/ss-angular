import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuBuffetCeComponent } from './menu-buffet-ce.component';

describe('MenuBuffetCeComponent', () => {
  let component: MenuBuffetCeComponent;
  let fixture: ComponentFixture<MenuBuffetCeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MenuBuffetCeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuBuffetCeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
