import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ValidatorService } from 'shared/utils/validator.service';

import { MenuBuffetCeComponent } from './menu-buffet-ce.component';

import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { CasMaxlengthModule } from 'shared/directive/cas-maxlength/cas-maxlength.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';
import { DigitFormaterModule } from 'shared/modules/digit-formater/digit-formater.module';
import { UiSwitchModule } from 'shared/modules/ui-switch/index';

import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { CasDropzoneModule } from 'app/modules/cas-dropzone/cas-dropzone.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CasOrderItemsModule } from 'app/modules/cas-order-items/cas-order-items.module';
import { SelectMenuItemModule } from 'shared/modules/select-menu-item/select-menu-item.module';
import { BuffetDeleteModule } from './buffet-delete/buffet-delete.module';

@NgModule({
  declarations: [MenuBuffetCeComponent],
  imports: [
    CommonModule,
    FormsModule,
    OnlyNumericModule,
    TooltipModule.forRoot(),
    SpaceValidationModule,
    CasMaxlengthModule,
    CasNgSelectModule,
    DigitFormaterModule,
    CasDialogModule,
    CasDropzoneModule,
    ModalModule.forRoot(),
    CasOrderItemsModule,
    UiSwitchModule.forRoot({
      size: 'small',
      bgColor: '#c3ed12',
    }),
    SelectMenuItemModule,
    BuffetDeleteModule,
  ],
  providers: [ValidatorService],
  entryComponents: [MenuBuffetCeComponent],
  exports: [MenuBuffetCeComponent],
})
export class MenuBuffetCeModule {}
