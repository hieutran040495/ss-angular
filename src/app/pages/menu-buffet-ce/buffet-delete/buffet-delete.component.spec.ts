import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuffetDeleteComponent } from './buffet-delete.component';

describe('BuffetDeleteComponent', () => {
  let component: BuffetDeleteComponent;
  let fixture: ComponentFixture<BuffetDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuffetDeleteComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuffetDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
