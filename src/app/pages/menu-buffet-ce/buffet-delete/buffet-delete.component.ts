import { Component } from '@angular/core';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { MenuBuffetService } from 'shared/http-services/menu-buffet.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { DIALOG_EVENT } from 'shared/enums/modes';

@Component({
  selector: 'app-buffet-delete',
  templateUrl: './buffet-delete.component.html',
  styleUrls: ['./buffet-delete.component.scss'],
})
export class BuffetDeleteComponent {
  buffetId: number;
  isLoading: boolean = false;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private toastSv: ToastService,
    private menuBuffetSv: MenuBuffetService,
  ) {}

  buffetRemove() {
    if (this.isLoading) return;

    this.isLoading = true;

    this.menuBuffetSv.deleteMenuBuffet(this.buffetId).subscribe(
      (res: any) => {
        this.toastSv.success('放題メニューを削除しました');
        this.closeDialog(DIALOG_EVENT.BUFFET_REMOVE);
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
    this.isLoading = false;
  }
}
