import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

import { BuffetDeleteComponent } from './buffet-delete.component';

@NgModule({
  declarations: [BuffetDeleteComponent],
  imports: [CommonModule, CasDialogModule],
  entryComponents: [BuffetDeleteComponent],
  exports: [BuffetDeleteComponent],
})
export class BuffetDeleteModule {}
