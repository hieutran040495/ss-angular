import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';

import { MenuBuffetService } from 'shared/http-services/menu-buffet.service';

@Injectable()
export class MenuBuffetCeResolve implements Resolve<any> {
  constructor(private router: Router, private menuBuffetSV: MenuBuffetService) {}

  resolve(activeRoute: ActivatedRouteSnapshot) {
    if (activeRoute.params.buffetId && isNaN(Number(activeRoute.params.buffetId))) {
      return this.router.navigate(['/setting/menu-buffet']);
    }

    const opts: any = {
      with: 'items',
    };

    return this.menuBuffetSV.fetchMenuBuffetById(activeRoute.params.buffetId, opts);
  }
}
