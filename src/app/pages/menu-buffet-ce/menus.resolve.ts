import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { MenuService } from 'shared/http-services/menu.service';
import { Menu } from 'shared/models/menu';

import { Observable } from 'rxjs';

@Injectable()
export class MenusResolve implements Resolve<Observable<Menu[]>> {
  constructor(private menuSv: MenuService) {}

  resolve() {
    const opts: any = {
      order: '-updated',
      is_show: 1,
    };
    return this.menuSv.fetchMenus(opts);
  }
}
