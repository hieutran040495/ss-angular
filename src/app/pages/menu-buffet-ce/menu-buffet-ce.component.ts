import { Component, OnInit, ChangeDetectorRef, EventEmitter, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { MenuBuffet } from 'shared/models/menu-buffet';
import { Menu } from 'shared/models/menu';
import { Regexs } from 'shared/constants/regex';

import { FileUpload } from 'shared/interfaces/file-upload';

import { ValidatorService } from 'shared/utils/validator.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { MenuBuffetService } from 'shared/http-services/menu-buffet.service';

import { BuffetDeleteComponent } from './buffet-delete/buffet-delete.component';

import { DIALOG_EVENT } from 'shared/enums/modes';
import { DROPZONE_TYPE } from 'shared/enums/drop-zone';
import { OPTION_TYPE } from 'shared/enums/type-search-order';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-menu-buffet-ce',
  templateUrl: './menu-buffet-ce.component.html',
  styleUrls: ['./menu-buffet-ce.component.scss'],
})
export class MenuBuffetCeComponent implements OnInit, OnDestroy {
  menuBuffet: MenuBuffet = new MenuBuffet().deserialize({});

  eventDropzone: EventEmitter<any> = new EventEmitter<any>();

  isLoadMenu: boolean = false;
  menus: Menu[] = [];

  thumbnail: FileUpload;
  validation: any = {};
  validatorQuantity: any = {};
  validationImg: boolean = false;

  isLoading: boolean = false;
  isSetMenuBuffPrice: boolean = false;
  rules = Regexs;
  OPTION_TYPE = OPTION_TYPE;

  private pageSub;
  private modalSubscribe: Subscription;

  get is_invalid_items(): boolean {
    if (!this.menuBuffet.items || !this.menuBuffet.items.length) {
      return true;
    }
    const items = this.menuBuffet.items.filter((item: Menu) => {
      return !item.quantity;
    });
    return !!items.length;
  }

  get dialogTitle(): string {
    return this.menuBuffet.id ? '放題メニュー編集' : '放題メニュー作成';
  }

  constructor(
    private menuBuffetSv: MenuBuffetService,
    private validationSv: ValidatorService,
    private toastrSv: ToastService,
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private _cd: ChangeDetectorRef,
  ) {
    this.pageSub = this.eventDropzone.subscribe((res: any) => {
      if (!res.type) {
        return;
      }

      if (res.type === DROPZONE_TYPE.UPLOAD) {
        this.thumbnail = {
          key: 'image',
          value: res.data[0].origin,
          name: 'menuImg',
        };
        this._cd.detectChanges();
      }
    });
  }

  ngOnInit() {
    if (this.menuBuffet && this.menuBuffet.id) {
      this.menuBuffet.items = this.menuBuffet.items.filter((item) => !item.only_course);
      this.setPrice();
    }
  }

  ngOnDestroy(): void {
    this.pageSub.unsubscribe();
  }

  submitMenuBuffet(): void {
    if (this.menuBuffet.id) {
      this.updateMenuBuffet();
    } else {
      this.createMenuBuffet();
    }
  }

  private createMenuBuffet(): void {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    this._cd.detectChanges();
    this.menuBuffetSv.createMenuBuffet([this.thumbnail], this.menuBuffet.formData()).subscribe(
      (res) => {
        this.toastrSv.success('放題メニューを追加しました');
        this.isLoading = false;
        this.closeModal(DIALOG_EVENT.BUFFET_RELOAD);
        this._cd.detectChanges();
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validationSv.setErrors(errors.errors);
          this.validatorQuantity = this.validationSv.setErrorsQuatity(errors.errors);
          this.rederValidatorItems();
        }
        this.toastrSv.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  private updateMenuBuffet(): void {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    this._cd.detectChanges();

    this.menuBuffetSv.updateMenuBuffet(this.menuBuffet.id, [this.thumbnail], this.menuBuffet.formData()).subscribe(
      (res) => {
        this.toastrSv.success('放題メニューを編集しました');
        this.isLoading = false;
        this.closeModal(DIALOG_EVENT.BUFFET_RELOAD);
        this._cd.detectChanges();
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validationSv.setErrors(errors.errors);
          this.validatorQuantity = this.validationSv.setErrorsQuatity(errors.errors);
          this.rederValidatorItems();
        }
        this.toastrSv.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  private rederValidatorItems() {
    if (!this.validatorQuantity.items) {
      return;
    }
    this.menuBuffet.items.forEach((item, key) => {
      if (this.validatorQuantity.items) {
        this.menuBuffet.items[key].validator = this.validatorQuantity.items[key];
      }
    });

    this._cd.detectChanges();
  }

  updateInputPrice() {
    if (this.isSetMenuBuffPrice) {
      return;
    }

    let total: number = 0;

    if (this.menuBuffet.items.length !== 0) {
      this.menuBuffet.items.map((item: Menu) => {
        if (typeof item.quantity === 'undefined') {
          item.quantity = 1;
        }
        total += item.quantity * item.price;
      });
    }
    this.menuBuffet.price = total;

    this._cd.detectChanges();
  }

  validationFile(isValidation: boolean): void {
    this.validationImg = isValidation;
    this._cd.detectChanges();
  }

  clearMenu() {
    this.menuBuffet.items = [];
  }

  setPrice() {
    this.isSetMenuBuffPrice = true;
    this.resetValidation('price');
  }

  resetValidation(key: string): void {
    this.validation[key] = undefined;
    this._cd.detectChanges();
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  deleteBuffet() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        buffetId: this.menuBuffet.id,
      },
    };

    this._openModalWithComponent(BuffetDeleteComponent, opts);
  }

  private _openModalWithComponent(comp, opts: ModalOptions = {}) {
    this.modalSubscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      if (reason === DIALOG_EVENT.BUFFET_REMOVE) {
        this.closeModal(DIALOG_EVENT.BUFFET_REMOVE);
      }
    });

    this.modalSv.show(comp, opts);
  }
}
