import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalResvCancelComponent } from './modal-resv-cancel.component';

describe('ModalResvCancelComponent', () => {
  let component: ModalResvCancelComponent;
  let fixture: ComponentFixture<ModalResvCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalResvCancelComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalResvCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
