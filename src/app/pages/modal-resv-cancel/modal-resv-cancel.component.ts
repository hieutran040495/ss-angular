import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { RESV_MODE } from 'shared/enums/resv-mode';

@Component({
  selector: 'app-modal-resv-cancel',
  templateUrl: './modal-resv-cancel.component.html',
  styleUrls: ['./modal-resv-cancel.component.scss'],
})
export class ModalResvCancelComponent implements OnInit {
  isLoading: boolean = false;
  resvId: number;
  isWalkIn: boolean;

  get successMess(): string {
    return this.isWalkIn ? 'ウォークイン予約をキャンセルしました' : '予約をキャンセルしました';
  }

  constructor(
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private resvSv: ReservationService,
    private toast: ToastService,
  ) {}

  ngOnInit() {}

  cancelResv() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    this.resvSv.cancelReservation(this.resvId).subscribe(
      (res) => {
        this.toast.success(this.successMess);
        this.isLoading = false;
        this.closeModal(RESV_MODE.CANCEL);
      },
      (errors) => {
        this.toast.error(errors);
        this.isLoading = false;
      },
    );
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
