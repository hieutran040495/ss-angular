import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalResvCancelComponent } from './modal-resv-cancel.component';

@NgModule({
  imports: [CommonModule, FormsModule],
  entryComponents: [ModalResvCancelComponent],
  declarations: [ModalResvCancelComponent],
  exports: [ModalResvCancelComponent],
})
export class ModalResvCancelModule {}
