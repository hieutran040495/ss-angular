import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { DEFAULT_PERFECT_SCROLLBAR_CONFIG } from 'shared/constants/scroll-config';
import { PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { HistoryCallComponent } from './history-call.component';
import { HistoryCallDetailComponent } from './history-call-detail/history-call-detail.component';

const routes: Routes = [
  {
    path: '',
    component: HistoryCallComponent,
  },
];

@NgModule({
  declarations: [HistoryCallComponent, HistoryCallDetailComponent],
  imports: [CommonModule, RouterModule.forChild(routes), PerfectScrollbarModule, FormsModule, TooltipModule.forRoot()],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
  ],
})
export class HistoryCallModule {}
