import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { Router, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/filter';

import { Pagination } from 'shared/models/pagination';

import { ToastService } from 'shared/logical-services/toast.service';
import { HistoryCallService } from 'shared/http-services/history-call.service';

import { HistoryCall } from 'shared/models/history-call';
import * as moment from 'moment';

import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

import { TAB_UP_DOWN_KEY, SPACE_DELETE_BACKPAGE_KEY } from 'shared/constants/invalid-key-search';

@Component({
  selector: 'app-history-call',
  templateUrl: './history-call.component.html',
  styleUrls: ['./history-call.component.scss'],
})
export class HistoryCallComponent implements OnInit, AfterViewInit {
  @ViewChild(PerfectScrollbarComponent)
  componentScroll;

  isLoading: boolean = false;
  isFirstTime: boolean = true;
  isSearching: boolean = false;
  isLoadingHistoryDetail = false;

  historyCalls: HistoryCall[] = [];
  detailHistoryCalls: HistoryCall[] = [];
  selectedCall: HistoryCall;

  previousUrl: string;
  clientName: string = '';

  start_at = moment()
    .startOf('month')
    .toISOString();
  end_at = moment().toISOString();

  pagination: Pagination = new Pagination().deserialize({ current_page: null, per_page: 20 });
  next_page_token: string;
  prev_page_token: string;

  searchInput$: Subject<string> = new Subject<string>();

  constructor(private router: Router, private historyCallSv: HistoryCallService, private toastSv: ToastService) {
    this.router.events
      .filter((event) => event instanceof NavigationEnd)
      .subscribe((e) => {
        this.previousUrl = e['url'];
      });

    this.searchInput$
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
      )
      .subscribe(() => {
        this.pagination.reset();
        this.historyCalls = [];
        this.isSearching = true;
        this.getListHistoryCall();
      });
  }

  ngOnInit() {
    this.getListHistoryCall();
  }

  ngAfterViewInit() {
    this.eventScroll();
  }

  eventScroll() {
    this.componentScroll.directiveRef.elementRef.nativeElement.addEventListener('ps-y-reach-end', (e) => {
      if (!this.isLoading && this.pagination.hasNextPage() && !this.isSearching) {
        this.pagination.nextPage();
        this.getListHistoryCall();
      }
    });
  }

  getListHistoryCall() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    const opts = {
      ...this.pagination.hasJSON(),
      with: 'clientUser',
      start_at: this.start_at,
      end_at: this.end_at,
      phone_or_name: this.clientName.replace(/\s/g, '') || undefined,
    };

    this.historyCallSv.fetchHistoryCalls(opts).subscribe(
      (res) => {
        this.historyCalls = this.historyCalls.concat(res.data);

        if (this.isFirstTime && this.historyCalls.length !== 0) {
          this.selectedCall = this.historyCalls[0];
          this.historyCalls[0].is_show = true;
          this.fetchDetailHistoryCall(this.selectedCall);
        }

        this.pagination.deserialize(res);
        this.isLoading = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
      () => {
        this.isFirstTime = false;
        this.isSearching = false;
      },
    );
  }

  searchHistoryCalls(event) {
    const keyInput = event.which || event.keyCode;
    if (TAB_UP_DOWN_KEY.includes(keyInput)) {
      return;
    }
    if (SPACE_DELETE_BACKPAGE_KEY.includes(keyInput) && !!this.clientName.length && !this.clientName.trim()) {
      return;
    }

    this.searchInput$.next(this.clientName);
  }

  showDetailHistoryCall(call: HistoryCall) {
    this.selectedCall = call;
    this.historyCalls.forEach((item) => {
      item.is_show = false;
    });
    call.is_show = true;
    this.fetchDetailHistoryCall(call);
  }

  fetchDetailHistoryCall(call: HistoryCall) {
    if (this.isLoadingHistoryDetail) {
      return;
    }
    this.isLoadingHistoryDetail = true;
    this.detailHistoryCalls = [];

    const opts = {
      phone_or_name: call.user_name_phone,
    };

    this.historyCallSv.fetchHistoryCalls(opts).subscribe(
      (res) => {
        this.detailHistoryCalls = res.data;
        this.isLoadingHistoryDetail = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoadingHistoryDetail = false;
      },
    );
  }
}
