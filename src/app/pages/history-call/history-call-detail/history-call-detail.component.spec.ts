import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryCallDetailComponent } from './history-call-detail.component';

describe('HistoryCallDetailComponent', () => {
  let component: HistoryCallDetailComponent;
  let fixture: ComponentFixture<HistoryCallDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HistoryCallDetailComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryCallDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
