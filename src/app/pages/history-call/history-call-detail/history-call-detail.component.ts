import { Component, OnInit, Input } from '@angular/core';
import { HistoryCall } from 'shared/models/history-call';

@Component({
  selector: 'app-history-call-detail',
  templateUrl: './history-call-detail.component.html',
  styleUrls: ['./history-call-detail.component.scss'],
})
export class HistoryCallDetailComponent implements OnInit {
  private _historyCall: HistoryCall;
  get historyCall(): HistoryCall {
    return this._historyCall || new HistoryCall();
  }
  @Input('historyCall')
  set historyCall(v: HistoryCall) {
    this._historyCall = v;
  }

  private _detailHistoryCalls: HistoryCall[];
  get detailHistoryCalls(): HistoryCall[] {
    return this._detailHistoryCalls || [];
  }
  @Input('detailHistoryCalls')
  set detailHistoryCalls(v: HistoryCall[]) {
    this._detailHistoryCalls = v;
  }

  @Input('isLoadingHistoryDetail') isLoadingHistoryDetail: boolean;

  constructor() {}

  ngOnInit() {}
}
