import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalResvDetailsComponent } from './modal-resv-details.component';

describe('ModalResvDetailsComponent', () => {
  let component: ModalResvDetailsComponent;
  let fixture: ComponentFixture<ModalResvDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalResvDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalResvDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
