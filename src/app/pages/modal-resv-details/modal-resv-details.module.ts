import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalResvCancelModule } from 'app/pages/modal-resv-cancel/modal-resv-cancel.module';

import { ModalResvDetailsComponent } from './modal-resv-details.component';

@NgModule({
  imports: [CommonModule, FormsModule, ModalResvCancelModule],
  entryComponents: [ModalResvDetailsComponent],
  declarations: [ModalResvDetailsComponent],
  exports: [ModalResvDetailsComponent],
})
export class ModalResvDetailsModule {}
