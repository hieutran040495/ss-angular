import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Reservation } from 'shared/models/reservation';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ToastService } from 'shared/logical-services/toast.service';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ModalResvCancelComponent } from 'app/pages/modal-resv-cancel/modal-resv-cancel.component';
import { RESV_MODE } from 'shared/enums/resv-mode';

@Component({
  selector: 'app-modal-resv-details',
  templateUrl: './modal-resv-details.component.html',
  styleUrls: ['./modal-resv-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalResvDetailsComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  reservation: Reservation = new Reservation();
  reservationId: number;

  constructor(
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private resvSv: ReservationService,
    private toast: ToastService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.getResvById();
  }

  ngOnDestroy() {
    if (this.bsModalRef) {
      this.bsModalRef.hide();
    }
  }

  getResvById() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    const opts: any = {
      with: 'coupon,clientUser,tables,user,client_member',
    };

    this.resvSv.getResvById(this.reservationId, opts).subscribe(
      (res) => {
        this.isLoading = false;
        this.reservation.deserialize(res);
      },
      (errors) => {
        this.toast.error(errors);
        this.isLoading = false;
      },
      () => {
        this.getResvItems();
      },
    );
  }

  getResvItems() {
    const opts = {
      with: 'courses,items,buffets',
      reservation_id: this.reservationId,
    };

    this.resvSv.getResvItems(opts).subscribe(
      (res) => {
        this.reservation.deserialize(res.data);
        this.cdRef.markForCheck();
      },
      (errors) => {
        this.toast.error(errors);
      },
    );
  }

  openModalResvCancel() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered',
      initialState: {
        resvId: this.reservationId,
      },
      keyboard: false,
      ignoreBackdropClick: true,
    };

    this.openModalWithComponent(ModalResvCancelComponent, opts);
  }

  private openModalWithComponent(comp, opts: any = {}) {
    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();
      if (reason === RESV_MODE.CANCEL) {
        this.closeModal(RESV_MODE.REMOVE);
      }
    });

    this.modalSv.show(comp, opts);
  }

  sendMessage() {
    this.closeModal();
    this.router.navigate(['/reservations', this.reservation.id, 'email']);
  }

  navigateEditResv() {
    this.router.navigate(['/reservations', this.reservation.id, 'edit']);
  }

  navigateEditTable() {
    this.router.navigate(['/reservations', this.reservation.id, 'table']);
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
