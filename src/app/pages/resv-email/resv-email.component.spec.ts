import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResvEmailComponent } from './resv-email.component';

describe('ResvEmailComponent', () => {
  let component: ResvEmailComponent;
  let fixture: ComponentFixture<ResvEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResvEmailComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResvEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
