import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ResvEmailComponent } from './resv-email.component';
import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';

const routes: Routes = [
  {
    path: '',
    component: ResvEmailComponent,
  },
];

@NgModule({
  declarations: [ResvEmailComponent],
  imports: [CommonModule, FormsModule, RouterModule.forChild(routes), SpaceValidationModule],
})
export class ResvEmailModule {}
