import { Component, OnInit } from '@angular/core';
import { ResvEmail } from 'shared/models/resv-email';
import { Regexs } from 'shared/constants/regex';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Reservation } from 'shared/models/reservation';

@Component({
  selector: 'app-resv-email',
  templateUrl: './resv-email.component.html',
  styleUrls: ['./resv-email.component.scss'],
})
export class ResvEmailComponent implements OnInit {
  isLoading: boolean = false;
  resvEmail: ResvEmail = new ResvEmail();
  resv: Reservation = new Reservation();
  rules = Regexs;

  constructor(
    private resvSv: ReservationService,
    private toastSv: ToastService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {
    this.activatedRoute.params.subscribe((res) => {
      if (res.id) {
        this.getResvById(+res.id);
      }
    });
  }

  ngOnInit() {}

  getResvById(resvId: number) {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    const opts = {
      with: 'user',
    };

    this.resvSv.getResvById(resvId, opts).subscribe(
      (res) => {
        this.resv = new Reservation().deserialize(res);
        this.resvEmail.email = this.resv.user.email;
        this.isLoading = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  sendEmail() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;

    this.resvSv.sendEmail(this.resvEmail.formData()).subscribe(
      (res) => {
        this.toastSv.success('メールを送信しました');
        this.router.navigate(['/reservations']);
        this.isLoading = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }
}
