import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalResvWalkinComponent } from './modal-resv-walkin.component';
import { ValidatorService } from 'shared/utils/validator.service';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';
import { PhoneValidationModule } from 'shared/directive/phone-validation/phone-validation.module';
import { TimePickerModule } from 'shared/modules/time-picker/time-picker.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';

@NgModule({
  imports: [CommonModule, FormsModule, CasNgSelectModule, OnlyNumericModule, PhoneValidationModule, TimePickerModule],
  declarations: [ModalResvWalkinComponent],
  entryComponents: [ModalResvWalkinComponent],
  exports: [ModalResvWalkinComponent],
  providers: [ValidatorService],
})
export class ModalResvWalkinModule {}
