import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalResvWalkinComponent } from './modal-resv-walkin.component';

describe('ModalResvWalkinComponent', () => {
  let component: ModalResvWalkinComponent;
  let fixture: ComponentFixture<ModalResvWalkinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalResvWalkinComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalResvWalkinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
