import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { Walkin } from 'shared/models/walkin';
import { Table } from 'shared/models/table';
import { Client } from 'shared/models/client';
import { getTimeRange } from 'shared/constants/time-range';
import { Regexs } from 'shared/constants/regex';

import { PhoneLookupService } from 'shared/http-services/phone-lookup.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ValidatorService } from 'shared/utils/validator.service';
import * as moment from 'moment';
import { RESV_MODE } from 'shared/enums/resv-mode';
import { TableService } from 'shared/http-services/table.service';

@Component({
  selector: 'app-modal-resv-walkin',
  templateUrl: './modal-resv-walkin.component.html',
  styleUrls: ['./modal-resv-walkin.component.scss'],
})
export class ModalResvWalkinComponent implements OnInit {
  walkin: Walkin = new Walkin();
  clientInfo: Client;
  walkinData: any;
  validator: any = {};
  rules = Regexs;

  tables: Table[];
  quantities: any[];
  times: Object[] = getTimeRange();

  minTime: string = moment().format('HH:mm');
  maxTime: string = moment()
    .add(1, 'hour')
    .format('HH:mm');

  dateOptions = {
    start_at: moment().toISOString(),
    end_at: moment()
      .add(1, 'hour')
      .toISOString(),
  };

  isLoading: boolean = false;
  isLoadTable: boolean = false;

  get isErrorOutOfRange(): boolean {
    if (!this.walkin || !this.walkin.start_time) {
      return;
    }
    return this.clientInfo.isOutOfWorkingRange(this.walkin.start_time);
  }

  constructor(
    private bsModalRef: BsModalRef,
    private bsModalService: BsModalService,
    private lookupSv: PhoneLookupService,
    private toastSv: ToastService,
    private resvSv: ReservationService,
    private _validationSv: ValidatorService,
    private tableSv: TableService,
  ) {}

  ngOnInit() {
    this.initialData();
    this.getTables();
    this.renderTableQuantities();
  }

  initialData() {
    const data: any = {
      client_user: {
        name: 'ウォークイン',
      },
      tables: this.walkinData ? [this.walkinData.table] : [],
      start_time: this.walkinData && this.walkinData.startTime ? this.walkinData.startTime : moment().toISOString(),
    };

    this.walkin.deserialize(data);
  }

  private getTables() {
    if (this.isLoadTable) {
      return;
    }

    this.isLoadTable = true;

    const opts = {
      ...this.dateOptions,
      with: 'type,reservations_count',
      order: 'reservation.start,created_at',
    };

    this.tableSv.getTables(opts).subscribe(
      (res) => {
        this.tables = res.data.filter((item) => item.reservations_count === 0);

        const table_ids = this.tables.map((item) => item.id);
        this.walkin.tables = this.walkin.tables.filter((item) => table_ids.indexOf(item.id) >= 0);

        this.isLoadTable = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoadTable = false;
      },
    );
  }

  onLookupPhone() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;

    const opts: any = {
      phone: this.walkin.client_user.phone,
    };
    this.lookupSv.getPhoneLookup(opts).subscribe(
      (res) => {
        if (res.client_user) {
          this.walkin.client_user.deserialize(res.client_user);
        } else {
          this.walkin.client_user.name = null;
        }
        this.isLoading = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  onCheckTables() {
    this.dateOptions.start_at = moment(this.walkin.start_at).toISOString();
    this.getTables();
  }

  renderTableQuantities() {
    this.quantities = [];
    for (let i = 1; i <= 100; i++) {
      this.quantities.push({
        label: `${i}人`,
        value: i,
      });
    }
  }

  createWalkinResv() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.resvSv.createResvWalkIn(this.walkin.formData()).subscribe(
      (res) => {
        this.toastSv.success('ウォークインを作成しました');
        this.closeModal(RESV_MODE.REFRESH);
        this.isLoading = false;
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validator = this._validationSv.setErrors(errors.errors);
        }
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  selectTime(event: { time: string }) {
    this.walkin.start_time = event.time;
    this.resetValidator('start_at');
  }

  closeModal(reason?: string) {
    this.bsModalService.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  resetValidator(key: string) {
    if (this.validator[key]) {
      this.validator[key] = undefined;
    }
  }

  clearTable(table: Table) {
    this.walkin.tables = this.walkin.tables.filter((item) => item.id !== table.id);
  }
}
