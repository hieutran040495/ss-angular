import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalNotifyUploadMasterComponent } from './modal-notify-upload-master.component';

describe('ModalNotifyUploadMasterComponent', () => {
  let component: ModalNotifyUploadMasterComponent;
  let fixture: ComponentFixture<ModalNotifyUploadMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalNotifyUploadMasterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalNotifyUploadMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
