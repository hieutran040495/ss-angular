import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-modal-notify-upload-master',
  templateUrl: './modal-notify-upload-master.component.html',
  styleUrls: ['./modal-notify-upload-master.component.scss'],
})
export class ModalNotifyUploadMasterComponent implements OnInit {
  constructor(private bsModalRef: BsModalRef) {}
  ngOnInit() {}

  closeDialog() {
    this.bsModalRef.hide();
  }
}
