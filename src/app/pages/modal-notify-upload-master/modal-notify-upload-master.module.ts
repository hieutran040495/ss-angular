import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { ModalModule } from 'ngx-bootstrap/modal';

import { ModalNotifyUploadMasterComponent } from './modal-notify-upload-master.component';

@NgModule({
  declarations: [ModalNotifyUploadMasterComponent],
  imports: [CommonModule, CasDialogModule, ModalModule.forRoot()],
  exports: [ModalNotifyUploadMasterComponent],
  entryComponents: [ModalNotifyUploadMasterComponent],
})
export class ModalNotifyUploadMasterModule {}
