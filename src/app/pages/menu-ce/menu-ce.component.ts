import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, EventEmitter, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { Menu } from 'shared/models/menu';
import { Category } from 'shared/models/category';
import { FileUpload } from 'shared/interfaces/file-upload';
import { Regexs } from 'shared/constants/regex';
import { DIALOG_EVENT } from 'shared/enums/modes';
import { MENU_TYPE_TOOGLE } from 'shared/enums/menu-type';

import { MenuService } from 'shared/http-services/menu.service';
import { CategoryService } from 'shared/http-services/category.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ValidatorService } from 'shared/utils/validator.service';

import { Subject, of, Subscription, BehaviorSubject } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, tap, switchMap } from 'rxjs/operators';
import { MenuOptionService } from 'shared/http-services/menu-option.service';
import { MenuOption } from 'shared/models/menu-option';
import { Pagination } from 'shared/models/pagination';
import { DROPZONE_TYPE } from 'shared/enums/drop-zone';
import { MenuAdditionService } from 'shared/http-services/menu-addition.service';
import { MenuSubStatus } from 'shared/models/menu-substatus';
import { MenuCookingType } from 'shared/models/menu-cooking-type';
import { MenuCookingDuration } from 'shared/models/menu-cooking-duration';
import { OrdinalInput } from 'shared/interfaces/ordinal-input';

import { MenuRemoveComponent } from './menu-remove/menu-remove.component';
import { ClientService } from 'shared/http-services/client.service';

interface ValidationInput {
  name: string | undefined;
  ordinal_number: string | undefined;
  internal_name: string | undefined;
  category_id: string | number | undefined;
  price: string | number | undefined;
  image: string | File | Blob | undefined;
  menu_option_ids: string;
  cooking_type_id: string | number | undefined;
  cooking_duration_id: string | number | undefined;
  substatus_id: string | number | undefined;
  nameLang: string | undefined;
}

@Component({
  selector: 'app-menu-create',
  templateUrl: './menu-ce.component.html',
  styleUrls: ['./menu-ce.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuCeComponent implements OnInit, OnDestroy {
  menu: Menu = new Menu();
  menuFile: FileUpload[] = [];

  eventDropzone: EventEmitter<any> = new EventEmitter<any>();

  isLoading: boolean = false;

  isLoadCategory: boolean = false;
  categories: Category[] = [];
  categoriesInput$: Subject<string> = new Subject<string>();
  paginationCats: Pagination = new Pagination();
  searchCat: string;
  subCatSearch: Subscription;

  isLoadMenuOption: boolean = false;
  menuOptions: MenuOption[] = [];
  menuOptionsInput$: Subject<string> = new Subject<string>();
  paginationOpt: Pagination = new Pagination();
  searchOpt: string;
  subOptSearch: Subscription;

  isLoadStatus: boolean = false;
  statuses: MenuSubStatus[] = [];
  statusesInput$: Subject<string> = new Subject<string>();
  paginationStatuses: Pagination = new Pagination();
  searchStatuses: string;
  subStatusesSearch: Subscription;

  isLoadCookType: boolean = false;
  cookTypes: MenuCookingType[] = [];
  cookTypesInput$: Subject<string> = new Subject<string>();
  paginationCookTypes: Pagination = new Pagination();
  searchCookTypes: string;
  subCookTypesSearch: Subscription;

  isLoadCookDuration: boolean = false;
  cookDurations: MenuCookingDuration[] = [];
  cookDurationsInput$: Subject<string> = new Subject<string>();
  paginationCookDurations: Pagination = new Pagination();
  searchCookDurations: string;
  subCookDurationsSearch: Subscription;

  validation: ValidationInput = {
    name: undefined,
    ordinal_number: undefined,
    internal_name: undefined,
    category_id: undefined,
    price: undefined,
    image: undefined,
    menu_option_ids: undefined,
    cooking_type_id: undefined,
    cooking_duration_id: undefined,
    substatus_id: undefined,
    nameLang: undefined,
  };
  validationImg: boolean = false;
  validationOptions: boolean = false;

  rules = Regexs;
  MENU_TYPE_TOOGLE = MENU_TYPE_TOOGLE;

  ordinalInput: OrdinalInput;

  languages: any[] = [];

  languagesArr: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([
    {
      type: 'name_en',
      name: '英語',
      value: '',
      image: 'assets/icon/flag_english.svg',
      placeholder: 'メニュー名を英語で入力してください',
      isSelected: true,
    },
    {
      type: 'name_zh_hans',
      name: '中国語(簡体)',
      value: '',
      image: 'assets/icon/flag_china.svg',
      placeholder: 'メニュー名を中国語(簡体)で入力してください',
      isSelected: true,
    },
    {
      type: 'name_zh_hant',
      name: '中国語(繁体)',
      value: '',
      image: 'assets/icon/flag_china.svg',
      placeholder: 'メニュー名を中国語(繁体)で入力してください',
      isSelected: true,
    },
    {
      type: 'name_ko',
      name: '韓国語',
      value: '',
      image: 'assets/icon/flag_korea.svg',
      placeholder: 'メニュー名を韓国語で入力してください',
      isSelected: true,
    },
  ]);
  private nameMultiple: string[] = ['name_en', 'name_zh_hans', 'name_zh_hant', 'name_ko'];

  private pageSub;
  private modalSubscribe: Subscription;

  get isEdit(): boolean {
    return this.menu && !!this.menu.id;
  }

  constructor(
    private menuSv: MenuService,
    private categorySv: CategoryService,
    private menuOptionSv: MenuOptionService,
    private validationSv: ValidatorService,
    private toastrSv: ToastService,
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private menuAdditionSv: MenuAdditionService,
    private _cd: ChangeDetectorRef,
    private _clientService: ClientService,
  ) {
    this.pageSub = this.eventDropzone.subscribe((res: any) => {
      if (!res.type) {
        return;
      }

      switch (res.type) {
        case DROPZONE_TYPE.UPLOAD:
          this.menuFile.push({
            key: 'image',
            value: res.data[0].origin,
            name: 'menuImg',
          });
          break;
        case DROPZONE_TYPE.REMOVE:
          this.deleteImage();
          break;
      }
    });
  }

  ngOnInit() {
    if (!this.menu.id) {
      this.menu.item_category = null;
    }

    this.nameMultiple.filter((item: string, idx: number) => {
      this.languages.push({
        type: item,
        value: this.menu[item],
      });
    });

    this.ordinalInput = {
      name: this.menu.name,
      ordinal_number: this.menu.ordinal_number,
    };

    this.fetchCategories();
    this.fetchOptions();
    this.fetchSubStatuses();
    this.fetchCookingTypes();
    this.fetchCookingDurations();
    this.fetchClient();
  }

  private fetchClient() {
    const langs: string[] = ['allow_en', 'allow_zh_hans', 'allow_zh_hant', 'allow_ko'];

    this._clientService.fetchClient({ with: 'client_language' }).subscribe(
      (res) => {
        const langsInfo = this.languagesArr.value.map((lang, key) => {
          return {
            ...lang,
            allow: res.client_language[langs[key]],
          };
        });

        this.languagesArr.next(langsInfo);

        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
      },
    );
  }

  ngOnDestroy(): void {
    if (this.pageSub) {
      this.pageSub.unsubscribe();
    }
    if (this.subCatSearch) {
      this.subCatSearch.unsubscribe();
    }
    if (this.subOptSearch) {
      this.subOptSearch.unsubscribe();
    }
    if (this.modalSubscribe) {
      this.modalSubscribe.unsubscribe();
    }
  }

  private fetchCategories() {
    const options = {
      ...this.paginationCats.hasJSON(),
      with: 'item_type',
      name: this.searchCat,
    };
    return this.categorySv.getListCategory(options).subscribe(
      (res) => {
        this.categories = this.categories.concat(res.data);
        this.paginationCats.deserialize(res);
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
        this._cd.detectChanges();
      },
      () => {
        this.searchCategories();
      },
    );
  }

  searchCategories() {
    this.subCatSearch = this.categoriesInput$
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        tap(() => {
          this.isLoadCategory = true;
          this._cd.detectChanges();
        }),
        switchMap((name) => {
          this.paginationCats.reset();
          const opts: any = {
            ...this.paginationCats.hasJSON(),
            order: '-updated',
            with: 'item_type',
          };

          if (name && name.trim().length > 0) {
            opts.name = name.trim();
            this.searchCat = opts.name;
          } else {
            delete opts.name;
            this.searchCat = undefined;
          }

          return this.categorySv.getListCategory(opts).pipe(
            catchError(() => of([])),
            tap(() => {
              this.isLoadCategory = false;
            }),
          );
        }),
      )
      .subscribe((res) => {
        this.categories = res.data;
        this.paginationCats.deserialize(res);
        this._cd.detectChanges();
      });
  }

  loadMoreCategories() {
    if (this.isLoadCategory || !this.paginationCats.hasNextPage()) {
      return;
    }
    this.paginationCats.nextPage();
    this.fetchCategories();
  }

  private fetchOptions() {
    const opts = {
      ...this.paginationOpt.hasJSON(),
      order: 'updated',
      name: this.searchOpt,
    };

    return this.menuOptionSv.getListMenuOptions(opts).subscribe(
      (res) => {
        this.menuOptions = this.menuOptions.concat(res.data);
        this.paginationOpt.deserialize(res);
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
        this._cd.detectChanges();
      },
      () => {
        this.searchMenuOptions();
      },
    );
  }

  searchMenuOptions() {
    this.subOptSearch = this.menuOptionsInput$
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        tap(() => {
          this.isLoadMenuOption = true;
          this._cd.detectChanges();
        }),
        switchMap((name) => {
          this.paginationOpt.reset();
          const opts: any = {
            ...this.paginationOpt.hasJSON(),
            order: '-updated',
          };
          if (name && name.trim().length > 0) {
            opts.name = name.trim();
            this.searchOpt = opts.name;
          } else {
            delete opts.name;
            this.searchOpt = undefined;
          }

          return this.menuOptionSv.getListMenuOptions(opts).pipe(
            catchError(() => of([])),
            tap(() => {
              this.isLoadMenuOption = false;
            }),
          );
        }),
      )
      .subscribe((res) => {
        this.menuOptions = res.data;
        this.paginationOpt.deserialize(res);
        this._cd.detectChanges();
      });
  }

  loadMoreOptions() {
    if (this.isLoadMenuOption || !this.paginationOpt.hasNextPage()) {
      return;
    }
    this.paginationOpt.nextPage();
    this.fetchOptions();
  }

  private deleteImage() {
    if (this.menu.image_url) {
      this.menuSv.destroyMenuImage(this.menu.id).subscribe(
        (res: any) => {
          this.toastrSv.success('画像を削除しました');
          this.menuFile = [];
          this.menu.image_url = null;
          this.eventDropzone.next({
            type: DROPZONE_TYPE.RESET_INPUT,
          });
          this._cd.detectChanges();
        },
        (errors: any) => {
          if (errors.hasOwnProperty('errors')) {
            this.validation = this.validationSv.setErrors(errors.errors);
          }
          this.toastrSv.error(errors);
          this._cd.detectChanges();
        },
      );
    } else {
      this.menuFile = [];
      this._cd.detectChanges();
    }
  }

  private fetchSubStatuses() {
    const opts = {
      ...this.paginationStatuses.hasJSON(),
      name: this.searchStatuses,
      order: 'ordinal',
    };
    return this.menuAdditionSv.fetchSubStatuses(opts).subscribe(
      (res) => {
        this.statuses = this.statuses.concat(res.data);
        this.paginationStatuses.deserialize(res);
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
        this._cd.detectChanges();
      },
      () => {
        this.searchSubStatuses();
      },
    );
  }

  searchSubStatuses() {
    this.subStatusesSearch = this.statusesInput$
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        tap(() => {
          this.isLoadStatus = true;
          this._cd.detectChanges();
        }),
        switchMap((name) => {
          this.paginationStatuses.reset();
          const opts: any = {
            ...this.paginationStatuses.hasJSON(),
            order: 'ordinal',
          };

          if (name && name.trim().length > 0) {
            opts.name = name.trim();
            this.searchStatuses = opts.name;
          } else {
            delete opts.name;
            this.searchStatuses = undefined;
          }

          return this.menuAdditionSv.fetchSubStatuses(opts).pipe(
            catchError(() => of([])),
            tap(() => {
              this.isLoadStatus = false;
            }),
          );
        }),
      )
      .subscribe((res) => {
        this.statuses = res.data;
        this.paginationStatuses.deserialize(res);
        this._cd.detectChanges();
      });
  }

  loadMoreSubStatuses() {
    if (this.isLoadStatus || !this.paginationStatuses.hasNextPage()) {
      return;
    }
    this.paginationStatuses.nextPage();
    this.fetchSubStatuses();
  }

  private fetchCookingTypes() {
    const opts = {
      ...this.paginationCookTypes.hasJSON(),
      name: this.searchCookTypes,
      order: 'ordinal',
    };
    return this.menuAdditionSv.fetchCookingTypes(opts).subscribe(
      (res) => {
        this.cookTypes = this.cookTypes.concat(res.data);
        this.paginationCookTypes.deserialize(res);
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
        this._cd.detectChanges();
      },
      () => {
        this.searchCookingTypes();
      },
    );
  }

  searchCookingTypes() {
    this.subCookTypesSearch = this.cookTypesInput$
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        tap(() => {
          this.isLoadCookType = true;
          this._cd.detectChanges();
        }),
        switchMap((name) => {
          this.paginationCookTypes.reset();
          const opts: any = {
            ...this.paginationCookTypes.hasJSON(),
            order: 'ordinal',
          };

          if (name && name.trim().length > 0) {
            opts.name = name.trim();
            this.searchCookTypes = opts.name;
          } else {
            delete opts.name;
            this.searchCookTypes = undefined;
          }

          return this.menuAdditionSv.fetchCookingTypes(opts).pipe(
            catchError(() => of([])),
            tap(() => {
              this.isLoadCookType = false;
            }),
          );
        }),
      )
      .subscribe((res) => {
        this.cookTypes = res.data;
        this.paginationCookTypes.deserialize(res);
        this._cd.detectChanges();
      });
  }

  loadMoreCookingTypes() {
    if (this.isLoadCookType || !this.paginationCookTypes.hasNextPage()) {
      return;
    }
    this.paginationCookTypes.nextPage();
    this.fetchCookingTypes();
  }

  private fetchCookingDurations() {
    const opts = {
      ...this.paginationCookDurations.hasJSON(),
      name: this.searchCookDurations,
      order: 'ordinal',
    };
    return this.menuAdditionSv.fetchCookingDurations(opts).subscribe(
      (res) => {
        this.cookDurations = this.cookDurations.concat(res.data);
        this.paginationCookDurations.deserialize(res);
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
        this._cd.detectChanges();
      },
      () => {
        this.searchCookingDurations();
      },
    );
  }

  searchCookingDurations() {
    this.subCookDurationsSearch = this.cookDurationsInput$
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        tap(() => {
          this.isLoadCookDuration = true;
          this._cd.detectChanges();
        }),
        switchMap((duration) => {
          this.paginationCookDurations.reset();
          const opts: any = {
            ...this.paginationCookDurations.hasJSON(),
            order: 'ordinal',
          };

          if (duration && duration.trim().length > 0) {
            opts.duration = duration.trim();
            this.searchCookDurations = opts.duration;
          } else {
            delete opts.duration;
            this.searchCookDurations = undefined;
          }

          return this.menuAdditionSv.fetchCookingDurations(opts).pipe(
            catchError(() => of([])),
            tap(() => {
              this.isLoadCookDuration = false;
            }),
          );
        }),
      )
      .subscribe((res) => {
        this.cookDurations = res.data;
        this.paginationCookDurations.deserialize(res);
        this._cd.detectChanges();
      });
  }

  loadMoreCookingDurations() {
    if (this.isLoadCookDuration || !this.paginationCookDurations.hasNextPage()) {
      return;
    }
    this.paginationCookDurations.nextPage();
    this.fetchCookingDurations();
  }

  submitMenu() {
    if (this.isLoading) return;

    this.nameMultiple.filter((item: string) => {
      const idx = this.languages.findIndex((lang: any) => lang.type === item);
      if (idx !== -1) {
        this.menu[item] = this.languages[idx].value;
      } else {
        this.menu[item] = '';
      }
    });

    this.isLoading = true;
    this._cd.detectChanges();
    if (this.menu.id) {
      this.updateMenu();
    } else {
      this.createMenu();
    }
  }

  private createMenu() {
    this.menuSv.createMenu(this.menuFile, this.menu.formData()).subscribe(
      (res) => {
        this.toastrSv.success('メニューを作成しました');
        this.closeModal(DIALOG_EVENT.MENU_RELOAD);
        this._cd.detectChanges();
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validationSv.setErrors(errors.errors);
        }
        this.toastrSv.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  private updateMenu() {
    this.menuSv.updateMenu(this.menu.id, this.menuFile, this.menu.formData()).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastrSv.success('メニューを編集しました');
        this.closeModal(DIALOG_EVENT.MENU_RELOAD);
        this._cd.detectChanges();
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validationSv.setErrors(errors.errors);
        }
        this.toastrSv.error(errors);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  validatorReset(key) {
    this.validation[key] = undefined;
    this._cd.detectChanges();
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  changeToogle(type: string, value: boolean) {
    if (!value) {
      return;
    }

    switch (type) {
      case MENU_TYPE_TOOGLE.ONLY_COURSE:
        this.menu.is_show = false;
        this.menu.only_buffet = false;
        this.menu.recommend = false;
        this.menu.allow_take_out = false;
        break;
      case MENU_TYPE_TOOGLE.ONLY_BUFFET:
        this.menu.is_show = false;
        this.menu.only_course = false;
        this.menu.recommend = false;
        this.menu.allow_take_out = false;
        break;
      case MENU_TYPE_TOOGLE.IS_SHOW:
        this.menu.only_course = false;
        this.menu.only_buffet = false;
        break;
      case MENU_TYPE_TOOGLE.RECOMMEND:
        this.menu.only_course = false;
        this.menu.only_buffet = false;
        this.menu.is_show = true;
        break;
      case MENU_TYPE_TOOGLE.ALLOW_TAKE_OUT:
        this.menu.only_buffet = false;
        this.menu.only_course = false;
        break;
    }
  }

  onChangeOrdinal() {
    if (!this.ordinalInput) {
      return;
    }
    this.menu.name = this.ordinalInput.name;
    this.menu.ordinal_number = this.ordinalInput.ordinal_number;
  }

  deleteMenu() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        menuId: this.menu.id,
      },
    };

    this._openModalWithComponent(MenuRemoveComponent, opts);
  }

  private _openModalWithComponent(comp, opts: ModalOptions = {}) {
    this.modalSubscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      if (reason === DIALOG_EVENT.MENU_REMOVE) {
        this.closeModal(DIALOG_EVENT.MENU_REMOVE);
      }
    });

    this.modalSv.show(comp, opts);
  }
}
