import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';

import { MenuService } from 'shared/http-services/menu.service';

@Injectable({
  providedIn: 'root',
})
export class MenuCeResolve implements Resolve<any> {
  constructor(private _router: Router, private menuSv: MenuService) {}

  resolve(activeRoute: ActivatedRouteSnapshot) {
    if (activeRoute.params.menuId && isNaN(Number(activeRoute.params.menuId))) {
      return this._router.navigate(['/setting/menus']);
    }
    return this.menuSv.fetchMenuById(activeRoute.params.menuId, {
      with: 'item_category.item_type,item_category',
    });
  }
}
