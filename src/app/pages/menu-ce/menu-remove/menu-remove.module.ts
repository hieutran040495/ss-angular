import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

import { MenuRemoveComponent } from './menu-remove.component';

@NgModule({
  declarations: [MenuRemoveComponent],
  imports: [CommonModule, CasDialogModule],
  entryComponents: [MenuRemoveComponent],
  exports: [MenuRemoveComponent],
})
export class MenuRemoveModule {}
