import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { MenuService } from 'shared/http-services/menu.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { DIALOG_EVENT } from 'shared/enums/modes';

@Component({
  selector: 'app-menu-remove',
  templateUrl: './menu-remove.component.html',
  styleUrls: ['./menu-remove.component.scss'],
})
export class MenuRemoveComponent implements OnInit {
  menuId: number;
  isLoading: boolean = false;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private toastSv: ToastService,
    private menuSv: MenuService,
  ) {}

  ngOnInit() {}

  menuRemove() {
    if (this.isLoading) return;

    this.isLoading = true;

    this.menuSv.menuDestroy(this.menuId).subscribe(
      (res: any) => {
        this.toastSv.success('メニューを削除しました');
        this.closeDialog(DIALOG_EVENT.MENU_REMOVE);
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
    this.isLoading = false;
  }
}
