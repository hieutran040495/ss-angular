import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ValidatorService } from 'shared/utils/validator.service';
import { MenuService } from 'shared/http-services/menu.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { CategoryService } from 'shared/http-services/category.service';
import { MenuOptionService } from 'shared/http-services/menu-option.service';

import { MenuCeComponent } from './menu-ce.component';
import { UploadFileModule } from '../upload-file/upload-file.module';
import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';
import { DigitFormaterModule } from 'shared/modules/digit-formater/digit-formater.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { CasDropzoneModule } from 'app/modules/cas-dropzone/cas-dropzone.module';
import { UiSwitchModule } from 'shared/modules/ui-switch';

import { ToastServiceMock } from 'testing/services/toast.service.mock';
import { categories, options, subStatuses, cookingTypes, cookingDurations } from 'testing/data/menu.mock';

import { of, throwError } from 'rxjs';
import { Menu } from 'shared/models/menu';
import { MENU_TYPE_TOOGLE } from 'shared/enums/menu-type';
import { MenuAdditionService } from 'shared/http-services/menu-addition.service';

fdescribe('Describe MenuCeComponent', () => {
  let component: MenuCeComponent;
  let fixture: ComponentFixture<MenuCeComponent>;
  let toastSv: ToastService;

  const menuInput: any = {
    category_id: 1,
    name: '',
    is_show: false,
    price: 1000,
    recommend: false,
    only_course: true,
    menu_option_ids: [1, 2],
  };
  const menuSv = jasmine.createSpyObj('MenuService', ['destroyMenuImage', 'createMenu', 'updateMenu']);
  const categorySv = jasmine.createSpyObj('CategoryService', ['getListCategory']);
  const menuOptionSv = jasmine.createSpyObj('MenuOptionService', ['getListMenuOptions']);
  const validatorSv = jasmine.createSpyObj('ValidatorService', ['setErrors']);
  const modalSv = jasmine.createSpyObj('BsModalService', ['setDismissReason']);
  const bsModalRef = jasmine.createSpyObj('BsModalRef', ['hide']);
  const menuAdditionSv = jasmine.createSpyObj('MenuAdditionService', [
    'fetchCookingDurations',
    'fetchSubStatuses',
    'fetchCookingTypes',
  ]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        CommonModule,
        UploadFileModule,
        SpaceValidationModule,
        CasNgSelectModule,
        DigitFormaterModule,
        CasDialogModule,
        CasDropzoneModule,
        UiSwitchModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [MenuCeComponent],
      providers: [
        { provide: ToastService, useClass: ToastServiceMock },
        { provide: ValidatorService, useValue: validatorSv },
        { provide: MenuService, useValue: menuSv },
        { provide: CategoryService, useValue: categorySv },
        { provide: MenuOptionService, useValue: menuOptionSv },
        { provide: BsModalService, useValue: modalSv },
        { provide: BsModalRef, useValue: bsModalRef },
        { provide: MenuAdditionService, useValue: menuAdditionSv },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuCeComponent);
    component = fixture.componentInstance;
    toastSv = TestBed.get(ToastService);
    categorySv.getListCategory.and.returnValue(of({ data: categories }));
    menuOptionSv.getListMenuOptions.and.returnValue(of({ data: options }));
    menuAdditionSv.fetchSubStatuses.and.returnValue(of({ data: subStatuses }));
    menuAdditionSv.fetchCookingTypes.and.returnValue(of({ data: cookingTypes }));
    menuAdditionSv.fetchCookingDurations.and.returnValue(of({ data: cookingDurations }));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call categorySv', () => {
    expect(categorySv.getListCategory).toHaveBeenCalled();
  });

  it('should call menuOptionSv', () => {
    expect(menuOptionSv.getListMenuOptions).toHaveBeenCalled();
  });

  it('should call fetchSubStatuses', () => {
    expect(menuAdditionSv.fetchSubStatuses).toHaveBeenCalled();
  });

  it('should call fetchCookingTypes', () => {
    expect(menuAdditionSv.fetchCookingTypes).toHaveBeenCalled();
  });

  it('should call fetchCookingDurations', () => {
    expect(menuAdditionSv.fetchCookingDurations).toHaveBeenCalled();
  });

  describe('Create Menu Success', () => {
    const createMenuSpy = menuSv.createMenu.and.returnValue(of({ sussess: true }));

    beforeEach(() => {
      component.menu = new Menu().deserialize(menuInput);
      component.submitMenu();
    });

    it('isLoading should be true', () => {
      expect(component.isLoading).toBeTruthy();
    });

    it('should call function create menu', () => {
      expect(createMenuSpy.calls.any()).toBe(true, 'call menu create service');
    });

    it('should call notify success', () => {
      expect(toastSv.success).toHaveBeenCalled();
    });
  });

  describe('Create Menu Error', () => {
    beforeEach(() => {
      menuSv.createMenu.and.returnValue(throwError('create menu fail'));
      component.menu = new Menu().deserialize(menuInput);
      component.submitMenu();
    });

    it('isLoading should be false', () => {
      expect(component.isLoading).toBeFalsy();
    });

    it('should call notify error', () => {
      expect(toastSv.error).toHaveBeenCalled();
    });
  });

  describe('Update Menu Success', () => {
    const updateMenuSpy = menuSv.updateMenu.and.returnValue(of({ sussess: true }));

    beforeEach(() => {
      menuInput.id = 1;
      component.menu = new Menu().deserialize(menuInput);
      component.submitMenu();
    });

    it('isLoading should be false', () => {
      expect(component.isLoading).toBeFalsy();
    });

    it('should call function update menu', () => {
      expect(updateMenuSpy.calls.any()).toBe(true, 'call menu update service');
    });

    it('should call notify success', () => {
      expect(toastSv.success).toHaveBeenCalled();
    });
  });

  describe('Update Menu Error', () => {
    beforeEach(() => {
      menuSv.updateMenu.and.returnValue(throwError('update menu fail'));
      menuInput.id = 1;
      component.menu = new Menu().deserialize(menuInput);
      component.submitMenu();
    });

    it('isLoading should be false', () => {
      expect(component.isLoading).toBeFalsy();
    });

    it('should call notify error', () => {
      expect(toastSv.error).toHaveBeenCalled();
    });
  });

  describe('Validator', () => {
    const name = 'name';

    beforeEach(() => {
      component.validatorReset(name);
    });

    it('validation should be undefine', () => {
      expect(component.validation[name]).toBeUndefined();
    });
  });

  describe('Toggle is only course', () => {
    const type = MENU_TYPE_TOOGLE.ONLY_COURSE;
    const value = true;

    beforeEach(() => {
      component.changeToogle(type, value);
    });

    it('menu should be not shown', () => {
      expect(component.menu.is_show).toBeFalsy();
    });
  });

  describe('Toggle is shown', () => {
    const type = MENU_TYPE_TOOGLE.IS_SHOW;
    const value = true;

    beforeEach(() => {
      component.changeToogle(type, value);
    });

    it('menu should be not only course', () => {
      expect(component.menu.only_course).toBeFalsy();
    });
  });
});
