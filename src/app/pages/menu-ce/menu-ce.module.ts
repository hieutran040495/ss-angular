import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UploadFileModule } from 'app/pages/upload-file/upload-file.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { CasDropzoneModule } from 'app/modules/cas-dropzone/cas-dropzone.module';
import { InputNameWithIdModule } from 'app/modules/input-name-with-id/input-name-with-id.module';

import { UiSwitchModule } from 'shared/modules/ui-switch/index';
import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';
import { DigitFormaterModule } from 'shared/modules/digit-formater/digit-formater.module';
import { MenuRemoveModule } from './menu-remove/menu-remove.module';
import { CasMultipleLanguageModule } from 'app/modules/cas-multiple-language/cas-multiple-language.module';

import { MenuCeComponent } from './menu-ce.component';

import { ValidatorService } from 'shared/utils/validator.service';
import { MenuAdditionService } from 'shared/http-services/menu-addition.service';

@NgModule({
  declarations: [MenuCeComponent],
  imports: [
    CommonModule,
    FormsModule,
    UiSwitchModule.forRoot({
      size: 'small',
      bgColor: '#c3ed12',
    }),
    UploadFileModule,
    SpaceValidationModule,
    CasNgSelectModule,
    DigitFormaterModule,
    CasDialogModule,
    CasDropzoneModule,
    InputNameWithIdModule,
    CasMultipleLanguageModule,
    MenuRemoveModule,
  ],
  providers: [ValidatorService, MenuAdditionService],
  entryComponents: [MenuCeComponent],
  exports: [MenuCeComponent],
})
export class MenuCeModule {}
