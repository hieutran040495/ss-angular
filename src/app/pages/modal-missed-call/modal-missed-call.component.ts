import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { MissedCall } from 'shared/models/missed-call';
import { Pagination, PageChangedInput } from 'shared/models/pagination';

import { ToastService } from 'shared/logical-services/toast.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';

import { RESV_MODE_EMITTER } from 'shared/enums/event-emitter';
import { Reservation } from 'shared/models/reservation';
import { MissedCallService } from 'shared/http-services/missed-call.service';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ClientUser } from 'shared/models/client-user';

@Component({
  selector: 'app-modal-missed-call',
  templateUrl: './modal-missed-call.component.html',
  styleUrls: ['./modal-missed-call.component.scss'],
})
export class ModalMissedCallComponent implements OnInit {
  missedCalls: MissedCall[] = [];
  isLoading: boolean = false;

  isLookingPhone: boolean = false;
  isPaginate: boolean = false;
  isSolving: boolean = false;

  pagination: Pagination = new Pagination().deserialize({ per_page: 50 });

  constructor(
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private _missedCallService: MissedCallService,
    private toast: ToastService,
    private eventEmitter: EventEmitterService,
    private resvService: ReservationService,
  ) {}

  ngOnInit() {
    this.fetchMissedCalls();
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;

    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.fetchMissedCalls();
  }

  fetchMissedCalls() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    this.missedCalls = [];
    const opts = {
      with: 'client_user,client_user.upcoming_reservation,client_user.finished_reservations_count',
    };
    this._missedCallService.getMissedCalls(opts).subscribe(
      (res) => {
        this.missedCalls = res.data;
        this.isLoading = false;
      },
      (errors) => {
        this.toast.error(errors);
        this.isLoading = false;
      },
    );
  }

  solvedMissedCall(phone: string, isClearMissedCall: boolean = false) {
    if (this.isSolving) {
      return;
    }

    this.isSolving = true;
    const data = {
      solved: true,
    };
    this._missedCallService.solvedMissedCall(phone, data).subscribe(
      (res) => {
        if (isClearMissedCall) {
          this.missedCalls = this.missedCalls.filter((call) => call.phone !== phone);
          this.toast.success('未読着信を対応済みとしました');
        }
        this.isSolving = false;
      },
      (errors) => {
        this.toast.error(errors);
        this.isSolving = false;
      },
    );
  }

  createOrEditResv(missedCall: MissedCall) {
    this.solvedMissedCall(missedCall.phone);

    if (!missedCall.client_user.has_upcoming_resv) {
      this.closeModal();
      this._createResv(new ClientUser().deserialize({ phone: missedCall.phone }));
      return;
    }

    const opts = {
      with: 'coupon,clientUser,tables,user',
    };

    this.resvService.getResvById(missedCall.client_user.reservation_id, opts).subscribe(
      (res) => {
        const upcomingResv = new Reservation().deserialize(res);
        this._updateResv(upcomingResv);
        this.closeModal();
      },
      (errors) => {
        this.toast.error(errors);
      },
    );
  }

  private _createResv(clientUser: ClientUser) {
    this.eventEmitter.publishData({
      type: RESV_MODE_EMITTER.CREATE,
      data: {
        client_user: clientUser,
      },
    });
  }

  private _updateResv(upcomingResv: Reservation) {
    this.eventEmitter.publishData({
      type: RESV_MODE_EMITTER.EDIT,
      data: upcomingResv,
    });
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
