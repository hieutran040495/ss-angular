import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalMissedCallComponent } from './modal-missed-call.component';

describe('ModalMissedCallComponent', () => {
  let component: ModalMissedCallComponent;
  let fixture: ComponentFixture<ModalMissedCallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalMissedCallComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalMissedCallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
