import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { FormsModule } from '@angular/forms';

import { ModalMissedCallComponent } from './modal-missed-call.component';

@NgModule({
  declarations: [ModalMissedCallComponent],
  imports: [CommonModule, CasPaginationModule.forRoot(), FormsModule],
  entryComponents: [ModalMissedCallComponent],
  exports: [ModalMissedCallComponent],
})
export class ModalMissedCallModule {}
