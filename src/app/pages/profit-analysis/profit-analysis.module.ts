import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ProfitAnalysisComponent } from './profit-analysis.component';

const routes: Routes = [
  {
    path: '',
    component: ProfitAnalysisComponent,
    children: [
      {
        path: '',
        redirectTo: 'day',
      },
      {
        path: 'day',
        loadChildren: '../profit-day/profit-day.module#ProfitDayModule',
        data: {
          appScreen: 'C-6-4',
        },
      },
      {
        path: 'week',
        loadChildren: '../profit-week/profit-week.module#ProfitWeekModule',
        data: {
          appScreen: 'C-6-5',
        },
      },
      {
        path: 'month',
        loadChildren: '../profit-month/profit-month.module#ProfitMonthModule',
        data: {
          appScreen: 'C-6-6',
        },
      },
    ],
  },
];

@NgModule({
  declarations: [ProfitAnalysisComponent],
  imports: [CommonModule, RouterModule.forChild(routes)],
})
export class ProfitAnalysisModule {}
