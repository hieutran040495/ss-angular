import { Component, OnInit } from '@angular/core';

import { PROFIT_DATETIME } from 'shared/enums/profit';

@Component({
  selector: 'app-profit-analysis',
  templateUrl: './profit-analysis.component.html',
  styleUrls: ['./profit-analysis.component.scss'],
})
export class ProfitAnalysisComponent implements OnInit {
  profitNavigation: any = [
    {
      name: '日',
      link: 'day',
      type: PROFIT_DATETIME.DAY,
    },
    {
      name: '週',
      link: 'week',
      type: PROFIT_DATETIME.WEEKS,
    },
    {
      name: '月',
      link: 'month',
      type: PROFIT_DATETIME.MONTHS,
    },
  ];
  profitType: string = PROFIT_DATETIME.DAY;

  constructor() {}

  ngOnInit() {}

  settingProfit(type) {
    this.profitType = type;
  }
}
