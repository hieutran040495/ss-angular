import { Component, OnInit } from '@angular/core';
import { NotifyService } from 'shared/http-services/notify.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { NotifySetting } from 'shared/models/notify-setting';
import { Regexs } from 'shared/constants/regex';
import { ValidatorService } from 'shared/utils/validator.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ClientService } from 'shared/http-services/client.service';

@Component({
  selector: 'app-modal-setting-notify',
  templateUrl: './modal-setting-notify.component.html',
  styleUrls: ['./modal-setting-notify.component.scss'],
})
export class ModalSettingNotifyComponent implements OnInit {
  isLoading: boolean = false;
  notify_settings: NotifySetting = new NotifySetting();

  rules = Regexs;
  validation: any = {};

  constructor(
    private notifySv: NotifyService,
    private toastrSv: ToastService,
    private validatorSv: ValidatorService,
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private clientSv: ClientService,
  ) {}

  ngOnInit() {
    this.getClientInfo();
  }

  getClientInfo() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    const opts = {
      with: 'notifySettings',
    };
    this.clientSv.fetchClient(opts).subscribe(
      (res) => {
        this.notify_settings = new NotifySetting().deserialize(res.notify_settings);
        this.isLoading = false;
      },
      (errors) => {
        this.toastrSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  settingNotify() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = false;

    this.notifySv.settingNotify(this.notify_settings.formDataSettingJSON()).subscribe(
      (res) => {
        this.toastrSv.success('受け取り通知設定を変更しました');
        this.closeModal();
        this.isLoading = false;
      },
      (errors) => {
        this.toastrSv.error(errors);
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validatorSv.setErrors(errors.errors);
        }
        this.isLoading = false;
      },
    );
  }

  resetValidation(key: string): void {
    this.validation[key] = undefined;
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
