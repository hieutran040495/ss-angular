import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSettingNotifyComponent } from './modal-setting-notify.component';

describe('ModalSettingNotifyComponent', () => {
  let component: ModalSettingNotifyComponent;
  let fixture: ComponentFixture<ModalSettingNotifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalSettingNotifyComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSettingNotifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
