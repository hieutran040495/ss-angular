import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalSettingNotifyComponent } from './modal-setting-notify.component';
import { FormsModule } from '@angular/forms';
import { UiSwitchModule } from 'shared/modules/ui-switch/index';

import { ValidatorService } from 'shared/utils/validator.service';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [ModalSettingNotifyComponent],
  imports: [
    CommonModule,
    FormsModule,
    UiSwitchModule.forRoot({
      size: 'small',
      bgColor: '#c3ed12',
    }),
    OnlyNumericModule,
    CasDialogModule,
    ModalModule,
  ],
  entryComponents: [ModalSettingNotifyComponent],
  providers: [ValidatorService],
})
export class ModalSettingNotifyModule {}
