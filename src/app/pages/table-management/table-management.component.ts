import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { KonvaDataEmitter, KONVA_EMITTER_TYPE } from 'shared/modules/konva-canvas/data-emitter';
import { TableService } from 'shared/http-services/table.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { Table } from 'shared/models/table';
import { MODES } from 'shared/enums/modes';

@Component({
  selector: 'app-table-management',
  templateUrl: './table-management.component.html',
  styleUrls: ['./table-management.component.scss'],
})
export class TableManagementComponent implements OnInit {
  canvasHandleEvent$: Subject<KonvaDataEmitter> = new Subject<KonvaDataEmitter>();
  isLoading = false;
  tables: Table[] = [];
  MODES = MODES;

  constructor(private _tableSv: TableService, private _toastSv: ToastService) {}

  ngOnInit() {
    this._getTables();
  }

  private _getTables() {
    this.isLoading = true;

    this._tableSv.getTables().subscribe(
      (res) => {
        this.tables = res.data;

        this.canvasHandleEvent$.next({
          type: KONVA_EMITTER_TYPE.RELOAD_KONVA,
          data: this.tables,
        });

        this.isLoading = false;
      },
      (errors) => {
        this._toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  saveEventHandler(data: any[]) {
    this.isLoading = true;

    this._tableSv.updateLayout({ layout: data }).subscribe(
      () => {
        this.isLoading = false;
        this._toastSv.success('テーブルレイアウトを設定しました');
      },
      (errors) => {
        this.isLoading = false;
        this._toastSv.error(errors);
      },
    );
  }
}
