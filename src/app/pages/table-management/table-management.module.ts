import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { KonvaCanvasModule } from 'shared/modules/konva-canvas/konva-canvas.module';

import { TableManagementComponent } from './table-management.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: TableManagementComponent,
  },
];

@NgModule({
  declarations: [TableManagementComponent],
  imports: [CommonModule, FormsModule, RouterModule.forChild(routes), KonvaCanvasModule],
})
export class TableManagementModule {}
