import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSettingSelfOrderTopScreenComponent } from './modal-setting-self-order-top-screen.component';

describe('ModalSettingSelfOrderTopScreenComponent', () => {
  let component: ModalSettingSelfOrderTopScreenComponent;
  let fixture: ComponentFixture<ModalSettingSelfOrderTopScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalSettingSelfOrderTopScreenComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSettingSelfOrderTopScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
