import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ToastService } from 'shared/logical-services/toast.service';
import { Subject, Subscription } from 'rxjs';
import { SELF_ORDER, SETTING_DISPLAY, SETTING_RICH_MENU, KONVAJS_TYPE } from 'shared/enums/event-emitter';
import { ModalSettingDisplayComponent } from 'app/pages/modal-setting-display/modal-setting-display.component';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import cloneDeep from 'lodash/cloneDeep';
import { StyleOpts } from 'shared/interfaces/style-setting';
import { TopScreen } from 'shared/models/top-screen';
import { User } from 'shared/models/user';
import { SELF_ORDER_SCREEN_DIRECTION } from 'shared/enums/konva-size-config';

@Component({
  selector: 'app-modal-setting-self-order-top-screen',
  templateUrl: './modal-setting-self-order-top-screen.component.html',
  styleUrls: ['./modal-setting-self-order-top-screen.component.scss'],
})
export class ModalSettingSelfOrderTopScreenComponent implements OnInit, OnDestroy {
  @ViewChild('formFiles') formFiles: ElementRef;

  currentUser: User = new User();
  topScreen: TopScreen = new TopScreen();
  onEventEmitter: Subject<any> = new Subject();

  private maxFileSize: number = 3145728; // 3MB
  fileAccept: string = 'image/jpeg,image/png,image/jpg';

  public settingOpts: StyleOpts;
  private _subEvent: Subscription;
  private _subEventSetting: Subscription;

  isLoading: boolean = true;

  get isRemoveImage(): boolean {
    return !!this.topScreen.top_screen.image_url;
  }

  get isDirectionVert(): boolean {
    return this.currentUser.client.self_order_direction === SELF_ORDER_SCREEN_DIRECTION.VERT;
  }

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private toastSv: ToastService,
    private emitSv: EventEmitterService,
  ) {}

  ngOnInit() {
    this._initSettingDisplay();

    this._subEvent = this.emitSv.caseNumber$.subscribe((res) => {
      if (!res && !res.type) {
        return;
      }
      if (res.type === SETTING_DISPLAY.CHANGE_SETTING_DISPLAY) {
        this._updateStyle(res.data);
      }
    });

    this._subEventSetting = this.onEventEmitter.subscribe((res: any) => {
      if (!res && !res.type) {
        return;
      }

      if (res.type === SETTING_DISPLAY.SUCCESS || res.type === SETTING_DISPLAY.ERROR) {
        this.isLoading = false;
        this.closeModal();
        this.emitSv.publishData({
          type: SETTING_RICH_MENU.RELOAD_RICH_MENU,
        });
      }
    });
  }

  ngOnDestroy() {
    this._subEvent.unsubscribe();
    this._subEventSetting.unsubscribe();
  }

  private _initSettingDisplay() {
    this.settingOpts = {
      text_color: this.topScreen.top_screen.text_color,
      bg_color: this.topScreen.top_screen.bg_color,
      font_family: this.topScreen.top_screen.font_family,
    };
    setTimeout(() => {
      this.isLoading = false;
    }, 2000);
  }

  private _updateStyle(data: any) {
    this.settingOpts = data;
    this.topScreen.top_screen.updateStyle(data);

    return this.onEventEmitter.next({
      type: SELF_ORDER.CHANGE_SETTING_DISPLAY,
      data: data,
    });
  }

  closeModal(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  handleFile(files: FileList) {
    const file: File = files.item(0);
    if (file) {
      if (this.fileAccept.indexOf(file.type) === -1) {
        this.toastSv.info(`jpg, jpegとpngのファイルでアップロードしてください`);
        return false;
      }

      if (file.size > this.maxFileSize) {
        this.toastSv.info(`サイズが3MBを超える画像はアップロードできません`);
        return false;
      }

      const reader: FileReader = new FileReader();
      reader.onloadend = (e: any) => {
        const newImg = new Image();
        newImg.src = e.target.result;
        newImg.onload = (img: any) => {
          this._onLoadImage(img, file, e.target.result);
        };
      };
      reader.readAsDataURL(file);
      this.formFiles.nativeElement.value = null;
    }
  }

  uploadImage(el: any) {
    if (this.isRemoveImage) {
      this.topScreen.top_screen.resetImage();
      return this.onEventEmitter.next({
        type: KONVAJS_TYPE.REMOVE_MAIN_IMAGE,
      });
    }
    return el.click();
  }

  private _onLoadImage(element: any, file: File, image_url: string) {
    this.topScreen.top_screen.deserialize({
      image_url: image_url,
      image_width: element.composedPath()[0].width,
      image_height: element.composedPath()[0].height,
      ratio: 1,
    });
    this.onEventEmitter.next({
      type: SELF_ORDER.CHANGE_TOP_IMAGE,
      data: file,
    });
  }

  saveTopImage() {
    this.isLoading = true;

    return this.onEventEmitter.next({
      type: SELF_ORDER.SAVE_TOP_SCREEN,
    });
  }

  settingDisplay() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-md',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        settingOpts: cloneDeep(this.settingOpts),
      },
    };

    this.bsModalSv.show(ModalSettingDisplayComponent, opts);
  }
}
