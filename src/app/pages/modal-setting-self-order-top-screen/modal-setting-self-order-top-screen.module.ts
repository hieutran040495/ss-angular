import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'ngx-bootstrap/modal';

import { ScrollerModule } from 'shared/directive/scroller/scroller.module';
import { CasTopImageModule } from 'shared/modules/cas-top-image/cas-top-image.module';
import { FormsModule } from '@angular/forms';
import { ModalSettingDisplayModule } from 'app/pages/modal-setting-display/modal-setting-display.module';

import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { CasPopupGuideDragModule } from 'app/modules/cas-popup-guide-drag/cas-popup-guide-drag.module';
import { ModalSettingSelfOrderTopScreenComponent } from './modal-setting-self-order-top-screen.component';

@NgModule({
  declarations: [ModalSettingSelfOrderTopScreenComponent],
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    CasDialogModule,
    ScrollerModule,
    CasPopupGuideDragModule,
    CasTopImageModule,
    FormsModule,
    ModalSettingDisplayModule,
  ],
  exports: [ModalSettingSelfOrderTopScreenComponent],
  entryComponents: [ModalSettingSelfOrderTopScreenComponent],
})
export class ModalSettingSelfOrderTopScreenModule {}
