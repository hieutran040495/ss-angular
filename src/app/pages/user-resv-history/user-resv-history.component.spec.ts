import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserResvHistoryComponent } from './user-resv-history.component';

describe('UserResvHistoryComponent', () => {
  let component: UserResvHistoryComponent;
  let fixture: ComponentFixture<UserResvHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserResvHistoryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserResvHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
