import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Pagination, PageChangedInput } from 'shared/models/pagination';
import { Reservation } from 'shared/models/reservation';
import { ReservationService } from 'shared/http-services/reservation.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { User } from 'shared/models/user';
import { TableHeader } from 'app/modules/cas-table/cas-table.component';
import { DIALOG_EVENT } from 'shared/enums/modes';

@Component({
  selector: 'app-user-resv-history',
  templateUrl: './user-resv-history.component.html',
  styleUrls: ['./user-resv-history.component.scss'],
})
export class UserResvHistoryComponent implements OnInit {
  isLoading: boolean = false;
  isPaginate: boolean = false;
  reservations: Reservation[] = [];
  pagination: Pagination = new Pagination().deserialize({ per_page: 10 });

  user: User;

  tableHeader: Partial<TableHeader>[] = [
    {
      name: '日時',
      width: 150,
      class: 'text-center',
    },
    {
      name: `メニュー・個数`,
      width: 150,
      class: 'text-center',
    },
    {
      name: '金額',
      width: 150,
      class: 'text-center',
    },
  ];

  constructor(
    private reservationSv: ReservationService,
    private toastrSv: ToastrService,
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
  ) {}

  ngOnInit() {
    this.getReservations();
  }

  private getReservations() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.reservations = [];

    const opts = {
      ...this.pagination.hasJSON(),
      with: 'reservationItems',
      client_user_id: this.user.id,
      order: 'start_at',
    };

    this.reservationSv.getReservations(opts).subscribe(
      (res) => {
        this.reservations = res.data;
        this.pagination.deserialize(res);
        this.isLoading = false;
        this.isPaginate = false;
      },
      (errors) => {
        this.toastrSv.error(errors);
        this.isLoading = false;
        this.isPaginate = false;
      },
    );
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;
    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.getReservations();
  }

  createReservation() {
    this.closeDialog(DIALOG_EVENT.CREATE_RESERVATION);
  }

  openDialogDetail() {
    this.closeDialog(DIALOG_EVENT.OPEN_USER_DETAIL);
  }

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
