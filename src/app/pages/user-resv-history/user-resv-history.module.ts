import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { FormsModule } from '@angular/forms';
import { UserResvHistoryComponent } from './user-resv-history.component';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { CasTableModule } from 'app/modules/cas-table/cas-table.module';

@NgModule({
  declarations: [UserResvHistoryComponent],
  imports: [CommonModule, FormsModule, CasPaginationModule.forRoot(), CasDialogModule, CasTableModule],
  entryComponents: [UserResvHistoryComponent],
})
export class UserResvHistoryModule {}
