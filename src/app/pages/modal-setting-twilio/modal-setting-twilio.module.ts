import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalSettingTwilioComponent } from './modal-setting-twilio.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { FormsModule } from '@angular/forms';
import { TwilioService } from 'shared/http-services/twilio.service';
import { ValidatorService } from 'shared/utils/validator.service';
import { InputContenteditableModule } from 'app/modules/input-contenteditable/input-contenteditable.module';

@NgModule({
  imports: [CommonModule, CasDialogModule, FormsModule, ModalModule.forRoot(), InputContenteditableModule],
  declarations: [ModalSettingTwilioComponent],
  entryComponents: [ModalSettingTwilioComponent],
  exports: [ModalSettingTwilioComponent],
  providers: [TwilioService, ValidatorService],
})
export class ModalSettingTwilioModule {}
