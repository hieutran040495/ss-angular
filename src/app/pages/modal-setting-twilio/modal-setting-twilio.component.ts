import { Component, OnInit } from '@angular/core';
import { ClientTwilio } from 'shared/models/client-twilio';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TwilioService } from 'shared/http-services/twilio.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ValidatorService } from 'shared/utils/validator.service';

@Component({
  selector: 'app-modal-setting-twilio',
  templateUrl: './modal-setting-twilio.component.html',
  styleUrls: ['./modal-setting-twilio.component.scss'],
})
export class ModalSettingTwilioComponent implements OnInit {
  public clientTwilio: ClientTwilio = new ClientTwilio();
  public isLoading: boolean = false;

  public validator: any = {};
  public isDirtyAuthToken = false;

  constructor(
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private twilioService: TwilioService,
    private toast: ToastService,
    private validateSv: ValidatorService,
  ) {}

  ngOnInit() {}

  public resetValidator(key: string) {
    this.validator[key] = undefined;
  }

  public closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  /**
   * submitTwilio
   */
  public submitTwilio() {
    this.isLoading = true;

    this.twilioService.registerTwilio(this.clientTwilio.toJSON()).subscribe(
      (res) => {
        this.isLoading = false;
        this.toast.success('twilioにおける設定を行なっているので、少々お待ちください');
        this.closeModal('reload');
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validator = this.validateSv.setErrors(errors.errors);
        }

        this.isLoading = false;
        this.toast.error(errors);
      },
    );
  }
  public changeAuthToken(value: string) {
    this.clientTwilio.auth_token = value;
    if (value) {
      this.isDirtyAuthToken = true;
    }
  }
}
