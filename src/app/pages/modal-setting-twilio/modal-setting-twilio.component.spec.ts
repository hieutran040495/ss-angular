import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSettingTwilioComponent } from './modal-setting-twilio.component';

describe('ModalSettingTwilioComponent', () => {
  let component: ModalSettingTwilioComponent;
  let fixture: ComponentFixture<ModalSettingTwilioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalSettingTwilioComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSettingTwilioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
