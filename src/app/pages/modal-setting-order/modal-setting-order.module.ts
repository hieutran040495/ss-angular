import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalSettingOrderComponent } from './modal-setting-order.component';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalSettingOrderTopScreenModule } from '../modal-setting-order-top-screen/modal-setting-order-top-screen.module';
import { ModalSettingSelfOrderTopScreenModule } from '../modal-setting-self-order-top-screen/modal-setting-self-order-top-screen.module';
import { DialogRichMenuModule } from 'app/modules/dialog-rich-menu/dialog-rich-menu.module';
import { DialogSelfOrderModule } from 'app/modules/dialog-self-order/dialog-self-order.module';

import { TopScreenService } from 'shared/http-services/top-screen.service';

@NgModule({
  declarations: [ModalSettingOrderComponent],
  imports: [
    CommonModule,
    CasDialogModule,
    ModalModule.forRoot(),
    ModalSettingOrderTopScreenModule,
    ModalSettingSelfOrderTopScreenModule,
    DialogRichMenuModule,
    DialogSelfOrderModule,
  ],
  exports: [ModalSettingOrderComponent],
  entryComponents: [ModalSettingOrderComponent],
  providers: [TopScreenService],
})
export class ModalSettingOrderModule {}
