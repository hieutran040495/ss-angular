import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSettingOrderComponent } from './modal-setting-order.component';

describe('ModalSettingOrderComponent', () => {
  let component: ModalSettingOrderComponent;
  let fixture: ComponentFixture<ModalSettingOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalSettingOrderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSettingOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
