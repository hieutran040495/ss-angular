import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { APP_SETTING } from 'shared/enums/rich-menu';

import { RichMenu } from 'shared/models/rich-menu';
import { TopImage } from 'shared/models/top-image';
import { TopScreen } from 'shared/models/top-screen';
import { ItemCategory } from 'shared/models/item-category';
import { User } from 'shared/models/user';

import { DialogRichMenuComponent } from 'app/modules/dialog-rich-menu/dialog-rich-menu.component';
import { DialogSelfOrderComponent } from 'app/modules/dialog-self-order/dialog-self-order.component';
import { ModalSettingOrderTopScreenComponent } from '../modal-setting-order-top-screen/modal-setting-order-top-screen.component';
import { ModalSettingSelfOrderTopScreenComponent } from '../modal-setting-self-order-top-screen/modal-setting-self-order-top-screen.component';

import { ToastService } from 'shared/logical-services/toast.service';
import { TopScreenService } from 'shared/http-services/top-screen.service';
import { ItemCategoryService } from 'shared/http-services/item-category.service';

@Component({
  selector: 'app-modal-setting-order',
  templateUrl: './modal-setting-order.component.html',
  styleUrls: ['./modal-setting-order.component.scss'],
})
export class ModalSettingOrderComponent implements OnInit {
  currentUser: User = new User();
  richMenu: RichMenu = new RichMenu();
  topImage: TopImage = new TopImage();

  APP_SETTING = APP_SETTING;
  isLoading: boolean;

  constructor(
    private bsModalRef: BsModalRef,
    private modalSv: BsModalService,
    private toastSv: ToastService,
    private topImageSv: TopScreenService,
    private itemCategorySv: ItemCategoryService,
  ) {}

  ngOnInit() {}

  closeModal() {
    this.bsModalRef.hide();
  }

  chooseApp(app: string) {
    if (this.richMenu.top_screen) {
      if (app === APP_SETTING.ORDER) return;
      this._getTopScreenById(app);
    } else {
      this._getSettingRecommendScreen(app);
    }
  }

  private _getTopScreenById(app: string) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    const topScreenOpts: any = {
      with: app === APP_SETTING.SELF_ORDER ? APP_SETTING.SELF_ORDER_SETTING : APP_SETTING.ORDER_SETTING,
    };

    this.topImageSv.getDetailWithId(this.richMenu.id, topScreenOpts).subscribe(
      (res: any) => {
        this._openModalTopScreen(app, res);
        this.isLoading = false;
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  private _getSettingRecommendScreen(app: string) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;

    const opts: any = {
      with: 'self_order_rich_menu_settings,self_order_rich_menu_settings.items',
    };

    if (app === APP_SETTING.ORDER) {
      opts.with = 'order_rich_menu_settings,order_rich_menu_settings.items';
    }

    this.itemCategorySv.getSettingRecommendScreen(this.richMenu.id, app, opts).subscribe(
      (res: any) => {
        this._openModalRecommendScreen(app, res);
        this.isLoading = false;
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  private _openModalTopScreen(app: string, topScreen: TopScreen) {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-lg',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        topScreen: topScreen,
        currentUser: this.currentUser,
      },
    };

    switch (app) {
      case APP_SETTING.SELF_ORDER:
        return this._openModalWithComponent(ModalSettingSelfOrderTopScreenComponent, opts);
      case APP_SETTING.ORDER:
        return this._openModalWithComponent(ModalSettingOrderTopScreenComponent, opts);
    }
  }

  private _openModalRecommendScreen(app: string, itemCategory: ItemCategory) {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-lg',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        itemCategory: itemCategory,
        hasSetting: app === APP_SETTING.ORDER ? this.richMenu.has_setting_order : this.richMenu.has_setting_self_order,
        currentUser: this.currentUser,
      },
    };

    switch (app) {
      case APP_SETTING.SELF_ORDER:
        return this._openModalWithComponent(DialogSelfOrderComponent, opts);
      case APP_SETTING.ORDER:
        return this._openModalWithComponent(DialogRichMenuComponent, opts);
    }
  }

  private _openModalWithComponent(comp, opts: ModalOptions) {
    this.closeModal();
    this.modalSv.show(comp, opts);
  }
}
