import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { Menu } from 'shared/models/menu';
import { Category } from 'shared/models/category';
import { PageChangedInput, Pagination } from 'shared/models/pagination';

import { CategoryService } from 'shared/http-services/category.service';
import { MenuService } from 'shared/http-services/menu.service';
import { ToastService } from 'shared/logical-services/toast.service';

@Component({
  selector: 'app-modal-apply-tax-menus',
  templateUrl: './modal-apply-tax-menus.component.html',
  styleUrls: ['./modal-apply-tax-menus.component.scss'],
})
export class ModalApplyTaxMenusComponent implements OnInit {
  items: any = [];

  is_apply_all: boolean;

  listItems: Menu[] = [];
  categories: Category[] = [];
  categorySelected: Category;

  isSubmiting: boolean = false;
  isLoading: boolean = false;
  isLoadingCat: boolean = false;

  isPaginate: boolean = false;
  pagination: Pagination = new Pagination();

  constructor(
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private categorySv: CategoryService,
    private menuSv: MenuService,
    private toastSv: ToastService,
  ) {}

  ngOnInit() {
    this.fetchCategories();
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;

    this.pagination.pageChanged(paginate.page);
    this.fetchMenus();
  }

  private fetchCategories() {
    if (this.isLoadingCat) return;
    this.isLoadingCat = true;

    const opts: any = {
      with: 'items_count',
    };

    this.categories = [];

    return this.categorySv.getCategories(opts).subscribe(
      (res) => {
        this.categories = res.filter((cat: Category) => {
          return cat.items_count > 0;
        });

        this.isLoadingCat = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoadingCat = false;
      },
      () => {
        if (this.categories && this.categories.length !== 0) {
          this.selectCategory(this.categories[0]);
        }
      },
    );
  }

  private fetchMenus() {
    this.isLoading = true;

    const opts: any = {
      ...this.pagination.hasJSON(),
      category_id: this.categorySelected.id,
      is_show: 1,
    };

    this.menuSv.fetchMenus(opts, true).subscribe(
      (res) => {
        this.pagination.deserialize(res);
        this.checkListItems('items', res.data);
        this.isLoading = false;
        this.isPaginate = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this.isPaginate = false;
      },
    );
  }

  private fetchAllMenus() {
    const opts: any = {
      is_show: 1,
    };

    this.menuSv.fetchMenus(opts).subscribe(
      (res) => {
        this.items = res.data.filter((item: Menu) => (item.is_tax = true));
        this.listItems.filter((item: Menu) => (item.is_tax = true));
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  private checkListItems(type: string, data: any): void {
    if (!!this[type].length) {
      this[type].filter((item: Menu) => {
        const index = data.findIndex((sItem: Menu) => sItem.id === item.id);
        if (index !== -1) {
          data[index].is_tax = item.is_tax;
        }
      });
    }

    this.listItems = data;
  }

  confirmApplyTax() {
    this.items = this.items.filter((item: Menu) => item.is_tax);

    this.closeModal(
      JSON.stringify({
        is_apply_all: this.is_apply_all,
        items: this.items,
      }),
    );
  }

  applyAll() {
    this.is_apply_all = !this.is_apply_all;
    if (this.is_apply_all) {
      this.fetchAllMenus();
    } else {
      this.items = [];
      this.listItems.filter((item: Menu) => (item.is_tax = false));
    }
  }

  changeTax(menuItem: Menu) {
    if (this.is_apply_all) {
      this.is_apply_all = false;
    }

    this.settingTax('items', menuItem);
  }

  private settingTax(type: string, menuItem: Menu) {
    const idx = this[type].findIndex((item: Menu) => item.id === menuItem.id);
    if (idx !== -1) {
      this[type][idx] = menuItem;
    } else {
      this[type].push(menuItem);
    }
  }

  isActive(catId?: number): boolean {
    return this.categorySelected.id === catId;
  }

  selectCategory(category: Category) {
    this.pagination.reset();
    this.categorySelected = category;

    this.fetchMenus();
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
