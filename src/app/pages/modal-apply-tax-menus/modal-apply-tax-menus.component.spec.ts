import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalApplyTaxMenusComponent } from './modal-apply-tax-menus.component';

describe('ModalApplyTaxMenusComponent', () => {
  let component: ModalApplyTaxMenusComponent;
  let fixture: ComponentFixture<ModalApplyTaxMenusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalApplyTaxMenusComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalApplyTaxMenusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
