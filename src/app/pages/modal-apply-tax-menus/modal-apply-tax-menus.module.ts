import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { UiSwitchModule } from 'shared/modules/ui-switch/index';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';

import { ModalApplyTaxMenusComponent } from './modal-apply-tax-menus.component';

@NgModule({
  imports: [CommonModule, FormsModule, CasDialogModule, UiSwitchModule, CasPaginationModule],
  entryComponents: [ModalApplyTaxMenusComponent],
  declarations: [ModalApplyTaxMenusComponent],
  exports: [ModalApplyTaxMenusComponent],
})
export class ModalApplyTaxMenusModule {}
