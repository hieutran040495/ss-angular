import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDownloadCsvClientComponent } from './modal-download-csv-client.component';

describe('ModalDownloadCsvClientComponent', () => {
  let component: ModalDownloadCsvClientComponent;
  let fixture: ComponentFixture<ModalDownloadCsvClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalDownloadCsvClientComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDownloadCsvClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
