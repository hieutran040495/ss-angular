import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';

import { ModalDownloadCsvClientComponent } from './modal-download-csv-client.component';

@NgModule({
  declarations: [ModalDownloadCsvClientComponent],
  imports: [CommonModule, ModalModule.forRoot()],
  exports: [ModalDownloadCsvClientComponent],
  entryComponents: [ModalDownloadCsvClientComponent],
})
export class ModalDownloadCsvClientModule {}
