import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { DownloadFileService } from 'shared/http-services/download-file.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';

import { NOTIFY_CODE } from 'shared/enums/code-notify';
import { DownloadHelpers } from 'shared/utils/download';

interface ExportInput {
  code: string;
  id: string;
  type: string;
}

@Component({
  selector: 'app-modal-download-csv-client',
  templateUrl: './modal-download-csv-client.component.html',
  styleUrls: ['./modal-download-csv-client.component.scss'],
})
export class ModalDownloadCsvClientComponent implements OnInit {
  isLoading: boolean = false;
  clientUser: string = '';

  private path: { path: string };

  constructor(
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private downloadSv: DownloadFileService,
    private toastr: ToastService,
    private echoSv: LaravelEchoService,
  ) {}

  ngOnInit() {
    this.echoSv.onNotification((e: ExportInput) => {
      if (e.code === NOTIFY_CODE.CLIENT_USER_EXPORT_FINISH && this.path) {
        this.downloadCSV(this.path);
      }
    });
  }

  exportPathCSV() {
    if (this.isLoading) return;

    const opts = {
      name: this.clientUser,
      order: 'name',
    };

    this.isLoading = true;
    this.downloadSv.exportPathCSV(opts).subscribe(
      (res) => {
        this.path = res;
      },
      (error) => {
        this.toastr.error(error);
        this.isLoading = false;
      },
    );
  }

  private downloadCSV(path: { path: string }) {
    this.downloadSv.downloadCSV(path).subscribe(
      (res) => {
        DownloadHelpers.downloadFromUri(res.url);
        this.path = undefined;
        this.closeModal();
      },
      (error) => {
        this.toastr.error(error);
        this.isLoading = false;
      },
    );
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
