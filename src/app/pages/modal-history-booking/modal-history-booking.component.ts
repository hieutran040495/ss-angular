import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Reservation } from 'shared/models/reservation';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { User } from 'shared/models/user';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { EventEmitterType } from 'shared/enums/event-emitter';
import { DIALOG_EVENT } from 'shared/enums/modes';
import { RESV_ITEMS_TYPE } from 'shared/enums/reservation';
import { Menu } from 'shared/models/menu';
import { MenuBuffet } from 'shared/models/menu-buffet';
import { Course } from 'shared/models/course';

@Component({
  selector: 'app-modal-history-booking',
  templateUrl: './modal-history-booking.component.html',
  styleUrls: ['./modal-history-booking.component.scss'],
})
export class ModalHistoryBookingComponent implements OnInit {
  isLoading: boolean = false;
  resv: Reservation;
  user: User;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private resvSv: ReservationService,
    private toastSv: ToastService,
    private eventEmitterSv: EventEmitterService,
  ) {}

  ngOnInit() {
    this._getResvItems();
  }

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  close() {
    this.closeDialog(DIALOG_EVENT.CLOSE);
  }

  createReservation() {
    this.closeDialog(DIALOG_EVENT.CREATE_RESERVATION);
  }

  private _getResvItems() {
    const opts = {
      with: 'courses,items,buffets',
      reservation_id: this.resv.id,
    };

    this.resvSv.getResvItems(opts).subscribe(
      (res) => {
        this.resv.deserialize(res.data);
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  openUserDetails() {
    this.closeDialog();
    this.eventEmitterSv.publishData({
      type: EventEmitterType.OPEN_MODAL_USER_DETAIL,
    });
  }

  getPriceItem(item: Menu | MenuBuffet | Course): string {
    if (item.type === RESV_ITEMS_TYPE.ITEM) {
      if (item.total_price === 0) {
        return '0';
      }
      const price = item.total_price / item.quantity;
      return `${price.format()}`;
    }
    return item.price_format;
  }
}
