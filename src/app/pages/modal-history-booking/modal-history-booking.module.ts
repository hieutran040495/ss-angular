import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';

import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { CasTableModule } from 'app/modules/cas-table/cas-table.module';
import { ResizeTextModule } from 'shared/directive/resize-text/resize-text.module';

import { ModalHistoryBookingComponent } from './modal-history-booking.component';

@NgModule({
  declarations: [ModalHistoryBookingComponent],
  imports: [CommonModule, ModalModule.forRoot(), CasDialogModule, CasTableModule, ResizeTextModule],
  exports: [ModalHistoryBookingComponent],
  entryComponents: [ModalHistoryBookingComponent],
})
export class ModalHistoryBookingModule {}
