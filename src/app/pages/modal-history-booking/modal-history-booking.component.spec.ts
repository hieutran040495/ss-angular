import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalHistoryBookingComponent } from './modal-history-booking.component';

describe('ModalHistoryBookingComponent', () => {
  let component: ModalHistoryBookingComponent;
  let fixture: ComponentFixture<ModalHistoryBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalHistoryBookingComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalHistoryBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
