import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitDayComponent } from './profit-day.component';

describe('ProfitDayComponent', () => {
  let component: ProfitDayComponent;
  let fixture: ComponentFixture<ProfitDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfitDayComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
