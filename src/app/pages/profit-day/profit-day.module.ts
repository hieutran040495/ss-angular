import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ProfitDayComponent } from './profit-day.component';

import { ProfitSelectDateModule } from 'app/pages/profit-select-date/profit-select-date.module';
import { ProfitChartsModule } from 'app/pages/profit-charts/profit-charts.module';

const routes: Routes = [
  {
    path: '',
    component: ProfitDayComponent,
  },
];

@NgModule({
  declarations: [ProfitDayComponent],
  imports: [CommonModule, FormsModule, RouterModule.forChild(routes), ProfitSelectDateModule, ProfitChartsModule],
})
export class ProfitDayModule {}
