import { Component, OnInit } from '@angular/core';
import { PROFIT_DATETIME } from 'shared/enums/profit';

@Component({
  selector: 'app-profit-day',
  templateUrl: './profit-day.component.html',
  styleUrls: ['./profit-day.component.scss'],
})
export class ProfitDayComponent implements OnInit {
  profitType: string = PROFIT_DATETIME.DAY;

  constructor() {}

  ngOnInit() {}
}
