import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalResvConfirmCreateComponent } from './modal-resv-confirm-create.component';
import { FormsModule } from '@angular/forms';
import { SessionService } from 'shared/states/session';

@NgModule({
  imports: [CommonModule, FormsModule],
  entryComponents: [ModalResvConfirmCreateComponent],
  declarations: [ModalResvConfirmCreateComponent],
  exports: [ModalResvConfirmCreateComponent],
  providers: [SessionService],
})
export class ModalResvConfirmCreateModule {}
