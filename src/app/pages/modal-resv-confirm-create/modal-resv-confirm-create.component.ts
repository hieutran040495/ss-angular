import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Reservation } from 'shared/models/reservation';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { SessionQuery } from 'shared/states/session';
import { User } from 'shared/models/user';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { RESV_MODE_EMITTER } from 'shared/enums/event-emitter';

@Component({
  selector: 'app-modal-resv-confirm-create',
  templateUrl: './modal-resv-confirm-create.component.html',
  styleUrls: ['./modal-resv-confirm-create.component.scss'],
})
export class ModalResvConfirmCreateComponent implements OnInit {
  isLoading: boolean = false;
  resv: Reservation;
  receptionist: User;

  constructor(
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private resvSv: ReservationService,
    private toast: ToastService,
    private sessionQuery: SessionQuery,
    private eventEmitter: EventEmitterService,
  ) {}

  ngOnInit() {
    this.getTotalPrice();
    this.getUser();
  }

  private async getUser() {
    this.receptionist = await this.sessionQuery.getUserLoggined();
  }

  getTotalPrice() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.resvSv.getTotalPrice(this.resv.getTotalPrice()).subscribe(
      (res) => {
        this.resv.total_price = res.total_fee;
        this.isLoading = false;
      },
      (errors) => {
        this.toast.error(errors);
        this.isLoading = false;
      },
    );
  }

  confirmCreateResv() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.resvSv.createResv(this.resv.formData()).subscribe(
      (res) => {
        this.toast.success('予約を作成しました');
        this.isLoading = false;
        this.closeModal();
      },
      (errors) => {
        this.toast.error(errors);
        this.isLoading = false;
      },
    );
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  backToEdit() {
    this.closeModal();
    this.eventEmitter.publishData({
      type: RESV_MODE_EMITTER.EDIT,
      data: this.resv,
    });
  }
}
