import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalResvConfirmCreateComponent } from './modal-resv-confirm-create.component';

describe('ModalResvConfirmCreateComponent', () => {
  let component: ModalResvConfirmCreateComponent;
  let fixture: ComponentFixture<ModalResvConfirmCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalResvConfirmCreateComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalResvConfirmCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
