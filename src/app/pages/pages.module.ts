import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';

import { PagesComponent } from './pages.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NotificationModule } from './notification/notification.module';
import { ModalIncomingCallModule } from './modal-incoming-call/modal-incoming-call.module';

@NgModule({
  declarations: [PagesComponent],
  imports: [CommonModule, PagesRoutingModule, ModalModule.forRoot(), NotificationModule, ModalIncomingCallModule],
})
export class PagesModule {}
