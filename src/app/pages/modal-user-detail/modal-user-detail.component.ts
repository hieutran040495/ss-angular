import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { DIALOG_EVENT } from 'shared/enums/modes';
import { User } from 'shared/models/user';
import { Reservation } from 'shared/models/reservation';
import { Pagination, PageChangedInput } from 'shared/models/pagination';

import { UserService } from 'shared/http-services/user.service';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { TableHeader } from 'app/modules/cas-table/cas-table.component';
import { ModalHistoryBookingComponent } from '../modal-history-booking/modal-history-booking.component';

@Component({
  selector: 'app-modal-user-detail',
  templateUrl: './modal-user-detail.component.html',
  styleUrls: ['./modal-user-detail.component.scss'],
})
export class ModalUserDetailComponent implements OnInit {
  isLoading: boolean = false;
  isSubmiting: boolean = false;
  isReloadUsers: boolean = false;

  user: User = new User();
  reservations: Reservation[] = [];
  isPagination: boolean;
  pagination: Pagination = new Pagination().deserialize({ per_page: 5 });

  tableHeader: Partial<TableHeader>[] = [
    {
      name: '日時',
      width: 240,
      class: 'text-center',
    },
    {
      name: `金額`,
      width: 150,
      class: 'text-center',
    },
  ];

  constructor(
    private reservationSv: ReservationService,
    private userSv: UserService,
    private toastSv: ToastService,
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
  ) {}

  ngOnInit() {
    this.getReservations();
  }

  updateUser() {
    if (this.isSubmiting) {
      return;
    }
    this.isSubmiting = true;
    this.userSv.updateUser(this.user.id, this.user.userFormData()).subscribe(
      (res) => {
        this.toastSv.success('お客様メモを更新しました');
        this.isSubmiting = false;
        this.isReloadUsers = true;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isSubmiting = false;
      },
    );
  }

  private getReservations() {
    this.isLoading = true;

    const opts = {
      ...this.pagination.hasJSON(),
      client_user_id: this.user.id,
      order: '-start_at',
      with: 'coupon',
    };
    this.reservations = [];

    this.reservationSv.getReservations(opts).subscribe(
      (res) => {
        this.reservations = res.data;
        this.pagination.deserialize(res);
        this.isLoading = false;
        this.isPagination = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this.isPagination = false;
      },
    );
  }

  createReservation() {
    this._closeDialog(DIALOG_EVENT.CREATE_RESERVATION);
  }

  private _closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPagination) {
      return;
    }
    this.isPagination = true;

    this.pagination.pageChanged(paginate.page);

    this.getReservations();
  }

  close() {
    if (this.isReloadUsers) {
      this._closeDialog(DIALOG_EVENT.USER_LIST_RELOAD);
      return;
    }

    this._closeDialog(DIALOG_EVENT.CLOSE);
  }

  openReceiptDetails(resv: Reservation) {
    this._closeDialog();
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-lg',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        resv: resv,
        user: this.user,
      },
    };

    this.bsModalSv.show(ModalHistoryBookingComponent, opts);
  }
}
