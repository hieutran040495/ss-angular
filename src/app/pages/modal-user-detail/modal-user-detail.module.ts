import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

import { ModalUserDetailComponent } from './modal-user-detail.component';
import { CasTableModule } from 'app/modules/cas-table/cas-table.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalHistoryBookingModule } from '../modal-history-booking/modal-history-booking.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CasPaginationModule.forRoot(),
    CasDialogModule,
    CasTableModule,
    ModalHistoryBookingModule,
    ModalModule.forRoot(),
  ],
  declarations: [ModalUserDetailComponent],
  entryComponents: [ModalUserDetailComponent],
})
export class ModalUserDetailModule {}
