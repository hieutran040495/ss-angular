import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ModalSettingTwilioComponent } from '../modal-setting-twilio/modal-setting-twilio.component';

@Component({
  selector: 'app-modal-confirm-twilio',
  templateUrl: './modal-confirm-twilio.component.html',
  styleUrls: ['./modal-confirm-twilio.component.scss'],
})
export class ModalConfirmTwilioComponent implements OnInit {
  constructor(private modalSv: BsModalService, private bsModalRef: BsModalRef) {}

  ngOnInit() {}

  /**
   * confirmSettingTwilio
   */
  public confirmSettingTwilio() {
    this.closeModal();
    this._openModalWithComponent(ModalSettingTwilioComponent);
  }

  private _openModalWithComponent(comp, initialState?: any) {
    const config: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      initialState: initialState,
      keyboard: false,
      ignoreBackdropClick: true,
    };

    this.bsModalRef = this.modalSv.show(comp, config);
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
