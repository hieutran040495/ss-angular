import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfirmTwilioComponent } from './modal-confirm-twilio.component';

describe('ModalConfirmTwilioComponent', () => {
  let component: ModalConfirmTwilioComponent;
  let fixture: ComponentFixture<ModalConfirmTwilioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalConfirmTwilioComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfirmTwilioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
