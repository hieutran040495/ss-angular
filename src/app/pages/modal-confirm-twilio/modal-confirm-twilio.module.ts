import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalConfirmTwilioComponent } from './modal-confirm-twilio.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { ModalSettingTwilioModule } from '../modal-setting-twilio/modal-setting-twilio.module';

@NgModule({
  imports: [CommonModule, CasDialogModule, ModalModule.forRoot(), ModalSettingTwilioModule],
  declarations: [ModalConfirmTwilioComponent],
  entryComponents: [ModalConfirmTwilioComponent],
  exports: [ModalConfirmTwilioComponent],
})
export class ModalConfirmTwilioModule {}
