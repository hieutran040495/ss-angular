import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';

import { Table } from 'shared/models/table';
import { TableService } from 'shared/http-services/table.service';

import * as isNumber from 'lodash/isNumber';

@Injectable()
export class TableCeResolve implements Resolve<Table> {
  constructor(private router: Router, private tableSv: TableService) {}

  resolve(routeSnapshot: ActivatedRouteSnapshot) {
    if (routeSnapshot.params.buffetId && !isNumber(routeSnapshot.params.tableId)) {
      this.router.navigate(['/setting/table']);
      return;
    }
    // TODO call api from sv

    return this.tableSv.fetchTableById(routeSnapshot.params.tableId, {
      with: 'type',
    });
  }
}
