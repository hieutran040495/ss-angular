import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTableCeComponent } from './modal-table-ce.component';

describe('ModalTableCeComponent', () => {
  let component: ModalTableCeComponent;
  let fixture: ComponentFixture<ModalTableCeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalTableCeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTableCeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
