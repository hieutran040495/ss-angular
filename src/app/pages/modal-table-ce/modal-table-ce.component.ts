import { Component, OnInit, ChangeDetectorRef, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { TableService } from 'shared/http-services/table.service';
import { TableTypeService } from 'shared/http-services/table-type.service';
import { ValidatorService } from 'shared/utils/validator.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { Pagination } from 'shared/models/pagination';
import { Table } from 'shared/models/table';
import { TableType } from 'shared/models/table-type';

import { Mode } from 'shared/utils/mode';

import { Regexs } from 'shared/constants/regex';
import { DIALOG_EVENT } from 'shared/enums/modes';
import { OrdinalInput } from 'shared/interfaces/ordinal-input';

import { ModalWaringUpdateTableComponent } from '../modal-waring-update-table/modal-waring-update-table.component';
import { DialogRemoveComponent } from 'app/pages/setting/table/dialog-remove/dialog-remove.component';

import { concat, Observable, of, Subject, Subscription } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-modal-table-ce',
  templateUrl: './modal-table-ce.component.html',
  styleUrls: ['./modal-table-ce.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalTableCeComponent implements OnInit, OnDestroy {
  mode: Mode = new Mode();

  isLoading: boolean = false;
  isPagination: boolean = false;

  table: Table = new Table();
  validation: any = {};

  isLoadTableType: boolean = false;
  tablesType$: Observable<TableType[]>;
  tablesTypeInput$: Subject<string> = new Subject<string>();
  tablesTypeDefault: TableType[] = [];

  rules = Regexs;

  pagination: Pagination = new Pagination();

  get dialogTitle(): string {
    return this.table.id ? 'テーブル編集' : 'テーブル新規作成';
  }

  ordinalInput: OrdinalInput;

  private oldQuantity: number;
  private tableSubscription: Subscription;

  constructor(
    private tableSv: TableService,
    private validateSv: ValidatorService,
    private tableTypeSv: TableTypeService,
    private toastSv: ToastService,
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private _cd: ChangeDetectorRef,
  ) {
    this.pagination.per_page = 50;
  }

  ngOnInit(): void {
    if (this.table.id) {
      this.mode.setEdit();
      this.oldQuantity = this.table.quantity;
    } else {
      this.mode.setNew();
      this.table.type = undefined;
    }

    this.ordinalInput = {
      name: this.table.name,
      ordinal_number: this.table.ordinal_number,
    };
    this.getTablesType();
  }

  ngOnDestroy(): void {
    if (this.tableSubscription) {
      this.tableSubscription.unsubscribe();
    }
  }

  private getTablesType() {
    if (this.isLoadTableType || this.isPagination) return;

    const opts = {
      ...this.pagination.hasJSON(),
      order: '-updated',
    };
    this._cd.detectChanges();

    this.tableTypeSv.fetchTableTypes(opts).subscribe(
      (res: any) => {
        this.tablesTypeDefault = this.tablesTypeDefault.concat(res.data);
        this.searchTablesType(opts);
        this.pagination.deserialize(res);
        this._cd.detectChanges();
      },
      (errors: any) => {
        this.toastSv.error(errors.error);
        this.isLoadTableType = false;
        this.isPagination = false;
        this._cd.detectChanges();
      },
    );
  }

  private searchTablesType(opts: any) {
    this.pagination.reset();

    this.tablesType$ = concat(
      of(this.tablesTypeDefault),
      this.tablesTypeInput$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => {
          this.isLoadTableType = true;
          this._cd.detectChanges();
        }),
        switchMap((name) => {
          if (name && name.trim().length > 0) {
            opts.name = name.trim();
          } else {
            delete opts.name;
          }

          return this.tableTypeSv.searchTableTypes(opts).pipe(
            catchError(() => of([])),
            tap(() => {
              this.isLoadTableType = false;
              this.isPagination = false;
              this._cd.detectChanges();
            }),
          );
        }),
      ),
    );
  }

  closeModal(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  submitTable() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    this._cd.detectChanges();

    if (this.table.id) {
      this._checkValidSeats();
    } else {
      this.createTable();
    }
  }

  private createTable() {
    this.tableSv.createTable(this.table.formDataString()).subscribe(
      (res) => {
        this.toastSv.success('テーブルを作成しました');
        this.closeModal(DIALOG_EVENT.TABLE_RELOAD);
        this._cd.detectChanges();
      },
      (error) => {
        if (error.hasOwnProperty('errors')) {
          this.validation = this.validateSv.setErrors(error.errors);
        }
        this.toastSv.error(error);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  private updateTable() {
    this.tableSv.editTable(this.table.formDataString()).subscribe(
      (res) => {
        this.toastSv.success('テーブルを編集しました');
        this.closeModal(DIALOG_EVENT.TABLE_RELOAD);
        this._cd.detectChanges();
      },
      (error) => {
        if (error.hasOwnProperty('errors')) {
          this.validation = this.validateSv.setErrors(error.errors);
        }
        this.toastSv.error(error);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  openDialogRemoveTable() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      keyboard: false,
      ignoreBackdropClick: true,
      initialState: {
        tableID: this.table.id,
      },
    };
    this._openModalWithComponent(DialogRemoveComponent, opts);
  }

  private _checkValidSeats() {
    if (this.oldQuantity === this.table.quantity) {
      return this.updateTable();
    }

    const data = {
      quantity: this.table.quantity,
    };

    this.tableSv.checkValidSeats(this.table.id, data).subscribe(
      (res) => {
        if (res.valid) {
          return this.updateTable();
        }
        this._openModalWaringTable();
      },
      (error) => {
        this.toastSv.error(error);
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  resetValidate(key) {
    this.validation[key] = null;
    this._cd.detectChanges();
  }

  changePage(data: any) {
    if (this.isPagination || !this.pagination.hasNextPage()) return;

    this.pagination.nextPage();
    this.isPagination = true;
    this.getTablesType();
  }

  onChangeOrdinal() {
    if (!this.ordinalInput) {
      return;
    }
    this.table.name = this.ordinalInput.name;
    this.table.ordinal_number = this.ordinalInput.ordinal_number;
  }

  private _openModalWaringTable() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
    };

    this._openModalWithComponent(ModalWaringUpdateTableComponent, opts);
  }

  private _openModalWithComponent(comp, opts: ModalOptions) {
    this.tableSubscription = this.bsModalSv.onHidden.subscribe((reason: string) => {
      if (reason === DIALOG_EVENT.ACCEPTED_UPDATE_TABLE) {
        this.updateTable();
      }

      if (reason === DIALOG_EVENT.TABLE_REMOVE) {
        this.closeModal(DIALOG_EVENT.TABLE_REMOVE);
      }
    });

    this.bsModalSv.show(comp, opts);
  }
}
