import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';

import { ValidatorService } from 'shared/utils/validator.service';

import { ModalTableCeComponent } from './modal-table-ce.component';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';

import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { InputTextModule } from 'shared/modules/input-text/input-text.module';
import { InputNameWithIdModule } from 'app/modules/input-name-with-id/input-name-with-id.module';
import { ModalWaringUpdateTableModule } from '../modal-waring-update-table/modal-waring-update-table.module';
import { DialogRemoveModule } from 'app/pages/setting/table/dialog-remove/dialog-remove.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    OnlyNumericModule,
    SpaceValidationModule,
    CasDialogModule,
    CasNgSelectModule,
    InputTextModule,
    InputNameWithIdModule,
    ModalWaringUpdateTableModule,
    DialogRemoveModule,
  ],
  declarations: [ModalTableCeComponent],
  entryComponents: [ModalTableCeComponent],
  exports: [ModalTableCeComponent],
  providers: [ValidatorService],
})
export class ModalTableCeModule {}
