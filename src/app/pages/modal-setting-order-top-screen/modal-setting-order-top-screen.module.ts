import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalSettingOrderTopScreenComponent } from './modal-setting-order-top-screen.component';
import { FormsModule } from '@angular/forms';
import { CasCarouselModule } from 'app/modules/cas-carousel/cas-carousel.module';
import { CasPopupGuideDragModule } from 'app/modules/cas-popup-guide-drag/cas-popup-guide-drag.module';
import { ScrollerModule } from 'shared/directive/scroller/scroller.module';
import { ModalSettingDisplayModule } from '../modal-setting-display/modal-setting-display.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CasOrderTopImageModule } from 'shared/modules/cas-order-top-image/cas-order-top-image.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CasCarouselModule,
    CasPopupGuideDragModule,
    CasOrderTopImageModule,
    ScrollerModule,
    ModalSettingDisplayModule,
    ModalModule.forRoot(),
  ],
  declarations: [ModalSettingOrderTopScreenComponent],
  entryComponents: [ModalSettingOrderTopScreenComponent],
  exports: [ModalSettingOrderTopScreenComponent],
})
export class ModalSettingOrderTopScreenModule {}
