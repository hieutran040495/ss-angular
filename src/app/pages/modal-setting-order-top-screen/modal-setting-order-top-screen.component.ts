import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { SCREENS_SIZE } from 'shared/constants/screen-size';
import { Subject, Subscription, ObservableInput, forkJoin } from 'rxjs';
import { TopImage } from 'shared/models/top-image';
import { ORDER, SETTING_DISPLAY } from 'shared/enums/event-emitter';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { StyleOpts } from 'shared/interfaces/style-setting';
import { ModalSettingDisplayComponent } from '../modal-setting-display/modal-setting-display.component';
import { TopScreen } from 'shared/models/top-screen';
import { TopScreenService } from 'shared/http-services/top-screen.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { PresignedUrls } from 'shared/interfaces/presigned-url';
import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-modal-setting-order-top-screen',
  templateUrl: './modal-setting-order-top-screen.component.html',
  styleUrls: ['./modal-setting-order-top-screen.component.scss'],
})
export class ModalSettingOrderTopScreenComponent implements OnInit, OnDestroy {
  topScreen: TopScreen = new TopScreen();

  // Config
  screenSize = SCREENS_SIZE[1].value;
  onEventEmitter: Subject<any> = new Subject();
  topImage: TopImage = new TopImage();

  // Config CasCarousel
  slideItems: TopImage[];
  styleOpts: StyleOpts;
  isLoading: boolean;

  private _subEvent: Subscription;
  private _emitEvent: Subscription;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private emitSv: EventEmitterService,
    private topScreenSv: TopScreenService,
    private toastSv: ToastService,
  ) {}

  ngOnInit() {
    this._subEvent = this.emitSv.caseNumber$.subscribe((res) => {
      if (!res && !res.type) {
        return;
      }
      if (res.type === SETTING_DISPLAY.CHANGE_SETTING_DISPLAY) {
        this.onEventEmitter.next({
          type: ORDER.CHANGE_SETTING_DISPLAY,
          data: res.data,
        });
        this.styleOpts = res.data;
      }
    });

    this._initEventEmitter();
    this._initTopScreenSlides();
    this._initSettingDisplay();
  }

  ngOnDestroy() {
    this._subEvent.unsubscribe();
    this._emitEvent.unsubscribe();
  }

  private _initEventEmitter() {
    this._emitEvent = this.onEventEmitter.subscribe((res) => {
      if (!res && !res.type) {
        return;
      }

      switch (res.type) {
        case ORDER.CHANGE_TOP_SCREEN:
          this.topImage = res.data.image;
          this.styleOpts = {
            text_color: res.data.image.text_color,
            bg_color: res.data.image.bg_color,
            font_family: res.data.image.font_family,
          };
          break;
        case ORDER.TRANSFER_TOP_SCREEN_DATA:
          this.topScreen.top_screens = res.data;
          this._saveSettingTopScreen(res.data);
          break;
      }
    });
  }

  private _initSettingDisplay() {
    this.styleOpts = {
      text_color: this.topScreen.top_screens[0].text_color,
      bg_color: this.topScreen.top_screens[0].bg_color,
      font_family: this.topScreen.top_screens[0].font_family,
    };
  }

  private _initTopScreenSlides() {
    this.topImage = this.topScreen.top_screens[0];
    this.slideItems = this.topScreen.top_screens;
  }

  private _saveSettingTopScreen(topScreens: TopImage[]) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    const fileUploads = [];

    topScreens.forEach((item) => {
      if (item.file) {
        fileUploads.push(item);
      }
    });

    if (!fileUploads.length) {
      return this._saveSetting();
    }
    this._presignedUrls(fileUploads, topScreens);
  }

  private _presignedUrls(files: File[], topScreens: TopImage[]) {
    const data = {
      quantity: files.length,
    };
    this.topScreenSv.presignedUrls(data).subscribe(
      (res) => {
        this._publicImage(res.data, topScreens);
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  private _publicImage(presUrls: PresignedUrls[], topScreens: TopImage[]) {
    let i: number = 0;
    const urlsUpload: Array<ObservableInput<any>> = [];
    topScreens.forEach((item, index) => {
      if (item.file) {
        item.image_path = presUrls[i].path;
        topScreens[index] = item;
        const url = this.topScreenSv.publicImagePath(presUrls[i].url, item.file, item.file.type);
        urlsUpload.push(url);
        i++;
      }
    });

    forkJoin(urlsUpload).subscribe((res) => {
      this._saveSetting();
    });
  }

  private _saveSetting() {
    this.topScreenSv.settingTopScreen(this.topScreen.id, this.topScreen.toJSON()).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastSv.success('オーダーの来店画面を設定しました');
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  saveTopScreen() {
    return this.onEventEmitter.next({
      type: ORDER.SAVE_TOP_SCREEN,
    });
  }

  settingDisplay() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-dialog-absolute-centered modal-md',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        settingOpts: cloneDeep(this.styleOpts),
      },
    };

    this.bsModalSv.show(ModalSettingDisplayComponent, opts);
  }

  closeModal(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
