import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSettingOrderTopScreenComponent } from './modal-setting-order-top-screen.component';

describe('ModalSettingOrderTopScreenComponent', () => {
  let component: ModalSettingOrderTopScreenComponent;
  let fixture: ComponentFixture<ModalSettingOrderTopScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalSettingOrderTopScreenComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSettingOrderTopScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
