import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResvListComponent } from './resv-list.component';

describe('ResvListComponent', () => {
  let component: ResvListComponent;
  let fixture: ComponentFixture<ResvListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResvListComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResvListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
