import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

import { ModalResvDetailsModule } from 'app/pages/modal-resv-details/modal-resv-details.module';
import { ModalResvWalkinModule } from 'app/pages/modal-resv-walkin/modal-resv-walkin.module';

import { ResvListComponent } from './resv-list.component';
import { CalendarModule } from 'shared/modules/calendar/calendar.module';

import { ClientService } from 'shared/http-services/client.service';

const routes: Routes = [
  {
    path: '',
    component: ResvListComponent,
  },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot(),
    FormsModule,
    CalendarModule,
    ModalResvDetailsModule,
    ModalResvWalkinModule,
  ],
  declarations: [ResvListComponent],
  providers: [ClientService],
})
export class ResvListModule {}
