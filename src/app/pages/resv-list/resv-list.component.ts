import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { Client } from 'shared/models/client';

import { ToastService } from 'shared/logical-services/toast.service';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ReservationStatistic } from 'shared/models/reservation-statistic';
import { ClientService } from 'shared/http-services/client.service';

import { ModalResvDetailsComponent } from 'app/pages/modal-resv-details/modal-resv-details.component';
import { ModalResvWalkinComponent } from 'app/pages/modal-resv-walkin/modal-resv-walkin.component';

import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { RESV_MODE } from 'shared/enums/resv-mode';

@Component({
  selector: 'app-resv-list',
  templateUrl: './resv-list.component.html',
  styleUrls: ['./resv-list.component.scss'],
})
export class ResvListComponent implements OnInit, OnDestroy {
  resvStatistic: ReservationStatistic = new ReservationStatistic();
  defaultDate: string;
  selectedResvId: number;
  clientInfo: Client = new Client();

  isLoadingStatistic: boolean = false;

  private dateStatistic: {
    start_at_gte: string;
    start_at_lte: string;
  };

  private bsModalRef: BsModalRef;

  constructor(
    private resvSv: ReservationService,
    private toastSv: ToastService,
    private bsModalSv: BsModalService,
    private activeRouter: ActivatedRoute,
    private router: Router,
    private clientSv: ClientService,
  ) {
    this.activeRouter.queryParams.subscribe((res) => {
      if (res.reservation_id) {
        this.defaultDate = res.start_at;
        this.selectedResvId = res.reservation_id;
        this.router.navigate(['/reservations'], { replaceUrl: true });
        if (this.selectedResvId && this.defaultDate) {
          this.openModalDetailResv();
        }
      }
    });
  }

  ngOnInit() {
    this.dateStatistic = this.getTimeStatistic();
    this.getStatistic(this.dateStatistic);
    this.getClient();
  }

  private getTimeStatistic() {
    if (this.defaultDate) {
      return {
        start_at_gte: moment(this.defaultDate, 'YYYY/MM/DD')
          .startOf('day')
          .toISOString(),
        start_at_lte: moment(this.defaultDate, 'YYYY/MM/DD')
          .endOf('day')
          .toISOString(),
      };
    }
    return {
      start_at_gte: moment()
        .startOf('day')
        .toISOString(),
      start_at_lte: moment()
        .endOf('day')
        .toISOString(),
    };
  }

  ngOnDestroy() {
    if (this.bsModalRef) {
      this.bsModalRef.hide();
    }
  }

  private getClient() {
    this.clientSv.fetchClient().subscribe(
      (res) => {
        this.clientInfo = res;
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  private getStatistic(dateOpts: any) {
    if (this.isLoadingStatistic) {
      return;
    }

    this.isLoadingStatistic = true;

    this.resvSv.getResvStatistic(dateOpts).subscribe(
      (res) => {
        this.resvStatistic = new ReservationStatistic().deserialize(res);
        this.isLoadingStatistic = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoadingStatistic = false;
      },
    );
  }

  dayClick($event) {
    const walkinData: any = {
      table: $event.walkinTable,
      startTime: $event.date.toISOString(),
    };

    if (
      $event.date.isBefore(new Date()) ||
      $event.date.isAfter(
        moment()
          .add(1, 'hour')
          .toISOString(),
      )
    ) {
      walkinData.startTime = null;
      this.toastSv.warning('現在時刻より1時間の範囲内にて選択してください');
    }

    this.openModalWalkinResv(walkinData);
  }

  eventClick($event) {
    this.selectedResvId = $event.reservation.id;
    this.openModalDetailResv();
  }

  scrollEnd($event) {
    // TODO call api change data
  }

  private openModalDetailResv() {
    const opts: ModalOptions = {
      class: 'cas-modal-lg modal-dialog-centered',
      keyboard: false,
      ignoreBackdropClick: true,
      initialState: {
        reservationId: this.selectedResvId,
      },
    };
    this.openModalWithComponent(ModalResvDetailsComponent, opts);
  }

  openModalWalkinResv(walkinData?: any) {
    const opts: ModalOptions = {
      class: 'cas-modal modal-dialog-centered',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        walkinData: walkinData,
        clientInfo: this.clientInfo,
      },
    };
    this.openModalWithComponent(ModalResvWalkinComponent, opts);
  }

  private openModalWithComponent(comp, modalOptions: ModalOptions) {
    const subscribe = this.bsModalSv.onHidden.subscribe((reason: string) => {
      if (reason === RESV_MODE.REMOVE || reason === RESV_MODE.REFRESH) {
        this.getStatistic(this.dateStatistic);
        this.defaultDate = undefined;
        this.selectedResvId = undefined;
        subscribe.unsubscribe();
      }
    });

    this.bsModalRef = this.bsModalSv.show(comp, modalOptions);
  }
}
