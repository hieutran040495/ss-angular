import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DIALOG_EVENT } from 'shared/enums/modes';

@Component({
  selector: 'app-modal-waring-update-table',
  templateUrl: './modal-waring-update-table.component.html',
  styleUrls: ['./modal-waring-update-table.component.scss'],
})
export class ModalWaringUpdateTableComponent implements OnInit {
  DIALOG_EVENT = DIALOG_EVENT;

  constructor(private modalSv: BsModalService, private bsModalRef: BsModalRef) {}

  ngOnInit() {}

  closeModal(reason?: string) {
    if (reason) {
      this.modalSv.setDismissReason(reason);
    }
    this.bsModalRef.hide();
  }
}
