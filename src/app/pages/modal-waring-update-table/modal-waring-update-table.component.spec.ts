import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalWaringUpdateTableComponent } from './modal-waring-update-table.component';

describe('ModalWaringUpdateTableComponent', () => {
  let component: ModalWaringUpdateTableComponent;
  let fixture: ComponentFixture<ModalWaringUpdateTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalWaringUpdateTableComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalWaringUpdateTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
