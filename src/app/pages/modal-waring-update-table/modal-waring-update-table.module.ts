import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalWaringUpdateTableComponent } from './modal-waring-update-table.component';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [ModalWaringUpdateTableComponent],
  imports: [CommonModule, CasDialogModule, ModalModule.forRoot()],
  exports: [ModalWaringUpdateTableComponent],
  entryComponents: [ModalWaringUpdateTableComponent],
})
export class ModalWaringUpdateTableModule {}
