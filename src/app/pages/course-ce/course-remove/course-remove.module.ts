import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

import { CourseRemoveComponent } from './course-remove.component';

@NgModule({
  declarations: [CourseRemoveComponent],
  imports: [CommonModule, CasDialogModule],
  entryComponents: [CourseRemoveComponent],
  exports: [CourseRemoveComponent],
})
export class CourseRemoveModule {}
