import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { CourseService } from 'shared/http-services/course.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { DIALOG_EVENT } from 'shared/enums/modes';

@Component({
  selector: 'app-course-remove',
  templateUrl: './course-remove.component.html',
  styleUrls: ['./course-remove.component.scss'],
})
export class CourseRemoveComponent implements OnInit {
  courseId: number;
  isLoading: boolean = false;

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private toastSv: ToastService,
    private courseSv: CourseService,
  ) {}

  ngOnInit() {}

  courseRemove() {
    if (this.isLoading) return;

    this.isLoading = true;

    this.courseSv.deleteCourse(this.courseId).subscribe(
      (res: any) => {
        this.toastSv.success('コースを削除しました');
        this.closeDialog(DIALOG_EVENT.COURSE_REMOVE);
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
    this.isLoading = false;
  }
}
