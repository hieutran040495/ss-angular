import { Component, OnInit, ChangeDetectorRef, OnDestroy, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { FileUpload } from 'shared/interfaces/file-upload';
import { Course } from 'shared/models/course';
import { Menu } from 'shared/models/menu';
import { MenuBuffet } from 'shared/models/menu-buffet';
import { Regexs } from 'shared/constants/regex';

import { CourseService } from 'shared/http-services/course.service';
import { ValidatorService } from 'shared/utils/validator.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { CourseRemoveComponent } from './course-remove/course-remove.component';

import { Mode } from 'shared/utils/mode';
import { DIALOG_EVENT } from 'shared/enums/modes';
import { DROPZONE_TYPE } from 'shared/enums/drop-zone';
import { OPTION_TYPE } from 'shared/enums/type-search-order';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-manage-course-ce',
  templateUrl: './course-ce.component.html',
  styleUrls: ['./course-ce.component.scss'],
})
export class CourseCeComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  eventDropzone: EventEmitter<any> = new EventEmitter<any>();

  mode: Mode = new Mode();

  course: Course = new Course();
  thumbnail: FileUpload;

  isSetCoursePrice: boolean = false;
  validation: any = {};
  validatorQuatity: any = {};
  validationImg: boolean = false;
  rules = Regexs;

  OPTION_TYPE = OPTION_TYPE;

  private pageSub;
  private modalSubscribe: Subscription;

  constructor(
    private courseSv: CourseService,
    private validationSv: ValidatorService,
    private toastrSv: ToastService,
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private _cd: ChangeDetectorRef,
  ) {
    this.pageSub = this.eventDropzone.subscribe((res: any) => {
      if (!res.type) {
        return;
      }

      if (res.type === DROPZONE_TYPE.UPLOAD) {
        this.thumbnail = {
          key: 'image',
          value: res.data[0].origin,
          name: 'menuImg',
        };
        this._cd.detectChanges();
      }
    });
  }

  ngOnInit() {
    this.mode.setNew();
    if (this.course && this.course.id) {
      this.mode.setEdit();
      this.initialCourseData();
    }
  }

  ngOnDestroy(): void {
    if (this.pageSub) {
      this.pageSub.unsubscribe();
    }

    if (this.modalSubscribe) {
      this.modalSubscribe.unsubscribe();
    }
  }

  initialCourseData() {
    this.course.items = this.course.items.filter((item) => !item.only_buffet);
    this._cd.detectChanges();
  }

  setCoursePrice() {
    this.isSetCoursePrice = true;
    this.resetValidation('price');
    this._cd.detectChanges();
  }

  submitCourse() {
    if (this.isLoading) return;

    this.isLoading = true;
    this._cd.detectChanges();

    if (this.mode.isEdit) {
      this.updateCourse();
    } else {
      this.createCourse();
    }
  }

  private createCourse() {
    this.courseSv.createCourse([this.thumbnail], this.course.formData()).subscribe(
      (res) => {
        this.toastrSv.success('コースを作成しました');
        this.closeModal(DIALOG_EVENT.COURSE_RELOAD);
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validationSv.setErrors(errors.errors);
          this.validatorQuatity = this.validationSv.setErrorsQuatity(errors.errors);
          this.renderValidatorItems();
          this.renderValidatorBuffets();
        }
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  private updateCourse() {
    this.courseSv.updateCourse(this.course.id, [this.thumbnail], this.course.formData()).subscribe(
      (res) => {
        this.toastrSv.success('コースを更新しました');
        this.closeModal(DIALOG_EVENT.COURSE_RELOAD);
        this._cd.detectChanges();
      },
      (errors) => {
        this.toastrSv.error(errors);
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validationSv.setErrors(errors.errors);
          this.validatorQuatity = this.validationSv.setErrorsQuatity(errors.errors);
          this.renderValidatorItems();
          this.renderValidatorBuffets();
        }
        this.isLoading = false;
        this._cd.detectChanges();
      },
    );
  }

  private renderValidatorItems() {
    if (!this.validatorQuatity.items) {
      return;
    }
    this.course.items.forEach((item, key) => {
      if (this.validatorQuatity.items) {
        this.course.items[key].validator = this.validatorQuatity.items[key];
      }
    });

    this._cd.detectChanges();
  }

  private renderValidatorBuffets() {
    if (!this.validatorQuatity.buffets) {
      return;
    }
    this.course.buffets.forEach((item, key) => {
      if (this.validatorQuatity.buffets) {
        this.course.buffets[key].validator = this.validatorQuatity.buffets[key];
      }
    });

    this._cd.detectChanges();
  }

  addCoursePrice(name?: string) {
    if (name) {
      this.resetValidation(name);
    }

    this.updateInputPrice();
  }

  private updateInputPrice() {
    if (this.isSetCoursePrice || (this.course && this.course.id)) return;

    let total: number = 0;
    if (this.course.items.length !== 0) {
      this.course.items.map((item: Menu) => {
        if (typeof item.quantity === 'undefined') {
          item.quantity = 1;
        }
        total += item.quantity * item.price;
      });
    }

    if (this.course.buffets.length !== 0) {
      this.course.buffets.map((item: MenuBuffet) => {
        if (typeof item.quantity === 'undefined') {
          item.quantity = 1;
        }
        total += item.quantity * item.price;
      });
    }

    this.course.price = total;

    this._cd.detectChanges();
  }

  resetValidation(key: string): void {
    this.validation[key] = undefined;
    this._cd.detectChanges();
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  deleteCourse() {
    const opts: ModalOptions = {
      class: 'modal-dialog-centered modal-sm',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        courseId: this.course.id,
      },
    };

    this._openModalWithComponent(CourseRemoveComponent, opts);
  }

  private _openModalWithComponent(comp, opts: ModalOptions = {}) {
    this.modalSubscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      if (reason === DIALOG_EVENT.COURSE_REMOVE) {
        this.closeModal(DIALOG_EVENT.COURSE_REMOVE);
      }
    });

    this.modalSv.show(comp, opts);
  }
}
