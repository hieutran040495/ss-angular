import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';

import { CourseService } from 'shared/http-services/course.service';

@Injectable({
  providedIn: 'root',
})
export class CourseCeResolve implements Resolve<any> {
  constructor(private _router: Router, private courseSv: CourseService) {}

  resolve(activeRoute: ActivatedRouteSnapshot) {
    if (activeRoute.params.id && isNaN(Number(activeRoute.params.id))) {
      return this._router.navigate(['/setting/courses']);
    }
    return this.courseSv.fetchCourseById(activeRoute.params.id, {
      with: 'items,buffets',
    });
  }
}
