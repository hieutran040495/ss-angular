import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { UiSwitchModule } from 'shared/modules/ui-switch/index';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';
import { ValidatorService } from 'shared/utils/validator.service';
import { CourseCeComponent } from './course-ce.component';

import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { CasMaxlengthModule } from 'shared/directive/cas-maxlength/cas-maxlength.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';
import { UnFocusModule } from 'shared/directive/un-focus/us-focus.module';
import { DigitFormaterModule } from 'shared/modules/digit-formater/digit-formater.module';

import { CourseRemoveModule } from './course-remove/course-remove.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { CasDropzoneModule } from 'app/modules/cas-dropzone/cas-dropzone.module';
import { CasOrderItemsModule } from 'app/modules/cas-order-items/cas-order-items.module';
import { SelectMenuItemModule } from 'shared/modules/select-menu-item/select-menu-item.module';

@NgModule({
  declarations: [CourseCeComponent],
  imports: [
    CommonModule,
    FormsModule,
    UiSwitchModule.forRoot({
      size: 'small',
      bgColor: '#c3ed12',
    }),
    OnlyNumericModule,
    TooltipModule.forRoot(),
    SpaceValidationModule,
    CasMaxlengthModule,
    CasNgSelectModule,
    UnFocusModule,
    DigitFormaterModule,
    CasDialogModule,
    CasDropzoneModule,
    CasOrderItemsModule,
    SelectMenuItemModule,
    CourseRemoveModule,
  ],
  providers: [ValidatorService],
  entryComponents: [CourseCeComponent],
  exports: [CourseCeComponent],
})
export class CourseCeModule {}
