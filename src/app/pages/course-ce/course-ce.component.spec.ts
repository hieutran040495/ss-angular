import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseCeComponent } from './course-ce.component';

describe('CourseCeComponent', () => {
  let component: CourseCeComponent;
  let fixture: ComponentFixture<CourseCeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CourseCeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseCeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
