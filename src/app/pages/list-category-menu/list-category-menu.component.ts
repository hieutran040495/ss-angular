import { Component, OnInit, Input, EventEmitter, OnDestroy, AfterViewInit } from '@angular/core';
import { Pagination } from 'shared/models/pagination';
import { RichMenuItem } from 'shared/models/rich-menu-item';
import { SETTING_RICH_MENU, SELF_ORDER } from 'shared/enums/event-emitter';

import { RichMenuService } from 'shared/http-services/rich-menu.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ItemCategory } from 'shared/models/item-category';

@Component({
  selector: 'app-list-category-menu',
  templateUrl: './list-category-menu.component.html',
  styleUrls: ['./list-category-menu.component.scss'],
})
export class ListCategoryMenuComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input('onEventEmitter') onEventEmitter: EventEmitter<any>;

  private _itemCategory: ItemCategory;
  @Input('itemCategory')
  get itemCategory(): ItemCategory {
    return this._itemCategory;
  }
  set itemCategory(v: ItemCategory) {
    this._itemCategory = v;
  }

  @Input('isOrder') isOrder: Boolean = false;
  isLoading: boolean = false;
  isPaginate: boolean = false;

  pagination: Pagination = new Pagination();
  richMenuItems: RichMenuItem[] = [];
  selfOrderItem: RichMenuItem;

  private _dragNode: HTMLElement;
  private pageSub;

  constructor(private richMenuSv: RichMenuService, private toastSv: ToastService) {}

  ngOnInit() {
    this.getRichMenuItems(true);
  }

  ngOnDestroy(): void {
    this.pageSub.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.pageSub = this.onEventEmitter.subscribe((res: any) => {
      if (!res.type) {
        return false;
      }

      switch (res.type) {
        case SETTING_RICH_MENU.REMOVE_MENU_ITEM:
          this.richMenuItems.unshift(res.data);
          break;
        case SELF_ORDER.REMOVE_ITEM:
          this.selfOrderItem = new RichMenuItem().deserialize(res.data);
          break;
        case SELF_ORDER.REMOVE_ITEM_DEVICE:
          this.richMenuItems.push(new RichMenuItem().deserialize(res.data));
          break;
        case SELF_ORDER.CHANGE_PATTERN:
          if (res.data.itemCategory) {
            this.itemCategory = res.data.itemCategory;
            this.pagination.reset();
            this.getRichMenuItems(true);
          }
          break;
        case SELF_ORDER.REMOVE_LIST_ITEM:
          if (!res.data || !res.data.length) {
            return;
          }
          this.richMenuItems = this.richMenuItems.concat(res.data);
          break;
      }
    });
  }

  getRichMenuItems(isFirst: boolean = false) {
    if (this.isLoading) return;

    if (isFirst) {
      this.richMenuItems = [];
    }

    const opts = {
      rich_menu_setting_id_not_in: this.itemCategory.richMenuIdToJson(),
      category_id: this.itemCategory.id,
      is_show: 1,
      only_course: 0,
      order: 'ordinal_number',
      ...this.pagination.hasJSON(),
    };

    this.isLoading = true;
    this.richMenuSv.getRichMenuItems(opts).subscribe(
      (res: any) => {
        this.richMenuItems = this.richMenuItems.concat(res.data);
        this.pagination.deserialize(res);
        this.isLoading = false;
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  onStart(event: HTMLElement, i: number) {
    const next = <HTMLElement>event.nextElementSibling;
    if (next && i < this.richMenuItems.length - 1) {
      next.style.marginTop = '100px';
    }

    this._dragNode = <HTMLElement>event.cloneNode(true);

    this._dragNode.style.opacity = '0.5';

    if (i < this.richMenuItems.length - 1) {
      this._dragNode.style.marginTop = `-${(this.richMenuItems.length - i) * 90}px`;
    } else {
      this._dragNode.style.marginTop = `-${(this.richMenuItems.length - i - 1) * 90}px`;
    }

    event.classList.add('item--drag');
    event.parentElement.appendChild(this._dragNode);

    let style: HTMLElement = document.querySelector('style[name=item-rich-menu]');
    if (!style) {
      style = document.createElement('style');
      style.setAttribute('type', 'text/css');
      style.setAttribute('name', 'item-rich-menu');
      document.head.appendChild(style);
    }

    style.innerHTML = `.item--${i} { top: ${event.offsetTop - event.parentElement.scrollTop}px; }`;
    event.classList.add(`item--${i}`);
  }

  onStop(event: HTMLElement, menu: RichMenuItem, i: number) {
    const next = <HTMLElement>event.nextElementSibling;
    if (next) {
      next.style.marginTop = '';
    }

    const container = <DOMRect>document.getElementById('CategoryLayout').getBoundingClientRect();
    const target = <DOMRect>event.getBoundingClientRect();

    event.classList.remove('item--drag');
    event.parentElement.removeChild(this._dragNode);
    this._dragNode = null;

    event.classList.remove(`item--${i}`);

    if (
      target.x >= container.x &&
      target.x <= container.x + container.width &&
      target.y >= container.y &&
      target.y <= container.y + container.height
    ) {
      // hanlde add event
      menu.pos_x = target.x - container.x;
      menu.pos_y = target.y - container.y;

      this.onEventEmitter.next({
        type: SETTING_RICH_MENU.CHANGE_RICH_MENU,
        data: menu,
      });

      this.richMenuItems.splice(i, 1);
    }
  }

  scrollEnd(event: any) {
    const pos = event.target.scrollTop;
    const max = event.target.scrollHeight - event.target.offsetHeight;

    if (pos >= max && this.pagination.hasNextPage()) {
      this.pagination.nextPage();
      this.getRichMenuItems();
    }
  }

  allowDrop(ev) {
    ev.preventDefault();
  }

  drop(ev) {
    ev.preventDefault();
    const type = ev.dataTransfer.getData('type');
    if (type === SELF_ORDER.REMOVE_ITEM) {
      this.richMenuItems.push(new RichMenuItem().deserialize(this.selfOrderItem));
      this.onEventEmitter.next({
        type: SELF_ORDER.REMOVE_ITEM_SUCCESS,
        data: this.selfOrderItem,
      });
    }
  }
}
