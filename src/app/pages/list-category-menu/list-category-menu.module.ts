import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ListCategoryMenuComponent } from './list-category-menu.component';
import { ValidatorService } from 'shared/utils/validator.service';
import { AngularDraggableModule } from 'angular2-draggable';

@NgModule({
  imports: [CommonModule, FormsModule, AngularDraggableModule],
  declarations: [ListCategoryMenuComponent],
  exports: [ListCategoryMenuComponent],
  providers: [ValidatorService],
})
export class ListCategoryMenuModule {}
