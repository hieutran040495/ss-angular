import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCategoryMenuComponent } from './list-category-menu.component';

describe('ListCategoryMenuComponent', () => {
  let component: ListCategoryMenuComponent;
  let fixture: ComponentFixture<ListCategoryMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListCategoryMenuComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCategoryMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
