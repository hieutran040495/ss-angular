import { Component, OnInit } from '@angular/core';
import { PROFIT_DATETIME } from 'shared/enums/profit';

@Component({
  selector: 'app-profit-week',
  templateUrl: './profit-week.component.html',
  styleUrls: ['./profit-week.component.scss'],
})
export class ProfitWeekComponent implements OnInit {
  profitType: string = PROFIT_DATETIME.WEEKS;

  constructor() {}

  ngOnInit() {}
}
