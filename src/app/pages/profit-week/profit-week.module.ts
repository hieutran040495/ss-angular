import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ProfitWeekComponent } from './profit-week.component';

import { ProfitChartsModule } from 'app/pages/profit-charts/profit-charts.module';

const routes: Routes = [
  {
    path: '',
    component: ProfitWeekComponent,
  },
];
@NgModule({
  declarations: [ProfitWeekComponent],
  imports: [CommonModule, FormsModule, RouterModule.forChild(routes), ProfitChartsModule],
})
export class ProfitWeekModule {}
