import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitWeekComponent } from './profit-week.component';

describe('ProfitWeekComponent', () => {
  let component: ProfitWeekComponent;
  let fixture: ComponentFixture<ProfitWeekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfitWeekComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitWeekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
