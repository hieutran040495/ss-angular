import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalIncomingCallComponent } from './modal-incoming-call.component';

describe('ModalIncomingCallComponent', () => {
  let component: ModalIncomingCallComponent;
  let fixture: ComponentFixture<ModalIncomingCallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalIncomingCallComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalIncomingCallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
