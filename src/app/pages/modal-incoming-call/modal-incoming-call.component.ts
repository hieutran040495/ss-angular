import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { ClientUser } from 'shared/models/client-user';

import { RESV_MODE_EMITTER } from 'shared/enums/event-emitter';

import { ToastService } from 'shared/logical-services/toast.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { Reservation } from 'shared/models/reservation';
import { MissedCallService } from 'shared/http-services/missed-call.service';
import { ReservationService } from 'shared/http-services/reservation.service';

@Component({
  selector: 'app-modal-incoming-call',
  templateUrl: './modal-incoming-call.component.html',
  styleUrls: ['./modal-incoming-call.component.scss'],
})
export class ModalIncomingCallComponent implements OnInit {
  clientUser: ClientUser;
  upcomingResv: Reservation | null;

  isSolving: boolean = false;

  constructor(
    private bsModalRef: BsModalRef,
    private bsModalService: BsModalService,
    private _missedCallService: MissedCallService,
    private toast: ToastService,
    private eventEmitter: EventEmitterService,
    private resvService: ReservationService,
  ) {}

  ngOnInit() {}

  createOrUpdateResv() {
    this._solvedMissedCall(this.clientUser.phone.toString(), true);

    if (!this.clientUser || !this.upcomingResv) {
      this._createResv();
      return;
    }

    this._updateResv();
  }

  private _solvedMissedCall(phone: string, solved: boolean) {
    if (this.isSolving) {
      return;
    }

    this.isSolving = true;
    const data = {
      solved,
    };
    this._missedCallService.solvedMissedCall(phone, data).subscribe(
      (res) => {
        this.isSolving = false;
        this.closeModal();
      },
      (errors) => {
        this.toast.error(errors);
        this.isSolving = false;
      },
    );
  }

  private _createResv() {
    this.eventEmitter.publishData({
      type: RESV_MODE_EMITTER.CREATE,
      data: {
        client_user: this.clientUser,
      },
    });
  }

  private _updateResv() {
    const opts = {
      with: 'coupon,clientUser,tables,user',
    };
    this.resvService.getResvById(this.upcomingResv.id, opts).subscribe(
      (res) => {
        this.upcomingResv = new Reservation().deserialize(res);
        this.eventEmitter.publishData({
          type: RESV_MODE_EMITTER.EDIT,
          data: this.upcomingResv,
        });
      },
      (errors) => {
        this.toast.error(errors);
      },
    );
  }

  closeModal(reason?: string) {
    this.bsModalService.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
