import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalIncomingCallComponent } from './modal-incoming-call.component';

@NgModule({
  declarations: [ModalIncomingCallComponent],
  imports: [CommonModule, FormsModule],
  entryComponents: [ModalIncomingCallComponent],
  exports: [ModalIncomingCallComponent],
})
export class ModalIncomingCallModule {}
