import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { DIALOG_EVENT } from 'shared/enums/modes';
import { Receipt } from 'shared/models/receipt';
import { RESV_PAYMENT_BY } from 'shared/enums/reservation';

@Component({
  selector: 'app-modal-undo-payment',
  templateUrl: './modal-undo-payment.component.html',
  styleUrls: ['./modal-undo-payment.component.scss'],
})
export class ModalUndoPaymentComponent implements OnInit {
  receipt: Receipt = new Receipt();
  RESERVATION_PAYMENT_UNIT = RESV_PAYMENT_BY;
  isSubmiting: boolean;

  private paymentUnit: string[] = ['square_cash', 'square_card'];

  constructor(
    private bsModalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private resvSv: ReservationService,
    private toastSv: ToastService,
  ) {}

  get titleDialog() {
    return this.paymentUnit.indexOf(this.receipt.payment_by) !== -1 ? '払い戻し' : '会計復旧';
  }

  get contentDialog() {
    return this.paymentUnit.indexOf(this.receipt.payment_by) !== -1
      ? '払い戻しを行います。よろしいですか？'
      : '会計ステータスを会計前に戻しますか？';
  }

  get nameBtn() {
    return this.paymentUnit.indexOf(this.receipt.payment_by) !== -1 ? '実行する' : '適用する';
  }
  ngOnInit() {}

  closeDialog(reason?: string) {
    this.bsModalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }

  undoPayment() {
    if (this.isSubmiting) {
      return;
    }
    this.isSubmiting = true;

    this.resvSv.undoPaymentResv(this.receipt.id).subscribe(
      (res) => {
        this.isSubmiting = false;
        this.toastSv.success('会計前の状態に更新しました');
        this.closeDialog(DIALOG_EVENT.RECEIPT_UNDO_PAYMENT_SUCCESS);
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isSubmiting = false;
      },
    );
  }
}
