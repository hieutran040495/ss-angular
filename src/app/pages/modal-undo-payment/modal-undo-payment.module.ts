import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { ModalModule } from 'ngx-bootstrap/modal';

import { ModalUndoPaymentComponent } from './modal-undo-payment.component';

@NgModule({
  declarations: [ModalUndoPaymentComponent],
  imports: [CommonModule, CasDialogModule, ModalModule.forRoot()],
  exports: [ModalUndoPaymentComponent],
  entryComponents: [ModalUndoPaymentComponent],
})
export class ModalUndoPaymentModule {}
