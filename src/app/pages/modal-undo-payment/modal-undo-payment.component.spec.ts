import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalUndoPaymentComponent } from './modal-undo-payment.component';

describe('ModalUndoPaymentComponent', () => {
  let component: ModalUndoPaymentComponent;
  let fixture: ComponentFixture<ModalUndoPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalUndoPaymentComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalUndoPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
