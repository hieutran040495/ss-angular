import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { SELF_ORDER } from 'shared/enums/event-emitter';
@Component({
  selector: 'app-modal-confirm-remove-page',
  templateUrl: './modal-confirm-remove-page.component.html',
  styleUrls: ['./modal-confirm-remove-page.component.scss'],
})
export class ModalConfirmRemovePageComponent implements OnInit {
  SELF_ORDER = SELF_ORDER;

  constructor(private bsModalRef: BsModalRef, private modalSv: BsModalService) {}
  ngOnInit() {}

  closeDialog(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
