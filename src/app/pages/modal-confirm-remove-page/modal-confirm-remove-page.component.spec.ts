import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfirmRemovePageComponent } from './modal-confirm-remove-page.component';

describe('ModalConfirmRemovePageComponent', () => {
  let component: ModalConfirmRemovePageComponent;
  let fixture: ComponentFixture<ModalConfirmRemovePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalConfirmRemovePageComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfirmRemovePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
