import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalConfirmRemovePageComponent } from './modal-confirm-remove-page.component';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [ModalConfirmRemovePageComponent],
  imports: [CommonModule, CasDialogModule, ModalModule.forRoot()],
  exports: [ModalConfirmRemovePageComponent],
  entryComponents: [ModalConfirmRemovePageComponent],
})
export class ModalConfirmRemovePageModule {}
