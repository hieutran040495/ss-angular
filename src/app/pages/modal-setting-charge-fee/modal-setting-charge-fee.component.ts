import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ClientService } from 'shared/http-services/client.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ValidatorService } from 'shared/utils/validator.service';

import { chargeFeeTypes, serviceFeeBys } from 'shared/constants/charge-fee-setting';
import { Client } from 'shared/models/client';
import { SERVICE_FEE_TYPE, SERVICE_FEE_BY } from 'shared/enums/service-fee-setting';
import { Regexs } from 'shared/constants/regex';

@Component({
  selector: 'app-modal-setting-charge-fee',
  templateUrl: './modal-setting-charge-fee.component.html',
  styleUrls: ['./modal-setting-charge-fee.component.scss'],
})
export class ModalSettingChargeFeeComponent implements OnInit {
  isLoading: boolean = false;

  chargeFeeTypes = chargeFeeTypes;
  serviceFeeBys = serviceFeeBys;
  clientChargeFee: Client = new Client();
  fee_amount: number;
  fee_fercent: number;

  rules = Regexs;
  validator: any = {};

  constructor(
    private clientSv: ClientService,
    private toastSv: ToastService,
    private validatorSv: ValidatorService,
    private modalSv: BsModalService,
    private bsModalRef: BsModalRef,
    private _clientSv: ClientService,
  ) {}

  ngOnInit() {
    this._initInforServiceFee();
  }

  private _initInforServiceFee() {
    const opts: any = {
      with: 'images,tags,genre,client_workings,clientAdmin',
    };

    this._clientSv.fetchClient(opts).subscribe(
      (res) => {
        this.clientChargeFee = res;
        if (!this.clientChargeFee.service_fee_type) {
          this.clientChargeFee.service_fee_type = SERVICE_FEE_TYPE.AMOUNT;
        }
        if (this.clientChargeFee.isPercent) {
          this.fee_fercent = this.clientChargeFee.service_fee;
        }
        if (this.clientChargeFee.isAmount) {
          this.fee_amount = this.clientChargeFee.service_fee;
        }
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  settingChargeFee() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    this.clientChargeFee.service_fee = this.clientChargeFee.isAmount ? this.fee_amount : this.fee_fercent;

    this.clientSv.settingChangeFee(this.clientChargeFee.fromDateSettingFee()).subscribe(
      (res) => {
        this.toastSv.success('席料を設定しました');
        this.closeModal();
        this.isLoading = false;
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.validator = this.validatorSv.setErrors(errors.errors);
        }
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  changeFeeType() {
    switch (this.clientChargeFee.service_fee_type) {
      case SERVICE_FEE_TYPE.AMOUNT: {
        this.serviceFeeBys = serviceFeeBys;
        this.clientChargeFee.service_fee_by = SERVICE_FEE_BY.QUANTITY;
        break;
      }
      case SERVICE_FEE_TYPE.PERCENT: {
        this.serviceFeeBys = serviceFeeBys.filter((item) => item.value === SERVICE_FEE_BY.RESERVATION);
        this.clientChargeFee.service_fee_by = SERVICE_FEE_BY.RESERVATION;
        break;
      }
      default:
        this.clientChargeFee.service_fee_by = SERVICE_FEE_BY.QUANTITY;
    }
  }

  resetValidation(key: string): void {
    this.validator[key] = undefined;
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
