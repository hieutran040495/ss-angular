import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { UiSwitchModule } from 'shared/modules/ui-switch/index';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';

import { ValidatorService } from 'shared/utils/validator.service';
import { ModalSettingChargeFeeComponent } from './modal-setting-charge-fee.component';
import { UnFocusModule } from 'shared/directive/un-focus/us-focus.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';
import { DigitFormaterModule } from 'shared/modules/digit-formater/digit-formater.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    UiSwitchModule.forRoot({
      size: 'small',
      bgColor: '#c3ed12',
    }),
    OnlyNumericModule,
    UnFocusModule,
    CasNgSelectModule,
    DigitFormaterModule,
    CasDialogModule,
  ],
  declarations: [ModalSettingChargeFeeComponent],
  entryComponents: [ModalSettingChargeFeeComponent],
  exports: [ModalSettingChargeFeeComponent],
  providers: [ValidatorService],
})
export class ModalSettingChargeFeeModule {}
