import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSettingChargeFeeComponent } from './modal-setting-charge-fee.component';

describe('ModalSettingChargeFeeComponent', () => {
  let component: ModalSettingChargeFeeComponent;
  let fixture: ComponentFixture<ModalSettingChargeFeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalSettingChargeFeeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSettingChargeFeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
