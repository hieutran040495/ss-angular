import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UploadFileComponent } from './upload-file.component';

import { FileValueAccessorDirective } from './filevalueaccessor.directive';
import { FileValidator } from './file-input.validator';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [UploadFileComponent, FileValueAccessorDirective, FileValidator],
  exports: [UploadFileComponent, FileValueAccessorDirective, FileValidator],
})
export class UploadFileModule {}
