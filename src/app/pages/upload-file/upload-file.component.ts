import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { FileUpload } from 'shared/interfaces/file-upload';
import { NgModel } from '@angular/forms';

export const IMAGE_MIME = ['image/png', 'image/jpeg', 'image/gif'];

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UploadFileComponent implements OnInit {
  @ViewChild('uploadFile') uploadFile: ElementRef;
  @ViewChild('menuThumbnail') fileModel: NgModel;

  @Input('maxSize')
  maxSize: number;

  @Input('mimeTypes')
  mimeTypes: Array<string>;

  @Input('thumbnail')
  thumbnail: string;

  @Input('errorMessage')
  errorMessage: string;

  @Input('validator')
  validator: string;

  @Input('disabled')
  disabled: boolean;

  @Input('multiple')
  multiple: boolean = false;

  private _resetUpload: boolean;
  @Input('resetUpload')
  set resetUpload(v: boolean) {
    this._resetUpload = v;
  }
  get resetUpload(): boolean {
    return this._resetUpload;
  }

  @Output()
  selectFile: EventEmitter<FileUpload> = new EventEmitter<FileUpload>();

  @Output()
  errorFile: EventEmitter<FileUpload> = new EventEmitter<FileUpload>();

  file: File | string | ArrayBuffer;
  get showFile(): File | string | ArrayBuffer {
    // remove file when upload mutiple images
    if (this.resetUpload && this.file) {
      this.removeFile();
      return;
    }

    return this.thumbnail || this.file;
  }
  fileName: string;
  menuFile: File[] = [];

  private validMimeTypes;
  private maxSizeValid;

  get accept_type(): string {
    if (this.mimeTypes) {
      return this.mimeTypes.toString();
    }
    return IMAGE_MIME.toString();
  }

  constructor() {}

  ngOnInit() {
    this.validMimeTypes = this.mimeTypes || IMAGE_MIME;
    this.maxSizeValid = this.maxSize || 3145728;
  }

  getFile(event) {
    const files = event.target.files;

    if (files && files.length > 0) {
      for (const file of files) {
        this.validationFile(file);
      }
      this.menuFile.push(files[0]);
    } else {
      this.fileModel.errors.required = false;
      this.validationFile(this.menuFile[this.menuFile.length - 1]);
    }
  }

  private validationFile(file: File) {
    if (file) {
      if (this.validMimeTypes.indexOf(file.type) === -1) {
        this.validator = '画像はjpg、png、gif、jpegのファイルでアップロードしてください';
      } else if (file.size === 0) {
        this.validator = '画像は1キロバイト以上でアップロードしてください';
      } else if (file.size > this.maxSizeValid) {
        this.validator = '画像のサイズは最大3MBまでアップロード可能です';
      } else {
        this.fileName = file.name;
        this._handleFile(file);
        return;
      }

      this.thumbnail = '';
      this.file = null;
      this.fileName = '';
      this.errorFile.emit({
        validation: true,
      });
    }
  }

  resetValidator() {
    this.validator = undefined;
  }

  private _handleFile(file: File) {
    const fileData: File = file;

    this.selectFile.emit({
      value: fileData,
    });
    this.errorFile.emit({
      validation: false,
    });
    const reader = new FileReader();
    reader.onload = () => {
      this.file = reader.result;
      this.thumbnail = '';
    };
    reader.readAsDataURL(fileData);
  }

  removeFile() {
    this.thumbnail = '';
    // Reset value input file when delete image, user can choose image again
    this.uploadFile.nativeElement.value = '';

    if (!this.file) {
      return;
    }

    if (!this.resetUpload) {
      this.file = '';
      this.fileName = '';
      this.selectFile.emit({
        value: this.file,
      });
      this.errorFile.emit({
        validation: true,
      });
    } else {
      this.fileModel.reset();
    }
  }
}
