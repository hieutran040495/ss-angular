import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';
import { ModalSettingDisplayComponent } from './modal-setting-display.component';

@NgModule({
  imports: [CommonModule, FormsModule, CasDialogModule],
  declarations: [ModalSettingDisplayComponent],
  entryComponents: [ModalSettingDisplayComponent],
  exports: [ModalSettingDisplayComponent],
})
export class ModalSettingDisplayModule {}
