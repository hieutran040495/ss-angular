import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSettingDisplayComponent } from './modal-setting-display.component';

describe('ModalSettingDisplayComponent', () => {
  let component: ModalSettingDisplayComponent;
  let fixture: ComponentFixture<ModalSettingDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalSettingDisplayComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSettingDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
