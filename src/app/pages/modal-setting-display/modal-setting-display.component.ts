import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { SETTING_DISPLAY } from 'shared/enums/event-emitter';
import { StyleOpts } from 'shared/interfaces/style-setting';
import { OPTIONS_SETTING_DISPLAY, STYLE_SETTING_DEFAULT } from 'shared/constants/style-config-default';

@Component({
  selector: 'app-modal-setting-display',
  templateUrl: './modal-setting-display.component.html',
  styleUrls: ['./modal-setting-display.component.scss'],
})
export class ModalSettingDisplayComponent implements OnInit {
  isLoading: boolean = false;

  settingDisplay = OPTIONS_SETTING_DISPLAY;
  settingOpts: StyleOpts;

  constructor(private emitSv: EventEmitterService, private modalSv: BsModalService, private bsModalRef: BsModalRef) {}

  ngOnInit() {
    // Default setting
    if (!this.settingOpts) {
      this.settingOpts = STYLE_SETTING_DEFAULT;
    }
  }

  onChangeTextColor(color: string) {
    this.settingOpts.text_color = color;
  }

  onChangeBackground(color: string) {
    this.settingOpts.bg_color = color;
  }

  onChangeFont(font: string) {
    this.settingOpts.font_family = font;
  }

  submitSetting() {
    this.closeModal();

    return this.emitSv.publishData({
      type: SETTING_DISPLAY.CHANGE_SETTING_DISPLAY,
      data: this.settingOpts,
    });
  }

  closeModal(reason?: string) {
    this.modalSv.setDismissReason(reason);
    this.bsModalRef.hide();
  }
}
