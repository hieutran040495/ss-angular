import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagesComponent } from './pages.component';
import { AuthGuard } from 'app/guards/auth.guard';

// Import Resolve
import { MenuBuffetCeResolve } from 'app/pages/menu-buffet-ce/menu-buffet-ce.resolve';
import { CourseCeResolve } from 'app/pages/course-ce/course-ce.resolve';
import { MenuCeResolve } from 'app/pages/menu-ce/menu-ce.resolve';
import { ManageAccountCeResolve } from 'app/pages/manage-account-ce/manage-account-ce.resolve';
import { ModalDownloadCsvClientModule } from './modal-download-csv-client/modal-download-csv-client.module';
import { PagesResolve } from './pages.resolve';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: PagesComponent,
    resolve: {
      pages: PagesResolve,
    },
    children: [
      {
        path: 'users',
        children: [
          {
            path: '',
            loadChildren: './user-list/user-list.module#UserListModule',
            data: {
              isHeaderSeparate: true,
              isDownloadCSV: true,
              pageTitle: '顧客管理',
            },
          },
        ],
      },
      {
        path: 'setting',
        loadChildren: './setting/setting.module#SettingModule',
      },
      {
        path: 'menu-buffet',
        children: [
          {
            path: 'create',
            loadChildren: './menu-buffet-ce/menu-buffet-ce.module#MenuBuffetCeModule',
            data: {
              isChild: true,
              pageTitle: '放題メニュー新規作成',
            },
          },
          {
            path: ':buffetId/edit',
            loadChildren: './menu-buffet-ce/menu-buffet-ce.module#MenuBuffetCeModule',
            data: {
              isChild: true,
              pageTitle: '放題メニュー編集',
            },
            resolve: {
              buffet: MenuBuffetCeResolve,
            },
          },
        ],
      },
      {
        path: 'menu',
        children: [
          {
            path: 'create',
            loadChildren: './menu-ce/menu-ce.module#MenuCeModule',
            data: {
              isChild: true,
              pageTitle: 'メニュー新規作成',
            },
          },
          {
            path: ':menuId/edit',
            loadChildren: './menu-ce/menu-ce.module#MenuCeModule',
            data: {
              isChild: true,
              pageTitle: 'メニュー編集',
            },
            resolve: {
              menu: MenuCeResolve,
            },
          },
        ],
      },
      {
        path: 'course',
        children: [
          {
            path: 'create',
            loadChildren: './course-ce/course-ce.module#CourseCeModule',
            data: {
              isChild: true,
              pageTitle: 'コース新規作成',
            },
          },
          {
            path: ':id/edit',
            loadChildren: './course-ce/course-ce.module#CourseCeModule',
            data: {
              isChild: true,
              pageTitle: 'コース編集',
            },
            resolve: {
              course: CourseCeResolve,
            },
          },
        ],
      },
      {
        path: 'account',
        children: [
          {
            path: 'create',
            loadChildren: './manage-account-ce/manage-account-ce.module#ManageAccountCeModule',
            data: {
              isChild: true,
              pageTitle: 'アカウント新規作成',
            },
          },
          {
            path: ':accountId/edit',
            loadChildren: './manage-account-ce/manage-account-ce.module#ManageAccountCeModule',
            data: {
              isChild: true,
              pageTitle: 'アカウント編集',
            },
            resolve: {
              account: ManageAccountCeResolve,
            },
          },
        ],
      },
      {
        path: 'reservations',
        children: [
          {
            path: '',
            loadChildren: './resv-list/resv-list.module#ResvListModule',
          },
          {
            path: ':id/email',
            loadChildren: './resv-email/resv-email.module#ResvEmailModule',
            data: {
              isChild: true,
              pageTitle: 'メール送信',
            },
          },
          // {
          //   path: 'history',
          //   loadChildren: 'app/views/history/resv-history/resv-history.module#ResvHistoryModule',
          //   data: {
          //     pageTitle: '予約履歴一覧',
          //   },
          // },
          {
            path: '**',
            redirectTo: '',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: 'history-call',
        children: [
          {
            path: '',
            loadChildren: './history-call/history-call.module#HistoryCallModule',
            data: {
              isHeaderSeparate: true,
              pageTitle: '着信履歴',
              subPageTitle: 'お客様詳細',
            },
          },
        ],
      },
      {
        path: 'management/tables',
        loadChildren: './table-management/table-management.module#TableManagementModule',
        data: {
          isChild: true,
          pageTitle: '座席管理',
        },
      },
      {
        path: 'tables/view',
        loadChildren: './tables-view/tables-view.module#TablesViewModule',
        data: {
          isChild: true,
          pageTitle: '座席管理',
        },
      },
      {
        path: 'resv-update-history',
        children: [
          {
            path: '',
            loadChildren: './resv-update-history/resv-update-history.module#ResvUpdateHistoryModule',
            data: {
              pageTitle: '予約更新履歴',
            },
          },
        ],
      },
      // {
      //   path: 'receipts',
      //   children: [
      //     {
      //       path: '',
      //       loadChildren: './receipt/receipt.module#ReceiptModule',
      //       data: {
      //         pageTitle: '会計一覧',
      //       },
      //     },
      //   ],
      // },
      {
        path: 'profit-analysis',
        loadChildren: './profit-analysis/profit-analysis.module#ProfitAnalysisModule',
      },
      {
        path: '**',
        redirectTo: 'reservations',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [ModalDownloadCsvClientModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [MenuCeResolve, MenuBuffetCeResolve, CourseCeResolve, ManageAccountCeResolve, PagesResolve],
})
export class PagesRoutingModule {}
