import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { ToastService } from 'shared/logical-services/toast.service';
import { Header } from 'shared/models/header';
import { ModalDownloadCsvClientComponent } from './modal-download-csv-client/modal-download-csv-client.component';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';
import { OneSignalService } from 'shared/logical-services/one-signal/one-signal.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { EventEmitterType } from 'shared/enums/event-emitter';
import { ReservationStateService } from 'shared/logical-services/reservation-state.service';
import { SessionService } from 'shared/states/session';

import 'rxjs-compat/add/operator/mergeMap';
import 'rxjs-compat/add/operator/filter';
import 'rxjs-compat/add/operator/map';
import { AuthService } from 'shared/http-services/auth.service';
import { ModalIncomingCallComponent } from './modal-incoming-call/modal-incoming-call.component';

import { PusherEvent } from 'shared/models/event';
import { NOTIFY_CODE } from 'shared/enums/code-notify';
import { PhoneLookupService } from 'shared/http-services/phone-lookup.service';
import { ClientUser } from 'shared/models/client-user';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss'],
  providers: [Location, { provide: LocationStrategy, useClass: PathLocationStrategy }],
})
export class PagesComponent implements OnInit, AfterViewInit {
  @ViewChild('CasAppRef') private casAppRef: ElementRef;

  header: Header = new Header();
  modalRef: BsModalRef;
  totalUnseen: number;
  pageSub;

  private clientUser: string;

  isLoading: boolean = false;

  constructor(
    private router: Router,
    private location: Location,
    private toastSv: ToastService,
    private activatedRoute: ActivatedRoute,
    private modalSv: BsModalService,
    private echoSv: LaravelEchoService,
    private oneSignalSv: OneSignalService,
    private eventEmitter: EventEmitterService,
    private resvStateSv: ReservationStateService,
    private sessionService: SessionService,
    private authService: AuthService,
    private phoneLookupService: PhoneLookupService,
  ) {
    this.getDataRouter();

    this.router.events
      .filter((e) => e instanceof NavigationEnd)
      .subscribe((e: NavigationEnd) => {
        const url = e.url;
        if (url.indexOf('/reservations/create') === -1) {
          this.resvStateSv.resetResvationStore();
        }
      });
  }

  ngOnInit() {
    this.pageSub = this.eventEmitter.caseNumber$.subscribe((res) => {
      if (res.type === EventEmitterType.SEARCH_CLIENT_USER) {
        this.clientUser = res.data;
      }
    });

    this.initOneSignal();
    this.onNotification();
  }

  ngAfterViewInit() {}

  private onNotification() {
    this.echoSv.onNotification((e: any) => {
      const event = new PusherEvent().deserialize(e);
      switch (event.code) {
        case NOTIFY_CODE.INCOMING_CALL:
          this.findUserIncomingCall(event.phone);
          break;
      }
    });
  }

  private findUserIncomingCall(phone: string) {
    const opts = {
      phone: phone,
    };

    this.phoneLookupService.getPhoneLookup(opts).subscribe(
      (res) => {
        const config = {
          class: 'modal-md modal-dialog-right',
          initialState: {
            clientUser: new ClientUser().deserialize(res.client_user),
          },
        };
        this.openModalWithComponent(ModalIncomingCallComponent, config);
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  private initOneSignal() {
    this.oneSignalSv.init();
  }

  toogleSideNav(): void {
    this.casAppRef.nativeElement.classList.toggle('cas-app--show-sidenav');
  }

  backPage() {
    this.location.back();
  }

  logout() {
    if (this.isLoading) return;

    this.isLoading = true;

    this.authService.logout().subscribe(
      (res) => {
        this.echoSv.leaveChannel();
        this.oneSignalSv.destroyOneSignal();
        sessionStorage.clear();
        localStorage.clear();
        this.sessionService.resetStore();
        this.toastSv.success('ログアウトしました');
        this.router.navigate(['/auth']);
        this.isLoading = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  private getDataRouter() {
    this.router.events
      .filter((e) => e instanceof NavigationEnd)
      .map(() => this.activatedRoute)
      .map(() => {
        let route = this.activatedRoute.firstChild;
        let child = route;
        while (child) {
          if (child.firstChild) {
            child = child.firstChild;
            route = child;
          } else {
            child = null;
          }
        }
        return route;
      })
      .mergeMap((route) => route.data)
      .subscribe((e) => {
        this.header.deserialize({
          isSetting: e.isSetting || false,
          isChild: e.isChild || false,
          pageTitle: e.pageTitle || undefined,
          subPageTitle: e.subPageTitle || undefined,
          isHeaderSeparate: e.isHeaderSeparate || undefined,
          isDownloadCSV: e.isDownloadCSV || undefined,
        });

        this.casAppRef.nativeElement.classList.remove('cas-app--show-sidenav');
        window.scrollTo(0, 0);
      });
  }

  downloadListUser() {
    const config: ModalOptions = {
      class: 'modal-dialog-centered',
      keyboard: false,
      ignoreBackdropClick: true,
      initialState: {
        clientUser: this.clientUser,
      },
    };
    this.openModalWithComponent(ModalDownloadCsvClientComponent, config);
  }

  private openModalWithComponent(comp, config?: ModalOptions) {
    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();
    });

    this.modalRef = this.modalSv.show(comp, config);
  }
}
