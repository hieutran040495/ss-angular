import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ResetPwData } from 'shared/interfaces/auth-pw-data';

import { AuthService } from 'shared/http-services/auth.service';
import { ToastService } from 'shared/logical-services/toast.service';

interface SetupPasswordValidation {
  password: string | undefined;
  password_confirmation: string | undefined;
}

@Component({
  selector: 'app-account-setting-password',
  templateUrl: './account-setting-password.component.html',
  styleUrls: ['./account-setting-password.component.scss'],
})
export class AccountSettingPasswordComponent implements OnInit {
  isLoading: boolean = false;

  pwdData: ResetPwData = {
    client_id: null,
    id: '',
    password: '',
    token: '',
    password_confirmation: '',
  };

  validation: SetupPasswordValidation = {
    password: undefined,
    password_confirmation: undefined,
  };

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authSv: AuthService,
    private toastSv: ToastService,
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((res: any) => {
      if (!res.token || !res.id) {
        this.router.navigate(['/auth/login']);
        return;
      }
      this.pwdData.client_id = res.client_id;
      this.pwdData.id = res.id;
      this.pwdData.token = res.token;
    });
  }

  resetValidation(name: string): void {
    this.validation[name] = undefined;
  }

  resetPassword() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.authSv.resetPassword(this.pwdData).subscribe(
      (res) => {
        this.toastSv.success('パスワードの登録が完了しました');
        this.isLoading = false;
        this.router.navigate(['/auth/login']);
      },
      (errors) => {
        this.isLoading = false;
        this.toastSv.error(errors);
      },
    );
  }
}
