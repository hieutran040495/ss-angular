import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { EqualValidatorModule } from 'shared/directive/equal-validator/equal-validator.module';
import { PasswordValidatorModule } from 'shared/directive/password-validator/password-validator.module';

import { AccountSettingPasswordComponent } from './account-setting-password.component';

const routes: Routes = [
  {
    path: '',
    component: AccountSettingPasswordComponent,
  },
];

@NgModule({
  declarations: [AccountSettingPasswordComponent],
  imports: [CommonModule, FormsModule, RouterModule.forChild(routes), EqualValidatorModule, PasswordValidatorModule],
})
export class AccountSettingPasswordModule {}
