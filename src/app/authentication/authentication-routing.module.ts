import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationComponent } from './authentication.component';
import { NotAuthGuard } from 'app/guards/not-auth.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [NotAuthGuard],
    canActivateChild: [NotAuthGuard],
    component: AuthenticationComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
      },
      {
        path: 'login',
        loadChildren: './auth-login/auth-login.module#AuthLoginModule',
      },
      {
        path: 'password',
        children: [
          {
            path: 'forgot',
            loadChildren: './forgot-password/forgot-password.module#ForgotPasswordModule',
            data: {
              appScreen: 'C-1',
            },
          },
          {
            path: 'reset',
            loadChildren: './reset-password/reset-password.module#ResetPasswordModule',
            data: {
              appScreen: 'C-1-2',
            },
          },
        ],
      },
      {
        path: 'account/set-password',
        loadChildren: './account-setting-password/account-setting-password.module#AccountSettingPasswordModule',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthenticationRoutingModule {}
