import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { EmailValidatorModule } from 'shared/directive/email-validator/email-validator.module';

import { AuthLoginComponent } from './auth-login.component';
import { AuthService } from 'shared/http-services/auth.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { SessionService } from 'shared/states/session';
import { ToastServiceMock } from 'testing/services/toast.service.mock';
import { MockRouter } from 'testing/router.mock';

import { of, throwError } from 'rxjs';

fdescribe('Describe AuthLoginComponent', () => {
  let component: AuthLoginComponent;
  let fixture: ComponentFixture<AuthLoginComponent>;
  let toastSv: ToastService;
  let router: Router;
  let loginEl: HTMLElement;

  const authSv = jasmine.createSpyObj('AuthService', ['login']);
  const sessionSv = jasmine.createSpyObj('SessionService', ['fetchLocalStorageSession', 'updateToken']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, CommonModule, EmailValidatorModule],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [AuthLoginComponent],
      providers: [
        { provide: AuthService, useValue: authSv },
        { provide: ToastService, useClass: ToastServiceMock },
        { provide: SessionService, useValue: sessionSv },
        { provide: Router, useClass: MockRouter },
        { provide: ActivatedRoute, useValue: { queryParams: of({ redirect: '/' }) } },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthLoginComponent);
    component = fixture.componentInstance;

    component.loginData = {
      email: 'taistore555@yopmail.com',
      password: 'Abcd@1234',
    };

    toastSv = TestBed.get(ToastService);
    router = TestBed.get(Router);
    sessionSv.fetchLocalStorageSession.and.returnValue(of({ sussess: true }));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call fetchLocalStorageSession', () => {
    expect(sessionSv.fetchLocalStorageSession.calls.any()).toBe(true, 'call fetchLocalStorageSession');
  });

  describe('Login View', () => {
    beforeEach(() => {
      loginEl = fixture.nativeElement.querySelector('div.cas-auth__form');
    });
    it('should be have buton login サインイン ', () => {
      expect(loginEl.querySelector('button.cas-auth__btn').textContent).toContain('サインイン');
    });

    it('should be have link forgot password パスワードを忘れた方はこちら ', () => {
      expect(loginEl.querySelector('a.cas-auth__link').textContent).toContain('パスワードを忘れた方はこちら');
    });
  });

  describe('Login sucssess', () => {
    const loginSpy = authSv.login.and.returnValue(of({ sussess: true }));
    const sessionSpy = sessionSv.updateToken.and.returnValue(of({ sussess: true }));
    beforeEach(() => {
      component.login();
    });

    it('should call function Login service', () => {
      expect(loginSpy.calls.any()).toBe(true, 'call login service');
    });

    it('should call notify success', () => {
      expect(toastSv.success).toHaveBeenCalled();
    });

    it('should call update session service', () => {
      expect(sessionSpy.calls.any()).toBe(true, 'call updateToken');
    });

    it('should be navigate home page', () => {
      expect(router.navigate).toHaveBeenCalled();
    });
  });

  describe('Login Error', () => {
    beforeEach(() => {
      authSv.login.and.returnValue(throwError('Login fail'));
      const btnLogin = fixture.nativeElement.querySelector('button.cas-auth__btn');
      btnLogin.click();
    });

    it('should call notify error', () => {
      expect(toastSv.error).toHaveBeenCalled();
    });
  });
});
