import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SessionService } from 'shared/states/session';

import { LoginData } from 'shared/interfaces/login-data';
import { AuthService } from 'shared/http-services/auth.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { LOCALSTORAGE_KEY } from 'shared/enums/local-storage';

@Component({
  selector: 'app-auth-login',
  templateUrl: './auth-login.component.html',
  styleUrls: ['./auth-login.component.scss'],
})
export class AuthLoginComponent implements OnInit, AfterViewInit {
  loginData: LoginData = {
    login_id: '',
    email: '',
    password: '',
  };

  isLoading: boolean = false;
  isAutofill: boolean = true;
  private _redirect: string = '/';

  constructor(
    private sessionService: SessionService,
    private router: Router,
    private authService: AuthService,
    private toast: ToastService,
  ) {}

  ngOnInit() {}

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.isAutofill = false;
    }, 800);
  }

  login(): void {
    this.isLoading = true;

    this.authService.login(this.loginData).subscribe(
      (res) => {
        this.sessionService.updateToken(res.access_token);
        localStorage.setItem(LOCALSTORAGE_KEY.USE_IN_MAINTENANCE, res.use_in_maintenance);
        this.toast.success('ログインしました');
        this.router.navigate([this._redirect]);
      },
      (errors) => {
        this.isLoading = false;
        this.toast.error(errors);
      },
    );
  }
}
