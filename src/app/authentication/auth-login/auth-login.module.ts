import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AuthLoginComponent } from './auth-login.component';
import { EmailValidatorModule } from 'shared/directive/email-validator/email-validator.module';

const routes: Routes = [
  {
    path: '',
    component: AuthLoginComponent,
  },
];

@NgModule({
  declarations: [AuthLoginComponent],
  imports: [CommonModule, FormsModule, RouterModule.forChild(routes), EmailValidatorModule],
})
export class AuthLoginModule {}
