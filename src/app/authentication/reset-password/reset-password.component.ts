import { Component, OnInit } from '@angular/core';
import { ResetPwData } from 'shared/interfaces/auth-pw-data';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'shared/http-services/auth.service';
import { ToastService } from 'shared/logical-services/toast.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {
  isLoading: boolean = false;
  isCompleted: boolean = false;

  resetPwData: ResetPwData = {
    client_id: null,
    id: '',
    password: '',
    password_confirmation: '',
    token: '',
  };

  constructor(
    private activeRouter: ActivatedRoute,
    private authSv: AuthService,
    private toastSv: ToastService,
    private router: Router,
  ) {
    this.activeRouter.queryParams.subscribe((res) => {
      if (res.token && res.id) {
        this.resetPwData.client_id = res.client_id;
        this.resetPwData.id = res.id;
        this.resetPwData.token = res.token;
        return this.router.navigate(['/auth/password/reset'], { replaceUrl: true });
      }
    });
  }

  ngOnInit() {}

  resetPassword() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.authSv.resetPassword(this.resetPwData).subscribe(
      (res) => {
        this.isCompleted = true;
        this.isLoading = false;
      },
      (errors) => {
        this.isLoading = false;
        this.toastSv.error(errors);
      },
    );
  }
}
