import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { EqualValidatorModule } from 'shared/directive/equal-validator/equal-validator.module';
import { PasswordValidatorModule } from 'shared/directive/password-validator/password-validator.module';
import { ResetPasswordComponent } from './reset-password.component';

const routes: Routes = [
  {
    path: '',
    component: ResetPasswordComponent,
  },
];

@NgModule({
  declarations: [ResetPasswordComponent],
  imports: [CommonModule, FormsModule, RouterModule.forChild(routes), EqualValidatorModule, PasswordValidatorModule],
})
export class ResetPasswordModule {}
