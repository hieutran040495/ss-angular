import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ForgotPwData } from 'shared/interfaces/auth-pw-data';
import { AuthService } from 'shared/http-services/auth.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ValidatorService } from 'shared/utils/validator.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit, AfterViewInit {
  isLoading: boolean = false;
  isCompleted: boolean = false;

  validation: any = {};
  forgotPwData: ForgotPwData = {
    login_id: null,
    email: null,
  };

  get title_page(): string {
    if (this.isCompleted) {
      return 'パスワード再発行メール送信完了';
    }
    return 'パスワードを忘れた方はこちら';
  }

  isAutofill: boolean = true;

  constructor(private authSv: AuthService, private toastSv: ToastService, private validatorSv: ValidatorService) {}

  ngOnInit() {}

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.isAutofill = false;
    }, 800);
  }

  forgotPassword() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;

    this.authSv.forgotPassword(this.forgotPwData).subscribe(
      (res) => {
        this.isCompleted = true;
        this.isLoading = false;
      },
      (errors) => {
        this.isLoading = false;
        this.toastSv.error(errors);
        if (errors.hasOwnProperty('errors')) {
          this.validation = this.validatorSv.setErrors(errors.errors);
        }
      },
    );
  }

  resetValidation(key: string): void {
    this.validation[key] = undefined;
  }
}
