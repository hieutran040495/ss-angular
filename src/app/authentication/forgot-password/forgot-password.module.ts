import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { EmailValidatorModule } from 'shared/directive/email-validator/email-validator.module';

import { ForgotPasswordComponent } from './forgot-password.component';
import { ValidatorService } from 'shared/utils/validator.service';

const routes: Routes = [
  {
    path: '',
    component: ForgotPasswordComponent,
  },
];

@NgModule({
  declarations: [ForgotPasswordComponent],
  imports: [CommonModule, FormsModule, RouterModule.forChild(routes), EmailValidatorModule],
  providers: [ValidatorService],
})
export class ForgotPasswordModule {}
