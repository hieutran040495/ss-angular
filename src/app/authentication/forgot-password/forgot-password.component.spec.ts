import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ForgotPasswordComponent } from './forgot-password.component';

import { ValidatorService } from 'shared/utils/validator.service';
import { AuthService } from 'shared/http-services/auth.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { EmailValidatorModule } from 'shared/directive/email-validator/email-validator.module';

import { ToastServiceMock } from 'testing/services/toast.service.mock';

import { of, throwError } from 'rxjs';

fdescribe('Describe ForgotPasswordComponent', () => {
  let component: ForgotPasswordComponent;
  let fixture: ComponentFixture<ForgotPasswordComponent>;
  let toastSv: ToastService;
  let el: HTMLElement;

  const authSv = jasmine.createSpyObj('AuthService', ['forgotPassword']);
  const validatorSv = jasmine.createSpyObj('ValidatorService', ['setErrors']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, CommonModule, EmailValidatorModule],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [ForgotPasswordComponent],
      providers: [
        { provide: AuthService, useValue: authSv },
        { provide: ToastService, useClass: ToastServiceMock },
        { provide: ValidatorService, useValue: validatorSv },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgotPasswordComponent);
    component = fixture.componentInstance;
    toastSv = TestBed.get(ToastService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('validator should be object {}', () => {
    expect(component.validation).toEqual({});
  });

  it('validator should be undefine', () => {
    const key = 'email';
    component.resetValidation(key);
    expect(component.validation[key]).toBeUndefined();
  });

  describe('Forgot Password View', () => {
    beforeEach(() => {
      el = fixture.nativeElement.querySelector('.cas-auth');
    });

    it('should be have title forgot password パスワードを忘れた方はこちら ', () => {
      expect(el.querySelector('.cas-auth__title h2').textContent).toContain('パスワードを忘れた方はこちら');
    });

    it('should set isCompleted to false', () => {
      expect(component.isCompleted).toBeFalsy();
    });
  });

  describe('Forgot Password Success', () => {
    const forgotPasswordSpy = authSv.forgotPassword.and.returnValue(of({ sussess: true }));

    beforeEach(() => {
      component.forgotPassword();
    });

    it('should call function Forgot password service', () => {
      expect(forgotPasswordSpy.calls.any()).toBe(true, 'call forgot password service');
    });

    it('should set isCompleted to true and title change', () => {
      expect(component.isCompleted).toBeTruthy();
    });
  });

  describe('Forgot Password Error', () => {
    beforeEach(() => {
      authSv.forgotPassword.and.returnValue(throwError('Forgot Password fail'));
      const btnLogin = fixture.nativeElement.querySelector('button.cas-auth__btn');
      btnLogin.click();
    });

    it('should call notify error', () => {
      expect(toastSv.error).toHaveBeenCalled();
    });
  });
});
