import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: './authentication/authentication.module#AuthenticationModule',
  },
  // {
  //   path: '',
  //   loadChildren: './pages/pages.module#PagesModule',
  // },
  {
    path: '',
    loadChildren: 'app/views/views.module#ViewsModule',
  },
  {
    path: 'ai-demo',
    loadChildren: 'app/demo-omiseno/demo-omiseno.module#DemoOmisenoModule',
  },
  {
    path: 'printer/:receiptCode',
    loadChildren: 'app/views/printer/printer.module#PrinterModule',
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
