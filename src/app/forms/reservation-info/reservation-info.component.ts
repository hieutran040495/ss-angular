import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core';
import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { ClientUser } from 'shared/models/client-user';
import { Reservation } from 'shared/models/reservation';

import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ClientProfileQuery } from 'shared/states/client-profile';

import { CALENDAR_EVENT_EMITTER, RESV_TABLE_EVENT_EMITTER } from 'shared/enums/event-emitter';
import { Regexs } from 'shared/constants/regex';

@Component({
  selector: 'app-form-res-info',
  templateUrl: './reservation-info.component.html',
  styleUrls: ['./reservation-info.component.scss'],
})
export class ReservationInfoComponent implements OnInit, OnDestroy {
  @ViewChild('resvInfoForm') resvInfoForm: NgForm;
  @Output() changeFormValue: EventEmitter<null> = new EventEmitter<null>();

  @Input('resData') resData: Reservation;

  @Input('validator') validator: any = {};
  @Input() minDate: string;

  private _triggerValidForm: any;
  get triggerValidForm(): any {
    return this._triggerValidForm;
  }

  @Input('triggerValidForm') set triggerValidForm(v: any) {
    this._triggerValidForm = v;
    if (v && v.isValid) {
      this.onValidForm();
    }
  }

  private _resetForm: any;
  get resetForm(): any {
    return this._resetForm;
  }

  @Input('resetForm') set resetForm(v: any) {
    this._resetForm = v;
    if (v && v.reset) {
      this.onResetForm();
    }
  }

  rules = Regexs;

  isUseSmpOrder: boolean;

  private pageSub;

  constructor(
    private eventEmitter: EventEmitterService,
    private resvSv: ReservationService,
    private toastSv: ToastService,
    private clientProfileQuery: ClientProfileQuery,
  ) {}

  changeValuesForm(isSkipFocus: any = null) {
    this.changeFormValue.emit(isSkipFocus);
  }

  ngOnInit(): void {
    this.getClientInfo();
    this.pageSub = this.eventEmitter.caseNumber$.subscribe((res) => {
      if (res.type === RESV_TABLE_EVENT_EMITTER.CHOOSE_TABLE) {
        const i = this.resData.tables.findIndex((table) => table.id === res.data.id);
        if (i >= 0) {
          this.resData.tables.splice(i, 1);
        } else {
          this.resData.tables.push(res.data);
        }
        this.changeValuesForm();
      }
    });
  }

  ngOnDestroy(): void {
    this.pageSub.unsubscribe();
  }

  foundClientUser(clientUser: ClientUser) {
    const data: any = { client_user: clientUser };
    this.resData.deserialize(data);
    this.changeFormValue.emit();
  }

  onValidForm() {
    this.validateAllFormFields(this.resvInfoForm.form);
  }

  onResetForm() {
    this.resvInfoForm.reset();
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  activeSelectTable() {
    if (this.resData.isPassDate) return;

    this.eventEmitter.publishData({
      type: CALENDAR_EVENT_EMITTER.ENABLE_CHOOSE_TABLE,
      data: this.resData,
    });
  }

  changeSmpCode() {
    this.resvSv.changeSmpCode(this.resData.id).subscribe(
      (res) => {
        this.resData.smp_code = res.smp_code;
        this.eventEmitter.publishData({
          type: CALENDAR_EVENT_EMITTER.CHANGE_SMP_CODE,
          data: res.smp_code,
        });
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  public changeDate() {
    this.eventEmitter.publishData({
      type: CALENDAR_EVENT_EMITTER.CREATE_RES_CHANGE_DATE,
      data: this.resData.start_date,
    });

    this.changeDuration();
  }

  public changeDuration() {
    this.eventEmitter.publishData({
      type: CALENDAR_EVENT_EMITTER.CHANGE_DURATION,
      data: this.resData,
    });
  }

  private async getClientInfo() {
    const data = await this.clientProfileQuery.getClientProfile();
    this.isUseSmpOrder = data.allow_smp_order;
  }
}
