import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxDatepickerModule } from 'shared/modules/ngx-datepicker/ngx-datepicker.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';
import { InputTextModule } from 'shared/modules/input-text/input-text.module';
import { InputTimeModule } from 'shared/modules/input-time/input-time.module';
import { TimeSettingModule } from 'shared/modules/time-setting/time-setting.module';
import { ResvPhoneLookupModule } from 'app/modules/reservation-phone-lookup/resv-phone-lookup.module';
import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';

import { ReservationInfoComponent } from './reservation-info.component';

@NgModule({
  declarations: [ReservationInfoComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgxDatepickerModule,
    TimeSettingModule,
    CasNgSelectModule,
    InputTimeModule,
    InputTextModule,
    ResvPhoneLookupModule,
    SpaceValidationModule,
  ],
  exports: [ReservationInfoComponent],
})
export class ReservationInfoModule {}
