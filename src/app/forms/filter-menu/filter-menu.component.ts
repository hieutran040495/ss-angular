import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MenuType } from 'shared/models/menu-type';
import { Category } from 'shared/models/category';

import { MenuTypeService } from 'shared/http-services/menu-type.service';
import { CategoryService } from 'shared/http-services/category.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { Subject, Observable, of, concat } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, tap, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-form-filter-menu',
  templateUrl: './filter-menu.component.html',
  styleUrls: ['./filter-menu.component.scss'],
})
export class FilterMenuComponent implements OnInit {
  @Output() filterEvent: EventEmitter<any> = new EventEmitter<any>();

  private submitText: string;
  @Input('filterSubmitText')
  get filterSubmitText(): string {
    return this.submitText || '設定';
  }
  set filterSubmitText(v: string) {
    this.submitText = v;
  }

  filterData: any = {
    name: '',
    type_id_in: [],
    category_id: undefined,
    is_show: undefined,
    price_gte: undefined,
    price_lte: undefined,
    recommend: undefined,
    is_sold_out: undefined,
    allow_take_out: undefined,
  };

  isLoadMenuType: boolean = false;
  menusType$: Observable<MenuType[]>;
  menusTypeInput$: Subject<string> = new Subject<string>();

  isLoadCategory: boolean = false;
  categories$: Observable<Category[]>;
  categoryInput$: Subject<string> = new Subject<string>();

  toggleMenu = [
    {
      name: '表示',
      value: 1,
    },
    {
      name: '非表示',
      value: 0,
    },
  ];

  constructor(
    private menuTypeSv: MenuTypeService,
    private categorySv: CategoryService,
    private toastSv: ToastService,
  ) {}

  ngOnInit() {
    this._getMenusType();
    this._getCategories();
  }

  private _getMenusType() {
    if (this.isLoadMenuType) return;

    const opts = {
      order: '-updated',
    };
    this.isLoadMenuType = true;

    this.menuTypeSv.fetchMenutypes(opts).subscribe(
      (res) => {
        this.isLoadMenuType = false;
        this.searchMenusType(opts, res);
      },
      (errors) => {
        this.isLoadMenuType = false;
        this.toastSv.error(errors);
      },
    );
  }

  private searchMenusType(opts: any = {}, initData: MenuType[]) {
    this.menusType$ = concat(
      of(initData),
      this.menusTypeInput$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => (this.isLoadMenuType = true)),
        switchMap((name) => {
          if (name && name.trim().length > 0) {
            opts.name = name.trim();
          } else {
            delete opts.name;
          }

          return this.menuTypeSv.fetchMenutypes(opts).pipe(
            catchError(() => of([])),
            tap(() => (this.isLoadMenuType = false)),
          );
        }),
      ),
    );
  }

  private _getCategories() {
    if (this.isLoadCategory) return;

    const options = {
      order: '-updated',
    };

    this.isLoadCategory = true;
    this.categorySv.getCategories(options).subscribe(
      (res) => {
        this.searchCategories(options, res);
        this.isLoadCategory = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoadCategory = false;
      },
    );
  }

  private searchCategories(opts: any, initData: Category[]) {
    this.categories$ = concat(
      of(initData),
      this.categoryInput$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => (this.isLoadCategory = true)),
        switchMap((name) => {
          if (name && name.trim().length > 0) {
            opts.name = name.trim();
          } else {
            delete opts.name;
          }

          return this.categorySv.getCategories(opts).pipe(
            catchError(() => of([])),
            tap(() => (this.isLoadCategory = false)),
          );
        }),
      ),
    );
  }

  emitData() {
    this.filterData.name = this.filterData.name.trim();
    this.filterData.recommend = this.filterData.recommend ? 1 : undefined;
    this.filterData.is_sold_out = this.filterData.is_sold_out ? 1 : undefined;
    this.filterData.allow_take_out = this.filterData.allow_take_out ? 1 : undefined;
    this.filterEvent.emit(this.filterData);
  }

  onCloseSelect() {
    window.scroll(0, 0);
  }
}
