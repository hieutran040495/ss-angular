import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';

import { FilterMenuComponent } from './filter-menu.component';
import { DigitFormaterModule } from 'shared/modules/digit-formater/digit-formater.module';

@NgModule({
  declarations: [FilterMenuComponent],
  imports: [CommonModule, FormsModule, CasNgSelectModule, DigitFormaterModule],
  exports: [FilterMenuComponent],
})
export class FilterMenuModule {}
