import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TimeSettingModule } from 'shared/modules/time-setting/time-setting.module';
import { ReservationWalkinComponent } from './reservation-walkin.component';
import { InputTimeModule } from 'shared/modules/input-time/input-time.module';
import { KeyboardNumberModule } from 'shared/modules/keyboard-number/keyboard-number.module';
import { InputTextModule } from 'shared/modules/input-text/input-text.module';
import { ValidatorService } from 'shared/utils/validator.service';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';

@NgModule({
  declarations: [ReservationWalkinComponent],
  imports: [
    CommonModule,
    FormsModule,
    TimeSettingModule,
    InputTimeModule,
    KeyboardNumberModule,
    InputTextModule,
    CasNgSelectModule,
  ],
  exports: [ReservationWalkinComponent],
  providers: [ValidatorService],
})
export class ReservationWalkinModule {}
