import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationWalkinComponent } from './reservation-walkin.component';

describe('ReservationWalkinComponent', () => {
  let component: ReservationWalkinComponent;
  let fixture: ComponentFixture<ReservationWalkinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReservationWalkinComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationWalkinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
