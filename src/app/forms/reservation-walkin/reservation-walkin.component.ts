import { Component, OnInit, Input, Output, OnDestroy, ViewChild, EventEmitter } from '@angular/core';

import { Walkin } from 'shared/models/walkin';
import { Regexs } from 'shared/constants/regex';

import { CALENDAR_EVENT_EMITTER, RESV_TABLE_EVENT_EMITTER } from 'shared/enums/event-emitter';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { ClientProfileQuery } from 'shared/states/client-profile';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-form-reservation-walkin',
  templateUrl: './reservation-walkin.component.html',
  styleUrls: ['./reservation-walkin.component.scss'],
})
export class ReservationWalkinComponent implements OnInit, OnDestroy {
  @ViewChild('WalkinForm') WalkinForm: NgForm;

  @Output() formChange: EventEmitter<string> = new EventEmitter<string>();

  @Input('walkinData') walkin: Walkin = new Walkin();
  @Input('validation') validation: any = {};

  rules = Regexs;

  private pageSub;
  private formChanged;
  isUseSmpOrder: boolean;
  constructor(
    private eventEmitter: EventEmitterService,
    private resvSv: ReservationService,
    private toastSv: ToastService,
    private clientProfileQuery: ClientProfileQuery,
  ) {}

  ngOnInit() {
    this.getClientInfo();
    this.pageSub = this.eventEmitter.caseNumber$.subscribe((res) => {
      if (res.type === RESV_TABLE_EVENT_EMITTER.CHOOSE_TABLE) {
        const i = this.walkin.tables.findIndex((table) => table.id === res.data.id);

        if (i >= 0) {
          this.walkin.tables.splice(i, 1);
        } else {
          this.walkin.tables.push(res.data);
        }
        this.checkFormChange();
      }
    });
  }

  checkFormChange() {
    this.formChanged = this.WalkinForm.statusChanges.subscribe((status) => {
      this.formChange.emit(status);
    });
  }

  ngOnDestroy() {
    this.pageSub.unsubscribe();
    this.formChanged.unsubscribe();
  }

  resetValidator(key: string) {
    if (this.validation[key]) {
      this.validation[key] = undefined;
    }
  }

  activeSelectTable() {
    if (this.walkin.isFinished || this.walkin.isDatePass) return;
    this.eventEmitter.publishData({
      type: CALENDAR_EVENT_EMITTER.ENABLE_CHOOSE_TABLE,
      data: this.walkin,
    });
  }

  changeSmpCode() {
    this.resvSv.changeSmpCode(this.walkin.id).subscribe(
      (res) => {
        this.walkin.smp_code = res.smp_code;
        this.eventEmitter.publishData({
          type: CALENDAR_EVENT_EMITTER.CHANGE_SMP_CODE,
          data: res.smp_code,
        });
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  public changeDuration() {
    this.eventEmitter.publishData({
      type: CALENDAR_EVENT_EMITTER.CHANGE_DURATION,
      data: this.walkin,
    });
  }

  private async getClientInfo() {
    const data = await this.clientProfileQuery.getClientProfile();
    this.isUseSmpOrder = data.allow_smp_order;
  }
}
