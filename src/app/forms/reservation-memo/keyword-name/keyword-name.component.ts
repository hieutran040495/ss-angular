import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as cloneDeep from 'lodash/cloneDeep';

import { ResvKeyword } from 'shared/models/resv-keyword';

@Component({
  selector: 'app-res-keyword-name',
  templateUrl: './keyword-name.component.html',
  styleUrls: ['./keyword-name.component.scss'],
})
export class ReservationKeywordNameComponent implements OnInit {
  @Input('keyword') keyword: ResvKeyword;
  @Input('listKeywords') listKeywords: ResvKeyword[] = [];
  @Input('disabled') disabled: boolean;
  selectedKeyword: ResvKeyword = new ResvKeyword();

  @Output() eventKeywordChange: EventEmitter<ResvKeyword> = new EventEmitter<ResvKeyword>();
  @Output() eventSelectKeyword: EventEmitter<any> = new EventEmitter<any>();

  isLoading: boolean = false;

  validator: any = {};

  get is_invalid_name(): boolean {
    if (!this.listKeywords.length || !this.selectedKeyword.keyword) {
      return false;
    }

    const listKeywordNames = this.listKeywords.map((item) => item.keyword);
    return listKeywordNames.includes(this.selectedKeyword.keyword);
  }
  constructor() {}

  ngOnInit() {}

  submitKeyword() {
    this.eventKeywordChange.emit(this.selectedKeyword);
  }

  eventClick(popover: any) {
    if (!this.keyword.id) {
      this.selectKeyword();
      popover.show();
      return;
    }

    this.keyword.is_selected = !this.keyword.is_selected;
    this.eventSelectKeyword.emit();
  }

  eventDoubleClick(popover: any) {
    if (this.keyword.is_registered) {
      this.selectKeyword();
      popover.show();
      return;
    }
  }

  selectKeyword() {
    this.selectedKeyword = cloneDeep(this.keyword);
  }

  validatorReset(key: string): void {
    this.validator[key] = undefined;
  }
}
