import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationKeywordNameComponent } from './keyword-name.component';

describe('ReservationKeywordNameComponent', () => {
  let component: ReservationKeywordNameComponent;
  let fixture: ComponentFixture<ReservationKeywordNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReservationKeywordNameComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationKeywordNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
