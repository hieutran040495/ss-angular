import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationMemoComponent } from './reservation-memo.component';

describe('ReservationMemoComponent', () => {
  let component: ReservationMemoComponent;
  let fixture: ComponentFixture<ReservationMemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReservationMemoComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationMemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
