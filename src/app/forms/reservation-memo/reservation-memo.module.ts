import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PopoverModule } from 'ngx-bootstrap/popover';

import { ReservationMemoComponent } from './reservation-memo.component';
import { ReservationKeywordNameComponent } from './keyword-name/keyword-name.component';

import { SpaceValidationModule } from 'shared/directive/space-validation/space-validation.module';
import { AutoFocusModule } from 'shared/directive/auto-focus/auto-focus.module';
import { KeywordService } from 'shared/http-services/keyword.service';
import { DoubleClickModule } from 'shared/directive/double-click/double-click.module';

@NgModule({
  declarations: [ReservationMemoComponent, ReservationKeywordNameComponent],
  imports: [
    CommonModule,
    FormsModule,
    PopoverModule.forRoot(),
    SpaceValidationModule,
    AutoFocusModule,
    DoubleClickModule,
  ],
  exports: [ReservationMemoComponent],
  providers: [KeywordService],
})
export class ReservationMemoModule {}
