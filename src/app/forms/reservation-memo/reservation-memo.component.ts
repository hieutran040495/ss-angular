import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Reservation } from 'shared/models/reservation';
import { ResvKeyword } from 'shared/models/resv-keyword';

import { ToastService } from 'shared/logical-services/toast.service';
import { KeywordService } from 'shared/http-services/keyword.service';

@Component({
  selector: 'app-from-res-memo',
  templateUrl: './reservation-memo.component.html',
  styleUrls: ['./reservation-memo.component.scss'],
})
export class ReservationMemoComponent implements OnInit {
  @Input('resData') resData: Reservation;
  @Output() changeFormValue: EventEmitter<null> = new EventEmitter<null>();

  keywordList: ResvKeyword[] = [];
  isLoading: boolean = false;

  constructor(private toastSv: ToastService, private keywordSv: KeywordService) {}

  ngOnInit() {
    this.fetchKeywords();
  }

  fetchKeywords() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.keywordList = [];

    this.keywordSv.fetchKeywords().subscribe(
      (res) => {
        this.isLoading = false;
        this.renderKeywords(res.data);
      },
      (errors) => {
        this.isLoading = false;
      },
    );
  }

  renderKeywords(storeKeywords: ResvKeyword[]) {
    for (let i = 0; i < 9; i++) {
      if (storeKeywords[i]) {
        this.keywordList[i] = storeKeywords[i];
        continue;
      }
      this.keywordList.push(new ResvKeyword().deserialize({ keyword: null }));
    }

    this.keywordList.forEach((item) => {
      if (this.resData.keyword_ids.indexOf(item.id) >= 0) {
        item.is_selected = true;
      }
    });
  }

  eventSelectKeyword() {
    this.resData.keywords = this.keywordList.filter((item: ResvKeyword) => item.is_selected);
    this.changeValuesForm();
  }

  eventKeywordChange(event: ResvKeyword) {
    if (!event.id) {
      this.createKeyword(event);
      return;
    }

    this.updateKeyword(event);
  }

  createKeyword(keyword: ResvKeyword) {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    this.keywordSv.createKeyword(keyword.formData()).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastSv.success('キーワードを登録しました');
        this.fetchKeywords();
      },
      (errors) => {
        this.isLoading = false;
        this.toastSv.error(errors);
      },
    );
  }

  updateKeyword(keyword: ResvKeyword) {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    this.keywordSv.updateKeyword(keyword.id, keyword.formData()).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastSv.success('キーワードを更新しました');
        this.fetchKeywords();
      },
      (errors) => {
        this.isLoading = false;
        this.toastSv.error(errors);
      },
    );
  }

  changeValuesForm() {
    this.changeFormValue.emit();
  }
}
