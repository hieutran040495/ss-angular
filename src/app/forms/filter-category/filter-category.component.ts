import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { MenuType } from 'shared/models/menu-type';
import { MenuTypeService } from 'shared/http-services/menu-type.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { Pagination } from 'shared/models/pagination';

import { Subject, Observable, of, concat } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, tap, switchMap } from 'rxjs/operators';

interface FilterCategoryInput {
  name: string;
  type_id: string | number;
}

@Component({
  selector: 'app-filter-category',
  templateUrl: './filter-category.component.html',
  styleUrls: ['./filter-category.component.scss'],
})
export class FilterCategoryComponent implements OnInit {
  @Output() eventFilter: EventEmitter<Partial<FilterCategoryInput>> = new EventEmitter<Partial<FilterCategoryInput>>();

  pagination: Pagination = new Pagination();

  isLoadMenuType: boolean = false;
  menusType$: Observable<MenuType[]>;
  menusTypeInput$: Subject<string> = new Subject<string>();
  menusTypeDefault: MenuType[] = [];

  filterData: Partial<FilterCategoryInput> = {
    name: '',
    type_id: undefined,
  };

  constructor(private menuTypeSv: MenuTypeService, private toastSv: ToastService) {}

  ngOnInit() {
    this.getMenusType();
  }

  private getMenusType(type: boolean = false) {
    if (this.isLoadMenuType) return;

    const opts = {
      order: '-updated',
    };

    this.menuTypeSv.fetchMenutypes(opts).subscribe(
      (res) => {
        if (type) {
          this.menusTypeDefault = this.menusTypeDefault.concat(res);
        } else {
          this.menusTypeDefault = res;
        }
      },
      (errors) => {
        this.toastSv.error(errors);
      },
      () => {
        this.searchMenusType(opts);
      },
    );
  }

  private searchMenusType(opts) {
    this.menusType$ = concat(
      of(this.menusTypeDefault),
      this.menusTypeInput$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => (this.isLoadMenuType = true)),
        switchMap((name) => {
          if (name && name.trim().length > 0) {
            opts.name = name.trim();
          } else {
            delete opts.name;
          }

          return this.menuTypeSv.fetchMenutypes(opts).pipe(
            catchError(() => of([])),
            tap(() => (this.isLoadMenuType = false)),
          );
        }),
      ),
    );
  }

  nextPage() {
    this.pagination.nextPage();
    this.getMenusType(true);
  }

  filter() {
    this.filterData.name = this.filterData.name.trim();
    this.eventFilter.emit(this.filterData);
  }

  onCloseSelect() {
    window.scroll(0, 0);
  }
}
