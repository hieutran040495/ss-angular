import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';

import { FilterCategoryComponent } from './filter-category.component';

@NgModule({
  declarations: [FilterCategoryComponent],
  imports: [CommonModule, FormsModule, CasNgSelectModule],
  exports: [FilterCategoryComponent],
})
export class FilterCategoryModule {}
