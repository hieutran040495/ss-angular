import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SelectTimeComponent } from './select-time.component';
import { InputTimeModule } from 'shared/modules/input-time/input-time.module';

@NgModule({
  imports: [CommonModule, FormsModule, InputTimeModule],
  declarations: [SelectTimeComponent],
  exports: [SelectTimeComponent],
})
export class SelectTimeModule {}
