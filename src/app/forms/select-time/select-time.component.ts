import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ClientWorkingTime } from 'shared/models/client-working-time';

@Component({
  selector: 'app-select-working-time',
  templateUrl: './select-time.component.html',
  styleUrls: ['./select-time.component.scss'],
})
export class SelectTimeComponent implements OnInit {
  @Output() updateTime: EventEmitter<any> = new EventEmitter<any>();

  private _workingTime: ClientWorkingTime;
  @Input('workingTime')
  get workingTime(): ClientWorkingTime {
    return this._workingTime;
  }
  set workingTime(v: ClientWorkingTime) {
    this._workingTime = v;
    this.start = v.start_at;
    this.end = v.end_at;
  }

  private _errorMsg: string;
  @Input('errorMsg')
  public get errorMsg(): string {
    return this._errorMsg;
  }
  public set errorMsg(v: string) {
    this._errorMsg = v;
  }

  private _disabled: boolean;
  @Input('disabled')
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(v: boolean) {
    this._disabled = v;
  }

  public start: string = '00:00:00';
  public end: string = '23:59:00';

  constructor() {}

  ngOnInit() {}

  /**
   * Defined Function
   */
  changeSeletecTime() {
    this.updateTime.emit({
      start_at: this.start,
      end_at: this.end,
    });
  }
}
