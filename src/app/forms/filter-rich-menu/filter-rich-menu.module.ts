import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';

import { FilterRichMenuComponent } from './filter-rich-menu.component';

@NgModule({
  declarations: [FilterRichMenuComponent],
  imports: [CommonModule, FormsModule, CasNgSelectModule],
  exports: [FilterRichMenuComponent],
})
export class FilterRichMenuModule {}
