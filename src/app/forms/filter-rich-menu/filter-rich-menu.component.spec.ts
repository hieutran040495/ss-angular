import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterRichMenuComponent } from './filter-rich-menu.component';

describe('FilterRichMenuComponent', () => {
  let component: FilterRichMenuComponent;
  let fixture: ComponentFixture<FilterRichMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilterRichMenuComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterRichMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
