import { Component, EventEmitter, OnInit, Output } from '@angular/core';

interface FilterCategoryInput {
  name: string;
  has_setting: boolean;
}

@Component({
  selector: 'app-filter-rich-menu',
  templateUrl: './filter-rich-menu.component.html',
  styleUrls: ['./filter-rich-menu.component.scss'],
})
export class FilterRichMenuComponent implements OnInit {
  @Output() eventFilter: EventEmitter<Partial<FilterCategoryInput>> = new EventEmitter<Partial<FilterCategoryInput>>();

  settings: any = [
    {
      label: '設定中',
      value: 1,
    },
    {
      label: '未設定',
      value: 0,
    },
  ];

  filterData: Partial<FilterCategoryInput> = {
    name: '',
    has_setting: undefined,
  };

  constructor() {}

  ngOnInit() {}

  filter() {
    if (this.filterData.name) {
      this.filterData.name = this.filterData.name.trim();
    }

    this.eventFilter.emit(this.filterData);
  }
}
