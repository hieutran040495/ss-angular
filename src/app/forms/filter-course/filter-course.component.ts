import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CategoryService } from 'shared/http-services/category.service';

@Component({
  selector: 'app-form-filter-course',
  templateUrl: './filter-course.component.html',
  styleUrls: ['./filter-course.component.scss'],
  providers: [CategoryService],
})
export class FilterCourseComponent implements OnInit {
  @Output() filterEvent: EventEmitter<any> = new EventEmitter<any>();

  private submitText: string;
  @Input('filterSubmitText')
  get filterSubmitText(): string {
    return this.submitText || '設定';
  }
  set filterSubmitText(v: string) {
    this.submitText = v;
  }

  filterData: any = {
    name: '',
    is_show: undefined,
    price_gte: undefined,
    price_lte: undefined,
  };

  toggleMenu = [
    {
      name: '非表示',
      value: 0,
    },
    {
      name: '表示',
      value: 1,
    },
  ];

  constructor() {}

  ngOnInit() {}

  emitData() {
    this.filterData.name = this.filterData.name.trim();
    this.filterEvent.emit(this.filterData);
  }
}
