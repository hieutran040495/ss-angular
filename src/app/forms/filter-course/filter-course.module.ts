import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';

import { FilterCourseComponent } from './filter-course.component';
import { DigitFormaterModule } from 'shared/modules/digit-formater/digit-formater.module';

@NgModule({
  declarations: [FilterCourseComponent],
  imports: [CommonModule, FormsModule, CasNgSelectModule, DigitFormaterModule],
  exports: [FilterCourseComponent],
})
export class FilterCourseModule {}
