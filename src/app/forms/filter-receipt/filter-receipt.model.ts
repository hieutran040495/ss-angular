export class FilterReceiptModel {
  search_filter: string;
  table_code: string;
  unpaid: boolean;
  paid: boolean;
  receipt_number: string;
  is_self_order: boolean;

  constructor() {}

  public toJSON() {
    const output: any = {};
    if (this.paid) {
      output.is_paid = 1;
    }
    if (this.unpaid) {
      output.is_paid = 0;
    }
    if (this.paid && this.unpaid) {
      delete output.is_paid;
    }

    if (this.search_filter) {
      if (this.is_self_order) {
        output.receipt_number = this.search_filter.trim();
      } else {
        output.client_user_name = this.search_filter.trim();
      }
    }

    if (this.table_code) {
      output.table_code = this.table_code.trim();
    }

    if (this.receipt_number) {
      output.receipt_number = this.receipt_number.trim();
    }

    return output;
  }

  setSelfOrder() {
    this.is_self_order = true;
  }
}
