import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FilterReceiptModel } from './filter-receipt.model';

@Component({
  selector: 'app-form-filter-receipt',
  templateUrl: './filter-receipt.component.html',
  styleUrls: ['./filter-receipt.component.scss'],
})
export class FilterReceiptComponent implements OnInit {
  @Output() filterEvent: EventEmitter<any> = new EventEmitter<any>();

  @Input('filterData') filterData: FilterReceiptModel;
  @Input('isSelfOrder') isSelfOrder: boolean;

  constructor() {}

  ngOnInit() {}

  filterReceipt() {
    this.filterEvent.emit(this.filterData);
  }
}
