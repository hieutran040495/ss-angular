import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FilterReceiptComponent } from './filter-receipt.component';

@NgModule({
  declarations: [FilterReceiptComponent],
  imports: [CommonModule, FormsModule],
  exports: [FilterReceiptComponent],
})
export class FilterReceiptModule {}
