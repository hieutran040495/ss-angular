import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { TableType } from 'shared/models/table-type';
import { Pagination } from 'shared/models/pagination';

import { TableTypeService } from 'shared/http-services/table-type.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { Regexs } from 'shared/constants/regex';

import { Subject, Observable, of, concat } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, tap, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-filter-table',
  templateUrl: './filter-table.component.html',
  styleUrls: ['./filter-table.component.scss'],
})
export class FilterTableComponent implements OnInit {
  @Output() eventFilter: EventEmitter<any> = new EventEmitter();

  isLoadTableType: boolean = false;
  tablesType$: Observable<TableType[]>;
  tablesTypeInput$: Subject<string> = new Subject<string>();
  tablesTypeDefault: TableType[] = [];

  pagination: Pagination = new Pagination();
  isPagination: boolean = false;

  filterData = {
    name: undefined,
    type_id: undefined,
    can_smoke: undefined,
    quantity_gte: undefined,
    quantity_lte: undefined,
  };

  smoking = [
    {
      name: '喫煙可',
      value: 1,
    },
    {
      name: '喫煙不可',
      value: 0,
    },
  ];

  rules = Regexs;

  constructor(private tableTypeSv: TableTypeService, private toastSv: ToastService) {}

  ngOnInit() {
    this.getTablesType();
  }

  private getTablesType() {
    if (this.isLoadTableType || this.isPagination) return;

    const opts = {
      ...this.pagination.hasJSON(),
      order: '-updated',
    };

    this.tableTypeSv.fetchTableTypes(opts).subscribe(
      (res: any) => {
        this.tablesTypeDefault = this.tablesTypeDefault.concat(res.data);
        this.searchTablesType(opts);
        this.pagination.deserialize(res);
      },
      (errors: any) => {
        this.toastSv.error(errors.error);
        this.isLoadTableType = false;
        this.isPagination = false;
      },
    );
  }

  private searchTablesType(opts: any) {
    this.pagination.reset();

    this.tablesType$ = concat(
      of(this.tablesTypeDefault),
      this.tablesTypeInput$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => (this.isLoadTableType = true)),
        switchMap((name) => {
          if (name && name.trim().length > 0) {
            opts.name = name.trim();
          } else {
            delete opts.name;
          }

          return this.tableTypeSv.searchTableTypes(opts).pipe(
            catchError(() => of([])),
            tap(() => {
              this.isLoadTableType = false;
              this.isPagination = false;
            }),
          );
        }),
      ),
    );
  }

  changePage(data: any) {
    if (this.isPagination || !this.pagination.hasNextPage()) return;

    this.pagination.nextPage();
    this.isPagination = true;
    this.getTablesType();
  }

  tableFilter() {
    if (this.filterData.name) {
      this.filterData.name = this.filterData.name.trim();
    }
    this.eventFilter.emit(this.filterData);
  }

  onCloseSelect() {
    window.scroll(0, 0);
  }
}
