import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';

import { FilterTableComponent } from './filter-table.component';

@NgModule({
  declarations: [FilterTableComponent],
  imports: [CommonModule, FormsModule, CasNgSelectModule, OnlyNumericModule],
  exports: [FilterTableComponent],
})
export class FilterTableModule {}
