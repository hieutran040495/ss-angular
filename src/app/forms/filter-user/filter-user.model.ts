import * as moment from 'moment';

export class FilterUserModel {
  created_at_gte: Date;
  created_at_lte: Date;
  is_new: number;
  finished_reservations_count_gte: number;
  search_filter: string;

  constructor() {}

  public toJSON() {
    const output: any = {
      is_new: this.is_new,
      finished_reservations_count_gte: this.finished_reservations_count_gte,
    };

    if (this.created_at_gte) {
      output.created_at_gte = moment(this.created_at_gte).format('YYYY-MM-DD');
    }

    if (this.created_at_lte) {
      output.start_at_lte = moment(this.created_at_lte).format('YYYY-MM-DD');
    }

    if (this.search_filter) {
      output.phone_or_name = this.search_filter.trim();
    }

    return output;
  }
}
