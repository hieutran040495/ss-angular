import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';

import { FilterUserComponent } from './filter-user.component';
import { NgxDatepickerModule } from 'shared/modules/ngx-datepicker/ngx-datepicker.module';

@NgModule({
  declarations: [FilterUserComponent],
  imports: [CommonModule, FormsModule, CasNgSelectModule, NgxDatepickerModule, OnlyNumericModule],
  exports: [FilterUserComponent],
})
export class FilterUserModule {}
