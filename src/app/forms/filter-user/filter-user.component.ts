import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

import * as cloneDeep from 'lodash/cloneDeep';
import * as moment from 'moment';
import { FilterUserModel } from './filter-user.model';
import { Regexs } from 'shared/constants/regex';

@Component({
  selector: 'app-form-filter-user',
  templateUrl: './filter-user.component.html',
  styleUrls: ['./filter-user.component.scss'],
})
export class FilterUserComponent implements OnInit {
  @Output() filterEvent: EventEmitter<any> = new EventEmitter<any>();

  statuses = [
    {
      name: '新規',
      value: 1,
    },
    {
      name: 'リピーター',
      value: 0,
    },
  ];

  @Input('filterData') filterData: FilterUserModel;

  rules = Regexs;

  constructor() {}

  ngOnInit() {}

  filterUser() {
    const data = cloneDeep(this.filterData);

    if (data.created_at_gte) {
      data.created_at_gte = moment(data.created_at_gte)
        .local()
        .format('YYYY-MM-DD');
    }
    if (data.created_at_lte) {
      data.created_at_lte = moment(data.created_at_lte)
        .local()
        .format('YYYY-MM-DD');
    }

    this.filterEvent.emit(data);
  }

  resetCount() {
    if (!this.filterData.is_new) {
      this.filterData.finished_reservations_count_gte = undefined;
    }
  }
}
