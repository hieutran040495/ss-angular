import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReservationOrderComponent } from './reservation-order.component';
import { ModalOrderMenuModule } from 'app/pages/modal-order-menu/modal-order-menu.module';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';
import { FormsModule } from '@angular/forms';
import { InputQuantityModule } from 'shared/modules/input-quantity/input-quantity.module';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';

@NgModule({
  declarations: [ReservationOrderComponent],
  imports: [CommonModule, ModalOrderMenuModule, OnlyNumericModule, FormsModule, InputQuantityModule, CasNgSelectModule],
  exports: [ReservationOrderComponent],
})
export class ReservationOrderModule {}
