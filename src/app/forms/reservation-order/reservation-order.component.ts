import { Component, Input, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { EventEmitterType } from 'shared/enums/event-emitter';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import * as cloneDeep from 'lodash/cloneDeep';
import { ModalOrderMenuComponent } from 'app/pages/modal-order-menu/modal-order-menu.component';
import { Reservation } from 'shared/models/reservation';
import { Menu } from 'shared/models/menu';
import { Course } from 'shared/models/course';
import { MenuBuffet } from 'shared/models/menu-buffet';
import { RESV_ITEMS_TYPE } from 'shared/enums/reservation';
import { Coupon } from 'shared/models/coupon';
import { CouponService } from 'shared/http-services/coupon.service';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import * as findIndex from 'lodash/findIndex';
import { SEARCH_WITH, ORDER_TYPE } from 'shared/enums/type-search-order';

@Component({
  selector: 'app-form-res-order',
  templateUrl: './reservation-order.component.html',
  styleUrls: ['./reservation-order.component.scss'],
})
export class ReservationOrderComponent implements OnInit, OnDestroy {
  @Input('resData') resData: Reservation;
  @Output() eventChangeOrder: EventEmitter<any> = new EventEmitter<any>();
  @Output() changeFormValue: EventEmitter<null> = new EventEmitter<null>();
  @Output() clearAllCoupon: EventEmitter<null> = new EventEmitter<null>();

  private pageSub;
  modalRef: BsModalRef;
  coupons: Coupon[];
  validator: any = {};

  isLoading: boolean = false;
  isLoadCoupon: boolean = false;

  constructor(
    private modalSv: BsModalService,
    private eventEmitter: EventEmitterService,
    private couponSv: CouponService,
    private resvSv: ReservationService,
    private toastSv: ToastService,
  ) {}

  ngOnInit() {
    this.pageSub = this.eventEmitter.caseNumber$.subscribe((res) => {
      if (res.type === EventEmitterType.ORDER_MENU) {
        this.updateCourseMenu(res.data);
      }
    });

    if (!this.resData.id) {
      this.getTotalPrice();
    }

    if (!this.resData.isFinished) {
      this.fetchCoupons();
    }
  }

  ngOnDestroy() {
    this.pageSub.unsubscribe();
  }

  openModalOrderMenu() {
    if (this.resData.isFinished || this.resData.isCanceled) {
      return;
    }
    const opts = {
      class: 'modal-md modal-dialog-centered',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        courses: cloneDeep(this.resData.courses),
        items: cloneDeep(this.resData.items),
        seachType: SEARCH_WITH.COURSE,
        orderType: ORDER_TYPE.RESV_ORDER,
      },
    };
    this.openModalWithComponent(ModalOrderMenuComponent, opts);
  }

  private openModalWithComponent(comp, opts: ModalOptions = {}) {
    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();
    });

    this.modalRef = this.modalSv.show(comp, opts);
  }

  showPriceSelection(quantity: number, price: number): string {
    if (!quantity) {
      return '0';
    }
    return (quantity * price).format();
  }

  updateCourseMenu(data: any) {
    this.resData.courses = [];
    this.resData.items = [];
    this.resData.buffets = [];

    if (data.courses) {
      this.resData.deserialize({ courses: data.courses });
      this.fetchCoupons();
    }

    if (data.items) {
      this.resData.deserialize({ items: data.items });
    }

    if (data.buffets) {
      this.resData.deserialize({ buffets: data.buffets });
    }

    this.eventChangeOrder.emit({ orders: this.resData.all_selection });
    this.getTotalPrice();
  }

  removeSelection(item: Menu | Course | MenuBuffet, type: string) {
    if (type === RESV_ITEMS_TYPE.ITEM) {
      this.resData.items = this.resData.items.filter((menu: Menu) => {
        return menu.id !== item.id;
      });
    }

    if (type === RESV_ITEMS_TYPE.BUFFET) {
      this.resData.buffets = this.resData.buffets.filter((buffet: MenuBuffet) => {
        return buffet.id !== item.id;
      });
    }

    if (type === RESV_ITEMS_TYPE.COURSE) {
      this.resData.courses = this.resData.courses.filter((course: Course) => {
        return course.id !== item.id;
      });

      this.fetchCoupons();
    }

    this.getTotalPrice();
    this.eventChangeOrder.emit({ orders: this.resData.all_selection });
  }

  fetchCoupons() {
    if (this.isLoadCoupon) {
      return;
    }

    if (!this.resData.client_user.phone) {
      this.resData.coupon = undefined;
      return;
    }

    this.isLoadCoupon = true;
    this.coupons = [];

    this.couponSv.getCouponsAvailble(this.resData.getCouponToJSON()).subscribe(
      (res) => {
        this.coupons = res.data;
        this.isLoadCoupon = false;

        if (!this.resData.coupon) {
          return;
        }
        const index = findIndex(this.coupons, ['id', this.resData.coupon.id]);
        if (index === -1) {
          this.resData.coupon = undefined;
        }
      },
      (errors) => {
        this.isLoadCoupon = false;
      },
    );
  }

  getTotalPrice() {
    if (this.isLoading || this.resData.isFinished) {
      return;
    }
    this.isLoading = true;
    this.resvSv.getTotalPrice(this.resData.getTotalPrice()).subscribe(
      (res) => {
        this.resData.total_price = res.total_fee;
        this.isLoading = false;
        this.changeFormValue.emit();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  resetValidation(key: string): void {
    this.validator[key] = undefined;
  }

  changeQuantity() {
    this.getTotalPrice();
  }

  clearAll() {
    this.clearAllCoupon.emit();
  }
}
