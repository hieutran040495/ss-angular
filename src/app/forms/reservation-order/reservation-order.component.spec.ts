import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationOrderComponent } from './reservation-order.component';

describe('ReservationOrderComponent', () => {
  let component: ReservationOrderComponent;
  let fixture: ComponentFixture<ReservationOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReservationOrderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
