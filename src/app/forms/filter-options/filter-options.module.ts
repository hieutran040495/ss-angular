import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DigitFormaterModule } from 'shared/modules/digit-formater/digit-formater.module';

import { FilterOptionsComponent } from './filter-options.component';

@NgModule({
  declarations: [FilterOptionsComponent],
  imports: [CommonModule, FormsModule, DigitFormaterModule],
  exports: [FilterOptionsComponent],
})
export class FilterOptionsModule {}
