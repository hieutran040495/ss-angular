import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filter-options',
  templateUrl: './filter-options.component.html',
  styleUrls: ['./filter-options.component.scss'],
})
export class FilterOptionsComponent implements OnInit {
  @Output() filterEvent: EventEmitter<any> = new EventEmitter<any>();
  filterData: any = {
    name: '',
    price_gte: undefined,
    price_lte: undefined,
  };

  constructor() {}
  ngOnInit() {}

  filter() {
    this.filterData.name = this.filterData.name.trim();
    this.filterEvent.emit(this.filterData);
  }
}
