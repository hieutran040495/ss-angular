import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/guards/auth.guard';

import { ViewsComponent } from './views.component';
import { ViewsResolve } from './views.resolve';
import { MenuBuffetCeResolve } from 'app/pages/menu-buffet-ce/menu-buffet-ce.resolve';
import { MenuCeResolve } from 'app/pages/menu-ce/menu-ce.resolve';
import { CourseCeResolve } from 'app/pages/course-ce/course-ce.resolve';
import { ManageAccountCeResolve } from 'app/pages/manage-account-ce/manage-account-ce.resolve';
import { AdminResolve } from 'app/guards/admin.resolve';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: ViewsComponent,
    resolve: {
      pages: ViewsResolve,
    },
    children: [
      {
        path: '',
        redirectTo: 'reservations',
      },
      {
        path: 'reservations',
        loadChildren: 'app/views/reservations/reservations.module#ReservationsModule',
        data: {
          appScreen: 'C-2',
        },
      },
      {
        path: 'history',
        loadChildren: 'app/views/history/history.module#HistoryModule',
        data: {
          pageTitle: '予約履歴一覧',
          appScreen: 'C-9',
        },
      },
      {
        path: 'users',
        children: [
          {
            path: '',
            loadChildren: 'app/pages/user-list/user-list.module#UserListModule',
            data: {
              isHeaderSeparate: true,
              isDownloadCSV: true,
              pageTitle: '顧客管理',
              appScreen: 'C-3',
            },
          },
        ],
      },
      {
        path: 'setting',
        loadChildren: 'app/pages/setting/setting.module#SettingModule',
      },
      {
        path: 'menu-buffet',
        children: [
          {
            path: 'create',
            loadChildren: 'app/pages/menu-buffet-ce/menu-buffet-ce.module#MenuBuffetCeModule',
            data: {
              isChild: true,
              pageTitle: '放題メニュー新規作成',
            },
          },
          {
            path: ':buffetId/edit',
            loadChildren: 'app/pages/menu-buffet-ce/menu-buffet-ce.module#MenuBuffetCeModule',
            data: {
              isChild: true,
              pageTitle: '放題メニュー編集',
            },
            resolve: {
              buffet: MenuBuffetCeResolve,
            },
          },
        ],
      },
      {
        path: 'menu',
        children: [
          {
            path: 'create',
            loadChildren: 'app/pages/menu-ce/menu-ce.module#MenuCeModule',
            data: {
              isChild: true,
              pageTitle: 'メニュー新規作成',
            },
          },
          {
            path: ':menuId/edit',
            loadChildren: 'app/pages/menu-ce/menu-ce.module#MenuCeModule',
            data: {
              isChild: true,
              pageTitle: 'メニュー編集',
            },
            resolve: {
              menu: MenuCeResolve,
            },
          },
        ],
      },
      {
        path: 'course',
        children: [
          {
            path: 'create',
            loadChildren: 'app/pages/course-ce/course-ce.module#CourseCeModule',
            data: {
              isChild: true,
              pageTitle: 'コース新規作成',
            },
          },
          {
            path: ':id/edit',
            loadChildren: 'app/pages/course-ce/course-ce.module#CourseCeModule',
            data: {
              isChild: true,
              pageTitle: 'コース編集',
            },
            resolve: {
              course: CourseCeResolve,
            },
          },
        ],
      },
      {
        path: 'account',
        resolve: {
          role: AdminResolve,
        },
        children: [
          {
            path: 'create',
            loadChildren: 'app/pages/manage-account-ce/manage-account-ce.module#ManageAccountCeModule',
            data: {
              isChild: true,
              pageTitle: 'アカウント新規作成',
            },
          },
          {
            path: ':accountId/edit',
            loadChildren: 'app/pages/manage-account-ce/manage-account-ce.module#ManageAccountCeModule',
            data: {
              isChild: true,
              pageTitle: 'アカウント編集',
            },
            resolve: {
              account: ManageAccountCeResolve,
            },
          },
        ],
      },
      {
        path: 'history-call',
        children: [
          {
            path: '',
            loadChildren: 'app/pages/history-call/history-call.module#HistoryCallModule',
            data: {
              isHeaderSeparate: true,
              pageTitle: '着信履歴',
              subPageTitle: 'お客様詳細',
              appScreen: 'C-3-3',
            },
          },
        ],
      },
      // {
      //   path: 'management/tables',
      //   loadChildren: 'app/pages/table-management/table-management.module#TableManagementModule',
      //   data: {
      //     isChild: true,
      //     pageTitle: '座席管理',
      //     appScreen: 'C-7-1',
      //   },
      // },
      // {
      //   path: 'tables/view',
      //   loadChildren: 'app/pages/tables-view/tables-view.module#TablesViewModule',
      //   data: {
      //     isChild: true,
      //     pageTitle: '座席管理',
      //     appScreen: 'C-7',
      //   },
      // },
      {
        path: 'resv-update-history',
        children: [
          {
            path: '',
            loadChildren: 'app/pages/resv-update-history/resv-update-history.module#ResvUpdateHistoryModule',
            data: {
              pageTitle: '変更履歴',
              appScreen: 'C-4',
            },
          },
        ],
      },
      {
        path: 'receipts',
        children: [
          {
            path: '',
            loadChildren: 'app/views/receipt/receipt.module#ReceiptModule',
            data: {
              pageTitle: '会計一覧',
              appScreen: 'C-6-1',
            },
          },
        ],
      },
      {
        path: 'profit-analysis',
        loadChildren: 'app/pages/profit-analysis/profit-analysis.module#ProfitAnalysisModule',
      },
      {
        path: 'ai-analysis',
        loadChildren: 'app/views/ai-analysis/ai-analysis.module#AiAnalysisModule',
        data: {
          appScreen: 'C-10',
        },
      },
      {
        path: 'ai-analysis-access',
        loadChildren: 'app/views/ai-analysis-access/ai-analysis-access.module#AiAnalysisAccessModule',
      },
      {
        path: 'style-guide',
        loadChildren: 'app/views/cas-style-guide/cas-style-guide.module#CasStyleGuideModule',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [MenuCeResolve, MenuBuffetCeResolve, CourseCeResolve, ManageAccountCeResolve, ViewsResolve, AdminResolve],
  exports: [RouterModule],
})
export class ViewsRoutingModule {}
