import { Component, ElementRef, HostListener, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { PhoneLookupService } from 'shared/http-services/phone-lookup.service';
import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';
import { PusherEvent } from 'shared/models/event';
import { ClientUser } from 'shared/models/client-user';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ModalIncomingCallComponent } from 'app/pages/modal-incoming-call/modal-incoming-call.component';
import { ToastService } from 'shared/logical-services/toast.service';
import { Reservation } from 'shared/models/reservation';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { CALENDAR_EVENT_EMITTER } from 'shared/enums/event-emitter';
import * as $ from 'jquery';

@Component({
  selector: 'app-views',
  templateUrl: './views.component.html',
  styleUrls: ['./views.component.scss'],
})
export class ViewsComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('CasLayout') private casLayoutRef: ElementRef;
  modalRef: BsModalRef;

  constructor(
    private router: Router,
    private phoneLookupService: PhoneLookupService,
    private echoSv: LaravelEchoService,
    private toastSv: ToastService,
    private modalSv: BsModalService,
    private eventEmitter: EventEmitterService,
  ) {
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        // @ts-ignore: Accessing private variable as workaround for missing feature
        this.modalSv.loaders.forEach((loader) => {
          loader.instance.hide();
        });
        this.closeSideNav();
      }
    });
  }

  ngOnInit() {
    this.onNotification();
  }

  ngAfterViewInit() {
    const h = Number(sessionStorage.getItem('origin_height'));

    if (h === 0) {
      sessionStorage.setItem('origin_height', window.innerHeight.toString());
    }

    $(window).on('resize', (e) => {
      const height = Number(sessionStorage.getItem('origin_height'));

      if (e.target.innerHeight >= height) {
        $('.cas-slide__body-content').css('height', ``);
      } else {
        $('.cas-slide__body-content').css('height', `${e.target.innerHeight - 160}px`);
      }

      const focusEl = $(document.activeElement);

      if (['TEXTAREA', 'INPUT'].includes(focusEl.prop('tagName'))) {
        // scroll to focus input

        let offsetTop = focusEl.offset().top;

        if (offsetTop >= e.target.innerHeight) {
          offsetTop = offsetTop - 70;
        }

        $('.cas-slide__body-content').scrollTop(offsetTop);
      }
    });
  }

  ngOnDestroy() {
    $(window).off('resize');
  }

  @HostListener('document:click', ['$event'])
  @HostListener('touchstart', ['$event'])
  onEventCloseSideNav(event: any) {
    if (
      event.target.classList.contains('toggle-menu') ||
      event.target.classList.contains('nav-item') ||
      event.target.classList.contains('nav-item--main') ||
      event.target.classList.contains('main-text') ||
      event.target.classList.contains('nav-item--text') ||
      event.target.classList.contains('nav-item--icon') ||
      event.target.classList.contains('sub-icon') ||
      event.target.classList.contains('fa-chevron-down')
    ) {
      return;
    }

    this.closeSideNav();
  }

  closeSideNav(): void {
    if (this.casLayoutRef.nativeElement.classList.contains('cas-app--show-sidenav')) {
      this.casLayoutRef.nativeElement.classList.remove('cas-app--show-sidenav');
    }
  }

  toggleSideNav(): void {
    this.casLayoutRef.nativeElement.classList.toggle('cas-app--show-sidenav');
  }

  private onNotification() {
    this.echoSv.onNotification((e: any) => {
      const event = new PusherEvent().deserialize(e);

      // Do not reload calendar
      if (event.isCodeKeyWord) {
        return;
      }

      if (event.isIncomingCall) {
        if (this.modalRef) {
          this.modalRef.hide();
        }

        const incomingCall = setTimeout(() => {
          this.findUserIncomingCall(event.phone);
          clearTimeout(incomingCall);
        }, 500);

        return;
      }

      if (event.isReloadCalendar) {
        // reload calendar
        this.eventEmitter.publishData({
          type: CALENDAR_EVENT_EMITTER.RELOAD,
        });

        return;
      }
    });
  }

  private findUserIncomingCall(phone: string) {
    const opts = {
      phone: phone,
      with: 'upcoming_reservation,upcoming_reservation.tables,finished_reservations_count',
    };

    this.phoneLookupService.getPhoneLookup(opts).subscribe(
      (res) => {
        const config = {
          class: 'modal-md modal-dialog-right',
          initialState: {
            clientUser: new ClientUser(),
            upcomingResv: null,
          },
        };
        if (res.client_user) {
          config.initialState.clientUser = new ClientUser().deserialize(res.client_user);
          if (res.client_user.upcoming_reservation) {
            config.initialState.upcomingResv = new Reservation().deserialize(res.client_user.upcoming_reservation);
          }
        } else {
          config.initialState.clientUser.phone = phone;
        }

        this.openModalWithComponent(ModalIncomingCallComponent, config);
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  private openModalWithComponent(comp, config?: ModalOptions) {
    const subscribe = this.modalSv.onHidden.subscribe((reason: string) => {
      subscribe.unsubscribe();
    });

    this.modalRef = this.modalSv.show(comp, config);
  }
}
