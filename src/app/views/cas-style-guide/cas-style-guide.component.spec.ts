import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasStyleGuideComponent } from './cas-style-guide.component';

describe('CasStyleGuideComponent', () => {
  let component: CasStyleGuideComponent;
  let fixture: ComponentFixture<CasStyleGuideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasStyleGuideComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasStyleGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
