import { Component, OnInit } from '@angular/core';

import { FilePreview } from 'shared/models/file-preview';
import { TableHeader } from 'app/modules/cas-table/cas-table.component';

@Component({
  selector: 'app-cas-style-guide',
  templateUrl: './cas-style-guide.component.html',
  styleUrls: ['./cas-style-guide.component.scss'],
})
export class CasStyleGuideComponent implements OnInit {
  cardList = [
    {
      content:
        'トマト料理トマト料理トマト料理トマト料理トマト料理理トマト料理トマト料理トト料理トマト料理トト料理トマト料理トマト料理トマト料理トマト料理トマト料理トマト料理理トマト料理トマト料理',
      status: '非表示',
      price: 23000,
    },
    {
      content: 'トマト料理トマト料理トマト料理トマト料理トマト料理理トマト料理トマト料理',
      image: 'http://image.sggp.org.vn/w570/Uploaded/2019/duaeymdrei/2018_01_21/u8d_uant.jpg',
      status: '非表示',
      price: 23000,
    },
    {
      content: 'トマト料理トマト料理トマト料理トマト料理トマト料理理トマト料理トマト料理',
      price: 23000,
    },
  ];

  sortItems = [
    {
      name: 'メニュー名',
      value: '',
      active: true,
    },
    {
      name: 'カテゴリー名',
      value: '',
      active: false,
    },
    {
      name: '価格',
      value: '',
      active: false,
    },
    {
      name: '表示',
      value: '',
      active: false,
    },
  ];

  cardType = [
    {
      id: 1,
      title:
        'ココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココア',
      caption:
        'ココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココア',
      isAction: true,
    },
    {
      id: 2,
      title:
        'ココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココア',
      isAction: true,
    },
    {
      id: 3,
      title:
        'ココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココアココア',
      isAction: false,
    },
  ];

  cardTables = [
    {
      type: {
        name: '座敷座敷座敷座敷座敷座敷座敷座敷座敷座敷座敷座敷座敷座敷座敷',
      },
      name: 'カウンター03',
      quantity: 3,
      isSmoking: false,
    },
    {
      type: {
        name: '座敷',
      },
      name: 'カウンター03カウンター03カウンター03',
      quantity: 2,
      isSmoking: true,
    },
    {
      type: {
        name: '座敷',
      },
      name: 'カウンター03',
      quantity: 6,
      isSmoking: false,
    },
  ];

  tableHeader: Partial<TableHeader>[] = [
    {
      name: '設定ステータス',
      width: 150,
      class: 'text-center',
    },
    {
      name: `画面名<span class="font-weight-normal">（オーダー画面に表示される名前）</span>`,
    },
    {
      name: 'メニュー品数',
      width: 150,
      class: 'text-center',
    },
    {
      name: '',
      width: 150,
    },
  ];

  constructor() {}

  ngOnInit() {}

  /**
   * Event sort
   * @param sortData: { oldId: number; nextId: number }
   * @param oldId: This is the id of current item active
   * @param nextId: This is the id of the item click
   */
  eventSort(sortData: any) {
    // TODO: call api sort
    this.sortItems[sortData.oldId].active = false;
    this.sortItems[sortData.nextId].active = true;
  }

  /**
   * Event destroy item
   * @param id: number
   */
  eventTrash(id: number) {
    // TODO: call api delete item
  }

  uploadFile(files: FilePreview[]) {
    console.log(files);
  }
}
