import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

// Import module
import { CasCardItemModule } from 'app/modules/cas-card-item/cas-card-item.module';
import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { CasCardTypeModule } from 'app/modules/cas-card-type/cas-card-type.module';
import { CasCardTableModule } from 'app/modules/cas-card-table/cas-card-table.module';
import { CasDropzoneModule } from 'app/modules/cas-dropzone/cas-dropzone.module';
import { CasPreviewModule } from 'app/modules/cas-preview/cas-preview.module';
import { CasTableModule } from 'app/modules/cas-table/cas-table.module';
import { CasDialogModule } from 'app/modules/cas-dialog/cas-dialog.module';

import { CasStyleGuideComponent } from './cas-style-guide.component';

const routes: Routes = [
  {
    path: '',
    component: CasStyleGuideComponent,
  },
];

@NgModule({
  declarations: [CasStyleGuideComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CasCardItemModule,
    CasFilterModule,
    CasCardTypeModule,
    CasCardTableModule,
    CasDropzoneModule,
    CasPreviewModule,
    CasTableModule,
    CasDialogModule,
  ],
})
export class CasStyleGuideModule {}
