import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResvCourseHistoryComponent } from './resv-course-history.component';

describe('ResevationCourseHistoryComponent', () => {
  let component: ResvCourseHistoryComponent;
  let fixture: ComponentFixture<ResvCourseHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResvCourseHistoryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResvCourseHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
