import { Component, OnDestroy } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';

import { NOTIFY_CODE } from 'shared/enums/code-notify';
import { HISTORY_TYPE } from 'shared/enums/event-emitter';

import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { LaravelEchoService } from 'shared/http-services/laravel-echo.service';

import { DownloadHelpers } from 'shared/utils/download';

import { HistoryBaseComponent } from 'app/views/history/history.base.component';
import { ClientProfileQuery } from 'shared/states/client-profile';

@Component({
  selector: 'app-resv-course-history',
  templateUrl: './resv-course-history.component.html',
  styleUrls: ['./resv-course-history.component.scss'],
})
export class ResvCourseHistoryComponent extends HistoryBaseComponent implements OnDestroy {
  isDownload: boolean = false;

  filterTerm = {
    order: '-start_at',
  };

  reservationOpts = {
    with: 'client_user,reservation_preorder_items,tables',
    type_in: ['web', 'phone'],
  };

  sortItems: any[] = [
    {
      value: '-start_at',
      title: '新しい順',
      isSelected: true,
    },
    {
      value: 'start_at',
      title: '古い順',
      isSelected: false,
    },
  ];

  get sortItemSelected(): string {
    return this.sortItems.filter((item: any) => item.isSelected)[0].title;
  }

  constructor(
    reservationSv: ReservationService,
    toastSv: ToastService,
    eventEmitter: EventEmitterService,
    bsModalSv: BsModalService,
    private echoSv: LaravelEchoService,
    public clientProfileQuery: ClientProfileQuery,
  ) {
    super(reservationSv, toastSv, eventEmitter, bsModalSv, clientProfileQuery);

    this.pageSub = this.eventEmitter.caseNumber$.subscribe((res: any) => {
      if (res.type && res.type === HISTORY_TYPE.RESERVATION_FILTER) {
        this.filterTerm = { ...this.filterTerm, ...res.data };
        this.getReservations();
      }
    });

    this.echoSv.onNotification((e: any) => {
      if (e.code === NOTIFY_CODE.FINISH_EXPORT_PDF_RESERVATION && this.isDownload) {
        this.isDownload = false;
        DownloadHelpers.downloadFromUri(e.path);
      }
    });
  }

  changeSort(idx: number) {
    this.sortItems.filter((item: any) => {
      item.isSelected = false;
    });
    this.sortItems[idx].isSelected = true;
    this.filterTerm = { ...this.filterTerm, order: this.sortItems[idx].value };
    this.pagination.reset();
    this.getReservations();
  }

  downloadCSV() {
    if (!this.reservationKey.length) {
      this.toastSv.warning('データを表示した状態でクリックすると、PDFファイルでダウンロードすることができます');
      return;
    }

    if (this.isDownload) return;

    const opts = {
      ...this.filterTerm,
      ...this.reservationOpts,
    };

    this.isDownload = true;
    this.reservationSv.downloadCSVResCourse(opts).subscribe(
      (res: any) => {
        this.toastSv.success('PDFファイルをダウンロードしています。少々お待ちください。');
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isDownload = false;
      },
    );
  }
}
