import { Component, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { HISTORY_TYPE } from 'shared/enums/event-emitter';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';

import * as moment from 'moment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent implements OnDestroy {
  filterDate = {
    created_at_gte: moment()
      .startOf('days')
      .format(),
    created_at_lte: moment()
      .endOf('days')
      .format(),
  };

  private pageSub: Subscription;

  constructor(private eventEmitterSv: EventEmitterService, private router: Router) {
    this.pageSub = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.historyFilter();
      }
    });
  }

  ngOnDestroy(): void {
    this.pageSub.unsubscribe();
  }

  historyFilter() {
    this.eventEmitterSv.publishData({
      type: HISTORY_TYPE.RESERVATION_FILTER,
      data: {
        start_at_gte: moment(this.filterDate.created_at_gte)
          .startOf('days')
          .format(),
        start_at_lte: moment(this.filterDate.created_at_lte)
          .endOf('days')
          .format(),
      },
    });
  }
}
