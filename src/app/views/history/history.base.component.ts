import { Component, OnDestroy, OnInit } from '@angular/core';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';

import { RESV_MODE_EMITTER } from 'shared/enums/event-emitter';

import { PageChangedInput, Pagination } from 'shared/models/pagination';
import { Reservation } from 'shared/models/reservation';
import { Receipt } from 'shared/models/receipt';

import { ModalReceiptDetailComponent } from 'app/pages/modal-receipt-detail/modal-receipt-detail.component';
import { ClientProfileQuery } from 'shared/states/client-profile';

import { Subscription, forkJoin } from 'rxjs';
import * as groupBy from 'lodash/groupBy';

@Component({
  selector: 'app-history-base',
  template: `
    NO UI TO BE FOUND HERE!
  `,
  styleUrls: [],
})
export class HistoryBaseComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  isPaginate: boolean = false;
  isSubmitting: boolean = false;

  reservations: Reservation[] = [];
  reservationKey: string[] = [];

  pagination: Pagination = new Pagination();
  filterTerm = {};
  pageSub: Subscription;
  isUseSmpOrder: boolean;

  reservationOpts;

  constructor(
    public reservationSv: ReservationService,
    public toastSv: ToastService,
    public eventEmitter: EventEmitterService,
    public bsModalSv: BsModalService,
    public clientProfileQuery: ClientProfileQuery,
  ) {}

  ngOnDestroy(): void {
    if (this.pageSub) {
      this.pageSub.unsubscribe();
    }
  }

  ngOnInit() {
    this.getClientInfo();
  }

  getReservations() {
    const opts = {
      ...this.filterTerm,
      ...this.pagination.hasJSON(),
      ...this.reservationOpts,
    };

    this.isLoading = true;
    this.reservations = [];

    this.reservationSv.getReservations(opts).subscribe(
      (res) => {
        this.isLoading = false;
        this.isPaginate = false;
        this.reservations = groupBy(res.data, 'start_at_date_display');
        this.reservationKey = Object.keys(this.reservations);
        this.pagination.deserialize(res);
      },
      (errors) => {
        this.isLoading = false;
        this.isPaginate = false;
        this.toastSv.error(errors);
      },
    );
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPaginate) {
      return;
    }
    this.isPaginate = true;
    this.pagination.pageChanged(paginate.page);
    this.getReservations();
  }

  navigate(reservation: Reservation) {
    if (this.isSubmitting) {
      return;
    }
    this.isSubmitting = true;

    const opts = {
      with: `coupon,clientUser,tables,user,${this.isUseSmpOrder ? 'reservation_passcode' : ''}`,
    };
    this.reservationSv.getResvById(reservation.id, opts).subscribe(
      (res) => {
        if (reservation.isFinished) {
          this.openReceiptDetail(res);
          this.isSubmitting = false;
          return;
        }

        this.eventEmitter.publishData({
          type: RESV_MODE_EMITTER.EDIT,
          data: new Reservation().deserialize(res),
        });
        this.isSubmitting = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isSubmitting = false;
      },
    );
  }

  private async getClientInfo() {
    const data = await this.clientProfileQuery.getClientProfile();
    this.isUseSmpOrder = data.allow_smp_order;
  }

  private openReceiptDetail(reservation: Reservation) {
    if (this.isLoading) return;

    this.isLoading = true;

    const opts = {
      with: 'client_user,tables,coupon,reservation_passcode,latest_payment',
    };

    const itemOpts = {
      with: 'courses,items,buffets',
      reservation_id: reservation.id,
      is_free: 0,
    };

    const reservationDetails = this.reservationSv.getResvById(reservation.id, opts);
    const reservationItems = this.reservationSv.getResvItems(itemOpts);

    forkJoin([reservationDetails, reservationItems]).subscribe(
      (results: any) => {
        this.isLoading = false;
        const modalOpts: ModalOptions = {
          class: 'modal-dialog-centered modal-lg',
          ignoreBackdropClick: true,
          keyboard: false,
          initialState: {
            receipt: new Receipt().deserialize({ ...results[0], ...results[1].data }),
          },
        };

        this.bsModalSv.show(ModalReceiptDetailComponent, modalOpts);
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }
}
