import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { PopoverModule } from 'ngx-bootstrap/popover';

import { NgxDatepickerFilterModule } from 'shared/modules/ngx-datepicker-filter/ngx-datepicker-filter.module';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { ModalReceiptDetailModule } from 'app/pages/modal-receipt-detail/modal-receipt-detail.module';

import { HistoryBaseComponent } from 'app/views/history/history.base.component';

import { HistoryComponent } from './history.component';
import { ResvHistoryComponent } from './resv-history/resv-history.component';
import { ResvCourseHistoryComponent } from './resv-course-history/resv-course-history.component';

const routes: Routes = [
  {
    path: '',
    component: HistoryComponent,
    children: [
      {
        path: '',
        redirectTo: 'reservation',
      },
      {
        path: 'reservation',
        component: ResvHistoryComponent,
      },
      {
        path: 'courses',
        component: ResvCourseHistoryComponent,
      },
    ],
  },
];

@NgModule({
  declarations: [HistoryBaseComponent, HistoryComponent, ResvHistoryComponent, ResvCourseHistoryComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    PopoverModule.forRoot(),
    NgxDatepickerFilterModule,
    CasPaginationModule.forRoot(),
    ModalReceiptDetailModule,
  ],
})
export class HistoryModule {}
