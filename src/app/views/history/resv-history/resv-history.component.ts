import { Component, OnDestroy } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';

import { HISTORY_TYPE } from 'shared/enums/event-emitter';

import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';

import { HistoryBaseComponent } from 'app/views/history/history.base.component';
import { ClientProfileQuery } from 'shared/states/client-profile';

@Component({
  selector: 'app-resv-history',
  templateUrl: './resv-history.component.html',
  styleUrls: ['./resv-history.component.scss'],
})
export class ResvHistoryComponent extends HistoryBaseComponent implements OnDestroy {
  typeArr = {
    web: false,
    phone: false,
    'walk-in': false,
  };

  reservationOpts = {
    with: 'client_user,reservationItems,tables',
    created_by_not: 'self-order',
  };

  constructor(
    public reservationSv: ReservationService,
    public toastSv: ToastService,
    public eventEmitter: EventEmitterService,
    public bsModalSv: BsModalService,
    public clientProfileQuery: ClientProfileQuery,
  ) {
    super(reservationSv, toastSv, eventEmitter, bsModalSv, clientProfileQuery);

    this.pageSub = this.eventEmitter.caseNumber$.subscribe((res: any) => {
      if (res.type && res.type === HISTORY_TYPE.RESERVATION_FILTER) {
        this.typeArr = {
          web: false,
          phone: false,
          'walk-in': false,
        };
        this.filterTerm = res.data;
        this.getReservations();
      }
    });
  }

  filterResv() {
    const typeArr = Object.keys(this.typeArr).filter((key: string) => this.typeArr[key]);
    this.pagination.reset();
    this.filterTerm = { ...this.filterTerm, type_in: typeArr };
    this.getReservations();
  }
}
