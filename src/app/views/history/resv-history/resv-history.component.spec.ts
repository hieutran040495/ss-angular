import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResvHistoryComponent } from './resv-history.component';

describe('ResvHistoryComponent', () => {
  let component: ResvHistoryComponent;
  let fixture: ComponentFixture<ResvHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResvHistoryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResvHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
