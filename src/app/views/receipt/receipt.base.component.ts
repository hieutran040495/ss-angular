import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Pagination, PageChangedInput } from 'shared/models/pagination';
import { ToastService } from 'shared/logical-services/toast.service';
import { ReservationService } from 'shared/http-services/reservation.service';

import { Receipt } from 'shared/models/receipt';

import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { DIALOG_EVENT } from 'shared/enums/modes';

import { ModalReceiptDetailComponent } from 'app/pages/modal-receipt-detail/modal-receipt-detail.component';

import { FilterReceiptModel } from 'app/forms/filter-receipt/filter-receipt.model';
import { DeviceDetectorService } from 'shared/utils/device-detector';

import { BehaviorSubject, forkJoin, Subscription } from 'rxjs';
import * as _groupBy from 'lodash/groupBy';

@Component({
  selector: 'app-receipt-base',
  template: `
    NO UI TO BE FOUND HERE!
  `,
  styleUrls: [],
})
export class ReceiptBaseComponent implements OnDestroy {
  isLoading: boolean = false;
  isLoadDetail: boolean = false;
  orderParams: any = {
    order: '-start_at',
  };

  isPagination: boolean;
  pagination: Pagination = new Pagination();

  filterData = new BehaviorSubject<FilterReceiptModel>(new FilterReceiptModel());
  private filterTerm = {};

  searchConf: any = {
    placeholder: '顧客名を入力して検索',
    button: '検索',
  };

  sortItems;

  receipts;
  receiptsKey = [];
  name;

  receiptOpts: any = {
    with: 'client_user,tables',
    created_by_not: 'self-order',
    status_not: 'canceled',
  };

  private routerSub: Subscription;
  receiptData: any = undefined;

  constructor(
    private reservationSv: ReservationService,
    private toastSv: ToastService,
    private bsModalSv: BsModalService,
    public activatedRoute: ActivatedRoute,
    public deviceDetectorSv: DeviceDetectorService,
    public router: Router,
  ) {
    this.routerSub = this.activatedRoute.queryParams.subscribe((params: Params) => {
      let receiptId: string = '';

      if (Object.keys(params).length > 0) {
        // Android
        if (
          this.deviceDetectorSv.getMobileOperatingSystem() === 'Android' &&
          params['com.squareup.pos.REQUEST_METADATA']
        ) {
          this.receiptData = JSON.parse(params['com.squareup.pos.REQUEST_METADATA']);
          this.receiptData.errorAnd = params['com.squareup.pos.ERROR_CODE'];

          if (params['com.squareup.pos.SERVER_TRANSACTION_ID']) {
            this.receiptData.transaction_id = params['com.squareup.pos.SERVER_TRANSACTION_ID'];
          }

          if (params['com.squareup.pos.CLIENT_TRANSACTION_ID']) {
            this.receiptData.client_transaction_id = params['com.squareup.pos.CLIENT_TRANSACTION_ID'];
          }
        }

        // IOS
        if (this.deviceDetectorSv.getMobileOperatingSystem() === 'iOS' && params.data) {
          const results = JSON.parse(params.data);
          this.receiptData = JSON.parse(results.state);
          this.receiptData.status = results.status;

          if (results.transaction_id) {
            this.receiptData.transaction_id = results.transaction_id;
          }

          if (results.client_transaction_id) {
            this.receiptData.client_transaction_id = results.client_transaction_id;
          }
        }

        if (this.receiptData) {
          this.pagination.current_page = this.receiptData.page;
          receiptId = this.receiptData.receiptId;
        } else if (params.receiptId) {
          receiptId = params.receiptId;
        }
        this.openReceiptDetail(+receiptId);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.routerSub) {
      this.routerSub.unsubscribe();
    }
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPagination) {
      return;
    }
    this.isPagination = true;

    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.getReceipt();
  }

  getReceipt() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    const opts = {
      ...this.filterTerm,
      ...this.orderParams,
      ...this.pagination.hasJSON(),
      ...this.receiptOpts,
      with: 'latest_payment',
    };

    this.receipts = [];

    this.reservationSv.getReceipts(opts).subscribe(
      (res) => {
        this.receipts = _groupBy(res.data, 'start_date');
        this.receiptsKey = Object.keys(this.receipts);

        this.pagination.deserialize(res);
        this.isLoading = false;
        this.isPagination = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this.isPagination = false;
      },
    );
  }

  filterReceipt() {
    this.pagination.reset();
    const filterSubscribe = this.filterData.subscribe((res: FilterReceiptModel) => {
      this.filterTerm = res.toJSON();
      this.getReceipt();
    });
    filterSubscribe.unsubscribe();
  }

  orderEvent(event) {
    this.orderParams = event;
    this.filterReceipt();
  }

  openReceiptDetail(receiptId: number) {
    if (this.isLoadDetail) {
      return;
    }

    this.isLoadDetail = true;

    const opts = {
      with: 'client_user,tables,coupon,reservation_passcode,latest_payment',
    };

    const itemOpts = {
      with: 'courses,items,buffets',
      reservation_id: receiptId,
      is_free: 0,
    };

    const reservationDetails = this.reservationSv.getResvById(receiptId, opts);
    const reservationItems = this.reservationSv.getResvItems(itemOpts);

    forkJoin([reservationDetails, reservationItems]).subscribe(
      (results: any) => {
        this.isLoadDetail = false;
        const modalOpts: ModalOptions = {
          class: 'modal-dialog-centered modal-lg',
          ignoreBackdropClick: true,
          keyboard: false,
          initialState: {
            receipt: new Receipt().deserialize({ ...results[0], ...results[1].data }),
            page: this.pagination.current_page,
            receiptData: this.receiptData,
          },
        };
        this._openModalWithComponent(ModalReceiptDetailComponent, modalOpts);
        this.router.navigate([`${window.location.pathname}`], { replaceUrl: true });
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoadDetail = false;
      },
    );
  }

  private _openModalWithComponent(comp, opts: ModalOptions) {
    const subModal = this.bsModalSv.onHidden.subscribe((reason: string) => {
      if (reason === DIALOG_EVENT.RECEIPT_RELOAD || reason === DIALOG_EVENT.RECEIPT_UNDO_PAYMENT_SUCCESS) {
        this.pagination.reset();
        this.getReceipt();
      }

      this.receiptData = undefined;
      subModal.unsubscribe();
    });

    this.bsModalSv.show(comp, opts);
  }
}
