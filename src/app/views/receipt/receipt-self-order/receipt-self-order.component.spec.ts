import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptSelfOrderComponent } from './receipt-self-order.component';

describe('ReceiptSelfOrderComponent', () => {
  let component: ReceiptSelfOrderComponent;
  let fixture: ComponentFixture<ReceiptSelfOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReceiptSelfOrderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptSelfOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
