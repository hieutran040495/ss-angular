import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastService } from 'shared/logical-services/toast.service';
import { ReservationService } from 'shared/http-services/reservation.service';

import { ReceiptBaseComponent } from 'app/views/receipt/receipt.base.component';
import { DeviceDetectorService } from 'shared/utils/device-detector';

@Component({
  selector: 'app-receipt-self-order',
  templateUrl: './receipt-self-order.component.html',
  styleUrls: ['./receipt-self-order.component.scss'],
})
export class ReceiptSelfOrderComponent extends ReceiptBaseComponent implements OnInit {
  orderParams: any = {
    order: '-updated_at',
  };

  searchConf: any = {
    placeholder: 'オーダー番号を入力して検索',
    button: '検索',
  };

  sortItems = [
    {
      name: '追加順',
      value: 'start_at',
      active: true,
    },
    {
      name: 'オーダー番号順',
      value: 'receipt_number,start_at',
      active: false,
    },
    {
      name: '会計ステータス順',
      value: 'is_paid,start_at',
      active: false,
    },
  ];

  receiptOpts = {
    with: 'client_user,tables',
    created_by: 'self-order',
    status_not: 'canceled',
  };

  constructor(
    reservationSv: ReservationService,
    toastSv: ToastService,
    bsModalSv: BsModalService,
    activatedRoute: ActivatedRoute,
    deviceDetectorSv: DeviceDetectorService,
    router: Router,
  ) {
    super(reservationSv, toastSv, bsModalSv, activatedRoute, deviceDetectorSv, router);
  }

  ngOnInit() {
    this.getReceipt();
  }
}
