import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ModalModule } from 'ngx-bootstrap/modal';

import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';
import { CasFilterModule } from 'app/modules/cas-filter/cas-filter.module';
import { FilterReceiptModule } from 'app/forms/filter-receipt/filter-receipt.module';
import { CasFilterSelfOrderModule } from 'app/modules/cas-filter-self-order/cas-filter-self-order.module';
import { ModalReceiptDetailModule } from 'app/pages/modal-receipt-detail/modal-receipt-detail.module';

import { ReceiptBaseComponent } from 'app/views/receipt/receipt.base.component';

import { ReceiptComponent } from './receipt.component';
import { ReceiptOrderComponent } from './receipt-order/receipt-order.component';
import { ReceiptSelfOrderComponent } from './receipt-self-order/receipt-self-order.component';

const routes: Routes = [
  {
    path: '',
    component: ReceiptComponent,
    children: [
      {
        path: 'order',
        component: ReceiptOrderComponent,
      },
      {
        path: 'self-order',
        component: ReceiptSelfOrderComponent,
      },
      {
        path: '**',
        redirectTo: 'order',
        pathMatch: 'full',
      },
    ],
  },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ModalModule.forRoot(),
    CasPaginationModule,
    CasFilterModule,
    CasFilterSelfOrderModule,
    FilterReceiptModule,
    ModalReceiptDetailModule,
  ],
  declarations: [ReceiptBaseComponent, ReceiptComponent, ReceiptOrderComponent, ReceiptSelfOrderComponent],
})
export class ReceiptModule {}
