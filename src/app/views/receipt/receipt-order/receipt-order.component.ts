import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DeviceDetectorService } from 'shared/utils/device-detector';
import { BsModalService } from 'ngx-bootstrap/modal';

import { ToastService } from 'shared/logical-services/toast.service';
import { ReservationService } from 'shared/http-services/reservation.service';

import { ReceiptBaseComponent } from 'app/views/receipt/receipt.base.component';

@Component({
  selector: 'app-receipt-order',
  templateUrl: './receipt-order.component.html',
  styleUrls: ['./receipt-order.component.scss'],
})
export class ReceiptOrderComponent extends ReceiptBaseComponent implements OnInit {
  searchConf: any = {
    placeholder: '顧客名を入力して検索',
    button: '検索',
  };

  sortItems = [
    {
      name: '追加順',
      value: 'start_at',
      active: true,
    },
    {
      name: 'テーブル番号順',
      value: 'code,start_at',
      active: false,
    },
    {
      name: '会計ステータス順',
      value: 'is_paid,start_at',
      active: false,
    },
  ];

  constructor(
    resvService: ReservationService,
    toastSv: ToastService,
    bsModalSv: BsModalService,
    activatedRoute: ActivatedRoute,
    deviceDetectorSv: DeviceDetectorService,
    router: Router,
  ) {
    super(resvService, toastSv, bsModalSv, activatedRoute, deviceDetectorSv, router);
  }

  ngOnInit() {
    this.getReceipt();
  }
}
