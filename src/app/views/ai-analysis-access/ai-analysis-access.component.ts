import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { TableHeader } from 'app/modules/cas-table/cas-table.component';

import { AiAnalysisAccess } from 'shared/models/ai-analysis-access';
import { Pagination, PageChangedInput } from 'shared/models/pagination';

import { DownloadHelpers } from 'shared/utils/download';

import { ToastService } from 'shared/logical-services/toast.service';
import { AiAnalysisService } from 'shared/http-services/ai-analysis.service';

import * as moment from 'moment';

@Component({
  selector: 'app-ai-analysis-access',
  templateUrl: './ai-analysis-access.component.html',
  styleUrls: ['./ai-analysis-access.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AiAnalysisAccessComponent implements OnInit {
  filterDate: any = moment().format('YYYY-MM-DD');

  tableHeader: Partial<TableHeader>[] = [
    {
      name: 'NO.',
      width: 80,
      class: 'text-center',
    },
    {
      name: 'Face ID',
      width: 128,
      class: 'text-center',
    },
    {
      name: `世代`,
      width: 150,
      class: 'text-center',
    },
    {
      name: '性別',
      width: 100,
      class: 'text-center',
    },
    {
      name: '来店時間',
      width: 200,
      class: 'text-center',
    },
    {
      name: '退店時間',
      width: 200,
      class: 'text-center',
    },
  ];

  aiAnalysisAccess: AiAnalysisAccess[] = [];
  pagination: Pagination = new Pagination();

  isPagination: boolean;
  isLoading: boolean;
  latestUpdateTime: string;

  constructor(private aiAnalysisAccessSv: AiAnalysisService, private toastSv: ToastService) {}

  ngOnInit() {
    this.getLatestUpdateTime();
    this.getAnalysisAccess();
  }

  getLatestUpdateTime() {
    this.aiAnalysisAccessSv.getLatestUpdateTime().subscribe((res: any) => {
      this.latestUpdateTime = res.latest_update_time
        ? moment(res.latest_update_time).format('YYYY-MM-DD HH:mm:ss')
        : '';
    });
  }

  pageChanged(paginate: PageChangedInput) {
    if (this.isPagination) {
      return;
    }
    this.isPagination = true;

    this.pagination.pageChanged(paginate.page);
    window.scrollTo(0, 0);
    this.getLatestUpdateTime();
    this.getAnalysisAccess();
  }

  private getAnalysisAccess() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.aiAnalysisAccess = [];

    const opts = {
      ...this.pagination.hasJSON(),
      created_at: typeof this.filterDate === 'string' ? this.filterDate : this.filterDate.created_at_gte,
    };

    this.aiAnalysisAccessSv.fetchAiAnalysisAccess(opts).subscribe(
      (res: any) => {
        this.pagination.deserialize(res);
        this.aiAnalysisAccess = res.data;
        this.isLoading = false;
        this.isPagination = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
        this.isPagination = false;
      },
    );
  }

  filterAnalysis() {
    this.getAnalysisAccess();
  }

  exportCSV() {
    if (this.isLoading) return;

    const opts = {
      created_at: this.filterDate.created_at_gte,
    };

    this.isLoading = true;
    this.aiAnalysisAccessSv.exportCSVAnalysisAccess(opts).subscribe(
      (res) => {
        const path = res.url;
        DownloadHelpers.downloadFromUri(path);
        this.isLoading = false;
      },
      (error) => {
        this.toastSv.error(error);
        this.isLoading = false;
      },
    );
  }
}
