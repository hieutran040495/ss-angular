import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NgxDatepickerFilterModule } from 'shared/modules/ngx-datepicker-filter/ngx-datepicker-filter.module';
import { CasTableModule } from 'app/modules/cas-table/cas-table.module';
import { CasPaginationModule } from 'shared/modules/pagination/pagination.module';

import { ToastService } from 'shared/logical-services/toast.service';
import { AiAnalysisService } from 'shared/http-services/ai-analysis.service';

import { AiAnalysisAccessComponent } from './ai-analysis-access.component';

const route: Routes = [
  {
    path: '',
    component: AiAnalysisAccessComponent,
  },
];

@NgModule({
  declarations: [AiAnalysisAccessComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    FormsModule,
    NgxDatepickerFilterModule,
    CasTableModule,
    CasPaginationModule.forRoot(),
  ],
  providers: [ToastService, AiAnalysisService],
})
export class AiAnalysisAccessModule {}
