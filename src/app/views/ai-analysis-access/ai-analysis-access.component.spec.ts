import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AiAnalysisAccessComponent } from './ai-analysis-access.component';

describe('AiAnalysisAccessComponent', () => {
  let component: AiAnalysisAccessComponent;
  let fixture: ComponentFixture<AiAnalysisAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AiAnalysisAccessComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AiAnalysisAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
