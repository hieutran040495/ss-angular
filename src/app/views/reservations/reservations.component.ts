import { Component, OnInit, OnDestroy } from '@angular/core';

import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { CALENDAR_EVENT_EMITTER, RESV_TABLE_EVENT_EMITTER, RESV_MODE_EMITTER } from 'shared/enums/event-emitter';
import { RESV_TYPE } from 'shared/enums/reservation';
import { Walkin } from 'shared/models/walkin';

import * as cloneDeep from 'lodash/cloneDeep';
import { Subject } from 'rxjs/Subject';
import { Reservation } from 'shared/models/reservation';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ModalUpdateTableComponent } from 'app/pages/modal-update-table/modal-update-table.component';
import { Client } from 'shared/models/client';
import { ClientProfileQuery } from 'shared/states/client-profile';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.scss'],
})
export class ReservationsComponent implements OnInit, OnDestroy {
  public eventUpdateCalendar: Subject<any> = new Subject<any>();
  private _modalRef: BsModalRef;

  private pageSub;

  clientProfile: Client;

  constructor(
    private eventEmitter: EventEmitterService,
    private modalSv: BsModalService,
    private storeQuery: ClientProfileQuery,
  ) {
    this.pageSub = this.eventEmitter.caseNumber$.subscribe((res) => {
      if (!res.type) {
        return;
      }
      switch (res.type) {
        case CALENDAR_EVENT_EMITTER.DISABLE_CHOOSE_TABLE:
          this.eventUpdateCalendar.next({
            type: CALENDAR_EVENT_EMITTER.DISABLE_CHOOSE_TABLE,
          });
          break;
        case CALENDAR_EVENT_EMITTER.CHANGE_DATE:
          this.eventUpdateCalendar.next({
            type: CALENDAR_EVENT_EMITTER.CHANGE_DATE,
            data: res.data,
          });
          break;
        case CALENDAR_EVENT_EMITTER.RELOAD:
          this.eventUpdateCalendar.next({
            type: CALENDAR_EVENT_EMITTER.RELOAD,
            data: res.data,
          });
          break;
        case CALENDAR_EVENT_EMITTER.ENABLE_CHOOSE_TABLE:
          this.eventUpdateCalendar.next({
            type: CALENDAR_EVENT_EMITTER.ENABLE_CHOOSE_TABLE,
            data: res.data,
          });
          break;
        case CALENDAR_EVENT_EMITTER.CHANGE_DURATION:
          this.eventUpdateCalendar.next({
            type: CALENDAR_EVENT_EMITTER.CHANGE_DURATION,
            data: res.data,
          });
          break;
      }
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
    if (this._modalRef) {
      this._modalRef.hide();
    }
    this.pageSub.unsubscribe();
  }

  eventClick($event) {
    if ($event.reservation.type === RESV_TYPE.WALK_IN) {
      this.eventEmitter.publishData({
        type: CALENDAR_EVENT_EMITTER.EDIT_WALKIN,
        data: cloneDeep($event.reservation),
      });
      return;
    }

    this.eventEmitter.publishData({
      type: RESV_MODE_EMITTER.EDIT,
      data: cloneDeep($event.reservation),
    });
  }

  resourcesClick($event) {
    if ($event.table) {
      this.eventEmitter.publishData({
        type: RESV_TABLE_EVENT_EMITTER.CHOOSE_TABLE,
        data: $event.table,
      });
    }
  }

  private async initClientProfile() {
    this.clientProfile = await this.storeQuery.getClientProfile();
  }

  async dayClicked(event) {
    await this.initClientProfile();

    this.eventEmitter.publishData({
      type: CALENDAR_EVENT_EMITTER.CREATE_WALKIN,
      data: new Walkin().deserialize({
        tables: [event.table],
        start_time: event.date.format('HH:mm:ss'),
        start_date: event.date.format('YYYY/MM/DD'),
        duration: this.clientProfile ? this.clientProfile.min_reservation_duration : 1,
      }),
    });
  }

  scrollEnd($event) {
    // TODO call api change data
  }

  eventDrop(event: { resv: Reservation | Walkin; oldTable: number; newTable: number; revertFunc: Function }) {
    const tables = event.resv.tables.map((t) => {
      return t.id === event.oldTable ? event.newTable : t.id;
    });

    const opts: ModalOptions = {
      class: 'modal-sm modal-dialog-centered',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
        resvId: event.resv.id,
        tableIds: tables,
        revertFunc: event.revertFunc,
      },
    };

    this._openModalWithComponent(ModalUpdateTableComponent, opts);
  }

  private _openModalWithComponent(comp, opts: ModalOptions = {}) {
    this._modalRef = this.modalSv.show(comp, opts);
  }
}
