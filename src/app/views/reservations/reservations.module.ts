import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { ReservationsComponent } from './reservations.component';
import { CalendarModule } from 'shared/modules/calendar/calendar.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalUpdateTableModule } from 'app/pages/modal-update-table/modal-update-table.module';

const routes: Routes = [
  {
    path: '',
    component: ReservationsComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    CalendarModule,
    ModalModule.forRoot(),
    ModalUpdateTableModule,
  ],
  declarations: [ReservationsComponent],
})
export class ReservationsModule {}
