import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewsRoutingModule } from './views-routing.module';
import { CasHeaderModule } from 'app/modules/cas-header/cas-header.module';
import { CasSidenavModule } from 'app/modules/cas-sidenav/cas-sidenav.module';

import { ToastService } from 'shared/logical-services/toast.service';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';

import { ViewsComponent } from './views.component';
import { ModalIncomingCallModule } from 'app/pages/modal-incoming-call/modal-incoming-call.module';

@NgModule({
  declarations: [ViewsComponent],
  imports: [CommonModule, ViewsRoutingModule, CasHeaderModule, CasSidenavModule, ModalIncomingCallModule],
  providers: [ToastService, EventEmitterService],
})
export class ViewsModule {}
