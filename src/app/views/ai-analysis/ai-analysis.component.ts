import { Component, OnDestroy, OnInit } from '@angular/core';
import { AiAnalysisService } from 'shared/http-services/ai-analysis.service';
import { ActivatedRoute } from '@angular/router';
import { AiAnalysis } from 'shared/models/ai-analysis';
import { ToastService } from 'shared/logical-services/toast.service';
import * as moment from 'moment';
import { DownloadHelpers } from 'shared/utils/download';
import { Device } from 'shared/models/device';

@Component({
  selector: 'app-ai-analysis',
  templateUrl: './ai-analysis.component.html',
  styleUrls: ['./ai-analysis.component.scss'],
})
export class AiAnalysisComponent implements OnInit, OnDestroy {
  private modalSubscribe;
  aiAnalysis: AiAnalysis = new AiAnalysis();
  isConnected: boolean = false;
  filterDate = {
    created_at_gte: moment().format('YYYY-MM-DD'),
    created_at_lte: moment().format('YYYY-MM-DD'),
  };
  dateDisplay: string = moment().format('YYYY/MM/DD');

  isLoading: boolean;

  statusData: number[] = [];
  statusLabels: string[] = [];

  genderData: number[] = [];
  genderLabels: string[] = [];

  ageData: number[] = [];
  ageLabels: string[] = [];

  colors: any = [
    {
      backgroundColor: [],
    },
  ];

  constructor(
    private aiAnalysisSv: AiAnalysisService,
    private activeRoute: ActivatedRoute,
    private toastSv: ToastService,
  ) {
    this.activeRoute.data.subscribe((res) => {
      if (res.clientInfo && res.clientInfo.devices) {
        const deviceEntrance: Device = res.clientInfo.devices.find((device: Device) => device.isEntrance);
        if (deviceEntrance && deviceEntrance.id) {
          this.isConnected = true;
          this.getAnalysis();
        }
      }
    });
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    if (this.modalSubscribe) {
      this.modalSubscribe.unsubscribe();
    }
  }

  getAnalysis() {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    this.aiAnalysisSv.fetchAiAnalysis(this.filterDate).subscribe(
      (res) => {
        this.aiAnalysis = new AiAnalysis().deserialize(res);
        this.statusData = this.aiAnalysis.status_data;
        this.statusLabels = this.aiAnalysis.status_labels;
        this.genderData = this.aiAnalysis.gender_data;
        this.genderLabels = this.aiAnalysis.gender_labels;
        this.ageData = this.aiAnalysis.age_data;
        this.ageLabels = this.aiAnalysis.age_labels;

        this.generateColors();
        this.isLoading = false;
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  exportCSV() {
    if (this.isLoading) return;

    this.isLoading = true;
    this.aiAnalysisSv.exportCSV(this.filterDate).subscribe(
      (res) => {
        const path = res.url;
        DownloadHelpers.downloadFromUri(path);
        this.isLoading = false;
      },
      (error) => {
        this.toastSv.error(error);
        this.isLoading = false;
      },
    );
  }

  filterAnalysis(event) {
    this.dateDisplay = event;
    if (this.isConnected) {
      this.getAnalysis();
    }
  }

  generateColors() {
    const maxArray = this.aiAnalysis.maxArray();
    this.ageData.forEach((item, index) => {
      if (maxArray.indexOf(item) >= 0) {
        this.colors[0].backgroundColor[index] = '#54C2CB';
      } else {
        this.colors[0].backgroundColor[index] = '#8D8D8D';
      }
    });
  }
}
