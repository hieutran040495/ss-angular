import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { ClientService } from 'shared/http-services/client.service';

@Injectable()
export class AiAnalysisResolve implements Resolve<any> {
  constructor(private clientSv: ClientService) {}

  resolve() {
    return this.clientSv.fetchClient({ with: 'devices' });
  }
}
