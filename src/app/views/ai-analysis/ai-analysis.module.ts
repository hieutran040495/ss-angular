import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NgxDatepickerFilterModule } from 'shared/modules/ngx-datepicker-filter/ngx-datepicker-filter.module';
import { AiPieChartModule } from 'app/modules/ai-pie-chart/ai-pie-chart.module';
import { AiBarChartModule } from 'app/modules/ai-bar-chart/ai-bar-chart.module';

import { AiAnalysisComponent } from './ai-analysis.component';
import { AiAnalysisService } from 'shared/http-services/ai-analysis.service';
import { AiAnalysisResolve } from './ai-analysis.resolve';
import { ToastService } from 'shared/logical-services/toast.service';

const route: Routes = [
  {
    path: '',
    component: AiAnalysisComponent,
    resolve: {
      clientInfo: AiAnalysisResolve,
    },
  },
];

@NgModule({
  declarations: [AiAnalysisComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    FormsModule,
    NgxDatepickerFilterModule,
    AiPieChartModule,
    AiBarChartModule,
  ],
  providers: [AiAnalysisService, AiAnalysisResolve, ToastService],
})
export class AiAnalysisModule {}
