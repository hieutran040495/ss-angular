import { Component, OnDestroy, OnInit } from '@angular/core';

import { ActivatedRoute, Params } from '@angular/router';

import { Printer } from 'shared/models/printer';

import { StarWebPRNTService } from 'shared/logical-services/star-web-prnt/star-web-prnt.service';
import { ReservationService } from 'shared/http-services/reservation.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { environment } from 'environments/environment';

import { Subscription } from 'rxjs';

@Component({
  selector: 'app-printer',
  templateUrl: './printer.component.html',
  styleUrls: ['./printer.component.scss'],
})
export class PrinterComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  printer: Printer = new Printer();
  screen: string;
  browser: string = 'Safari';

  private routeParamSub: Subscription;

  get currentDate(): string {
    const currentDate = new Date();
    const dateStr = currentDate
      .toISOString()
      .substring(0, 10)
      .replace(/-/g, '/');
    const timeStr = currentDate.toTimeString().substring(0, 8);
    return `${dateStr} ${timeStr}`;
  }

  constructor(
    private reservationSv: ReservationService,
    private starWebPRNTSv: StarWebPRNTService,
    private toastSv: ToastService,
    private activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.routeParamSub = this.activatedRoute.params.subscribe((param: Params) => {
      const result = param.receiptCode.split('_cs_');
      this.openReceiptDetail(result[0]);
      this.screen = result[1];
      this.browser = result[2];
    });
  }

  ngOnDestroy(): void {
    if (this.routeParamSub) {
      this.routeParamSub.unsubscribe();
    }

    // Remove script
    const head = document.getElementsByTagName('head')[0];
    const scriptTags: any = document.getElementsByTagName('script');
    for (let i = 0; i < scriptTags.length; i++) {
      if (
        scriptTags.item(i).src.includes('StarWebPrintBuilder') ||
        scriptTags.item(i).src.includes('StarWebPrintTrader')
      ) {
        head.removeChild(scriptTags.item(i));
      }
    }
  }

  printSuccess = (response) => {
    // let msg = '- onReceive -\n\n';
    //
    // msg += 'TraderSuccess : [ ' + response.traderSuccess + ' ]\n';
    //
    // msg += 'TraderStatus : [ ' + response.traderStatus + ',\n';

    const screen = this.screen === 'reservations' ? '/reservations' : '/receipts/order';
    const urlPath = `${window.location.protocol}//${window.location.host}${screen}?receiptId=${this.printer.id}`;
    let shortcutName = 'opensafari';
    if (this.browser === 'Chrome') {
      shortcutName = 'openchrome';
    }

    window.location.href = `shortcuts://run-shortcut?name=${shortcutName}&input=${urlPath}`;
  };

  printError = (response) => {
    this.toastSv.error(response.responseText);
    // let msg = '- onError -\n\n';
    //
    // msg += '\tStatus:' + response.status + '\n';
    //
    // msg += '\tResponseText:' + response.responseText + '\n\n';
    //
    // msg += 'Do you want to retry?\n';
  };

  private openReceiptDetail(receiptCode: string) {
    if (this.isLoading) {
      return;
    }

    this.isLoading = true;

    this.reservationSv.printReceiptByCode(receiptCode).subscribe(
      (res: any) => {
        this.printer = new Printer().deserialize(res);

        if (this.printer.client.logo && this.printer.client.logo.url) {
          this.toDataURL(`${environment.api_path}/receipt/${receiptCode}/logo`, (e) => {
            this.printer.client.logo.url = e;
            this.starWebPRNTSv.onSendMessageApi(this.printer.printData(), this.printSuccess, this.printError);
          });
        } else {
          this.starWebPRNTSv.onSendMessageApi(this.printer.printData(), this.printSuccess, this.printError);
        }
      },
      (errors: any) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  private toDataURL(url, callback) {
    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
      const reader = new FileReader();
      reader.onloadend = function() {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }
}
