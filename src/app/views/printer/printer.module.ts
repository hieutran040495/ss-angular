import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { StarWebPRNTService } from 'shared/logical-services/star-web-prnt/star-web-prnt.service';

import { PrinterComponent } from './printer.component';

const routes: Routes = [
  {
    path: '',
    component: PrinterComponent,
  },
];

@NgModule({
  declarations: [PrinterComponent],
  imports: [CommonModule, RouterModule.forChild(routes)],
  providers: [StarWebPRNTService],
})
export class PrinterModule {}
