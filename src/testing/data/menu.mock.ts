import { Menu } from 'shared/models/menu';
import { Category } from 'shared/models/category';
import { MenuOption } from 'shared/models/menu-option';
import { MenuSubStatus } from 'shared/models/menu-substatus';
import { MenuCookingType } from 'shared/models/menu-cooking-type';
import { MenuCookingDuration } from 'shared/models/menu-cooking-duration';

const categoriesJsons: any = require('./categories.json');
const optionsJsons: any = require('./options.json');
const subStatusesJsons: any = require('./sub-statuses.json');
const cookingTypesJsons: any = require('./cooking-types.json');
const cookingDurationsJsons: any = require('./cooking-durations.json');
const menusJsons: any = require('./menus.json');

export const categories: Category[] = categoriesJsons.data.map((res) => new Category().deserialize(res));
export const options: MenuOption[] = optionsJsons.data.map((res) => new MenuOption().deserialize(res));
export const subStatuses: MenuSubStatus[] = subStatusesJsons.data.map((res) => new MenuSubStatus().deserialize(res));
export const cookingTypes: MenuCookingType[] = cookingTypesJsons.data.map((res) =>
  new MenuCookingType().deserialize(res),
);
export const cookingDurations: MenuCookingDuration[] = cookingDurationsJsons.data.map((res) =>
  new MenuCookingDuration().deserialize(res),
);
export const menus: Menu[] = menusJsons.data.map((res) => new Menu().deserialize(res));
