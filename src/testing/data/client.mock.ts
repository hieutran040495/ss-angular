import { Client } from 'shared/models/client';
import { Tag } from 'shared/models/tag';
import { City } from 'shared/models/city';
import { Prefecture } from 'shared/models/prefecture';
import { Genre } from 'shared/models/genre';
import { User } from 'shared/models/user';

const data: any = require('./clientInfor.json');
const prefsJsons: any = require('./pref.json');
const tagJsons: any = require('./tags.json');
const citiesJsons: any = require('./cities.json');
const genresJsons: any = require('./gernes.json');
const userJson: any = require('./user.json');

export const clientData: Client = new Client().deserialize(data);
export const tags: Tag[] = tagJsons.data.map((res) => new Tag().deserialize(res));
export const cities: City[] = citiesJsons.data.map((res) => new City().deserialize(res));
export const prefs: Prefecture[] = prefsJsons.data.map((res) => new Prefecture().deserialize(res));
export const genres: Genre[] = genresJsons.data.map((res) => new Genre().deserialize(res));
export const currentUser: User = new User().deserialize(userJson);

