export class ToastServiceMock {
  constructor() {
    spyOn(this, 'success').and.callThrough();
    spyOn(this, 'error').and.callThrough();
  }

  public success(message: string, title?: string) {
    return `success ${this._renderText(message)} ${title}`;
  }

  public error(message: any, title?: string) {
    let msg: string;
    switch (message.constructor) {
      case String:
        msg = message;
        break;
      case Object:
        if (message.hasOwnProperty('message')) {
          msg = message.message;
          break;
        }
        break;
    }

    if (msg) {
      return `error ${this._renderText(message)} ${title}`;
    }
  }

  public info(message: string, title?: string) {
    return `info ${this._renderText(message)} ${title}`;
  }

  public warning(message: string, title?: string) {
    return `warning ${this._renderText(message)} ${title}`;
  }

  public clear() {
  }

  private _renderText(s: string): string {
    return s.replace('\n', '<br>');
  }
}
