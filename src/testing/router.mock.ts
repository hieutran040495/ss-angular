export class MockRouter {
  constructor() {
    spyOn(this, 'navigate').and.callThrough();
  }

  public navigate () {
    jasmine.createSpyObj('Router', ['navigate']);
  }
}
