export const environment = {
  production: false,
  api_path: '',
  fc_key: 'fullcalendar',
  pusher_key: '',
  pusher_cluster: '',
  onesignal_app_id: '',
  square_client_id: '',
};
