export enum COUPON_TARGET {
  NEW = 'new_user',
  REPEAT_USER = 'repeat_user',
  USAGE_COUNT = 'usage_count',
}

export enum COUPON_STATUS {
  TOTAL = 'total',
  DRAFT = 'draft',
  PUBLISH = 'publish',
  UNPUBLISH = 'unpublish',
  EXPIRED = 'expired',
}

export enum COUPON_STATUS_TEXT {
  draft = '下書き',
  publish = '公開中',
  unpublish = '非公開',
  expired = '期限切れ',
}
export enum SPECIFIC_TARGET_TEXT {
  ALL_DAY = '終日',
  INDICATIONS_TIME = '時間指定',
}

export enum STATUS_CLASSES {
  publish = 'status--publish',
  unpublish = 'status--unpublish',
  draft = 'status--draft',
  expired = 'status--expired',
}

export enum USAGE_TYPE {
  USAGE_EQUAL = '=',
  USAGE_GREATER = '>',
}
