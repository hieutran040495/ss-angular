export enum MODAL_SIZE {
  WIDTH = 980,
  HEIGHT = 780,
}

export enum SELF_ORDER_SCREEN_DIRECTION {
  HORZ = 'horz',
  VERT = 'vert',
}

export enum SELF_ORDER_IMAGE_SIZE {
  WIDTH = 584,
  HEIGHT = 430,
}

export enum SELF_ORDER_TOP_IMAGE_SIZE {
  WIDTH = 837.01,
  HEIGHT = 626.99,
}

export enum SELF_ORDER_TOP_IMAGE_SIZE_VERT {
  WIDTH = 376,
  HEIGHT = 669,
}

export enum SELF_ORDER_RECOMMEND_SIZE {
  WIDTH = 376,
  HEIGHT = 669,
}
