export enum DROPZONE_TYPE {
  UPLOAD = 'upload',
  REMOVE = 'remove',
  RESET_INPUT = 'reset_input',
}
