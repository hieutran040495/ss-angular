export enum MENU_TYPE {
  UNSORTED = '未分類',
}

export enum MENU_TYPE_TOOGLE {
  ONLY_COURSE = 'only_course',
  ONLY_BUFFET = 'only_buffet',
  ALLOW_TAKE_OUT = 'allow_take_out',
  RECOMMEND = 'recommend',
  IS_SHOW = 'is_show',
}
