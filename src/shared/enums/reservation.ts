export enum RES_FORM_TYPE {
  INFO = 'info',
  ORDER = 'order',
  MEMO = 'memo',
}

export enum RESV_TYPE_CLASS {
  WEB_DRAFT = 'fc-event__resv--draft--by-web',
  PHONE_DRAFT = 'fc-event__resv--draft--by-phone',
  WALK_IN = 'fc-event__resv--walk-in',
  WEB = 'fc-event__resv--by-web',
  PHONE = 'fc-event__resv--by-phone',
  WAIT_FOR_PAY = 'fc-event__resv--wait-for-pay',
}

export enum RESV_TYPE {
  WEB_DRAFT = 'draft_by_web',
  PHONE_DRAFT = 'draft_by_phone',
  WALK_IN = 'walk-in',
  WEB = 'web',
  PHONE = 'phone', // PHONE
}

export enum RESV_ITEMS_TYPE {
  COURSE = 'course',
  ITEM = 'item',
  BUFFET = 'buffet',
}

export enum RESV_STATUS {
  INCOMING = 'incoming',
  CANCELED = 'canceled',
  WORKING = 'working',
  FINISHED = 'finished',
}

export enum RESV_STATUS_TEXT {
  incoming = '予約受付',
  canceled = 'キャンセル',
  working = '利用中',
  finished = '完了',
}

export enum HISTORY_RESV_STATUS {
  CREATE = 'create',
  CANCEL = 'cancel',
  UPDATE = 'update',
  DELETE = 'delete',
}

export enum RESV_PAYMENT_BY {
  CASH = 'cash',
  CARD = 'card',
  SQUARE = 'square',
  OMISENO = 'omiseno',
}

export enum RESV_PAYMENT_BY_TEXT {
  cash = '現金',
  card = 'クレジット',
  square_cash = '現金/Square会計',
  square_card = 'クレジット/Square会計',
}

export enum RES_CREATE_BY {
  CLIENT = 'client',
  USER = 'user',
  ORDER = 'order',
  SELF_ORDER = 'self-order',
  CMS = 'cms',
}

export enum ORDER_PAYMENT_METHOD {
  ALL = 'all',
}

export enum RESERVATION_RECEIPT_TYPE {
  RELOAD_DETAILS = 'reload_details',
  KITCHEN_ITEM_UPDATE_QUANTITY = 'kitchen_item_update_quantity',
  KITCHEN_ITEM_UPDATE_OPTIONS = 'kitchen_item_update_option',
  KITCHEN_ITEM_UPDATE_STATUS = 'kitchen_item_update_status',
  RESERVATION_ITEM_CANCEL = 'reservation_item_cancel',
  RESERVATION_ADD_ITEM = 'resv_add_item',
  RESERVATION_UPDATE = 'resv_update',
  RESERVATION_PAYMENT = 'resv_paid',
  RESERVATION_UNDO_PAYMENT = 'resv_refund_order',
}
