import { Reservation } from './reservation';
import { EventObjectInput, MomentInput } from 'fullcalendar/src/types/input-types';
import { Deserializable } from 'shared/interfaces/deserializable';

export interface FcEventInput extends EventObjectInput {
  reservation: Reservation;
  resourceId: string | number;
  isDraft: boolean;
}

export class FcEvent implements Deserializable<FcEvent>, FcEventInput {
  id: string | number;
  resourceId: string | number;
  start: MomentInput;
  end: MomentInput;
  className: string[];
  title: string;
  type: string;
  reservation: Reservation;
  isDraft: boolean = false;

  constructor() {}

  deserialize(input: Partial<FcEventInput>): FcEvent {
    if (input) {
      Object.assign(this, input);
    }

    if (input.reservation) {
      this.reservation = new Reservation().deserialize(input.reservation);
    }
    return this;
  }
}
