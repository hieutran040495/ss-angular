import { Deserializable } from 'shared/interfaces/deserializable';

export interface PrefectureInput {
  prefCode: string;
  prefName: string;
}

export class Prefecture implements Deserializable<Prefecture>, PrefectureInput {
  prefCode: string;
  prefName: string;

  deserialize(input: Partial<PrefectureInput>): Prefecture {
    Object.assign(this, input);
    return this;
  }
}
