import { Deserializable } from 'shared/interfaces/deserializable';
import { AI_ANALYSIS_TYPE } from 'shared/enums/ai-analysis';
import { SortHelper } from 'shared/utils/sort.helper';
import * as sortedUniq from 'lodash/sortedUniq';

export interface AiAnalysisAge {
  total_age_0: number;
  total_age_1: number;
  total_age_2: number;
  total_age_3: number;
  total_age_4: number;
  total_age_5: number;
  total_age_6: number;
}

export interface AiAnalysisGender {
  total_gender_female: number;
  total_gender_male: number;
}

export interface AiAnalysisStatus {
  total_customer_new: number;
  total_customer_repeater: number;
}

export interface AiAnalysisInput {
  total_age_0: number;
  total_age_1: number;
  total_age_2: number;
  total_age_3: number;
  total_age_4: number;
  total_age_5: number;
  total_age_6: number;
  total_customer_new: number;
  total_customer_repeater: number;
  total_gender_female: number;
  total_gender_male: number;
  total_user: number;
}

export class AiAnalysis implements Deserializable<AiAnalysis>, AiAnalysisInput {
  total_age_0: number;
  total_age_1: number;
  total_age_2: number;
  total_age_3: number;
  total_age_4: number;
  total_age_5: number;
  total_age_6: number;
  total_customer_new: number;
  total_customer_repeater: number;
  total_gender_female: number;
  total_gender_male: number;
  total_user: number;

  ai_analysis_age: AiAnalysisAge;
  ai_analysis_gender: AiAnalysisGender;
  ai_analysis_status: AiAnalysisStatus;

  get age_data(): number[] {
    return Object.values(this.ai_analysis_age).map((item) => item);
  }

  get age_labels(): string[] {
    return Object.keys(this.ai_analysis_age).map((item) => AI_ANALYSIS_TYPE[item]);
  }

  get gender_data(): number[] {
    return Object.values(this.ai_analysis_gender).map((item) => item);
  }

  get gender_labels(): string[] {
    return Object.keys(this.ai_analysis_gender).map((item) => AI_ANALYSIS_TYPE[item]);
  }

  get status_data(): number[] {
    return Object.values(this.ai_analysis_status).map((item) => item);
  }

  get status_labels(): string[] {
    return Object.keys(this.ai_analysis_status).map((item) => AI_ANALYSIS_TYPE[item]);
  }

  get gender_male_percent(): string {
    const percent = (this.total_gender_male / this.total_user) * 100;
    return isNaN(percent) ? '0' : percent.toFixed(1);
  }
  get gender_female_percent(): string {
    const percent = (this.total_gender_female / this.total_user) * 100;
    return isNaN(percent) ? '0' : percent.toFixed(1);
  }
  get status_new_percent(): string {
    const percent = (this.total_customer_new / this.total_user) * 100;
    return isNaN(percent) ? '0' : percent.toFixed(1);
  }
  get status_repeater_percent(): string {
    const percent = (this.total_customer_repeater / this.total_user) * 100;
    return isNaN(percent) ? '0' : percent.toFixed(1);
  }
  get data_status_not_found(): boolean {
    const sumStatus = this.total_customer_new + this.total_customer_repeater;
    return sumStatus === 0;
  }
  get data_gender_not_found(): boolean {
    const sumGender = this.total_gender_male + this.total_gender_female;
    return sumGender === 0;
  }
  get data_age_not_found(): boolean {
    const sumAge = this.age_data.reduce((accumulator, currentValue) => accumulator + currentValue);
    return sumAge === 0;
  }

  constructor() {
    this.ai_analysis_age = {
      total_age_0: 0,
      total_age_1: 0,
      total_age_2: 0,
      total_age_3: 0,
      total_age_4: 0,
      total_age_5: 0,
      total_age_6: 0,
    };

    this.ai_analysis_gender = {
      total_gender_male: 0,
      total_gender_female: 0,
    };

    this.ai_analysis_status = {
      total_customer_new: 0,
      total_customer_repeater: 0,
    };
  }

  deserialize(input: Partial<AiAnalysisInput>): AiAnalysis {
    Object.assign(this, input);
    this.ai_analysis_age = {
      total_age_0: input.total_age_0,
      total_age_1: input.total_age_1,
      total_age_2: input.total_age_2,
      total_age_3: input.total_age_3,
      total_age_4: input.total_age_4,
      total_age_5: input.total_age_5,
      total_age_6: input.total_age_6,
    };

    this.ai_analysis_gender = {
      total_gender_male: input.total_gender_male,
      total_gender_female: input.total_gender_female,
    };

    this.ai_analysis_status = {
      total_customer_new: input.total_customer_new,
      total_customer_repeater: input.total_customer_repeater,
    };

    return this;
  }

  maxArray(): number[] {
    const ageArr = SortHelper.quickSort(this.age_data, 0, this.age_data.length - 1);

    return sortedUniq(ageArr).splice(0, 3);
  }
}
