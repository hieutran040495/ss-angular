import { Deserializable } from 'shared/interfaces/deserializable';
import { RES_CREATE_BY, RESV_TYPE } from 'shared/enums/reservation';
import { ReservationNotify } from './reservation-notify';

export interface DataNotifyInput {
  code: string;
  created_at?: string;
  created_by?: string;
  reservation_id?: number;
  reservation_start_at?: string;
  resv_type?: string;
  duration?: number;
  phone?: string;
  client_user?: any;
  reservation?: Partial<ReservationNotify>;
}

export class DataNotify implements Deserializable<DataNotifyInput>, DataNotify {
  code: string;
  created_at: string;
  created_by: string;
  reservation_id: number;
  reservation_start_at: string;
  resv_type: string;
  duration: number;
  phone?: string;
  client_user?: any;
  reservation?: Partial<ReservationNotify>;

  get duration_display(): string {
    if (!this.duration) {
      return '0分';
    }
    return `${this.duration}分`;
  }

  get isWalkIn(): boolean {
    return this.resv_type === RESV_TYPE.WALK_IN;
  }

  get isCreateByUser(): boolean {
    return this.created_by === RES_CREATE_BY.USER;
  }

  get isCreateByClient(): boolean {
    return this.created_by === RES_CREATE_BY.CLIENT;
  }

  get isCreateByCMS(): boolean {
    return this.created_by === RES_CREATE_BY.CMS;
  }

  get isCreateByApp(): boolean {
    return this.created_by === RES_CREATE_BY.ORDER;
  }

  deserialize(input: Partial<DataNotifyInput>): DataNotify {
    Object.assign(this, input);
    if (input.reservation) {
      this.reservation = new ReservationNotify().deserialize(input.reservation);
    }
    return this;
  }
}
