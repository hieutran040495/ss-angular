import { Deserializable } from 'shared/interfaces/deserializable';
import { NotifyConfig } from './notify-config';

export interface NotifySettingInput {
  receive_call: Partial<NotifyConfig>;
  resv_create: Partial<NotifyConfig>;
  resv_update: Partial<NotifyConfig>;
  before_resv_end: Partial<NotifyConfig>;
  before_resv_start: Partial<NotifyConfig>;
}

export class NotifySetting implements Deserializable<NotifySettingInput>, NotifySetting {
  receive_call: Partial<NotifyConfig>;
  resv_create: Partial<NotifyConfig>;
  resv_update: Partial<NotifyConfig>;
  before_resv_end: Partial<NotifyConfig>;
  before_resv_start: Partial<NotifyConfig>;

  constructor() {
    this.receive_call = new NotifyConfig();
    this.resv_create = new NotifyConfig();
    this.resv_update = new NotifyConfig();
    this.before_resv_end = new NotifyConfig();
    this.before_resv_start = new NotifyConfig();
  }

  deserialize(input: Partial<NotifySettingInput>): NotifySetting {
    if (input.receive_call) {
      this.receive_call = new NotifyConfig().deserialize(input.receive_call);
    }
    if (input.resv_create) {
      this.resv_create = new NotifyConfig().deserialize(input.resv_create);
    }
    if (input.resv_update) {
      this.resv_update = new NotifyConfig().deserialize(input.resv_update);
    }
    if (input.before_resv_end) {
      this.before_resv_end = new NotifyConfig().deserialize(input.before_resv_end);
    }
    if (input.before_resv_start) {
      this.before_resv_start = new NotifyConfig().deserialize(input.before_resv_start);
    }
    return this;
  }

  formDataSettingJSON() {
    return {
      receive_call: this.receive_call.formDataJSON(),
      resv_create: this.resv_create.formDataJSON(),
      resv_update: this.resv_update.formDataJSON(),
      before_resv_end: this.before_resv_end.formDataJSON(),
      before_resv_start: this.before_resv_start.formDataJSON(),
    };
  }
}
