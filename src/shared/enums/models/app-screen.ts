import { Deserializable } from 'shared/interfaces/deserializable';
import { APP_PATTERN } from 'shared/enums/maintenance';

export interface AppScreenInput {
  id: number;
  app_id: number;
  name: string;
  pattern: number;
}

export class AppScreen implements Deserializable<AppScreen>, AppScreenInput {
  id: number;
  app_id: number;
  name: string;
  pattern: number;
  get isNotMaintenance(): boolean {
    return this.pattern === APP_PATTERN.PATTERN_0;
  }
  constructor() {}

  deserialize(input: Partial<AppScreenInput>): AppScreen {
    Object.assign(this, input);
    return this;
  }
}
