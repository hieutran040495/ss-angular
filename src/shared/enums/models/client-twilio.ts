import { Deserializable } from 'shared/interfaces/deserializable';

export interface ClientTwilioInput {
  id?: number;
  account_sid?: string;
  auth_token?: string;
  twilio_phone_number?: string;
}

export class ClientTwilio implements Deserializable<ClientTwilio> {
  account_sid: string;
  auth_token: string;
  twilio_phone_number: string;

  deserialize(input: Partial<{}>): ClientTwilio {
    if (!input) {
      return this;
    }

    Object.assign(this, input);
    return this;
  }

  /**
   * toJSON
   */
  public toJSON() {
    return {
      account_sid: this.account_sid,
      auth_token: this.auth_token,
    };
  }
}
