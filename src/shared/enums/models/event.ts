import { Deserializable } from 'shared/interfaces/deserializable';
import { NOTIFY_CODE } from 'shared/enums/code-notify';

export interface PusherEventInput {
  id: number;
  code: string;
  type: string;
  reservation_id?: number;
  save_db?: boolean;
  phone?: string;
  channels?: string[];
  notifications_count: number;
}
export class PusherEvent implements Deserializable<PusherEvent>, PusherEventInput {
  id: number;
  code: string;
  type: string;
  reservation_id: number;
  save_db: boolean;
  phone: string;
  channels: string[];
  notifications_count: number;

  get hasNotification(): boolean {
    return Object.values(NOTIFY_CODE).indexOf(this.code) >= 0;
  }

  get isIncomingCall(): boolean {
    return this.channels && this.channels.indexOf('onesignal') !== -1;
  }

  get isReloadNotify(): boolean {
    return this.channels && this.channels.indexOf('database') !== -1;
  }

  get isReloadCalendar(): boolean {
    return this.channels && this.channels.indexOf('broadcast') !== -1;
  }

  get isCodeKeyWord(): boolean {
    return this.code === NOTIFY_CODE.KEYWORD_CREATE || this.code === NOTIFY_CODE.KEYWORD_UPDATE;
  }

  get isTaxUpdate(): boolean {
    return this.code === NOTIFY_CODE.TAX_UPDATE;
  }

  get isImportResComplete(): boolean {
    return this.code === NOTIFY_CODE.IMPORT_RESV_COMPLETE;
  }

  deserialize(input: Partial<PusherEventInput>): PusherEvent {
    Object.assign(this, input);
    return this;
  }
}
