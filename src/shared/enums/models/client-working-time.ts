import { Deserializable } from 'shared/interfaces/deserializable';
import * as moment from 'moment';
import * as pickBy from 'lodash/pickBy';

export interface ClientWorkingTimeInput {
  client_id: number;
  end_at: string;
  start_at: string;
  weekday: number;
}

export class ClientWorkingTime implements Deserializable<ClientWorkingTime>, Partial<ClientWorkingTimeInput> {
  client_id: number;
  end_at: string;
  start_at: string;
  get display(): string {
    return `${moment(this.start_at, 'HH:mm:ss').format('HH:mm')} ~ ${moment(this.end_at, 'HH:mm:ss').format('HH:mm')}`;
  }
  weekday: number;
  error: string;

  constructor() {}

  deserialize(input: Partial<ClientWorkingTimeInput>): ClientWorkingTime {
    if (!input) {
      return;
    }
    Object.assign(this, input);

    return this;
  }

  toJSON() {
    return pickBy(this, (value, key) => {
      return ['client_id', 'start_at', 'end_at'].indexOf(key) >= 0 && value;
    });
  }
}
