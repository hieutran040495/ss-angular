import { Deserializable } from 'shared/interfaces/deserializable';
import { TAX_APPLY_TYPE, TAX_ROUND } from 'shared/enums/tax';
import { Menu, MenuInput } from './menu';
import { Course, CourseInput } from './course';
import { MenuBuffet, MenuBuffetInput } from './menu-buffet';

export interface TaxInput {
  id: number;
  client_id: number;
  name: string;
  is_active: boolean;
  ratio: string;
  round: string;
  tax_type: string;
  apply_type: string;
  is_apply_all: boolean;
  items: MenuInput[];
  courses: CourseInput[];
  buffets: MenuBuffetInput[];
}

export interface TaxOutput {
  id?: number;
  is_active: boolean;
  round: string;
  tax_type: string;
  is_apply_all: boolean;
  item_ids?: number[];
  buffet_ids?: number[];
  course_ids?: number[];
}

export class Tax implements Deserializable<Tax>, TaxInput {
  id: number;
  client_id: number;
  name: string;
  is_active: boolean;
  get badgeClass(): string {
    return this.is_active ? 'cas-badge--primary' : 'badge-secondary';
  }
  get statusString(): string {
    return this.is_active ? '適用済' : '未適用';
  }
  ratio: string;
  round: string;
  get roundString(): string {
    return this.round === TAX_ROUND.UP ? '切り上げ' : this.round === TAX_ROUND.OFF ? '四捨五入' : '切り捨て';
  }
  tax_type: string;
  get isTaxTypeIn(): boolean {
    return this.tax_type === TAX_APPLY_TYPE.IN;
  }
  get applyTypeStr(): string {
    return this.isTaxTypeIn ? '内税方式' : '外税方式';
  }
  apply_type: string;
  get isEatAtStore(): boolean {
    return this.apply_type === TAX_APPLY_TYPE.IN;
  }
  get isTakeAway(): boolean {
    return this.apply_type === TAX_APPLY_TYPE.OUT;
  }
  get isTaxNoUsed(): boolean {
    return this.apply_type === TAX_APPLY_TYPE.NOT_USE;
  }
  get isOrder(): number {
    return this.isEatAtStore ? 1 : this.isTakeAway ? 2 : 3;
  }
  items: Menu[];
  courses: CourseInput[];
  buffets: MenuBuffetInput[];
  get allItems(): any {
    return [].concat(this.items, this.buffets, this.courses);
  }
  is_apply_all: boolean;

  deserialize(input: Partial<TaxInput>): Tax {
    Object.assign(this, input);

    if (input.items) {
      this.items = input.items.map((item: MenuInput) => new Menu().deserialize({ ...item, is_tax: true }));
    }

    if (input.courses) {
      this.courses = input.courses.map((item: CourseInput) => new Course().deserialize({ ...item, is_tax: true }));
    }

    if (input.buffets) {
      this.buffets = input.buffets.map((item: MenuBuffetInput) =>
        new MenuBuffet().deserialize({ ...item, is_tax: true }),
      );
    }

    return this;
  }

  formData(): TaxOutput {
    const result: TaxOutput = {
      is_active: this.is_active,
      round: this.round,
      tax_type: this.tax_type,
      is_apply_all: this.is_apply_all,
    };

    if (this.id) {
      result.id = this.id;
    }

    if (this.items) {
      result.item_ids = this.items.map((item: Menu) => item.id);
    }

    if (this.courses) {
      result.course_ids = this.courses.map((item: Course) => item.id);
    }

    if (this.buffets) {
      result.buffet_ids = this.buffets.map((item: MenuBuffet) => item.id);
    }

    return result;
  }
}
