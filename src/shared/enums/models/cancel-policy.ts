import { Deserializable } from 'shared/interfaces/deserializable';

export interface CancelPolicyInput {
  cancel_policy: string;
  client_id: number;
  id: number;
}

export class CancelPolicy implements Deserializable<CancelPolicy>, CancelPolicyInput {
  cancel_policy: string;
  client_id: number;
  id: number;

  deserialize(input: Partial<CancelPolicyInput>): CancelPolicy {
    if (input) {
      Object.assign(this, input);
    }
    return this;
  }
}
