import { Deserializable } from 'shared/interfaces/deserializable';
import * as pickBy from 'lodash/pickBy';

export enum TABLE_LAYOUT_TYPE {
  CIRCLE = 'Circle',
  RECT = 'Rect',
  SQUARE = 'Square',
}

export interface TableLayoutInput {
  x: number;
  y: number;
  rotation: number;
  floor: number;
  name: string;
  type: string;
}

export class TableLayout implements Deserializable<TableLayout>, TableLayoutInput {
  x: number;
  y: number;
  floor: number = 1;
  name: string = '';
  type: string;

  private _rotation: number = 0;
  get rotation(): number {
    const temp = this._rotation % 360;

    return temp >= 0 ? temp : temp + 360;
  }
  set rotation(v: number) {
    this._rotation = v || 0;
  }

  get hasPosition(): boolean {
    return this.x !== undefined && this.x !== null && (this.y !== undefined && this.y !== null);
  }

  constructor() {
    this.type = TABLE_LAYOUT_TYPE.RECT;
  }

  deserialize(input: Partial<TableLayoutInput>): TableLayout {
    Object.assign(this, input);

    return this;
  }

  toJSON() {
    return pickBy(this, (value, key) => {
      return ['x', 'y', 'rotation', 'floor', 'type'].indexOf(key) >= 0 && (value || value === 0);
    });
  }
}
