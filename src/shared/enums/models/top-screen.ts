import { Deserializable } from 'shared/interfaces/deserializable';
import { TopImage } from './top-image';
import { APP_SETTING } from 'shared/enums/rich-menu';

export interface TopScreenInput {
  id: number;
  top_screen: Partial<TopImage>;
  top_screens: Partial<TopImage>[];
}
export class TopScreen implements Deserializable<TopScreen>, TopScreenInput {
  id: number;
  top_screen: TopImage;
  top_screens: TopImage[];

  constructor() {
    this.top_screens = [];
  }

  deserialize(input: Partial<TopScreenInput>): TopScreen {
    Object.assign(this, input);
    if (input.top_screen) {
      this.top_screen = new TopImage().deserialize(input.top_screen);
    }
    if (input.top_screens) {
      this.top_screens = input.top_screens.map((item) => new TopImage().deserialize(item));
    }
    return this;
  }

  toJSON() {
    return {
      settings: this.top_screens.map((item, index) => {
        return {
          page: item.page,
          pos_x: item.pos_x || 0,
          pos_y: item.pos_y || 0,
          ratio: 1,
          text_color: item.text_color,
          bg_color: item.bg_color,
          font_family: item.font_family,
          image_path: item.image_path,
        };
      }),
      type: APP_SETTING.ORDER,
    };
  }
}
