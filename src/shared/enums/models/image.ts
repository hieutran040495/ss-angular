import { Deserializable } from 'shared/interfaces/deserializable';

export interface ImageInput {
  compress_url: string;
  mime_type: string;
  id: number;
  url: string;
  name: string;
  extension: string;
  size: number;
  width: number;
  height: number;
  thumb_url: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
}

export class Image implements Deserializable<Image>, ImageInput {
  compress_url: string;
  mime_type: string;
  id: number;
  url: string;
  name: string;
  extension: string;
  size: number;
  width: number;
  height: number;
  thumb_url: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;

  deserialize(input: Partial<ImageInput>): Image {
    Object.assign(this, input);
    return this;
  }
}
