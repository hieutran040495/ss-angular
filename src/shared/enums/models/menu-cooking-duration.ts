import { Deserializable } from 'shared/interfaces/deserializable';
import { MASTER_STATUS } from 'shared/enums/cooking-master';

import * as moment from 'moment';

export interface MenuCookingDurationInput {
  id: number;
  duration: string;
  old_duration?: string;
  ordinal: number;
  status?: string;
  client_id: number;
  created_at?: string;
  updated_at?: string;
}

export class MenuCookingDuration implements Deserializable<MenuCookingDuration>, MenuCookingDurationInput {
  id: number;
  duration: string;
  get duration_display(): string {
    if (!this.duration) {
      return ``;
    }
    return moment(this.duration, 'HH:mm:ss').format('mm:ss');
  }
  old_duration: string;
  get old_duration_display(): string {
    if (!this.old_duration) {
      return ``;
    }
    return moment(this.old_duration, 'HH:mm:ss').format('mm:ss');
  }
  ordinal: number;
  status: string;
  get status_class(): string {
    switch (this.status) {
      case MASTER_STATUS.CREATE:
      case MASTER_STATUS.UPDATE:
        return `master--change`;
      case MASTER_STATUS.REMOVE:
        return `master--remove`;
      case MASTER_STATUS.NONE:
        return ``;
      default:
        break;
    }
  }
  client_id: number;
  created_at: string;
  get created_at_display(): string {
    return moment(this.created_at).format('YYYY/MM/DD HH:mm');
  }
  updated_at?: string;
  get updated_at_display(): string {
    return moment(this.updated_at).format('YYYY/MM/DD HH:mm');
  }

  deserialize(input: Partial<MenuCookingDurationInput>): MenuCookingDuration {
    Object.assign(this, input);
    return this;
  }
}
