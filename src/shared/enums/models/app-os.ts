import { Deserializable } from 'shared/interfaces/deserializable';

export interface AppOsInput {
  app_id: number;
  comment: string;
  created_at: string;
  id: number;
  message: string;
  name: string;
  os: string;
  update_mode: string;
  updated_at: string;
  url: string;
}

export class AppOS implements Deserializable<AppOS>, AppOsInput {
  app_id: number;
  comment: string;
  created_at: string;
  id: number;
  message: string;
  name: string;
  os: string;
  update_mode: string;
  updated_at: string;
  url: string;

  deserialize(input: Partial<AppOsInput>): AppOS {
    Object.assign(this, input);
    return this;
  }
}
