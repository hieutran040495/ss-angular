import { Deserializable } from 'shared/interfaces/deserializable';
import { Scale } from './scale';
import { MODAL_SIZE } from 'shared/enums/konva-size-config';

export interface ScreenSizeInput {
  width: number;
  height: number;
  img_width: number;
  img_height: number;
}

export class ScreenSize implements Deserializable<ScreenSize>, ScreenSizeInput {
  width: number;
  height: number;
  img_width: number;
  img_height: number;
  scale: Scale;
  old_scale: Scale;
  get screenWidth(): number {
    return this.width * this.scale.scale_x;
  }
  get screenHeight(): number {
    return this.height * this.scale.scale_y;
  }

  constructor() {
    this.scale = new Scale();
    this.old_scale = new Scale();
  }

  deserialize(input: Partial<ScreenSize>): ScreenSize {
    Object.assign(this, input);
    this.initScale();
    return this;
  }

  initScale() {
    this.old_scale.deserialize({
      scale_x: Number(this.scale.scale_x),
      scale_y: Number(this.scale.scale_y),
    });
    this.scale.deserialize({
      scale_x: (MODAL_SIZE.WIDTH / this.width) * (this.img_width / MODAL_SIZE.WIDTH),
      scale_y: (MODAL_SIZE.HEIGHT / this.height) * (this.img_height / MODAL_SIZE.HEIGHT),
    });
  }
}
