import { Deserializable } from 'shared/interfaces/deserializable';
import { ClientInput, Client } from './client';
import { TableTypeInput, TableType } from './table-type';
import { CurrentReservation } from './current_reservation';

export interface TableInput {
  id: number;
  client: ClientInput;
  name: string;
  stage_name?: string;
  quantity: number;
  type?: TableTypeInput;
  can_smoke: boolean;
  reservations_count?: number;
  current_reservation?: CurrentReservation;
  created_at?: string;
  updated_at?: string;
  deleted_at?: string;
  code: string;
  group: string;
  ordinal_number: number;
  swapClassActive?: string;
}
export interface TableOutput {
  id?: number;
  name: string;
  quantity: number;
  type_id: number;
  can_smoke: boolean;
  code: string;
  ordinal_number: number;
}

export class Table implements Deserializable<Table>, TableInput {
  id: number;
  client: Client;
  name: string;
  get nameTruncate(): string {
    return this.name.toEllipsisWithCountLetters(8).string;
  }
  stage_name: string;
  quantity: number;
  get seat(): number {
    return Math.ceil(this.quantity / 2);
  }
  get seatArr(): number[] {
    return Array(this.seat).fill(0);
  }
  get quantityString(): string {
    return `${this.quantity}人席`;
  }
  type: TableType;
  can_smoke: boolean;
  get can_smoke_string(): string {
    return this.can_smoke ? '喫煙' : '禁煙';
  }
  reservations_count: number;
  get total_reservation(): string {
    if (!+this.reservations_count) {
      return '0件';
    }
    return `${(+this.reservations_count).format()}件`;
  }
  current_reservation: CurrentReservation;
  get hasReservation(): boolean {
    return !!this.current_reservation;
  }
  created_at: string;
  updated_at: string;
  deleted_at: string;
  has_layout: boolean;
  code: string;
  group: string;

  get isStartResv() {
    return !this.hasReservation || (this.current_reservation && !!this.current_reservation.confirm_start_at);
  }

  ordinal_number: number;
  swapClassActive: string;

  constructor() {
    this.can_smoke = false;
    this.type = new TableType();
  }

  deserialize(input: Partial<TableInput>): Table {
    if (input) {
      Object.assign(this, input);
      if (input.client) {
        this.client = new Client().deserialize(input.client);
      }

      if (input.type) {
        this.type = new TableType().deserialize(input.type);
      }
      if (input.name) {
        this.stage_name = input.name.length > 10 ? `${input.name.substring(0, 9)}...` : input.name;
      }
      if (input.current_reservation) {
        this.current_reservation = new CurrentReservation().deserialize(input.current_reservation);
      }
    }
    return this;
  }

  formDataString(): TableOutput {
    const data: TableOutput = {
      name: this.name,
      quantity: this.quantity || 0,
      can_smoke: this.can_smoke,
      type_id: this.type.id,
      code: this.code,
      ordinal_number: this.ordinal_number,
    };

    if (this.id) {
      data.id = this.id;
    }

    return data;
  }
}
