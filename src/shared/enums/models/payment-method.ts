import { Deserializable } from 'shared/interfaces/deserializable';

export interface PaymentMethodInput {
  id: number;
  name: string;
  active: boolean;
  payment_type: string;
}

export class PaymentMethod implements Deserializable<PaymentMethod>, PaymentMethodInput {
  id: number;
  name: string;
  active: boolean;
  payment_type: string;

  constructor() {
    this.deserialize({
      active: false,
    });
  }

  deserialize(input: Partial<PaymentMethodInput>): PaymentMethod {
    Object.assign(this, input);
    return this;
  }
}
