import { Deserializable } from 'shared/interfaces/deserializable';
import { DataNotify } from './data-notify';
import { Reservation } from './reservation';
import { NOTIFY_CODE } from 'shared/enums/code-notify';
import * as moment from 'moment';
import { ICON_NOTIFY } from 'shared/enums/icon-notify';
import { GOURMET_NAME, GOURMET_LINK } from 'shared/enums/gourmet-site';

export interface NotifyInput {
  id: string;
  code: string;
  data: Partial<DataNotify>;
  reservation?: Partial<Reservation>;
  read_at?: string;
  created_at?: string;
  seen: boolean;
}

export class Notify implements Deserializable<NotifyInput>, Notify {
  id: string;
  code: string;
  data: DataNotify;
  reservation: Reservation;
  read_at: string;
  seen: boolean;
  get is_unread(): boolean {
    return this.read_at === null;
  }
  created_at: string;
  get created_at_display(): string {
    return moment(this.created_at).format('YYYY[年]MMMDo HH:mm');
  }

  get title(): string {
    switch (this.code) {
      case NOTIFY_CODE.RESV_CREATE:
        if (
          (this.data.isCreateByClient || this.data.isCreateByUser || this.data.isCreateByCMS) &&
          !this.data.isWalkIn
        ) {
          return `新しい予約（${this.reservation.start_end_time_string}）が入りました`;
        }
        // receiver notify msg when user create reservation walk-in or order app
        return `ウォークイン（${this.reservation.start_at_display_intl}~）が入りました`;
      case NOTIFY_CODE.RESV_UPDATE:
        return `予約(${this.reservation.start_end_time_string}）が更新されました`;
      case NOTIFY_CODE.RESV_CANCEL:
        return `予約（${this.reservation.start_end_time_string}）がキャンセルされました`;
      case NOTIFY_CODE.BEFORE_RESV_START:
        return `予約（${this.reservation.start_end_time_string}）開始時間${this.data.duration_display}前になりました。`;
      case NOTIFY_CODE.BEFORE_RESV_END:
        return `予約（${this.reservation.start_end_time_string}）終了時間${this.data.duration_display}前になりました。`;
      case NOTIFY_CODE.INCOMING_CALL:
        return `着信（${this.data.phone}）がありました`;
      case NOTIFY_CODE.IMPORT_RESV_FAIL:
        return `${GOURMET_NAME[this.data.reservation.serviceable_type]}の予約（予約日時 ${
          this.data.reservation.start_end_date_time_display
        } ${this.data.client_user.name}様）が自動登録できませんでした`;
      case NOTIFY_CODE.IMPORT_RESV_SUCCESS:
        return `${GOURMET_NAME[this.data.reservation.serviceable_type]}の予約（予約日時 ${
          this.data.reservation.start_end_date_time_display
        } ${this.data.client_user.name}様）が自動登録されました`;
      default:
        break;
    }
  }

  get link(): string {
    switch (this.code) {
      case NOTIFY_CODE.RESV_CREATE:
      case NOTIFY_CODE.RESV_UPDATE:
      case NOTIFY_CODE.RESV_CANCEL:
      case NOTIFY_CODE.BEFORE_RESV_START:
      case NOTIFY_CODE.BEFORE_RESV_END:
      case NOTIFY_CODE.IMPORT_RESV_SUCCESS:
        return `/reservations`;
      case NOTIFY_CODE.INCOMING_CALL:
        return `/history-call`;
      case NOTIFY_CODE.IMPORT_RESV_FAIL:
        return GOURMET_LINK[this.data.reservation.serviceable_type];
      default:
        break;
    }
  }

  get icon(): string {
    switch (this.code) {
      case NOTIFY_CODE.RESV_CREATE || NOTIFY_CODE.RESV_UPDATE:
        return ICON_NOTIFY.ICON_CALENDAR;
      case NOTIFY_CODE.INCOMING_CALL:
        return ICON_NOTIFY.ICON_PHONE;
      case NOTIFY_CODE.RESV_CANCEL:
        return ICON_NOTIFY.ICON_WARNING;
      default:
        return '';
    }
  }

  get query_params(): object {
    switch (this.code) {
      case NOTIFY_CODE.RESV_CREATE:
      case NOTIFY_CODE.RESV_UPDATE:
      case NOTIFY_CODE.RESV_CANCEL:
      case NOTIFY_CODE.BEFORE_RESV_START:
      case NOTIFY_CODE.BEFORE_RESV_END:
        return {
          queryParams: {
            reservation_id: this.reservation.id,
            start_at: this.reservation.start_at,
          },
          skipLocationChange: true,
        };
      case NOTIFY_CODE.INCOMING_CALL:
        return {
          queryParams: {
            phone: this.data.phone,
          },
        };
      default:
        break;
    }
  }

  get is_import_resv_fail(): boolean {
    return this.code === NOTIFY_CODE.IMPORT_RESV_FAIL;
  }

  constructor() {
    this.deserialize({
      reservation: new Reservation().deserialize({}),
    });
  }

  deserialize(input: Partial<NotifyInput>): Notify {
    Object.assign(this, input);
    if (input.data) {
      this.data = new DataNotify().deserialize(input.data);
    }
    if (input.reservation) {
      this.reservation = new Reservation().deserialize(input.reservation);
    }
    return this;
  }
}
