import { Deserializable } from 'shared/interfaces/deserializable';
import { Table } from './table';
import { ClientUserInput, ClientUser } from './client-user';
import * as moment from 'moment';
import { RESV_STATUS } from 'shared/enums/reservation';

export interface WalkinInput {
  id: number;
  tables: Table[];
  start_date: string;
  start_time: string;
  start_at: string;
  end_at: string;
  quantity: number;
  memo: string;
  client_user: ClientUserInput;
  duration: number;
  status: string;
  smp_code?: string;
  reservation_passcode: Partial<ReservationPasscodeOutput>;
}

export interface ReservationPasscodeOutput {
  smp_code: string;
  logged_at: string;
}

export class Walkin implements Deserializable<Walkin>, WalkinInput {
  id: number;
  smp_code: string;
  reservation_passcode: ReservationPasscodeOutput;
  tables: Table[];
  get tables_name_display(): string {
    return this.tables
      .map((table: Table) => {
        return table.code;
      })
      .join('、');
  }
  start_time: string;
  get isStartTimeInPast(): boolean {
    return moment(this.end_at).isBefore(moment());
  }
  start_date: string;
  start_at: string;
  end_at: string;
  quantity: number;
  memo: string;
  client_user: ClientUser;
  duration: number;
  status: string;
  get isInComing(): boolean {
    return this.status === RESV_STATUS.INCOMING;
  }

  get isCanceled(): boolean {
    return this.status === RESV_STATUS.CANCELED;
  }

  get isWorking(): boolean {
    return this.status === RESV_STATUS.WORKING;
  }

  get isFinished(): boolean {
    return this.status === RESV_STATUS.FINISHED;
  }
  get isUnAllowStartWalkin() {
    return this.isStartTimeInPast || this.isWorking || this.isFinished;
  }

  // Current date and time pass
  // doesn't edit duration
  get isCurrentDateAndTimePass(): boolean {
    const currentDate = moment();
    return (
      moment(this.end_at).isSame(currentDate, 'day') &&
      (currentDate.isAfter(this.end_at, 'hour') || currentDate.isAfter(this.end_at, 'minute'))
    );
  }

  // Date Pass
  get isDatePass(): boolean {
    const resvDateSelected: any = moment(`${this.start_date} ${this.start_time}`, 'YYYY/MM/DD HH:mm:ss');
    let currentDate = moment();

    if (resvDateSelected.hour() > 6) {
      currentDate = currentDate.add(-1, 'days');
    }

    return moment(resvDateSelected).isBefore(currentDate, 'days');
  }

  paid_at: string;
  get is_paid(): boolean {
    return !!this.paid_at;
  }

  constructor() {
    this.client_user = new ClientUser();
    this.tables = [];
  }

  deserialize(input: Partial<WalkinInput>): Walkin {
    Object.assign(this, input);
    if (input.client_user) {
      this.client_user = new ClientUser().deserialize(input.client_user);
    }

    if (input.reservation_passcode) {
      this.smp_code = this.reservation_passcode.smp_code;
    }

    return this;
  }

  formData() {
    const dateTime: moment.Moment = this.checkDate();

    return {
      table_ids: this.tables.map((item) => item.id),
      start_at: dateTime.toISOString(),
      end_at: dateTime.add(this.duration, 'minute').toISOString(),
      duration: this.duration,
      quantity: this.quantity,
    };
  }

  private checkDate(): moment.Moment {
    const date = moment();
    const time = moment.duration(this.start_time);
    if (date.hour() < 6) {
      date.subtract(1, 'day');
    }
    if (time.asHours() < 6) {
      time.add(24, 'hour');
    }

    return date.set({
      hour: Math.floor(time.asHours()),
      minute: time.minutes(),
      second: 0,
      millisecond: 0,
    });
  }
}
