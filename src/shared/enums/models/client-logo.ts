import { Deserializable } from 'shared/interfaces/deserializable';

export interface ClientLogoInput {
  compress_url: string;
  created_at: string;
  extension: string;
  height: number;
  id: number;
  mime_type: string;
  name: string;
  size: number;
  thumb_url: string;
  updated_at: string;
  url: string;
  width: number;
}

export class ClientLogo implements Deserializable<ClientLogo>, ClientLogoInput {
  compress_url: string;
  created_at: string;
  extension: string;
  height: number;
  id: number;
  mime_type: string;
  name: string;
  size: number;
  thumb_url: string;
  updated_at: string;
  url: string;
  width: number;

  deserialize(input: Partial<ClientLogoInput>): ClientLogo {
    Object.assign(this, input);

    return this;
  }
}
