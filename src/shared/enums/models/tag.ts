import { Deserializable } from 'shared/interfaces/deserializable';

export interface TagInput {
  id: number;
  name: string;
  isCheck?: boolean;
  created_at: string;
  updated_at: string;
  deleted_at: string;
}

export class Tag implements Deserializable<Tag>, TagInput {
  id: number;
  name: string;
  get nameTruncate(): string {
    return this.name.toEllipsisWithCountLetters(50).string;
  }
  isCheck: boolean;
  created_at: string;
  updated_at: string;
  deleted_at: string;

  deserialize(input: Partial<TagInput>): Tag {
    Object.assign(this, input);
    return this;
  }
}
