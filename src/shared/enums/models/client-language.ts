import { Deserializable } from 'shared/interfaces/deserializable';

export interface ClientLanguageInput {
  id?: number;
  client_id?: number;
  allow_en?: boolean;
  allow_ko?: boolean;
  allow_zh_hans?: boolean;
  allow_zh_hant?: boolean;
}

export class ClientLanguage implements Deserializable<ClientLanguage> {
  client_id: number;
  allow_en: boolean = false;
  allow_ko: boolean = false;
  allow_zh_hans: boolean = false;
  allow_zh_hant: boolean = false;

  deserialize(input: Partial<{}>): ClientLanguage {
    if (!input) {
      return this;
    }

    Object.assign(this, input);
    return this;
  }

  /**
   * toJSON
   */
  public toJSON() {
    return {
      allow_en: this.allow_en,
      allow_ko: this.allow_ko,
      allow_zh_hans: this.allow_zh_hans,
      allow_zh_hant: this.allow_zh_hant,
    };
  }
}
