import { Deserializable } from 'shared/interfaces/deserializable';

export interface StyleInput {
  backgroundImage?: string;
  backgroundColor?: string;
  width: string;
}

export class Style implements Deserializable<Style>, Partial<StyleInput> {
  backgroundImage?: string;
  backgroundColor?: string;
  width: string;

  deserialize(input: Partial<StyleInput>): Style {
    if (!input) {
      return;
    }

    Object.assign(this, input);
    return this;
  }

  get styleJson(): any {
    return {
      backgroundColor: this.backgroundColor,
      backgroundImage: this.backgroundImage,
      width: this.width,
    };
  }
}
