import { Deserializable } from 'shared/interfaces/deserializable';
import { HISTORY_RESV_STATUS } from 'shared/enums/reservation';
import { ClientMemberInput, ClientMember } from './client-member';
import { ReservationInput, Reservation } from './reservation';
import * as moment from 'moment';

export interface ReservationHistoryInput {
  client_id: number;
  reservation_id: number;
  client_member_id: number;
  status: string;
  client_member: ClientMemberInput;
  reservation: ReservationInput;
  created_at: string;
  updated_at: string;
}

export class ReservationHistory implements Deserializable<ReservationHistory>, ReservationHistoryInput {
  client_id: number;
  reservation_id: number;
  client_member_id: number;
  client_member: ClientMember;
  reservation: Reservation;
  status: string;
  get status_string(): string {
    switch (this.status) {
      case HISTORY_RESV_STATUS.CREATE:
        return '新規追加';
      case HISTORY_RESV_STATUS.DELETE:
        return '削除';
      case HISTORY_RESV_STATUS.CANCEL:
        return 'キャンセル';
      case HISTORY_RESV_STATUS.UPDATE:
        return '変更';
      default:
        return '新規追加';
    }
  }
  created_at: string;
  updated_at: string;
  get updated_at_display(): string {
    return moment(this.created_at).format('YYYY/MM/DD HH:mm');
  }
  get updated_at_date_display(): string {
    return moment(this.created_at).format('YYYY/MM/DD');
  }
  get updated_at_time_display(): string {
    return moment(this.created_at).format('HH:mm');
  }

  constructor() {
    this.reservation = new Reservation();
  }

  deserialize(input: Partial<ReservationHistoryInput>): ReservationHistory {
    Object.assign(this, input);

    if (input.reservation) {
      this.reservation = new Reservation().deserialize(input.reservation);
    }

    if (input.client_member) {
      this.client_member = new ClientMember().deserialize(input.client_member);
    }

    return this;
  }
}
