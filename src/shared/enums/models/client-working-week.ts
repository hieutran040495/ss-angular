import { Deserializable } from 'shared/interfaces/deserializable';

import * as moment from 'moment';
import { ClientWorkingTimeInput, ClientWorkingTime } from './client-working-time';
import { CLIENT_WORKING_WEEK, WORKING_TYPE } from 'shared/constants/client-working';
import JapaneseHolidays from 'japanese-holidays';

export interface ClientWorkingTimeWeekInput {
  [0]: ClientWorkingTimeInput[];
  [1]: ClientWorkingTimeInput[];
  [2]: ClientWorkingTimeInput[];
  [3]: ClientWorkingTimeInput[];
  [4]: ClientWorkingTimeInput[];
  [5]: ClientWorkingTimeInput[];
  [6]: ClientWorkingTimeInput[];
  // ngày lễ inerface
  [7]: ClientWorkingTimeInput[];
}

export interface ClientWorkingTimeWeekChanging {
  durations: ClientWorkingTime[];
  /**
   * WORKING_TYPE.DAY_OFF => 0
   * * WORKING_TYPE.FULL_DAY => 1
   * * WORKING_TYPE.BREAKING => 2
   */
  type: WORKING_TYPE.DAY_OFF | WORKING_TYPE.FULL_DAY | WORKING_TYPE.BREAKING;
}

export class ClientWorkingTimeWeek implements Deserializable<ClientWorkingTimeWeek> {
  [0]: ClientWorkingTimeWeekChanging = {
    durations: [
      new ClientWorkingTime().deserialize({
        start_at: '06:00:00',
        end_at: '05:59:00',
      }),
    ],
    type: WORKING_TYPE.FULL_DAY,
  };
  [1]: ClientWorkingTimeWeekChanging = {
    durations: [
      new ClientWorkingTime().deserialize({
        start_at: '06:00:00',
        end_at: '05:59:00',
      }),
    ],
    type: WORKING_TYPE.FULL_DAY,
  };
  [2]: ClientWorkingTimeWeekChanging = {
    durations: [
      new ClientWorkingTime().deserialize({
        start_at: '06:00:00',
        end_at: '05:59:00',
      }),
    ],
    type: WORKING_TYPE.FULL_DAY,
  };
  [3]: ClientWorkingTimeWeekChanging = {
    durations: [
      new ClientWorkingTime().deserialize({
        start_at: '06:00:00',
        end_at: '05:59:00',
      }),
    ],
    type: WORKING_TYPE.FULL_DAY,
  };
  [4]: ClientWorkingTimeWeekChanging = {
    durations: [
      new ClientWorkingTime().deserialize({
        start_at: '06:00:00',
        end_at: '05:59:00',
      }),
    ],
    type: WORKING_TYPE.FULL_DAY,
  };
  [5]: ClientWorkingTimeWeekChanging = {
    durations: [
      new ClientWorkingTime().deserialize({
        start_at: '06:00:00',
        end_at: '05:59:00',
      }),
    ],
    type: WORKING_TYPE.FULL_DAY,
  };
  [6]: ClientWorkingTimeWeekChanging = {
    durations: [
      new ClientWorkingTime().deserialize({
        start_at: '06:00:00',
        end_at: '05:59:00',
      }),
    ],
    type: WORKING_TYPE.FULL_DAY,
  };
  // prop for ngày lễ
  [7]: ClientWorkingTimeWeekChanging = {
    durations: [],
    type: WORKING_TYPE.DAY_OFF,
  };

  constructor() {}

  deserialize(input: ClientWorkingTimeWeek): ClientWorkingTimeWeek {
    if (!input) {
      return;
    }

    Object.keys(input).forEach((key) => {
      this[key] = {
        durations: input[key].map((item) => new ClientWorkingTime().deserialize(item)),
        type: input[key].length,
      };
    });
    return this;
  }

  public getWorkingTime(date: moment.Moment): { start: string; end: string } | null {
    let i = date.weekday();

    if (JapaneseHolidays.isHoliday(date.toDate()) && this[7] && this[7].durations.length === 0) {
      // check setting holiday
      i = 7;
    }

    if (i > 7 || !this[i] || !this[i].durations || this[i].durations.length === 0) {
      return null;
    }

    if (this[i].durations.length === 1) {
      return this._correctTime(this[i].durations[0].start_at, this[i].durations[0].end_at);
    }

    return this._correctTime(this[i].durations[0].start_at, this[i].durations[this[i].durations.length - 1].end_at);
  }

  private _correctTime(start: string, end: string): { start: string; end: string } {
    const startM = moment.duration(start);
    const endM = moment.duration(end);

    if (endM.asMinutes() < startM.asMinutes()) {
      return {
        start: startM.format('HH:mm:ss'),
        end: endM.add(24, 'hour').format('HH:mm:ss'),
      };
    }

    return {
      start: startM.format('HH:mm:ss'),
      end: endM.format('HH:mm:ss'),
    };
  }

  public clientWorkingToArray(): any[] {
    return CLIENT_WORKING_WEEK.map((item) => this[item.weekday]);
  }
}
