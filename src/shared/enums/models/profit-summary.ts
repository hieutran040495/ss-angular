import { Deserializable } from 'shared/interfaces/deserializable';

export interface ProfitSummaryInput {
  phone_total_price: number;
  phone_total_reservations: number;
  total_price: number;
  total_reservations: number;
  web_total_price: number;
  web_total_reservations: number;
  walk_in_total_price: number;
  walk_in_total_reservations: number;
}

export class ProfitSummary implements Deserializable<ProfitSummary>, ProfitSummaryInput {
  phone_total_price: number;
  get phone_total_price_display(): string {
    return this.phone_total_price ? this.phone_total_price.format() : '0';
  }

  phone_total_reservations: number;
  get phone_total_reservations_display(): string {
    return this.phone_total_reservations ? `(${this.phone_total_reservations.format()}本)` : '';
  }

  total_price: number;
  get total_price_display(): string {
    return this.total_price ? this.total_price.format() : '0';
  }

  total_reservations: number;
  get total_reservations_display(): string {
    return this.total_reservations ? `(${this.total_reservations.format()}本)` : '';
  }

  web_total_price: number;
  get web_total_price_display(): string {
    return this.web_total_price ? this.web_total_price.format() : '0';
  }

  web_total_reservations: number;
  get web_total_reservations_display(): string {
    return this.web_total_reservations ? `(${this.web_total_reservations.format()}本)` : '';
  }

  walk_in_total_price: number;
  get walk_in_total_price_display(): string {
    return this.walk_in_total_price ? this.walk_in_total_price.format() : '0';
  }

  walk_in_total_reservations: number;
  get walk_in_total_reservations_display(): string {
    return this.walk_in_total_reservations ? `(${this.walk_in_total_reservations.format()}本)` : '';
  }

  deserialize(input: Partial<ProfitSummaryInput>): ProfitSummary {
    if (input) {
      Object.assign(this, input);
      return this;
    }
  }
}
