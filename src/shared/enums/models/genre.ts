import { Deserializable } from 'shared/interfaces/deserializable';

export interface GenreInput {
  id: number;
  name: string;
}

export class Genre implements Deserializable<Genre>, GenreInput {
  id: number;
  name: string;
  get nameTruncate(): string {
    return this.name.toEllipsisWithCountLetters(50).string;
  }

  deserialize(input: Partial<GenreInput>): Genre {
    Object.assign(this, input);
    return this;
  }
}
