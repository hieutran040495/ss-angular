import { Deserializable } from 'shared/interfaces/deserializable';

export interface ClientSettingInput {
  allow_export_csv: boolean;
  allow_client_printer: boolean;
}

export class ClientSetting implements Deserializable<ClientSetting>, ClientSettingInput {
  allow_export_csv: boolean;
  allow_client_printer: boolean;

  deserialize(input: Partial<ClientSettingInput>): ClientSetting {
    Object.assign(this, input);
    return this;
  }
}
