import { ClientUser, ClientUserInput, ClientUserOutput } from './client-user';
import { BusinessHoursInput } from 'fullcalendar/src/types/input-types';
import { Deserializable } from 'shared/interfaces/deserializable';
import { TableInput, Table } from './table';
import { Menu, MenuInput, MenuOrderOutput } from './menu';
import { Coupon, CouponInput } from './coupon';
import { CourseInput, Course, CourseOrderOutput } from './course';
import { MenuBuffet, MenuBuffetOrderOutput } from './menu-buffet';
import {
  RESV_TYPE,
  RESV_STATUS,
  RESV_PAYMENT_BY_TEXT,
  RESV_STATUS_TEXT,
  RESV_PAYMENT_BY,
  ORDER_PAYMENT_METHOD,
  RES_CREATE_BY,
} from '../enums/reservation';
import { UserInput, User } from './user';
import { ClientMember, ClientMemberInput } from './client-member';
import { ReservationLatestPayment, ReservationLatestPaymentInput } from './reservation-latest-payment';

import * as moment from 'moment';
import { DURATIONS_TEXT, DURATIONS_VALUE } from '../constants/time-setting';
import { ResvKeyword } from './resv-keyword';

export interface ReservationInput {
  id: number;
  quantity: number;
  has_children: boolean;
  total_price: number;
  client_user: Partial<ClientUserInput>;
  table: Partial<TableInput>;
  tables: Partial<TableInput[]>;
  name: string;
  memo?: string;
  reservation_items?: Menu[];
  start_at?: string;
  end_at?: string;
  type: string;
  created_by: string;
  receipt_date: string;
  first_at_store: boolean;
  coupon?: Partial<CouponInput>;
  courses?: Partial<CourseInput>[];
  reservation_courses?: Partial<CourseInput>[];
  reservation_preorder_items?: Partial<CourseInput>[];
  items?: Partial<MenuInput>[];
  buffets?: Partial<MenuBuffet>[];
  user?: Partial<UserInput>;
  client_member?: Partial<ClientMemberInput>;
  status: string;
  payment_by: string;
  service_fee: number;
  service_fee_type: string;
  paid_at: string;
  prepaid: number;
  is_temp: boolean;
  keywords?: ResvKeyword[];
  preorder_paid: number;
  preorder_items_count: number;
  coupon_discount: number;
  duration: number;
  order_payment_method: string;
  receipt_number: number;
  smp_code?: string;
  reservation_passcode: Partial<ReservationPasscodeOutput>;
  wait_for_payment: boolean;
  latest_payment?: Partial<ReservationLatestPaymentInput>;
  receipt_code?: string;
}

export interface ReservationOutput {
  user: ClientUserOutput;
  phone: string;
  start_at: string;
  end_at: string;
  quantity: number;
  memo: string;
  has_children: boolean;
  items?: Partial<MenuInput>[];
  buffets?: Partial<MenuBuffet>[];
  courses?: Partial<CourseInput>[];
  coupon_id?: number;
  table_ids?: number[];
  keywords?: ResvKeyword[];
}

export interface ReservationPasscodeOutput {
  smp_code: string;
  logged_at: string;
}
export interface CheckAvailableTableOutput {
  start_at: string;
  end_at: string;
  table_ids?: number[];
}

export interface TotalPriceOutput {
  reservation_id: number;
  items: MenuOrderOutput[];
  buffets: MenuBuffetOrderOutput[];
  courses: CourseOrderOutput[];
  coupon_id: number;
  quantity: number;
}

export class Reservation implements Deserializable<Reservation>, ReservationInput {
  id: number;
  quantity: number;
  smp_code: string;
  reservation_passcode: ReservationPasscodeOutput;
  wait_for_payment: boolean;
  get totalQuantity(): string {
    return `${+this.quantity}名`;
  }

  has_children: boolean;
  total_price: number;
  get total_price_display(): string {
    if (!this.total_price) {
      return '0';
    }
    return `${this.total_price.format()}`;
  }
  client_user: ClientUser;
  get client_user_display() {
    return this.client_user ? `${this.client_user.name}` : '';
  }
  user?: User;
  client_member?: ClientMember;
  get client_member_display() {
    return this.client_member ? this.client_member.name : 'なし';
  }
  get isCashlessUser(): boolean {
    return !!(this.user && this.user.id);
  }

  table: Table;
  tables: Table[];
  get tables_name_display(): string {
    return this.tables
      .map((table: Table) => {
        return table.code;
      })
      .join('、');
  }
  get tablesQuanlity(): number {
    return (
      +this.quantity -
      this.tables.reduce((accumulator, currentValue: Table) => {
        return accumulator + currentValue.quantity;
      }, 0)
    );
  }
  get isEnoughSeat(): any {
    return this.tables.length !== 0 && this.tablesQuanlity > 0;
  }
  name: string;
  memo: string;
  reservation_items: Menu[];
  first_at_store: boolean;
  start_at: string;
  get start_at_form(): string {
    const duration = moment.duration(this.start_time);
    return moment(this.start_date, 'YYYY/MM/DD')
      .set('milliseconds', duration.asMilliseconds())
      .toISOString();
  }
  get start_at_form_display(): string {
    return moment(this.start_date, 'YYYY/MM/DD').format('YYYY[年]MMMDo');
  }
  get start_at_display(): string {
    return moment(this.start_at).format('YYYY[年]MMMDo (ddd) HH:mm');
  }
  get start_at_display_intl(): string {
    return moment(this.start_at).format('YYYY/MM/DD HH:mm');
  }
  get start_at_date_display(): string {
    return moment(this.start_at).format('YYYY/MM/DD');
  }
  get start_at_time_display(): string {
    return moment(this.start_at).format('HH:mm');
  }
  get end_at_time_display(): string {
    return moment(this.end_at).format('HH:mm');
  }
  get start_week_day(): string {
    // const time = this.start_at ? this.start_at : this.start_at_form;
    return moment(this.start_at_form).format('ddd');
  }
  start_date: string;
  start_time: string;
  get end_time(): string {
    const endTimeDuration = moment.duration(this.start_time).add(this.duration, 'minute');
    return endTimeDuration.format('HH:mm');
  }
  duration: number;
  get bookingDurationString(): string {
    return DURATIONS_TEXT[`TEXT_${this.duration}`];
  }

  end_at: string;
  get duration_usage(): string {
    const diff = moment(this.end_at).diff(moment(this.start_at), 'minute');
    const hour = Math.floor(diff / 60);
    const minute = diff % 60;
    const hours = hour > 0 ? `${hour}時間` : '';
    const minutes = minute > 0 ? `${minute}分` : '';
    return `${hours} ${minutes}`;
  }

  type: string;
  get isWeb(): boolean {
    return this.type === RESV_TYPE.WEB;
  }
  get isPhone(): boolean {
    return this.type === RESV_TYPE.PHONE;
  }
  get isWalkIn(): boolean {
    return this.type === RESV_TYPE.WALK_IN;
  }
  get resvTypeDisplay(): string {
    return this.isWeb ? 'web予約' : this.isWalkIn ? 'ウォークイン' : '電話予約';
  }

  created_by: string;
  get isSelfOrder(): boolean {
    return this.created_by === RES_CREATE_BY.SELF_ORDER;
  }

  get resv_name_display(): string {
    if (this.isSelfOrder) {
      return 'ウォークイン';
    }

    return (this.client_user && this.client_user.name) || this.name || (this.isWalkIn ? 'ウォークイン' : '');
  }

  receipt_date: string;
  get receipt_date_display(): string {
    if (!this.receipt_date && !this.start_at) {
      return;
    }

    return this.receipt_date
      ? moment(this.receipt_date)
          .locale('ja')
          .format('DD')
      : moment(this.start_at)
          .locale('ja')
          .format('DD');
  }
  coupon?: Coupon;
  get discount_display(): string {
    return this.coupon ? `-${this.coupon.discount_display}` : '0';
  }
  courses?: Course[];
  reservation_courses?: Course[];
  reservation_preorder_items: Course[];

  get courseCountPre(): string {
    let str: string = '';
    this.reservation_preorder_items.filter((course: Course, key: number) => {
      const classStr = `course-history-item ${this.reservation_preorder_items.length - 1 === key ? 'h-auto' : ''}`;
      str += `<span class="${classStr}">x${course.quantity}</span>`;
    });
    return str;
  }

  get courseNamePre(): string {
    let str: string = '';
    this.reservation_preorder_items.filter((course: Course, key: number) => {
      const classStr = `course-history-item ${this.reservation_preorder_items.length - 1 === key ? 'h-auto' : ''}`;
      str += `<span class="${classStr}">${course.name}</span>`;
    });
    return str;
  }

  items?: Menu[];
  buffets?: MenuBuffet[];
  get all_selection(): Array<Course | Menu | MenuBuffet> {
    return []
      .concat(this.courses)
      .concat(this.items)
      .concat(this.buffets);
  }
  get all_selection_string(): string[] {
    return this.all_selection.map((item) => `${item.name} × ${item.quantity}`);
  }

  get resvDraft(): boolean {
    return this.is_temp;
  }

  get resvInfoCalendar(): string {
    if (!this.resvDraft && !!this.prepaid) {
      return `( ${this.prepaid.format()}円 )`;
    }
    return ``;
  }

  get date_string() {
    return moment(this.start_at).format('YYYY[年]MMMDo');
  }
  get start_end_time_string(): string {
    let start_time = '';
    let end_time = '';
    if (this.start_at) {
      start_time = moment(this.start_at).format('HH:mm');
    }
    if (this.end_at) {
      end_time = moment(this.end_at).format('HH:mm');
    }
    return `${start_time}~${end_time}`;
  }
  get start_end_date_string(): string {
    return moment(this.start_at).format('YYYY[年]MMMDo');
  }

  status: string;
  get status_display(): string {
    return RESV_STATUS_TEXT[this.status];
  }
  get isInComing(): boolean {
    return this.status === RESV_STATUS.INCOMING;
  }

  get isCanceled(): boolean {
    return this.status === RESV_STATUS.CANCELED;
  }

  get isWorking(): boolean {
    return this.status === RESV_STATUS.WORKING;
  }

  get isFinished(): boolean {
    return this.status === RESV_STATUS.FINISHED;
  }

  get isDisabledCancelBtn(): boolean {
    return this.isCanceled || this.isWalkIn || this.isWorking || this.isFinished;
  }

  get resvStatusString(): string {
    return this.first_at_store ? '新規' : '既存';
  }

  payment_by: string;
  get isPaymentByCash(): boolean {
    return this.payment_by === RESV_PAYMENT_BY.CASH;
  }
  get isPaymentByCard(): boolean {
    return this.payment_by === RESV_PAYMENT_BY.CARD;
  }
  get isPaymentByOmiseno(): boolean {
    return this.payment_by === RESV_PAYMENT_BY.OMISENO;
  }
  get payment_by_display(): string {
    if (!this.payment_by) {
      return '-';
    }
    return `${RESV_PAYMENT_BY_TEXT[this.payment_by] ? RESV_PAYMENT_BY_TEXT[this.payment_by] : this.payment_by}`;
  }
  get is_card(): boolean {
    return this.payment_by && this.payment_by === RESV_PAYMENT_BY.CARD;
  }

  service_fee: number;
  get service_fee_format(): string {
    if (!this.service_fee) {
      return '-';
    }
    return `¥${this.service_fee.format()}`;
  }

  service_fee_type: string;
  paid_at: string;
  get is_paid(): boolean {
    return !!this.paid_at;
  }
  prepaid: number;
  get prepaid_format(): string {
    if (!this.prepaid) {
      return '-';
    }
    return `${this.prepaid.format()}`;
  }
  get is_paid_or_prepaid(): boolean {
    return !!this.paid_at || !!this.prepaid;
  }
  get paid_status_text(): string {
    if (this.is_paid_or_prepaid) {
      return '会計済';
    }
    return '会計前';
  }
  preorder_paid: number;
  get preorder_paid_format(): string {
    if (!this.preorder_paid) {
      return '-';
    }
    return `¥${this.preorder_paid.format()}`;
  }
  preorder_items_count: number;
  get isOrdered(): boolean {
    return this.preorder_items_count > 0;
  }
  get isShowOrderedStatus(): boolean {
    return this.isOrdered && !this.isFinished;
  }
  is_temp: boolean;
  keywords: ResvKeyword[];
  get keyword_ids(): number[] {
    return this.keywords.map((item) => item.id);
  }
  coupon_discount: number;
  get coupon_discount_format(): string {
    if (!this.coupon_discount) {
      return '-';
    }
    return `¥${this.coupon_discount.format()}`;
  }

  get isEnoughInfo(): boolean {
    return (
      this.client_user.phone &&
      this.client_user.phone.length >= 10 &&
      this.client_user.name &&
      this.start_date &&
      this.start_time &&
      this.quantity &&
      !!this.tables.length &&
      !!this.duration
    );
  }

  get isPassDate(): boolean {
    let resvDateSelected: any = moment(`${this.start_date} ${this.start_time}`, 'YYYY/MM/DD HH:mm:ss');

    if (resvDateSelected.hour() < 6) {
      resvDateSelected = resvDateSelected.add(1, 'days');
    }

    return moment(resvDateSelected).isBefore(moment(), 'days');
  }

  order_payment_method: string;
  get isWaitForPay(): boolean {
    return this.isWorking && this.order_payment_method === ORDER_PAYMENT_METHOD.ALL && !this.is_paid;
  }
  get isReadyForPay(): boolean {
    if (this.isSelfOrder) {
      return this.order_payment_method === ORDER_PAYMENT_METHOD.ALL && !this.is_paid;
    }
    return !this.is_paid;
  }

  receipt_number: number;
  latest_payment?: ReservationLatestPayment;
  get isRefund(): boolean {
    const paymentUnit = ['stripe', 'square_card', 'square_cash'];
    return (
      this.latest_payment && this.latest_payment.refund_at && paymentUnit.indexOf(this.latest_payment.payment_by) !== -1
    );
  }
  get isRefundNot3rd(): boolean {
    const paymentUnit = ['stripe', 'square_card', 'square_cash'];
    return (
      this.latest_payment && this.latest_payment.refund_at && paymentUnit.indexOf(this.latest_payment.payment_by) === -1
    );
  }
  get isRefundSelfOrder(): boolean {
    return this.latest_payment && this.latest_payment.refund_at && this.isSelfOrder;
  }

  receipt_code?: string;

  constructor() {
    this.courses = [];
    this.items = [];
    this.buffets = [];
    this.client_user = new ClientUser();
    this.table = new Table();
    this.latest_payment = new ReservationLatestPayment();
    this.tables = [];
    this.start_date = moment().format('YYYY/MM/DD');
    this.start_time = moment().format('HH:mm:ss');
    this.duration = DURATIONS_VALUE.VALUE_120;
    this.keywords = [];
  }

  deserialize(input: Partial<ReservationInput>): Reservation {
    Object.assign(this, input);

    if (input.client_user) {
      this.client_user =
        input.client_user instanceof ClientUser ? input.client_user : new ClientUser().deserialize(input.client_user);
    }
    if (input.table) {
      this.table = new Table().deserialize(input.table);
    } else {
      this.table = new Table();
    }
    if (input.tables) {
      this.tables = input.tables.map((item: TableInput) => new Table().deserialize(item));
    }
    if (input.items) {
      this.items = input.items.map((item: MenuInput) => new Menu().deserialize(item));
    }
    if (input.buffets) {
      this.buffets = input.buffets.map((item: MenuBuffet) => new MenuBuffet().deserialize(item));
    }
    if (input.courses) {
      this.courses = input.courses.map((item: CourseInput) => new Course().deserialize(item));
    }
    if (input.reservation_courses) {
      this.reservation_courses = input.reservation_courses.map((item: CourseInput) => new Course().deserialize(item));
    }
    if (input.coupon) {
      this.coupon = new Coupon().deserialize(input.coupon);
    }
    if (input.user) {
      this.user = new User().deserialize(input.user);
    }
    if (input.client_member) {
      this.client_member = new ClientMember().deserialize(input.client_member);
    }
    if (input.reservation_items) {
      this.items = input.reservation_items.map((item: MenuInput) => new Menu().deserialize(item));
    }
    if (input.start_at) {
      this.start_date = moment(input.start_at).format('YYYY/MM/DD');
      this.start_time = moment(input.start_at).format('HH:mm:ss');
    }
    if (input.keywords) {
      this.keywords = input.keywords.map((item) => new ResvKeyword().deserialize(item));
    }
    if (input.reservation_passcode) {
      this.smp_code = this.reservation_passcode.smp_code;
    }
    return this;
  }

  addFilter(input: Partial<ReservationInput>): Reservation {
    Object.assign(this, input);
    return this;
  }

  formData(): ReservationOutput {
    const data: any = {
      user: this.client_user.formData(),
      client_user_id: this.client_user.id,
      phone: this.client_user.phone,
      duration: this.duration,
      quantity: +this.quantity,
      memo: this.memo,
      has_children: this.has_children,
      items: this.getOrderQuantity(this.items),
      buffets: this.getOrderQuantity(this.buffets),
      courses: this.getOrderQuantity(this.courses),
      coupon_id: this.coupon ? this.coupon.id : null,
      table_ids: this.tables.map((item) => item.id),
      ...this.renderDateTimeFormData(),
    };

    if (!!this.keywords.length) {
      data.keyword_ids = this.keyword_ids;
    }

    return data;
  }

  // Check the time less more than 06:00, this is a future day
  renderDateTimeFormData() {
    const duration = moment.duration(this.start_time);
    let dateTmp = moment(this.start_date, 'YYYY/MM/DD');
    if (duration.hours() < 6) {
      dateTmp = dateTmp.add(1, 'days');
    }

    return {
      start_at: dateTmp.set('milliseconds', duration.asMilliseconds()).toISOString(),
      end_at: dateTmp.set('minutes', this.duration).toISOString(),
    };
  }

  getCouponToJSON(): any {
    return {
      phone: this.client_user.phone,
      start_at: this.start_at_form,
      end_at: moment(this.start_at_form)
        .add(this.duration, 'minute')
        .toISOString(),
      course_ids: this.courses.map((res) => res.id),
    };
  }

  checkAvailableTable(): CheckAvailableTableOutput {
    return {
      start_at: this.start_at,
      end_at: this.end_at,
      table_ids: this.tables.map((res) => res.id),
    };
  }

  getBusinessHoursInput(): BusinessHoursInput {
    return {
      start: moment(this.start_at).format('HH:mm'),
      end: moment(this.end_at).format('HH:mm'),
    };
  }

  getTotalPrice(): TotalPriceOutput {
    return {
      reservation_id: this.id || undefined,
      items: this.items.map((item) => item.getMenuOrder()).filter((item: any) => !!item.quantity),
      buffets: this.buffets.map((buffet) => buffet.getMenuBuffetOrder()).filter((item: any) => !!item.quantity),
      courses: this.courses.map((course) => course.getCourseOrder()).filter((item: any) => !!item.quantity),
      coupon_id: this.coupon ? this.coupon.id : undefined,
      quantity: this.quantity,
    };
  }

  private getOrderQuantity(orders: Partial<Menu | MenuBuffet | Course>[]): any[] {
    return orders.map((item) => {
      return {
        id: item.id,
        quantity: item.quantity,
      };
    });
  }
}
