import { Deserializable } from 'shared/interfaces/deserializable';
import { RICH_MENU_STATUS_TEXT } from 'shared/enums/rich-menu';
import { RichMenuItem, RichMenuItemInput } from './rich-menu-item';
import * as pickBy from 'lodash/pickBy';

export interface RichMenuInput {
  has_setting_order: boolean;
  has_setting_self_order: boolean;
  id: number;
  name: string;
  image_url: string;
  image_height: number;
  image_width: number;
  pos_x: number;
  pos_y: number;
  ratio: number;
  bg_color: string;
  font_family: string;
  text_color: string;
  items_count: number;
  items: Partial<RichMenuItemInput>[];
  rich_menu_items: Partial<RichMenuItemInput>[];
  top_screen: boolean;
  recommend: boolean;
  image_path: string;
  pattern: number;
  page: number;
  file_upload: File;
  is_take_out: boolean;
}

export class RichMenu implements Deserializable<RichMenu>, Partial<RichMenuInput> {
  id: number;
  name: string;
  has_setting_order: boolean;
  has_setting_self_order: boolean;
  get hasOrderSettingStr() {
    return this.has_setting_order ? RICH_MENU_STATUS_TEXT.SETTING : RICH_MENU_STATUS_TEXT.NOT_SETTING;
  }
  get hasSelfOrderSettingStr() {
    return this.has_setting_self_order ? RICH_MENU_STATUS_TEXT.SETTING : RICH_MENU_STATUS_TEXT.NOT_SETTING;
  }
  pos_x: number = 0;
  pos_y: number = 0;
  image_url: string;
  get image_url_src(): string {
    return this.image_url || 'assets/img/no-image.png';
  }
  get style(): any {
    return {
      'background-image': `url(${this.image_url_src})`,
    };
  }
  image_height: number = 0;
  get heightScale() {
    return this.image_height * this.ratio;
  }
  image_width: number = 0;
  get widthScale() {
    return this.image_width * this.ratio;
  }

  items_count: number = 0;
  get quantity(): string {
    return !this.top_screen ? `${this.items_count}品` : '-';
  }
  items: RichMenuItem[] = [];
  rich_menu_items: RichMenuItem[] = [];
  ratio: number = 1;
  bg_color: string;
  font_family: string;
  text_color: string;

  top_screen: boolean;
  recommend: boolean;
  image_path: string;
  pattern: number;
  page: number;
  file_upload: File;
  is_take_out: boolean;

  constructor() {
    this.pos_y = 0;
    this.pos_x = 0;
    this.items = [];
    this.ratio = 1;
    this.image_url = null;
    this.rich_menu_items = [];
  }

  deserialize(input: Partial<RichMenuInput>): RichMenu {
    if (!input) {
      return;
    }

    Object.assign(this, input);

    this.items = input.items && input.items.map((item) => new RichMenuItem().deserialize(item));
    this.rich_menu_items =
      input.rich_menu_items && input.rich_menu_items.map((item) => new RichMenuItem().deserialize(item));
    this.pos_x = +input.pos_x;
    this.pos_y = +input.pos_y;

    return this;
  }

  public toJSON() {
    const data = pickBy(this, (value, key) => {
      return (
        ['pos_x', 'pos_y', 'pattern', 'font_family', 'text_color', 'bg_color', 'page', 'ratio'].indexOf(key) >= 0 &&
        ![null, undefined, ''].includes(value)
      );
    });
    data.image_path = this.image_path ? this.image_path : null;
    data.rich_menu_items = this.items.map((item) => item.toJSON());

    return data;
  }

  public getStyleSetting() {
    return {
      bg_color: this.bg_color,
      font_family: this.font_family,
      text_color: this.text_color,
    };
  }

  updateSettingDisplay(data: any) {
    this.bg_color = data.bg_color;
    this.text_color = data.text_color;
    this.font_family = data.font_family;
  }

  changeImage(data: any) {
    this.image_url = data.image_url;
    this.image_width = data.image_width;
    this.image_height = data.image_height;
    this.pos_x = 0;
    this.pos_y = 0;
    this.ratio = 1;
    this.file_upload = data.file_upload;
  }

  removeImage() {
    this.image_path = null;
    this.image_url = null;
    this.file_upload = null;
  }
}
