import { Deserializable } from 'shared/interfaces/deserializable';

export interface ClientMemberInput {
  id: number;
  name: string;
  email: string;
  created_at: string;
  updated_at: string;
}

export interface ClientMemberOutput {
  id: number;
  name: number;
  email: number;
  created_at: string;
  updated_at: string;
}

export class ClientMember implements Deserializable<ClientMember>, ClientMemberInput {
  id: number;
  name: string;
  email: string;
  created_at: string;
  updated_at: string;

  constructor() {}

  deserialize(input: Partial<ClientMemberInput>): ClientMember {
    if (input) {
      Object.assign(this, input);
    }
    return this;
  }
}
