import { ROLES } from 'shared/enums/roles';
import { Deserializable } from 'shared/interfaces/deserializable';
import { Client, ClientInput } from './client';

export interface UserInput {
  client?: ClientInput;
  id: number;
  name: string;
  role: string;
  role_name: string;
  email: string;
  phone: string;
  memo: string;
  password: string;
  password_confirmation?: string;
  reservations_count: number;
  created_at: string;
  updated_at: string;
  is_new: boolean;
  finished_reservations_count: number;
  is_admin: boolean;
  user_id: number;
}

export interface UserOutput {
  id: number;
  name: string;
  email: string;
  role_name: string;
  password: string;
  password_confirmation: string;
}
export interface UserUpdateInfo {
  name: string;
  phone: string;
  memo: string;
  email: string;
}

export class User implements Deserializable<User>, UserInput {
  id: number;
  get defaultChannel(): string {
    return `client_member.${this.id}`;
  }
  name: string;
  role: string;
  get isAdmin(): boolean {
    return this.role === ROLES.ADMIN;
  }
  get isClient(): boolean {
    return this.role === ROLES.CLIENT;
  }
  get isUser(): boolean {
    return this.role === ROLES.USER;
  }
  role_name: string;
  email: string;
  phone: string;
  memo: string;
  password: string;
  password_confirmation: string;
  reservations_count: number;
  finished_reservations_count: number;
  is_new: boolean;
  get statusDisplay(): string {
    return this.is_new ? '新規' : `既存(${this.finished_reservations_count}回)`;
  }
  get status_string(): string {
    return this.is_new ? '新規' : 'リピーター';
  }
  created_at: string;
  updated_at: string;
  is_admin: boolean;
  user_id: number;
  get is_cashless_user(): boolean {
    return !!this.user_id;
  }
  client: Client;

  get isConnectGourmet(): boolean {
    return this.client && !!this.client.gourmet_accounts_count;
  }

  deserialize(input: Partial<UserInput>): User {
    Object.assign(this, input);

    return this;
  }

  memberFormData(): Partial<UserOutput> {
    const result: Partial<UserOutput> = {
      name: this.name,
      email: this.email.toLowerCase(),
      role_name: this.role_name,
    };

    if (this.password) {
      result.password = this.password;
      result.password_confirmation = this.password_confirmation;
    }

    if (this.id) {
      result.id = this.id;
    }

    return result;
  }

  userFormData(): UserUpdateInfo {
    return {
      name: this.name,
      phone: this.phone,
      memo: this.memo,
      email: this.email,
    };
  }
}
