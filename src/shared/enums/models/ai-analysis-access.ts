import { Deserializable } from 'shared/interfaces/deserializable';
import { GENGER } from 'shared/enums/ai-analysis';
import * as moment from 'moment';

export interface AiAnalysisAccessInput {
  id: number;
  age: number;
  face_id: number;
  gender: boolean;
  entrance_at: string;
  exit_at: string;
}

export class AiAnalysisAccess implements Deserializable<AiAnalysisAccess>, Partial<AiAnalysisAccessInput> {
  id: number;
  age: number;
  get ageDisplay(): string {
    const realAge = this.age === 0 ? 19 : (this.age + 1) * 10;
    const ageAppendix = realAge === 19 ? '歳以下' : realAge === 70 ? '歳以上' : '代';

    return `${realAge}${ageAppendix}`;
  }
  gender: boolean;
  get gender_string(): string {
    return this.gender ? GENGER.female : GENGER.male;
  }
  entrance_at: string;
  get entranceDisplay(): string {
    return this.entrance_at
      ? moment(this.entrance_at)
          .local()
          .format('YYYY-MM-DD HH:mm')
      : '';
  }
  exit_at: string;
  get exitDisplay(): string {
    return this.exit_at
      ? moment(this.exit_at)
          .local()
          .format('YYYY-MM-DD HH:mm')
      : '';
  }
  face_id: number;

  deserialize(input: Partial<AiAnalysisAccessInput>): AiAnalysisAccess {
    Object.assign(this, input);
    return this;
  }
}
