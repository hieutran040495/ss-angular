import { Deserializable } from 'shared/interfaces/deserializable';

export interface ScaleInput {
  scale_x: number;
  scale_y: number;
}

export class Scale implements Deserializable<Scale>, Scale {
  scale_x: number;
  scale_y: number;

  constructor() {
    this.scale_x = 1;
    this.scale_y = 1;
  }
  deserialize(input: Partial<ScaleInput>): Scale {
    Object.assign(this, input);
    return this;
  }
}
