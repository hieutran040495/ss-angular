import { Deserializable } from 'shared/interfaces/deserializable';

export interface DeviceInput {
  id: number;
  code: string;
  has_client: boolean;
  type: string;
}

export class Device implements Deserializable<Device>, DeviceInput {
  id: number;
  code: string;
  has_client: boolean;
  type: string;
  get code_display(): string {
    return this.has_client ? `${this.code} (設定済)` : this.code;
  }
  get disabled(): boolean {
    return this.has_client ? true : false;
  }

  get isEntrance(): boolean {
    return this.type === 'entrance';
  }

  get isExit(): boolean {
    return this.type === 'exit';
  }

  get label(): string {
    return this.isEntrance ? '入店カメラデバイスID' : '退店カメラデバイスID';
  }

  deserialize(input: Partial<DeviceInput>): Device {
    Object.assign(this, input);

    return this;
  }

  formData() {
    return {
      id: this.id,
      type: this.type,
    };
  }
}
