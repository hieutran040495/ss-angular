export enum TAX_ROUND {
  UP = 'up',
  DOWN = 'down',
  OFF = 'off',
}

export enum TAX_TYPE {
  INTERNAL = 'internal',
  FOREIGN = 'foreign',
}

export enum TAX_APPLY_TYPE {
  IN = 'in',
  OUT = 'out',
  NOT_USE = 'nrdc',
}
