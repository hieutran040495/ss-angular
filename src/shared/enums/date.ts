export enum DATE_TYPE {
  DATE = 'date',
  WEEK = 'week',
  MONTH = 'month',
  CUSTOM = 'custom',
}

export enum MIN_MODE {
  DAY = 'day',
  MONTH = 'month',
  YEAR = 'year',
}
