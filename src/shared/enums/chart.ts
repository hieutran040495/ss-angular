export enum PIE_CHART_USER {
  total_new_user = '新規',
  total_old_user = 'リピート',
}

export enum PIE_CHART_PAYMENT {
  total_cash = '現金',
  total_cashless = 'オミセノ',
  total_credit_card = 'クレカ',
  total_other = 'その他',
}
