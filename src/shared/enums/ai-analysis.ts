export enum AI_ANALYSIS_TYPE {
  total_age_0 = '0~19  ',
  total_age_1 = '20~29',
  total_age_2 = '30~39',
  total_age_3 = '40~49',
  total_age_4 = '50~59',
  total_age_5 = '60~69',
  total_age_6 = '70~    ',
  total_customer_new = '新規顧客',
  total_customer_repeater = 'リピーター',
  total_gender_female = '女性',
  total_gender_male = '男性',
}

export enum GENGER {
  female = '女性',
  male = '男性',
}
