export enum RICH_MENU_STATUS {
  SETTING = 'setting',
  NOT_SETTING = 'not_setting',
}

export enum RICH_MENU_STATUS_TEXT {
  SETTING = '設定中',
  NOT_SETTING = '未設定',
}

export enum APP_SETTING {
  ORDER = 'order',
  SELF_ORDER = 'self-order',
  ORDER_SETTING = 'order_settings',
  SELF_ORDER_SETTING = 'self_order_setting',
}
