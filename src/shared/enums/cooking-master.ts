export enum COOKING_MASTER {
  SUB_STATUS = 'm-subStatus',
  COOKING_TYPE = 'm-cooking_type',
  TIME_LIMIT = 'm-timeLimit',
}

export enum MASTER_STATUS {
  UPDATE = 'update',
  NONE = 'none',
  CREATE = 'create',
  REMOVE = 'remove',
  ERROR = 'error',
}

export enum MASTER_TABLE_NAME {
  'm-subStatus' = '特記',
  'm-cooking_type' = '調理タイプ',
  'm-timeLimit' = '推奨調理時間',
}
