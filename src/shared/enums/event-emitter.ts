export enum EventEmitterType {
  ORDER_MENU = 'order-menu',
  SELECT_MENU_ITEM = 'selected-menu-item',
  SELECT_MENU_BUFFET = 'selected-menu-buffet',
  SEARCH_CLIENT_USER = 'search_client_user',
  UPDATE_CLIENT_PROFILE = 'update_client_profile',
  OPEN_MODAL_USER_DETAIL = 'open-modal-user-detail',
}

export enum CALENDAR_EVENT_EMITTER {
  ENABLE_CHOOSE_TABLE = 'enable_choose_table',
  DISABLE_CHOOSE_TABLE = 'disable_choose_table',
  RELOAD = 'reload_calendar',
  EDIT_WALKIN = 'edit-walkin',
  CREATE_WALKIN = 'create-walkin',
  CREATE_RES_CHANGE_DATE = 'create-res-change-date',
  CHANGE_DATE = 'change_date',
  CHANGE_DURATION = 'change_duration',
  CHANGE_SMP_CODE = 'change_smp_code',
}

export enum CALENDAR_MODE {
  SELECT_TABLE = 'select_table',
  VIEW = 'view',
}

export enum RESV_TABLE_EVENT_EMITTER {
  CHOOSE_TABLE = 'choose_table',
}

export enum RESV_MODE_EMITTER {
  CREATE = 'create',
  EDIT = 'edit',
  CLOSE = 'close',
}

export enum SETTING_RICH_MENU {
  CHANGE_RICH_MENU = 'change_rich_menu',
  SAVE_RICH_MENU = 'save_rich_menu',
  CLOSE_MODAL_SETTING_RICH_MENU = 'close_modal_setting_rich_menu',
  RELOAD_RICH_MENU = 'reload_rich_menu',
  ERROR_RICH_MENU = 'error_rich_menu',
  REMOVE_MENU_ITEM = 'remove_menu_item',
}

export enum SETTING_DISPLAY {
  CHANGE_SETTING_DISPLAY = 'change_setting_display',
  SUCCESS = 'setting_display_rich_menu_success',
  ERROR = 'setting_display_rich_menu_error',
}

export enum SELF_ORDER {
  CHANGE_TOP_IMAGE = 'change_top_image',
  SAVE_TOP_SCREEN = 'save_top_screen',
  SAVE_SETING_RICH_MENU = 'save_setting_rich_menu_self_order',
  CHANGE_SETTING_DISPLAY = 'change_setting_display_self_order',
  CHANGE_SLIDE = 'change_slide',
  REMOVE_ITEM = 'remove_item',
  REMOVE_ITEM_DEVICE = 'remove_item_device',
  REMOVE_ITEM_SUCCESS = 'remove_item_success',
  CHANGE_FIRST_PATTERN = 'change_first_pattern',
  CHANGE_SECOND_PATTERN = 'change_second_pattern',
  CHANGE_PATTERN = 'change_pattern',
  CHANGE_IMAGE_RICH_MENU = 'change_image_rich_menu',
  ACCEPT_REMOVE_SLIDE = 'accept_remove_slide',
  REMOVE_LIST_ITEM = 'remove_list_item',
}

export enum ORDER {
  CHANGE_TOP_IMAGE = 'change_top_image',
  REMOVE_TOP_IMAGE = 'remove_top_image',
  CHANGE_TOP_SCREEN = 'change_top_screen',
  CHANGE_SETTING_DISPLAY = 'change_setting_display',
  SAVE_TOP_SCREEN = 'save_top_screen',
  TRANSFER_TOP_SCREEN_DATA = 'transfer_top_screen_data',
}

export enum KONVAJS_TYPE {
  REMOVE_MAIN_IMAGE = 'remove_main_image',
}

export enum HISTORY_TYPE {
  RESERVATION_FILTER = 'RESERVATION_FILTER',
}
