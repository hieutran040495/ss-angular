export enum PROFIT_DATETIME {
  DAY = 'DAY',
  WEEKS = 'WEEKS',
  MONTHS = 'MONTHS',
}

export enum PROFIT_TYPE {
  DAY = 'days',
  WEEKS = 'weeks',
  MONTHS = 'months',
}

export enum PROFIT_FORMAT {
  WEEKS = 'MM/dd (ccc)',
  MONTHS = 'yyyy年MM月',
}
