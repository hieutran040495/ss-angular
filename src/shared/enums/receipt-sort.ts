export enum RECEIPT_SORT {
  USER_NAME = 'client_user.name',
  TABLE_CODE = 'table_code',
  IS_PAID = 'is_paid',
}
