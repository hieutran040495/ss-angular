export enum SEARCH_WITH {
  COURSE = 'course',
  CATEGORY = 'category',
  BUFFET = 'buffet',
}

export enum ORDER_TYPE {
  RESV_ORDER = 'resv_order',
  SELECT_MENU = 'select_menu',
  SELECT_BUFFET = 'select_buffet',
}

export enum OPTION_TYPE {
  COURSE = 'course',
  BUFFET = 'buffet',
}
