export enum RESV_MODE {
  CANCEL = 'cancel_resv',
  REMOVE = 'remove_resv',
  REFRESH = 'refresh_resv',
  NAVIGATION = 'navigate_resv',
  EDIT = 'edit',
}
