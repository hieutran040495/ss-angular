export enum ICON_NOTIFY {
  ICON_PHONE = 'assets/icons/icon_phone.svg',
  ICON_CALENDAR = 'assets/icons/icon_calendar.svg',
  ICON_WARNING = 'assets/icons/icon_warning.svg',
}
