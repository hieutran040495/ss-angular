export enum LOCALSTORAGE_KEY {
  APP_NAME = 'cas_client_app',
  RESV_STORE = 'resv_store',
  USE_IN_MAINTENANCE = 'use_in_maintenance',
  LASTEST_VERSION = 'lastest_version',
}
