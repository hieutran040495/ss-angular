export enum SERVICE_FEE_TYPE {
  AMOUNT = 'amount',
  PERCENT = 'percent',
}

export enum SERVICE_FEE_BY {
  RESERVATION = 'resv',
  QUANTITY = 'qty',
}

export enum SERVICE_FEE_TYPE_UNIT {
  amount = '円',
  percent = '%',
}
