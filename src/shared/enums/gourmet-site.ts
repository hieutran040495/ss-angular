export enum GOURMET_NAME {
  h7r = 'ホットペッパー',
  g3i = 'ぐるなび',
  t5g = '食べログ',
  r3y = 'Retty',
}

export enum GOURMET_LINK {
  h7r = 'https://www.cms.hotpepper.jp/CLN/login/',
  g3i = 'https://pro.gnavi.co.jp/login',
  t5g = 'https://ssl.tabelog.com/owner_account/login/',
  r3y = '',
}
