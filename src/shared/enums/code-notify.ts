export enum NOTIFY_CODE {
  RESV_CREATE = 'resv_create',
  RESV_UPDATE = 'resv_update',
  RESV_CANCEL = 'resv_cancel',
  RESV_PAID = 'resv_paid',
  READ_NOTIFY = 'read_notify',
  READ_NOTIFY_CSV = 'read_notify_csv',
  CLIENT_USER_EXPORT_FINISH = 'client_user_export_finish',
  FINISH_EXPORT_PDF_RESERVATION = 'finish_export_pdf_reservation',
  BEFORE_RESV_START = 'before_resv_start',
  BEFORE_RESV_END = 'before_resv_end',
  INCOMING_CALL = 'incoming_call',
  RESV_START = 'resv_start',
  RESV_CLEAN_UP = 'resv_clean_up',
  CALL_SOLVE_MISSED = 'call_solve_missed',
  KEYWORD_CREATE = 'keyword_create',
  KEYWORD_UPDATE = 'keyword_update',
  TAX_UPDATE = 'tax_update',
  IMPORT_RESV_FAIL = 'import_resv_fail',
  IMPORT_RESV_SUCCESS = 'import_resv_success',
  IMPORT_RESV_COMPLETE = 'import_resv_complete',
}

export enum NOTIFY_MAINTENANCE {
  MAINTENANCE_START = 'maintenance_start',
  MAINTENANCE_END = 'maintenance_end',
}
