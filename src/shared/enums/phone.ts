export enum PHONE_STATUS {
  NO_ANSWER = 'no-answer',
  IN_PROGRESS = 'in-progress',
}
