export enum ROLES {
  ADMIN = 'admin',
  CLIENT = 'client',
  USER = 'user',
}
