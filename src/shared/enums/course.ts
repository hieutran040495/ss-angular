export enum COURSE_ACTIVE {
  SHOW = '表示',
  HIDE = '非表示',
}

export enum TYPE_VALIDATION {
  BUFFETS = 'buffets',
  MENU = 'items',
}
