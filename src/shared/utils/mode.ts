import { MODES } from 'shared/enums/modes';

export class Mode {
  private _currMode: string;
  get currMode(): string {
    return this._currMode;
  }
  set currMode(v: string) {
    this._currMode = v;
  }

  get isNew(): boolean {
    return this.currMode === MODES.NEW;
  }

  get isEdit(): boolean {
    return this.currMode === MODES.EDIT;
  }

  get isView(): boolean {
    return this.currMode === MODES.VIEW;
  }

  get isRemove(): boolean {
    return this.currMode === MODES.REMOVE;
  }

  constructor() {
    this.currMode = MODES.NEW;
  }

  setNew() {
    this.currMode = MODES.NEW;
  }

  setEdit() {
    this.currMode = MODES.EDIT;
  }

  setView() {
    this.currMode = MODES.VIEW;
  }

  setRemove() {
    this.currMode = MODES.REMOVE;
  }
}
