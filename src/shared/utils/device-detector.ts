import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DeviceDetectorService {
  getMobileOperatingSystem() {
    const userAgent = navigator.userAgent || navigator.vendor;
    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|Macintosh/.test(userAgent)) {
      return 'iOS';
    }

    if (/android/i.test(userAgent)) {
      return 'Android';
    }

    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
      return 'Windows Phone';
    }

    return 'unknown';
  }

  getBrowserSystem() {
    const userAgent = navigator.userAgent || navigator.vendor;
    if (/CriOS/.test(userAgent)) {
      return 'Chrome';
    }

    if (/Safari/.test(userAgent)) {
      return 'Safari';
    }
  }
}
