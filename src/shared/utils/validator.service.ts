import { Injectable } from '@angular/core';
import { TYPE_VALIDATION } from 'shared/enums/course';

@Injectable()
export class ValidatorService {
  constructor() {}

  resetErrors() {
    return {};
  }

  setErrors(errors): any {
    const validators = {};

    Object.keys(errors).map((key) => {
      if (!!errors[key]) {
        const attrKey = key.split('.');
        if (attrKey.length === 2) {
          this.setErrorListIds(validators, errors, key, attrKey[0]);
        }
        validators[key] = errors[key];
      }
    });

    return validators;
  }

  setErrorListIds(validators: any, errors: any, key: string, newkey: string): void {
    if (validators.hasOwnProperty(newkey)) {
      validators[newkey] = validators[newkey] + ' ' + errors[key];
    } else {
      validators[newkey] = errors[key];
    }
  }

  setErrorsQuatity(errors: Object = {}): null | Object {
    const validators = {
      items: {},
      buffets: {},
    };

    Object.keys(errors).map((key) => {
      if (!!errors[key]) {
        const attrKey = key.split('.');
        if (attrKey.length < 3) {
          return;
        }
        const _key = attrKey[1];
        if (attrKey[0] === TYPE_VALIDATION.MENU) {
          if (validators.hasOwnProperty(_key)) {
            validators.items[_key] = validators[_key] + ' ' + errors[key];
          } else {
            validators.items[_key] = errors[key];
          }
        }
        if (attrKey[0] === TYPE_VALIDATION.BUFFETS) {
          if (validators.hasOwnProperty(_key)) {
            validators.buffets[_key] = validators[_key] + ' ' + errors[key];
          } else {
            validators.buffets[_key] = errors[key];
          }
        }
      }
    });
    return validators;
  }
}
