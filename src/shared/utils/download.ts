export const download = (res: string) => {
  const anchor = document.createElement('a');
  anchor.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(res));
  anchor.setAttribute('download', 'user-sample.xlsx');
  anchor.style.display = 'none';
  document.body.appendChild(anchor);
  anchor.click();
  document.body.removeChild(anchor);
};

export class DownloadHelpers {
  static downloadFromUri(uri: string) {
    const link = document.createElement('a');
    link.download = uri;
    link.target = '_downloadFrame';
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
}
