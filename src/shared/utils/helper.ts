import * as $ from 'jquery';

export class Helpers {
  static setLoading(enable) {
    const body = $('body');
    if (enable) {
      $(body).addClass('page-loading--non-block');
    } else {
      $(body).removeClass('page-loading--non-block');
    }
  }
}
