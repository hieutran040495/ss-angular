export class SortHelper {
  // Quick Sort
  static quickSort(items, left, right) {
    let index;
    if (items.length > 1) {
      index = this.partition(items, left, right); //index returned from partition
      if (left < index - 1) {
        //more elements on the left side of the pivot
        this.quickSort(items, left, index - 1);
      }
      if (index < right) {
        //more elements on the right side of the pivot
        this.quickSort(items, index, right);
      }
    }
    return items;
  }

  static partition(items, left, right) {
    const pivot = items[Math.floor((right + left) / 2)]; //middle element
    let i = left, //left pointer
      j = right; //right pointer
    while (i <= j) {
      while (items[i] > pivot) {
        i++;
      }
      while (items[j] < pivot) {
        j--;
      }
      if (i <= j) {
        this.swap(items, i, j); //swap two elements
        i++;
        j--;
      }
    }
    return i;
  }

  static swap(items, leftIndex, rightIndex) {
    const temp = items[leftIndex];
    items[leftIndex] = items[rightIndex];
    items[rightIndex] = temp;
  }
}
