import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextComponent } from './input-text.component';
import { FormsModule } from '@angular/forms';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { KeyboardNumberModule } from '../keyboard-number/keyboard-number.module';

@NgModule({
  imports: [CommonModule, FormsModule, PopoverModule.forRoot(), KeyboardNumberModule],
  declarations: [InputTextComponent],
  exports: [InputTextComponent],
})
export class InputTextModule {}
