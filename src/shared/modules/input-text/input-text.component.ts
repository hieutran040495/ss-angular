import { Component, OnInit, Input, forwardRef, ViewEncapsulation, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import * as $ from 'jquery';
import { PopoverDirective } from 'ngx-bootstrap/popover';

const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => InputTextComponent),
  multi: true,
};

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
  encapsulation: ViewEncapsulation.None,
})
export class InputTextComponent implements OnInit, ControlValueAccessor {
  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;
  @ViewChild('popover') popover: PopoverDirective;
  @Input('pattern') pattern: RegExp;
  @Input('class') class: string;
  @Input('required') required: boolean;
  @Input('errorMessage') errorMessage: string;
  @Input('minlength') minlength: string;
  @Input('maxlength') maxlength: string;
  @Input('disabled') disabled: boolean;
  @Input('placeholder') placeholder: string = '';

  // ngModel value
  private _text: string;
  public get text(): string {
    return this._text || '';
  }
  public set text(v: string) {
    if (v !== this.text) {
      this._text = v;
      this._onChangeCallback(v);
    }
  }

  public textEntering: string = '';

  constructor() {}

  ngOnInit() {}

  /**
   * Overriden for interface ControlValueAccessor
   */
  public onTouched() {
    this._onTouchedCallback();
  }

  public writeValue(value: string) {
    this._text = undefined;
    this.textEntering = '';
    if (value && this.pattern) {
      if (!this.pattern.test(value)) {
        console.warn('The input value is not properly formatted');
      }
      this._text = value.toString();
      this.textEntering = value.toString();
    }
  }

  public registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  public registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  public clear() {
    this.textEntering = '';
  }

  clickedDigit(event: number) {
    if (this.pattern.test(this.textEntering + event)) {
      this.textEntering += event.toString();
    }
  }

  confirmed() {
    this.text = this.textEntering;
    this.popover.hide();
  }

  delete() {
    this.textEntering = this.textEntering.substring(0, this.textEntering.length - 1);
  }

  onShown() {
    jQuery(() => {
      const el = $('.input-text-container');

      if (el.offset().top + el.innerHeight() > window.innerHeight) {
        el.addClass('offset-bottom');
        return;
      }

      if (el.offset().top < 0) {
        el.addClass('offset-top');
      }
    });
  }

  onHidden() {
    this.textEntering = this.text;
  }
}
