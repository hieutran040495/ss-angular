import {
  Component,
  OnInit,
  forwardRef,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
} from '@angular/core';

import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Subject } from 'rxjs';
import { CasDialogService } from 'app/modules/cas-dialog/cas-dialog.service';
import { ToastService } from 'shared/logical-services/toast.service';

const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  /* tslint:disable-next-line: no-use-before-declare */
  useExisting: forwardRef(() => CasNgSelectComponent),
  provide: NG_VALUE_ACCESSOR,
  multi: true,
};

@Component({
  selector: 'app-cas-ng-select',
  templateUrl: './cas-ng-select.component.html',
  styleUrls: ['./cas-ng-select.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CasNgSelectComponent implements OnInit, ControlValueAccessor {
  @ViewChild('casNgSelect') casNgSelect: NgSelectComponent;
  @ViewChild('inputSearch') inputSearch: ElementRef;

  @Input() name: string;
  @Input() maxLengthSearch: number = 255;
  @Input() items: any[];
  @Input() bindLabel: string;
  @Input() bindValue: string;
  @Input() clearable: boolean = true;
  @Input() placeholder: string;
  @Input() clearAllText: string;
  @Input() loading: boolean = false;
  @Input() multiple: boolean = false;
  @Input() searchable: boolean = false;
  @Input() isSearchTemplate: boolean = false;
  @Input() selectableGroup: boolean = false;
  @Input() closeOnSelect: boolean = true;
  @Input() isTemplateInput: boolean = false;
  @Input() isTemplateSpan: boolean = false;
  @Input() disabled: boolean = false;
  @Input() sliceMax: number = 2;
  @Input() autoFocus: boolean = false;
  @Input() typeahead: Subject<string>;
  @Input() dropdownPosition: string = 'auto';
  @Input() virtualScroll: boolean = false;
  @Input() className: string = 'mi-form-ng-select';
  @Input() maxSelectedItems: number;
  @Input() appendTo: string = 'body';

  @Output() change = new EventEmitter();
  @Output() remove = new EventEmitter();
  @Output() clear = new EventEmitter();
  @Output() blur = new EventEmitter();
  @Output() open = new EventEmitter();
  @Output() close = new EventEmitter();
  @Output() scroll = new EventEmitter();
  @Output() scrollToEnd = new EventEmitter();

  private _selectedCas: any;
  get selectedCas(): any {
    return this._selectedCas;
  }
  set selectedCas(v: any) {
    this._selectedCas = v;
  }

  maxItems: number;
  isReachMaxItems: boolean;

  // if allow search is not allow clear on backspace
  get clearOnBackspace(): boolean {
    return !this.searchable;
  }

  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;

  constructor(private _cd: ChangeDetectorRef, private _casDialogSv: CasDialogService, private toastSv: ToastService) {}

  ngOnInit() {
    this._casDialogSv.casSelectComponent.push(this.casNgSelect);
  }

  writeValue(value: any) {
    this.selectedCas = value;
    this._cd.markForCheck();
  }

  onChange() {
    this._onChangeCallback(this.selectedCas);
    this._cd.markForCheck();
  }

  registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  search(name: string) {
    if (this.typeahead) {
      this.typeahead.next(name);
    }
    this._cd.markForCheck();
  }

  emitChanged(event) {
    this.change.emit(event);

    if (event && this.maxSelectedItems && event.length === this.maxSelectedItems && this.isReachMaxItems) {
      this.toastSv.warning(`オプションは${this.maxSelectedItems}個まで設定できます`);
      return;
    }
    this.isReachMaxItems = event && event.length < this.maxSelectedItems ? false : true;
  }

  emitRemoved(event) {
    this.remove.emit(event);
  }

  emitClear(event) {
    this.clear.emit(event);
  }

  emitBlur(event) {
    this.blur.emit(event);
    this._onTouchedCallback();
  }

  emitOpened(event) {
    this.open.emit(event);
    if (!this.searchable) {
      return;
    }

    if (this.autoFocus) {
      setTimeout(() => {
        if (this.inputSearch && this.inputSearch.nativeElement) {
          this.inputSearch.nativeElement.focus();
        }
      }, 200);
    }

    if (this.selectedCas && this.selectedCas.length === this.maxSelectedItems) {
      this.isReachMaxItems = true;
    }

    this.maxItems = this.maxSelectedItems;
  }

  emitClose(event) {
    if (this.typeahead) {
      this.typeahead.next('');
    }
    this.close.emit(event);

    this.maxItems = undefined;
  }

  emitScroll(event) {
    this.scroll.emit(event);
  }

  emitScrollToEnd(event) {
    this.scrollToEnd.emit(event);
  }
}
