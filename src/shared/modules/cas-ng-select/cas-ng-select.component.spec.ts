import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasNgSelectComponent } from './cas-ng-select.component';

describe('CasNgSelectComponent', () => {
  let component: CasNgSelectComponent;
  let fixture: ComponentFixture<CasNgSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasNgSelectComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasNgSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
