import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule, NgSelectConfig } from '@ng-select/ng-select';
import { CAS_NG_SELECT_DEFAULT_CONFIG } from 'shared/constants/select-config';

import { CasNgSelectComponent } from './cas-ng-select.component';
import { FormsModule } from '@angular/forms';
import { UnFocusModule } from 'shared/directive/un-focus/us-focus.module';

@NgModule({
  declarations: [CasNgSelectComponent],
  imports: [CommonModule, NgSelectModule, FormsModule, UnFocusModule],
  providers: [
    {
      provide: NgSelectConfig,
      useValue: CAS_NG_SELECT_DEFAULT_CONFIG,
    },
  ],
  exports: [CasNgSelectComponent],
})
export class CasNgSelectModule {}
