import { Component, OnInit, forwardRef, Output, Input, EventEmitter } from '@angular/core';

import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  /* tslint:disable-next-line: no-use-before-declare */
  useExisting: forwardRef(() => InputQuantityComponent),
  provide: NG_VALUE_ACCESSOR,
  multi: true,
};

@Component({
  selector: 'app-input-quantity',
  templateUrl: './input-quantity.component.html',
  styleUrls: ['./input-quantity.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
})
export class InputQuantityComponent implements OnInit, ControlValueAccessor {
  @Input() name: string = 'quantity';
  @Input() containerClass: string = '';
  @Input() disabled: boolean;
  @Output() remove = new EventEmitter();
  @Output() change = new EventEmitter();

  private _quantity: number;
  get quantity(): number {
    return this._quantity;
  }
  set quantity(v: number) {
    if (v !== this.quantity) {
      this._quantity = v;
      this._onChangeCallback(v);
    }
  }

  quantity_input: number;
  isOpen: boolean;

  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;
  constructor() {}

  ngOnInit() {}

  writeValue(value: any) {
    this.quantity = value;
    if (!this.quantity_input) {
      this.quantity_input = this.quantity || 0;
    }
  }

  registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  minusQuantity() {
    if (this.quantity_input === 0) {
      return;
    }
    this.quantity_input--;
  }

  plusQuantity() {
    if (this.quantity_input === 100) {
      return;
    }
    this.quantity_input++;
  }

  removeQuantity() {
    this.quantity = 0;
    this.quantity_input = 0;
    this.remove.emit(this.quantity);
    this._onTouchedCallback();
  }

  addQuantity() {
    this.quantity = this.quantity_input;
    this.change.emit(this.quantity_input);
    if (!this.quantity) {
      this.remove.emit(this.quantity);
    }
  }
}
