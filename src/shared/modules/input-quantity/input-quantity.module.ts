import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PopoverModule, PopoverDirective } from 'ngx-bootstrap/popover';
import { InputQuantityComponent } from './input-quantity.component';

@NgModule({
  declarations: [InputQuantityComponent],
  imports: [CommonModule, FormsModule, PopoverModule.forRoot()],
  exports: [InputQuantityComponent],
  providers: [PopoverDirective],
})
export class InputQuantityModule {}
