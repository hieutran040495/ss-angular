import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-keyboard-number',
  templateUrl: './keyboard-number.component.html',
  styleUrls: ['./keyboard-number.component.scss'],
})
export class KeyboardNumberComponent implements OnInit {
  @Input('keyboardNumber') keyboardNumber: boolean = false;

  @Output() digitClicked: EventEmitter<number> = new EventEmitter<number>();
  @Output() confirmClicked: EventEmitter<null> = new EventEmitter<null>();
  @Output() currentClicked: EventEmitter<string> = new EventEmitter<string>();
  @Output() deleteClicked: EventEmitter<null> = new EventEmitter<null>();

  constructor() {}

  ngOnInit() {}

  public clickedDigit(digit: number) {
    this.digitClicked.emit(digit);
  }

  public confirmed() {
    this.confirmClicked.emit(null);
  }

  public getCurrentTime() {
    this.currentClicked.emit(moment().format('HH:mm:ss'));
  }

  public delete() {
    this.deleteClicked.emit(null);
  }
}
