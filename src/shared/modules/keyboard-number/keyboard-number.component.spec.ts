import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyboardNumberComponent } from './keyboard-number.component';

describe('KeyboardNumberComponent', () => {
  let component: KeyboardNumberComponent;
  let fixture: ComponentFixture<KeyboardNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KeyboardNumberComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyboardNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
