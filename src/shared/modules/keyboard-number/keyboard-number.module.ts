import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KeyboardNumberComponent } from './keyboard-number.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [KeyboardNumberComponent],
  exports: [KeyboardNumberComponent],
})
export class KeyboardNumberModule {}
