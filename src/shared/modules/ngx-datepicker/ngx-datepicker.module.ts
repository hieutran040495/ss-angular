import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxDatepickerComponent } from './ngx-datepicker.component';
import { FormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';

@NgModule({
  declarations: [NgxDatepickerComponent],
  imports: [CommonModule, FormsModule, BsDatepickerModule.forRoot()],
  exports: [NgxDatepickerComponent],
  providers: [BsLocaleService],
})
export class NgxDatepickerModule {}
