import { Component, OnInit, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';

import { BS_DATEPICKER_CONFIG } from 'shared/constants/datepicker-config';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { jaLocale, deLocale } from 'ngx-bootstrap/locale';

import * as moment from 'moment';
import { ClientWorkingTimeWeek } from 'shared/models/client-working-week';
import { ClientWorkingsQuery } from 'shared/states/client_workings';
import { DatePicker } from 'shared/mixins/date-picker';
import { applyMixins } from 'shared/utils/apply-mixins';

const locales = [jaLocale, deLocale];
locales.forEach((locale) => defineLocale(locale.abbr, locale));
const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  /* tslint:disable-next-line: no-use-before-declare */
  useExisting: forwardRef(() => NgxDatepickerComponent),
  provide: NG_VALUE_ACCESSOR,
  multi: true,
};

@Component({
  selector: 'app-ngx-datepicker',
  templateUrl: './ngx-datepicker.component.html',
  styleUrls: ['./ngx-datepicker.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
})
export class NgxDatepickerComponent implements OnInit, ControlValueAccessor, DatePicker {
  onShownDatepicker: (event) => void;
  _onClickNextPrev: () => void;
  _checkAndAddClassVisible: () => Promise<void>;
  _handleDayOff: (date: moment.Moment, el: JQuery, workingTime: ClientWorkingTimeWeek) => void;

  @Output() change = new EventEmitter<string>();

  @Input('name') name: string = 'date';
  @Input('bsConfig') bsConfig: Partial<BsDatepickerConfig>;
  @Input('placeholder') placeholder: string = 'Select date';
  @Input('minDate') minDate: string;
  @Input('maxDate') maxDate: string;
  @Input('locale') locale: string = 'ja';
  @Input('isDaterangepicker') isDaterangepicker: boolean;
  @Input('validatorError') validatorError: string;
  @Input() disabled = false;
  @Input('required') required: boolean = false;
  @Input('isClearPicker') isClearPicker: boolean = false;
  @Input('placement') placement: string = 'left top';

  defaultBsConfig: Partial<BsDatepickerConfig> = BS_DATEPICKER_CONFIG;

  private _selectedDate: string = moment().format('YYYY/MM/DD');
  get selectedDate(): string {
    return this._selectedDate;
  }

  set selectedDate(v: string) {
    this._selectedDate = v;
    this._onChangeCallback(v);
  }

  constructor(private bsLocaleSv: BsLocaleService, private clientWorkingsQuery: ClientWorkingsQuery) {}

  /**
   * Overriden for interface ControlValueAccessor
   */
  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;

  ngOnInit() {
    this.bsLocaleSv.use(this.locale);
    if (this.bsConfig) {
      this.defaultBsConfig = Object.assign(this.defaultBsConfig, this.bsConfig);
    }
  }

  onTouched() {
    this._onTouchedCallback();
  }

  writeValue(value: any) {
    this.selectedDate = value;
  }

  registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  onChange() {
    this.change.emit(this.selectedDate);
  }

  clearPicker() {
    this.selectedDate = null;
  }

  onblur(event) {
    $(event.target).trigger('blur');
  }

  _getWorkingTime(): Promise<ClientWorkingTimeWeek> {
    return this.clientWorkingsQuery.getWorkingTime();
  }
}

applyMixins(NgxDatepickerComponent, [DatePicker]);
