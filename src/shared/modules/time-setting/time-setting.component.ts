import {
  Component,
  OnInit,
  forwardRef,
  Input,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Output,
  EventEmitter,
} from '@angular/core';
import { DURATIONS } from 'shared/constants/time-setting';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  /* tslint:disable-next-line: no-use-before-declare */
  useExisting: forwardRef(() => TimeSettingComponent),
  provide: NG_VALUE_ACCESSOR,
  multi: true,
};

@Component({
  selector: 'app-time-setting',
  templateUrl: './time-setting.component.html',
  styleUrls: ['./time-setting.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimeSettingComponent implements OnInit, ControlValueAccessor {
  @Input() disabled: boolean;
  @Input() isWalkin: boolean;

  @Output() eventSelect: EventEmitter<any> = new EventEmitter<any>();

  private _duration: number = 1;
  get duration(): number {
    return this._duration;
  }
  set duration(v: number) {
    this._duration = v;
    this._onChangeCallback(this._duration);
  }

  DURATIONS = DURATIONS;

  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;
  constructor(private _cd: ChangeDetectorRef) {}

  ngOnInit() {}

  /**
   * Overriden for interface ControlValueAccessor
   */
  public onTouched() {
    this._onTouchedCallback();
  }

  public writeValue(value: number) {
    if (value) {
      this.duration = value;
      this._cd.markForCheck();
    }
  }

  public registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  setTime(time: number) {
    if (this.duration === time) {
      return;
    }
    this.duration = time;
    this.eventSelect.emit();
  }

  isActiveBtn(time: number): boolean {
    return this.duration === time;
  }

  getClassName(time: number): string {
    if (!this.isActiveBtn(time)) {
      return '';
    }
    return this.isWalkin ? 'active--blue' : 'active--orange';
  }
}
