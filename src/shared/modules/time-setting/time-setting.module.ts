import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeSettingComponent } from './time-setting.component';
import { FormsModule } from '@angular/forms';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

@NgModule({
  declarations: [TimeSettingComponent],
  imports: [CommonModule, FormsModule, PerfectScrollbarModule],
  exports: [TimeSettingComponent],
})
export class TimeSettingModule {}
