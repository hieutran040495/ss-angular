import { Group, ContainerConfig, Rect, Text, Image, RectConfig } from 'konva';
import { CONFIG_TITLE, CONFIG_PRICE, CONFIG_ITEM, ITEM_STYLES } from './item-layer';
import { RichMenuItem } from 'shared/models/rich-menu-item';

export class MenuItem extends Group {
  public menu: RichMenuItem;
  private _background: Rect;
  private _title: Text;
  private _price: Text;
  private _iconPlus: Image;

  constructor(containerConfig: ContainerConfig, menu: RichMenuItem) {
    super(containerConfig);
    this.menu = menu;
    this._initBackground();
    this._initTitle(menu.name);
    this._initPrice(menu.total_price_display);
    this._initIcon('/assets/icons/icon-plus.png');
  }

  private _initBackground(config?: RectConfig) {
    this._background = new Rect({
      ...CONFIG_ITEM,
      ...config,
    });
    this.add(this._background);
  }

  private _initTitle(menu_name: string) {
    this._title = new Text({
      ...CONFIG_TITLE,
      text: menu_name,
    });
    this.add(this._title);
  }

  private _initPrice(menu_price: string) {
    this._price = new Text({
      ...CONFIG_PRICE,
      text: menu_price,
    });
    this.add(this._price);
  }

  private _initIcon(imageUrl: string) {
    const imgY: number = ITEM_STYLES.HEIGHT / 2 - ITEM_STYLES.IMG_PLUS_HEIGHT / 2;
    const imgX: number = ITEM_STYLES.WIDTH - 25;

    Image.fromURL(imageUrl, (image) => {
      image.x(imgX);
      image.y(imgY);
      this._iconPlus = image;
      this.add(this._iconPlus);
      this.draw();
    });
  }
}
