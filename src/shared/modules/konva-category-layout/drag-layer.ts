import { Layer, LayerConfig } from 'konva';

export class DragLayer extends Layer {
  constructor(layerConfig?: LayerConfig) {
    super(layerConfig);
  }
}
