import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Input, OnDestroy } from '@angular/core';
import { KonvaCategoryStage } from './stage';

import { MAIN_IMAGE } from './item-layer';
import { STYLE_SETTING_DEFAULT } from 'shared/constants/style-config-default';

import { RichMenu } from 'shared/models/rich-menu';
import { ItemCategory } from 'shared/models/item-category';
import { SETTING_RICH_MENU } from 'shared/enums/event-emitter';

import { RichMenuService } from 'shared/http-services/rich-menu.service';
import { TopScreenService } from 'shared/http-services/top-screen.service';
import { ItemCategoryService } from 'shared/http-services/item-category.service';
import { ToastService } from 'shared/logical-services/toast.service';

import { Subject, Subscription } from 'rxjs';

interface ScreenSize {
  width: number;
  height: number;
}

@Component({
  selector: 'app-konva-category-layout',
  templateUrl: './konva-category-layout.component.html',
  styleUrls: ['./konva-category-layout.component.scss'],
})
export class KonvaCategoryLayoutComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('CategoryLayout') categoryLayout: ElementRef;
  private _stage: KonvaCategoryStage;

  @Input('onEventEmitter') onEventEmitter: Subject<any>;

  private _screenSize: ScreenSize;
  @Input('screenSize')
  public get screenSize(): ScreenSize {
    return this._screenSize;
  }
  get screenWidth(): number {
    return this.screenSize.width * this._scale;
  }
  get screenHeight(): number {
    return this.screenSize.height * this._scale;
  }
  public set screenSize(v: ScreenSize) {
    this._screenSize = v;
    this._oldScale = Number(this._scale);
    this._scale = (940 / this.screenSize.width) * 0.6;

    this._setScreenSize();
  }

  private richMenu: RichMenu = new RichMenu();

  private _itemCategory: ItemCategory;
  @Input('itemCategory')
  get itemCategory(): ItemCategory {
    return this._itemCategory;
  }
  set itemCategory(v: ItemCategory) {
    this._itemCategory = v;
  }

  private _page: number;
  @Input('page')
  get page(): number {
    return this._page;
  }
  set page(v: number) {
    this._page = v;
    this.richMenu = this.itemCategory.order_rich_menu_settings[v] || new RichMenu();
    this.richMenu.bg_color = this.richMenu.bg_color || STYLE_SETTING_DEFAULT.bg_color;
    this.richMenu.text_color = this.richMenu.text_color || STYLE_SETTING_DEFAULT.text_color;
    this.richMenu.font_family = this.richMenu.font_family || STYLE_SETTING_DEFAULT.font_family;
    this.richMenu.page = 1;
    this.richMenu.pattern = 1;
  }

  private _scale = 1;
  private _oldScale = 1;
  private _fileUpload: File;

  fileAccept: string = 'image/jpeg,image/png,image/jpg';
  private maxFileSize: number = 3145728; // 3MB
  private _subEvent: Subscription;

  lastDist = 0;
  activeShape = null;

  constructor(
    private _richMenuSv: RichMenuService,
    private topScreenSv: TopScreenService,
    private itemCategorySv: ItemCategoryService,
    private toastSv: ToastService,
  ) {}

  ngOnInit() {
    this.categoryEventEmitter();
  }

  ngAfterViewInit() {
    this._initLayoutSettingCategoryLayout();
    this.eventDraggable();
  }

  ngOnDestroy() {
    if (this._subEvent) {
      this._subEvent.unsubscribe();
    }
  }

  private eventDraggable() {
    this._stage.on('dragend', (evt: any) => {
      this.removeMenuItem(evt);
    });

    this._stage.on('click touchstart tap', (event: any) => {
      if (event.target.getName() === MAIN_IMAGE.name) {
        this.activeShape = event.target;
      } else {
        this.activeShape = null;
      }
    });

    this._stage.on('dragmove', (event: any) => {
      const evt = event.evt;

      const touch1 = evt.targetTouches ? evt.targetTouches[0] : undefined;
      const touch2 = evt.targetTouches ? evt.targetTouches[1] : undefined;

      if (touch1 && touch2 && this.activeShape) {
        const dist = this.getDistance(
          {
            x: touch1.clientX,
            y: touch1.clientY,
          },
          {
            x: touch2.clientX,
            y: touch2.clientY,
          },
        );

        if (!this.lastDist) {
          this.lastDist = dist;
        }

        const scale = (this.activeShape.scaleX() * dist) / this.lastDist;

        this.activeShape.scaleX(scale);
        this.activeShape.scaleY(scale);
        this._stage.mainLayer.draw();
        this.lastDist = dist;
      }
    });

    this._stage.on('contentTouchend', () => {
      this.lastDist = 0;
    });
  }

  getDistance(p1, p2) {
    return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
  }

  private removeMenuItem(evt: any) {
    const currentPos = evt.currentTarget.getPointerPosition();

    const maxPosX = this._stage.mainLayer.width();
    const maxPosY = this._stage.mainLayer.height();

    if (currentPos.x < 5 || currentPos.y < 5 || currentPos.x > maxPosX - 5 || currentPos.y > maxPosY - 5) {
      if (evt.target.menu) {
        this.onEventEmitter.next({
          type: SETTING_RICH_MENU.REMOVE_MENU_ITEM,
          data: evt.target.menu,
        });
        evt.target.destroy();
        this._stage.draw();
        this.toastSv.info('メニューが一旦リッチメニューから削除されます');
      }
    }
  }

  private categoryEventEmitter() {
    this._subEvent = this.onEventEmitter.subscribe((res) => {
      if (!res || !res.type) {
        return;
      }

      switch (res.type) {
        case SETTING_RICH_MENU.CHANGE_RICH_MENU:
          res.data.pos_x /= this._scale;
          res.data.pos_y /= this._scale;

          this._stage.mainLayer.addMenuItem(res.data, this._scale);
          break;
        case SETTING_RICH_MENU.SAVE_RICH_MENU:
          this._saveRichMenu();
          break;
      }
    });
  }

  private _setScreenSize() {
    if (!this._stage) {
      return;
    }

    this._stage.width(this.screenWidth);
    this._stage.height(this.screenHeight);

    if (this._stage.mainLayer) {
      this._stage.mainLayer.updateScale(this._oldScale, this._scale);
      this._stage.mainLayer.addMenuItems(this.richMenu.items, this._scale);
    }

    this._stage.draw();
  }

  private _initLayoutSettingCategoryLayout() {
    this._stage = new KonvaCategoryStage({
      container: this.categoryLayout.nativeElement,
    });

    this._stage.initLayer();
    this._stage.initRichMenu(this.richMenu, this._scale);

    this._setScreenSize();
  }

  private _saveRichMenu() {
    if (this._fileUpload) {
      this._getPathUploadS3();
      return;
    }

    this._settingRecommendScreen();
  }

  private _getPathUploadS3() {
    const data = {
      quantity: 1,
    };

    this.topScreenSv.presignedUrls(data).subscribe(
      (res) => {
        this._uploadImageToS3(res.data[0]);
      },
      (errors: any) => {
        this.toastSv.error(errors);
      },
    );
  }

  private _uploadImageToS3(data: any) {
    this.topScreenSv.publicImagePath(data.url, this._fileUpload, this._fileUpload.type).subscribe(
      (res: any) => {
        this.richMenu.image_path = data.path;
        this._settingRecommendScreen();
      },
      (errors) => {
        this.toastSv.error(errors);
      },
    );
  }

  private _settingRecommendScreen() {
    this.itemCategory.order_rich_menu_settings[this.page] = this._stage.mainLayer.dataToJSON(this._scale);
    this.itemCategorySv
      .settingRecommendScreen(this.itemCategory.id, 'order', this.itemCategory.settingOrderToJSON())
      .subscribe(
        () => {
          this.onEventEmitter.next({
            type: SETTING_RICH_MENU.CLOSE_MODAL_SETTING_RICH_MENU,
          });
          this.toastSv.success('リッチメニューを設定しました');
        },
        (errors) => {
          this.onEventEmitter.next({
            type: SETTING_RICH_MENU.ERROR_RICH_MENU,
          });
          this.toastSv.error(errors);
        },
      );
  }

  handleFile(files: FileList) {
    const file: File = files.item(0);
    if (file) {
      if (this.fileAccept.indexOf(file.type) === -1) {
        this.toastSv.info(`jpg, jpegとpngのファイルでアップロードしてください`);
        return false;
      }

      if (file.size > this.maxFileSize) {
        this.toastSv.info(`サイズが3MBを超える画像はアップロードできません`);
        return false;
      }

      this._fileUpload = file;
      // Render file preview
      const reader: FileReader = new FileReader();
      reader.onloadend = (e: any) => {
        this.richMenu.image_url = e.target.result;
        // Get Width & Height of Image
        const newImg = new Image();
        newImg.src = e.target.result;
        newImg.onload = (img: any) => {
          this.richMenu.image_width = img.composedPath()[0].width;
          this.richMenu.image_height = img.composedPath()[0].height;
          this.richMenu.ratio = 1;
          this.richMenu.pos_x = 0;
          this.richMenu.pos_y = 0;
          this._stage.updateMainImage(this.richMenu, this._scale);
        };
      };
      reader.readAsDataURL(file);
    }
  }

  uploadImage(el: any) {
    el.click();
  }
}
