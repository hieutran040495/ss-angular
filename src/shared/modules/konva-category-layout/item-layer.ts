export const MAIN_IMAGE = {
  name: 'main-image',
  draggable: true,
};

export const ITEM_STYLES = {
  WIDTH: 245,
  HEIGHT: 106,
  BACKGROUND: 'rgba(0, 0, 0, 0.4)',
  TITLE_FONT_SIZE: 16,
  TITLE_WIDTH: 220,
  PRICE_FONT_SIZE: 16,
  PRICE_WIDTH: 145,
  IMG_PLUS_WIDTH: 71,
  IMG_PLUS_HEIGHT: 71,
};

export const CONFIG_ITEM = {
  width: ITEM_STYLES.WIDTH,
  height: ITEM_STYLES.HEIGHT,
  fill: ITEM_STYLES.BACKGROUND,
  stroke: ITEM_STYLES.BACKGROUND,
  strokeWidth: 0,
  opacity: 0.9,
};

export const CONFIG_TITLE = {
  fontSize: ITEM_STYLES.TITLE_FONT_SIZE,
  fontStyle: 'bold',
  align: 'left',
  verticalAlign: 'middle',
  ellipsis: true,
  shadowColor: 'red',
  wrap: 'none',
  fill: '#fff',
  name: 'menu-name',
  x: 15,
  y: 30,
  width: ITEM_STYLES.WIDTH - 45,
};

export const CONFIG_PRICE = {
  fontSize: ITEM_STYLES.PRICE_FONT_SIZE,
  fontStyle: 'bold',
  align: 'right',
  verticalAlign: 'middle',
  ellipsis: true,
  shadowColor: 'red',
  wrap: 'none',
  fill: '#fff',
  name: 'menu-price',
  x: 15,
  y: 65,
  width: ITEM_STYLES.WIDTH - 45,
};
