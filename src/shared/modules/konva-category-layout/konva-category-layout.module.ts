import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KonvaCategoryLayoutComponent } from './konva-category-layout.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [KonvaCategoryLayoutComponent],
  exports: [KonvaCategoryLayoutComponent],
})
export class KonvaCategoryLayoutModule {}
