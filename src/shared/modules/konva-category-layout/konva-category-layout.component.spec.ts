import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KonvaCategoryLayoutComponent } from './konva-category-layout.component';

describe('KonvaCategoryLayoutComponent', () => {
  let component: KonvaCategoryLayoutComponent;
  let fixture: ComponentFixture<KonvaCategoryLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KonvaCategoryLayoutComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KonvaCategoryLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
