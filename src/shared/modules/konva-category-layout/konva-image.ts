import { Image, ImageConfig } from 'konva';

export class KonvaImage extends Image {
  constructor(config?: ImageConfig) {
    super(config);
  }
}
