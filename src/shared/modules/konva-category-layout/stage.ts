import { Stage, StageConfig } from 'konva';
import { MainLayer } from './main-layer';
import { RichMenu } from 'shared/models/rich-menu';

export class KonvaCategoryStage extends Stage {
  private _mainLayer: MainLayer;
  public get mainLayer() {
    return this._mainLayer;
  }

  constructor(stageConfig: StageConfig) {
    super(stageConfig);
  }

  public initLayer() {
    this._initMainLayer();
  }

  public initRichMenu(richMenu: RichMenu, scale: number) {
    this._mainLayer.initRichMenu(richMenu, scale);
  }

  updateMainImage(richMenu: RichMenu, scale: number) {
    this._mainLayer.changeImage(richMenu, scale);
  }

  private _initMainLayer() {
    this._mainLayer = new MainLayer();
    this.add(this._mainLayer);
  }
}
