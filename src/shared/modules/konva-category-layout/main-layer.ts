import { Layer, LayerConfig } from 'konva';
import { MenuItem } from './menu-item';
import { RichMenu } from 'shared/models/rich-menu';
import { RichMenuItem } from 'shared/models/rich-menu-item';
import { KonvaImage } from './konva-image';
import { MAIN_IMAGE } from './item-layer';

export class MainLayer extends Layer {
  mainImage: KonvaImage;

  private richMenu: RichMenu;

  constructor(layerConfig?: LayerConfig) {
    super(layerConfig);
  }

  public initRichMenu(richMenu: RichMenu, scale: number) {
    this.richMenu = richMenu;
    const imageObj = new Image();
    this.mainImage = new KonvaImage({
      image: imageObj,
      ...MAIN_IMAGE,
    });

    this.add(this.mainImage);

    imageObj.onload = () => {
      this.updateMainImg(imageObj, richMenu, scale);
    };

    imageObj.src = richMenu.image_url || '';
  }

  changeImage(richMenu: RichMenu, scale: number) {
    const imageObj = new Image();
    imageObj.onload = () => {
      this.updateMainImg(imageObj, richMenu, scale, false);
    };
    imageObj.src = richMenu.image_url;
  }

  private updateMainImg(imageObj: any, richMenu: RichMenu, scale: number, isAddItem: boolean = true) {
    this.mainImage.scale({
      x: scale,
      y: scale,
    });

    this.mainImage.width(richMenu.widthScale);
    this.mainImage.height(richMenu.heightScale);

    this.mainImage.x(richMenu.pos_x * scale);
    this.mainImage.y(richMenu.pos_y * scale);
    this.mainImage.image(imageObj);
    this.draw();
  }

  addMenuItems(richMenuItems: RichMenuItem[] = [], scale: number) {
    richMenuItems.forEach((menu) => this.addMenuItem(menu, scale));
  }

  public addMenuItem(menu: RichMenuItem, scale: number) {
    const m = new MenuItem(
      {
        draggable: true,
        scale: {
          x: scale,
          y: scale,
        },
        x: menu.pos_x * scale,
        y: menu.pos_y * scale,
        dragBoundFunc: (pos) => {
          const newY: number = pos.y < 72 ? 72 : pos.y;
          return {
            x: pos.x,
            y: newY,
          };
        },
      },
      menu,
    );

    this.add(m);

    this.draw();
  }

  public dataToJSON(scale: number): RichMenu {
    const data: RichMenu = this.richMenu;
    data.items = [];
    const childrens = this.getChildren();

    childrens.toArray().forEach((child) => {
      if (child instanceof KonvaImage) {
        data.pos_x = child.x() / scale;
        data.pos_y = child.y() / scale;
        const newWidth = (child.getWidth() * child.scaleX()) / scale;
        data.ratio = newWidth / this.richMenu.image_width;
      }

      if (child instanceof MenuItem) {
        data.items.push(
          new RichMenuItem().deserialize({
            ...child.menu,
            pos_x: child.x() / scale,
            pos_y: child.y() / scale,
          }),
        );
      }
    });

    return data;
  }

  public updateScale(oldScale: number, scale: number) {
    const childrens = this.getChildren();

    childrens.toArray().forEach((child) => {
      child.scale({
        x: scale,
        y: scale,
      });

      child.x((child.x() / oldScale) * scale);
      child.y((child.y() / oldScale) * scale);
    });

    this.draw();
  }
}
