import { Layer, Node, Transformer } from 'konva';

export class KonvaDragLayer extends Layer {
  transformer: Transformer;

  private _createTransformerIfNotExists() {
    if (!this.transformer) {
      this.transformer = new Transformer({
        rotateEnabled: true,
        resizeEnabled: true,
        keepRatio: true,
        enabledAnchors: ['top-left', 'top-right', 'bottom-left', 'bottom-right'],
      });
      this.add(this.transformer);
    }
  }

  attachTransformer(group: Node) {
    this._createTransformerIfNotExists();
    this.transformer.attachTo(group);
    this.draw();
  }

  detachTransformer() {
    if (this.hasTransformer()) {
      this.transformer.destroy();
      this.transformer = null;
      this.draw();
    }
  }

  hasTransformer() {
    return !!this.transformer;
  }
}
