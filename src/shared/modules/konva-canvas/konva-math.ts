import { SizeConfig, Vector2d } from 'konva';

export class KonvaMath {
  static haveIntersection(r1: SizeConfig, r2: SizeConfig) {
    return !(r2.x > r1.x + r1.width || r2.x + r2.width < r1.x || r2.y > r1.y + r1.height || r2.y + r2.height < r1.y);
  }

  static positionAfterGroup(parent: Vector2d, child: Vector2d) {
    return { x: child.x - parent.x, y: child.y - parent.y };
  }

  static positionAfterUnroup(parent: Vector2d, child: Vector2d) {
    return { x: parent.x + child.x, y: parent.y + child.y };
  }
}
