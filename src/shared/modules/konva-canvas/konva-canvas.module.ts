import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { KonvaCanvasComponent } from './konva-canvas.component';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [KonvaCanvasComponent],
  imports: [CommonModule, PopoverModule.forRoot(), ModalModule.forRoot(), FormsModule, RouterModule],
  exports: [KonvaCanvasComponent],
})
export class KonvaCanvasModule {}
