import { Layer, Stage, Node, Group, StageConfig } from 'konva';
import { KonvaGroup } from './konva-group';
import { KonvaMath } from './konva-math';
import { KonvaDragLayer } from './konva-drag-layer';
import { Table } from 'shared/models/table';

export enum KonvaStageAction {
  GROUP = 'onGroupTables',
  UNGROUP = 'onUngroupTables',
  REMOVE = 'onRemoveTable',
  ZOOM_IN = 'onZoomIn',
  ZOOM_OUT = 'onZoomOut',
}

interface KonvaStageSize {
  width: number;
  height: number;
}

interface KonvaStageConfig extends StageConfig {
  isUpdateMode: boolean;
}

export class KonvaStage extends Stage {
  private _maxScale: number = 2;
  private _minScale: number = 0.5;

  private _tables: any = {};
  private _mainLayer: Layer;
  get mainLayer(): Layer {
    return this._mainLayer;
  }
  private _dragLayer: KonvaDragLayer;

  private _isUpdateMode = false;

  defaultSize: KonvaStageSize = {
    width: this.width(),
    height: this.height(),
  };

  constructor(config: KonvaStageConfig) {
    super(config);

    this._isUpdateMode = config.isUpdateMode;
  }

  initLayer() {
    this._initMainLayer();

    if (this._isUpdateMode) {
      this._initDragLayer();
      this._attachEvents();
    }
  }

  /**
   * Main layer for contain all table's sharps
   */
  private _initMainLayer() {
    this._mainLayer = new Layer();
    this.add(this._mainLayer);
    this._onDragEnd();
  }

  private _onDragEnd() {
    this.on('dragend', (e) => {
      // handle dragend group
    });
  }

  /**
   * Drag layer for handling tranformation
   */
  private _initDragLayer() {
    this._dragLayer = new KonvaDragLayer();
    this.add(this._dragLayer);
  }

  private _attachEvents() {
    this.on('click tap', (e) => {
      if (<any>e.target === this) {
        this._dragLayer.detachTransformer();

        return;
      }
    });
  }

  onUngroupTables() {
    if (!this._dragLayer.hasTransformer()) {
      return;
    }

    const targetGroup = <Group>this._dragLayer.transformer;
    const targetNode = <Group>this._dragLayer.transformer.getNode();

    if (targetNode.name() !== 'group_tables') {
      return;
    }

    const childs = targetNode.getChildren();

    childs.toArray().forEach((item: Group, index: number) => {
      const heightGroup = targetGroup.height() > 140 ? targetGroup.height() / 2 : targetGroup.height();
      const positionX = targetGroup.x();
      const positionY = index === 1 ? targetGroup.y() : targetGroup.y() + heightGroup;
      const scaleX = item.scaleX() > 1 ? item.scaleX() : targetNode.scaleX();
      const scaleY = item.scaleY() > 1 ? item.scaleY() : targetNode.scaleY();

      const itemWithNewPos = item.clone({
        x: positionX,
        y: positionY,
        draggable: true,
        id: item.id(),
        scale: {
          x: scaleX,
          y: scaleY,
        },
        rotation: targetNode.rotation(),
      });

      if (index === childs.length - 1) {
        this._dragLayer.attachTransformer(this.addTable(itemWithNewPos.toJSON(), false));
        return;
      }

      this.addTable(itemWithNewPos.toJSON(), false);
    });

    targetNode.destroy();

    this.mainLayer.draw();
  }

  onRemoveTable(callback: Function) {
    if (!this._dragLayer.hasTransformer()) {
      return;
    }

    const target = <Group>this._dragLayer.transformer.getNode();
    if (target.name() === 'group_tables') {
      const childs = target.getChildren();

      childs.toArray().map((item: Group) => {
        callback(item.id());
      });
    } else {
      callback(target.id());
    }

    target.destroy();

    // Remove transformer and redraw
    this._dragLayer.detachTransformer();
    this._mainLayer.draw();
  }

  onGroupTables() {
    if (!this._dragLayer.hasTransformer()) {
      return;
    }

    const target = <Group>this._dragLayer.transformer.getNode();

    if (target.name() === 'group_tables') {
      return;
    }

    const newGroup: Group = new Group({
      draggable: true,
      x: 0,
      y: 0,
      name: 'group_tables',
    });

    this._mainLayer
      .getChildren()
      .toArray()
      .forEach((group: Group) => {
        if (group === target) {
          return;
        }

        if (KonvaMath.haveIntersection(group.getClientRect(), target.getClientRect())) {
          if (newGroup.id()) {
            newGroup.id(newGroup.id() + '_' + group.id());
          } else {
            newGroup.id(group.id());
          }

          if (group.name() === 'group_tables') {
            group
              .getChildren()
              .toArray()
              .forEach((item: Group) => {
                item
                  .clone({
                    draggable: false,
                    id: item.id(),
                    x: group.x() + item.x(),
                    y: group.y() + item.y(),
                  })
                  .moveTo(newGroup);
              });
          } else {
            group
              .clone({
                draggable: false,
                id: group.id(),
                x: group.x() - newGroup.x(),
                y: group.y() - newGroup.y(),
              })
              .moveTo(newGroup);
          }

          group.destroy();
        }
      });

    if (target.name() === 'group_tables') {
      // add child's target to new group
      target
        .getChildren()
        .toArray()
        .forEach((item: Group) => {
          item
            .clone({
              draggable: false,
              id: item.id(),
              x: target.x() + item.x(),
              y: target.y() + item.y(),
            })
            .moveTo(newGroup);
        });
    } else {
      // add target to new group
      target
        .clone({
          draggable: false,
          id: target.id(),
        })
        .moveTo(newGroup);
      newGroup.id(newGroup.id() + '_' + target.id());
    }

    if (newGroup.getChildren().length <= 1) {
      return;
    }

    target.destroy();

    KonvaGroup.attachMouseEvents(newGroup, this._mainLayer, this._dragLayer);
    KonvaGroup.attachTransformEvents(newGroup);
    KonvaGroup.attachClickEvent(newGroup, (id: string) => {
      this._dragLayer.attachTransformer(newGroup);
    });
    this._dragLayer.attachTransformer(newGroup);

    this._mainLayer.add(newGroup);
    this._mainLayer.draw();
  }

  initContentTable(data: any, tables: Table[]) {
    const tableGroup: Group = Node.create(data);

    if (!tables.length) {
      return tableGroup;
    }

    const sharps = tableGroup.find('.table');
    if (sharps) {
      sharps.toArray().forEach((sh) => {
        const parent = sh.getParent();
        const table_id = +parent.getAttr('id');
        const index = tables.findIndex((menu: Table) => menu.id === table_id);
        if (index !== -1) {
          const start_time = `~${tables[index].current_reservation.start_at_display}`;
          sh.setAttr('fill', KonvaGroup.TABLE_COLOR_HAS_RESV);
          sh.setAttr('stroke', KonvaGroup.TABLE_COLOR_OVERLAP_HAS_RESV);
          const resv_time = KonvaGroup.generateTextResvTime(sh.getClassName(), start_time, sh.width());
          parent.add(resv_time);
        }
      });
    }
    return tableGroup;
  }

  addTable(data: any, requiredDraw = true, tables: Table[] = [], isChild = false) {
    const tableGroup: Group = this.initContentTable(data, tables);
    // position of table = postion of table + width of table by default 200px
    const positionX: number = tableGroup.position().x + 200;
    const positionY: number = tableGroup.position().y + 200;
    this.defaultSize = {
      width: positionX > this.defaultSize.width ? positionX : this.defaultSize.width,
      height: positionY > this.defaultSize.height ? positionY : this.defaultSize.height,
    };

    if (isChild) {
      return tableGroup;
    }

    if (this._isUpdateMode) {
      this._tables[`${tableGroup.id()}`] = tableGroup;
      KonvaGroup.attachMouseEvents(tableGroup, this._mainLayer, this._dragLayer);
      KonvaGroup.attachTransformEvents(tableGroup);
      KonvaGroup.attachClickEvent(tableGroup, (id: string) => {
        this._dragLayer.attachTransformer(this._tables[id]);
      });
    } else {
      tableGroup.draggable(false);
    }

    // Add to main layer
    this._mainLayer.add(tableGroup);
    if (requiredDraw) {
      this._mainLayer.draw();

      if (this._isUpdateMode) {
        this._dragLayer.detachTransformer();
      }
    }

    return tableGroup;
  }

  /**
   * ==============
   * = Zoom events
   * ==============
   */
  onZoomIn() {
    this.zoom(11 / 10, KonvaStageAction.ZOOM_IN);
  }

  onZoomOut() {
    this.zoom(10 / 11, KonvaStageAction.ZOOM_OUT);
  }

  private zoom(scale: number, type: string): void {
    const newScale = this.scaleX() * scale;

    const width = this.defaultSize.width * newScale;
    const height = this.defaultSize.height * newScale;

    if (newScale > this._maxScale || newScale < this._minScale) return;

    // when zoom in, re-calculate width and height value of stage
    // because table will be hidden when keep origin width, height
    if (type === KonvaStageAction.ZOOM_IN) {
      this.setAttrs({ width, height, scale: { x: newScale, y: newScale } });
    }

    // when zoom out, remove limited width and height value of stage
    // because user should change position of table out of origin width, height
    if (type === KonvaStageAction.ZOOM_OUT) {
      this.setAttrs({ scale: { x: newScale, y: newScale } });
    }
    this.batchDraw();
  }

  toData() {
    return this._mainLayer
      .getChildren()
      .toArray()
      .filter((child: Group) => {
        if (child.hasChildren()) {
          return child.toJSON();
        }
      });
  }
}
