import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  AfterViewInit,
  TemplateRef,
  ViewEncapsulation,
} from '@angular/core';
import { applyMixins } from 'shared/utils/apply-mixins';
import { Table } from 'shared/models/table';
import { Subject, Subscription } from 'rxjs';
import { KonvaDataEmitter, KONVA_EMITTER_TYPE } from './data-emitter';

import { KonvaStage, KonvaStageAction } from './konva-stage';
import { MODES } from 'shared/enums/modes';
import { TABLE_LAYOUT_TYPE } from 'shared/models/table-layout';
import { KonvaGroup } from './konva-group';
import { ClientService } from 'shared/http-services/client.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { TableService } from 'shared/http-services/table.service';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-konva-canvas',
  templateUrl: './konva-canvas.component.html',
  styleUrls: ['./konva-canvas.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class KonvaCanvasComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('KonvaCanvas') konvaCanvas: ElementRef;

  @Input('canvasHandleEvent') canvasHandleEvent: Subject<KonvaDataEmitter>;
  @Input('isLoading') isLoading: boolean;
  @Input('mode') mode: string;
  get isModeEdit(): boolean {
    return this.mode === MODES.EDIT;
  }

  private _tables: Table[];
  @Input('tables')
  get tables(): Table[] {
    return this._tables;
  }
  set tables(v: Table[]) {
    this._tables = v;
  }

  @Output()
  saveEventHandler: EventEmitter<any[]> = new EventEmitter<any[]>();

  @Output()
  eventChooseTable: EventEmitter<any> = new EventEmitter<any>();

  padding: number = 20;
  height: number = 1000;

  private _stage: KonvaStage;
  get canUpdate(): boolean {
    return true;
  }
  private _originLayout = [];

  konvaStageActions = KonvaStageAction;
  tableLayoutTypes = TABLE_LAYOUT_TYPE;

  private _selectedTable: Table;

  private subscriber: Subscription;
  private stageTables: Table[] = [];
  isLoadFloors: boolean = true;
  floors = [
    {
      label: `1階`,
      value: 0,
    },
  ];
  get isFirstFloor(): boolean {
    return this.floors.length === 1;
  }

  selectedFloor = 0;
  modalRef: BsModalRef;

  constructor(
    private _clientApi: ClientService,
    private _tableSv: TableService,
    private _toastService: ToastService,
    private _modalService: BsModalService,
  ) {}

  ngOnInit() {
    this._onEnventEmitter();
  }

  ngAfterViewInit(): void {
    this._stage = new KonvaStage({
      container: this.konvaCanvas.nativeElement,
      width: $('#konvaManager').innerWidth(),
      height: this.height,
      isUpdateMode: this.isModeEdit,
    });
    this._stage.initLayer();

    // Attach click event with view mode
    if (!this.isModeEdit) {
      this._stage.on('click tap', (e) => {
        const tableGroup: any = e.target.findAncestor('.table-group', true);
        if (tableGroup) {
          const index = this.stageTables.findIndex((table: Table) => table.id === +tableGroup.attrs.id);

          if (index !== -1) {
            this.eventChooseTable.emit(this.stageTables[index]);
          }
        }
      });
    }
  }

  private _onEnventEmitter() {
    this.subscriber = this.canvasHandleEvent.subscribe((res: KonvaDataEmitter) => {
      if (res.type === KONVA_EMITTER_TYPE.RELOAD_KONVA) {
        this._getLayouts(res.data);
      }
      if (res.type === KONVA_EMITTER_TYPE.RELOAD_KONVA_LAYOUT) {
        this._stage.destroyChildren();
        this._stage.initLayer();
        this._getLayouts(res.data);
      }
    });
  }

  changeFloor(e: number) {
    // keep current floor

    if (JSON.stringify(this._stage.toData()) !== JSON.stringify(this._originLayout[this.selectedFloor])) {
      this._originLayout[this.selectedFloor] = cloneDeep(this._stage.toData());
    }

    this.selectedFloor = e;
    this._stage.destroyChildren();
    this._stage.initLayer();
    this._renderLayout(cloneDeep(this._originLayout[this.selectedFloor]));
  }

  createFloor() {
    const tables = this.tables.filter((table: Table) => !table.has_layout);

    if (!tables.length) {
      this._toastService.warning('設定されていないテーブルはありません');
      return;
    }

    if (this.floors.length === 10) {
      this._toastService.warning('10 フロアを作成することができません');
      return;
    }
    const selectedFloor = this.floors.length;
    this.floors.push({
      value: selectedFloor,
      label: `${selectedFloor + 1}階`,
    });
    this._originLayout.push([]);

    this.changeFloor(selectedFloor);
    this._toastService.info(`フロア${selectedFloor + 1}を作成しました`);
  }

  removeFloor() {
    if (this.isFirstFloor) {
      return;
    }

    this._stage.mainLayer.getChildren((node) => {
      const tableID: string = node.getAttr('id').toString();
      for (let i = 0; i < this._tables.length; i++) {
        if (this._tables[i].id.toString() === tableID) {
          this._tables[i].has_layout = false;
          break;
        }
      }
    });

    this._stage.destroyChildren();
    this._stage.initLayer();

    this._originLayout.splice(this.selectedFloor, 1);
    this.renderFloors();
    this.selectedFloor = 0;
    this._renderLayout(cloneDeep(this._originLayout[this.selectedFloor]));
    this._toastService.info(`フロアを削除しました`);
    this.save();
  }

  confirmRemoveFloor(template: TemplateRef<any>) {
    this.openModalTemplate(template);
  }

  private _getLayouts(tables: Table[]) {
    this.isLoadFloors = true;

    return this._clientApi.fetchClient().subscribe(
      (res) => {
        if (!res.layout) {
          return;
        }

        this._originLayout = JSON.parse(res.layout);

        // TODO: add more floor
        if (!(this._originLayout instanceof Array) || !(this._originLayout[this.selectedFloor] instanceof Array)) {
          return;
        }

        this.renderFloors();
        this._renderLayout(cloneDeep(this._originLayout[this.selectedFloor]));
      },
      (errors) => {
        this._toastService.error(errors);
        this.isLoading = false;
        this.isLoadFloors = false;
      },
      () => {
        this.isLoading = false;
      },
    );
  }

  private renderFloors(): void {
    this.floors = this._originLayout.map((value, key) => {
      return {
        label: `${key + 1}階`,
        value: key,
      };
    });
    this.isLoadFloors = false;
  }

  private _renderLayout(layout: any) {
    if (this.isModeEdit) {
      return this._addTable(layout);
    }
    this.getCurrentResv(layout);
  }

  private _addTable(layout: any, tables?: Table[]) {
    if (!layout) {
      return;
    }

    layout.forEach((table) => {
      if (tables) {
        this._stage.addTable(table, false, tables);
        return;
      }
      this._stage.addTable(table, false);
    });

    this._stage.setAttr('width', this._stage.defaultSize.width);
    this._stage.draw();
  }

  private getCurrentResv(layout: any) {
    const opts: any = {
      with: 'currentReservation',
      has_layout: 1,
    };

    this._tableSv.getTables(opts).subscribe(
      (res) => {
        this.stageTables = res.data;
        const tables = res.data.filter((table: Table) => {
          return !!table.current_reservation;
        });
        this._addTable(layout, tables);
      },
      (error) => {
        this._toastService.error(error);
      },
    );
  }

  konvaAction(action: KonvaStageAction) {
    if (action === KonvaStageAction.REMOVE) {
      this._stage[action]((id) => {
        for (let i = 0; i < this._tables.length; i++) {
          if (this._tables[i].id.toString() === id.toString()) {
            this._tables[i].has_layout = false;
            break;
          }
        }
      });

      return;
    }

    this._stage[action]();
  }

  ngOnDestroy() {
    this.subscriber.unsubscribe();
  }

  selectTable(table: Table) {
    this._selectedTable = table;
  }

  changeTableType(type: string) {
    if (!this._stage || !this._selectedTable) {
      return;
    }

    const offsetScrollX = this.konvaCanvas.nativeElement.scrollLeft + this.konvaCanvas.nativeElement.offsetWidth / 2;
    const offsetScrollY = this.konvaCanvas.nativeElement.scrollTop + this.konvaCanvas.nativeElement.offsetHeight / 2;

    KonvaGroup.DEFAULT_GROUP.x = offsetScrollX - KonvaGroup.DEFAULT_WIDTH;
    KonvaGroup.DEFAULT_GROUP.y = offsetScrollY - KonvaGroup.DEFAULT_HEIGHT;

    const tableGroup = KonvaGroup.generate(
      this._selectedTable.id,
      type,
      `${this._selectedTable.stage_name}\n(${this._selectedTable.quantity}人)`,
    );
    this._stage.addTable(tableGroup);
    const index = this._tables.indexOf(this._selectedTable);
    this._tables[index].has_layout = true;
  }

  save() {
    this.closeModal();
    this._originLayout[this.selectedFloor] = cloneDeep(this._stage.toData());
    this.saveEventHandler.emit(this._originLayout);
  }

  openModalConfirmSetting(template: TemplateRef<any>) {
    if (!this.canUpdate) {
      return;
    }

    this.openModalTemplate(template);
  }

  closeModal() {
    this.modalRef.hide();
  }

  private openModalTemplate(template: TemplateRef<any>, options?: ModalOptions) {
    const opts: ModalOptions = {
      ignoreBackdropClick: true,
      ...options,
    };

    this.modalRef = this._modalService.show(template, opts);
  }
}

applyMixins(KonvaCanvasComponent, []);
