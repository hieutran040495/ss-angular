import { Group, Text, Image, Layer, Shape } from 'konva';
import { KonvaDragLayer } from './konva-drag-layer';

export interface TempElement {
  table: Shape;
  text: Text;
  image: Image;
}

export class KonvaGroup extends Group {
  static TABLE_COLOR_DEFAULT = '#5c868d';
  static TABLE_COLOR_OVERLAP = '#5c868d';
  static TABLE_COLOR_HAS_RESV = '#8d8d8d';
  static TABLE_COLOR_OVERLAP_HAS_RESV = '#8d8d8d';
  static DEFAULT_WIDTH = 70;
  static DEFAULT_HEIGHT = 70;

  static DEFAULT_GROUP = {
    x: 10,
    y: 10,
    draggable: true,
    name: 'table-group',
  };

  static DEFAULT_SHAPE = {
    fill: KonvaGroup.TABLE_COLOR_DEFAULT,
    strokeScaleEnabled: false,
    name: 'table',
    stroke: KonvaGroup.TABLE_COLOR_OVERLAP,
    strokeWidth: 1,
    shadowColor: 'black',
    shadowBlur: 5,
    shadowOffset: {
      y: 3,
      x: 3,
    },
    shadowOpacity: 0.6,
  };

  static DEFAULT_TEXT = {
    fontSize: 13,
    align: 'center',
    verticalAlign: 'middle',
    ellipsis: true,
    shadowColor: 'red',
    wrap: 'none',
    fill: '#fff',
    name: 'table-name',
    padding: 5,
    x: 0,
    y: 0,
  };

  static DEFAULT_TEXT_TIME_RESV = {
    fontSize: 20,
    align: 'center',
    verticalAlign: 'middle',
    fill: 'black',
    name: 'resv-time',
    x: 0,
    y: -20,
  };

  static generateTextResvTime(type: string, start_time: string, width: number) {
    if (type === 'Circle') {
      return new Text({
        ...this.DEFAULT_TEXT_TIME_RESV,
        text: start_time,
        width: width,
        x: -KonvaGroup.DEFAULT_WIDTH / 2,
        y: -KonvaGroup.DEFAULT_WIDTH / 2 - 20,
      });
    }
    return new Text({
      ...KonvaGroup.DEFAULT_TEXT_TIME_RESV,
      text: start_time,
      width: width,
    });
  }

  static generate(id: string | number, tableType: string, tableName: string) {
    return KonvaGroup[`generate${tableType}`](id, tableType, tableName);
  }

  static generateRect(id: string | number, tableType: string, tableName: string) {
    return {
      attrs: {
        ...KonvaGroup.DEFAULT_GROUP,
        id: id.toString(),
      },
      className: 'Group',
      children: [
        {
          attrs: {
            ...KonvaGroup.DEFAULT_SHAPE,
            width: KonvaGroup.DEFAULT_WIDTH * 2,
            height: KonvaGroup.DEFAULT_HEIGHT,
            cornerRadius: 5,
          },
          className: tableType,
        },
        {
          attrs: {
            ...KonvaGroup.DEFAULT_TEXT,
            text: tableName,
            width: KonvaGroup.DEFAULT_WIDTH * 2,
            height: KonvaGroup.DEFAULT_HEIGHT,
          },
          className: 'Text',
        },
      ],
    };
  }

  static generateSquare(id: string | number, tableType: string, tableName: string) {
    return {
      attrs: {
        ...KonvaGroup.DEFAULT_GROUP,
        id: id.toString(),
      },
      className: 'Group',
      children: [
        {
          attrs: {
            ...KonvaGroup.DEFAULT_SHAPE,
            width: KonvaGroup.DEFAULT_WIDTH,
            height: KonvaGroup.DEFAULT_HEIGHT,
            cornerRadius: 5,
          },
          className: 'Rect',
        },
        {
          attrs: {
            ...KonvaGroup.DEFAULT_TEXT,
            text: tableName,
            width: KonvaGroup.DEFAULT_WIDTH,
            height: KonvaGroup.DEFAULT_HEIGHT,
          },
          className: 'Text',
        },
      ],
    };
  }

  static generateCircle(id: string | number, tableType: string, tableName: string) {
    return {
      attrs: {
        ...KonvaGroup.DEFAULT_GROUP,
        id: id.toString(),
      },
      className: 'Group',
      children: [
        {
          attrs: {
            ...KonvaGroup.DEFAULT_SHAPE,
            width: KonvaGroup.DEFAULT_WIDTH,
            height: KonvaGroup.DEFAULT_HEIGHT,
            radius: KonvaGroup.DEFAULT_WIDTH / 2,
          },
          className: tableType,
        },
        {
          attrs: {
            ...KonvaGroup.DEFAULT_TEXT,
            width: KonvaGroup.DEFAULT_WIDTH,
            height: KonvaGroup.DEFAULT_WIDTH,
            x: -KonvaGroup.DEFAULT_WIDTH / 2,
            y: -KonvaGroup.DEFAULT_HEIGHT / 2,
            text: tableName,
          },
          className: 'Text',
        },
      ],
    };
  }

  static attachMouseEvents(tableGroup: Group, mainLayer: Layer, dragLayer: KonvaDragLayer) {
    // event mouse down
    tableGroup.on('mousedown touchstart', () => {
      if (dragLayer.hasTransformer()) {
        dragLayer.transformer.visible(false);
      }

      tableGroup.moveTo(dragLayer);
      mainLayer.draw();
      dragLayer.draw();
      tableGroup.startDrag();
    });

    // event mouse up
    tableGroup.on('mouseup touchend', () => {
      if (dragLayer.hasTransformer()) {
        dragLayer.attachTransformer(tableGroup);
        dragLayer.transformer.visible(true);
      } else {
        dragLayer.attachTransformer(tableGroup);
      }

      tableGroup.moveTo(mainLayer);
      mainLayer.draw();
      dragLayer.draw();
    });

    tableGroup.on('dragend mouseover', () => {
      document.body.style.cursor = 'pointer';
    });

    tableGroup.on('dragmove mouseout', (e) => {
      document.body.style.cursor = 'default';
    });
  }

  static attachClickEvent(tableGroup: Group, callback: Function) {
    tableGroup.on('click tap', () => {
      callback(tableGroup.id());
    });
  }

  static attachTransformEvents(tableGroup: Group) {
    tableGroup.on('transform', function() {
      this.getLayer().draw();
    });
  }
}
