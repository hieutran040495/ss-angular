import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KonvaCanvasComponent } from './konva-canvas.component';

describe('KonvaCanvasComponent', () => {
  let component: KonvaCanvasComponent;
  let fixture: ComponentFixture<KonvaCanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KonvaCanvasComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KonvaCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
