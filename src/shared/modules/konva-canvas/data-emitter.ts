export enum KONVA_EMITTER_TYPE {
  RELOAD_KONVA = 'reload_conva',
  RELOAD_KONVA_LAYOUT = 'reload_konva_layout',
  ADD_NEW_ITEM = 'add_new_item',
  UPDATED_SUCCESS = 'updated_success',
  CHANGE_FLOOR = 'change_floor',
}

export interface KonvaDataEmitter {
  type: string;
  data?: any;
}
