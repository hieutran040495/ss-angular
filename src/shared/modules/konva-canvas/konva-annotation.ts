import { Text } from 'konva';

export class KonvaAnnotation {
  getText(id, start, end, x, y) {
    const midpoint = this.getMidpoint(start, end);

    return new Text({
      x: midpoint.x + x,
      y: midpoint.y + y,
      text: this.getDistance(start, end),
      fill: 'green',
      offsetX: 15,
      offsetY: 15,
      id: id,
      rotation: this.getAngle(start, end),
    });
  }

  updateText(text, start, end) {
    const midpoint = this.getMidpoint(start, end);

    text.setAttrs({
      x: midpoint.x,
      y: midpoint.y,
      text: this.getDistance(start, end),
      rotation: this.getAngle(start, end),
    });
  }

  getDistance(start, end) {
    // TODO: add constraint
    const ratio = 1;
    return ratio * Math.ceil(Math.sqrt(Math.pow(start.x - end.x, 2) + Math.pow(start.y - end.y, 2))) + 'cm';
  }

  getMidpoint(start, end) {
    return {
      x: (start.x + end.x) / 2,
      y: (start.y + end.y) / 2,
    };
  }

  getAngle(start, end) {
    return (Math.atan2(end.y - start.y, end.x - start.x) * 180) / Math.PI;
  }
}
