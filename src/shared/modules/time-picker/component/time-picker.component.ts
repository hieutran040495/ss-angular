import {
  Component,
  OnInit,
  Input,
  HostListener,
  forwardRef,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CasTimePicker, TIME } from '../time-picker.model';
import * as $ from 'jquery';

const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CasTimePickerComponent),
  multi: true,
};

@Component({
  selector: 'app-time-picker',
  templateUrl: './time-picker.component.html',
  styleUrls: ['./time-picker.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
})
export class CasTimePickerComponent implements OnInit, ControlValueAccessor {
  @ViewChild('timepickerForm')
  timepickerForm: ElementRef;

  private _casTimepicker: CasTimePicker = new CasTimePicker();

  MODE_TIME = TIME;

  /**
   * input params
   */
  private _minTime: string;
  @Input('minTime')
  get minTime(): string {
    return this._minTime;
  }
  set minTime(v: string) {
    this._minTime = v;
    this._casTimepicker.minTime = v;
  }

  private _maxTime: string;
  @Input('maxTime')
  get maxTime(): string {
    return this._maxTime;
  }
  set maxTime(v: string) {
    this._maxTime = v;
    this._casTimepicker.maxTime = v;
  }

  private _minuteStep: number;
  @Input('minuteStep')
  get minuteStep(): number {
    return this._minuteStep;
  }
  set minuteStep(v: number) {
    this._minuteStep = v;
    this._casTimepicker.minuteStep = v;
  }

  private _disabled: boolean | undefined;
  @Input('disabled')
  get disabled(): boolean | undefined {
    return this._disabled;
  }
  set disabled(v: boolean | undefined) {
    this._disabled = v || undefined;
  }

  private _isInValid: boolean;
  @Input('isInValid')
  get isInValid(): boolean {
    return this._isInValid;
  }
  set isInValid(v: boolean) {
    this._isInValid = v;
  }

  private _validateMsg: string;
  @Input('validateMsg')
  get validateMsg(): string {
    return this._validateMsg || '';
  }
  set validateMsg(v: string) {
    this._validateMsg = v;
  }

  private _placeholder: string;
  @Input('placeholder')
  get placeholder(): string {
    return this._placeholder || '--:--';
  }
  set placeholder(v: string) {
    this._placeholder = v;
  }

  @Input('isLimitedTimeRange')
  isLimitedTimeRange: boolean = false;

  @Output()
  updateDate: EventEmitter<any> = new EventEmitter();

  @Output() hideSelect: EventEmitter<null> = new EventEmitter<null>();

  // value
  hour: any = this._casTimepicker.minHour;
  min: any = this._casTimepicker.minMinute;

  /**
   * Overriden for interface ControlValueAccessor
   */
  private _onTouchedCallback: () => void = noop;

  private _onChangeCallback: (_: any) => void = noop;

  private _timeInput: any = '';
  get timeInput(): any {
    return this._timeInput;
  }

  set timeInput(v: any) {
    if (v !== this._timeInput) {
      this._timeInput = v;
      this._onChangeCallback(v);
    }
  }

  onTouched() {
    this._onTouchedCallback();
  }

  writeValue(value: any) {
    this._timeInput = value;
    if (this._timeInput) {
      const hm = this._timeInput.split(':');
      this.hour = +hm[0];
      this.min = +hm[1];

      // update model and view
      this._update();
    }
  }

  registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  /**
   * UI show/hidden for form timepicker
   */
  @HostListener('document:click', ['$event'])
  @HostListener('document:touchstart', ['$event'])
  clickedOutside($event) {
    const clickedInside = this.timepickerForm.nativeElement.contains($event.target);

    this._update();

    if (!clickedInside && $('.timepicker-form').is(':visible')) {
      $('.timepicker-form').hide();
      this.hideSelect.emit();
    }
  }

  clickedInside($event: Event, dom) {
    $('.timepicker-form').hide();
    $event.preventDefault();
    $event.stopPropagation(); // <- that will stop propagation on lower layers

    if (!this.disabled) {
      $(dom)
        .find('.timepicker-form')
        .show();
    }
  }

  /**
   * @param event
   * allow input numberic only
   */
  inputHour(event: any) {
    if (this._casTimepicker.isValidHour(this.hour, this.min)) {
      setTimeout(() => {
        this._update();
      }, 100);
    } else if (this.hour.length === 2) {
      this.hour = this._casTimepicker.setHourDefault(this.hour);
      this._update();
    }
  }

  /**
   * @param event
   * allow input numberic only
   */
  inputMinute(event: any) {
    if (this._casTimepicker.isValidMinute(this.hour, this.min)) {
      setTimeout(() => {
        this._update();
      }, 100);
    } else if (this.min.length === 2) {
      this.min = this._casTimepicker.setMinuteDefault(this.min);
      this._update();
    }
  }

  ngOnInit() {}

  /**
   * Increment/Decrement for hour
   * @param v: number +1, -1
   * minHour and maxHour
   */
  setHour(v: string) {
    let minuteTmp = this._casTimepicker.minuteTotal(this.timeInput);

    switch (v) {
      case TIME.UP:
        minuteTmp = minuteTmp + 60;
        this._emitDate(minuteTmp);
        if (minuteTmp > this._casTimepicker.maxMinuteTotal) {
          if (this.isLimitedTimeRange) {
            minuteTmp = this._casTimepicker.maxMinuteTotal;
          } else {
            minuteTmp = minuteTmp - Math.floor(minuteTmp / 60) * 60;
          }
        }
        break;
      case TIME.DOWN:
        minuteTmp = minuteTmp - 60;
        this._emitDate(minuteTmp);
        if (minuteTmp < this._casTimepicker.minMinuteTotal) {
          if (this.isLimitedTimeRange) {
            minuteTmp = this._casTimepicker.minMinuteTotal;
          } else if (minuteTmp < 0) {
            minuteTmp = (Math.floor(this._casTimepicker.maxMinuteTotal / 60) + 1) * 60 + minuteTmp;
          } else {
            minuteTmp = (Math.floor(this._casTimepicker.maxMinuteTotal / 60) - 1) * 60;
          }
        }
        break;
    }

    this.hour = this._casTimepicker.setHour(minuteTmp);
    this.min = this._casTimepicker.setMinute(minuteTmp);

    this._update();
  }

  private _emitDate(target: number) {
    if (target > this._casTimepicker.maxMinuteTotal) {
      this.updateDate.emit(1);
    }

    if (target < this._casTimepicker.minMinuteTotal) {
      this.updateDate.emit(-1);
    }
  }

  /**
   * Set value Minute for Timepicker
   */
  setMin(type) {
    let minuteTmp = this._casTimepicker.minuteTotal(this.timeInput);

    switch (type) {
      case TIME.UP:
        minuteTmp = this._minuteIncrement(minuteTmp);
        break;
      case TIME.DOWN:
        minuteTmp = this._minuteDecrement(minuteTmp);
        break;
    }

    this.hour = this._casTimepicker.setHour(minuteTmp);
    this.min = this._casTimepicker.setMinute(minuteTmp);

    this._update();
  }

  /**
   * Minute Increment
   * @param minute: number
   */
  private _minuteIncrement(minute: number) {
    minute += +this._casTimepicker.minuteStep;
    this._emitDate(minute);

    if (minute > +this._casTimepicker.maxMinuteTotal) {
      if (this.isLimitedTimeRange) {
        minute = this._casTimepicker.maxMinuteTotal;
      } else {
        minute = this._casTimepicker.minMinuteTotal;
      }
    }
    return minute;
  }

  /**
   * Minute Decrement
   * @param minute: number
   */
  private _minuteDecrement(minute: number) {
    minute -= +this._casTimepicker.minuteStep;
    this._emitDate(minute);

    if (minute < +this._casTimepicker.minMinuteTotal) {
      if (this.isLimitedTimeRange) {
        minute = this._casTimepicker.minMinuteTotal;
      } else {
        minute = this._casTimepicker.maxMinuteTotal;
      }
    }

    return minute;
  }

  // output
  private _update() {
    if (+this.hour < 10) {
      this.hour = `0${+this.hour}`;
    }

    if (+this.min < 10) {
      this.min = `0${+this.min}`;
    }

    this.timeInput = `${this.hour}:${this.min}`;
    this.isInValid = false;
    this.validateMsg = '';
  }
}
