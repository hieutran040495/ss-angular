import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CasTimePickerComponent } from './component/time-picker.component';
import { OnlyNumericModule } from 'shared/directive/only-numeric/only-numeric.module';

@NgModule({
  imports: [CommonModule, FormsModule, OnlyNumericModule],
  declarations: [CasTimePickerComponent],
  exports: [CasTimePickerComponent, FormsModule],
})
export class TimePickerModule {}
