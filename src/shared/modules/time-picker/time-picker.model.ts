import { Deserializable } from 'shared/interfaces/deserializable';

export enum TIME {
  DEFAULT = 60,
  MIN = '00:00',
  MAX = '23:59',
  STEP = 15,
  UP = 'up',
  DOWN = 'down',
}

export interface CasTimePickerInput {
  maxTime: string;
  minTime: string;
  minMinuteTotal: number;
  maxMinuteTotal: number;
  minHour: number;
  maxHour: number;
  minMinute: number;
  maxMinute: number;
  minuteStep: number;
}

export class CasTimePicker implements Deserializable<CasTimePicker>, CasTimePickerInput {
  private _maxTime: string;
  get maxTime(): string {
    return this._maxTime;
  }
  set maxTime(v: string) {
    this._maxTime = v;
    this.initMinMax();
  }

  private _minTime: string;
  get minTime(): string {
    return this._minTime;
  }
  set minTime(v: string) {
    this._minTime = v;
    this.initMinMax();
  }

  private _minuteStep: number;
  get minuteStep(): number {
    return this._minuteStep;
  }
  set minuteStep(v: number) {
    this._minuteStep = v;
    this.initMinMax();
  }

  minMinuteTotal: number;
  maxMinuteTotal: number;
  minHour: number;
  maxHour: number;
  minMinute: number;
  maxMinute: number;

  constructor(data?: any) {
    this.minTime = data ? data.minTime : TIME.MIN;
    this.maxTime = data ? data.maxTime : TIME.MAX;
    this.minuteStep = data ? data.step : TIME.STEP;
  }

  deserialize(input: Partial<CasTimePickerInput>): CasTimePicker {
    Object.assign(this, input);
    return this;
  }

  initMinMax() {
    if (!this.minTime || !this.maxTime || !this.minuteStep) {
      return;
    }

    this.minMinuteTotal = this.minuteTotal(this.minTime);
    this.maxMinuteTotal = this.minuteTotal(this.maxTime);

    this.minHour = this.setHour(this.minMinuteTotal);
    this.maxHour = this.setHour(this.maxMinuteTotal);

    this.minMinute = this.setMinute(this.minMinuteTotal);
    this.maxMinute = this.setMinute(this.maxMinuteTotal);
  }

  minuteTotal(time: string): number {
    const _time = time.split(':');
    return +_time[0] * TIME.DEFAULT + +_time[1];
  }

  setHour(minuteTotal: number): number {
    return Math.floor(minuteTotal / TIME.DEFAULT);
  }

  setMinute(minuteTotal: number): number {
    return Math.round(minuteTotal % TIME.DEFAULT);
  }

  /**
   * check value with regex and (min - max)
   * @param hour: any because of using with regex
   * @param minute: any
   */
  isValidHour(hour: any, minute: any) {
    const _pattern = /^[01]\d|2\d$/g;
    const minuteTotal = +hour * TIME.DEFAULT + +minute;
    return _pattern.test(hour) && minuteTotal <= this.maxMinuteTotal && minuteTotal >= this.minMinuteTotal;
  }

  /**
   * check value with regex and (min - max)
   * @param hour: any
   * @param minute: any because of using with regex
   */
  isValidMinute(hour: any, minute: any) {
    const _pattern = /^[0-5]\d$/g;
    const minuteTotal = +hour * TIME.DEFAULT + +minute;
    return _pattern.test(minute) && minuteTotal <= this.maxMinuteTotal && minuteTotal >= this.minMinuteTotal;
  }

  /**
   * set value when typing invalid hour
   * @param v: number hour
   */
  setHourDefault(v: number) {
    const middle = Math.floor((this.maxHour - this.minHour) / 2);
    const vReturn: any = middle > v ? this.minHour : this.maxHour;
    if (+vReturn < 10) {
      return `0${+vReturn}`;
    }
    return vReturn;
  }

  /**
   * set value when typing invalid minute
   * @param v: number minute
   */
  setMinuteDefault(v: number) {
    const middle = Math.floor((this.maxMinute - this.minMinute) / 2);
    const vReturn: any = middle > v ? this.minMinute : this.maxMinute;
    if (+vReturn < 10) {
      return `0${+vReturn}`;
    }
    return vReturn;
  }
}
