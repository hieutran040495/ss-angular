# Sod Shared Module

This module was generated with [Angular CLI](https://github.com/angular/angular-cli) version ^1.7.4.

# How To Install Time-picker

- Implement to your module

`import { TimePickerModule } from '<source-path>/cas-time-picker.module';`

```shell
@NgModule({
imports: [
  TimePickerModule
]
})
```

- Implement to your component

```shell
<app-time-picker
  [minTime]="minTime"
  [maxTime]="maxTime"
  [(ngModel)]="ngModel"
  [minuteStep]="minuteStep"
  [isInValid]="isInValid"
  [disabled]="disabled"
  [validateMsg]="validateMsg"
  [placeholder]="placeholder"
  name="name">
</app-time-picker>

minTime: default '05:00'
maxTime: default '27:59'
minuteStep: default '15'
placeholder: default '--/--'
isInValid: default false
disabled: default false
validateMsg: default ''
```
