export class Helper {
  static setTime(totalMinute: number) {
    return {
      hour: Math.floor(totalMinute / 60),
      minute: totalMinute % 60,
    };
  }
}
