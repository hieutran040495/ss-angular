import { Layer, LayerConfig } from 'konva';
import { TopImage } from 'shared/models/top-image';
import { KonvaImage } from 'shared/models/konva-image';

export class MainLayer extends Layer {
  mainImage: KonvaImage;

  private topImage: TopImage;

  constructor(layerConfig?: LayerConfig) {
    super(layerConfig);
  }

  public initMainImage(topImage: TopImage, scale: number) {
    this.topImage = topImage;
    const imageObj = new Image();
    this.mainImage = new KonvaImage({
      image: imageObj,
      name: 'main-image',
      draggable: true,
    });

    this.add(this.mainImage);

    imageObj.onload = () => {
      this.updateMainImg(imageObj, topImage, scale);
    };
    imageObj.src = topImage.image_url || '';
  }

  changeImage(topImage: TopImage, scale: number) {
    const imageObj = new Image();
    imageObj.onload = () => {
      this.updateMainImg(imageObj, topImage, scale, false);
    };
    imageObj.src = topImage.image_url;
  }

  private updateMainImg(imageObj: any, topImage: TopImage, scale: number, isAddItem: boolean = true) {
    this.mainImage.scale({
      x: scale,
      y: scale,
    });

    this.mainImage.width(topImage.widthScale);
    this.mainImage.height(topImage.heightScale);

    this.mainImage.x(topImage.pos_x * scale);
    this.mainImage.y(topImage.pos_y * scale);
    this.mainImage.image(imageObj);
    this.draw();
  }

  public dataToJSON(scale: number) {
    const data: any = {};
    data.items = [];
    const childrens = this.getChildren();

    childrens.toArray().forEach((child) => {
      if (child instanceof KonvaImage) {
        data.pos_x = child.x() / scale;
        data.pos_y = child.y() / scale;
        const new_width = (child.getWidth() * child.scaleX()) / scale;
        data.ratio = new_width / this.topImage.image_width;
      }
    });

    return data;
  }

  public updateScale(oldScale: number, scale: number) {
    const childrens = this.getChildren();

    childrens.toArray().forEach((child) => {
      child.scale({
        x: scale,
        y: scale,
      });

      child.x((child.x() / oldScale) * scale);
      child.y((child.y() / oldScale) * scale);
    });

    this.draw();
  }
}
