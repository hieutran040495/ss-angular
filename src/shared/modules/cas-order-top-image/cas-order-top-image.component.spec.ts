import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasOrderTopImageComponent } from './cas-order-top-image.component';

describe('CasOrderTopImageComponent', () => {
  let component: CasOrderTopImageComponent;
  let fixture: ComponentFixture<CasOrderTopImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasOrderTopImageComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasOrderTopImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
