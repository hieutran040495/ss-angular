import { Stage, StageConfig } from 'konva';
import { MainLayer } from './main-layer';
import { TopImage } from 'shared/models/top-image';

export class OrderTopImageStage extends Stage {
  private _mainLayer: MainLayer;
  public get mainLayer() {
    return this._mainLayer;
  }

  constructor(stageConfig: StageConfig) {
    super(stageConfig);
  }

  public initLayer() {
    this._initMainLayer();
  }

  public initMainImage(topImage: TopImage, scale: number) {
    this._mainLayer.initMainImage(topImage, scale);
  }

  updateMainImage(topImage: TopImage, scale: number) {
    this._mainLayer.changeImage(topImage, scale);
  }

  removeImage() {
    const imageObj = new Image();
    this._mainLayer.mainImage.image(imageObj);
    this._mainLayer.batchDraw();
  }

  private _initMainLayer() {
    this._mainLayer = new MainLayer();
    this.add(this._mainLayer);
  }
}
