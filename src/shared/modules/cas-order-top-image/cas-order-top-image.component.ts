import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Input } from '@angular/core';
import { OrderTopImageStage } from './stage';
import { Subject } from 'rxjs';
import { ORDER } from 'shared/enums/event-emitter';
import { ToastService } from 'shared/logical-services/toast.service';
import { TopImage } from 'shared/models/top-image';
import { StyleOpts } from 'shared/interfaces/style-setting';

interface ScreenSize {
  width: number;
  height: number;
}

@Component({
  selector: 'app-cas-order-top-image',
  templateUrl: './cas-order-top-image.component.html',
  styleUrls: ['./cas-order-top-image.component.scss'],
})
export class CasOrderTopImageComponent implements OnInit, AfterViewInit {
  @ViewChild('casOrderTopImage') casOrderTopImage: ElementRef;
  @ViewChild('formFiles') formFiles: ElementRef;

  private _stage: OrderTopImageStage;
  private _topImage: TopImage;
  @Input('topImage')
  get topImage(): TopImage {
    return this._topImage;
  }
  set topImage(v: TopImage) {
    this._topImage = v;
    if (v) {
      this._initTopImage();
    }
  }

  @Input('onEventEmitter') onEventEmitter: Subject<any>;
  @Input('styleOpts') styleOpts: StyleOpts;

  private _screenSize: ScreenSize;
  @Input('screenSize')
  public get screenSize(): ScreenSize {
    return this._screenSize;
  }
  public set screenSize(v: ScreenSize) {
    this._screenSize = v;
    this._oldScale = Number(this._scale);
    this._scale = (940 / this.screenSize.width) * 0.6;

    this._setScreenSize();
  }
  get screenWidth(): number {
    return this.screenSize.width * this._scale;
  }
  get screenHeight(): number {
    return this.screenSize.height * this._scale;
  }

  private _scale = 1;
  private _oldScale = 1;
  private _fileUpload: File;

  fileAccept: string = 'image/jpeg,image/png,image/jpg';
  private maxFileSize: number = 3145728; // 3MB

  lastDist = 0;
  activeShape = null;

  constructor(private toastSv: ToastService) {}

  ngOnInit() {
    this.casOrderTopImageEventEmitter();
  }

  ngAfterViewInit() {
    this._initLayoutCasOrderTopImage();
    this._eventDraggable();
  }

  private casOrderTopImageEventEmitter() {
    this.onEventEmitter.subscribe((res) => {
      if (!res || !res.type) {
        return;
      }

      switch (res.type) {
        case ORDER.CHANGE_TOP_SCREEN:
          if (res.data.image && res.data.image.image_url) {
            this._topImage = new TopImage().deserialize(res.data.image);
            return this._stage.updateMainImage(this._topImage, this._scale);
          }

          this._stage.clear();
          break;
        default:
          break;
      }
    });
  }

  private _eventDraggable() {
    this._stage.on('dragend', (evt: any) => {
      this._topImage.pos_x = evt.target.attrs.x / this._scale;
      this._topImage.pos_y = evt.target.attrs.y / this._scale;

      this.onEventEmitter.next({
        type: ORDER.CHANGE_TOP_IMAGE,
        data: this._topImage,
      });
    });
  }

  private _initTopImage() {
    if (!this._stage) {
      return;
    }

    if (this._topImage && this._topImage.image_url) {
      this._stage.updateMainImage(this._topImage, this._scale);
    }

    this._stage.clear();
  }

  private _setScreenSize() {
    if (!this._stage) {
      return;
    }

    this._stage.width(this.screenWidth);
    this._stage.height(this.screenHeight);

    if (this._stage.mainLayer) {
      this._stage.mainLayer.updateScale(this._oldScale, this._scale);
    }

    this._stage.draw();
  }

  private _initLayoutCasOrderTopImage() {
    this._stage = new OrderTopImageStage({
      container: this.casOrderTopImage.nativeElement,
    });

    this._stage.initLayer();
    this._stage.initMainImage(this._topImage, this._scale);
    this._setScreenSize();
  }

  handleFile(files: FileList) {
    const file: File = files.item(0);
    if (file) {
      if (this.fileAccept.indexOf(file.type) === -1) {
        this.toastSv.info(`jpg, jpegとpngのファイルでアップロードしてください`);
        return false;
      }

      if (file.size > this.maxFileSize) {
        this.toastSv.info(`サイズが3MBを超える画像はアップロードできません`);
        return false;
      }

      this._fileUpload = file;
      // Render file preview
      const reader: FileReader = new FileReader();
      reader.onloadend = (e: any) => {
        this._topImage.image_url = e.target.result;
        // Get Width & Height of Image
        const newImg = new Image();
        newImg.src = e.target.result;
        newImg.onload = (img: any) => {
          this._topImage.file = file;
          this._topImage.image_width = img.composedPath()[0].width;
          this._topImage.image_height = img.composedPath()[0].height;
          this._topImage.ratio = 1;
          this._topImage.pos_x = 0;
          this._topImage.pos_y = 0;
          this._stage.updateMainImage(this._topImage, this._scale);

          this.onEventEmitter.next({
            type: ORDER.CHANGE_TOP_IMAGE,
            data: this._topImage,
          });
        };
      };
      reader.readAsDataURL(file);
      this.formFiles.nativeElement.value = null;
    }
  }

  uploadImage(el: any) {
    if (!!this.topImage.image_url) {
      this.topImage.image_url = null;
      this.topImage.image_path = null;
      this._stage.removeImage();
      this.onEventEmitter.next({
        type: ORDER.REMOVE_TOP_IMAGE,
        data: this.topImage,
      });
      return;
    }

    el.click();
  }
}
