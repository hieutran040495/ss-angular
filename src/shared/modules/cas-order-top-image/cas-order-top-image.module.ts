import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasOrderTopImageComponent } from './cas-order-top-image.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [CasOrderTopImageComponent],
  exports: [CasOrderTopImageComponent],
})
export class CasOrderTopImageModule {}
