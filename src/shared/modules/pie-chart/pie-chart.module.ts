import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PieChartComponent } from './pie-chart.component';
import { ChartsModule } from 'ng2-charts';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [PieChartComponent],
  imports: [CommonModule, ChartsModule, FormsModule],
  exports: [PieChartComponent],
})
export class PieChartModule {}
