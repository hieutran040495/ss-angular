import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss'],
})
export class PieChartComponent implements OnInit {
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;
  private _data: number[];
  get data(): number[] {
    return this._data || [];
  }
  @Input('data') set data(v: number[]) {
    this._data = v;
    if (v && !!v.length) {
      this.checkHasData(v);
      this.renderLegend();
    }
  }

  @Input('labels') labels: string[];
  @Input('tooltip') tooltip: string[];
  @Input('title') title: string;
  @Input('isLoadingChart') isLoadingChart: string;

  private _isDataNotFound: boolean;
  get isDataNotFound(): boolean {
    return this._isDataNotFound || false;
  }
  @Input('isDataNotFound') set isDataNotFound(v: boolean) {
    this._isDataNotFound = v;
  }

  pieChartOptions: any = {
    title: {
      display: false,
      text: this.title,
    },
    responsive: true,
    legend: {
      display: false,
    },
    legendCallback: function(chart) {
      const result = [];
      for (let i = 0; i < chart.data.labels.length; i++) {
        result.push({
          chartId: chart.id,
          styles: {
            'background-color': chart.data.datasets[0].backgroundColor[i],
          },
          key: i,
          label: chart.data.labels[i],
          disabled: false,
        });
      }
      return result;
    },
    tooltips: {
      callbacks: {
        label: function(tooltipItem, data) {
          return data['labels'][tooltipItem['index']];
        },
      },
    },
  };

  legendData: any;

  constructor() {}

  ngOnInit() {
    this.pieChartOptions.title.text = this.title;
  }

  renderLegend() {
    const setChartInterval = setInterval(() => {
      if (this.chart && this.chart.chart) {
        this.legendData = this.chart.chart.generateLegend();
        clearInterval(setChartInterval);
      }
      return;
    }, 1500);
  }

  checkHasData(data: number[]) {
    const sum = data.reduce((a, b) => {
      return a + b;
    });
    if (!sum) {
      this.isDataNotFound = true;
    } else {
      this.isDataNotFound = false;
    }
  }

  handleChartLengend(legend) {
    const curr = this.chart.chart.data.datasets[0]._meta[legend.chartId].data[legend.key];
    curr.hidden = !curr.hidden;
    legend.disabled = !legend.disabled;
    this.chart.chart.update();
  }

  // events
  chartClicked(e: any) {
    console.log(e);
  }

  chartHovered(e: any) {
    console.log(e);
  }
}
