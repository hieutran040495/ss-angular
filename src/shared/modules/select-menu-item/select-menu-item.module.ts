import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectMenuItemComponent } from './select-menu-item.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalOrderMenuModule } from 'app/pages/modal-order-menu/modal-order-menu.module';
import { CasOrderItemsModule } from 'app/modules/cas-order-items/cas-order-items.module';

@NgModule({
  declarations: [SelectMenuItemComponent],
  imports: [CommonModule, FormsModule, ModalOrderMenuModule, ModalModule.forRoot(), CasOrderItemsModule],
  exports: [SelectMenuItemComponent],
})
export class SelectMenuItemModule {}
