import { Component, OnInit, OnDestroy, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { ModalOrderMenuComponent } from 'app/pages/modal-order-menu/modal-order-menu.component';
import { ModalOptions, BsModalService } from 'ngx-bootstrap/modal';

import { EventEmitterType } from 'shared/enums/event-emitter';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';

import { Menu } from 'shared/models/menu';
import { MenuBuffet } from 'shared/models/menu-buffet';
import { SEARCH_WITH, ORDER_TYPE } from 'shared/enums/type-search-order';

import * as cloneDeep from 'lodash/cloneDeep';
import { Subscription } from 'rxjs';

const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  /* tslint:disable-next-line: no-use-before-declare */
  useExisting: forwardRef(() => SelectMenuItemComponent),
  provide: NG_VALUE_ACCESSOR,
  multi: true,
};

@Component({
  selector: 'app-select-menu-item',
  templateUrl: './select-menu-item.component.html',
  styleUrls: ['./select-menu-item.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
})
export class SelectMenuItemComponent implements OnInit, ControlValueAccessor, OnDestroy {
  @Input() disabled: boolean;
  @Input() isBuffet: boolean;
  @Input() name: string = 'select-menu-item';
  @Input() placeholder: string = 'メニューを選択';
  @Input() optionType: string;

  @Output() remove = new EventEmitter<boolean>();
  @Output() change = new EventEmitter<Array<Menu | MenuBuffet>>();

  private _items: Menu[] = [];
  get items(): Menu[] {
    return this._items;
  }
  set items(v: Menu[]) {
    if (v !== this.items) {
      this._items = v;
    }
  }

  private pageSub: Subscription;

  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;

  constructor(private modalSv: BsModalService, private eventEmitter: EventEmitterService) {}

  writeValue(value: any) {
    this.items = value;
  }

  registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  ngOnInit() {
    this.pageSub = this.eventEmitter.caseNumber$.subscribe((res) => {
      if (
        (res.type === EventEmitterType.SELECT_MENU_ITEM && !this.isBuffet) ||
        (res.type === EventEmitterType.SELECT_MENU_BUFFET && this.isBuffet)
      ) {
        this.itemChange(res.data);
      }
    });
  }

  ngOnDestroy() {
    this.pageSub.unsubscribe();
  }

  itemChange(data: any) {
    this.items = data.items;
    this._onChangeCallback(data.items);

    if (!this.items.length) {
      this.remove.emit(true);
      this._onTouchedCallback();
      return;
    }

    this.change.emit(this.items);
  }

  openModalOrderMenu() {
    if (this.isBuffet) {
      return this._openModal(this._initDataSelectBuffet());
    }
    this._openModal(this._initDataSelectMenu());
  }

  private _initDataSelectBuffet() {
    return {
      buffets: cloneDeep(this.items),
      seachType: SEARCH_WITH.BUFFET,
      orderType: ORDER_TYPE.SELECT_BUFFET,
    };
  }

  private _initDataSelectMenu() {
    return {
      items: cloneDeep(this.items),
      seachType: SEARCH_WITH.CATEGORY,
      orderType: ORDER_TYPE.SELECT_MENU,
      optionType: this.optionType,
    };
  }

  private _openModal(dataState: any) {
    const opts: ModalOptions = {
      class: 'modal-md modal-dialog-centered',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: dataState,
    };
    this.modalSv.show(ModalOrderMenuComponent, opts);
  }
}
