import {
  Component,
  OnInit,
  HostListener,
  forwardRef,
  Input,
  Output,
  EventEmitter,
  Inject,
  Optional,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';

import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { UI_SWITCH_OPTIONS } from './ui-switch-option';
import { UiSwitchModuleConfig } from './ui-switch-config';

import { SWITCH_TYPE } from './type';

const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  /* tslint:disable-next-line: no-use-before-declare */
  useExisting: forwardRef(() => UiSwichComponent),
  provide: NG_VALUE_ACCESSOR,
  multi: true,
};

@Component({
  selector: 'app-ui-switch',
  templateUrl: './ui-switch.component.html',
  styleUrls: ['./ui-switch.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UiSwichComponent implements OnInit, ControlValueAccessor {
  @Output() changeEvent = new EventEmitter<MouseEvent>();
  @Output() valueChange = new EventEmitter<boolean>();

  SWITCH_TYPE = SWITCH_TYPE;

  private _name = 'ui-switch';
  @Input('name')
  get name(): string {
    return this._name;
  }
  set name(v: string) {
    this._name = v;
  }

  private _labelOn = '';
  @Input('labelOn')
  get labelOn(): string {
    return this._labelOn;
  }
  set labelOn(v: string) {
    this._labelOn = v;
  }

  private _labelOff = '';
  @Input('labelOff')
  get labelOff(): string {
    return this._labelOff;
  }
  set labelOff(v: string) {
    this._labelOff = v;
  }

  private _disabled = false;
  @Input('disabled')
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(v: boolean) {
    this._disabled = v;
  }

  private _className = '';
  @Input('className')
  get className(): string {
    return this._className;
  }
  set className(v: string) {
    this._className = v;
  }

  /**
   * Overriden for interface ControlValueAccessor
   */
  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;

  private _switchValue: boolean;
  get switchValue(): boolean {
    return this._switchValue;
  }
  set switchValue(v: boolean) {
    this._switchValue = v;
    this._onChangeCallback(v);
  }

  constructor(
    @Inject(UI_SWITCH_OPTIONS) @Optional() config: UiSwitchModuleConfig = {},
    private _cd: ChangeDetectorRef,
  ) {
    this.labelOn = (config && config.labelOn) || '';
    this.labelOff = (config && config.labelOff) || '';
  }

  ngOnInit() {}

  @HostListener('document:click', ['$event'])
  onToggle(event: MouseEvent) {
    if (this.disabled) {
      return;
    }

    this.changeEvent.emit(event);
    this.valueChange.emit(this.switchValue);
    this._cd.markForCheck();
  }

  onTouched() {
    this._onTouchedCallback();
  }

  writeValue(value: any) {
    this.switchValue = value;
  }

  registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }
}
