import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiSwichComponent } from './ui-switch.component';

describe('UiSwichComponent', () => {
  let component: UiSwichComponent;
  let fixture: ComponentFixture<UiSwichComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UiSwichComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiSwichComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
