export enum SWITCH_TYPE {
  SQUARE = 'square',
  CIRCLE = 'circle',
}

export enum SWITCH_TYPE_CLASS {
  square = 'ui-switch--square',
  circle = 'ui-switch--circle',
}
