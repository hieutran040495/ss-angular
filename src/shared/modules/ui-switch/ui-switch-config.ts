export interface UiSwitchModuleConfig {
  size?: string;
  colorText?: string;
  switchType?: string;
  defaultBgColor?: string;
  bgColor?: string;
  labelOn?: string;
  labelOff?: string;
}
