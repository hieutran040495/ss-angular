import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UiSwichComponent } from './ui-switch.component';

import { UI_SWITCH_OPTIONS } from './ui-switch-option';
import { UiSwitchModuleConfig } from './ui-switch-config';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [UiSwichComponent],
  exports: [UiSwichComponent],
})
export class UiSwitchModule {
  static forRoot(config: UiSwitchModuleConfig | null | undefined): ModuleWithProviders {
    return {
      ngModule: UiSwitchModule,
      providers: [{ provide: UI_SWITCH_OPTIONS, useValue: config || {} }],
    };
  }
}
