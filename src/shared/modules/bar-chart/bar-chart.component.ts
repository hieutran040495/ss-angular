import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss'],
})
export class BarChartComponent implements OnInit {
  @Input('datasets') datasets: any[] = [];
  @Input('labels') labels: string[] = [];
  @Input('title') title: string;
  @Input('isLoadingChart') isLoadingChart: boolean;

  barChartOptions: any = {
    scaleShowVerticalLines: false,
    title: {
      display: true,
      text: this.title,
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false,
    },
    scales: {
      xAxes: [
        {
          ticks: {
            autoSkip: false,
            callback: (value) => {
              return value.truncateString(10);
            },
          },
        },
      ],
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            callback: (value) => {
              return `¥${value.format()}`;
            },
          },
        },
      ],
    },
  };

  colors = [
    {
      backgroundColor: '#57bbcf',
      borderColor: '#CDCDCD',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)',
    },
  ];

  constructor() {}

  ngOnInit() {
    this.barChartOptions.title.text = this.title;
  }

  // events
  chartClicked(e: any) {
    console.log(e);
  }

  chartHovered(e: any) {
    console.log(e);
  }
}
