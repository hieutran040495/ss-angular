import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BarChartComponent } from './bar-chart.component';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [BarChartComponent],
  imports: [CommonModule, FormsModule, ChartsModule],
  exports: [BarChartComponent],
})
export class BarChartModule {}
