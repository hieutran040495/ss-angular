import { Group, ContainerConfig, Text, Rect } from 'konva';
import { CONFIG_TITLE, CONFIG_PRICE } from './item-layer';
import { RichMenuItem } from 'shared/models/rich-menu-item';
import { TextConfig } from 'shared/interfaces/style-setting';

export class MenuItem extends Group {
  menu: RichMenuItem;
  textConfig: TextConfig;
  private _title: Text;
  private _price: Text;
  private _rect: Rect;

  private maxRecWidth: number = 300;
  private maxRecHeight: number = 100;
  private maxTitleWidth: number = 260;

  private ratio: number = 2; // distance 2x resolution in iPad pro

  constructor(
    containerConfig: ContainerConfig,
    menu: RichMenuItem,
    textConfig: TextConfig,
    isDirectionVert: boolean = false,
  ) {
    super(containerConfig);

    if (isDirectionVert) {
      this.ratio = 1;
    }

    this.menu = menu;
    this.textConfig = textConfig;

    this._rect = new Rect({
      name: 'rect-transparent',
      width: this.maxRecWidth * this.ratio,
      height: this.maxRecHeight * this.ratio,
      stroke: '#00efb8',
      strokeWidth: 1,
    });

    this.add(this._rect);

    this._initTitle(menu.nameTruncateSelfOrderPatternOne);
    this._initPrice(menu.total_price_display);
  }

  private _initTitle(menu_name: string) {
    this._title = new Text({
      fontSize: CONFIG_TITLE.fontSize * this.ratio,
      text: menu_name,
      fill: this.textConfig.fill,
      fontFamily: this.textConfig.fontFamily,
      ellipsis: false,
      wrap: 'char',
      width: this.maxTitleWidth * this.ratio,
      lineHeight: 1.1,
      x: 20 * this.ratio,
      y: 13 * this.ratio,
    });

    this.add(this._title);
  }

  private _initPrice(menu_price: string) {
    this._price = new Text({
      fontSize: CONFIG_PRICE.fontSize * this.ratio,
      text: menu_price,
      fill: this.textConfig.fill,
      fontFamily: this.textConfig.fontFamily,
      y: 64 * this.ratio,
      lineHeight: 1.4375,
      width: this.maxTitleWidth * this.ratio,
    });
    this._price.x(this._price.getWidth() + 20 - this._price.getTextWidth());

    this.add(this._price);
  }
}
