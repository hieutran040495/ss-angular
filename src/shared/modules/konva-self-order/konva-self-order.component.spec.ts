import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KonvaSelfOrderComponent } from './konva-self-order.component';

describe('KonvaSelfOrderComponent', () => {
  let component: KonvaSelfOrderComponent;
  let fixture: ComponentFixture<KonvaSelfOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KonvaSelfOrderComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KonvaSelfOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
