import { Layer, LayerConfig } from 'konva';
import { MenuItem } from './menu-item';
import { RichMenu } from 'shared/models/rich-menu';
import { RichMenuItem } from 'shared/models/rich-menu-item';
import { MAIN_IMAGE } from './item-layer';
import { Scale } from 'shared/models/scale';
import { KonvaImage } from 'shared/models/konva-image';

export class MainSelfOrderLayer extends Layer {
  mainImage: KonvaImage;

  private richMenu: RichMenu;
  private isDirectionVert: boolean = false;

  constructor(layerConfig?: LayerConfig) {
    super(layerConfig);
  }

  initRichMenu(richMenu: RichMenu, scale: Scale, isDirectionVert: boolean = false) {
    this.richMenu = richMenu;
    if (isDirectionVert) {
      this.isDirectionVert = isDirectionVert;
    }
    const imageObj = new Image();
    this.mainImage = new KonvaImage({
      image: imageObj,
      ...MAIN_IMAGE,
    });

    this.add(this.mainImage);
    if (richMenu && !richMenu.image_url) {
      return this._addMenuItems(richMenu.items, scale);
    }

    imageObj.onload = () => {
      this.updateMainImg(imageObj, richMenu, scale);
    };
    imageObj.src = richMenu.image_url || '';
  }

  changeImage(richMenu: RichMenu, scale: Scale) {
    const imageObj = new Image();
    imageObj.onload = () => {
      this.updateMainImg(imageObj, richMenu, scale, false);
    };
    imageObj.src = richMenu.image_url;
  }

  private updateMainImg(imageObj: any, richMenu: RichMenu, scale: Scale, isAddItem: boolean = true) {
    this.mainImage.scale({
      x: scale.scale_x,
      y: scale.scale_y,
    });

    this.mainImage.width(richMenu.widthScale);
    this.mainImage.height(richMenu.heightScale);

    this.mainImage.x(richMenu.pos_x * scale.scale_x);
    this.mainImage.y(richMenu.pos_y * scale.scale_y);
    this.mainImage.image(imageObj);
    this.draw();

    if (isAddItem) {
      this._addMenuItems(richMenu.items, scale);
    }
  }

  private _addMenuItems(richMenuItems: RichMenuItem[] = [], scale: Scale) {
    if (!richMenuItems || !richMenuItems.length) {
      return;
    }
    richMenuItems.forEach((menu) => this.addMenuItem(menu, scale));
  }

  addMenuItem(menu: RichMenuItem, scale: Scale) {
    const m = new MenuItem(
      {
        draggable: true,
        scaleX: scale.scale_x,
        scaleY: scale.scale_y,
        x: menu.pos_x * scale.scale_x,
        y: menu.pos_y * scale.scale_y,
        dragBoundFunc: (pos) => {
          const newHeight: number = this.getHeight() - (this.isDirectionVert ? 65 : 95);
          const newY: number = pos.y > newHeight ? newHeight : pos.y;
          return {
            x: pos.x,
            y: newY,
          };
        },
      },
      menu,
      {
        fill: this.richMenu.text_color,
        fontFamily: this.richMenu.font_family,
      },
      this.isDirectionVert,
    );

    this.add(m);

    this.draw();
  }

  dataToJSON(scale: Scale): RichMenu {
    const data: any = {};
    const rich_menu_items: RichMenuItem[] = [];
    const childrens = this.getChildren();

    childrens.toArray().forEach((child) => {
      if (child instanceof KonvaImage) {
        data.pos_x = child.x() / scale.scale_x;
        data.pos_y = child.y() / scale.scale_y;
        if (!!this.richMenu.image_url) {
          const new_width = (child.getWidth() * child.scaleX()) / scale.scale_x;
          data.ratio = new_width / this.richMenu.image_width;
        } else {
          data.ratio = 1;
        }
      }

      if (child instanceof MenuItem) {
        rich_menu_items.push(
          new RichMenuItem().deserialize({
            ...child.menu,
            pos_x: child.x() / scale.scale_x,
            pos_y: child.y() / scale.scale_y,
          }),
        );
      }
    });

    return this.richMenu.deserialize({
      ...data,
      items: rich_menu_items,
    });
  }

  updateScale(oldScale: Scale, scale: Scale) {
    const childrens = this.getChildren();

    childrens.toArray().forEach((child) => {
      child.scale({
        x: scale.scale_x,
        y: scale.scale_y,
      });

      child.x((child.x() / oldScale.scale_x) * scale.scale_x);
      child.y((child.y() / oldScale.scale_y) * scale.scale_y);
    });

    this.draw();
  }

  updateStyle() {
    const childrens = this.getChildren();
    childrens.toArray().forEach((child) => {
      if (child instanceof MenuItem) {
        const chil2 = child.getChildren();
        chil2.toArray().forEach((c) => {
          if (c.name() !== 'rect-transparent') {
            c.setAttr('fill', this.richMenu.text_color);
            c.setAttr('fontFamily', this.richMenu.font_family);
          }
        });
      }
    });
    this.draw();
  }

  updateRichMenu(richMenu: RichMenu) {
    this.richMenu = richMenu;
  }

  clearLayout() {
    const childrens = this.getChildren();

    childrens.toArray().forEach((child) => {
      child.destroy();
    });
    this.draw();
  }
}
