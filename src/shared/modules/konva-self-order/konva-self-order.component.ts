import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Input, OnDestroy } from '@angular/core';
import { KonvaSelfOrderStage } from './stage';
import { RichMenu } from 'shared/models/rich-menu';
import { SETTING_RICH_MENU, SELF_ORDER, KONVAJS_TYPE } from 'shared/enums/event-emitter';
import { ToastService } from 'shared/logical-services/toast.service';
import { MAIN_IMAGE } from './item-layer';
import { ScreenSize } from 'shared/models/screen-size';
import { SCREENS_SIZE } from 'shared/constants/screen-size';
import {
  SELF_ORDER_IMAGE_SIZE,
  SELF_ORDER_RECOMMEND_SIZE,
  SELF_ORDER_SCREEN_DIRECTION,
} from 'shared/enums/konva-size-config';
import { TopScreenService } from 'shared/http-services/top-screen.service';
import { ItemCategoryService } from 'shared/http-services/item-category.service';
import { ItemCategory } from 'shared/models/item-category';
import { PresignedUrls } from 'shared/interfaces/presigned-url';
import { Subject, Subscription, forkJoin, ObservableInput } from 'rxjs';
import { STYLE_SETTING_DEFAULT } from 'shared/constants/style-config-default';
import { RichMenuItem } from 'shared/models/rich-menu-item';

@Component({
  selector: 'app-konva-self-order',
  templateUrl: './konva-self-order.component.html',
  styleUrls: ['./konva-self-order.component.scss'],
})
export class KonvaSelfOrderComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('CategoryLayout') categoryLayout: ElementRef;
  @ViewChild('formFiles') formFiles: ElementRef;

  private _stage: KonvaSelfOrderStage;
  @Input('onEventEmitter') onEventEmitter: Subject<any>;
  @Input('pattern') pattern: number;

  private _itemCategory: ItemCategory;
  @Input('itemCategory')
  get itemCategory(): ItemCategory {
    return this._itemCategory;
  }
  set itemCategory(v: ItemCategory) {
    this._itemCategory = v;
  }

  private _page: number;
  @Input('page')
  get page(): number {
    return this._page;
  }
  set page(v: number) {
    this._page = v;
  }

  private _direction: string;
  @Input('direction')
  get direction(): string {
    return this._direction;
  }
  get isDirectionVert(): boolean {
    return this.direction === SELF_ORDER_SCREEN_DIRECTION.VERT;
  }
  set direction(v: string) {
    this._direction = v;
  }

  currentRichMenu: RichMenu = new RichMenu();
  private maxFileSize: number = 3145728; // 3MB
  fileAccept: string = 'image/jpeg,image/png,image/jpg';

  lastDist = 0;
  activeShape = null;
  isLoading: boolean;

  screenSize = new ScreenSize().deserialize({
    ...SCREENS_SIZE[0].value,
    img_width: SELF_ORDER_IMAGE_SIZE.WIDTH,
    img_height: SELF_ORDER_IMAGE_SIZE.HEIGHT,
  });

  private _subEvent: Subscription;

  constructor(
    private toastSv: ToastService,
    private itemCatSv: ItemCategoryService,
    private topImageSv: TopScreenService,
  ) {}

  ngOnInit() {
    if (this.isDirectionVert) {
      this.screenSize = new ScreenSize().deserialize({
        ...SCREENS_SIZE[2].value,
        img_width: SELF_ORDER_RECOMMEND_SIZE.WIDTH,
        img_height: SELF_ORDER_RECOMMEND_SIZE.HEIGHT,
      });
    }

    if (!this.itemCategory.self_order_rich_menu_settings.length) {
      this._initNewPage(this.page);
    }

    this.currentRichMenu = this.itemCategory.self_order_rich_menu_settings[this.page - 1];
    this._categoryEventEmitter();
  }

  ngOnDestroy() {
    if (this._subEvent) {
      this._subEvent.unsubscribe();
    }
  }

  ngAfterViewInit() {
    this._initLayoutSelfOrderLayout();
    this._eventDraggable();
  }

  private _categoryEventEmitter() {
    this._subEvent = this.onEventEmitter.subscribe((res) => {
      if (!res || !res.type) {
        return;
      }

      switch (res.type) {
        case SETTING_RICH_MENU.CHANGE_RICH_MENU:
          this._addMenuItem(res.data);
          break;
        case SELF_ORDER.CHANGE_SETTING_DISPLAY:
          this.currentRichMenu.updateSettingDisplay(res.data);
          this._stage.mainLayer.updateRichMenu(this.currentRichMenu);
          this._stage.mainLayer.updateStyle();
          break;
        case SELF_ORDER.SAVE_SETING_RICH_MENU:
          if (res.data.pattern === 2) {
            return;
          }
          this._saveSettingSelfOrder();
          break;
        case SELF_ORDER.CHANGE_SLIDE:
          this._updateLayout(res.data);
          break;
        case SELF_ORDER.ACCEPT_REMOVE_SLIDE:
          this._removePage(res.data);
          break;
        case SELF_ORDER.CHANGE_PATTERN:
          this.page = 1;
          break;
      }
    });
  }

  private _addMenuItem(menu: RichMenuItem) {
    menu.pos_x /= this.screenSize.scale.scale_x;
    menu.pos_y /= this.screenSize.scale.scale_y;
    this._stage.mainLayer.addMenuItem(menu, this.screenSize.scale);

    if (!this.currentRichMenu.items) {
      this.currentRichMenu.items = [];
    }
    this.currentRichMenu.items.push(menu);
    this.itemCategory.self_order_rich_menu_settings[this.page - 1] = this.currentRichMenu;
  }

  private _removePage(page: number) {
    let oldItem = [];
    const data = this.itemCategory.self_order_rich_menu_settings
      .filter((item: RichMenu) => {
        if (item.page === page) {
          oldItem = item.items;
        }
        return item.page !== page;
      })
      .map((res: RichMenu, index) => {
        res.page = index + 1;
        return res;
      });
    this.itemCategory.self_order_rich_menu_settings = data;
    this._updateLayout({ page: 1 }, true);

    this.onEventEmitter.next({
      type: SELF_ORDER.REMOVE_LIST_ITEM,
      data: oldItem,
    });
  }

  private _initNewPage(page) {
    const newPage = new RichMenu().deserialize({
      page: page,
      pattern: this.pattern,
      pos_x: 0,
      pos_y: 0,
      items: [],
      ...STYLE_SETTING_DEFAULT,
    });
    this.itemCategory.self_order_rich_menu_settings[page - 1] = newPage;
  }

  private _eventDraggable() {
    this._stage.on('dragend', (evt: any) => {
      this._removeMenuItem(evt);
    });

    this._stage.on('click touchstart tap', (event: any) => {
      if (event.target.getName() === MAIN_IMAGE.name) {
        this.activeShape = event.target;
      } else {
        this.activeShape = null;
      }
    });

    this._stage.on('dragmove', (event: any) => {
      if (!event.evt.targetTouches) {
        return;
      }
      const evt = event.evt;

      const touch1 = evt.targetTouches[0];
      const touch2 = evt.targetTouches[1];

      if (touch1 && touch2 && this.activeShape) {
        const dist = this._getDistance(
          {
            x: touch1.clientX,
            y: touch1.clientY,
          },
          {
            x: touch2.clientX,
            y: touch2.clientY,
          },
        );

        if (!this.lastDist) {
          this.lastDist = dist;
        }

        const scale = (this.activeShape.scaleX() * dist) / this.lastDist;

        this.activeShape.scaleX(scale);
        this.activeShape.scaleY(scale);
        this._stage.mainLayer.draw();
        this.lastDist = dist;
      }
    });

    this._stage.on('contentTouchend', () => {
      this.lastDist = 0;
    });
  }

  private _getDistance(p1, p2) {
    return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
  }

  private _removeMenuItem(evt: any) {
    const currentPos = evt.currentTarget.getPointerPosition();

    const maxPosX = this._stage.mainLayer.width();
    const maxPosY = this._stage.mainLayer.height();

    if (currentPos.x < 5 || currentPos.y < 5 || currentPos.x > maxPosX - 5 || currentPos.y > maxPosY - 5) {
      if (evt.target.menu) {
        this.onEventEmitter.next({
          type: SETTING_RICH_MENU.REMOVE_MENU_ITEM,
          data: evt.target.menu,
        });
        evt.target.destroy();
        this._stage.draw();
        this.toastSv.info('メニューが一旦リッチメニューから削除されます');
      }
    }
  }

  private _updateLayout(data: any, isRemove?: boolean) {
    if (data.page === this.page) {
      return;
    }

    if (!isRemove) {
      const konvaData = this._stage.mainLayer.dataToJSON(this.screenSize.scale);

      this.itemCategory.self_order_rich_menu_settings[this.page - 1].deserialize({
        ...konvaData,
      });
    }

    this._stage.mainLayer.clearLayout();
    this.page = data.page;
    if (data.isNew || !this.itemCategory.self_order_rich_menu_settings[this.page - 1]) {
      this._initNewPage(data.page);
    }

    this.currentRichMenu = this.itemCategory.self_order_rich_menu_settings[this.page - 1];
    this._stage.mainLayer.initRichMenu(this.currentRichMenu, this.screenSize.scale, this.isDirectionVert);
  }

  private _setScreenSize() {
    if (!this._stage) {
      return;
    }

    this._stage.width(this.screenSize.screenWidth);
    this._stage.height(this.screenSize.screenHeight);

    this._stage.draw();
  }

  private _initLayoutSelfOrderLayout() {
    this._stage = new KonvaSelfOrderStage({
      container: this.categoryLayout.nativeElement,
    });

    this._stage.initLayer();
    this._stage.initRichMenu(this.currentRichMenu, this.screenSize.scale, this.isDirectionVert);

    this._setScreenSize();
  }

  private _saveSettingSelfOrder() {
    if (!this.itemCategory.self_order_rich_menu_settings.length || this.isLoading) {
      return;
    }
    this.isLoading = true;

    if (!this.itemCategory.totalSelfOrderUploadFile) {
      return this._saveData();
    }
    this._presignedUrls();
  }

  private _presignedUrls() {
    const data = {
      quantity: this.itemCategory.totalSelfOrderUploadFile,
    };
    this.topImageSv.presignedUrls(data).subscribe(
      (res) => {
        this._publicImage(res.data);
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  private _publicImage(presUrls: PresignedUrls[]) {
    let i: number = 0;
    const urlsUpload: Array<ObservableInput<any>> = [];

    this.itemCategory.self_order_rich_menu_settings.forEach((item, index) => {
      if (item.file_upload) {
        item.image_path = presUrls[i].path;
        this.itemCategory.self_order_rich_menu_settings[index] = item;
        const url = this.topImageSv.publicImagePath(presUrls[i].url, item.file_upload, item.file_upload.type);
        urlsUpload.push(url);
        i++;
      }
    });

    forkJoin(urlsUpload).subscribe((res) => {
      this._saveData();
    });
  }

  private _saveData() {
    this.itemCategory.self_order_rich_menu_settings[this.page - 1] = this._stage.mainLayer.dataToJSON(
      this.screenSize.scale,
    );

    this.itemCatSv.settingSelfOrder(this.itemCategory.id, this.itemCategory.settingToJson()).subscribe(
      () => {
        this.onEventEmitter.next({
          type: SETTING_RICH_MENU.CLOSE_MODAL_SETTING_RICH_MENU,
        });
        this.toastSv.success('セルフオーダー端末画面を設定しました。');
        this.isLoading = false;
      },
      (errors) => {
        if (errors.hasOwnProperty('errors')) {
          this.toastSv.error(errors.errors);
        } else {
          this.toastSv.error(errors);
        }
        this.isLoading = false;
      },
    );
  }

  private _onLoadImage(element: any, image_url: string, file: File) {
    this.currentRichMenu.changeImage({
      image_url: image_url,
      image_width: element.composedPath()[0].width,
      image_height: element.composedPath()[0].height,
      file_upload: file,
    });
    this._stage.updateMainImage(this.currentRichMenu, this.screenSize.scale);

    this.itemCategory.self_order_rich_menu_settings[this.page - 1] = this.currentRichMenu;

    this.onEventEmitter.next({
      type: SELF_ORDER.CHANGE_IMAGE_RICH_MENU,
      data: image_url,
    });
  }

  handleFile(files: FileList) {
    const file: File = files.item(0);
    if (file) {
      if (this.fileAccept.indexOf(file.type) === -1) {
        this.toastSv.info(`jpg, jpegとpngのファイルでアップロードしてください`);
        return false;
      }

      if (file.size > this.maxFileSize) {
        this.toastSv.info(`サイズが3MBを超える画像はアップロードできません`);
        return false;
      }

      const reader: FileReader = new FileReader();
      reader.onloadend = (e: any) => {
        const newImg = new Image();
        newImg.src = e.target.result;
        newImg.onload = (img: any) => {
          this._onLoadImage(img, e.target.result, file);
        };
      };
      reader.readAsDataURL(file);

      this.formFiles.nativeElement.value = null;
    }
  }

  uploadImage(el: any) {
    if (!!this.currentRichMenu.image_url) {
      this.currentRichMenu.removeImage();
      this.itemCategory.self_order_rich_menu_settings[this.page - 1] = this.currentRichMenu;
      this._stage.removeImage();
      this.onEventEmitter.next({
        type: KONVAJS_TYPE.REMOVE_MAIN_IMAGE,
      });
      return;
    }

    el.click();
  }
}
