import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KonvaSelfOrderComponent } from './konva-self-order.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [KonvaSelfOrderComponent],
  exports: [KonvaSelfOrderComponent],
})
export class KonvaSelfOrderModule {}
