export const MAIN_IMAGE = {
  name: 'main-image',
  draggable: true,
};

export const ITEM_STYLES = {
  WIDTH: 347,
  HEIGHT: 123,
  BACKGROUND: 'rgba(0, 0, 0, 0.4)',
  TITLE_FONT_SIZE: 18,
  TITLE_WIDTH: 220,
  PRICE_FONT_SIZE: 16,
  PRICE_WIDTH: 145,
  IMG_PLUS_WIDTH: 71,
  IMG_PLUS_HEIGHT: 71,
};

export const CONFIG_TITLE = {
  fontSize: ITEM_STYLES.TITLE_FONT_SIZE,
  align: 'left',
  verticalAlign: 'middle',
  ellipsis: true,
  shadowColor: 'red',
  wrap: 'none',
  fill: '#fff',
  name: 'menu-name',
  x: 0,
  y: 10,
  width: ITEM_STYLES.WIDTH,
};

export const CONFIG_PRICE = {
  fontSize: ITEM_STYLES.PRICE_FONT_SIZE,
  align: 'left',
  verticalAlign: 'middle',
  ellipsis: true,
  shadowColor: 'red',
  wrap: 'none',
  fill: '#fff',
  name: 'menu-price',
  x: 0,
  y: 80,
  width: ITEM_STYLES.WIDTH,
};
