import { Stage, StageConfig } from 'konva';
import { MainSelfOrderLayer } from './main-layer';
import { RichMenu } from 'shared/models/rich-menu';
import { Scale } from 'shared/models/scale';

export class KonvaSelfOrderStage extends Stage {
  private _mainLayer: MainSelfOrderLayer;
  get mainLayer() {
    return this._mainLayer;
  }

  constructor(stageConfig: StageConfig) {
    super(stageConfig);
  }

  initLayer() {
    this._initMainLayer();
  }

  initRichMenu(richMenu: RichMenu, scale: Scale, isDirectionVert: boolean = false) {
    this._mainLayer.initRichMenu(richMenu, scale, isDirectionVert);
  }

  updateMainImage(richMenu: RichMenu, scale: Scale) {
    this._mainLayer.changeImage(richMenu, scale);
  }

  removeImage() {
    const imageObj = new Image();
    this._mainLayer.mainImage.image(imageObj);
    this._mainLayer.batchDraw();
  }

  private _initMainLayer() {
    this._mainLayer = new MainSelfOrderLayer();
    this.add(this._mainLayer);
  }
}
