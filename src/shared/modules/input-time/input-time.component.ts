import { Component, OnInit, forwardRef, Input, ViewEncapsulation, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import * as moment from 'moment';

import * as $ from 'jquery';
import { PopoverDirective } from 'ngx-bootstrap/popover';

const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => InputTimeComponent),
  multi: true,
};

@Component({
  selector: 'app-input-time',
  templateUrl: './input-time.component.html',
  styleUrls: ['./input-time.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
  encapsulation: ViewEncapsulation.None,
})
export class InputTimeComponent implements OnInit, ControlValueAccessor {
  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;
  @ViewChild('popover') popover: PopoverDirective;
  @Input('class') class: string;
  @Input('required') required: boolean;
  @Input('placement') placement: string = 'left';
  @Input('disabled') disabled: boolean;
  @Input('containerClass') containerClass: string = '';

  // ngModel value
  private _time: moment.Moment;
  public get time(): moment.Moment {
    return this._time;
  }
  public set time(v: moment.Moment) {
    if (v !== this.time) {
      this._time = v;

      this._onChangeCallback(this.time.format('HH:mm:ss'));
    }
  }
  get time_display(): string {
    return this.time && this.time.format('HH:mm');
  }

  public focused: number;

  private _digit1: string;
  public get digit1(): string {
    return this._digit1;
  }
  public set digit1(v: string) {
    this._digit1 = v;
  }
  private _digit2: string;
  public get digit2(): string {
    return this._digit2;
  }
  public set digit2(v: string) {
    this._digit2 = v;
  }
  private _digit3: string;
  public get digit3(): string {
    return this._digit3;
  }
  public set digit3(v: string) {
    this._digit3 = v;
  }
  private _digit4: string;
  public get digit4(): string {
    return this._digit4;
  }
  public set digit4(v: string) {
    this._digit4 = v;
  }

  constructor() {}

  ngOnInit() {}

  /**
   * Overriden for interface ControlValueAccessor
   */
  public onTouched() {
    this._onTouchedCallback();
  }

  public writeValue(value: string) {
    if (value) {
      this.time = moment(value, 'HH:mm:ss');
      this._initDigit(this.time);
    }
  }

  private _initDigit(time: moment.Moment) {
    const h = time.get('hour');
    if (h > 9) {
      this.digit1 = h.toString()[0];
      this.digit2 = h.toString()[1];
    } else {
      this.digit1 = '0';
      this.digit2 = h.toString();
    }

    const m = time.get('minute');
    if (m > 9) {
      this.digit3 = m.toString()[0];
      this.digit4 = m.toString()[1];
    } else {
      this.digit3 = '0';
      this.digit4 = m.toString();
    }
  }

  public registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  public registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  public clear() {
    this.digit1 = null;
    this.digit2 = null;
    this.digit3 = null;
    this.digit4 = null;
    this.focused = 1;
  }

  public currentClicked(e: string) {
    this._initDigit(moment(e, 'HH:mm:ss'));
  }

  public delete() {
    this[`digit${this.focused}`] = null;

    if (this.focused <= 1) {
      return;
    }

    this.focused--;
  }

  public clickedDigit(digit: number) {
    switch (this.focused) {
      case 1:
        if (digit >= 3) {
          return;
        }
        break;
      case 2:
        if (+(this.digit1 + digit) >= 24) {
          return;
        }
        break;
      case 3:
        if (digit >= 6) {
          return;
        }
        break;
      case 4:
        if (+(this.digit3 + digit) >= 60) {
          return;
        }
        break;

      default:
        break;
    }

    this[`digit${this.focused}`] = digit.toString();

    if (this.focused >= 4) {
      return;
    }

    this.focused++;
  }

  public onShown() {
    jQuery(() => {
      const el = $('.input-time-container');

      if (el.offset().top + el.innerHeight() > window.innerHeight) {
        el.addClass('offset-bottom');
        return;
      }

      if (el.offset().top < 0) {
        el.addClass('offset-top');
        return;
      }
    });

    this._initDigit(this.time);
    this.focused = 1;
  }

  public onHidden() {
    this.focused = null;
    this.clear();
  }

  public focusDigit(digitId: number) {
    this.focused = digitId;
  }

  public confirmed() {
    const dateTime: moment.Moment = moment(`${this.digit1}${this.digit2}:${this.digit3}${this.digit4}:00`, 'HH:mm:ss');

    if (!dateTime.isValid) {
      return;
    }

    this.popover.hide();

    this.time = dateTime;
  }
}
