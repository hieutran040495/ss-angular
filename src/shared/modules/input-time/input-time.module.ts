import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTimeComponent } from './input-time.component';
import { FormsModule } from '@angular/forms';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { KeyboardNumberModule } from '../keyboard-number/keyboard-number.module';

@NgModule({
  imports: [CommonModule, FormsModule, PopoverModule.forRoot(), KeyboardNumberModule],
  declarations: [InputTimeComponent],
  exports: [InputTimeComponent, FormsModule],
})
export class InputTimeModule {}
