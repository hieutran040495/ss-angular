import { Component, Input, ViewChild, ElementRef, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

import { TopImageStage } from './stage';
import { Subject, Subscription } from 'rxjs';
import { TopImage } from 'shared/models/top-image';
import { SELF_ORDER, SETTING_DISPLAY, KONVAJS_TYPE } from 'shared/enums/event-emitter';
import { ScreenSize } from 'shared/models/screen-size';
import { SCREENS_SIZE } from 'shared/constants/screen-size';
import {
  SELF_ORDER_TOP_IMAGE_SIZE,
  SELF_ORDER_TOP_IMAGE_SIZE_VERT,
  SELF_ORDER_SCREEN_DIRECTION,
} from 'shared/enums/konva-size-config';
import { TopScreenService } from 'shared/http-services/top-screen.service';
import { ToastService } from 'shared/logical-services/toast.service';
import { PresignedUrls } from 'shared/interfaces/presigned-url';
import { TopScreen } from 'shared/models/top-screen';
import { StyleOpts } from 'shared/interfaces/style-setting';

@Component({
  selector: 'app-cas-top-image',
  templateUrl: './cas-top-image.component.html',
  styleUrls: ['./cas-top-image.component.scss'],
})
export class CasTopImageComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('TopImageLayout') topImageLayout: ElementRef;
  @Input('onEventEmitter') onEventEmitter: Subject<any>;

  @Input('topScreen')
  private _topScreen: TopScreen;
  get topScreen(): TopScreen {
    return this._topScreen;
  }
  set topScreen(v: TopScreen) {
    this._topScreen = v;
  }

  private _stylesheets: StyleOpts;
  @Input('stylesheets')
  get stylesheets(): StyleOpts {
    return this._stylesheets;
  }
  set stylesheets(v: StyleOpts) {
    this._stylesheets = v;
  }

  private _direction: string;
  @Input('direction')
  get direction(): string {
    return this._direction;
  }
  get isDirectionVert(): boolean {
    return this.direction === SELF_ORDER_SCREEN_DIRECTION.VERT;
  }
  set direction(v: string) {
    this._direction = v;
  }

  private topImage: TopImage;
  screenSize = new ScreenSize().deserialize({
    ...SCREENS_SIZE[0].value,
    img_width: SELF_ORDER_TOP_IMAGE_SIZE.WIDTH,
    img_height: SELF_ORDER_TOP_IMAGE_SIZE.HEIGHT,
  });

  private _stage: TopImageStage;
  private _fileUpload: File;
  private _subEvent: Subscription;
  isLoading: boolean;

  constructor(private topImageSv: TopScreenService, private toastSv: ToastService) {}

  ngOnInit() {
    this.topImage = this.topScreen.top_screen;
    if (this.isDirectionVert) {
      this.screenSize = new ScreenSize().deserialize({
        ...SCREENS_SIZE[2].value,
        img_width: SELF_ORDER_TOP_IMAGE_SIZE_VERT.WIDTH,
        img_height: SELF_ORDER_TOP_IMAGE_SIZE_VERT.HEIGHT,
      });
    }

    this._topScreenEventEmitter();
  }

  ngOnDestroy() {
    this._subEvent.unsubscribe();
  }

  ngAfterViewInit() {
    this._initTopImageLayout();
  }

  private _setScreenSize() {
    if (!this._stage) {
      return;
    }

    this._stage.width(this.screenSize.screenWidth);
    this._stage.height(this.screenSize.screenHeight);
    this._stage.draw();
  }

  private _initTopImageLayout() {
    this._stage = new TopImageStage({
      container: this.topImageLayout.nativeElement,
    });

    this._stage.initLayer();
    this._stage.initTopImage(this.topImage, this.screenSize.scale, this.isDirectionVert);

    this._setScreenSize();
  }

  private _topScreenEventEmitter() {
    this._subEvent = this.onEventEmitter.subscribe((res) => {
      if (!res || !res.type) {
        return;
      }

      switch (res.type) {
        case SELF_ORDER.CHANGE_TOP_IMAGE:
          this._fileUpload = res.data;
          return this._stage.updateMainImage(this.topImage, this.screenSize.scale);
        case SELF_ORDER.SAVE_TOP_SCREEN:
          return this._saveTopScreen();
        case KONVAJS_TYPE.REMOVE_MAIN_IMAGE:
          this._fileUpload = undefined;
          this._stage.removeImage();
          return;
        case SELF_ORDER.CHANGE_SETTING_DISPLAY:
          return this._stage.mainLayer.updateStyle(this.topImage);
      }
    });
  }

  private _saveTopScreen() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;

    if (this._fileUpload) {
      return this._presignedUrls();
    }
    this._saveSetting();
  }

  private _presignedUrls() {
    const data = {
      quantity: 1,
    };
    this.topImageSv.presignedUrls(data).subscribe(
      (res) => {
        this._publicImage(res.data[0]);
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  private _publicImage(data: PresignedUrls) {
    this.topImageSv.publicImagePath(data.url, this._fileUpload, this._fileUpload.type).subscribe(
      (res) => {
        this.topImage.deserialize({
          image_path: data.path,
        });
        this._saveSetting();
      },
      (errors) => {
        this.toastSv.error(errors);
        this.isLoading = false;
      },
    );
  }

  private _saveSetting() {
    const _imgData = this._stage.mainLayer.dataToJSON(this.screenSize.scale);

    this.topImage.deserialize({
      ..._imgData,
    });

    this.topImageSv.settingTopScreen(this.topScreen.id, this.topImage.toJSON()).subscribe(
      (res) => {
        this.isLoading = false;
        this.onEventEmitter.next({
          type: SETTING_DISPLAY.SUCCESS,
        });
        this.toastSv.success('セルフオーダーの来店画面を設定しました');
      },
      (errors) => {
        this.toastSv.error(errors);
        this.onEventEmitter.next({
          type: SETTING_DISPLAY.ERROR,
        });
        this.isLoading = false;
      },
    );
  }
}
