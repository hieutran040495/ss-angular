import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasTopImageComponent } from './cas-top-image.component';

describe('CasTopImageComponent', () => {
  let component: CasTopImageComponent;
  let fixture: ComponentFixture<CasTopImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CasTopImageComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasTopImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
