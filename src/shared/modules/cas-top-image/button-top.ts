import { Group, ContainerConfig, Rect, Text, RectConfig } from 'konva';
import { TopImage } from 'shared/models/top-image';
import { NAME_CONFIG, BUTTON_CONFIG, BUTTON_CONFIG_VERT, NAME_CONFIG_VERT } from './button-config';

export class Button extends Group {
  public topImage: TopImage;
  private _background: Rect;
  private _name: Text;

  private isDirectionVert: boolean = false;

  constructor(containerConfig: ContainerConfig, topImage: TopImage, name: string, isDirectionVert: boolean = false) {
    super(containerConfig);
    this.topImage = topImage;
    this.isDirectionVert = isDirectionVert;
    this._initBackground();
    this._initName(name);
  }

  private _initBackground(config?: RectConfig) {
    const globalConfig = this.isDirectionVert ? BUTTON_CONFIG_VERT : BUTTON_CONFIG;
    globalConfig.stroke = this.topImage.text_color;

    const opts: RectConfig = {
      ...globalConfig,
      ...config,
    };

    this._background = new Rect(opts);

    this.add(this._background);
  }

  private _initName(name: string) {
    const globalConfig = this.isDirectionVert ? NAME_CONFIG_VERT : NAME_CONFIG;
    globalConfig.fill = this.topImage.text_color;

    this._name = new Text({
      ...globalConfig,
      text: name,
    });

    this.add(this._name);
  }
}
