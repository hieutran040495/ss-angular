import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasTopImageComponent } from './cas-top-image.component';
import { FormsModule } from '@angular/forms';
import { TopScreenService } from 'shared/http-services/top-screen.service';

@NgModule({
  declarations: [CasTopImageComponent],
  imports: [CommonModule, FormsModule],
  exports: [CasTopImageComponent],
  providers: [TopScreenService],
})
export class CasTopImageModule {}
