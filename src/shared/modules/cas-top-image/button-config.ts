export const BUTTON_CONFIG = {
  width: 326.62,
  height: 52.79,
  radius: 40,
  strokeWidth: 1,
  stroke: '#fff',
  cornerRadius: 60,
};

export const BUTTON_CONFIG_VERT = {
  ...BUTTON_CONFIG,
  width: 452,
  height: 82,
};

export const NAME_CONFIG = {
  fontSize: 17,
  align: 'center',
  verticalAlign: 'middle',
  ellipsis: true,
  wrap: 'none',
  fill: '#fff',
  name: 'btn-name',
  y: 18.3,
  width: 326.62,
};

export const NAME_CONFIG_VERT = {
  ...NAME_CONFIG,
  fontSize: 28,
  width: 452,
  y: 28.1,
};
