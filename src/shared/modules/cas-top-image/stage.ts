import { Stage, StageConfig } from 'konva';
import { TopImageLayer } from './top-image-layer';
import { TopImage } from 'shared/models/top-image';
import { Scale } from 'shared/models/scale';

export class TopImageStage extends Stage {
  private _topImageLayer: TopImageLayer;
  public get mainLayer() {
    return this._topImageLayer;
  }
  constructor(stageConfig: StageConfig) {
    super(stageConfig);
  }

  initLayer() {
    this._topImageLayer = new TopImageLayer();
    this.add(this._topImageLayer);
  }

  initTopImage(topImage: TopImage, scale: Scale, isDirectionVert: boolean = false) {
    this._topImageLayer.initTopImage(topImage, scale, isDirectionVert);
  }

  removeImage() {
    const imageObj = new Image();
    this._topImageLayer.mainImage.image(imageObj);
    this._topImageLayer.batchDraw();
  }

  updateMainImage(topImage: TopImage, scale: Scale) {
    this._topImageLayer.changeImage(topImage, scale);
  }
}
