import { Layer, LayerConfig, Text } from 'konva';
import { KonvaImage } from 'shared/models/konva-image';
import { TopImage } from 'shared/models/top-image';
import { Scale } from 'shared/models/scale';
import { Button } from './button-top';
import { Metadata, ButtonTop } from 'shared/models/button-top';

export class TopImageLayer extends Layer {
  mainImage: KonvaImage;
  topImage: TopImage;

  constructor(layerConfig?: LayerConfig) {
    super(layerConfig);
  }

  public initTopImage(topImage: TopImage, scale: Scale, isDirectionVert: boolean = false) {
    this.topImage = topImage;
    const imageObj = new Image();
    this.mainImage = new KonvaImage({
      image: imageObj,
      name: 'top-image',
      draggable: true,
      x: 0,
      y: 0,
    });

    this.add(this.mainImage);

    imageObj.onload = () => {
      this.updateTopImg(imageObj, scale);
    };

    imageObj.src = topImage.image_url || '';

    this._drawButton(scale, isDirectionVert);
  }

  changeImage(topImage: TopImage, scale: Scale) {
    const imageObj = new Image();
    imageObj.onload = () => {
      this.updateTopImg(imageObj, scale);
    };
    imageObj.src = topImage.image_url;
  }

  private updateTopImg(imageObj: any, scale: Scale) {
    this.mainImage.scale({
      x: scale.scale_x,
      y: scale.scale_y,
    });
    this.mainImage.width(this.topImage.image_width);
    this.mainImage.height(this.topImage.image_height);

    this.mainImage.x(this.topImage.pos_x * scale.scale_x);
    this.mainImage.y(this.topImage.pos_y * scale.scale_y);
    this.mainImage.image(imageObj);
    this.draw();
  }

  private _drawButton(scale: Scale, isDirectionVert: boolean = false) {
    const btnFirst = new Button(
      {
        draggable: true,
        x: this.topImage.metadata.btn_1.pos_x * scale.scale_x,
        y: this.topImage.metadata.btn_1.pos_y * scale.scale_y,
      },
      this.topImage,
      'オーダー方法',
      isDirectionVert,
    );

    const btnSecond = new Button(
      {
        draggable: true,
        x: this.topImage.metadata.btn_2.pos_x * scale.scale_x,
        y: this.topImage.metadata.btn_2.pos_y * scale.scale_y,
      },
      this.topImage,
      '注文画面',
      isDirectionVert,
    );

    if (isDirectionVert) {
      btnFirst.scale({
        x: scale.scale_x,
        y: scale.scale_y,
      });
      btnSecond.scale({
        x: scale.scale_x,
        y: scale.scale_y,
      });
    }

    this.add(btnFirst);
    this.add(btnSecond);
    this.draw();
  }

  updateStyle(topImage: TopImage) {
    this.topImage = topImage;

    const childrens = this.getChildren();
    childrens.toArray().forEach((child) => {
      if (child instanceof Button) {
        const chil2 = child.getChildren();
        chil2.toArray().forEach((c) => {
          this._settAttr(c);
        });
      }
    });
    this.draw();
  }

  private _settAttr(child: any) {
    if (child instanceof Text) {
      child.setAttr('fill', this.topImage.text_color);
      child.setAttr('fontFamily', this.topImage.font_family);
    } else {
      child.setAttr('stroke', this.topImage.text_color);
    }
  }

  public dataToJSON(scale: Scale) {
    const data: any = {};
    const metadata: Metadata = new Metadata();

    const childrens = this.getChildren();

    childrens.toArray().forEach((child, index) => {
      if (child instanceof KonvaImage) {
        data.pos_x = child.x() / scale.scale_x;
        data.pos_y = child.y() / scale.scale_y;
        const new_width = (child.getWidth() * child.scaleX()) / scale.scale_x;
        data.ratio = new_width / this.topImage.image_width;
      }

      if (child instanceof Button) {
        const btn = {
          pos_x: child.x() / scale.scale_x,
          pos_y: child.y() / scale.scale_y,
        };

        if (index === 1) {
          metadata.btn_1 = new ButtonTop().deserialize(btn);
        } else {
          metadata.btn_2 = new ButtonTop().deserialize(btn);
        }
      }
    });

    return { ...data, metadata: metadata };
  }
}
