import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DigitFormaterComponent } from './digit-formater.component';

describe('DigitFormaterComponent', () => {
  let component: DigitFormaterComponent;
  let fixture: ComponentFixture<DigitFormaterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DigitFormaterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DigitFormaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
