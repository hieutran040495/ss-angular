import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DigitFormaterComponent } from './digit-formater.component';
import { FormsModule } from '@angular/forms';
import { UnFocusModule } from 'shared/directive/un-focus/us-focus.module';

@NgModule({
  imports: [CommonModule, FormsModule, UnFocusModule],
  declarations: [DigitFormaterComponent],
  exports: [DigitFormaterComponent],
})
export class DigitFormaterModule {}
