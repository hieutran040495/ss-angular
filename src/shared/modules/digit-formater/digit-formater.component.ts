import {
  Component,
  forwardRef,
  Input,
  ViewChild,
  ElementRef,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Regexs } from 'shared/constants/regex';

const noop = () => {};
const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DigitFormaterComponent),
  multi: true,
};

@Component({
  selector: 'app-digit-formater',
  templateUrl: './digit-formater.component.html',
  styleUrls: ['./digit-formater.component.scss'],
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DigitFormaterComponent implements ControlValueAccessor {
  @ViewChild('InputEl') inputEl: ElementRef;
  @Input('max') max: number;
  @Input('min') min: number;
  @Input('allowNegative') allowNegative: boolean = false;

  rules = Regexs;

  private _placeholder: string;
  @Input('placeholder')
  public get placeholder(): string {
    return this._placeholder;
  }
  public set placeholder(v: string) {
    this._cd.markForCheck();
    this._placeholder = v;
  }

  private _validator: string;
  @Input('validator')
  public get validator(): string {
    return this._validator;
  }
  public set validator(v: string) {
    this._cd.markForCheck();
    this._validator = v;
  }

  private _required: string;
  @Input('required')
  public get required(): string {
    return this._required;
  }
  public set required(v: string) {
    this._cd.markForCheck();
    this._required = v;
  }
  public get isRequired(): boolean {
    return !!this.required;
  }

  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;

  // ngModel value
  private _value: number;
  public get value(): number {
    return this._value;
  }
  public set value(v: number) {
    if (v !== this.value) {
      this._value = v;
      this._onChangeCallback(v);
    }
  }
  public get valueFormat(): string {
    return this.value || this.value === 0 ? this.value.format() : '';
  }
  public set changeValue(v: string) {
    const target = v.replace(/\,/gi, '');

    if (target === '') {
      this.value = null;
      return;
    }

    if (this.allowNegative && target === '-') {
      return;
    }

    if (isNaN(target.convertNumber())) {
      this.inputEl.nativeElement.value = this.valueFormat;
      return;
    }

    if (target.convertNumber() > this.max) {
      this.value = target.substr(0, this.max.toString().length).convertNumber();
      this.inputEl.nativeElement.value = this.valueFormat;
      return;
    }

    if (target.convertNumber() < this.min) {
      this.value = this.min;
      this.inputEl.nativeElement.value = this.valueFormat;
      return;
    }

    if (target.convertNumber() === 0) {
      this.value = 0;
      this.inputEl.nativeElement.value = '0';
      return;
    }

    this.value = target.convertNumber();
  }

  constructor(private _cd: ChangeDetectorRef) {}

  /**
   * Overriden for interface ControlValueAccessor
   */
  public onTouched() {
    this._onTouchedCallback();
  }

  public writeValue(v: number) {
    this.value = v;
  }

  public registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  public registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  handleKeyPress(event: KeyboardEvent) {
    if (!Regexs.only_number_minus.test(event.key)) {
      event.preventDefault();
    }
  }
}
