import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { CalendarComponent } from './calendar.component';
import { CalendarService } from './calendar.service';
import { CasSwitchTimebarModule } from 'app/modules/cas-switch-timebar/cas-switch-timebar.module';
import { ClickOutsideModule } from 'shared/directive/click-outside/click-outside.module';

@NgModule({
  imports: [CommonModule, FormsModule, BsDatepickerModule.forRoot(), CasSwitchTimebarModule, ClickOutsideModule],
  declarations: [CalendarComponent],
  exports: [CalendarComponent],
  providers: [CalendarService],
})
export class CalendarModule {}
