import { Table } from 'shared/models/table';
import { FcResource } from 'shared/models/fc-resource';

export class CalendarResourceMixin {
  initResources(data: Table[]): FcResource[] {
    return data.map((table) => {
      const input: any = {
        id: `table_${table.id}`,
        table,
        group: table.group,
      };

      return new FcResource().deserialize(input);
    });
  }

  resourceRender(res: FcResource, labelEl: JQuery, bodyEl: JQuery) {
    labelEl.find('.fc-cell-content').html(`
      <div class="fc-info-seat">
        <div class="seat-item seat-item--name">
          <span class="seat-item--bold text-truncate w-100 mb-0" title="${res.table.code}">
            ${res.table.code}
          </span>
          <img src="${res.table.can_smoke ? 'assets/icons/icon_smoke.png' : 'assets/icons/icon_nosmoke.png'}">
        </div>

        <div class="seat-item seat-item--quantity">
          <span class="seat-item--info text-truncate" title="${res.table.type.name}">
            <span class="seat-item--bold">${res.table.quantity}</span>
            名席${res.table.type.name}
          </span>

          <div class="seat-item--res-count">
            <img src="assets/icons/icon_resvs_count.png">
            <span class="seat-item--bold">${res.table.reservations_count}</span>件
          </div>
        </div>
      </div>
    `);
  }
}
