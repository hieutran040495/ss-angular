import * as $ from 'jquery';
import 'fullcalendar';
import 'fullcalendar-scheduler';
import 'fullcalendar/dist/locale/ja.js';
import { environment } from 'environments/environment';

import { OptionsInput } from 'fullcalendar/src/types/input-types';
import { FcResource } from 'shared/models/fc-resource';
import { FcEvent } from 'shared/models/fc-event';
import ResourceTimelineView from 'fullcalendar-scheduler/src/resource-timeline/ResourceTimelineView';

import * as debounce from 'lodash/debounce';
import { Calendar } from 'fullcalendar';

export class CalendarMixin {
  otherHeight: number;
  resources: FcResource[];
  calendar: Calendar = undefined;
  _el: string;
  _eventsResv: FcEvent[];

  initCalendar(el: string, overrideConfig: Partial<OptionsInput> = {}) {
    const containerEl: JQuery = $(el);

    const config: OptionsInput = {
      schedulerLicenseKey: environment.fc_key,
      defaultView: 'timelineDay',
      header: {
        left: '',
        center: '',
        right: '',
      },
      titleFormat: 'YYYY/MM/DD [(]ddd[)]',
      locale: 'ja',
      slotLabelFormat: 'HH:mm',
      // slotLabelFormat: <any>['MM/DD [(]ddd[)]', 'HH:mm'],
      timezone: 'local',
      resourceAreaWidth: 210,
      nowIndicator: true,
      resourceLabelText: '席',
      refetchResourcesOnNavigate: false,
      windowResize: (view: ResourceTimelineView) => {
        containerEl.fullCalendar('option', 'height', window.innerHeight - this.otherHeight);
      },
      // resourceGroupField: 'group',
      // resourceGroupText: (groupValue) => {
      //   if (groupValue) {
      //     return this.resources
      //       .filter((resource) => resource.group === groupValue)
      //       .map((table) => table.table.code)
      //       .join('-');
      //   }
      //   return groupValue;
      // },
      // resourceGroupField: 'resvName',
      ...overrideConfig,
    };

    containerEl.fullCalendar(config);

    return containerEl.fullCalendar('getCalendar');
  }

  onScrollStopped(el: JQuery, callback: Function) {
    el.on('scroll', debounce(callback, 250));
  }

  _resetDataCalendar() {
    $(this._el)
      .fullCalendar('getResources')
      .map((resource) => {
        this.calendar.removeResource(resource);
      });

    $(this._el).fullCalendar('removeEventSource', this._eventsResv);

    $(this._el).fullCalendar('removeEvents');
  }

  _reloadCalendar() {
    $(this._el).fullCalendar('refetchEvents');
    $(this._el).fullCalendar('refetchResources');
  }
}
