import { Injectable } from '@angular/core';
import { TableService } from 'shared/http-services/table.service';
import { ReservationService } from 'shared/http-services/reservation.service';
import { Reservation } from 'shared/models/reservation';
import { Table } from 'shared/models/table';
import { ToastService } from 'shared/logical-services/toast.service';

@Injectable()
export class CalendarService {
  constructor(private tableSv: TableService, private resvSv: ReservationService, private _toastSv: ToastService) {}

  public async getResourcesCalendar(opts: any = {}): Promise<Table[]> {
    try {
      const res = await this.tableSv.getTables(opts).toPromise();

      if (res.data) {
        return res.data;
      }

      return [];
    } catch (error) {
      this._toastSv.error(error);
    }
  }

  public async getEventsCalendar(opts: any = {}): Promise<Reservation[]> {
    try {
      const res = await this.resvSv.getReservationsCalendar(opts).toPromise();

      if (res.data) {
        return res.data;
      }

      return [];
    } catch (error) {
      this._toastSv.error(error);
    }
  }
}
