import { Reservation } from 'shared/models/reservation';
import { FcEvent, FcEventInput } from 'shared/models/fc-event';
import { RESV_TYPE, RESV_TYPE_CLASS } from 'shared/enums/reservation';

export class CalendarEventMixin {
  initEvents(data: Reservation[]): FcEvent[] {
    const res: FcEvent[] = [];

    data.forEach((resv) => {
      resv.tables.forEach((table) => {
        const input: FcEventInput = {
          id: `resv_${resv.id}_table_${table.id}`,
          title: resv.client_user ? resv.client_user.name : '',
          reservation: resv,
          resourceId: `table_${table.id}`,
          start: resv.start_at,
          end: resv.end_at,
          isDraft: false,
          resourceEditable: true,
        };

        switch (resv.type) {
          case RESV_TYPE.WEB_DRAFT:
            input.className = RESV_TYPE_CLASS.WEB_DRAFT;
            break;
          case RESV_TYPE.PHONE_DRAFT:
            input.className = RESV_TYPE_CLASS.PHONE_DRAFT;
            break;
          case RESV_TYPE.WALK_IN:
            input.className = RESV_TYPE_CLASS.WALK_IN;
            break;
          case RESV_TYPE.WEB:
            input.className = RESV_TYPE_CLASS.WEB;
            break;
          case RESV_TYPE.PHONE:
            input.className = RESV_TYPE_CLASS.PHONE;
            break;
        }

        if (resv.isWaitForPay) {
          input.className = RESV_TYPE_CLASS.WAIT_FOR_PAY;
        }

        input.className += ` is-${resv.status}`;
        res.push(new FcEvent().deserialize(input));
      });
    });

    return res;
  }

  eventRender(event: FcEvent, el: JQuery, isUseSmpOrder: boolean) {
    const renderName = event.reservation.resv_name_display ? event.reservation.resv_name_display + ': ' : '';
    const classPaid = event.reservation.is_paid_or_prepaid ? 'badge-paid' : 'badge-no-paid';
    if (event.isDraft) {
      return;
    }

    el.attr('data-event-id', event.id);

    el.find('.fc-content').html(`
      <div class="fc-resv-info">
        <p>
          ${renderName} ${event.reservation.totalQuantity} ${event.reservation.resvInfoCalendar}
        </p>
        <p>
          <span class="fc-resv-info_badge ${classPaid}">${event.reservation.paid_status_text}</span>
          ${event.reservation.isShowOrderedStatus ? '<span class="fc-resv-info_badge badge-ordered">注文済</span>' : ''}

          <span class="fc-resv-info_code">${
            event.reservation.status !== 'finished' &&
            isUseSmpOrder &&
            event.reservation.smp_code &&
            !event.reservation.paid_at
              ? event.reservation.smp_code
              : ''
          }</span>
        </p>
      </div>
    `);
  }
}
