import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  AfterViewInit,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';

import { FcEvent } from 'shared/models/fc-event';
import { FcResource } from 'shared/models/fc-resource';
import { Reservation } from 'shared/models/reservation';
import { Table } from 'shared/models/table';

import { OptionsInput } from 'fullcalendar/src/types/input-types';

import { applyMixins } from 'shared/utils/apply-mixins';
import { CalendarMixin } from 'shared/modules/calendar/calendar.mixin';
import { Calendar } from 'fullcalendar';

import * as $ from 'jquery';

import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { BS_DATEPICKER_CONFIG } from 'shared/constants/datepicker-config';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { jaLocale } from 'ngx-bootstrap/locale';
import { ClientWorkingsQuery } from 'shared/states/client_workings';
import { Subscription } from 'rxjs/Subscription';
import { CALENDAR_EVENT_EMITTER, CALENDAR_MODE, RESV_MODE_EMITTER } from 'shared/enums/event-emitter';
import { CalendarService } from './calendar.service';
import * as moment from 'moment';
import { ClientWorkingTimeWeek } from 'shared/models/client-working-week';
import { RootScopeService } from 'shared/logical-services/root-scope.service';
import { Subject } from 'rxjs';
import { CalendarEventMixin } from './calendar-event.mixin';
import { CalendarResourceMixin } from './calendar-resource.mixin';
import { EventEmitterService } from 'shared/logical-services/event-emitter.service';
import { SwitchTimebar } from 'shared/models/switch-timebar';
import { SESSION_STORAGE_KEY } from 'shared/enums/session-storage';
import { ClientProfileQuery } from 'shared/states/client-profile';

defineLocale(jaLocale.abbr, jaLocale);
interface CalendarConfig extends OptionsInput {
  slotWidth: number;
}
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CalendarComponent
  implements OnInit, AfterViewInit, OnDestroy, CalendarMixin, CalendarEventMixin, CalendarResourceMixin {
  private _mode: string = CALENDAR_MODE.VIEW;
  get mode_select(): boolean {
    return this._mode === CALENDAR_MODE.SELECT_TABLE;
  }

  otherHeight = 50;

  @Input('eventUpdateCalendar') eventUpdateCalendar: Subject<any>;
  @Input('instructionClass') insClass: string = '';
  @Input('isDisabledBtnSwitchTimebar') isDisabledBtnSwitchTimebar: boolean = false;

  @Output() eventClick: EventEmitter<any> = new EventEmitter();
  @Output() dayClick: EventEmitter<any> = new EventEmitter();
  @Output() resourcesClick: EventEmitter<any> = new EventEmitter();
  @Output() scrollEndReached: EventEmitter<any> = new EventEmitter();
  @Output() eventDrop: EventEmitter<any> = new EventEmitter();

  initCalendar: (el: string, overrideConfig?: Partial<CalendarConfig>) => Calendar;
  initEvents: (data: Reservation[]) => FcEvent[];
  eventRender: (event: FcEvent, element: JQuery, isUseSmpOrder: boolean) => void;
  initResources: (data: Table[]) => FcResource[];
  resourceRender: (resource: FcResource, labelTds: JQuery, bodyTds: JQuery) => void;
  onScrollStopped: (el: JQuery, callback: Function) => void;
  _resetDataCalendar: () => void;
  _reloadCalendar: () => void;

  _el = '#Calendar';
  calendar: Calendar = undefined;
  _eventsResv: FcEvent[] = [];
  _eventSelected: FcEvent[] = [];
  _draftEvents: FcEvent[] = [];
  _dragEvent: FcEvent;

  public resources: FcResource[] = [];

  public selectedDate: Date = moment().toDate();
  public selectedResv: Reservation;
  private _keepDate: Date;

  public bsConfig = BS_DATEPICKER_CONFIG;
  private _subscriber: Subscription;
  private workingTime: ClientWorkingTimeWeek | any;

  public isLoadingResource = false;
  public isLoadingEvents = false;
  private _currentScrollLeft: number = 0;

  private businessHours: any[] = [];
  public isShowSwitchTimebar: boolean = false;
  private _pageSub: Subscription;
  private _switchTimeBar: SwitchTimebar = new SwitchTimebar();
  isUseSmpOrder: boolean;

  constructor(
    private localeService: BsLocaleService,
    private clientWorkingsQuery: ClientWorkingsQuery,
    private _calendarSv: CalendarService,
    private _rootScope: RootScopeService,
    private _eventEmitterCalendar: EventEmitterService,
    private clientProfileQuery: ClientProfileQuery,
  ) {
    this.localeService.use(jaLocale.abbr);
    if (JSON.parse(sessionStorage.getItem(SESSION_STORAGE_KEY.SWITCH_TIMEBAR))) {
      this._switchTimeBar.deserialize(JSON.parse(sessionStorage.getItem(SESSION_STORAGE_KEY.SWITCH_TIMEBAR)));
    }
  }

  async ngOnInit() {
    if (moment(this.selectedDate).hour() < 6) {
      this.selectedDate = moment(this.selectedDate)
        .subtract(1, 'day')
        .toDate();
    }
    this._onEventEmitterCalendar();
    this._onEventEmitter();
    await this.clientWorkingsQuery
      .getWorkingTime()
      .then((res) => {
        this.workingTime = res;
        this.settingBusinessHours();
        return this.clientProfileQuery.getClientProfile();
      })
      .then((res: any) => {
        this.isUseSmpOrder = res.allow_smp_order;
        this._initCalendar();
      });

    this._currentScrollLeft = this._rootScope.scroll_time || 0;
  }

  ngAfterViewInit() {}

  ngOnDestroy() {
    if (this._subscriber) {
      this._subscriber.unsubscribe();
    }

    if (this._pageSub) {
      this._pageSub.unsubscribe();
    }

    const scrollLeft = $('.fc-time-area .fc-scroller.fc-no-scrollbars').scrollLeft();
    this._rootScope.scroll_time = scrollLeft;
  }

  private settingBusinessHours(): void {
    const businessHours: any[] = [];
    this.workingTime.clientWorkingToArray().map((item: any) => {
      item.durations.map((sItem: any) => {
        const times: string[] = sItem.end_at.split(':');

        if (+times[0] <= 5) {
          times[0] = `${24 + +times[0]}`;
        }

        businessHours.push({
          dow: [sItem.weekday],
          start: sItem.start_at,
          end: times.join(':'),
        });
      });
    });
    this.businessHours = businessHours;
  }

  private _onEventEmitterCalendar() {
    this._pageSub = this._eventEmitterCalendar.caseNumber$.subscribe((res) => {
      switch (res.type) {
        case RESV_MODE_EMITTER.CREATE:
        case RESV_MODE_EMITTER.EDIT:
        case CALENDAR_EVENT_EMITTER.EDIT_WALKIN:
        case CALENDAR_EVENT_EMITTER.CREATE_WALKIN:
        case CALENDAR_EVENT_EMITTER.CREATE_RES_CHANGE_DATE:
        case CALENDAR_EVENT_EMITTER.CHANGE_DURATION:
          this.isDisabledBtnSwitchTimebar = true;
          break;
        case CALENDAR_EVENT_EMITTER.DISABLE_CHOOSE_TABLE:
          if (res.close) {
            this.isDisabledBtnSwitchTimebar = false;
          }
          break;
        case CALENDAR_EVENT_EMITTER.CHANGE_SMP_CODE:
          this._reloadCalendar();
          break;
      }
    });
  }

  private _onEventEmitter() {
    this._subscriber = this.eventUpdateCalendar.subscribe((res) => {
      if (res) {
        switch (res.type) {
          case CALENDAR_EVENT_EMITTER.DISABLE_CHOOSE_TABLE:
            if (this._mode === CALENDAR_MODE.VIEW) {
              return;
            }
            this._mode = CALENDAR_MODE.VIEW;
            this._clearDraftEvents();
            this._clearSelectedTable();

            if (this._keepDate && !moment(this._keepDate).isSame(moment(this.selectedDate), 'day')) {
              this.selectedDate = moment(this._keepDate)
                .clone()
                .toDate();
              this._keepDate = null;
              this.calendar.gotoDate(this.selectedDate);
              this._reloadCalendar();
              this._updateMinMaxTime();
            }

            break;
          case CALENDAR_EVENT_EMITTER.CHANGE_DATE:
            if (this._mode === CALENDAR_MODE.SELECT_TABLE) {
              this._keepDate = moment(res.data).toDate();
              return;
            }

            if (
              moment(this.selectedDate, 'YYYY-MM-DD')
                .startOf('d')
                .diff(moment(res.data).startOf('d'), 'day') !== 0
            ) {
              this.selectedDate = moment(res.data).toDate();
              this.calendar.gotoDate(this.selectedDate);
              this._reloadCalendar();
              this._updateMinMaxTime();
            }
            break;
          case CALENDAR_EVENT_EMITTER.RELOAD:
            this._reloadCalendar();

            const selectTableTimeout = setTimeout(() => {
              if (this._mode === CALENDAR_MODE.SELECT_TABLE) {
                this.handleSelectedTable();
              }
              clearTimeout(selectTableTimeout);
            }, 2000);

            break;
          case CALENDAR_EVENT_EMITTER.ENABLE_CHOOSE_TABLE:
            if (this._mode === CALENDAR_MODE.SELECT_TABLE) {
              return;
            }
            this._mode = CALENDAR_MODE.SELECT_TABLE;
            this.selectedResv = res.data;
            this.handleSelectedTable();
            break;
          case CALENDAR_EVENT_EMITTER.CHANGE_DURATION:
            if (this._mode === CALENDAR_MODE.VIEW) {
              return;
            }

            this._initEventsCanChoose(res.data.tables, res.data);
            break;
        }
      }
    });
  }

  private handleSelectedTable() {
    this._initSelectedTable(this.selectedResv.tables.map((t) => `table_${t.id}`));
    this._initEventsCanChoose(this.selectedResv.tables, this.selectedResv);
  }

  private _initCalendar() {
    const time = this.workingTime.getWorkingTime(moment(this.selectedDate));
    const dimmerEL = document.getElementsByClassName('dimmer');
    if (!time) {
      if (dimmerEL.length > 0) {
        dimmerEL[0].classList.add('active');
      }
    } else {
      if (dimmerEL.length > 0) {
        dimmerEL[0].classList.remove('active');
      }
    }

    const config: Partial<CalendarConfig> = {
      events: (start, end, timezone, callback) => {
        callback([]);
      },
      eventClick: (event: FcEvent, jsEvent, view) => {
        if (event.isDraft) {
          // handle choose event
          const jsEl = $(jsEvent.currentTarget);
          if (jsEl.hasClass('active')) {
            jsEl.find('.fc-title').text('席を選択する');
          } else {
            jsEl.find('.fc-title').text('選択済');
          }

          jsEl.toggleClass('active');

          const el = <HTMLElement>(
            document.querySelector(`.fc-resource-area [data-resource-id=${event.resourceId}] .fc-widget-content`)
          );
          el.click();

          return;
        }

        if (this._mode === CALENDAR_MODE.SELECT_TABLE) {
          return;
        }

        // Event Click
        this.eventClick.emit({
          reservation: event.reservation,
          jsEvent: jsEvent,
          view: view,
        });
      },
      dayClick: (date, jsEvent, view, resourceObj: FcResource) => {
        if (this._mode === CALENDAR_MODE.SELECT_TABLE) {
          return;
        }
        const data: any = {
          date: date,
          table: resourceObj.table,
        };

        this.dayClick.emit(data);
      },
      eventRender: (event: FcEvent, el: JQuery) => this.eventRender(event, el, this.isUseSmpOrder),
      resources: (callback, start, end) => {
        callback([]);
        this._getTables();
      },
      resourceRender: (resourceObj: FcResource, labelTds: JQuery, bodyTds: JQuery) => {
        this.resourceRender(resourceObj, labelTds, bodyTds);

        labelTds.on('click', () => {
          const events = $(this._el).fullCalendar('clientEvents', (e: FcEvent) => {
            return e.resourceId === resourceObj.id && e.isDraft;
          });

          if (events.length > 0) {
            const e = events[0];

            if (e.className.indexOf('active') >= 0) {
              e.className.splice(e.className.indexOf('active'), 1);
              e.title = '席を選択する';
            } else {
              e.className.push('active');
              e.title = '選択済';
            }

            $(this._el).fullCalendar('updateEvent', e);

            this.resourceRenderEvent(resourceObj);
          }
        });
      },
      eventAfterAllRender: (view) => {
        if (this._currentScrollLeft > 0) {
          $('.fc-time-area .fc-scroller').scrollLeft(this._currentScrollLeft);
        } else {
          const newPosition = $('.fc-now-indicator').position();
          if (newPosition) {
            $('.fc-time-area .fc-scroller').scrollLeft(newPosition.left - 50);
          }
        }

        $('.fc-time-area .fc-scroller.fc-no-scrollbars').off('scroll');
        $('.fc-time-area .fc-scroller.fc-no-scrollbars').on('scroll', (e) => {
          this._currentScrollLeft = e.target.scrollLeft;
        });

        const domEl = $('.fc-timeline .fc-body .fc-scroller');

        this.onScrollStopped(domEl, () => {
          if (domEl.scrollTop() + domEl.innerHeight() >= domEl[0].scrollHeight) {
            // call api loadmore resource
            this.scrollEndReached.emit('end reached');
          }
        });
      },
      lazyFetching: true,
      defaultDate: moment(this.selectedDate),
      aspectRatio: 2,
      height: window.innerHeight - this.otherHeight,
      businessHours: this.businessHours,
      // default minTime/maxTime
      // maxTime: (time && time['end']) || '23:59:00',
      // minTime: (time && time['start']) || '00:00:00',
      maxTime: '29:59:00',
      minTime: '06:00:00',
      eventLongPressDelay: 100,
      slotDuration: `00:${this._switchTimeBar.slot_duration}:00`,
      slotWidth: this._switchTimeBar.slot_width,
      eventDragStart: (event: FcEvent, jsEvent, ui, view) => {
        const dom: any = $(jsEvent.currentTarget).find(
          `[data-resource-id=${event.resourceId}] [data-event-id=${event.id}]`,
        );
        dom.addClass('fc-event-old-drag');
        this._dragEvent = event;
        this.calendar.option('businessHours', {
          start: moment(event.start).format('HH:mm'),
          end: moment(event.end).format('HH:mm'),
          dow: [0, 1, 2, 3, 4, 5, 6, 7],
        });
      },
      eventDragStop: (event: FcEvent, jsEvent, ui, view) => {
        const dom: any = $(jsEvent.currentTarget).find(
          `[data-resource-id=${event.resourceId}] [data-event-id=${event.id}]`,
        );
        dom.removeClass('fc-event-old-drag');
        this.calendar.option('businessHours', this.businessHours);
        this.calendar.option('jsEvent', {});
      },
      eventDrop: (event: FcEvent, delta, revertFunc: Function) => {
        const data = {
          resv: event.reservation,
          oldTable: Number(String(this._dragEvent.resourceId).substr(6)),
          newTable: Number(String(event.resourceId).substr(6)),
          revertFunc,
        };
        this.eventDrop.emit(data);
      },
      eventAllow: (dropLocation, draggedEvent) => {
        if (this._mode === CALENDAR_MODE.SELECT_TABLE) {
          return false;
        }

        return true;
      },
    };

    this.calendar = this.initCalendar(this._el, config);
  }

  private resourceRenderEvent(resourceObj: FcResource) {
    if (this._mode === CALENDAR_MODE.VIEW) {
      return;
    }
    this._toggleRows(resourceObj);
    const data = {
      table: resourceObj.table,
    };
    this.resourcesClick.emit(data);
  }

  private _initSelectedTable(tables: string[] = []) {
    tables.forEach((tableId) => {
      $(this._el)
        .find(`[data-resource-id=${tableId}]`)
        .addClass('active');
    });
  }

  private _toggleRows(resource: FcResource) {
    $(this._el)
      .find(`[data-resource-id=${resource.id}]`)
      .toggleClass('active');
  }

  private _clearSelectedTable() {
    $(this._el)
      .find(`[data-resource-id]`)
      .removeClass('active');
  }

  private _updateMinMaxTime() {
    const time = this.workingTime.getWorkingTime(this.calendar.getDate());

    const dimmerEL = document.getElementsByClassName('dimmer');
    if (!time) {
      if (dimmerEL.length > 0) {
        dimmerEL[0].classList.add('active');
      }
    } else {
      if (dimmerEL.length > 0) {
        dimmerEL[0].classList.remove('active');
      }
    }

    // update maxTime/minTime
    // $(this._el).fullCalendar('option', {
    //   minTime: (time && time['start']) || '00:00:00',
    //   maxTime: (time && time['end']) || '23:59:00',
    // });
  }

  private async _getTables() {
    if (this.isLoadingResource) {
      return;
    }

    let time = this.workingTime.getWorkingTime(moment(this.selectedDate));

    if (!time) {
      time = {
        start: '06:00:00',
        end: '29:59:00',
      };
    }

    try {
      const startDuration = moment.duration('06:00:00');
      const endDuration = moment.duration('29:59:00');

      const opts = {
        with: 'type,reservations_count',
        order: 'ordinal_number',
        start_at: moment(this.selectedDate)
          .set({
            hour: Math.floor(startDuration.asHours()),
            minute: startDuration.minutes(),
            second: 0,
            millisecond: 0,
          })
          .toISOString(),
        end_at: moment(this.selectedDate)
          .set({
            hour: Math.floor(endDuration.asHours()),
            minute: endDuration.minutes(),
            second: 0,
            millisecond: 0,
          })
          .toISOString(),
      };

      this.isLoadingResource = true;

      const res = await this._calendarSv.getResourcesCalendar(opts);

      this._resetDataCalendar();

      this.resources = this.initResources(res);

      this.resources.forEach((resource) => {
        this.calendar.addResource(resource);
      });

      // handle draft events;
      if (this._draftEvents && this._draftEvents.length > 0) {
        this.calendar.addEventSource(this._draftEvents);
        const tables: string[] = this._draftEvents
          .filter((e: FcEvent) => {
            return e.className.indexOf('active') > 0;
          })
          .map((e) => String(e.resourceId));
        this._initSelectedTable(tables);

        this._draftEvents = [];
      }

      this._getReservations();

      this.isLoadingResource = false;
      return;
    } catch (error) {
      this.isLoadingResource = false;
    }
  }

  private async _getReservations() {
    if (this.isLoadingEvents) {
      return;
    }

    const time = this.workingTime.getWorkingTime(moment(this.selectedDate));

    if (!time) {
      return;
    }

    try {
      const startDuration = moment.duration('06:00:00');
      const endDuration = moment.duration('29:59:00');
      const opts = {
        with: `coupon,tables,client_user,keywords,preorder_items_count,${
          this.isUseSmpOrder ? 'reservation_passcode' : ''
        }`,
        status_not: 'canceled',
        start_at: moment(this.selectedDate)
          .set({
            hour: Math.floor(startDuration.asHours()),
            minute: startDuration.minutes(),
            second: 0,
            millisecond: 0,
          })
          .toISOString(),
        end_at: moment(this.selectedDate)
          .set({
            hour: Math.floor(endDuration.asHours()),
            minute: endDuration.minutes(),
            second: 0,
            millisecond: 0,
          })
          .toISOString(),
      };
      this.isLoadingEvents = true;
      let res = await this._calendarSv.getEventsCalendar(opts);
      if (this._mode === CALENDAR_MODE.SELECT_TABLE) {
        res = res.filter((item) => item.id !== this.selectedResv.id);
      }
      this._eventsResv = this.initEvents(res);
      this.calendar.addEventSource(this._eventsResv);

      this.isLoadingEvents = false;

      return;
    } catch (error) {
      this.isLoadingEvents = false;
    }
  }

  private _initEventsCanChoose(selectedTables: Table[] = [], resv: Reservation) {
    this._clearDraftEvents();
    const start = moment(resv.start_date, 'YYYY-MM-DD').set('minute', moment.duration(resv.start_time).asMinutes());
    if (
      moment.duration(resv.start_time).asHours() < 6 &&
      moment(this.selectedDate)
        .startOf('day')
        .isSame(moment(resv.start_date, 'YYYY-MM-DD').startOf('day'))
    ) {
      start.add(1, 'day');
    }
    const end = start.clone().add(resv.duration, 'minute');

    const resourceIdsOverlap: string[] = $(this._el)
      .fullCalendar('clientEvents', (e: FcEvent) => {
        if (e.reservation.id === resv.id) {
          this._eventSelected.push(e);

          $(this._el).fullCalendar('removeEvents', e.id);

          return false;
        }
        return !e.isDraft && moment(e.end).isAfter(start) && moment(e.start).isBefore(end);
      })
      .map((e: FcEvent) => e.resourceId);

    const events = this.resources
      .filter((resource) => {
        return !resourceIdsOverlap.includes(resource.id);
      })
      .map((resource) => {
        const e = new FcEvent().deserialize({
          id: `resv_draft_table_${resource.table.id}`,
          title: '席を選択する',
          reservation: resv,
          resourceId: `table_${resource.table.id}`,
          start: start,
          end: end,
          isDraft: true,
          className: ['fc-event__resv--draft'],
        });

        if (resv instanceof Reservation) {
          e.className.push('resv');
        } else {
          e.className.push('walkin');
        }

        const i = selectedTables.findIndex((table) => table.id === resource.table.id);

        if (i >= 0) {
          e.className.push('active');
        }

        return e;
      });

    const hour = start.hour();
    this._changeCurrentScrollLeft(hour);
    this.calendar.addEventSource(events);
  }

  private _clearDraftEvents() {
    if (this._eventSelected.length > 0) {
      this.calendar.addEventSource(this._eventSelected);
      this._eventSelected = [];
    }
    $(this._el).fullCalendar('removeEvents', (e: FcEvent) => {
      return e.id.toString().indexOf(`resv_draft_table_`) >= 0;
    });
  }

  private _updateDraftEvents(resv: Reservation) {
    const start = moment(resv.start_date, 'YYYY-MM-DD').set('minute', moment.duration(resv.start_time).asMinutes());
    if (
      moment.duration(resv.start_time).asHours() < 6 &&
      moment(this.selectedDate)
        .startOf('day')
        .isSame(moment(resv.start_date, 'YYYY-MM-DD').startOf('day'))
    ) {
      start.add(1, 'day');
    }

    const hour = start.hour();
    this._changeCurrentScrollLeft(hour);

    this.calendar.gotoDate(moment(resv.start_date, 'YYYY-MM-DD'));
    this._updateMinMaxTime();
  }

  public showSwitchTimebar() {
    this.isShowSwitchTimebar = !this.isShowSwitchTimebar;
  }

  public clickOutside(e: any) {
    if (this.isShowSwitchTimebar) {
      this._changeCalendar(true);
    }
    this.isShowSwitchTimebar = false;
  }

  public closeSwitchTimebar() {
    this.isShowSwitchTimebar = false;
    this._changeCalendar();
  }

  public changeSwitchTimebar(e: any) {
    this._changeCalendar(false, e);
  }

  private _changeCalendar(isOutside?: boolean, data?: any) {
    $('.fc-time-area .fc-scroller').scrollLeft(0);

    if (data) {
      this._switchTimeBar.deserialize(data);
      $(this._el).fullCalendar('option', 'slotDuration', `00:${this._switchTimeBar.slot_duration}:00`);
      $(this._el).fullCalendar('option', 'slotWidth', this._switchTimeBar.slot_width);
    } else {
      if (JSON.parse(sessionStorage.getItem(SESSION_STORAGE_KEY.SWITCH_TIMEBAR))) {
        this._switchTimeBar.deserialize(JSON.parse(sessionStorage.getItem(SESSION_STORAGE_KEY.SWITCH_TIMEBAR)));
      } else {
        this._switchTimeBar = new SwitchTimebar();
      }

      if (isOutside) {
        $(this._el).fullCalendar('option', 'slotDuration', `00:${this._switchTimeBar.slot_duration}:00`);
        $(this._el).fullCalendar('option', 'slotWidth', this._switchTimeBar.slot_width);
      }
    }
  }

  private _changeCurrentScrollLeft(hour: number) {
    let new_hour: number;

    new_hour = hour < 6 ? hour + 24 : hour;

    if (this._switchTimeBar.scale === 2) {
      new_hour = (new_hour - 6) * 2;
    } else {
      new_hour = new_hour - 6;
    }
    this._currentScrollLeft = new_hour * this._switchTimeBar.width_column;
  }
}

applyMixins(CalendarComponent, [CalendarMixin, CalendarResourceMixin, CalendarEventMixin]);
