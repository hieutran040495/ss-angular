import {
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { BsDatepickerConfig, BsDaterangepickerDirective, BsLocaleService } from 'ngx-bootstrap/datepicker';

import { BS_DATEPICKER_CONFIG } from 'shared/constants/datepicker-config';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { deLocale, jaLocale } from 'ngx-bootstrap/locale';

import * as moment from 'moment';
import { ClientWorkingTimeWeek } from 'shared/models/client-working-week';
import { ClientWorkingsQuery } from 'shared/states/client_workings';
import { DatePicker } from 'shared/mixins/date-picker';
import { applyMixins } from 'shared/utils/apply-mixins';
import { DATE_UNIT, DATE_UNIT_CUSTOM } from 'shared/constants/date';
import { DATE_TYPE, MIN_MODE } from 'shared/enums/date';
import { DateFilter } from 'shared/interfaces/date';

const locales = [jaLocale, deLocale];
locales.forEach((locale) => defineLocale(locale.abbr, locale));
const noop = () => {};

const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  /* tslint:disable-next-line: no-use-before-declare */
  useExisting: forwardRef(() => NgxDatepickerFilterComponent),
  provide: NG_VALUE_ACCESSOR,
  multi: true,
};
@Component({
  selector: 'app-ngx-datepicker-filter',
  templateUrl: './ngx-datepicker-filter.component.html',
  styleUrls: ['./ngx-datepicker-filter.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
})
export class NgxDatepickerFilterComponent implements OnInit, ControlValueAccessor, DatePicker {
  onShownDatepicker: (event) => void;
  _onClickNextPrev: () => void;
  _checkAndAddClassVisible: () => Promise<void>;
  _handleDayOff: (date: moment.Moment, el: JQuery, workingTime: ClientWorkingTimeWeek) => void;

  @Output() change = new EventEmitter<any>();

  @Input('name') name: string = 'date';
  @Input('bsConfig') bsConfig: Partial<BsDatepickerConfig>;
  @Input('placeholder') placeholder: string = 'Select date';
  @Input('minDate') minDate: Date;
  @Input('maxDate') maxDate: Date;
  @Input('locale') locale: string = 'ja';
  @Input('validatorError') validatorError: string;
  @Input() disabled = false;
  @Input('required') required: boolean = false;
  @Input('isLoading') isLoading: boolean;
  @Input('isClearPicker') isClearPicker: boolean = false;
  @Input('placement') placement: string = 'left top';
  @Input('isShowUnit') isShowUnit: boolean = false;
  @Input('isSmall') isSmall: boolean = false;
  @Input('is-unit-custom') isUnitCustom: boolean = false;

  defaultBsConfig: Partial<BsDatepickerConfig> = BS_DATEPICKER_CONFIG;

  selectedDateCalendar: Date = moment().toDate();
  unitType: string = DATE_TYPE.DATE;

  customFrom: Date = moment().toDate();
  customTo: Date = moment()
    .add(1, 'days')
    .toDate();

  private _selectedDate: DateFilter;
  get selectedDate(): DateFilter {
    return this._selectedDate;
  }
  set selectedDate(v: DateFilter) {
    this._selectedDate = v;
    if (v) {
      this._onChangeCallback(this.selectedDateOutput);
    }
  }

  get selectedDateDisplay(): string {
    let startDate: string;
    let endDate: string;
    switch (this.unitType) {
      case DATE_TYPE.DATE:
        return moment(this.selectedDateCalendar).format('YYYY/MM/DD');
      case DATE_TYPE.WEEK:
        startDate = moment(this.selectedDate.created_at_gte).format('YYYY/MM/DD');
        endDate = moment(this.selectedDate.created_at_lte).format('MM/DD');
        return `${startDate} - ${endDate}`;
      case DATE_TYPE.MONTH:
        return moment(this.selectedDateCalendar).format('YYYY/MM');
      case DATE_TYPE.CUSTOM:
        startDate = moment(this.customFrom).format('YYYY/MM/DD');
        endDate = moment(this.customTo).format('YYYY/MM/DD');
        return `${startDate} - ${endDate}`;
    }
  }

  get selectedDateOutput(): DateFilter {
    const formatStr: string = 'YYYY-MM-DD';

    return {
      created_at_gte: moment(this.selectedDate.created_at_gte).format(formatStr),
      created_at_lte: moment(this.selectedDate.created_at_lte).format(formatStr),
    };
  }

  get isUnitTypeCustom(): boolean {
    return this.unitType === DATE_TYPE.CUSTOM;
  }

  isOpen: boolean = false;
  DATE_UNIT = DATE_UNIT;

  @ViewChild('dp') datepicker: BsDaterangepickerDirective;

  constructor(private bsLocaleSv: BsLocaleService, private clientWorkingsQuery: ClientWorkingsQuery) {}

  /**
   * Overriden for interface ControlValueAccessor
   */
  private _onTouchedCallback: () => void = noop;
  private _onChangeCallback: (_: any) => void = noop;

  ngOnInit() {
    this.bsLocaleSv.use(this.locale);
    if (this.bsConfig) {
      this.defaultBsConfig = Object.assign(this.defaultBsConfig, this.bsConfig);
    }

    if (this.isUnitCustom) {
      this.DATE_UNIT = DATE_UNIT_CUSTOM;
    }
  }

  changeUnit() {
    this.selectedDateCalendar = moment().toDate();

    switch (this.unitType) {
      case DATE_TYPE.DATE:
        this.selectedDate = {
          created_at_gte: this.selectedDateCalendar,
          created_at_lte: this.selectedDateCalendar,
        };
        this.defaultBsConfig = Object.assign(this.defaultBsConfig, { minMode: MIN_MODE.DAY });
        this.datepicker.setConfig();
        break;
      case DATE_TYPE.WEEK:
        this.selectedDate = {
          created_at_gte: this.selectedDateCalendar,
          created_at_lte: moment(this.selectedDateCalendar)
            .add(7, 'days')
            .toDate(),
        };
        this.defaultBsConfig = Object.assign(this.defaultBsConfig, { minMode: MIN_MODE.DAY });
        this.datepicker.setConfig();
        break;
      case DATE_TYPE.MONTH:
        this.selectedDate = {
          created_at_gte: moment(this.selectedDateCalendar)
            .startOf('month')
            .toDate(),
          created_at_lte: moment(this.selectedDateCalendar)
            .endOf('month')
            .toDate(),
        };
        this.defaultBsConfig = Object.assign(this.defaultBsConfig, { minMode: MIN_MODE.MONTH });
        this.datepicker.setConfig();
        break;
      case DATE_TYPE.CUSTOM:
        this.selectedDate = {
          created_at_gte: this.customFrom,
          created_at_lte: this.customTo,
        };
        this.defaultBsConfig = Object.assign(this.defaultBsConfig, { minMode: MIN_MODE.DAY });
        this.datepicker.setConfig();
        break;
    }

    this._onChangeCallback(this.selectedDateOutput);
    this.change.emit(this.selectedDateDisplay);
  }

  prevDate() {
    switch (this.unitType) {
      case DATE_TYPE.DATE:
        this.selectedDateCalendar = moment(this.selectedDateCalendar)
          .subtract(1, 'days')
          .toDate();
        this.selectedDate = {
          created_at_gte: this.selectedDateCalendar,
          created_at_lte: this.selectedDateCalendar,
        };
        break;
      case DATE_TYPE.WEEK:
        this.selectedDateCalendar = moment(this.selectedDateCalendar)
          .subtract(7, 'days')
          .toDate();
        this.selectedDate = {
          created_at_gte: this.selectedDateCalendar,
          created_at_lte: moment(this.selectedDateCalendar)
            .add(7, 'days')
            .toDate(),
        };
        break;
      case DATE_TYPE.MONTH:
        this.selectedDateCalendar = moment(this.selectedDateCalendar)
          .subtract(1, 'months')
          .toDate();
        this.selectedDate = {
          created_at_gte: moment(this.selectedDateCalendar)
            .startOf('month')
            .toDate(),
          created_at_lte: moment(this.selectedDateCalendar)
            .endOf('month')
            .toDate(),
        };
        break;
      default:
        break;
    }

    this._onChangeCallback(this.selectedDateOutput);
    this.change.emit(this.selectedDateDisplay);
  }

  nextDate() {
    switch (this.unitType) {
      case DATE_TYPE.DATE:
        this.selectedDateCalendar = moment(this.selectedDateCalendar)
          .add(1, 'days')
          .toDate();
        this.selectedDate = {
          created_at_gte: this.selectedDateCalendar,
          created_at_lte: this.selectedDateCalendar,
        };
        break;
      case DATE_TYPE.WEEK:
        this.selectedDateCalendar = moment(this.selectedDateCalendar)
          .add(7, 'days')
          .toDate();
        this.selectedDate = {
          created_at_gte: this.selectedDateCalendar,
          created_at_lte: moment(this.selectedDateCalendar)
            .add(7, 'days')
            .toDate(),
        };
        break;
      case DATE_TYPE.MONTH:
        this.selectedDateCalendar = moment(this.selectedDateCalendar)
          .add(1, 'months')
          .toDate();
        this.selectedDate = {
          created_at_gte: moment(this.selectedDateCalendar)
            .startOf('month')
            .toDate(),
          created_at_lte: moment(this.selectedDateCalendar)
            .endOf('month')
            .toDate(),
        };
        break;
      default:
        break;
    }

    this._onChangeCallback(this.selectedDateOutput);
    this.change.emit(this.selectedDateDisplay);
  }

  onTouched() {
    this._onTouchedCallback();
  }

  writeValue(value: any) {
    this.selectedDate = value;
  }

  registerOnChange(fn: any) {
    this._onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this._onTouchedCallback = fn;
  }

  onChange(e: any) {
    let createdAtGte: Date = e;
    let createdAtLte: Date = e;

    switch (this.unitType) {
      case DATE_TYPE.WEEK:
        createdAtLte = moment(e)
          .add(7, 'days')
          .toDate();
        break;
      case DATE_TYPE.MONTH:
        createdAtGte = moment(e)
          .startOf('month')
          .toDate();
        createdAtLte = moment(e)
          .endOf('month')
          .toDate();
        break;
    }

    this.selectedDate = {
      created_at_gte: createdAtGte,
      created_at_lte: createdAtLte,
    };

    this._onChangeCallback(this.selectedDateOutput);
    this.change.emit(this.selectedDateOutput);
  }

  customChange(e, isFrom: boolean = true) {
    if (isFrom) {
      this.selectedDate.created_at_gte = e;
    } else {
      this.selectedDate.created_at_lte = e;
    }

    this._onChangeCallback(this.selectedDateOutput);
    this.change.emit(this.selectedDateDisplay);
  }

  clearPicker() {
    this.selectedDate = null;
  }

  onblur(event) {
    $(event.target).trigger('blur');
  }

  _getWorkingTime(): Promise<ClientWorkingTimeWeek> {
    return this.clientWorkingsQuery.getWorkingTime();
  }
}

applyMixins(NgxDatepickerFilterComponent, [DatePicker]);
