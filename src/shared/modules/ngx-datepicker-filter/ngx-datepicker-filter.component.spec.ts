import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxDatepickerFilterComponent } from './ngx-datepicker-filter.component';

describe('NgxDatepickerFilterComponent', () => {
  let component: NgxDatepickerFilterComponent;
  let fixture: ComponentFixture<NgxDatepickerFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NgxDatepickerFilterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxDatepickerFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
