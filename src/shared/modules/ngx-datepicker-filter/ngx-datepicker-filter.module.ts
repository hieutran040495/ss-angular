import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';

import { NgxDatepickerFilterComponent } from './ngx-datepicker-filter.component';
import { CasNgSelectModule } from 'shared/modules/cas-ng-select/cas-ng-select.module';

@NgModule({
  declarations: [NgxDatepickerFilterComponent],
  imports: [CommonModule, FormsModule, BsDatepickerModule.forRoot(), CasNgSelectModule],
  exports: [NgxDatepickerFilterComponent],
  providers: [BsLocaleService],
})
export class NgxDatepickerFilterModule {}
