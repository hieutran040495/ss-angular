export const CLIENT_WORKING_WEEK = [
  {
    weekday: 0,
  },
  {
    weekday: 1,
  },
  {
    weekday: 2,
  },
  {
    weekday: 3,
  },
  {
    weekday: 4,
  },
  {
    weekday: 5,
  },
  {
    weekday: 6,
  },
  {
    weekday: 7,
  },
];

export const WEEK_DAY = ['日', '月', '火', '水', '木', '金', '土', '祝日'];

export enum WORKING_TYPE {
  DAY_OFF = 0,
  FULL_DAY = 1,
  BREAKING = 2,
}

export const WORKING_TYPES = [
  {
    name: '営業日',
    sub: '(休憩あり)',
    value: WORKING_TYPE.BREAKING,
  },
  {
    name: '営業日',
    sub: '(休憩なし)',
    value: WORKING_TYPE.FULL_DAY,
  },
  {
    name: '休業日',
    sub: '',
    value: WORKING_TYPE.DAY_OFF,
  },
];
