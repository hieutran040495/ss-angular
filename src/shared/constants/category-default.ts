import { Category } from 'shared/models/category';

export const catSelectedDefault = new Category().deserialize({
  name: '全て',
  id: undefined,
  items_count: 0,
  item_type: null,
});
