export const OPTIONS_SETTING_DISPLAY: any = {
  text: [
    {
      color: '#FFFFFF',
      content: '白',
      textColor: '#222222',
      borderColor: '#DEDEDE',
    },
    {
      color: '#000000',
      content: '黒',
      textColor: '#FFFFFF',
    },
  ],
  background: [
    {
      color: '#000000',
    },
    {
      color: '#2B363C',
    },
    {
      color: '#959595',
    },
    {
      color: '#FFFFFF',
      borderColor: '#C3C3C3',
    },
  ],
  font: [
    {
      content: 'フォント',
      font: 'Noto Sans CJK JP',
    },
    {
      content: 'フォント',
      font: 'Noto Serif JP',
    },
  ],
};

export const STYLE_SETTING_DEFAULT = {
  text_color: OPTIONS_SETTING_DISPLAY.text[0].color,
  bg_color: OPTIONS_SETTING_DISPLAY.background[0].color,
  font_family: OPTIONS_SETTING_DISPLAY.font[0].font,
};
