export enum DURATIONS_VALUE {
  VALUE_60 = 60,
  VALUE_90 = 90,
  VALUE_120 = 120,
  VALUE_150 = 150,
  VALUE_180 = 180,
  VALUE_210 = 210,
  VALUE_240 = 240,
}

export enum DURATIONS_TEXT {
  TEXT_60 = '1時間',
  TEXT_90 = '1時間半',
  TEXT_120 = '2時間',
  TEXT_150 = '2時間半',
  TEXT_180 = '3時間',
  TEXT_210 = '3時間半',
  TEXT_240 = '4時間',
}

export const DURATIONS = [
  {
    name: '1時間',
    value: DURATIONS_VALUE.VALUE_60,
  },
  {
    name: '1時間半',
    value: DURATIONS_VALUE.VALUE_90,
  },
  {
    name: '2時間',
    value: DURATIONS_VALUE.VALUE_120,
  },
  {
    name: '2時間半',
    value: DURATIONS_VALUE.VALUE_150,
  },
  {
    name: '3時間',
    value: DURATIONS_VALUE.VALUE_180,
  },
  {
    name: '3時間半',
    value: DURATIONS_VALUE.VALUE_210,
  },
  {
    name: '4時間',
    value: DURATIONS_VALUE.VALUE_240,
  },
];
