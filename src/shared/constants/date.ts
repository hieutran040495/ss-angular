export const DATE_UNIT = [
  {
    value: 'date',
    label: '日',
  },
  {
    value: 'week',
    label: '週',
  },
  {
    value: 'month',
    label: '月',
  },
];

export const DATE_UNIT_CUSTOM = [
  {
    value: 'date',
    label: '日',
  },
  {
    value: 'week',
    label: '週',
  },
  {
    value: 'custom',
    label: 'カスタム',
  },
];
