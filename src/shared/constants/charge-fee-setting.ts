export const chargeFeeTypes: any = [
  {
    label: '固定する',
    value: 'amount',
  },
  {
    label: '割合にする',
    value: 'percent',
  },
];

export const serviceFeeBys: any = [
  {
    label: '人数単位',
    value: 'qty',
  },
  {
    label: '座席単位',
    value: 'resv',
  },
];
