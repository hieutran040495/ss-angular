export const BS_DATEPICKER_CONFIG: any = {
  dateInputFormat: `YYYY年 MM月 DD日`,
  rangeInputFormat: `YYYY年 MM月 DD日 (ddd)`,
  rangeSeparator: ' ~ ',
  showWeekNumbers: false,
  selectFromOtherMonth: true,
  containerClass: 'cashless-theme',
};
