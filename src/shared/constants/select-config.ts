export const CAS_NG_SELECT_DEFAULT_CONFIG = {
  notFoundText: 'データが見つかりません',
  addTagText: 'アイテムを追加',
  typeToSearchText: '',
  loadingText: '読み込み',
  clearAllText: 'すべてクリア',
};
