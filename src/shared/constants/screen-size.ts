export const SCREENS_SIZE = [
  {
    title: 'Apple iPad Pro (1st Generation) (2732x2048)',
    value: {
      width: 2732,
      height: 2048,
    },
  },
  {
    title: 'Apple iPad Mini 1 (1st Generation) (1024x768)',
    value: {
      width: 1024,
      height: 768,
    },
  },
  {
    title: 'chromebook (1080x1920)',
    value: {
      width: 1080,
      height: 1920,
    },
  },
  {
    title: 'Apple iPad Pro (9,7inch) (2048x1536)',
    value: {
      width: 2048,
      height: 1536,
    },
  },
  {
    title: 'Samsung Galaxy Tab S2 (2048x1536)',
    value: {
      width: 2048,
      height: 1536,
    },
  },
  {
    title: 'Samsung Galaxy Tab A (1920x1200)',
    value: {
      width: 1920,
      height: 1200,
    },
  },
];
