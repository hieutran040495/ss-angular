export const getTimeRange = () => {
  const timeRange = [];
  for (let i = 0; i < 24; i++) {
    let hour = `${i}`;
    if (i < 10) {
      hour = `0${i}`;
    }

    for (let j = 0; j < 60; j += 30) {
      if (j === 0) {
        timeRange.push({ time: `${hour}:0${j}` });
        continue;
      }
      timeRange.push({ time: `${hour}:${j}` });
    }
  }

  return timeRange;
};
