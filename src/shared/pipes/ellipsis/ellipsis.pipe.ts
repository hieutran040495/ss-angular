import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'ellipsisStr' })
export class EllipsisStr implements PipeTransform {
  transform(value: string, strMax: number): string {
    const letters: Partial<StringOutput> = value.toEllipsisWithCountLetters(strMax);
    let newStr: string = letters.string;

    if (letters.length > strMax) {
      newStr = `${letters.string}...`;
    }

    return newStr;
  }
}
