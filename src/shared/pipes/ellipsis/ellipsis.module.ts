import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EllipsisStr } from './ellipsis.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [EllipsisStr],
  exports: [EllipsisStr],
})
export class EllipsisModule {}
