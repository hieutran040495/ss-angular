import { Pipe, PipeTransform } from '@angular/core';
import * as orderBy from 'lodash/orderBy';

@Pipe({
  name: 'orderBy',
})
export class OrderByPipe implements PipeTransform {
  transform(value: any, prototypes: string, orderType: string): any {
    return orderBy(value, prototypes, orderType);
  }
}
