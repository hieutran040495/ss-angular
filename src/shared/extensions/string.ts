interface StringOutput {
  length: number;
  string: string;
}

interface String {
  convertNumber(): number;
  toEllipsisWithCountLetters(count?: number): Partial<StringOutput>;
  truncateString(max: number): string;
}

String.prototype.truncateString = function(max: number): string {
  const add = '...';
  return this.length > max ? this.substring(0, max - 1) + add : this;
};

String.prototype.convertNumber = function(): number {
  return Number(this.replace(/\,/gi, ''));
};

String.prototype.toEllipsisWithCountLetters = function(count?: number): Partial<StringOutput> {
  let len: number = 0;
  let str: string = '';

  if (this.length !== 0) {
    for (let i: number = 0; i < this.length; i++) {
      const code = this.charCodeAt(i);

      if ((code >= 0x0020 && code <= 0x1fff) || (code >= 0xff61 && code <= 0xff9f)) {
        len += 1;
      } else if ((code >= 0x2000 && code <= 0xff60) || code >= 0xffa0) {
        len += 2;
      } else {
        len += 0;
      }

      if (count && len <= count) {
        str = str.concat(this[i]);
      }
    }

    if (count && len > count) {
      str = `${str}...`;
    }
  }

  return { length: len, string: str };
};
