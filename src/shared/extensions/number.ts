interface Number {
  format(): string;
}

Number.prototype.format = function(): string {
  return this.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
};
