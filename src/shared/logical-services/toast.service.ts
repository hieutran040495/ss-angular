import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  constructor(private _toastrService: ToastrService) {}

  success(message: string, title?: string) {
    return this._toastrService.success(this._renderText(message), title);
  }

  error(message: any, title?: string) {
    let msg: string;

    switch (message.constructor) {
      case String:
        msg = message;
        break;
      case Object:
        if (message.hasOwnProperty('message')) {
          msg = message.message;
          break;
        } else {
          for (const key of Object.keys(message)) {
            msg = message[key][0];
            break;
          }
        }
        break;
      case Array:
        if (message.length > 0) {
          msg = message[0];
        }
        break;
    }

    if (msg) {
      this._toastrService.error(this._renderText(msg), title);
    }
  }

  info(message: string, title?: string) {
    return this._toastrService.info(this._renderText(message), title);
  }

  warning(message: string, title?: string) {
    return this._toastrService.warning(this._renderText(message), title);
  }

  clear() {
    this._toastrService.clear();
  }

  private _renderText(s: string): string {
    return s.replace('\n', '<br>');
  }
}
