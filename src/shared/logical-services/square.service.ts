import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';

export interface SquareInput {
  callbackUrl: string;
  clientId: string;
  iosTenderTypes: string[];
  androidTenderTypes: string[];
  currentCode?: string;
}

@Injectable()
export class SquareService {
  private _config: SquareInput = {
    callbackUrl: window.location.origin + window.location.pathname,
    clientId: environment.square_client_id,
    iosTenderTypes: ['CREDIT_CARD', 'CASH'],
    androidTenderTypes: ['com.squareup.pos.TENDER_CARD', 'com.squareup.pos.TENDER_CASH'],
    currentCode: 'JPY',
  };

  config(config?: SquareInput) {
    this._config = { ...this.config, ...config };
  }

  /**
   * Open SquareUp POS for iOS
   * @link https://developer.squareup.com/docs/pos-api/web-technical-reference#mobile-web-on-ios
   */
  openIosPOS(amount: number, metadata: object, reservationId: number) {
    const { currentCode, callbackUrl, clientId, iosTenderTypes } = this._config;

    const data: object = {
      amount_money: {
        amount: amount,
        currency_code: currentCode,
      },

      // Replace this value with your application's callback URL
      callback_url: callbackUrl,

      // Replace this value with your application's ID
      client_id: clientId,
      version: '1.3',
      notes: JSON.stringify({ reservation_id: reservationId }),
      options: {
        supported_tender_types: iosTenderTypes,
        auto_return: true,
        skip_receipt: true,
      },

      state: JSON.stringify(metadata),
    };

    // @ts-ignore
    window.location = 'square-commerce-v1://payment/create?data=' + encodeURIComponent(JSON.stringify(data));
  }

  /**
   * Open SquareUp POS for Android
   * @link https://developer.squareup.com/docs/pos-api/web-technical-reference#mobile-web-on-android
   */
  openAndroidPOS(amount: number, metadata: object, reservationId: number) {
    const { callbackUrl, clientId, currentCode, androidTenderTypes } = this._config;

    const data: object = {
      action: 'com.squareup.pos.action.CHARGE',
      package: 'com.squareup',
      'S.browser_fallback_url': callbackUrl,
      'S.com.squareup.pos.NOTE': JSON.stringify({ reservation_id: reservationId }),
      'S.com.squareup.pos.REQUEST_METADATA': JSON.stringify(metadata),
      'S.com.squareup.pos.WEB_CALLBACK_URI': callbackUrl,
      'S.com.squareup.pos.CLIENT_ID': clientId,
      'S.com.squareup.pos.API_VERSION': 'v2.0',
      'l.com.squareup.pos.AUTO_RETURN_TIMEOUT_MS': '3200',
      'S.com.squareup.pos.CURRENCY_CODE': currentCode,
      'i.com.squareup.pos.TOTAL_AMOUNT': amount,
      'S.com.squareup.pos.TENDER_TYPES': androidTenderTypes,
    };

    // Make the params as 'action=com.squareup.pos.action.CHARGE;package=com.squareup;.....'
    const params = Object.keys(data)
      .map((k) => `${k}=${data[k]}`)
      .join(';');
    window.open(`intent:#Intent;${params};end`);
  }
}
