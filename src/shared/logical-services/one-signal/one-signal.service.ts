import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SessionQuery } from 'shared/states/session';
import { environment } from 'environments/environment';

import { NOTIFY_CODE } from 'shared/enums/code-notify';

interface OneSignalReceive {
  code: string;
  reservation_id: number;
  reservation_start_at: string;
}

@Injectable({
  providedIn: 'root',
})
export class OneSignalService {
  get OneSignal() {
    return window['OneSignal'] || [];
  }

  constructor(private sessionQuery: SessionQuery, private router: Router) {}

  init() {
    this.OneSignal instanceof Function
      ? this._checkNotificationPermission()
      : this._addScript('https://cdn.onesignal.com/sdks/OneSignalSDK.js', (e: Event) => {
          console.log('OneSignal Script Loaded');
          this._initOneSignal();
        });
  }

  renderLink(data: OneSignalReceive): string {
    switch (data.code) {
      case NOTIFY_CODE.RESV_CREATE:
      case NOTIFY_CODE.RESV_UPDATE:
      case NOTIFY_CODE.RESV_CANCEL:
      case NOTIFY_CODE.BEFORE_RESV_START:
      case NOTIFY_CODE.BEFORE_RESV_END:
        return `/reservations`;
      case NOTIFY_CODE.INCOMING_CALL:
        return `/history-call`;
      default:
        break;
    }
  }

  renderQueryParams(data: OneSignalReceive) {
    switch (data.code) {
      case NOTIFY_CODE.RESV_CREATE:
      case NOTIFY_CODE.RESV_UPDATE:
      case NOTIFY_CODE.RESV_CANCEL:
      case NOTIFY_CODE.BEFORE_RESV_START:
      case NOTIFY_CODE.BEFORE_RESV_END:
        return {
          queryParams: {
            reservation_id: data.reservation_id,
            start_at: data.reservation_start_at,
          },
          skipLocationChange: true,
        };
      default:
        break;
    }
  }

  private _addScript(fileSrc: string, callback) {
    const head = document.getElementsByTagName('head')[0];
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.onload = callback;
    script.src = fileSrc;
    head.appendChild(script);
  }

  private _initOneSignal() {
    console.log('Init OneSignal');
    this.OneSignal.push([
      'init',
      {
        appId: environment.onesignal_app_id,
        notifyButton: {
          enable: true,
        },
        allowLocalhostAsSecureOrigin: false,
      },
    ]);

    this.OneSignal.push(() => {
      this.OneSignal.on('notificationPermissionChange', (permissionChange) => {
        const currentPermission = permissionChange.to;

        switch (currentPermission) {
          case 'granted':
            this._checkIfSubscribed();
            break;
          case 'denied':
            this.destroyOneSignal();
            break;
        }
      });
    });

    this.onNotificationOpened();

    console.log('OneSignal Initialized');
    this._checkNotificationPermission();
  }

  private onNotificationOpened() {
    this.OneSignal.push([
      'addListenerForNotificationOpened',
      (event) => {
        this.router.navigate([this.renderLink(event.data)], { ...this.renderQueryParams(event.data) });

        this.onNotificationOpened();
      },
    ]);
  }

  private async _sendTag() {
    const user = await this.sessionQuery.getUserLoggined();

    if (user && user.id) {
      this.OneSignal.sendTag('channel', `client_member.${user.id}`, (tagsSent) => {
        // Callback called when tags have finished sending
        console.log('OneSignal Tag Sent', tagsSent);
      });
    }
  }

  private _subscribe() {
    this.OneSignal.push(() => {
      console.log('Register For Push');
      this.OneSignal.push(['registerForPushNotifications']);
      this._sendTag();

      this.OneSignal.on('subscriptionChange', (isSubscribed) => {
        console.log("The user's subscription state is now:", isSubscribed);
        if (isSubscribed) {
          this._listenForNotification();
          this._getUserID();
        }
      });
    });
  }

  private _listenForNotification() {
    console.log('Initalize Listener');

    this.OneSignal.on('notificationDisplay', (event) => {
      console.log('OneSignal notification displayed:', event);
      this._listenForNotification();
    });
  }

  private _getUserID() {
    this.OneSignal.getUserId().then((userId) => {
      console.log('User ID is', userId);
    });
  }

  private _checkNotificationPermission() {
    this.OneSignal.push(() => {
      const isPushSupported = this.OneSignal.isPushNotificationsSupported();

      if (!isPushSupported) {
        console.log('Push notifications are not supported');
        return;
      }

      this.OneSignal.getNotificationPermission()
        .then((permission) => {
          console.log('Site Notification Permission:', permission);

          if (permission !== 'granted') {
            return;
          }

          this._checkIfSubscribed();
        })
        .catch(console.log);
    });
  }

  private _checkIfSubscribed() {
    this.OneSignal.push(() => {
      this.OneSignal.isPushNotificationsEnabled(
        (isEnabled) => {
          if (isEnabled) {
            console.log('Push notifications are enabled!');

            this._sendTag();
            this._getUserID();
            return;
          }

          console.log('Push notifications are not enabled yet.');

          this.OneSignal.showHttpPrompt();
          this._subscribe();
        },
        (error) => {
          console.log('Push permission not granted');
        },
      );
    });
  }

  destroyOneSignal() {
    this.OneSignal.push(() => {
      this.OneSignal.deleteTags(['channel']).then((tagsDeleted) => {
        console.log('tagDelete', tagsDeleted);
      });
    });
  }
}
