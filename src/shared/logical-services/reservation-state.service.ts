import { Reservation } from 'shared/models/reservation';
import { Injectable } from '@angular/core';
import { LOCALSTORAGE_KEY } from 'shared/enums/local-storage';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class ReservationStateService {
  constructor(private router: Router) {}

  updateReservevation(reservation: Reservation) {
    localStorage.setItem(LOCALSTORAGE_KEY.RESV_STORE, JSON.stringify(reservation));
  }

  resetResvationStore() {
    localStorage.removeItem(LOCALSTORAGE_KEY.RESV_STORE);
  }

  fetchLocalStoregeReservation(redirect?: string) {
    const data = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY.RESV_STORE));
    if (data) {
      return new Reservation().deserialize(data);
    }
    if (redirect) {
      this.router.navigate([redirect]);
    }
  }
}
