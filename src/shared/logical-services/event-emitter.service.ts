import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

export interface DataEmitter {
  type: string;
  data?: any;
  close?: boolean;
}
@Injectable()
export class EventEmitterService {
  // Observable string sources
  private caseNumber = new Subject<DataEmitter>();

  // Observable string streams
  caseNumber$ = this.caseNumber;

  // Service message commands
  publishData(data: DataEmitter) {
    this.caseNumber.next(data);
  }
}
