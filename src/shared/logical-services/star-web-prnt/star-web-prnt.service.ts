import { Injectable } from '@angular/core';
import { WebPRNTHelpers } from './WebPRNTHelpers';

import * as _padEnd from 'lodash/padEnd';

@Injectable()
export class StarWebPRNTService {
  builder: any;
  trader: any;
  request: string = '';

  constructor() {
    this._addScript('assets/starWebPrint/StarWebPrintBuilder.js', () => {
      // @ts-ignore
      this.builder = new StarWebPrintBuilder();
    });

    this._addScript('assets/starWebPrint/StarWebPrintTrader.js', () => {
      const url = '//localhost:8001/StarWebPRNT/SendMessage';
      // @ts-ignore
      this.trader = new StarWebPrintTrader({ url: url });
    });
  }

  async createRequestImage(image) {
    const canvas = document.createElement('canvas');

    canvas.width = 384;
    canvas.height = 100;
    const context = canvas.getContext('2d');

    const imageObj = new Image(),
      destWidth = 300,
      destHeight = 100;

    const imagePromise = WebPRNTHelpers.onload2promise(imageObj);

    imageObj.src = image;

    await imagePromise;

    context.drawImage(
      imageObj,
      0,
      0,
      imageObj.width,
      imageObj.height,
      canvas.width / 2 - destWidth / 2,
      canvas.height / 2 - destHeight / 2,
      destWidth,
      destHeight,
    );

    return this.builder.createBitImageElement({
      context: canvas.getContext('2d'),
      x: 0,
      y: 0,
      width: canvas.width,
      height: canvas.height,
    });
  }

  async createRequestData(params, paperWidth?: number) {
    // paper width 2 inch
    let maxLength: number = 32;
    let options: any = { characterspace: 0 };

    if (paperWidth === 3) {
      // paper width 3 inch
      maxLength = 48;
      options = { characterspace: 2 };
    } else if (paperWidth === 4) {
      // paper width 4 inch
      maxLength = 64;
      options = { characterspace: 1 };
    }

    this.request += this.builder.createTextElement({ ...options, international: 'japan' });
    this.request += this.builder.createTextElement({ codepage: 'shift_jis' });
    this.request += this.builder.createAlignmentElement({ position: 'center' });

    if (params.logo) {
      this.request += await this.createRequestImage(params.logo);
    }

    this.request += this.builder.createTextElement({ emphasis: true });

    if (params.address) {
      this.request += this.builder.createTextElement({ data: params.address + '\n' });
    }

    if (params.phone) {
      this.request += this.builder.createTextElement({ data: 'TEL ' + params.phone + '\n' });
    }

    if (params.start_at) {
      this.request += this.builder.createTextElement({ data: params.start_at + '\n' });
    }

    this.request += this.builder.createTextElement({
      data: `予約No.${params.reservationNumber}  ${params.tablesNumber}\n`,
    });

    this.request += this.builder.createTextElement({ emphasis: false, data: '\n\n' });
    this.request += this.builder.createAlignmentElement({ position: 'left' });

    params.items.forEach((item) => {
      this.request += this.builder.createTextElement({ data: item.name + '\n' });
      const is8Mark: boolean = +item.taxRatio === 8 ? true : false;
      this.request += this.builder.createTextElement({
        data:
          WebPRNTHelpers.createRowDataThreeCol(item.price, item.quantity, item.total_price, maxLength, is8Mark) + '\n',
      });
      if (item.menu_options && item.menu_options.length > 0) {
        item.menu_options.forEach((itemOption) => {
          this.request += this.builder.createTextElement({
            data: _padEnd('', maxLength / 16, ' ') + itemOption.name + '\n',
          });
          this.request += this.builder.createTextElement({
            data:
              WebPRNTHelpers.createRowDataThreeCol(
                itemOption.price,
                itemOption.quantity,
                itemOption.total_price,
                maxLength,
              ) + '\n',
          });
        });
      }
    });

    // this.request += this.builder.createTextElement({ data: '\n\n' });

    this.request += this.builder.createTextElement({ data: _padEnd('', maxLength, '-') + '\n' });
    this.request += this.builder.createTextElement({
      data: WebPRNTHelpers.createRowDataTwoCol('小計', params.subtotal_price, maxLength) + '\n',
    });

    if (params.taxTenPercentTarget) {
      this.request += this.builder.createTextElement({
        data: WebPRNTHelpers.createRowDataTwoCol('10％対象', params.taxTenPercentTarget, maxLength) + '\n',
      });
      this.request += this.builder.createTextElement({
        data: WebPRNTHelpers.createRowDataTwoCol('(内消費税等', params.taxTenPercentPrice, maxLength - 1) + ')\n',
      });
    }

    if (params.taxEightPercentTarget) {
      this.request += this.builder.createTextElement({
        data: WebPRNTHelpers.createRowDataTwoCol('8％対象', params.taxEightPercentTarget, maxLength) + '\n',
      });
      this.request += this.builder.createTextElement({
        data: WebPRNTHelpers.createRowDataTwoCol('(内消費税等', params.taxEightPercentPrice, maxLength - 1) + ')\n',
      });
    }

    if (params.discount_fee) {
      this.request += this.builder.createTextElement({
        data: WebPRNTHelpers.createRowDataTwoCol('割引', params.discount_fee * -1, maxLength) + '\n',
      });
    }

    this.request += this.builder.createTextElement({ data: _padEnd('', maxLength, '-') + '\n' });

    const totalPriceData = WebPRNTHelpers.getRowData('合計', params.total_price, maxLength, 2);
    this.request += this.builder.createTextElement({ data: totalPriceData.label });
    this.request += this.builder.createTextElement({
      emphasis: true,
      data: totalPriceData.value + '\n',
      width: 2,
      height: 2,
    });

    this.request += this.builder.createTextElement({
      emphasis: false,
      data: WebPRNTHelpers.createRowDataTwoCol('(内消費税等', params.amountTax, maxLength - 1) + ')\n',
      width: 1,
      height: 1,
    });

    const totalUserPaymentData = WebPRNTHelpers.getRowData('現金', params.userPayment, maxLength, 1);
    this.request += this.builder.createTextElement({ data: totalUserPaymentData.label });
    this.request += this.builder.createTextElement({
      emphasis: true,
      data: totalUserPaymentData.value + '\n',
    });

    if (!params.card_number) {
      if (params.displayChange) {
        const totalDisplayChangeData = WebPRNTHelpers.getRowData('お釣り', params.displayChange, maxLength + 1, 2);
        this.request += this.builder.createTextElement({ data: totalDisplayChangeData.label });
        this.request += this.builder.createTextElement({
          emphasis: true,
          data: totalDisplayChangeData.value,
          width: 2,
          height: 2,
        });
      }
    } else {
      this.request += this.builder.createTextElement({
        data: `****-****-****-${params.card_number} ${params.card_brand.toUpperCase()}`,
      });
    }

    this.request += this.builder.createTextElement({ emphasis: false, data: '\n\n', width: 1, height: 1 });

    this.request += this.builder.createTextElement({ data: `[※]印は軽減税率対象品目です\n` });
    this.request += this.builder.createTextElement({ data: `NO.${params.receiptNumber}` });

    this.request += this.builder.createCutPaperElement({ feed: true, type: 'partial' });

    return this.request;
  }

  async onSendMessageApi(params: any, cbSuccess, cbError) {
    this.request = '';
    this.request += this.builder.createInitializationElement();

    this.request += await this.createRequestData(params);

    this.sendMessage(this.request, cbSuccess, cbError);
  }

  sendMessage(request, successCb?: Function, errorCb?: Function) {
    this.trader.onReceive = successCb;

    this.trader.onError = errorCb;

    this.trader.sendMessage({ request: request });
  }

  private _addScript(fileSrc: string, callback) {
    const head = document.getElementsByTagName('head')[0];
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.onload = callback;
    script.src = fileSrc;
    head.appendChild(script);
  }
}
