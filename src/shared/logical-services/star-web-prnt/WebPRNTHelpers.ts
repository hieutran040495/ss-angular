import * as _padEnd from 'lodash/padEnd';
import * as _padStart from 'lodash/padStart';

export class WebPRNTHelpers {
  static onload2promise(obj) {
    return new Promise((resolve, reject) => {
      obj.onload = () => resolve(obj);
      obj.onerror = reject;
    });
  }

  static calcStrLength(str) {
    const re = new RegExp('([ぁ-んァ-ン一-龥]+)', 'g');
    const match = str.match(re);
    let fullSizeLength = 0;
    match.forEach((item) => {
      fullSizeLength += item.length;
    });

    return str.length + fullSizeLength;
  }

  static createRowDataTwoCol(label, value, length) {
    let result = _padEnd(label, label.length, ' ');
    let strLengthLeft = length - label.toEllipsisWithCountLetters().length;
    if (value < 0) {
      strLengthLeft = strLengthLeft - 1;
      value *= -1;
      result += _padStart('-\\' + value.format(), strLengthLeft, ' ');
    } else {
      result += _padStart('\\' + value.format(), strLengthLeft, ' ');
    }

    return result;
  }

  static createRowDataThreeCol(price, quantity, total, length, is8Mark: boolean = false) {
    let paddingLength = length / 8;
    if (paddingLength % 2 !== 0) {
      paddingLength = paddingLength + 1;
    }
    const quantityLength = 3 + paddingLength / 2;
    const newLength = (is8Mark ? length - 2 : length) - paddingLength - quantityLength;

    let result = _padEnd('', paddingLength, ' '); // length / 16 characters
    if (price < 0) {
      result += _padEnd('-\\' + (-price).format(), newLength / 2, ' ');
    } else {
      result += _padEnd('\\' + price.format(), newLength / 2, ' ');
    }

    result += _padStart(quantity, is8Mark ? quantityLength + 1 : quantityLength, ' ');

    const totalTmp = total < 0 ? -total : total;
    const totalSrt = is8Mark ? `${totalTmp.format()}※` : `${totalTmp.format()}`;
    if (total < 0) {
      result += _padStart('-\\' + totalSrt, newLength / 2, ' ');
    } else {
      result += _padStart('\\' + totalSrt, newLength / 2, ' ');
    }

    return result;
  }

  static getRowData(label, value, length, valueWidth = 1) {
    const valueFormat = value.format();
    let strLengthLeft = length - this.calcStrLength(label) - valueFormat.length + 1;

    if (valueWidth > 1) {
      strLengthLeft = length - this.calcStrLength(label) - valueFormat.length * valueWidth;
    }
    const leftStr = _padEnd(label, strLengthLeft, ' ');
    const rightStr = _padStart('\\' + value.format(), valueFormat.length, ' ');

    return {
      label: leftStr,
      value: rightStr,
    };
  }
}
