import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { Client } from 'shared/models/client';

export interface ClientProfileState {
  info: Client;
}

export function createInitialState(): ClientProfileState {
  return {
    info: null,
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'client-profile' })
export class ClientProfileStore extends Store<ClientProfileState> {
  constructor() {
    super(createInitialState());
  }
}
