import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { ClientProfileStore, ClientProfileState } from './client-profile.store';
import { Client } from 'shared/models/client';
import { ClientProfileService } from './client-profile.service';

@Injectable({ providedIn: 'root' })
export class ClientProfileQuery extends Query<ClientProfileState> {
  clientProfile$ = this.selectOnce((store) => {
    return store.info;
  });

  constructor(protected store: ClientProfileStore, private clientProfileService: ClientProfileService) {
    super(store);
  }

  public getClientProfile(): Promise<Client> {
    const data = this.clientProfile$.toPromise();
    if (!data) {
      return this.clientProfileService.getClientInfor();
    }

    return data;
  }
}
