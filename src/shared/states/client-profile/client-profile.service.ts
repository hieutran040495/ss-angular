import { Injectable } from '@angular/core';
import { ClientProfileStore, createInitialState } from './client-profile.store';
import { Client } from 'shared/models/client';
import { ClientService } from 'shared/http-services/client.service';

@Injectable({
  providedIn: 'root',
})
export class ClientProfileService {
  constructor(private clientProfileStore: ClientProfileStore, private clientSv: ClientService) {}

  resetStore() {
    this.clientProfileStore.update(createInitialState());
  }

  updateClientProfile(clientProfile: Client): void {
    this.clientProfileStore.update((store) => {
      return { ...store, info: clientProfile };
    });
  }

  async getClientInfor(): Promise<Client> {
    try {
      const opt = {
        with: 'clientSetting',
      };

      const res = await this.clientSv.fetchClient(opt).toPromise();

      if (!res) {
        return;
      }

      this.updateClientProfile(res);

      return res;
    } catch (error) {}
  }
}
