import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { ClientWorkingTimeWeek } from 'shared/models/client-working-week';

export interface ClientWorkingsState {
  client_workings: ClientWorkingTimeWeek;
}

export function createInitialState(): ClientWorkingsState {
  return {
    client_workings: null,
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'client_workings' })
export class ClientWorkingsStore extends Store<ClientWorkingsState> {
  constructor() {
    super(createInitialState());
  }
}
