import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { ClientWorkingsStore, ClientWorkingsState } from './client-workings.store';
import { ClientWorkingsService } from './client-workings.service';
import { ClientWorkingTimeWeek } from 'shared/models/client-working-week';

@Injectable({ providedIn: 'root' })
export class ClientWorkingsQuery extends Query<ClientWorkingsState> {
  workingTime$ = this.selectOnce((store) => {
    return store.client_workings;
  });

  constructor(protected store: ClientWorkingsStore, private clientWorkingsService: ClientWorkingsService) {
    super(store);
  }

  public async getWorkingTime(): Promise<ClientWorkingTimeWeek> {
    const data = await this.workingTime$.toPromise();

    if (!data) {
      return await this.clientWorkingsService.getClientWorking();
    }

    return data;
  }
}
