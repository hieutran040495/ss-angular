import { Injectable } from '@angular/core';
import { ClientWorkingsStore, createInitialState } from './client-workings.store';
import { ClientProfileService } from 'shared/states/client-profile';
import { ClientService } from 'shared/http-services/client.service';
import { ClientWorkingTimeWeek } from 'shared/models/client-working-week';

@Injectable({ providedIn: 'root' })
export class ClientWorkingsService {
  constructor(
    private clientWorkingsStore: ClientWorkingsStore,
    private clientSv: ClientService,
    private clientProfileSv: ClientProfileService,
  ) {}

  async getClientWorking(): Promise<ClientWorkingTimeWeek> {
    const opts = {
      with: 'clientWorkings,clientAdmin,clientSetting',
    };
    try {
      const res: any = await this.clientSv.fetchClient(opts).toPromise();

      if (!res.client_workings) {
        return;
      }

      this.updateWorkingTime(res.client_workings);
      this.clientProfileSv.updateClientProfile(res);

      return res.client_workings;
    } catch (error) {
      console.error(error);
    }
  }

  resetStore() {
    this.clientWorkingsStore.update(createInitialState());
  }

  updateWorkingTime(clientWorkingTime: ClientWorkingTimeWeek): void {
    this.clientWorkingsStore.update((store: any) => {
      return { ...store, client_workings: clientWorkingTime };
    });
  }
}
