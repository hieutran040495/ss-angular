import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { SessionStore, SessionState } from './session.store';
import { JwtHelper } from 'shared/utils/jwt.helper';
import { User } from 'shared/models/user';
import { SessionService } from './session.service';

@Injectable({ providedIn: 'root' })
export class SessionQuery extends Query<SessionState> {
  isLoggedIn$ = this.selectOnce((session) => {
    return !!session.token && !JwtHelper.isTokenExpired(session.token);
  });
  token$ = this.selectOnce((session) => {
    return session.token;
  });

  userLoggined$ = this.selectOnce((session) => {
    return session.userLoggined;
  });

  constructor(protected store: SessionStore, private sessionService: SessionService) {
    super(store);
  }

  getToken(): Promise<string> {
    return this.token$.toPromise();
  }

  async getUserLoggined(): Promise<User> {
    const data = await this.userLoggined$.toPromise();

    if (!data) {
      return this.sessionService.getUserLoggined();
    }

    return data;
  }
}
