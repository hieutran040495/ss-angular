import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { User } from 'shared/models/user';

export interface SessionState {
  token: string;
  userLoggined: User;
}

export function createInitialState(): SessionState {
  return {
    token: '',
    userLoggined: null,
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'session' })
export class SessionStore extends Store<SessionState> {
  constructor() {
    super(createInitialState());
  }
}
