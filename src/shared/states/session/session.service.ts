import { Injectable } from '@angular/core';
import { SessionStore, createInitialState } from './session.store';
import { LOCALSTORAGE_KEY } from 'shared/enums/local-storage';
import { Router } from '@angular/router';
import { User } from 'shared/models/user';
import { UserService } from 'shared/http-services/user.service';
@Injectable({ providedIn: 'root' })
export class SessionService {
  constructor(private router: Router, private _userSv: UserService, private sessionStore: SessionStore) {}

  updateToken(token: string) {
    this.sessionStore.update((session) => {
      localStorage.setItem(LOCALSTORAGE_KEY.APP_NAME, JSON.stringify({ token }));
      return { ...session, token };
    });
  }

  updateUserLoggined(user: User) {
    this.sessionStore.update((session) => {
      return { ...session, userLoggined: user };
    });
  }

  resetStore() {
    this.sessionStore.update(createInitialState());
  }

  async getUserLoggined(): Promise<User> {
    try {
      if (!localStorage.getItem(LOCALSTORAGE_KEY.APP_NAME)) {
        return;
      }

      const res = await this._userSv.getCurUser().toPromise();

      if (!res) {
        return;
      }

      this.updateUserLoggined(res);

      return res;
    } catch (error) {
      console.log(error);
    }
  }

  fetchLocalStorageSession(redirect?: string) {
    try {
      const data = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY.APP_NAME));

      if (data.hasOwnProperty('token')) {
        this.updateToken(data.token);

        if (redirect) {
          this.router.navigate([redirect]);
        }
      }
    } catch (e) {}
  }
}
