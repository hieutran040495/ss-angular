import { Deserializable } from 'shared/interfaces/deserializable';

export interface PaginationInput {
  current_page: number;
  last_page: number;
  per_page: number;
  total: number;
  max_size?: number;
}

export interface PaginationOutput {
  page: number;
  per_page: number;
}

export interface PageChangedInput {
  page: number;
  itemsPerPage: number;
}

export class Pagination implements Deserializable<Pagination>, PaginationInput {
  current_page: number;
  last_page: number;
  per_page: number;
  total: number;
  get isPagination(): boolean {
    return this.total > this.per_page;
  }
  max_size: number;

  constructor() {
    this.deserialize({
      current_page: 1,
      last_page: 1,
      per_page: 20,
      total: 1,
      max_size: 5,
    });
  }

  deserialize(input: Partial<PaginationInput>): Pagination {
    Object.assign(this, input);
    return this;
  }

  pageChanged(page: number): void {
    this.current_page = page;
  }

  hasJSON(): PaginationOutput {
    return {
      page: this.current_page,
      per_page: this.per_page,
    };
  }

  reset(): void {
    this.current_page = 1;
  }

  nextPage(): void {
    this.current_page = this.current_page + 1;
  }

  hasNextPage(): boolean {
    return this.current_page < this.last_page;
  }

  isFirstPage(): boolean {
    return this.current_page === 1;
  }
}
