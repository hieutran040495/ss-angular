import { Deserializable } from 'shared/interfaces/deserializable';
import { MenuTypeInput, MenuType } from './menu-type';

export interface CategoryInput {
  id: number;
  name: string;
  items_count?: number;
  item_type: MenuTypeInput;
  ordinal_number: number;
  swapClassActive?: string;
}

export interface CategoryOutput {
  id?: number;
  name: string;
  type_id: number;
  ordinal_number: number;
}

export class Category implements Deserializable<Category>, CategoryInput {
  id: number;
  name: string;
  items_count: number;
  item_type: MenuType;
  ordinal_number: number;
  swapClassActive?: string;

  get isCategory(): boolean {
    return this.id > 0;
  }

  get isCourse(): boolean {
    return this.id === -3;
  }

  get isBuffet(): boolean {
    return this.id === -2;
  }

  deserialize(input: Partial<CategoryInput>): Category {
    if (input) {
      Object.assign(this, input);
      if (input.item_type) {
        this.item_type = new MenuType().deserialize(input.item_type);
      }
      return this;
    }
  }

  formDataString(): CategoryOutput {
    const data: CategoryOutput = {
      name: this.name,
      type_id: this.item_type.id,
      ordinal_number: this.ordinal_number,
    };

    if (this.id) {
      data.id = this.id;
    }

    return data;
  }
}
