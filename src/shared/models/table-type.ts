import { Deserializable } from 'shared/interfaces/deserializable';
import { ClientInput, Client } from './client';

export enum TABLETYPE {
  RELOAD = 'reload',
}

export interface TableTypeInput {
  id: number;
  client?: ClientInput;
  name: string;
  typeable: string;
  created_at?: string;
  updated_at?: string;
}

export interface TableTypeOutput {
  id?: number;
  name: string;
}

export class TableType implements Deserializable<TableType>, TableTypeInput {
  id: number;
  client: Client;
  name: string;
  typeable: string;
  created_at: string;
  updated_at: string;

  constructor() {
    this.client = new Client();
  }

  deserialize(input: Partial<TableTypeInput>): TableType {
    if (input) {
      Object.assign(this, input);

      if (input.client) {
        this.client = new Client().deserialize(input.client);
      }
    }
    return this;
  }

  formData(): TableTypeOutput {
    const result: TableTypeOutput = {
      name: this.name,
    };

    if (this.id) {
      result.id = this.id;
    }

    return result;
  }
}
