import { Deserializable } from 'shared/interfaces/deserializable';
import { TagInput, Tag } from './tag';
import { CancelPolicy, CancelPolicyInput } from './cancel-policy';
import { ImageInput, Image } from './image';
import { GenreInput, Genre } from './genre';
import { Prefecture } from './prefecture';
import { City } from './city';
import { Record } from './send-sales-record';
import { NotifySetting } from './notify-setting';
import { NotifyConfig } from './notify-config';
import { SERVICE_FEE_TYPE, SERVICE_FEE_TYPE_UNIT, SERVICE_FEE_BY } from 'shared/enums/service-fee-setting';
import { User } from './user';
import { ClientWorkingTimeWeek } from './client-working-week';
import { WEEK_DAY } from 'shared/constants/client-working';
import { ClientWorkingTime } from './client-working-time';
import { ClientLogo, ClientLogoInput } from './client-logo';
import { Device, DeviceInput } from './device';

import * as moment from 'moment';
import { ClientTwilio, ClientTwilioInput } from './client-twilio';
import { ClientLanguage } from './client-language';
import { ClientSetting, ClientSettingInput } from './client-setting';

export interface ClientInput {
  id: number;
  title?: string;
  description?: string;
  transportation?: string;
  payment_method?: string;
  name: string;
  kana_name: string;
  pref: Prefecture;
  pref_code: string;
  pref_name: string;
  city: City;
  city_code: string;
  city_name: string;
  address: string;
  time_open: string;
  time_close: string;
  website_url: string;
  phone: string;
  image_path: string;
  cost_estimate: number;
  secret_code: string;
  min_reservation_duration: number;
  cleaning_duration: number;
  tags?: TagInput[];
  images?: ImageInput[];
  genre?: GenreInput;
  notify_settings?: Partial<NotifySetting>;
  cancel_policy: CancelPolicyInput;
  service_fee: number;
  service_fee_type: string;
  use_handy: boolean;
  use_self_order: boolean;
  use_client_pos: boolean;
  allow_smp_order: boolean;
  self_order_direction: string;
  use_service_fee: boolean;
  service_fee_by: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
  working_note: string;
  client_workings: ClientWorkingTimeWeek;
  client_admin: User;
  devices: Device[];
  logo: ClientLogoInput;
  sold_out_image?: ImageInput;
  twilio: ClientTwilioInput;
  cms_url?: string;
  gourmet_accounts_count: number;
  sale_email: Record;
  client_language: ClientLanguage;
  client_setting: ClientSettingInput;
}

export interface ClientOutput {
  name: string;
  kana_name: string;
  city_code: string;
  pref_code: string;
  address: string;
  time_open: string;
  time_close: string;
  phone: string;
  website_url?: string;
  tag_ids: number[];
  genre_id: number;
  client_workings: any;
}

export interface ClientSubOutput {
  title?: string;
  description?: string;
  transportation?: string;
  payment_method?: string;
  min_reservation_duration: number;
  cleaning_duration: number;
  working_note: string;
  cost_estimate: string;
  cancel_policy: string;
}

export interface PrivateSettingOutput {
  secret_code: string;
}

export interface SettingCancelPolicyOutput {
  cancel_policy: string;
}

export interface SettingServiceFeeOutput {
  service_fee: number;
  service_fee_type: string;
  use_service_fee: boolean;
  service_fee_by: string;
}

export class Client implements Deserializable<Client>, ClientInput {
  id: number;
  title: string;
  description: string;
  transportation: string;
  payment_method: string;
  name: string;
  kana_name: string;
  pref: Prefecture;
  pref_code: string;
  pref_name: string;
  city: City;
  sale_email: Record;
  city_code: string;
  city_name: string;
  layout: string;
  twilio: ClientTwilio;
  gourmet_accounts_count: number;

  private _time_open: string;
  get time_open(): string {
    return this._time_open || '00:00';
  }
  set time_open(v: string) {
    this._time_open = v;
  }

  private _time_close: string;
  get time_close(): string {
    return this._time_close || '23:59';
  }
  set time_close(v: string) {
    this._time_close = v;
  }

  get codeAddress(): string {
    return `〒${this.pref.prefCode}-${this.city.cityCode}`;
  }
  address: string;
  get fullAddress(): string {
    return `${this.codeAddress} ${this.address}`;
  }

  website_url: string;
  cms_url?: string;
  phone: string;
  image_path: string;
  images: Image[];
  get isMaxImages(): boolean {
    return this.images.length === 5;
  }
  cost_estimate: number;
  secret_code: string;
  min_reservation_duration: number;
  cleaning_duration: number;
  tags: Tag[];
  genre: Genre;
  notify_settings: NotifySetting;
  cancel_policy: CancelPolicy;
  service_fee: number;
  get service_fee_display(): string {
    return this.service_fee ? this.service_fee.format() : null;
  }
  service_fee_type: string;
  get service_fee_type_unit(): string {
    return SERVICE_FEE_TYPE_UNIT[this.service_fee_type];
  }
  get isPercent(): boolean {
    return this.service_fee_type === SERVICE_FEE_TYPE.PERCENT;
  }
  get isAmount(): boolean {
    return this.service_fee_type === SERVICE_FEE_TYPE.AMOUNT;
  }
  use_service_fee: boolean;
  service_fee_by: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
  working_note: string;
  client_workings: ClientWorkingTimeWeek;
  client_admin: User;
  devices: Device[];

  use_handy: boolean;
  use_self_order: boolean;
  use_client_pos: boolean;
  allow_smp_order: boolean;
  self_order_direction: string;
  get appUseStr(): string {
    const str: string[] = [];
    if (this.use_self_order) {
      str.push('KIOSK');
    }
    if (this.use_handy) {
      str.push('ハンディ');
    }
    if (this.use_client_pos) {
      str.push('SquarePOSレジ連携');
    }
    if (this.allow_smp_order) {
      str.push('スマホオーダー');
    }
    return str.join('、');
  }

  logo: ClientLogo;
  sold_out_image: Image;
  client_language: ClientLanguage;
  client_setting: ClientSetting;

  constructor() {
    this.deserialize({
      genre: new Genre(),
      tags: [],
      images: [],
      client_admin: new User(),
      logo: new ClientLogo(),
      use_service_fee: false,
      cancel_policy: new CancelPolicy(),
    });
    this.client_workings = new ClientWorkingTimeWeek();
    this.service_fee_by = SERVICE_FEE_BY.QUANTITY;
    this.client_language = new ClientLanguage();
    this.client_setting = new ClientSetting();
  }

  deserialize(input: Partial<ClientInput>): Client {
    if (input) {
      input.pref = new Prefecture().deserialize({
        prefCode: input.pref_code,
        prefName: input.pref_name,
      });
      input.city = new City().deserialize({
        cityCode: input.city_code,
        cityName: input.city_name,
      });

      Object.assign(this, input);

      if (input.client_admin) {
        this.client_admin = new User().deserialize(input.client_admin);
      }

      if (input.tags && input.tags.length !== 0) {
        this.addTags(input.tags);
      }

      if (input.images && input.images.length !== 0) {
        this.addImages(input.images);
      }
      if (input.genre) {
        this.genre = new Genre().deserialize(input.genre);
      }
      if (input.time_open) {
        const timeDuration = moment.duration(input.time_open);
        this.time_open = timeDuration.format('HH:mm');
      }
      if (input.time_close) {
        const timeDuration = moment.duration(input.time_close);
        this.time_close = timeDuration.format('HH:mm');
      }
      if (input.notify_settings) {
        this.notify_settings = new NotifySetting().deserialize(this.mapSettingNotify(input.notify_settings));
      }

      if (input.devices) {
        this.devices = input.devices.map((item: DeviceInput) => new Device().deserialize(item));
      }

      if (input.cancel_policy) {
        this.cancel_policy = new CancelPolicy().deserialize(input.cancel_policy);
      }

      this.logo = new ClientLogo().deserialize(input.logo);

      this.client_workings =
        input.client_workings instanceof ClientWorkingTimeWeek
          ? input.client_workings
          : new ClientWorkingTimeWeek().deserialize(input.client_workings);

      if (input.sold_out_image) {
        this.sold_out_image = new Image().deserialize(input.sold_out_image);
      }

      if (input.twilio) {
        this.twilio =
          input.twilio instanceof ClientTwilio ? input.twilio : new ClientTwilio().deserialize(input.twilio);
      }
      if (input.client_language) {
        this.client_language =
          input.client_language instanceof ClientLanguage
            ? input.client_language
            : new ClientLanguage().deserialize(input.client_language);
      }
    }
    return this;
  }

  addTags(tags: Partial<TagInput[]>) {
    this.tags = tags.map((tag: Tag) => new Tag().deserialize(tag));
  }

  addImages(images: Partial<ImageInput[]>) {
    this.images = images.map((image: Image) => new Image().deserialize(image));
  }

  formDataClient(): Partial<ClientOutput> {
    const data: Partial<ClientOutput> = {
      name: this.name,
      kana_name: this.kana_name,
      address: this.address,
      phone: this.phone,
      genre_id: this.genre.id,
      tag_ids: this.tags.map((tag: Tag) => tag.id),
      time_open: `${this.time_open}:00`,
      time_close: `${this.time_close}:00`,
      website_url: this.website_url,
      client_workings: WEEK_DAY.map((value, key) => {
        return this.client_workings[key].durations.map((duration: ClientWorkingTime) => duration.toJSON());
      }),
    };

    if (this.pref) {
      data.pref_code = this.pref.prefCode.toString();
    }

    if (this.city) {
      data.city_code = this.city.cityCode.toString();
    }

    return data;
  }

  formDataPrivateSetting(): Partial<PrivateSettingOutput> {
    const data: Partial<PrivateSettingOutput> = {
      secret_code: this.secret_code,
    };

    return data;
  }

  formDataSubClient(): Partial<ClientSubOutput> {
    const data: Partial<ClientSubOutput> = {
      title: this.title,
      description: this.description,
      transportation: this.transportation,
      payment_method: this.payment_method,
      min_reservation_duration: this.min_reservation_duration,
      cleaning_duration: +this.cleaning_duration,
      cost_estimate: this.cost_estimate.toString(),
      working_note: this.working_note,
      cancel_policy: this.cancel_policy.cancel_policy,
    };

    return data;
  }

  fromDateSettingFee(): Partial<SettingServiceFeeOutput> {
    return {
      service_fee: this.service_fee ? this.service_fee : null,
      service_fee_type: this.service_fee_type,
      use_service_fee: this.use_service_fee,
      service_fee_by: this.service_fee_by,
    };
  }

  private mapSettingNotify(data: any) {
    const obj = {};
    Object.values(data).map((value: NotifyConfig) => {
      obj[value.name] = value;
    });
    return obj;
  }

  isOutOfWorkingRange(inputTime: string): boolean {
    const timeInput = moment.duration(inputTime);
    const timeOpen = moment.duration(this.time_open);
    const timeClose = moment.duration(this.time_close);
    return timeInput.asMinutes() < timeOpen.asMinutes() || timeInput.asMinutes() > timeClose.asMinutes();
  }
}
