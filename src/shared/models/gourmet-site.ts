import { Deserializable } from 'shared/interfaces/deserializable';
import { GOURMET_NAME } from 'shared/enums/gourmet-site';

export interface GourmetSiteInput {
  id?: number;
  service_type: string;
  username?: string;
  password?: string;
  code?: string;
}

export class GourmetSite implements Deserializable<GourmetSite>, GourmetSiteInput {
  id?: number;
  service_type: string;
  username?: string;
  password?: string;
  code?: string;

  get service_name(): string {
    return GOURMET_NAME[this.service_type];
  }

  get status_name(): string {
    return this.username ? '連携済' : '未連携';
  }

  get btn_name(): string {
    return this.username ? '編集する' : '設定する';
  }

  deserialize(input: Partial<GourmetSiteInput>): GourmetSite {
    Object.assign(this, input);
    return this;
  }
}
