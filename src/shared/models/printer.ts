import { Deserializable } from 'shared/interfaces/deserializable';
import { Client } from './client';
import { Menu, MenuInput } from './menu';
import { Course, CourseInput } from './course';
import { MenuBuffet, MenuBuffetInput } from './menu-buffet';
import { RESV_ITEMS_TYPE } from 'shared/enums/reservation';
import { Table, TableInput } from './table';

interface AppliedTaxesInput {
  percent: number;
  target_price: number;
  tax_price: number;
}

interface ReservationPayerInput {
  id: number;
  amount: number;
  group: any;
  paid_at: string;
  preorder: boolean;
  transaction_id: any;
  tender_amount: number;
  change_back: number;
  card_last_4: string;
  card_brand: string;
  refund_at: any;
  payment_by: string;
}

export interface PrinterInput {
  id: number;
  quantity: number;
  total_price: number;
  total_taxes: number;
  name: string;
  start_at: string;
  end_at: string;
  confirm_start_at: string;
  confirm_end_at: string;
  payment_by: string;
  paid_at: string;
  coupon_discount: number;
  receipt_number: any;
  applied_taxes: AppliedTaxesInput[];
  client: Client;
  reservation_items: Array<MenuInput | CourseInput | MenuBuffetInput>;
  reservation_payer: ReservationPayerInput;
  tables: TableInput[];
}

export class Printer implements Deserializable<Printer>, PrinterInput {
  id: number;
  quantity: number;
  total_price: number = 0;
  total_taxes: number = 0;
  name: string;
  start_at: string;
  end_at: string;
  confirm_start_at: string;
  confirm_end_at: string;
  payment_by: string;
  paid_at: string;
  coupon_discount: number;
  receipt_number: any;
  applied_taxes: AppliedTaxesInput[];
  client: Client;
  reservation_items: Array<Menu | Course | MenuBuffet>;
  reservation_payer: ReservationPayerInput;
  tables: Table[];

  get isCard(): boolean {
    return this.reservation_payer && !!this.reservation_payer.card_last_4;
  }

  tax10?: AppliedTaxesInput;
  get tax10TargetStr(): string {
    return this.tax10 && this.tax10.target_price ? this.tax10.target_price.format() : '0';
  }
  get tax10PriceStr(): string {
    return this.tax10 && this.tax10.tax_price ? this.tax10.tax_price.format() : '0';
  }

  tax8?: AppliedTaxesInput;
  get tax8TargetStr(): string {
    return this.tax8 && this.tax8.target_price ? this.tax8.target_price.format() : '0';
  }
  get tax8PriceStr(): string {
    return this.tax8 && this.tax8.tax_price ? this.tax8.tax_price.format() : '0';
  }

  get changeBackStr(): string {
    return this.reservation_payer && this.reservation_payer.change_back
      ? this.reservation_payer.change_back.format()
      : '0';
  }

  constructor() {
    this.client = new Client();
  }

  deserialize(input: Partial<Printer>): Printer {
    Object.assign(this, input);

    if (input.client) {
      this.client = new Client().deserialize(input.client);
    }

    if (input.reservation_items) {
      this.reservation_items = input.reservation_items.map((item: MenuInput | CourseInput | MenuBuffetInput) => {
        switch (item.type) {
          case RESV_ITEMS_TYPE.ITEM:
            return new Menu().deserialize(item);
          case RESV_ITEMS_TYPE.COURSE:
            return new Course().deserialize(item);
          case RESV_ITEMS_TYPE.BUFFET:
            return new MenuBuffet().deserialize(item);
        }
      });
    }

    if (this.applied_taxes) {
      this.tax10 = this.applied_taxes.find((item: AppliedTaxesInput) => item.percent === 10);
      this.tax8 = this.applied_taxes.find((item: AppliedTaxesInput) => item.percent === 8);
    }

    if (input.tables) {
      this.tables = input.tables.map((table: TableInput) => new Table().deserialize(table));
    }

    return this;
  }

  printData(): any {
    const currentDate = new Date();
    const dateStr = currentDate
      .toISOString()
      .substring(0, 10)
      .replace(/-/g, '/');
    const timeStr = currentDate.toTimeString().substring(0, 8);

    return {
      logo: this.client.logo.url,
      address: this.client.fullAddress,
      phone: this.client.phone,
      start_at: `${dateStr} ${timeStr}`,
      reservationNumber: this.id,
      tablesNumber: this.tables.map((table: Table) => table.code).join(','),
      items: this.reservation_items.map((item: Menu | Course | MenuBuffet | any) => ({
        name: item.name,
        price: item.price,
        total_price: item.total_price,
        quantity: item.quantity,
        taxRatio: item.tax.ratio,
        menu_options: item.menu_options,
      })),
      subtotal_price: this.total_price - this.total_taxes,
      taxTenPercentTarget: this.tax10 ? this.tax10.target_price : 0,
      taxTenPercentPrice: this.tax10 ? this.tax10.tax_price : 0,
      taxEightPercentTarget: this.tax8 ? this.tax8.target_price : 0,
      taxEightPercentPrice: this.tax8 ? this.tax8.tax_price : 0,
      discount_fee: this.coupon_discount,
      total_price: this.total_price,
      amountTax: this.total_taxes,
      userPayment: this.reservation_payer.tender_amount,
      displayChange: this.reservation_payer.change_back,
      receiptNumber: this.reservation_payer.id,
      card_number: this.reservation_payer.card_last_4,
      card_brand: this.reservation_payer.card_brand,
    };
  }
}
