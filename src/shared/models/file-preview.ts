import { Deserializable } from 'shared/interfaces/deserializable';

export interface FilePreviewInput {
  origin?: File;
  name?: string;
  size?: number;
  type?: string;
  isUpload?: boolean;
  isError?: boolean;
  errorMsg?: string;
  base64?: string;
  downloadURL?: string;
  fullPath?: string;
}

export class FilePreview implements Deserializable<FilePreview>, FilePreviewInput {
  origin: File;
  name: string;
  size: number;
  type: string;
  isUpload: boolean;
  isError: boolean;
  errorMsg: string;
  base64: string;
  downloadURL: string;
  fullPath: string;

  get stylesheet() {
    return {
      'background-image': `url(${this.base64 ? this.base64 : 'https://via.placeholder.com/150'})`,
    };
  }

  deserialize(input: Partial<FilePreviewInput | File>): FilePreview {
    Object.assign(this, input);
    return this;
  }
}
