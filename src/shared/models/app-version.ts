import { Deserializable } from 'shared/interfaces/deserializable';
import { AppOsInput, AppOS } from './app-os';

export interface AppVersionInput {
  ios: AppOsInput;
  android: AppOsInput;
  web: AppOsInput;
}

export class AppVersion implements Deserializable<AppVersion>, AppVersionInput {
  ios: AppOS;
  android: AppOS;
  web: AppOS;

  deserialize(input: Partial<AppVersionInput>): AppVersion {
    Object.assign(this, input);

    this.web = input.web ? new AppOS().deserialize(input.web) : new AppOS();
    this.ios = input.ios ? new AppOS().deserialize(input.ios) : new AppOS();
    this.android = input.android ? new AppOS().deserialize(input.android) : new AppOS();

    return this;
  }
}
