import { Deserializable } from 'shared/interfaces/deserializable';
import * as moment from 'moment';
import { ClientUserInput, ClientUser } from './client-user';

export interface MissedCallInput {
  id: number;
  call_sid: string;
  duration: number;
  end_at: string;
  start_at: string;
  phone: string;
  status: string;
  client_user: Partial<ClientUserInput>;
}

export class MissedCall implements Deserializable<MissedCall>, MissedCallInput {
  id: number;
  call_sid: string;
  duration: number;
  end_at: string;
  start_at: string;
  get start_at_display_intl(): string {
    return moment(this.start_at).format('YYYY/MM/DD HH:mm');
  }
  phone: string;
  status: string;
  client_user: ClientUser;

  deserialize(input: Partial<MissedCallInput>): MissedCall {
    Object.assign(this, input);
    if (input.client_user) {
      this.client_user = new ClientUser().deserialize(input.client_user);
    } else {
      this.client_user = new ClientUser();
    }
    return this;
  }
}
