import { Deserializable } from 'shared/interfaces/deserializable';

export interface MenuOptionInput {
  id: number;
  name: string;
  price: number;
  quantity?: number;
  total_price?: number;
  is_single: boolean;
  is_checked: boolean;
}

export class MenuOption implements Deserializable<MenuOption>, MenuOptionInput {
  id: number;
  name: string;
  get nameTruncate(): string {
    return this.name.toEllipsisWithCountLetters(50).string;
  }
  get nameDisplay(): string {
    return `${this.name} (${this.price_format})`;
  }
  price: number;
  get price_format(): string {
    return this.price ? `${this.price.format()}円` : '0円';
  }
  is_single: boolean;
  is_checked: boolean;
  quantity?: number;
  total_price?: number;

  deserialize(input: Partial<MenuOptionInput>): MenuOption {
    Object.assign(this, input);

    this.is_single = !input.is_single;

    return this;
  }

  formDataString() {
    const data: any = {
      name: this.name,
      price: this.price,
      is_single: this.is_single ? 0 : 1,
    };

    if (this.id) {
      data.id = this.id;
    }
    return data;
  }
}
