import { Deserializable } from 'shared/interfaces/deserializable';
import { MenuInput, Menu, MenuOrderOutput } from './menu';
import { MenuBuffet, MenuBuffetInput, MenuBuffetOrderOutput } from './menu-buffet';
import { COURSE_ACTIVE } from 'shared/enums/course';
import { RESV_ITEMS_TYPE } from 'shared/enums/reservation';

export interface CourseInput {
  id: number;
  name: string;
  price: number;
  memo: string;
  image_url: string;
  is_show: boolean;
  items: MenuInput[];
  buffets?: MenuBuffetInput[];
  quantity?: number;
  total_price: number;
  created_at?: string;
  paid_at: string;
  preoder: boolean;
  updated_at?: string;
  deleted_at?: string;
  type: string;
  is_tax: boolean;
  reservation_item_id?: number;
  // used in screen receipt details (C-6)
  isReceiptFocus?: boolean;
}

export interface CourseFormData {
  name: string;
  price: number | string;
  memo: string;
  is_show: number;
  items: string[] | number[] | MenuOrderOutput[];
  buffets: string[] | number[] | MenuBuffetOrderOutput[];
}

export interface CourseOrderOutput {
  id: number;
  quantity: number;
}

export class Course implements Deserializable<Course>, CourseInput {
  id: number;
  name: string;
  price: number;

  get price_format(): string {
    if (!this.price) {
      return '0';
    }
    return `${this.price.format()}`;
  }
  get price_string(): string {
    if (!this.price) {
      return;
    }
    return `¥${this.price.format()}`;
  }

  memo: string;
  image_url: string;
  get image_url_src(): string {
    return this.image_url || 'assets/img/no-image.png';
  }
  get style(): any {
    return {
      'background-image': `url(${this.image_url_src})`,
    };
  }
  is_show: boolean;
  get activeString() {
    return this.is_show ? COURSE_ACTIVE.SHOW : COURSE_ACTIVE.HIDE;
  }
  get hideString(): string {
    return !this.is_show ? COURSE_ACTIVE.HIDE : '';
  }
  items: Menu[];
  buffets: MenuBuffet[];
  quantity: number;
  paid_at: string;
  get is_paid(): boolean {
    return !!this.paid_at;
  }
  preoder: boolean;
  total_price: number;
  get total_price_format(): string {
    if (!this.total_price) {
      return '0';
    }
    return `${this.total_price.format()}`;
  }
  created_at: string;
  updated_at: string;
  deleted_at: string;
  get isDisabled(): boolean {
    return !!this.deleted_at;
  }
  type: string;
  is_tax: boolean;

  get input_name_quantity(): string {
    return `quantity-${RESV_ITEMS_TYPE.COURSE}-${this.id}`;
  }
  reservation_item_id?: number;

  // used in screen receipt details (C-6)
  isReceiptFocus: boolean = false;

  constructor() {
    this.deserialize({
      quantity: 0,
      items: [],
      buffets: [],
      is_show: true,
    });
  }

  deserialize(input: Partial<CourseInput>): Course {
    Object.assign(this, input);
    if (input.items) {
      this.items = input.items.map((item: MenuInput) => new Menu().deserialize(item));
    }
    if (input.buffets) {
      this.buffets = input.buffets.map((item: MenuBuffetInput) => new MenuBuffet().deserialize(item));
    }
    return this;
  }

  formData(): CourseFormData {
    return {
      name: this.name,
      is_show: this.is_show ? 1 : 0,
      memo: this.memo,
      price: this.price,
      items: this.items.map((menu: Menu) => {
        return {
          id: menu.id,
          quantity: menu.quantity,
        };
      }),
      buffets: this.buffets.map((buffet: MenuBuffet) => {
        return {
          id: buffet.id,
          quantity: buffet.quantity,
        };
      }),
    };
  }

  minusQuantity() {
    if (this.quantity === 0) {
      return;
    }
    this.quantity--;
  }

  plusQuantity() {
    if (this.quantity === 100) {
      return;
    }
    this.quantity++;
  }

  getCourseOrder(): CourseOrderOutput {
    return {
      id: this.id,
      quantity: this.quantity,
    };
  }
}
