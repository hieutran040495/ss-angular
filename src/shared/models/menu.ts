import { Deserializable } from 'shared/interfaces/deserializable';
import { MenuType } from './menu-type';
import { Category, CategoryInput } from './category';
import { RESV_ITEMS_TYPE } from 'shared/enums/reservation';
import { MenuOption } from './menu-option';
import { MenuSubStatus } from './menu-substatus';
import { MenuCookingType } from './menu-cooking-type';
import { MenuCookingDuration } from './menu-cooking-duration';

export interface MenuInput {
  id: number;
  name: string;
  name_en?: string;
  name_ko?: string;
  name_zh_hans?: string;
  name_zh_hant?: string;
  internal_name: string;
  price: number;
  quantity?: number;
  total_price: number;
  is_show: boolean;
  image_url: string;
  item_category: CategoryInput;
  validator?: string;
  type: string;
  recommend: boolean;
  paid_at: string;
  preoder: boolean;
  only_course: boolean;
  only_buffet: boolean;
  menu_options: MenuOption[];
  sub_statuses?: MenuSubStatus[];
  cooking_type?: MenuCookingType;
  cooking_duration?: MenuCookingDuration;
  is_speed: boolean;
  ordinal_number: number;
  swapClassActive: string;
  is_sold_out: boolean;
  is_tax: boolean;
  deleted_at: string;
  reservation_item_id?: number;
  allow_take_out: boolean;
  // used in screen receipt details (C-6)
  isReceiptFocus?: boolean;
}

export interface MenuOutput {
  name: string;
  name_en?: string;
  name_ko?: string;
  name_zh_hans?: string;
  name_zh_hant?: string;
  internal_name: string;
  is_show: number;
  category_id: number;
  price: number;
  recommend: number;
  only_course: number;
  only_buffet: number;
  menu_option_ids: number[];
  sub_status_ids?: number[];
  cooking_type_id?: number;
  cooking_duration_id?: number;
  is_speed: number;
  ordinal_number: number;
  is_sold_out: number;
  allow_take_out: number;
}

export interface MenuOrderOutput {
  id: number;
  quantity: number;
}

export interface MenuFilter {
  name: string;
  menu_type: MenuType[];
  category: string;
  is_show: number;
  price: string;
}

export class Menu implements Deserializable<Menu>, MenuInput {
  id: number;
  name: string;
  get nameTruncate(): string {
    return this.name.toEllipsisWithCountLetters(50).string;
  }
  name_en: string;
  name_ko: string;
  name_zh_hans: string;
  name_zh_hant: string;
  internal_name: string;
  price: number;
  get price_string(): string {
    return `¥${this.price.format()}`;
  }
  get price_format(): string {
    return this.price ? `${this.price.format()}` : '0';
  }

  item_type: MenuType;
  is_show: boolean;
  get isShowString(): string {
    return !this.is_show ? '非表示' : '';
  }
  quantity: number;
  total_price: number;
  get total_price_format(): string {
    if (!this.total_price) {
      return '0';
    }
    return `${this.total_price.format()}`;
  }
  image_url: string;
  get image_url_src(): string {
    return this.image_url || 'assets/img/no-image.png';
  }
  get style(): any {
    return {
      'background-image': `url(${this.image_url_src})`,
    };
  }
  item_category: Category;
  validator?: string;
  paid_at: string;
  get is_paid(): boolean {
    return !!this.paid_at;
  }
  preoder: boolean;
  type: string;
  get isCourse(): boolean {
    return this.type === RESV_ITEMS_TYPE.COURSE;
  }

  get item_infor_display(): string {
    return `${this.name} (${this.price_format}円) × ${this.quantity}`;
  }

  get input_name_quantity(): string {
    return `quantity-${RESV_ITEMS_TYPE.ITEM}-${this.id}`;
  }

  recommend: boolean;
  only_course: boolean;
  only_buffet: boolean;
  menu_options: MenuOption[];
  get menu_options_price(): number {
    const priceArr = this.menu_options.map((item) => item.price);
    return !!priceArr.length ? priceArr.reduce((total, value) => total + value) : 0;
  }

  sub_statuses?: MenuSubStatus[];
  cooking_type?: MenuCookingType;
  cooking_duration?: MenuCookingDuration;
  is_speed: boolean;
  ordinal_number: number;

  swapClassActive: string;
  is_sold_out: boolean;
  is_tax: boolean;
  deleted_at: string;
  get isDisabled(): boolean {
    return !!this.deleted_at;
  }
  reservation_item_id?: number;
  allow_take_out: boolean;

  // used in screen receipt details (C-6)
  isReceiptFocus: boolean = false;

  constructor() {
    this.deserialize({
      quantity: 1,
      item_category: new Category().deserialize({}),
      is_show: true,
      menu_options: [],
      is_sold_out: false,
      is_tax: false,
    });
  }

  deserialize(input: Partial<MenuInput>): Menu {
    Object.assign(this, input);
    if (input.item_category) {
      this.item_category = new Category().deserialize(input.item_category);
    }
    if (input.menu_options) {
      this.menu_options = input.menu_options.map((menu_option: MenuOption) =>
        new MenuOption().deserialize(menu_option),
      );
    }
    if (input.sub_statuses) {
      this.sub_statuses = input.sub_statuses.map((status) => new MenuSubStatus().deserialize(status));
    }
    if (input.cooking_type) {
      this.cooking_type = new MenuCookingType().deserialize(input.cooking_type);
    }
    if (input.cooking_duration) {
      this.cooking_duration = new MenuCookingDuration().deserialize(input.cooking_duration);
    }
    return this;
  }

  formData(): MenuOutput {
    const data: MenuOutput = {
      category_id: this.item_category.id,
      name: this.name,
      name_en: this.name_en,
      name_ko: this.name_ko,
      name_zh_hans: this.name_zh_hans,
      name_zh_hant: this.name_zh_hant,
      internal_name: this.internal_name,
      is_show: this.is_show ? 1 : 0,
      price: this.price,
      recommend: this.recommend ? 1 : 0,
      only_course: this.only_course ? 1 : 0,
      only_buffet: this.only_buffet ? 1 : 0,
      menu_option_ids: this.menu_options.map((option: MenuOption) => option.id),
      is_speed: this.is_speed ? 1 : 0,
      ordinal_number: this.ordinal_number,
      is_sold_out: this.is_sold_out ? 1 : 0,
      allow_take_out: this.allow_take_out ? 1 : 0,
    };

    if (this.sub_statuses && !!this.sub_statuses.length) {
      data.sub_status_ids = this.sub_statuses.map((status: MenuSubStatus) => status.id);
    }
    if (this.cooking_type) {
      data.cooking_type_id = this.cooking_type.id;
    }
    if (this.cooking_duration) {
      data.cooking_duration_id = this.cooking_duration.id;
    }

    return data;
  }

  minusQuantity() {
    if (this.quantity === 0) {
      return;
    }
    this.quantity--;
  }

  plusQuantity() {
    if (this.quantity === 100) {
      return;
    }
    this.quantity++;
  }

  getMenuOrder(): MenuOrderOutput {
    return {
      id: this.id,
      quantity: this.quantity,
    };
  }
}
