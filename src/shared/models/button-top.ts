import { Deserializable } from 'shared/interfaces/deserializable';

export interface ButtonTopInput {
  pos_x: number;
  pos_y: number;
}

export interface MetadataInput {
  btn_1: Partial<ButtonTop>;
  btn_2: Partial<ButtonTop>;
}

export class ButtonTop implements Deserializable<ButtonTop>, ButtonTopInput {
  pos_x: number;
  pos_y: number;

  constructor() {
    this.pos_y = 0;
    this.pos_x = 0;
  }

  deserialize(input: Partial<ButtonTopInput>): ButtonTop {
    Object.assign(this, input);

    return this;
  }

  toJSON() {
    return {
      pos_x: this.pos_x || 0,
      pos_y: this.pos_y || 0,
    };
  }
}

export class Metadata implements Deserializable<Metadata>, MetadataInput {
  btn_1: ButtonTop;
  btn_2: ButtonTop;

  constructor() {}

  deserialize(input: Partial<MetadataInput>): Metadata {
    if (input.btn_1) {
      this.btn_1 = new ButtonTop().deserialize(input.btn_1);
    }
    if (input.btn_2) {
      this.btn_2 = new ButtonTop().deserialize(input.btn_2);
    }
    return this;
  }

  toJSON() {
    return {
      btn_1: this.btn_1.toJSON(),
      btn_2: this.btn_2.toJSON(),
    };
  }
}
