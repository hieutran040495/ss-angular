import { Deserializable } from 'shared/interfaces/deserializable';

export interface ResvEmailInput {
  id: number;
  email: string;
  title: string;
  content: string;
}

export interface ResvEmailOutput {
  email: string;
  title: string;
  content: string;
}

export class ResvEmail implements Deserializable<ResvEmail>, ResvEmailInput {
  id: number;
  email: string;
  title: string;
  content: string;

  deserialize(input: Partial<ResvEmailInput>): ResvEmail {
    Object.assign(this, input);
    return this;
  }

  formData(): ResvEmailOutput {
    const data: ResvEmailOutput = {
      email: this.email,
      title: this.title,
      content: this.content,
    };

    return data;
  }
}
