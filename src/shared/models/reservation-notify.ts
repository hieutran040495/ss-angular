import { Deserializable } from 'shared/interfaces/deserializable';
import * as moment from 'moment';

export interface ReservationNotifyInput {
  id: number;
  start_at?: string;
  end_at?: string;
  created_at: string;
  serviceable_id?: number;
  serviceable_type?: string;
}

export class ReservationNotify implements Deserializable<ReservationNotify>, ReservationNotifyInput {
  id: number;

  start_at: string;
  end_at: string;
  get start_end_date_time_display(): string {
    const date = moment(this.start_at).format('YYYY/MM/DD');
    const start_time = moment(this.start_at).format('HH:mm');
    const end_time = moment(this.end_at).format('HH:mm');

    return `${date} ${start_time}~${end_time}`;
  }
  created_at: string;
  serviceable_id?: number;
  serviceable_type?: string;

  constructor() {}

  deserialize(input: Partial<ReservationNotifyInput>): ReservationNotify {
    Object.assign(this, input);
    return this;
  }
}
