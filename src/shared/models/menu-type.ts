import { Deserializable } from 'shared/interfaces/deserializable';
import { ClientInput, Client } from './client';

export interface MenuTypeInput {
  id: number;
  client: ClientInput;
  name: string;
  ordinal_number: number;
  typeable: string;
  created_at: string;
  updated_at: string;
}
export interface MenuTypeOutput {
  name: string;
  ordinal_number: number;
  id?: number;
}

export class MenuType implements Deserializable<MenuType>, MenuTypeInput {
  id: number;
  client: Client;
  name: string;
  ordinal_number: number;
  typeable: string;
  created_at: string;
  updated_at: string;

  get nameTruncate() {
    return this.name.toEllipsisWithCountLetters(10).string;
  }

  deserialize(input: Partial<MenuTypeInput>): MenuType {
    Object.assign(this, input);
    this.client = new Client().deserialize(input.client);
    return this;
  }

  formDataString(): MenuTypeOutput {
    const data: MenuTypeOutput = {
      name: this.name,
      ordinal_number: this.ordinal_number,
    };

    if (this.id) {
      data.id = this.id;
    }

    return data;
  }
}
