import { Deserializable } from 'shared/interfaces/deserializable';
import { MASTER_STATUS } from 'shared/enums/cooking-master';

import * as moment from 'moment';

export interface MenuCookingTypeInput {
  id: number;
  name: string;
  old_name?: string;
  ordinal: number;
  status?: string;
  client_id: number;
  created_at?: string;
  updated_at?: string;
}

export class MenuCookingType implements Deserializable<MenuCookingType>, MenuCookingTypeInput {
  id: number;
  name: string;
  old_name: string;
  get nameTruncate() {
    return this.name.toEllipsisWithCountLetters(10).string;
  }
  ordinal: number;
  status: string;
  get status_class(): string {
    switch (this.status) {
      case MASTER_STATUS.CREATE:
      case MASTER_STATUS.UPDATE:
        return `master--change`;
      case MASTER_STATUS.REMOVE:
        return `master--remove`;
      case MASTER_STATUS.NONE:
        return ``;
      default:
        break;
    }
  }
  client_id: number;
  created_at: string;
  get created_at_display(): string {
    return moment(this.created_at).format('YYYY/MM/DD HH:mm');
  }
  updated_at: string;
  get updated_at_display(): string {
    return moment(this.updated_at).format('YYYY/MM/DD HH:mm');
  }

  deserialize(input: Partial<MenuCookingTypeInput>): MenuCookingType {
    Object.assign(this, input);
    return this;
  }
}
