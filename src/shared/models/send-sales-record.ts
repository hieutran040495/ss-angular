import { Deserializable } from 'shared/interfaces/deserializable';

export interface RecordInput {
  email: string;
  enable: boolean;
}

export class Record implements Deserializable<Record>, RecordInput {
  email: string;
  enable: boolean = true;

  deserialize(input: Partial<RecordInput>): Record {
    Object.assign(this, input);
    return this;
  }
}
