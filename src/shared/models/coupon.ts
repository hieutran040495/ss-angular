import { Deserializable } from 'shared/interfaces/deserializable';
import { Course, CourseInput } from 'shared/models/course';
import {
  COUPON_STATUS,
  COUPON_TARGET,
  COUPON_STATUS_TEXT,
  SPECIFIC_TARGET_TEXT,
  USAGE_TYPE,
  STATUS_CLASSES,
} from 'shared/enums/coupon';
import * as moment from 'moment';

export interface CouponInput {
  id: number;
  is_show: boolean;
  name: string;
  code: string;
  client_id: number;
  start_date: string | Date;
  start_time: string;
  end_date: string | Date;
  end_time: string;
  is_specific_target: boolean;
  is_specific_time: boolean;
  is_all_day: boolean;
  target_type: string;
  usage_count: number;
  usage_type: string;
  status: string;
  discount: number;
  expiry_at: string;
  courses: Course[];
  created_at: string;
  updated_at: string;
  deleted_at: string;
}
export interface CouponOutPut {
  id?: number;
  is_show?: number;
  name: string;
  code: string;
  start_date: string;
  start_time?: string;
  end_date: string;
  end_time?: string;
  is_specific_target: boolean;
  is_specific_time: boolean;
  target_type?: string;
  usage_count?: number;
  usage_type?: string;
  status?: string;
  discount: number;
}

export interface CouponStatistics {
  total_drafts: number;
  total_publishes: number;
  total_unpublishes: number;
  total_expiries: number;
  total: number;
}

export class Coupon implements Deserializable<Coupon>, CouponInput {
  id: number;
  is_show: boolean;
  name: string;
  code: string;
  get code_name_display(): string {
    return `${this.code}(${this.name})`;
  }
  client_id: number;
  start_date: string | Date;

  get start_date_display(): string {
    return moment(this.start_date, 'YYYY/MM/DD').format('YYYY年 MM月 DD日');
  }
  start_time: string;
  end_date: string | Date;
  get end_date_display(): string {
    return moment(this.end_date, 'YYYY/MM/DD').format('YYYY年 MM月 DD日');
  }
  end_time: string;
  is_specific_time: boolean;
  is_all_day: boolean;
  is_specific_target: boolean;
  target_type: string;
  get isTargetNew(): boolean {
    return this.target_type === COUPON_TARGET.NEW;
  }
  get isTargetRepeat(): boolean {
    return this.target_type === COUPON_TARGET.REPEAT_USER;
  }
  get isTargetUsage(): boolean {
    return this.target_type === COUPON_TARGET.USAGE_COUNT;
  }
  usage_type: string;

  get isUsageEqual(): boolean {
    return this.usage_type === USAGE_TYPE.USAGE_EQUAL;
  }
  get isUsageGreater(): boolean {
    return this.usage_type === USAGE_TYPE.USAGE_GREATER;
  }

  usage_count: number;
  status: string;
  discount: number;
  get discount_display(): string {
    if (!this.discount) {
      return;
    }
    return this.discount.format();
  }

  get isDraft(): boolean {
    return this.status === COUPON_STATUS.DRAFT;
  }
  get isPublish(): boolean {
    return this.status === COUPON_STATUS.PUBLISH;
  }
  get isUnPublish(): boolean {
    return this.status === COUPON_STATUS.UNPUBLISH;
  }
  get isExpiryAt(): boolean {
    return this.status === COUPON_STATUS.EXPIRED;
  }
  get status_string(): string {
    if (!this.status) {
      return;
    }
    return COUPON_STATUS_TEXT[this.status];
  }
  get specific_target_string(): string {
    if (this.is_specific_time) {
      return SPECIFIC_TARGET_TEXT.INDICATIONS_TIME;
    }
    return SPECIFIC_TARGET_TEXT.ALL_DAY;
  }

  get status_class(): string {
    return STATUS_CLASSES[this.status];
  }

  expiry_at: string;
  courses: Course[];

  created_at: string;
  updated_at: string;
  deleted_at: string;

  constructor() {
    this.deserialize({
      courses: [],
      is_specific_target: false,
      is_specific_time: true,
      is_show: false,
      usage_type: USAGE_TYPE.USAGE_GREATER,
      start_date: moment().format('YYYY-MM-DD'),
      end_date: moment()
        .add(1, 'day')
        .format('YYYY-MM-DD'),
      start_time: '06:00:00',
      end_time: '05:59:00',
    });
  }

  deserialize(input: Partial<CouponInput>): Coupon {
    Object.assign(this, input);
    if (input.courses) {
      this.courses = input.courses.map((course: CourseInput) => new Course().deserialize(course));
    }
    if (input.start_time) {
      const timeDuration = moment(`${input.start_date} ${input.start_time}`, 'YYYY-MM-DD H:mm:ss a');
      this.start_time = timeDuration.format('H:mm');
    }

    if (input.end_time) {
      const timeDuration = moment(`${input.end_date} ${input.end_time}`, 'YYYY-MM-DD H:mm:ss a');
      this.end_time = timeDuration.format('H:mm');
    }
    this.is_all_day = !this.is_specific_time;
    return this;
  }

  formData(): CouponOutPut {
    const data: CouponOutPut = {
      name: this.name,
      code: this.code,
      start_date: moment(this.start_date, 'YYYY/MM/DD').format('YYYY-MM-DD'),
      end_date: moment(this.end_date, 'YYYY-MM-DD').format('YYYY-MM-DD'),
      discount: this.discount,
      is_specific_target: this.is_specific_target,
      is_specific_time: !this.is_all_day,
      status: this.status,
      is_show: this.is_show ? 1 : 0,
    };

    if (this.is_specific_target) {
      data.target_type = this.target_type;
    }
    if (this.isTargetUsage) {
      data.usage_count = this.usage_count;
      data.usage_type = this.usage_type;
    }

    if (!this.is_all_day) {
      data.start_time = `${this.start_time}:00`;
      data.end_time = `${this.end_time}:00`;
    }
    return data;
  }
}
