import { Deserializable } from 'shared/interfaces/deserializable';
import * as moment from 'moment';
import { AppScreen, AppScreenInput } from './app-screen';

export interface MaintenanceInput {
  id: number;
  start_at?: string;
  end_at?: string;
  start_date: string | Date;
  start_time: string;
  end_date: string | Date;
  end_time: string;
  message: string;
  in_maintenance_time: boolean;
  app_screens: AppScreen[];
}

export class Maintenance implements Deserializable<Maintenance>, MaintenanceInput {
  id: number;
  name: string;
  start_date: string | Date;
  start_time: string;
  end_date: string | Date;
  end_time: string;
  message: string;
  in_maintenance_time: boolean = false;
  app_screens: AppScreen[];

  constructor() {
    this.deserialize({
      id: 1,
      start_date: null,
      end_date: null,
      start_time: '00:00',
      end_time: '00:00',
      in_maintenance_time: false,
      app_screens: [],
    });
  }

  deserialize(input: Partial<MaintenanceInput>): Maintenance {
    Object.assign(this, input);

    if (input.start_at) {
      const startAt = moment(input.start_at);
      this.start_date = startAt.toDate();
      this.start_time = startAt.format('HH:mm');
    }

    if (input.end_at) {
      const endAt = moment(input.end_at);
      this.end_date = endAt.toDate();
      this.end_time = endAt.format('HH:mm');
    }
    if (input.app_screens) {
      this.app_screens = input.app_screens.map((app: AppScreenInput) => new AppScreen().deserialize(app));
    }
    return this;
  }
}
