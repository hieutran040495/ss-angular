import { Deserializable } from 'shared/interfaces/deserializable';

export interface ResvKeywordInput {
  id: number;
  client_id: number;
  keyword: string;
}

export interface KeywordOutput {
  keyword: string;
}

export class ResvKeyword implements Deserializable<ResvKeyword>, ResvKeywordInput {
  id: number;
  client_id: number;
  keyword: string;
  get name_display(): string {
    return this.keyword ? this.keyword : '未登録';
  }
  get is_registered(): boolean {
    return !!this.keyword;
  }
  is_selected: boolean;

  deserialize(input: Partial<ResvKeywordInput>): ResvKeyword {
    Object.assign(this, input);
    return this;
  }

  formData(): KeywordOutput {
    return {
      keyword: this.keyword,
    };
  }
}
