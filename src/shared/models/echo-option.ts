import { Deserializable } from 'shared/interfaces/deserializable';
import { environment } from 'environments/environment';
import * as urljoin from 'url-join';

export interface HederInput {
  headers: {
    Authorization: string;
  };
}

export interface EchoOptionInput {
  key: string;
  cluster: string;
  encrypted: boolean;
  authEndpoint: string;
  auth: Partial<HederInput>;
  broadcaster: string;
  disableStats?: boolean;
}

export class EchoOption implements Deserializable<EchoOption>, EchoOptionInput {
  key: string;
  cluster: string;
  encrypted: boolean;
  authEndpoint: string;
  auth: HederInput;
  broadcaster: string;
  disableStats: boolean;

  constructor() {
    this.deserialize({
      key: environment.pusher_key,
      cluster: environment.pusher_cluster,
      broadcaster: 'pusher',
      encrypted: true,
      authEndpoint: urljoin(environment.api_path, 'broadcasting/auth'),
      disableStats: true,
    });
  }

  deserialize(input: Partial<EchoOptionInput>): EchoOption {
    Object.assign(this, input);
    return this;
  }
}
