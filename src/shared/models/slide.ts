import { TopImage } from 'shared/models/top-image';
import { RichMenu } from 'shared/models/rich-menu';
import { Deserializable } from 'shared/interfaces/deserializable';
import { Style } from 'shared/models/style';

export interface SlideInput {
  image?: Partial<TopImage>;
  style: Partial<Style>;
  caption: string;
  active: boolean;
  isNew?: boolean;
  richMenu?: Partial<RichMenu>;
}

export class Slide implements Deserializable<Slide>, Partial<SlideInput> {
  image?: Partial<TopImage>;
  style: Partial<Style>;
  caption: string;
  active: boolean;
  isNew?: boolean;
  richMenu?: Partial<RichMenu>;

  deserialize(input: Partial<SlideInput>): Slide {
    if (!input) {
      return;
    }
    Object.assign(this, input);
    if (input.style) {
      this.style = new Style().deserialize(input.style);
    }
    return this;
  }
}
