import { Deserializable } from 'shared/interfaces/deserializable';

export interface StatisticInput {
  phone_total_price: number;
  web_total_price: number;
  walk_in_total_price: number;
  total_price: number;
  total_reservations: number;
}
export class ReservationStatistic implements Deserializable<ReservationStatistic>, StatisticInput {
  phone_total_price: number;
  get phone_total_price_display(): string {
    if (!+this.phone_total_price) {
      return '0円';
    }
    return `${(+this.phone_total_price).format()}円`;
  }
  web_total_price: number;
  get web_total_price_display(): string {
    if (!+this.web_total_price) {
      return '0円';
    }
    return `${(+this.web_total_price).format()}円`;
  }
  walk_in_total_price: number;
  get walk_in_total_price_display(): string {
    if (!+this.walk_in_total_price) {
      return '0円';
    }
    return `${(+this.walk_in_total_price).format()}円`;
  }
  total_price: number;
  get total_price_display(): string {
    if (!+this.total_price) {
      return '0円';
    }
    return `${(+this.total_price).format()}円`;
  }
  total_reservations: number;
  get total_reservations_display(): string {
    if (!+this.total_reservations) {
      return '0件';
    }
    return `${(+this.total_reservations).format()}件`;
  }

  constructor() {}

  deserialize(input: Partial<StatisticInput>): ReservationStatistic {
    Object.assign(this, input);
    return this;
  }
}
