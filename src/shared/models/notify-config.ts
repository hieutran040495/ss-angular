import { Deserializable } from 'shared/interfaces/deserializable';

export interface NotifyConfigInput {
  client_id: number;
  is_enable: number;
  duration?: number;
  name: string;
}

export class NotifyConfig implements Deserializable<NotifyConfigInput>, NotifyConfig {
  client_id: number;
  is_enable: number;
  duration: number;
  name: string;

  constructor() {
    this.deserialize({
      is_enable: 0,
      duration: 0,
    });
  }

  deserialize(input: Partial<NotifyConfigInput>): NotifyConfig {
    Object.assign(this, input);
    return this;
  }

  formDataJSON() {
    return {
      is_enable: this.is_enable,
      duration: this.duration ? +this.duration : null,
    };
  }
}
