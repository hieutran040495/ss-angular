import { Deserializable } from 'shared/interfaces/deserializable';
import * as moment from 'moment';

export interface CurrentReservationInput {
  id: number;
  quantity: number;
  name: string;
  memo?: string;
  start_at?: string;
  end_at?: string;
  type: string;
  created_by: string;
  receipt_date: string;
  first_at_store: boolean;
  status: string;
  confirm_start_at?: string;
  confirm_end_at?: string;
}

export class CurrentReservation implements Deserializable<CurrentReservation>, CurrentReservationInput {
  id: number;
  quantity: number;
  total_price: number;
  name: string;
  memo: string;
  first_at_store: boolean;
  start_at: string;
  get start_at_display(): string {
    return moment(this.start_at).format('HH:mm');
  }
  end_at: string;
  get start_end_time_string(): string {
    let end_time = '';
    if (this.end_at) {
      end_time = moment(this.end_at).format('HH:mm');
    }
    return `${this.start_at_display}~${end_time}`;
  }
  type: string;
  created_by: string;
  receipt_date: string;
  status: string;
  confirm_start_at: string;
  confirm_end_at: string;

  constructor() {}

  deserialize(input: Partial<CurrentReservationInput>): CurrentReservation {
    Object.assign(this, input);
    return this;
  }
}
