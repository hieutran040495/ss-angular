import { Deserializable } from 'shared/interfaces/deserializable';
import { ClientUser, ClientUserInput } from './client-user';
import * as moment from 'moment';
import { PHONE_STATUS } from 'shared/enums/phone';

export interface HistoryCallInput {
  id: number;
  start_at: string;
  end_at: string;
  status: string;
  duration: string;
  phone: string;
  client_user: ClientUserInput;
}

export class HistoryCall implements Deserializable<HistoryCall>, HistoryCallInput {
  id: number;
  start_at: string;
  start_at_string: string;
  start_time_string: string;
  end_at: string;
  status: string;
  get is_missed(): boolean {
    return this.status === PHONE_STATUS.NO_ANSWER;
  }
  duration: string;
  phone: string;
  client_user: ClientUser;
  get user_name_phone(): string {
    return this.client_user ? this.client_user.name : this.phone;
  }
  is_show: boolean;

  deserialize(input: Partial<HistoryCallInput>): HistoryCall {
    Object.assign(this, input);
    if (input.client_user) {
      this.client_user = new ClientUser().deserialize(input.client_user);
    }

    if (input.start_at) {
      this.start_at_string = moment(input.start_at)
        .locale('ja')
        .format('YYYY[年]MMMDo HH:mm');

      this.start_time_string = this.getTimeDisplay(input.start_at);
    }

    return this;
  }

  getTimeDisplay(dateTime: string): string {
    const now = moment();
    const date = moment(dateTime);
    const diffDays = now.date() - date.date();
    if (diffDays > 1) {
      return date.format('MM/DD');
    }
    if (diffDays === 1) {
      return '昨日';
    }
    return date.format('HH:mm');
  }
}
