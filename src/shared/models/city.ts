import { Deserializable } from 'shared/interfaces/deserializable';

export interface CityInput {
  bigCityFlag: string;
  prefCode: string;
  cityCode: string;
  cityName: string;
}

export class City implements Deserializable<City>, Partial<CityInput> {
  bigCityFlag: string;
  prefCode: string;
  cityCode: string;
  cityName: string;

  deserialize(input: Partial<CityInput>): City {
    Object.assign(this, input);
    return this;
  }
}
