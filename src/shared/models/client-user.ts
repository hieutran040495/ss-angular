import { Deserializable } from 'shared/interfaces/deserializable';
import { ReservationInput } from './reservation';

export interface ClientUserInput {
  id: number;
  client_id: number;
  user_id: number;
  memo: string;
  name: string;
  phone: string | number;
  upcoming_reservation?: ReservationInput;
  finished_reservations_count: number;
  is_new: boolean;
}

export interface ClientUserOutput {
  id: number;
  client_id: number;
  user_id: number;
  memo: string;
  name: string;
  phone: string | number;
}

export class ClientUser implements Deserializable<ClientUser>, ClientUserInput {
  id: number;
  client_id: number;
  user_id: number;
  memo: string;
  name: string;
  get name_display(): string {
    return this.is_new ? 'ご新規' : this.name;
  }
  phone: string;
  has_near_resv = false;
  is_new: boolean;
  has_upcoming_resv = false;
  finished_reservations_count: number;
  reservation_id: number;

  get status_string(): string {
    return this.is_new ? '新規' : `${this.finished_reservations_count}回`;
  }

  get is_casless_user(): boolean {
    return !!this.user_id;
  }
  get isOmiseno(): boolean {
    return !!this.id && !!this.user_id;
  }

  constructor() {
    this.is_new = true;
  }

  deserialize(input: Partial<ClientUserInput>): ClientUser {
    if (input) {
      Object.assign(this, input);
    }
    if (input.upcoming_reservation) {
      this.has_upcoming_resv = !!input.upcoming_reservation;
      this.reservation_id = input.upcoming_reservation.id;
    }
    return this;
  }

  formData(): ClientUserOutput {
    return {
      id: this.id,
      client_id: this.client_id,
      user_id: this.user_id,
      memo: this.memo,
      name: this.name,
      phone: this.phone,
    };
  }
}
