import { ResourceInput } from 'fullcalendar-scheduler/src/types/input-types';
import { Deserializable } from 'shared/interfaces/deserializable';
import { Table, TableInput } from './table';

interface FcResourceInput extends ResourceInput {
  table: Partial<TableInput>;
  group: string;
}
export class FcResource implements Deserializable<FcResource>, FcResourceInput {
  id: string;
  table: Table;
  group: string;

  constructor() {}

  deserialize(input: Partial<FcResourceInput>): FcResource {
    if (input) {
      Object.assign(this, input);
    }

    if (input.table) {
      this.table = new Table().deserialize(input.table);
    }

    return this;
  }
}
