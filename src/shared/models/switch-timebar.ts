import { Deserializable } from 'shared/interfaces/deserializable';

export interface SwitchTimeBarInput {
  serial: number;
  scale?: number;
  slot_duration: number;
  slot_width: number;
  val_color?: number;
  width_column?: number;
}

export class SwitchTimebar implements Deserializable<SwitchTimebar>, Partial<SwitchTimeBarInput> {
  serial: number;
  scale?: number;
  slot_duration: number;
  slot_width: number;
  val_color?: number;
  width_column?: number;

  constructor() {
    this.deserialize({
      serial: 6,
      scale: 12,
      slot_duration: 30,
      slot_width: this.cal_slot_width_serial(),
      width_column: this.cal_width_column_serial(),
    });
  }

  deserialize(input: Partial<SwitchTimeBarInput>): SwitchTimebar {
    Object.assign(this, input);
    return this;
  }

  private cal_slot_width_serial(): number {
    const timebar_width = window.innerWidth - 210;
    const width_column = Math.round(timebar_width / 12);
    return Math.round(width_column / 2);
  }

  private cal_width_column_serial(): number {
    return Math.round((window.innerWidth - 210) / 12);
  }
}
