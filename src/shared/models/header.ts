import { Deserializable } from 'shared/interfaces/deserializable';

export interface HeaderInput {
  isSetting: boolean;
  isChild: boolean;
  pageTitle: string;
  subPageTitle?: string;
  isHeaderSeparate?: boolean;
  isDownloadCSV?: boolean;
}

export class Header implements Deserializable<Header>, HeaderInput {
  isSetting: boolean;
  isHeaderSeparate?: boolean;
  isDownloadCSV?: boolean;
  isChild: boolean;
  pageTitle: string;
  subPageTitle: string;
  get isPageTitle(): boolean {
    return !!this.pageTitle;
  }
  get isNotify(): boolean {
    return !this.isSetting && !this.isChild;
  }

  deserialize(input: Partial<HeaderInput>): Header {
    Object.assign(this, input);
    return this;
  }
}
