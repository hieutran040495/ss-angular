import { Reservation } from './reservation';
import { Table } from './table';

export class Receipt extends Reservation {
  get total_price_include_fee_display(): string {
    return `${this.total_price_display}円(税込)`;
  }

  get total_payment_left_display(): string {
    return `${(this.total_price - this.preorder_paid).format()}`;
  }

  get tableDisplay(): string {
    return this.tables[0] ? this.tables[0].name : '';
  }

  get tableCodeDisplay(): string {
    return this.isSelfOrder ? this.receipt_number.toString() : this.tables.map((table: Table) => table.code).join(', ');
  }
}
