import { Deserializable } from 'shared/interfaces/deserializable';

export interface ReservationLatestPaymentInput {
  id: number;
  amount: number;
  group: any;
  paid_at: string;
  preorder: boolean;
  transaction_id: string;
  tender_amount: number;
  change_back: number;
  refund_at: string;
  card_last_4: any;
  payment_by: string;
}

export class ReservationLatestPayment
  implements Deserializable<ReservationLatestPayment>, ReservationLatestPaymentInput {
  id: number;
  amount: number;
  group: any;
  paid_at: string;
  preorder: boolean;
  transaction_id: string;
  tender_amount: number;
  change_back: number;
  refund_at: string;
  card_last_4: any;
  payment_by: string;

  deserialize(input: Partial<ReservationLatestPaymentInput>): ReservationLatestPayment {
    Object.assign(this, input);
    return this;
  }
}
