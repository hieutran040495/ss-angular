import { Deserializable } from 'shared/interfaces/deserializable';
import * as pickBy from 'lodash/pickBy';

export interface RichMenuItemInput {
  id: number;
  image_url: string;
  is_show: boolean;
  name: string;
  pos_x: number;
  pos_y: number;
  price: number;
  recommend: boolean;
  total_revenue: number;
  type_id: number;
  created_at: string;
  updated_at: string;
}

export class RichMenuItem implements Deserializable<RichMenuItem>, Partial<RichMenuItemInput> {
  id: number;
  name: string;
  get nameTruncate(): string {
    return this.name.toEllipsisWithCountLetters(54).string;
  }
  get nameTruncateSelfOrderPatternOne(): string {
    return this.name.toEllipsisWithCountLetters(83).string;
  }
  get image_url_src(): string {
    return this.image_url || 'assets/img/no-image.png';
  }
  get style(): any {
    return {
      'background-image': `url(${this.image_url_src})`,
    };
  }
  pos_x: number;
  pos_y: number;
  image_url: string;
  price: number;
  get total_price_display(): string {
    return this.price ? `${this.price.format()} 円` : '0円';
  }
  recommend: boolean;
  total_revenue: number;
  type_id: number;
  created_at: string;
  updated_at: string;

  constructor() {
    this.pos_x = 0;
    this.pos_y = 0;
  }

  deserialize(input: Partial<RichMenuItemInput>): RichMenuItem {
    if (!input) {
      return;
    }
    Object.assign(this, input);

    return this;
  }

  public toJSON() {
    return pickBy(this, (value, key) => {
      return ['pos_x', 'pos_y', 'id'].indexOf(key) >= 0 && ![null, undefined, ''].includes(value);
    });
  }
}
