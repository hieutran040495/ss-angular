import { Deserializable } from 'shared/interfaces/deserializable';
import { LOCALSTORAGE_KEY } from 'shared/enums/local-storage';
import { AppVersionInput, AppVersion } from './app-version';

export interface VersionInput {
  id: number;
  name: string;
  created_at: string;
  updated_at: string;
  is_active: boolean;
  app_versions: AppVersionInput;
}

export interface VersionOutput {
  name: string;
}

export class Version implements Deserializable<Version>, VersionInput {
  id: number;
  name: string;
  created_at: string;
  updated_at: string;
  is_active: boolean;
  app_versions: AppVersion;

  constructor() {}

  deserialize(input: Partial<VersionInput>): Version {
    Object.assign(this, input);
    if (!localStorage.getItem(LOCALSTORAGE_KEY.LASTEST_VERSION)) {
      localStorage.setItem(LOCALSTORAGE_KEY.LASTEST_VERSION, this.toJsonString());
    }
    this.app_versions = input.app_versions ? new AppVersion().deserialize(input.app_versions) : new AppVersion();
    return this;
  }

  toJsonString() {
    return JSON.stringify({
      id: this.id,
      name: this.name,
    });
  }
}
