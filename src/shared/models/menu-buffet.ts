import { Deserializable } from '../interfaces/deserializable';
import { ClientInput, Client } from './client';
import { MenuInput, MenuOrderOutput, Menu } from './menu';

export interface MenuBuffetInput {
  id: number;
  client?: ClientInput;
  name: string;
  price: number;
  quantity?: number;
  total_price: number;
  memo: string;
  is_show: boolean;
  image_url?: string;
  typeable?: string;
  created_at?: string;
  updated_at?: string;
  deleted_at?: string;
  items: MenuInput[];
  validator?: string;
  paid_at: string;
  preoder: boolean;
  type: string;
  is_tax: boolean;
  reservation_item_id?: number;
  // used in screen receipt details (C-6)
  isReceiptFocus?: boolean;
}

export interface MenuBuffetOutput {
  name: string;
  price: number | string;
  memo: string;
  items: string[] | number[] | MenuOrderOutput[];
  is_show: number;
}

export interface MenuBuffetOrderOutput {
  id: number;
  quantity: number;
}

export class MenuBuffet implements Deserializable<MenuBuffet>, MenuBuffetInput {
  id: number;
  client: Client;
  name: string;
  get nameTruncate(): string {
    return this.name.toEllipsisWithCountLetters(50).string;
  }
  price: number;
  get price_format(): string {
    return this.price ? this.price.format() : '0';
  }
  memo: string;
  quantity: number;
  total_price: number;
  get total_price_format(): string {
    if (!this.total_price) {
      return '0';
    }
    return `${this.total_price.format()}`;
  }
  image_url: string;
  get image_url_src(): string {
    return this.image_url || 'assets/img/no-image.png';
  }
  get style(): any {
    return {
      'background-image': `url(${this.image_url_src})`,
    };
  }
  is_show: boolean;
  typeable: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
  items: Menu[];
  validator?: string;
  paid_at: string;
  get is_paid(): boolean {
    return !!this.paid_at;
  }
  preoder: boolean;
  type: string;
  is_tax: boolean;
  reservation_item_id?: number;

  // used in screen receipt details (C-6)
  isReceiptFocus: boolean = false;

  constructor() {
    this.deserialize({
      items: [],
      quantity: 1,
      is_show: true,
    });
  }

  deserialize(input: Partial<MenuBuffetInput>): MenuBuffet {
    Object.assign(this, input);
    if (input.client) {
      this.client = new Client().deserialize(input.client);
    }
    if (input.items) {
      this.items = input.items.map((item: MenuInput) => new Menu().deserialize(item));
    }
    return this;
  }

  formData(): MenuBuffetOutput {
    return {
      name: this.name,
      memo: this.memo,
      price: this.price,
      items: this.items.map((menu: Menu) => {
        return {
          id: menu.id,
          quantity: menu.quantity,
        };
      }),
      is_show: this.is_show ? 1 : 0,
    };
  }

  getMenuBuffetOrder(): MenuBuffetOrderOutput {
    return {
      id: this.id,
      quantity: this.quantity,
    };
  }

  minusQuantity() {
    if (this.quantity === 0) {
      return;
    }
    this.quantity--;
  }

  plusQuantity() {
    if (this.quantity === 100) {
      return;
    }
    this.quantity++;
  }
}
