import { Deserializable } from 'shared/interfaces/deserializable';
import { RichMenu, RichMenuInput } from './rich-menu';

export interface ItemCategoryInput {
  id: number;
  created_at: string;
  name: string;
  ordinal_number: number;
  self_order_rich_menu_settings: RichMenu[];
  order_rich_menu_settings: RichMenu[];
}
export class ItemCategory implements Deserializable<ItemCategory>, ItemCategoryInput {
  id: number;
  created_at: string;
  name: string;
  ordinal_number: number;
  self_order_rich_menu_settings: RichMenu[];
  get totalSelfOrderUploadFile(): number {
    return this.self_order_rich_menu_settings.reduce((total, item) => {
      if (!!item.file_upload) {
        return total + 1;
      }
      return total;
    }, 0);
  }
  order_rich_menu_settings: RichMenu[];

  constructor() {}

  deserialize(input: Partial<ItemCategoryInput>): ItemCategory {
    Object.assign(this, input);
    if (input.self_order_rich_menu_settings && !!input.self_order_rich_menu_settings.length) {
      this.self_order_rich_menu_settings = input.self_order_rich_menu_settings.map((item: RichMenuInput) =>
        new RichMenu().deserialize(item),
      );
    }
    if (input.order_rich_menu_settings && !!input.order_rich_menu_settings.length) {
      this.order_rich_menu_settings = input.order_rich_menu_settings.map((item: RichMenuInput) =>
        new RichMenu().deserialize(item),
      );
    }
    return this;
  }

  richMenuIdToJson() {
    const richMenu = this.self_order_rich_menu_settings || this.order_rich_menu_settings;
    const ids = richMenu
      .map((res) => {
        return res.id;
      })
      .filter((item: any) => item !== undefined);
    return ids;
  }

  settingToJson() {
    const data = this.self_order_rich_menu_settings.map((res) => {
      return res.toJSON();
    });
    return { rich_menu_settings: data };
  }

  settingOrderToJSON() {
    const data = this.order_rich_menu_settings.map((res) => {
      return res.toJSON();
    });
    return { rich_menu_settings: data };
  }
}
