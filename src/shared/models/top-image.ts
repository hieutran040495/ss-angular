import { Deserializable } from 'shared/interfaces/deserializable';
import { APP_SETTING } from 'shared/enums/rich-menu';
import { Metadata } from './button-top';

export interface TopImageInput {
  id: number;
  page: number;
  scale: number;
  image_width: number;
  image_height: number;
  widthScale: number;
  heightScale: number;
  pos_x: number;
  pos_y: number;
  image_path: string;
  image_url: string;
  ratio: number;
  font_family: string;
  bg_color: string;
  text_color: string;
  type: string;
  rich_menu_id: number;
  pattern: number;
  client_id: number;
  file?: File;
  metadata?: Partial<Metadata>;
}
export class TopImage implements Deserializable<TopImage>, TopImageInput {
  id: number;
  page: number;
  scale: number;
  image_width: number;
  image_height: number;
  widthScale: number;
  heightScale: number;
  pos_x: number;
  pos_y: number;
  image_url: string;
  ratio: number;
  font_family: string;
  bg_color: string;
  text_color: string;
  type: string;
  rich_menu_id: number;
  pattern: number;
  client_id: number;
  image_path: string;
  file: File;
  metadata: Metadata;

  constructor() {
    this.pos_y = 0;
    this.pos_x = 0;
    this.ratio = 1;
    this.page = 1;
  }

  deserialize(input: Partial<TopImageInput>): TopImage {
    Object.assign(this, input);
    if (input.metadata) {
      this.metadata = new Metadata().deserialize(input.metadata);
    }
    return this;
  }

  toJSON() {
    return {
      settings: [
        {
          page: this.page,
          pos_x: this.pos_x || 0,
          pos_y: this.pos_y || 0,
          ratio: 1,
          text_color: this.text_color,
          bg_color: this.bg_color,
          font_family: this.font_family,
          image_path: this.image_path,
          metadata: this.metadata ? this.metadata.toJSON() : undefined,
        },
      ],
      type: APP_SETTING.SELF_ORDER,
    };
  }

  resetImage() {
    this.image_path = null;
    this.image_url = null;
    this.image_width = undefined;
    this.image_height = undefined;
  }

  updateStyle(data: any) {
    this.text_color = data.text_color;
    this.bg_color = data.bg_color;
    this.font_family = data.font_family;
  }
}
