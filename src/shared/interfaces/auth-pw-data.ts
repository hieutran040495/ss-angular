export interface ForgotPwData {
  login_id: string;
  email: string;
}

export interface ResetPwData {
  client_id: number;
  id: string;
  password?: string | undefined;
  password_confirmation?: string | undefined;
  token: string;
}
