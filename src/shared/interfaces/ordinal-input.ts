export interface OrdinalInput {
  name: string;
  ordinal_number: number;
}
