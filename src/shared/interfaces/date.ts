export interface DateFilter {
  created_at_gte: Date | string;
  created_at_lte: Date | string;
}
