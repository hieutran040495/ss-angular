export interface StyleOpts {
  text_color: string;
  bg_color?: string;
  font_family: string;
}

export interface TextConfig {
  fill: string;
  fontFamily: string;
}
