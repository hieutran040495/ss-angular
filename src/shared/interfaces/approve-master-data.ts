export interface ApproveMasterData {
  path: string;
  import_at: string;
}
