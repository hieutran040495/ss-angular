export interface FileUpload {
  key?: string;
  value?: File | string | Blob;
  name?: string;
  validation?: boolean;
}
