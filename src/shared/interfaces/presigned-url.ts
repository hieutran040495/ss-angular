export interface PresignedUrls {
  path: string;
  url: string;
}
