export interface LoginData {
  login_id: string;
  email: string;
  password: string;
  phone?: string;
}
