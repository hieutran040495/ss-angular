import { TopImage } from 'shared/models/top-image';
import { RichMenu } from 'shared/models/rich-menu';

export interface Slide {
  image?: TopImage;
  style: any;
  caption: string;
  active: boolean;
  isNew?: boolean;
  richMenu?: RichMenu;
}
