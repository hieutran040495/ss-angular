import { Injectable } from '@angular/core';

import { CasApiService } from './cas-api.service';
import { UploaderService } from './uploader.service';

import { FileUpload } from 'shared/interfaces/file-upload';
import { MenuBuffet, MenuBuffetInput } from 'shared/models/menu-buffet';

@Injectable({
  providedIn: 'root',
})
export class MenuBuffetService {
  constructor(private casApiSv: CasApiService, private uploaderSv: UploaderService) {}

  fetchMenuBuffetById(id: number, opts?: any) {
    return this.casApiSv.get(`buffets/${id}`, opts).map((res: MenuBuffetInput) => new MenuBuffet().deserialize(res));
  }

  getBuffets(opts?: any, isResetQuantity?: boolean) {
    return this.casApiSv.get('buffets', opts).map((res) => {
      res.data = res.data.map((menu: MenuBuffetInput) => {
        if (isResetQuantity) {
          menu.quantity = 0;
        }
        return new MenuBuffet().deserialize(menu);
      });
      return res;
    });
  }

  createMenuBuffet(files: FileUpload[], data: any) {
    return this.uploaderSv.store('buffets', files, data);
  }

  updateMenuBuffet(buffetId: number, files: FileUpload[], data: any) {
    return this.uploaderSv.update(`buffets/${buffetId}`, files, data);
  }

  deleteMenuBuffet(buffetId: number) {
    return this.casApiSv.delete(`buffets/${buffetId}`);
  }
}
