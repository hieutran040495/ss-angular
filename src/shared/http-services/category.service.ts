import { Injectable } from '@angular/core';
import { CasApiService } from 'shared/http-services/cas-api.service';
import { Category, CategoryInput, CategoryOutput } from 'shared/models/category';
import 'rxjs-compat/add/operator/map';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  constructor(private casApiSv: CasApiService) {}

  getCategories(opts?: any) {
    return this.casApiSv.get('item-categories', opts).map((res) => {
      res.data = res.data.map((cat: CategoryInput) => new Category().deserialize(cat));
      return res.data;
    });
  }

  getListCategory(opts?: any) {
    return this.casApiSv.get('item-categories', opts).map((res) => {
      res.data = res.data.map((cat: CategoryInput) => new Category().deserialize(cat));
      return res;
    });
  }

  createCategory(category: CategoryOutput) {
    return this.casApiSv.post('item-categories', category);
  }

  editCategory(category: CategoryOutput) {
    return this.casApiSv.put(`item-categories/${category.id}`, category);
  }

  deleteCategory(categoryId: number) {
    return this.casApiSv.delete(`item-categories/${categoryId}`);
  }

  swapCategory(opts: any) {
    return this.casApiSv.post('item-categories/swap', opts);
  }
}
