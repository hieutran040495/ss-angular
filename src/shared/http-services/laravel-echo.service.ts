import { Injectable } from '@angular/core';
import { EchoOption } from 'shared/models/echo-option';
import { SessionQuery } from 'shared/states/session';
import { LOCALSTORAGE_KEY } from 'shared/enums/local-storage';

import 'pusher-js';
import Echo from 'laravel-echo';

@Injectable({
  providedIn: 'root',
})
export class LaravelEchoService {
  echo: Echo;
  subscriber: any;
  subscriberGlobal: any;
  private channelClient: string = 'client_member.global';

  constructor(private sessionQuery: SessionQuery) {}

  private async initEchoConfig() {
    const data = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY.APP_NAME));

    let option = new EchoOption();

    if (data && data.token) {
      option = new EchoOption().deserialize({
        auth: {
          headers: { Authorization: `Bearer ${data.token}` },
        },
        disableStats: true,
      });
    }

    return option;
  }

  async initEcho() {
    if (this.echo && this.echo.options.auth) {
      return;
    }

    if (this.echo && !this.echo.options.auth) {
      console.log('disconnect Pusher');
      await this.leaveChannelGlobal();
    }

    console.log('init config pusher');
    const config = await this.initEchoConfig();
    console.log('init pusher');
    this.echo = new Echo(config);
  }

  async initChannelPrivate() {
    console.log('init channel private');
    const user = await this.sessionQuery.getUserLoggined();
    if (user && user.id) {
      this.subscriber = await this.echo.private(user.defaultChannel);

      if (!this.subscriberGlobal) {
        this.initChannelGlobal();
      }
    }
  }

  initChannelGlobal() {
    console.log('init channel global');
    this.subscriberGlobal = this.echo.channel(this.channelClient);
  }

  onNotification(cb: Function) {
    if (this.subscriber) {
      this.subscriber.notification(cb);
    }
  }

  onNotificationGlobal(cb: Function) {
    this.subscriberGlobal.notification(cb);
  }

  async leaveChannel() {
    if (this.echo) {
      console.log('leave Channel Private');
      const user = await this.sessionQuery.getUserLoggined();
      this.echo.leave(user.defaultChannel);
      this.echo = undefined;
      this.subscriber = undefined;
      console.log('Disconnect');
      await this.initEcho();
      this.initChannelGlobal();
    }
  }

  async leaveChannelGlobal() {
    if (this.echo) {
      console.log('leave Channel Global');
      await this.echo.leave(this.channelClient);
      this.subscriberGlobal = undefined;
    }
  }
}
