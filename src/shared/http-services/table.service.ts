import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';

import { Table, TableInput, TableOutput } from 'shared/models/table';
import { CheckAvailableTableOutput } from 'shared/models/reservation';

@Injectable({
  providedIn: 'root',
})
export class TableService {
  constructor(private casApiSv: CasApiService) {}

  getTables(opts?: any) {
    return this.casApiSv.get(`tables`, opts).map((res) => {
      res.data = res.data.map((item: TableInput) => new Table().deserialize(item));
      return res;
    });
  }

  fetchTableById(tableId: number, opts?: any) {
    return this.casApiSv.get(`tables/${tableId}`, opts).map((res: TableInput) => new Table().deserialize(res));
  }

  createTable(table: TableOutput) {
    return this.casApiSv.post('tables', table);
  }

  editTable(table: TableOutput) {
    return this.casApiSv.put(`tables/${table.id}`, table);
  }

  updateLayout(data: any) {
    return this.casApiSv.post(`tables/setting`, data);
  }

  removeTable(tableId: number) {
    return this.casApiSv.delete(`tables/${tableId}`);
  }

  checkAvailable(data: CheckAvailableTableOutput) {
    return this.casApiSv.post(`tables/check-available`, data);
  }

  swapTable(opts: any) {
    return this.casApiSv.post('tables/swap', opts);
  }

  checkValidSeats(idTable: number, data: any) {
    return this.casApiSv.post(`tables/${idTable}/valid-seats`, data);
  }

  downloadQRCode(opts?: any) {
    return this.casApiSv.get('/tables/qr-codes', opts);
  }
}
