import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';
import { MissedCallInput, MissedCall } from 'shared/models/missed-call';

@Injectable({
  providedIn: 'root',
})
export class MissedCallService {
  constructor(private casApiSv: CasApiService) {}

  getMissedCalls(opts?: any) {
    return this.casApiSv.get(`phones/missed`, opts).map((res) => {
      res.data = res.data.map((item: MissedCallInput) => new MissedCall().deserialize(item));
      return res;
    });
  }

  solvedMissedCall(phone: string, data: { solved: boolean }) {
    return this.casApiSv.post(`/phones/${phone}/solve`, data);
  }

  getUnSolvedMissedCall(opts?: any) {
    return this.casApiSv.get(`stats/warning-count`, opts);
  }
}
