import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';
import { Tax, TaxInput, TaxOutput } from '../models/tax';
import { Observable } from 'rxjs';

@Injectable()
export class TaxService {
  constructor(private _api: CasApiService) {}

  /**
   * Get taxes from server
   * @method GET
   * @param opts: any
   */
  getTaxes(opts?: any): Observable<any> {
    return this._api.get('taxes', opts).map((res: any) => {
      res.data = res.data.map((item: TaxInput) => new Tax().deserialize(item));
      return res;
    });
  }

  /**
   * Get tax by id from server
   * @method GET
   * @param taxId: number
   * @param opts: any
   */
  getTaxById(taxId: number, opts?: any): Observable<Tax> {
    return this._api.get(`taxes/${taxId}`, opts).map((res: TaxInput) => new Tax().deserialize(res));
  }

  /**
   * Update Tax
   * @method PUT
   * @param taxId: number
   * @param data: TaxOutput
   */
  updateTax(taxId: number, data: TaxOutput): Observable<TaxInput> {
    return this._api.put(`taxes/${taxId}`, data);
  }

  /**
   * Update Tax Type
   * @method PUT
   * @param opts: Object
   */
  updateTaxType(opts) {
    return this._api.put('taxes/type', opts);
  }

  /**
   * Delete Tax
   * @method DELETE
   * @param taxId: number
   */
  deleteTax(taxId: number): Observable<boolean> {
    return this._api.delete(`taxes/${taxId}`);
  }
}
