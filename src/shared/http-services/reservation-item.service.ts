import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';

@Injectable()
export class ReservationItemService {
  constructor(private casApiSv: CasApiService) {}

  updateQuantity(itemId: number, data?: any) {
    return this.casApiSv.put(`reservation-items/${itemId}/quantity`, data);
  }

  updateOptions(itemId: number, data?: any) {
    return this.casApiSv.put(`reservation-items/${itemId}/options`, data);
  }

  getItemById(opts?: any) {
    return this.casApiSv.get(`menu-options`, opts);
  }

  removeById(itemId: number) {
    return this.casApiSv.get(`reservation-items/${itemId}/cancel`);
  }
}
