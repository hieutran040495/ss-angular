import { Injectable } from '@angular/core';
import { CasApiService } from 'shared/http-services/cas-api.service';

@Injectable({
  providedIn: 'root',
})
export class MaintenanceService {
  constructor(private _api: CasApiService) {}

  getMaintenanceStatus(opts?: any) {
    return this._api.get('/maintenance/status', opts);
  }
}
