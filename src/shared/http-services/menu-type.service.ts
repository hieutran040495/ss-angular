import { Injectable } from '@angular/core';
import { CasApiService } from 'shared/http-services/cas-api.service';
import { MenuType, MenuTypeInput, MenuTypeOutput } from 'shared/models/menu-type';
import { MENU_TYPE } from 'shared/enums/menu-type';

@Injectable({
  providedIn: 'root',
})
export class MenuTypeService {
  constructor(private casApiSv: CasApiService) {}

  fetchMenutypes(opts?: any) {
    return this.casApiSv.get('item-types', opts).map((res) => {
      const indexUnsorted = res.data.findIndex((item) => item.name === MENU_TYPE.UNSORTED);
      if (indexUnsorted >= 0) {
        res.data.splice(indexUnsorted, 1);
      }
      res.data = res.data.map((item: MenuTypeInput) => new MenuType().deserialize(item));
      return res.data;
    });
  }

  getListMenutypes(opts?: any) {
    return this.casApiSv.get('item-types', opts).map((res) => {
      const indexUnsorted = res.data.findIndex((item) => item.name === MENU_TYPE.UNSORTED);
      if (indexUnsorted >= 0) {
        res.data.splice(indexUnsorted, 1);
      }
      res.data = res.data.map((item: MenuTypeInput) => new MenuType().deserialize(item));
      return res;
    });
  }

  createMenutype(menuType: MenuTypeOutput) {
    return this.casApiSv.post('item-types', menuType);
  }

  updateMenutype(menuTypeId: number, menuType: MenuTypeOutput) {
    return this.casApiSv.put(`item-types/${menuTypeId}`, menuType);
  }

  destroyMenutype(menuTypeId: number) {
    return this.casApiSv.delete(`item-types/${menuTypeId}`);
  }
}
