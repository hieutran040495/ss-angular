import { Injectable } from '@angular/core';
import { User, UserInput, UserUpdateInfo } from 'shared/models/user';
import { CasApiService } from './cas-api.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private casApiSv: CasApiService) {}

  getUsers(opts?: any) {
    return this.casApiSv.get('client-users', opts).map((res) => {
      res.data = res.data.map((user: UserInput) => new User().deserialize(user));
      return res;
    });
  }

  getUserById(userId: number, opts?: any) {
    return this.casApiSv.get(`client-users/${userId}`, opts).map((res: UserInput) => new User().deserialize(res));
  }

  updateUser(userId: number, data: UserUpdateInfo) {
    return this.casApiSv.put(`client-users/${userId}`, data);
  }

  getCurUser() {
    return this.casApiSv.get('/profile').map((res) => new User().deserialize(res));
  }
}
