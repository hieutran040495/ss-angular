import { Injectable } from '@angular/core';
import { CasApiService } from 'shared/http-services/cas-api.service';
import { UploaderService } from 'shared/http-services/uploader.service';
import { Menu, MenuInput, MenuOutput } from 'shared/models/menu';
import 'rxjs-compat/add/operator/map';

@Injectable({
  providedIn: 'root',
})
export class MenuService {
  constructor(private casApiSv: CasApiService, private uploaderSv: UploaderService) {}

  fetchMenus(opts?: any, isResetQuantity?: boolean) {
    return this.casApiSv.get('items', opts).map((res) => {
      res.data = res.data.map((menu: MenuInput) => {
        if (isResetQuantity) {
          menu.quantity = 0;
        }
        return new Menu().deserialize(menu);
      });
      return res;
    });
  }

  fetchMenuById(menuId: number, opts?: any) {
    return this.casApiSv.get(`items/${menuId}`, opts).map((res: MenuInput) => new Menu().deserialize(res));
  }

  createMenu(files: any, menu: MenuOutput) {
    return this.uploaderSv.store('items', files, menu);
  }

  updateMenu(menuId: number, files: any, menu: MenuOutput) {
    return this.uploaderSv.update(`items/${menuId}`, files, menu);
  }

  destroyMenuImage(menuId: number) {
    return this.casApiSv.delete(`item/${menuId}/remove-image`);
  }

  swapMenu(opts: any) {
    return this.casApiSv.post('items/swap', opts);
  }

  menuDestroy(menuId: number) {
    return this.casApiSv.delete(`items/${menuId}`);
  }
}
