import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Deserializable } from 'shared/interfaces/deserializable';
import { environment } from 'environments/environment';

import * as urljoin from 'url-join';

interface UploaderApiInput {
  apiUrl: string;
  progress: number;
}

@Injectable({
  providedIn: 'root',
})
export class UploaderService implements Deserializable<UploaderService>, UploaderApiInput {
  apiUrl: string;
  progress: number;

  private _progressObserver;

  constructor() {
    this.deserialize({
      apiUrl: environment.api_path,
      progress: 0,
    });
  }

  deserialize(input: Partial<UploaderApiInput>): UploaderService {
    Object.assign(this, input);
    return this;
  }

  /**
   * @param method POST/PUT
   * @param url string
   * @param files Array {key, value}
   * @param params Object
   */
  private _getFormData(
    method,
    url: string,
    files: Array<any> = [],
    params: any = {},
    isMethodPut?: boolean,
  ): Observable<any> {
    return Observable.create((observer) => {
      const formData: FormData = this._initParams(params, new FormData());
      const xhr: XMLHttpRequest = new XMLHttpRequest();

      if (isMethodPut) {
        formData.append('_method', 'PUT');
      }

      files = files.filter((item: any) => item !== undefined);

      if (files.length !== 0) {
        files.map((item: any) => {
          if (item.value) {
            formData.append(item.key, item.value, item.value.name);
          }
        });
      }

      const path_url = urljoin(this.apiUrl, url);
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200 || xhr.status === 201 || xhr.status === 204) {
            if (xhr.response === '') {
              observer.next({ success: true });
              observer.complete();
              return;
            }

            observer.next(JSON.parse(xhr.response));
            observer.complete();
          } else {
            const errors = JSON.parse(xhr.response);
            observer.error(errors);
          }
        }
      };

      xhr.upload.onprogress = (event) => {
        this.progress = Math.round((event.loaded / event.total) * 100);
        if (this._progressObserver !== undefined) {
          this._progressObserver.next(this.progress);
        }
      };

      xhr.open(method, path_url, true);
      const token = JSON.parse(localStorage.getItem('cas_client_app')).token;
      xhr.setRequestHeader('Authorization', `Bearer ${token}`);
      xhr.send(formData);
    });
  }

  private _initParams(params = {}, formData: FormData, preKey: string = ''): FormData {
    Object.keys(params).forEach((key) => {
      if ([null, undefined].includes(params[key])) {
        return;
      }

      if ([true, false].includes(params[key])) {
        return formData.append(!preKey ? key : preKey + `[${key}]`, params[key]);
      }

      if (params[key] instanceof Array) {
        return this._initParamsArr(params[key], formData, !preKey ? key : preKey + `[${key}]`);
      }

      if (typeof params[key] === 'object') {
        return this._initParams(params[key], formData, !preKey ? key : preKey + `[${key}]`);
      }

      formData.append(!preKey ? key : preKey + `[${key}]`, params[key]);
    });

    return formData;
  }

  private _initParamsArr(params = [], formData: FormData, preKey = '') {
    params.forEach((value, index) => {
      if (value instanceof Object) {
        return this._initParams(value, formData, !preKey ? `[${index}]` : preKey + `[${index}]`);
      }

      formData.append(!preKey ? `[${index}]` : preKey + `[${index}]`, value);
    });

    return formData;
  }

  store(url: string, files: any[], params: any = {}, method: string = 'POST'): Observable<any> {
    return this._getFormData(method, url, files, params, false);
  }

  update(url: string, files: any[], params: any = {}): Observable<any> {
    return this._getFormData('POST', url, files, params, true);
  }

  uploadS3(files: any[], params: any = {}): Observable<any> {
    return this._getFormData('PUT', ``, files, params, true);
  }
}
