import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';
import { ClientTwilio } from 'shared/models/client-twilio';

@Injectable({
  providedIn: 'root',
})
export class TwilioService {
  constructor(private api: CasApiService) {}

  /**
   * registerTwilio
   */
  public registerTwilio(data = {}) {
    return this.api.post(`twilios`, data).map((res) => new ClientTwilio().deserialize(res));
  }
}
