import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root',
})
export class S3UploaderService {
  progress: number;

  private _progressObserver;

  constructor() {
    this.progress = 0;
  }

  /*
   * @param method PUT
   * @param S3url string
   * @param files Array {key, value}
   */
  private _getFormData(s3SignalUrl: string, files: File, image_type?: string): Observable<any> {
    return Observable.create((observer) => {
      const reader = new FileReader();
      const xhr: XMLHttpRequest = new XMLHttpRequest();

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200 || xhr.status === 201 || xhr.status === 204) {
            if (xhr.response === '') {
              observer.next({ success: true });
              observer.complete();
              return;
            }

            observer.next(JSON.parse(xhr.response));
            observer.complete();
          } else {
            const errors = JSON.parse(xhr.response);
            observer.error(errors);
          }
        }
      };

      xhr.upload.onprogress = (event) => {
        this.progress = Math.round((event.loaded / event.total) * 100);
        if (this._progressObserver !== undefined) {
          this._progressObserver.next(this.progress);
        }
      };

      xhr.open('PUT', s3SignalUrl, true);
      xhr.overrideMimeType('text/plain; charset=x-user-defined-binary');
      xhr.setRequestHeader('Content-Type', image_type);
      reader.onload = (evt: any) => {
        xhr.send(evt.target.result);
      };
      reader.readAsArrayBuffer(files);
    });
  }

  put(s3SignalUrl: string, file: File, image_type: string): Observable<any> {
    return this._getFormData(s3SignalUrl, file, image_type);
  }
}
