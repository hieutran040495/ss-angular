import { Injectable } from '@angular/core';
import { CasApiService } from 'shared/http-services/cas-api.service';

import 'rxjs-compat/add/operator/map';

@Injectable({
  providedIn: 'root',
})
export class GourmetSiteService {
  constructor(private casApiSv: CasApiService) {}

  fetchGourmet() {
    return this.casApiSv.get('gourmet-accounts');
  }

  authenticateUser(opts: any) {
    return this.casApiSv.post('authenticate', opts);
  }

  linkGourmet(opts: any) {
    return this.casApiSv.post('gourmet-accounts', opts);
  }

  deleteLink(idLink: number) {
    return this.casApiSv.delete(`gourmet-accounts/${idLink}`);
  }
}
