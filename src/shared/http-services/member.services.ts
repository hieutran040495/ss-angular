import { Injectable } from '@angular/core';
import { User, UserInput, UserOutput } from 'shared/models/user';
import { CasApiService } from './cas-api.service';

@Injectable({
  providedIn: 'root',
})
export class MemberServices {
  constructor(private casApiSv: CasApiService) {}

  getMembers(opts?: any) {
    return this.casApiSv.get('client-members', opts).map((res) => {
      res.data = res.data.map((user: UserInput) => new User().deserialize(user));
      return res;
    });
  }

  getMemberById(memberId: number, opts?: any) {
    return this.casApiSv.get(`client-members/${memberId}`, opts).map((res: UserInput) => new User().deserialize(res));
  }

  createMember(member: Partial<UserOutput>) {
    return this.casApiSv.post('client-members', member);
  }

  editMember(member: Partial<UserOutput>) {
    return this.casApiSv.put(`client-members/${member.id}`, member);
  }

  getRoles(opts?: any) {
    return this.casApiSv.get('roles', opts);
  }
}
