import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';
import { S3UploaderService } from './s3-uploader.service';
import { TopScreen } from 'shared/models/top-screen';

@Injectable({
  providedIn: 'root',
})
export class TopScreenService {
  constructor(private casApiSv: CasApiService, private s3UploaderSv: S3UploaderService) {}

  presignedUrls(data: any) {
    return this.casApiSv.post('presigned-urls', data);
  }

  publicImagePath(path: string, files: File, image_type: string) {
    return this.s3UploaderSv.put(path, files, image_type);
  }

  settingTopScreen(topScreenId: number, data: any) {
    return this.casApiSv.put(`top-screens/${topScreenId}`, data);
  }

  getDetailWithId(topScreenId: number, opts: any) {
    return this.casApiSv.get(`top-screens/${topScreenId}`, opts).map((res: any) => new TopScreen().deserialize(res));
  }
}
