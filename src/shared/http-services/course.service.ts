import { Injectable } from '@angular/core';
import { CasApiService } from 'shared/http-services/cas-api.service';
import { UploaderService } from 'shared/http-services/uploader.service';

import { Course, CourseInput } from 'shared/models/course';

import 'rxjs-compat/add/operator/map';
import { FileUpload } from '../interfaces/file-upload';

@Injectable({
  providedIn: 'root',
})
export class CourseService {
  constructor(private casApiSv: CasApiService, private uploaderSv: UploaderService) {}

  fetchCourses(opts?: any) {
    return this.casApiSv.get('courses', opts).map((res) => {
      res.data = res.data.map((item: CourseInput) => new Course().deserialize(item));
      return res;
    });
  }

  fetchCourseById(courseId: number, opts?: any) {
    return this.casApiSv.get(`courses/${courseId}`, opts).map((res: CourseInput) => new Course().deserialize(res));
  }

  createCourse(files: FileUpload[], course: any) {
    return this.uploaderSv.store('courses', files, course);
  }

  updateCourse(courseId: number, files: FileUpload[], course: any) {
    return this.uploaderSv.update(`courses/${courseId}`, files, course);
  }

  deleteCourse(courseId: number) {
    return this.casApiSv.delete(`courses/${courseId}`);
  }
}
