import { Injectable } from '@angular/core';
import { CasApiService } from 'shared/http-services/cas-api.service';
import { MenuSubStatus, MenuSubStatusInput } from 'shared/models/menu-substatus';
import { MenuCookingType, MenuCookingTypeInput } from 'shared/models/menu-cooking-type';
import { MenuCookingDurationInput, MenuCookingDuration } from 'shared/models/menu-cooking-duration';

@Injectable({
  providedIn: 'root',
})
export class MenuAdditionService {
  constructor(private casApiSv: CasApiService) {}

  fetchSubStatuses(opts?: any) {
    return this.casApiSv.get('sub-statuses', opts).map((res) => {
      res.data = res.data.map((item: MenuSubStatusInput) => new MenuSubStatus().deserialize(item));
      return res;
    });
  }

  fetchCookingTypes(opts?: any) {
    return this.casApiSv.get('cooking-types', opts).map((res) => {
      res.data = res.data.map((item: MenuCookingTypeInput) => new MenuCookingType().deserialize(item));
      return res;
    });
  }

  fetchCookingDurations(opts?: any) {
    return this.casApiSv.get('cooking-durations', opts).map((res) => {
      res.data = res.data.map((item: MenuCookingDurationInput) => new MenuCookingDuration().deserialize(item));
      return res;
    });
  }
}
