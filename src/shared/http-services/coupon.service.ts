import { Injectable } from '@angular/core';
import { Coupon, CouponInput, CouponOutPut } from 'shared/models/coupon';
import { CasApiService } from 'shared/http-services/cas-api.service';

@Injectable({
  providedIn: 'root',
})
export class CouponService {
  constructor(private casApiSv: CasApiService) {}

  getCoupons(opts?: any) {
    return this.casApiSv.get('coupons', opts).map((res) => {
      res.data = res.data.map((coupon: CouponInput) => new Coupon().deserialize(coupon));
      return res;
    });
  }

  createCoupon(coupon: CouponOutPut) {
    return this.casApiSv.post('coupons', coupon);
  }

  updateCoupon(couponId: number, coupon: CouponOutPut) {
    return this.casApiSv.put(`coupons/${couponId}`, coupon);
  }

  showCoupon(couponId: number, opts?: any) {
    return this.casApiSv.get(`coupons/${couponId}`, opts).map((res: CouponInput) => new Coupon().deserialize(res));
  }

  couponStatistics() {
    return this.casApiSv.get('coupons/statistics');
  }

  getCouponsAvailble(opts?: any) {
    return this.casApiSv.get('coupons/available', opts).map((res) => {
      res.data = res.data.map((coupon: CouponInput) => new Coupon().deserialize(coupon));
      return res;
    });
  }

  publicCoupon(couponId: number, data?: any) {
    return this.casApiSv.post(`coupons/${couponId}/publish`, data);
  }

  deleteCoupon(couponId: number) {
    return this.casApiSv.delete(`coupons/${couponId}`);
  }
}
