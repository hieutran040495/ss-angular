import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';

@Injectable({
  providedIn: 'root',
})
export class PhoneLookupService {
  constructor(private casApiSv: CasApiService) {}

  getPhoneLookup(data: any) {
    return this.casApiSv.post('/phone/lookup', data);
  }
}
