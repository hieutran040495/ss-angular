import { Injectable } from '@angular/core';
import { CasApiService } from 'shared/http-services/cas-api.service';

import { Genre, GenreInput } from 'shared/models/genre';

import 'rxjs-compat/add/operator/map';

@Injectable({
  providedIn: 'root',
})
export class GenreService {
  constructor(private _api: CasApiService) {}

  fetchGenres(opts?: any) {
    return this._api.get('genres', opts).map((res) => {
      res.data = res.data.map((item: GenreInput) => new Genre().deserialize(item));
      return res;
    });
  }

  fetchGenresSelect(opts?: any) {
    return this._api.get('genres', opts).map((res) => {
      res.data = res.data.map((item: GenreInput) => new Genre().deserialize(item));
      return res.data;
    });
  }
}
