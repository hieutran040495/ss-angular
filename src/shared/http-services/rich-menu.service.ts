import { Injectable } from '@angular/core';
import { UploaderService } from './uploader.service';
import { FileUpload } from 'shared/interfaces/file-upload';
import { CasApiService } from 'shared/http-services/cas-api.service';
import { RichMenu, RichMenuInput } from 'shared/models/rich-menu';
import { RichMenuItem, RichMenuItemInput } from 'shared/models/rich-menu-item';

@Injectable({
  providedIn: 'root',
})
export class RichMenuService {
  constructor(private uploaderSv: UploaderService, private apiSv: CasApiService) {}

  getRichMenus(opts?: any) {
    return this.apiSv.get('rich-menus', opts).map((res: any) => {
      res.data = res.data.map((item) => new RichMenu().deserialize(item));
      return res;
    });
  }

  getRichMenuById(richMenuId: number, opts?: any) {
    return this.apiSv.get(`rich-menus/${richMenuId}`, opts).map((res: RichMenuInput) => {
      return new RichMenu().deserialize(res);
    });
  }

  getRichMenuItems(opts?: any) {
    return this.apiSv.get('items', opts).map((res: any) => {
      res.data = res.data.map((item: RichMenuItemInput) => new RichMenuItem().deserialize(item));
      return res;
    });
  }

  configRichMenu(richMenuId: number, files: FileUpload[] = [], data: any) {
    return this.uploaderSv.store(`rich-menus/${richMenuId}`, files, data);
  }
}
