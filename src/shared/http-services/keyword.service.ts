import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';
import { ResvKeyword } from 'shared/models/resv-keyword';

@Injectable({
  providedIn: 'root',
})
export class KeywordService {
  constructor(private _api: CasApiService) {}

  fetchKeywords(opts?: any) {
    return this._api.get('keywords', opts).map((res) => {
      res.data = res.data.map((item) => new ResvKeyword().deserialize(item));

      return res;
    });
  }

  createKeyword(opts?: any) {
    return this._api.post('keywords', opts);
  }

  updateKeyword(id: number, opts?: any) {
    return this._api.put(`keywords/${id}`, opts);
  }
}
