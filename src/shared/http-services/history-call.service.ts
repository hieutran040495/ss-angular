import { Injectable } from '@angular/core';
import { CasApiService } from 'shared/http-services/cas-api.service';
import { HistoryCall, HistoryCallInput } from 'shared/models/history-call';

@Injectable({
  providedIn: 'root',
})
export class HistoryCallService {
  constructor(private _api: CasApiService) {}

  fetchHistoryCalls(opts?: any) {
    return this._api.get('calls/histories', opts).map((res) => {
      res.data = res.data.map((item: HistoryCallInput) => new HistoryCall().deserialize(item));
      return res;
    });
  }
}
