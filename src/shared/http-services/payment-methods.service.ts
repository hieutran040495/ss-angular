import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';
import { PaymentMethod, PaymentMethodInput } from 'shared/models/payment-method';

@Injectable({
  providedIn: 'root',
})
export class PaymentMethodService {
  constructor(private casApiSv: CasApiService) {}

  fetchPaymentMethods(opts?: any) {
    return this.casApiSv.get('payment-methods', opts).map((res) => {
      res.data = res.data.map((item: PaymentMethodInput) => new PaymentMethod().deserialize(item));
      return res;
    });
  }

  createPaymentMethod(data: any) {
    return this.casApiSv.post('payment-methods', data);
  }

  deletePaymentMethod(paymentMethodId: number) {
    return this.casApiSv.delete(`payment-methods/${paymentMethodId}`);
  }
}
