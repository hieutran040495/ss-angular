import { Injectable } from '@angular/core';
import { CasApiService } from 'shared/http-services/cas-api.service';
import { Notify, NotifyInput } from 'shared/models/notify';

@Injectable({
  providedIn: 'root',
})
export class NotifyService {
  constructor(private casApiSv: CasApiService) {}

  getNotify(opts?: any) {
    return this.casApiSv.get('notifications', opts).map((res) => {
      res.data = res.data.map((notify: NotifyInput) => new Notify().deserialize(notify));
      return res;
    });
  }

  getNotifyCsv(opts?: any) {
    return this.casApiSv.get('crawler-notifications', opts).map((res) => {
      res.data = res.data.map((notify: NotifyInput) => new Notify().deserialize(notify));
      return res;
    });
  }

  readNotifyCsv(id: string) {
    return this.casApiSv.get(`/crawler-notifications/${id}/solve`);
  }

  fetchNotifyById(notifyId: string | number, opts?: any) {
    return this.casApiSv
      .get(`notifications/${notifyId}`, opts)
      .map((res: NotifyInput) => new Notify().deserialize(res));
  }

  readNotify(notifyId: string) {
    return this.casApiSv.get(`notifications/${notifyId}/read`);
  }

  getNotifyStatistics() {
    return this.casApiSv.get('notifications/statistics');
  }

  settingNotify(data: any) {
    return this.casApiSv.put(`notify-settings`, data);
  }

  unSeenNotify(opts?: any) {
    return this.casApiSv.get('notifications/seen', opts);
  }

  unSeenNotifyCsv(opts?: any) {
    return this.casApiSv.get('crawler-notifications/seen', opts);
  }
}
