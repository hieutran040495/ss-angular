import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';
import { Tag } from 'shared/models/tag';

@Injectable({
  providedIn: 'root',
})
export class TagService {
  constructor(private _api: CasApiService) {}

  fetchTags(opts?: any) {
    return this._api.get('tags', opts).map((res) => {
      res.data = res.data.map((item) => new Tag().deserialize(item));

      return res;
    });
  }
}
