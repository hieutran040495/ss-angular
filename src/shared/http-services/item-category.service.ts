import { Injectable } from '@angular/core';
import { CasApiService } from 'shared/http-services/cas-api.service';
import { ItemCategoryInput, ItemCategory } from 'shared/models/item-category';

@Injectable({
  providedIn: 'root',
})
export class ItemCategoryService {
  constructor(private apiSv: CasApiService) {}

  getItemCategorySelfOrder(richMenuId: number, opts?: any) {
    return this.apiSv
      .get(`item-categories/${richMenuId}/self-order`, opts)
      .map((res: ItemCategoryInput) => new ItemCategory().deserialize(res));
  }

  settingSelfOrder(richMenuId: number, data: any) {
    return this.apiSv.put(`item-categories/${richMenuId}/self-order`, data);
  }

  getSettingRecommendScreen(categoryId: number, app: string, opts?: any) {
    const urlPath = `item-categories/${categoryId}/${app}`;
    return this.apiSv.get(urlPath, opts).map((res: ItemCategoryInput) => new ItemCategory().deserialize(res));
  }

  settingRecommendScreen(categoryId: number, app, data: any) {
    return this.apiSv.put(`item-categories/${categoryId}/${app}`, data);
  }
}
