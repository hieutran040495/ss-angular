import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';
import { UploaderService } from './uploader.service';
import { ApproveMasterData } from 'shared/interfaces/approve-master-data';

@Injectable({
  providedIn: 'root',
})
export class CookingMasterService {
  constructor(private _api: CasApiService, private _uploaderSv: UploaderService) {}

  importSubStatus(files: any) {
    return this._uploaderSv.store('sub-statuses/import', files);
  }

  importCookingType(files: any) {
    return this._uploaderSv.store('cooking-types/import', files);
  }

  importTimeLimit(files: any) {
    return this._uploaderSv.store('cooking-durations/import', files);
  }

  approveSubStatus(data: ApproveMasterData) {
    return this._api.post('sub-statuses/approve', data);
  }

  approveCookingType(data: ApproveMasterData) {
    return this._api.post('cooking-types/approve', data);
  }

  approveTimeLimit(data: ApproveMasterData) {
    return this._api.post('cooking-durations/approve', data);
  }
}
