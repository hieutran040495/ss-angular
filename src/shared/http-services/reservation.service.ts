import { Injectable } from '@angular/core';
import { Reservation, ReservationInput, ReservationOutput, TotalPriceOutput } from 'shared/models/reservation';
import { CasApiService } from './cas-api.service';
import { ReservationHistory, ReservationHistoryInput } from 'shared/models/reservation-history';
import { Receipt } from 'shared/models/receipt';
import { Menu } from 'shared/models/menu';
import { Course } from 'shared/models/course';
import { MenuBuffet } from 'shared/models/menu-buffet';
import { RESV_ITEMS_TYPE } from 'shared/enums/reservation';

@Injectable({
  providedIn: 'root',
})
export class ReservationService {
  constructor(private casApiSv: CasApiService) {}

  getReservations(opts?: any) {
    return this.casApiSv.get('reservations', opts).map((res) => {
      res.data = res.data.map((resv: ReservationInput) => new Reservation().deserialize(resv));
      return res;
    });
  }

  getReceipts(opts?: any) {
    return this.casApiSv.get('reservations', opts).map((res) => {
      res.data = res.data.map((item) => new Receipt().deserialize(item));
      return res;
    });
  }

  getReservationsCalendar(opts?: any) {
    return this.casApiSv.get('reservations/lookup', opts).map((res) => {
      res.data = res.data.map((resv: ReservationInput) => new Reservation().deserialize(resv));
      return res;
    });
  }

  getResvById(resvId: number, opts?: any) {
    return this.casApiSv.get(`reservations/${resvId}`, opts);
  }

  getResvItems(opts?: any) {
    return this.casApiSv.get('reservation-items', opts).map((res) => {
      const items: Menu[] = [];
      const courses: Course[] = [];
      const buffets: MenuBuffet[] = [];

      res.data.forEach((item) => {
        if (item.type === RESV_ITEMS_TYPE.COURSE) {
          courses.push(new Course().deserialize(item));
        }
        if (item.type === RESV_ITEMS_TYPE.ITEM) {
          items.push(new Menu().deserialize(item));
        }
        if (item.type === RESV_ITEMS_TYPE.BUFFET) {
          buffets.push(new MenuBuffet().deserialize(item));
        }
      });
      res.data = { items: items, courses: courses, buffets: buffets };
      return res;
    });
  }

  createResv(data: ReservationOutput) {
    return this.casApiSv.post('reservations', data);
  }

  updateResv(resvId: number, data: ReservationOutput) {
    return this.casApiSv.put(`reservations/${resvId}`, data);
  }

  getResvStatistic(opts?: any) {
    return this.casApiSv.get('stats/reservations/revenue', opts);
  }

  cancelReservation(resvId: number) {
    return this.casApiSv.get(`reservations/${resvId}/cancel`);
  }

  updateTable(resvId: number, data: any) {
    return this.casApiSv.put(`reservations/${resvId}/table`, data);
  }

  sendEmail(data: any) {
    return this.casApiSv.post(`reservations/send-mail`, data);
  }

  getTotalPrice(data: TotalPriceOutput) {
    return this.casApiSv.post('reservations/calculate', data);
  }

  getHistoryReservation(opts?: any) {
    return this.casApiSv.get('reservations/histories', opts).map((res) => {
      res.data = res.data.map((resv: ReservationHistoryInput) => new ReservationHistory().deserialize(resv));
      return res;
    });
  }

  paymentResv(resvId: number, data: any) {
    return this.casApiSv.put(`reservations/${resvId}/pay`, data);
  }

  checkPaymentRsv(resvId: number) {
    return this.casApiSv.get(`reservations/${resvId}/payment`);
  }

  undoPaymentResv(resvId: number) {
    return this.casApiSv.get(`reservations/${resvId}/refund`);
  }

  cleanUpResv(resvId: number) {
    return this.casApiSv.get(`reservations/${resvId}/clean-up`);
  }

  startReservation(resvId: number) {
    return this.casApiSv.get(`reservations/${resvId}/start`);
  }

  createResvWalkIn(data: any) {
    return this.casApiSv.post(`reservations/walk-in`, data);
  }

  updateResvWalkIn(id: number, data: any) {
    return this.casApiSv.put(`reservations/${id}/walk-in`, data);
  }

  downloadCSVResCourse(opts?: any) {
    return this.casApiSv.get('reservations/export-pdf', opts);
  }

  changeSmpCode(id: number) {
    return this.casApiSv.get(`reservations/${id}/change-smp-code`);
  }

  cancelReqPayment(id: number) {
    return this.casApiSv.get(`reservations/${id}/cancel-wait-for-payment`);
  }

  printReceiptByCode(receiptCode: string) {
    return this.casApiSv.get(`receipt/${receiptCode}`);
  }
}
