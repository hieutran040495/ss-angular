import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';
import { City } from 'shared/models/city';

@Injectable({
  providedIn: 'root',
})
export class CityService {
  constructor(private _api: CasApiService) {}

  fetchCities(opts?: any) {
    return this._api.get(`cities`, opts).map((res) => {
      res.data = res.data.map((item) => new City().deserialize(item));
      return res;
    });
  }
}
