import { Injectable } from '@angular/core';
import { CasApiService } from 'shared/http-services/cas-api.service';
import { MenuOption, MenuOptionInput } from 'shared/models/menu-option';

@Injectable({
  providedIn: 'root',
})
export class MenuOptionService {
  constructor(private casApiSv: CasApiService) {}

  getListMenuOptions(opts?: any) {
    return this.casApiSv.get('menu-options', opts).map((res) => {
      res.data = res.data.map((cat: MenuOptionInput) => new MenuOption().deserialize(cat));
      return res;
    });
  }

  createMenuOption(menuOption: MenuOptionInput) {
    return this.casApiSv.post('menu-options', menuOption);
  }

  editMenuOption(menuOption: MenuOptionInput) {
    return this.casApiSv.put(`menu-options/${menuOption.id}`, menuOption);
  }

  deleteMenuOption(menuOptionId: number) {
    return this.casApiSv.delete(`menu-options/${menuOptionId}`);
  }
}
