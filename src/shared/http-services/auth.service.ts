import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';
import { ForgotPwData, ResetPwData } from 'shared/interfaces/auth-pw-data';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private casApiSv: CasApiService) {}

  login(data: any) {
    return this.casApiSv.post('auth/login', data);
  }

  logout() {
    return this.casApiSv.post('auth/logout');
  }

  forgotPassword(data: ForgotPwData) {
    return this.casApiSv.post('password/forgot', data);
  }

  resetPassword(data: ResetPwData) {
    return this.casApiSv.post('password/reset', data);
  }
}
