import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';
import {
  Client,
  ClientOutput,
  ClientSubOutput,
  PrivateSettingOutput,
  SettingCancelPolicyOutput,
  SettingServiceFeeOutput,
} from 'shared/models/client';
import { UploaderService } from './uploader.service';

import { Image, ImageInput } from 'shared/models/image';

@Injectable({
  providedIn: 'root',
})
export class ClientService {
  constructor(private _api: CasApiService, private _uploaderSv: UploaderService) {}

  fetchClient(opts?: any) {
    return this._api.get('/information', opts).map((res) => {
      return new Client().deserialize(res);
    });
  }

  updateClient(data: Partial<ClientOutput | ClientSubOutput | SettingCancelPolicyOutput | PrivateSettingOutput>) {
    return this._api.put('/information', data).map((res) => {
      return new Client().deserialize(res);
    });
  }

  updateImage(files: any, params?: any) {
    return this._uploaderSv.store('client-images', files, params);
  }

  removeImage(id: number) {
    return this._api.delete(`client-images/${id}`);
  }

  settingChangeFee(data: Partial<SettingServiceFeeOutput>) {
    return this._api.put('settings/service-fee', data);
  }

  getSoldOut() {
    return this._api.get('/sold-out-image').map((img: ImageInput) => new Image().deserialize(img));
  }

  settingSoldOut(files: any[], params?: any) {
    return this._uploaderSv.store('/sold-out-image', files, params);
  }

  public settingClientLanguage(data: any = {}, params: any = {}) {
    return this._api.post('setting-languages', data);
  }
}
