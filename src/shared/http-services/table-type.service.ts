import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';

import { TableType, TableTypeInput, TableTypeOutput } from 'shared/models/table-type';
import 'rxjs-compat/add/operator/map';

@Injectable({
  providedIn: 'root',
})
export class TableTypeService {
  constructor(private casApiSv: CasApiService) {}

  fetchTableTypes(opts?: any) {
    return this.casApiSv.get('table-types', opts).map((res) => {
      res.data = res.data.map((item: TableTypeInput) => new TableType().deserialize(item));
      return res;
    });
  }

  searchTableTypes(opts?: any) {
    return this.casApiSv.get('table-types', opts).map((res) => {
      res.data = res.data.map((item: TableTypeInput) => new TableType().deserialize(item));
      return res.data;
    });
  }

  createTableType(tableType: TableTypeOutput) {
    return this.casApiSv.post('table-types', tableType);
  }

  updateTableType(tableType: TableTypeOutput) {
    return this.casApiSv.put(`table-types/${tableType.id}`, tableType);
  }

  destroyTableType(tableType: TableTypeOutput) {
    return this.casApiSv.delete(`table-types/${tableType.id}`);
  }
}
