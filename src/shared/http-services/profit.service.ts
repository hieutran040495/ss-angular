import { Injectable } from '@angular/core';

import { CasApiService } from './cas-api.service';

@Injectable({
  providedIn: 'root',
})
export class ProfitService {
  constructor(private casApiSv: CasApiService) {}

  profitSummary(opts?: any) {
    return this.casApiSv.get('stats/reservations/revenue', opts);
  }

  getAnalysisBarchartRevenue(opts?: any) {
    return this.casApiSv.get('stats/items/revenue', opts);
  }

  getAnalyticsCustomer(opts?: any) {
    return this.casApiSv.get('stats/client_users/summary', opts);
  }

  getAnalyticsPayment(opts?: any) {
    return this.casApiSv.get('stats/payment-by/summary', opts);
  }

  getAnalyticsCategory(opts?: any) {
    return this.casApiSv.get('stats/item-types/summary', opts);
  }

  getAnalyticsCoupon(opts?: any) {
    return this.casApiSv.get('stats/coupons/summary', opts);
  }
}
