import { Injectable } from '@angular/core';
import { CasApiService } from 'shared/http-services/cas-api.service';

@Injectable({
  providedIn: 'root',
})
export class DownloadFileService {
  constructor(private _api: CasApiService) {}

  exportPathCSV(opts?: any) {
    return this._api.get('/client-users/csv/export', opts);
  }

  downloadCSV(opts?: any) {
    return this._api.get('/csv/download', opts);
  }
}
