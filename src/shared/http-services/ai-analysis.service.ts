import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';
import { AiAnalysisAccess, AiAnalysisAccessInput } from 'shared/models/ai-analysis-access';

@Injectable()
export class AiAnalysisService {
  constructor(private _api: CasApiService) {}

  fetchAiAnalysis(opts?: any) {
    return this._api.get(`user/analyses`, opts);
  }

  fetchAiAnalysisAccess(opts?: any) {
    return this._api.get('/user/analyses/access', opts).map((res: any) => {
      res.data = res.data.map((item: AiAnalysisAccessInput) => new AiAnalysisAccess().deserialize(item));
      return res;
    });
  }

  getLatestUpdateTime(opts?: any) {
    return this._api.get('user/analyses/latest-update-time', opts);
  }

  exportCSV(opts?: any) {
    return this._api.get('user/analyses/export-csv', opts);
  }

  exportCSVAnalysisAccess(opts?: any) {
    return this._api.get('user/analyses/access/export-csv', opts);
  }
}
