import { Injectable } from '@angular/core';
import { CasApiService } from 'shared/http-services/cas-api.service';
import 'rxjs-compat/add/operator/map';

@Injectable({
  providedIn: 'root',
})
export class SendSalesRecordService {
  constructor(private casApiSv: CasApiService) {}

  sendEmail(opts: any) {
    return this.casApiSv.put('setting-sale-email', opts);
  }
}
