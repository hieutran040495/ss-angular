import { Injectable } from '@angular/core';
import { CasApiService } from './cas-api.service';
import { Prefecture } from 'shared/models/prefecture';

@Injectable({
  providedIn: 'root',
})
export class PrefecturesService {
  constructor(private _api: CasApiService) {}

  fetchPrefectures(opts?: any) {
    return this._api.get('prefectures', opts).map((res) => {
      res.data = res.data.map((item) => new Prefecture().deserialize(item));

      return res;
    });
  }
}
