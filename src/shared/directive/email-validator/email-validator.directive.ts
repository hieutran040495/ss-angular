import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, Validator } from '@angular/forms';

@Directive({
  selector: '[appEmailValidator][formControlName],[appEmailValidator][formControl],[appEmailValidator][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => EmailValidatorDirective),
      multi: true,
    },
  ],
})
export class EmailValidatorDirective implements Validator {
  private emailRegExp = /^[\w-\.\+]+@([\w-]+\.)+[\w-]{2,4}$/;

  constructor() {}

  validate(c: AbstractControl): { [key: string]: any } {
    const value = c.value;

    if (!!value && !this.emailRegExp.test(value)) {
      return { appEmailValidator: true };
    }

    return null;
  }
}
