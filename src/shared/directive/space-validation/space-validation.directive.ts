import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, Validator } from '@angular/forms';

@Directive({
  selector: '[appSpaceValidation][formControlName],[appSpaceValidation][formControl],[appSpaceValidation][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => SpaceValidationDirective),
      multi: true,
    },
  ],
})
export class SpaceValidationDirective implements Validator {
  constructor() {}

  validate(c: AbstractControl): { [key: string]: any } {
    const value: string = c.value;
    const spaceRegExp: RegExp = new RegExp(/\S/);

    if (!!value && !spaceRegExp.test(value)) {
      return { appSpaceValidation: true };
    }

    return null;
  }
}
