import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpaceValidationDirective } from './space-validation.directive';

@NgModule({
  declarations: [SpaceValidationDirective],
  imports: [CommonModule],
  exports: [SpaceValidationDirective],
})
export class SpaceValidationModule {}
