import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appCasMaxlength]',
})
export class CasMaxlengthDirective {
  @Input('maxlength') maxlength: number | string;

  constructor() {}

  @HostListener('document:click', ['$event'])
  handleClick(event: Event) {
    $(event.target).attr('maxlength', this.maxlength || 255);
  }
}
