import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasMaxlengthDirective } from './cas-maxlength.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [CasMaxlengthDirective],
  exports: [CasMaxlengthDirective],
})
export class CasMaxlengthModule {}
