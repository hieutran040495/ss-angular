import { NgModule } from '@angular/core';
import { PasswordValidatorDirective } from './password-validator.directive';

@NgModule({
  declarations: [PasswordValidatorDirective],
  exports: [PasswordValidatorDirective],
})
export class PasswordValidatorModule {}
