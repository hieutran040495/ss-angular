import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, Validator } from '@angular/forms';

@Directive({
  selector:
    '[appPasswordValidator][formControlName],[appPasswordValidator][formControl],[appPasswordValidator][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => PasswordValidatorDirective),
      multi: true,
    },
  ],
})
export class PasswordValidatorDirective implements Validator {
  private passwordRegExp = /^(?=.*[0-9])(?=.*[a-zA-Z])(.+){8,100}$/;

  constructor() {}

  validate(c: AbstractControl): { [key: string]: any } {
    const value = c.value;

    if (!!value && !this.passwordRegExp.test(value)) {
      return { appPasswordValidator: true };
    }

    return null;
  }
}
