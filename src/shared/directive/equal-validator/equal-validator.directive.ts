import { Directive, forwardRef, Attribute } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, Validator } from '@angular/forms';

@Directive({
  selector: '[appEqualValidator][formControlName],[appEqualValidator][formControl],[appEqualValidator][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => EqualValidatorDirective),
      multi: true,
    },
  ],
})
export class EqualValidatorDirective implements Validator {
  constructor(
    @Attribute('appEqualValidator') private appEqualValidator: string,
    @Attribute('reverse') private reverse: string,
  ) {}

  private get isReverse(): boolean {
    if (!this.reverse) {
      return false;
    }

    return this.reverse === 'true' ? true : false;
  }

  validate(c: AbstractControl): { [key: string]: any } {
    // self value
    const value = c.value;

    // control vlaue
    const e = c.root.get(this.appEqualValidator);

    // value not equal
    if (e && !!value && value !== e.value && !this.isReverse) {
      return { appEqualValidator: true };
    }

    // value equal and reverse
    if (e && value === e.value && this.isReverse) {
      delete e.errors['appEqualValidator'];

      if (!Object.keys(e.errors).length) {
        e.setErrors(null);
      }
    }

    // value not equal and reverse
    if (e && !!value && value !== e.value && this.isReverse) {
      e.setErrors({ appEqualValidator: true });
    }

    return null;
  }
}
