import { NgModule } from '@angular/core';
import { ResizeTextDirective } from './resize-text.directive';

@NgModule({
  declarations: [ResizeTextDirective],
  exports: [ResizeTextDirective],
})
export class ResizeTextModule {}
