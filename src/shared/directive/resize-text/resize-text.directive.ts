import { Directive, ElementRef, Renderer2, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appResizeText]',
})
export class ResizeTextDirective implements AfterViewInit {
  constructor(private el: ElementRef, private _r2: Renderer2) {}

  ngAfterViewInit() {
    setTimeout(() => {
      this.resize();
    }, 0);
  }

  private resize() {
    const childWidth = this.el.nativeElement.offsetWidth;
    const parentWidth = this.el.nativeElement.parentElement.offsetWidth;
    if (childWidth > parentWidth) {
      this._r2.setStyle(this.el.nativeElement, 'font-size', '70%');
    }
  }
}
