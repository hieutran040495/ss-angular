import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnFocusDirective } from './un-focus.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [UnFocusDirective],
  exports: [UnFocusDirective],
})
export class UnFocusModule {}
