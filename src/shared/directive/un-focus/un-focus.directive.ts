import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[unFocus]',
})
export class UnFocusDirective {
  constructor(private el: ElementRef) {}

  @HostListener('document:touchstart', ['$event'])
  clickedOutside($event) {
    const clickedInside = this.el.nativeElement.contains($event.target);

    if (!clickedInside && $(this.el.nativeElement).is(':visible')) {
      $(this.el.nativeElement).trigger('blur');
    }
  }
}
