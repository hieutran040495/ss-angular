import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[casPriceDirective]',
})
export class PriceDirective {
  // Allowed value
  private regex: RegExp = /^[1-9]\d*$/;
  // Allowed keyboard
  private specialKeys: Array<string> = [
    'Backspace',
    'Tab',
    'End',
    'Home',
    'ArrowLeft',
    'ArrowRight',
    'ArrowUp',
    'ArrowDown',
  ];

  constructor(private el: ElementRef) {}

  private _maxPrice: number;
  @Input('maxPrice')
  get maxPrice(): number {
    return this._maxPrice || 9;
  }
  set maxPrice(v: number) {
    this._maxPrice = v;
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }

    const current: string = this.el.nativeElement.value;
    const next: string = current.concat(event.key).replace(/\,/gi, '');

    if (next && (!String(next).match(this.regex) || next.length > this.maxPrice)) {
      event.preventDefault();
    }
  }
}
