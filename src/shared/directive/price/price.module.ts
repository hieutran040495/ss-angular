import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PriceDirective } from './price.directive';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [PriceDirective],
  exports: [PriceDirective],
})
export class PriceModule {}
