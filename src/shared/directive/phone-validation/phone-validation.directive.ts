import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, Validator } from '@angular/forms';

@Directive({
  selector: '[appPhoneValidation][formControlName],[appPhoneValidation][formControl],[appPhoneValidation][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => PhoneValidationDirective),
      multi: true,
    },
  ],
})
export class PhoneValidationDirective implements Validator {
  constructor() {}

  validate(c: AbstractControl): { [key: string]: any } {
    const value = c.value;

    const phoneRegExp: RegExp = new RegExp(/^0\d{0,10}$/);

    if (!phoneRegExp.test(value) && value && !!value.length) {
      return { appPhoneValidation: true };
    }

    return null;
  }
}
