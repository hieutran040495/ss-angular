import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhoneValidationDirective } from './phone-validation.directive';

@NgModule({
  declarations: [PhoneValidationDirective],
  imports: [CommonModule],
  exports: [PhoneValidationDirective],
})
export class PhoneValidationModule {}
