import { Directive, ElementRef, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
  selector: '[appClickOutside]',
})
export class ClickOutsideDirective {
  constructor(private _elementRef: ElementRef) {}

  @Output()
  clickOutside: EventEmitter<any> = new EventEmitter();

  @HostListener('document:click', ['$event'])
  handleClick(event: Event) {
    const clickedInside = this._elementRef.nativeElement.contains(event.target);

    if (!clickedInside) {
      this.clickOutside.emit(null);
    }
  }

  @HostListener('document:keydown', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.clickOutside.emit(null);
    }
  }
}
