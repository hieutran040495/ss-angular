import { Directive, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
  selector: '[appDoubleClick]',
})
export class DoubleClickDirective {
  @Output() doubleClick: EventEmitter<any> = new EventEmitter<any>();
  count: number = 0;

  public constructor() {}

  @HostListener('click', ['$event']) onclick() {
    this.count++;
    const timeout = setTimeout(() => {
      if (this.count === 1) {
        this.count = 0;
      }

      if (this.count > 1) {
        this.count = 0;
        this.doubleClick.emit();
      }

      clearTimeout(timeout);
    }, 250);
  }
}
