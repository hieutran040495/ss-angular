import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoubleClickDirective } from './double-click.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [DoubleClickDirective],
  exports: [DoubleClickDirective],
})
export class DoubleClickModule {}
