import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { OnlyNumericDirective } from './only-numeric.directive';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [OnlyNumericDirective],
  exports: [OnlyNumericDirective],
})
export class OnlyNumericModule {}
