import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { Regexs } from 'shared/constants/regex';
import * as remove from 'lodash/remove';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[OnlyNumeric]',
})
export class OnlyNumericDirective {
  // Allowed value
  private regex: RegExp;
  private regexFullWidth: RegExp = new RegExp(Regexs.fullWidthCharacter);
  // Allowed keyboard
  private specialKeys: Array<string> = [
    'Backspace',
    'Tab',
    'End',
    'Home',
    'ArrowLeft',
    'ArrowRight',
    'ArrowUp',
    'ArrowDown',
  ];

  constructor(private el: ElementRef) {}

  @Input('NumberPattern') numberPattern: RegExp = Regexs.only_number;
  @Input('AllowDelete') allowDelete: boolean = false;

  @HostListener('keydown', ['$event'])
  onKeyDown(event: any) {
    if (this.allowDelete) {
      this.specialKeys = remove(this.specialKeys, function(key) {
        return key !== 'Backspace';
      });
    }
    if (
      this.specialKeys.indexOf(event.key) !== -1 ||
      // Allow: Ctrl+A
      (event.keyCode === 65 && (event.ctrlKey || event.metaKey)) ||
      // Allow: Ctrl+C
      (event.keyCode === 67 && (event.ctrlKey || event.metaKey)) ||
      // Allow: Ctrl+V
      (event.keyCode === 86 && (event.ctrlKey || event.metaKey)) ||
      // Allow: Ctrl+X
      (event.keyCode === 88 && (event.ctrlKey || event.metaKey)) ||
      // Allow: home, end, left, right
      (event.keyCode >= 35 && event.keyCode <= 39)
      // Allow: 0 - 9
      // (event.keyCode >= 48 && event.keyCode <= 57) ||
      // (event.keyCode >= 96 && event.keyCode <= 105)
    ) {
      return;
    }

    this.regex = new RegExp(this.numberPattern);

    const current: string = this.el.nativeElement.value;
    const next: string = current.concat(event.key).replace(/\,/gi, '');
    if (next && (!String(next).match(this.regex) || String(next).match(this.regexFullWidth))) {
      event.preventDefault();
    }
  }

  @HostListener('keyup', ['$event'])
  onKeyUp(event: any) {
    if (!this.el.nativeElement.max || +this.el.nativeElement.max !== 100) {
      return;
    }
    if (+this.el.nativeElement.value > +this.el.nativeElement.max) {
      this.el.nativeElement.value = 100;
      return;
    }
  }
}
