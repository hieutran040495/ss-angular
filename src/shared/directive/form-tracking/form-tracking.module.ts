import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormTrackingDirective } from './form-tracking.directive';

@NgModule({
  declarations: [FormTrackingDirective],
  imports: [CommonModule],
  exports: [FormTrackingDirective],
})
export class FormTrackingModule {}
