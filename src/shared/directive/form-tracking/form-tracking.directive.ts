import { Directive, Output, HostListener, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appFormTracking]',
})
export class FormTrackingDirective {
  @Output() tracking: EventEmitter<boolean> = new EventEmitter<boolean>();

  private _formExist = ['INPUT', 'TEXTAREA', 'BUTTON'];

  constructor() {}

  @HostListener('click', ['$event'])
  @HostListener('touch', ['$event'])
  onTouch($event: any) {
    if (
      (this._formExist.includes($event.target.tagName.toUpperCase()) ||
        $event.target.classList.contains('ng-input') ||
        $event.target.classList.contains('memo-form__keyword-item')) &&
      !$event.target.disabled
    ) {
      this.tracking.emit(true);
    }
  }
}
