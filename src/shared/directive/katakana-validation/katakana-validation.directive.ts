import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, Validator } from '@angular/forms';

@Directive({
  selector:
    '[appKatakanaValidation][formControlName],[appKatakanaValidation][formControl],[appKatakanaValidation][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => KatakanaValidationDirective),
      multi: true,
    },
  ],
})
export class KatakanaValidationDirective implements Validator {
  constructor() {}

  validate(c: AbstractControl): { [key: string]: any } {
    const value: string = c.value;

    const katakanaRegExp: RegExp = new RegExp(/^[ァ-・ヽヾ゛゜ー]+$/);
    const spaceRegExp: RegExp = new RegExp(/\S/);

    if (spaceRegExp.test(value) && !katakanaRegExp.test(value)) {
      return { appKatakanaValidation: true };
    }

    return null;
  }
}
