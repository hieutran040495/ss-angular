import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KatakanaValidationDirective } from './katakana-validation.directive';

@NgModule({
  declarations: [KatakanaValidationDirective],
  imports: [CommonModule],
  exports: [KatakanaValidationDirective],
})
export class KatakanaValidationModule {}
