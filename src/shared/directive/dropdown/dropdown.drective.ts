import { Directive, HostListener, ElementRef, Renderer2, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appDropdown]',
})
export class DropdownDirective implements AfterViewInit {
  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

  get isOpened(): boolean {
    return this.renderer.parentNode(this.elementRef.nativeElement).classList.contains('is-open');
  }

  ngAfterViewInit() {
    this.showActiveLink();
  }

  showActiveLink() {
    const subParent = this.renderer.parentNode(this.elementRef.nativeElement);
    const submenu = this.elementRef.nativeElement.nextSibling;

    setTimeout(() => {
      for (const element of submenu.children) {
        for (const subElement of element.children) {
          if (subElement.classList.contains('active')) {
            this.renderer.addClass(subParent, 'is-open');
          }
        }
      }
    }, 0);
  }

  @HostListener('click')
  openDropdown(e) {
    const subParent = this.renderer.parentNode(this.elementRef.nativeElement);
    const rootParent = this.renderer.parentNode(subParent);
    const siblings = rootParent.children;

    for (const element of siblings) {
      if (element !== subParent) {
        this.renderer.removeClass(element, 'is-open');
      }
    }

    if (!this.isOpened) {
      this.renderer.addClass(subParent, 'is-open');
    } else {
      this.renderer.removeClass(subParent, 'is-open');
    }
  }
}
