import { NgModule } from '@angular/core';
import { DropdownDirective } from './dropdown.drective';

@NgModule({
  declarations: [DropdownDirective],
  exports: [DropdownDirective],
})
export class DropDownModule {}
