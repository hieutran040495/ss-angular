import { Directive, HostListener } from '@angular/core';
import { CasDialogService } from 'app/modules/cas-dialog/cas-dialog.service';
import { NgSelectComponent } from '@ng-select/ng-select';

@Directive({
  selector: '[appScroller]',
})
export class ScrollerDirective {
  constructor(private _casDialogSv: CasDialogService) {}

  // handle host scroll
  @HostListener('scroll', ['$event']) public scrolled($event: Event) {
    this.elementScrollEvent($event);
  }

  // handle window scroll
  @HostListener('window:scroll', ['$event']) public windowScrolled($event: Event) {
    this.windowScrollEvent($event);
  }

  protected windowScrollEvent($event: Event) {
    this._closeNgSelect();
  }

  protected elementScrollEvent($event: Event) {
    this._closeNgSelect();
  }

  private _closeNgSelect() {
    this._casDialogSv.casSelectComponent.forEach((item: NgSelectComponent) => {
      item.close();
    });
  }
}
