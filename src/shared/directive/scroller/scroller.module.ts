import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollerDirective } from './scroller.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [ScrollerDirective],
  exports: [ScrollerDirective],
})
export class ScrollerModule {}
