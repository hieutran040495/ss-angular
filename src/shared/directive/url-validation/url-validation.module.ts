import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UrlValidationDirective } from './url-validation.directive';

@NgModule({
  declarations: [UrlValidationDirective],
  imports: [CommonModule],
  exports: [UrlValidationDirective],
})
export class UrlValidationModule {}
