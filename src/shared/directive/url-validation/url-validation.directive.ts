import { Directive, forwardRef } from '@angular/core';
import { AbstractControl, Validator, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[appUrlValidation],[appUrlValidation][formControl],[appUrlValidation][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => UrlValidationDirective),
      multi: true,
    },
  ],
})
export class UrlValidationDirective implements Validator {
  constructor() {}

  validate(c: AbstractControl): { [key: string]: any } {
    const value: string = c.value;

    const urlRegExp: RegExp = new RegExp(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.-]*)*\/?$/);
    if (value !== null && value !== '' && !urlRegExp.test(value)) {
      return { appUrlValidation: true };
    }

    return null;
  }
}
