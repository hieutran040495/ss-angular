import * as moment from 'moment';
import { ClientWorkingTimeWeek } from 'shared/models/client-working-week';
import { BsDatepickerContainerComponent } from 'ngx-bootstrap/datepicker/themes/bs/bs-datepicker-container.component';
import { promiseDelay } from 'shared/utils/promise-delay';
export class DatePicker {
  public _getWorkingTime: () => Promise<ClientWorkingTimeWeek>;

  onShownDatepicker(event: BsDatepickerContainerComponent) {
    event.viewMode.subscribe((res) => {
      if (res === 'day') {
        this._onClickNextPrev();
        this._checkAndAddClassVisible();
      }
    });

    const dayHoverHandler = event.dayHoverHandler;
    const hoverWrapper = ($event) => {
      const cell = $event;
      cell.isHovered = false;
      return dayHoverHandler($event);
    };
    event.dayHoverHandler = hoverWrapper;
  }

  _onClickNextPrev() {
    const elPrev = $('button.previous');
    elPrev.off('click');
    elPrev.on('click', (e) => {
      this._onClickNextPrev();
      this._checkAndAddClassVisible();
    });

    const elNext = $('button.next');
    elNext.off('click');
    elNext.on('click', (e) => {
      this._onClickNextPrev();
      this._checkAndAddClassVisible();
    });
  }

  async _checkAndAddClassVisible() {
    await promiseDelay(0);

    const workingTime: ClientWorkingTimeWeek = await this._getWorkingTime();

    const cells = $('[role=gridcell]');
    const currents = $('button.current');
    if (currents.length < 2) {
      return;
    }

    const month = $(currents[0])
      .children()
      .text();
    const year = $(currents[1])
      .children()
      .text();

    cells.each((key, value) => {
      const day = $(value)
        .children()
        .text();

      if (
        $(value)
          .children()
          .hasClass('is-other-month')
      ) {
        if (Number(day) > 21) {
          // handle prev month
        } else {
          // handle next month
        }
      } else {
        this._handleDayOff(moment(`${year} ${month} ${day}`, 'YYYY MMM D'), $(value), workingTime);
      }
    });
  }

  _handleDayOff(date: moment.Moment, el: JQuery, workingTime: ClientWorkingTimeWeek) {
    const working = workingTime.getWorkingTime(date);

    if (!working) {
      el.children().addClass('day-off');
    }
  }
}
