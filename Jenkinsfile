#!/usr/bin/env groovy

pipeline {
    agent {
        docker {
            image 'localhost:5000/web-lint'
            label 'master'
            registryUrl 'http://localhost:5000'
        }
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '3'))
        disableConcurrentBuilds()
    }

    stages {
        stage('Notify on starting') {
            steps {
                script {
                    sendMessage('Starting')
                }
            }
        }

        stage('Install dependencies') {
            steps {
                sh 'yarn install --pure-lockfile --no-progress --non-interactive --ignore-scripts --silent'
            }
        }

        stage('Linting') {
            steps {
                sh 'yarn run full-lint'
            }
        }
    }

    post {
        always {
            sendMessage(currentBuild.currentResult)
        }

        success {
            script {
                if (env.BRANCH_NAME in ['develop']) {
                    build job: '../../dev/client-web', wait: false
                }
            }
        }
    }
}

def sendMessage(String status) {
    def color = '#c72c00'
    def success = true

    if (status == 'Starting') {
        color = '#a0d4f1'
    } else if (status == 'SUCCESS') {
        color = '#00d213'
    } else {
        success = false
    }

    // https://wiki.jenkins-ci.org/display/JENKINS/Build+User+Vars+Plugin
    // wrap([$class: 'BuildUser']) {
    rocketSend channel: '#cas-cicd',
        rawMessage: true,
        message: success ? '' : "@son.dc @nga.ttt @hieu.ttn ${status}",
        attachments: [[
            color: color,
            title: "#${env.BUILD_NUMBER} pipeline » client-web » ${env.BRANCH_NAME}",
            collapsed: true,
            text: "${currentBuild.rawBuild.getCauses()[0].getShortDescription()}\nStatus: ${status}\nURL: ${env.BUILD_URL}console"
        ]]
    // }
}
